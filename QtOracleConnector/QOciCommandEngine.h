#ifndef QOCICOMMANDENGINE_H
#define QOCICOMMANDENGINE_H

#include <QDbCommandEngine>

class QOciConnection;

class QOciCommandEngine : public QDbCommandEngine
{
   Q_DISABLE_COPY(QOciCommandEngine)
   Q_DECLARE_TR_FUNCTIONS(QOciCommandEngine)

public:
   QOciCommandEngine(QOciConnection* connection);
   virtual ~QOciCommandEngine();

   QDbState rowCount(int &count) const override;

   bool atEnd() const override;

   QDbState next() override;

   QDbSchema::Table   resultSchema() const override;
   QDbState           columnValue(int index, Variant &value) const override;
   QDbState           columnValue(const QString &name, Variant &value) const override;

protected:
   class OciPhInfo;

   QDbConnectionData* connection() override;
   PhInfo* createPhInfo(const QDbSchema::Column& schema, QDbConnection::PhType phType) override;
   QDbState setPlaceholderType(PhInfo* phInfo, QDbDataType type) override; // Changes also an already defined placeholder
   QDbState setPlaceholderValue(PhInfo* phInfo, const Variant& value) override; // Doesn't change the placeholder type if it is already defined
   QDbState placeholderValue(const PhInfo* phInfo, Variant& value) const override;
   QDbState prepare(const QList<PhInfo*>& phList) override;
   QDbState exec(const QList<PhInfo*>& phList, int rowsToFetch) override;
   void clear() override;

private:
   struct Data;

private:
   Data* _data = nullptr;
};

#endif // QOCICOMMANDENGINE_H
