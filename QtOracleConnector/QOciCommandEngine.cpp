#include "QOciCommandEngine.h"
#include "QOciConnection.h"
#include "OciBuffers.h"
#include <QVector>
#include <SmartPointer.h>
#include <wchar.h>

//
// struct QOciCommandEngine::Data
//
struct QOciCommandEngine::Data
{
   Data(QOciConnection* c) : connection(c) {}
   ~Data() { clear(); }

   void clearOutputBuffers();
   void clear();

   bp<QOciConnection> connection;

   OCIStmt *stmthp = nullptr; // As long as stmthp is valid _statement must not be changed!!!
   int dmlCommand = OCI_STMT_UNKNOWN;
   QVector<OciOutputBuffer> outputBuffers;
   int arraySize = 0;
   int rowsFetched = 0;
   int rowIndex = 0;
   bool hasMore = false;
};

void QOciCommandEngine::Data::clearOutputBuffers()
{
   connection->releaseOutputBuffers(outputBuffers, arraySize);
   outputBuffers.clear();
   arraySize = 0;
}

void QOciCommandEngine::Data::clear()
{
   clearOutputBuffers();
   if (stmthp)
   {
      connection->releaseStatement(stmthp);
      stmthp = nullptr;
   }
   dmlCommand = OCI_STMT_UNKNOWN;
   rowsFetched = 0;
   rowIndex = 0;
   hasMore = false;
}

//
// class QOciCommandEngine
//
QOciCommandEngine::QOciCommandEngine(QOciConnection* connection) : _data(new Data(connection))
{
}

QOciCommandEngine::~QOciCommandEngine()
{
   delete _data;
}

QDbState QOciCommandEngine::rowCount(int &count) const
{
   QDbState dbState(_statement);

   if (_data->stmthp)
      dbState = _data->connection->rowCount(_data->stmthp, count);

   return dbState;
}

bool QOciCommandEngine::atEnd() const
{
   return _data->rowIndex == _data->rowsFetched;
}

QDbState QOciCommandEngine::next()
{
   QDbState dbState(_statement);

   if (_data->rowIndex < _data->rowsFetched)
      _data->rowIndex++;

   if (_data->rowIndex == _data->rowsFetched && _data->hasMore)
   {
      _data->rowIndex = 0;
      _data->rowsFetched = _data->arraySize; // Must be initialized with the number of rows to fetch!
      _data->hasMore = false;

      dbState = _data->connection->fetch(_data->stmthp, _data->rowsFetched, _data->hasMore);

      if (_data->rowsFetched < 0)
         _data->rowsFetched = 0;
   }

   return dbState;
}

QDbSchema::Table QOciCommandEngine::resultSchema() const
{
   QDbSchema::Table table;

   table.name = QLatin1String("SELECT_LIST");
   foreach (const OciOutputBuffer &buffer, _data->outputBuffers)
      table.columns.append(buffer.schema);

   return table;
}

QDbState QOciCommandEngine::columnValue(int index, Variant &value) const
{
   QDbState dbState(_statement);

   if (_data->rowIndex >= _data->rowsFetched)
      dbState = QDbState(QDbState::Error, tr("Invalid position of row counter"), QDbState::Statement);
   else if (index < 0 || index >= _data->outputBuffers.count())
      dbState = QDbState(QDbState::Error, tr("Index %1 not in result set").arg(index), QDbState::Statement);
   else
   {
      const OciOutputBuffer &outBuffer = _data->outputBuffers.at(index);

      if (outBuffer.indicator[_data->rowIndex] != -1) // NULL check
      {
         dbState = _data->connection->toVariant(
            (unsigned char *)outBuffer.buffer + _data->rowIndex * outBuffer.size,
            outBuffer.actualSize[_data->rowIndex],
            outBuffer.schema.vendorType,
            outBuffer.schema.scale,
            value);
      }
      else
         value.clear();
   }

   return dbState;
}

QDbState QOciCommandEngine::columnValue(const QString &name, Variant &value) const
{
   for (int i = 0; i < _data->outputBuffers.count(); ++i)
   {
      if (_data->outputBuffers.at(i).schema.name.compare(name, Qt::CaseInsensitive) == 0)
         return columnValue(i, value);
   }

   return QDbState(QDbState::Error, tr("Column name %1 not found in result set").arg(name), QDbState::Statement);
}

QDbConnectionData* QOciCommandEngine::connection()
{
   return _data->connection.data();
}

QDbCommandEngine::PhInfo* QOciCommandEngine::createPhInfo(const QDbSchema::Column& schema, QDbConnection::PhType phType)
{
   return new OciPhInfo(schema, phType, _data->connection.data());
}

QDbState QOciCommandEngine::setPlaceholderType(PhInfo* phInfo, QDbDataType type)
{
   if (phInfo->type() != type)
   {
      if (_data->stmthp)
         return QDbState(QDbState::Error, tr("The data type of the placeholder at position %1 cannot be set after execution").arg(phInfo->positions().first()), _statement);

      phInfo->setType(type);

      if (!phInfo->isValid())
         return errorInferringDataType(type, phInfo);
   }

   return QDbState();
}

QDbState QOciCommandEngine::setPlaceholderValue(PhInfo* phInfo, const Variant& value)
{
   if (phInfo->type() == DT_UNKNOWN)
   {
      // Infer the type from the value
      phInfo->setType(dbTypeFromVariant(value));

      if (!phInfo->isValid())
         return errorInferringDataType(value, phInfo);
   }

   return phInfo->setValue(value);
}

QDbState QOciCommandEngine::placeholderValue(const PhInfo* phInfo, Variant& value) const
{
   const auto ph = dynamic_cast<const OciPhInfo*>(phInfo);

   return ph->value(value);
}

QDbState QOciCommandEngine::prepare(const QList<PhInfo*>& phList)
{
   return QDbState();
}

QDbState QOciCommandEngine::exec(const QList<PhInfo*>& phList, int rowsToFetch)
{
   QDbState dbState(_statement);

   _data->rowsFetched = 0;
   _data->rowIndex = 0;
   _data->hasMore = false;

   // Because of the buffers it is currently not possible to repeat a statement
   if (_data->stmthp)
   {
      // Reset the statement
      _data->connection->releaseStatement(_data->stmthp);
      _data->stmthp = nullptr;
      _data->dmlCommand = OCI_STMT_UNKNOWN;
   }

   if (_data->stmthp == nullptr)
   {
      // Are all placeholders defined?
      for (int i = 0; i < phList.count(); i++)
      {
         if (!phList[i]->isValid())
            return QDbState(QDbState::Error, tr("Placeholder at position %1 is not defined").arg(i + 1), _statement);
      }

      // Complete the statement if its a procedure call
      // This has to be done here because of the optional parameters!

      if (_functionSchema.isValid())
      {
         // Create a proper PL/SQL statement
         // This has to correspond to QDbCommandEngine::setStatement() !!!

         if (!_preparedStatement.isEmpty())
         {
            // The statement has to be prepared again because the parameter may have changed
            _preparedStatement.clear();
            for (int i = 0; i < phList.count(); i++)
            {
               phList[i]->clearPositions();
            }
         }

         int index = 0;
         int pos = 1;

         // First add the return value
         if (_functionSchema.retVal.isValid())
         {
            phList[index]->addPosition(pos);
            _preparedStatement = QString(":PAR%1 := ").arg(pos++, 2, 10, QLatin1Char('0'));
            index++;
         }

         // Add the function identifier
         _preparedStatement.append(_functionSchema.schema);
         if (!_functionSchema.packageName.isEmpty())
         {
            _preparedStatement.append(".");
            _preparedStatement.append(_functionSchema.packageName);
         }
         _preparedStatement.append(".");
         _preparedStatement.append(_functionSchema.name);

         // Append the rest of the parameters
         _preparedStatement.append(QLatin1Char('('));
         foreach(const QDbSchema::Column &argument, _functionSchema.arguments)
         {
            auto ph = dynamic_cast<OciPhInfo*>(phList[index]);

            if (!ph->isDefault())
            {
               ph->addPosition(pos);
               if (!_preparedStatement.endsWith(QLatin1Char('(')))
                  _preparedStatement.append(QLatin1String(", "));
               _preparedStatement.append(argument.name);
               _preparedStatement.append(QLatin1String(" => "));
               _preparedStatement.append(QString::fromLatin1(":PAR%1").arg(pos++, 2, 10, QLatin1Char('0')));
            }
            index++;
         }
         _preparedStatement.append(QLatin1Char(')'));

         // Mark the statement as PL/SQL block
         _preparedStatement.prepend(QLatin1String("BEGIN "));
         _preparedStatement.append(QLatin1String("; END;"));
      }
      else if (_preparedStatement.isEmpty())
      {
         // Statement without placeholder
         _preparedStatement = _statement;
      }

      // Prepare the statement to bind the placeholders
      dbState = _data->connection->prepareStatement(&_data->stmthp, (const wchar_t *)_preparedStatement.utf16()); // The statement must not be changed before the statement handle is freed!
      if (dbState.type() == QDbState::Error)
         return dbState;

      dbState = _data->connection->statementType(_data->stmthp, _data->dmlCommand);
      if (dbState.type() == QDbState::Error)
         return dbState;

      // Bind placeholders
      auto pos = 0; // For PL/SQL

      for (int index = 0; index < phList.count(); index++)
      {
         auto ph = dynamic_cast<OciPhInfo*>(phList[index]);

         if (!ph->isDefault()) // If the bind value is default it doesn't appear in the statement
         {
            if (_data->dmlCommand == OCI_STMT_BEGIN)
               pos++; // A PL/SQL statement (see Oracle documentation)
            else
               pos = ph->positions().first(); // It is sufficient to supply the first position of equally named placeholders

            dbState = _data->connection->bindParameter(_data->stmthp, pos, ph, _data->dmlCommand);

            if (dbState.type() == QDbState::Error)
               return dbState;
         }
      }
   }

   // Execute the statement
   if (_data->dmlCommand == OCI_STMT_SELECT)
      dbState = _data->connection->executeStatement(_data->stmthp, 0);
   else
      dbState = _data->connection->executeStatement(_data->stmthp, 1);
   if (dbState.type() == QDbState::Error)
      return dbState.withSourceText(description());

   // Prepare fetch
   _data->hasMore = true;

   // Bind output buffers
   _data->clearOutputBuffers();
   _data->arraySize = rowsToFetch;
   dbState = _data->connection->defineOutputBuffers(_data->stmthp, _data->outputBuffers, _data->arraySize);

   return dbState;
}

void QOciCommandEngine::clear()
{
   QDbCommandEngine::clear();
   _data->clear();
}
