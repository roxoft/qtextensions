#include "TestCases.h"
#include "QOciConnection.h"
#include <QDbCommand.h>
#include <QDateTime>
#include <QSettingsFile.h>
#include <QDir>
#include <QThread>

static QString toQString(const QString& text) { return text; }

static QString toQString(const QByteArray& binary)
{
   return QString::fromLatin1(binary.toHex());
}

static QOciConnection* createConnection(const QString &dbName, const QString &dbUser, const QString &dbPassword)
{
   auto ociConnection = new QOciConnection(dbUser, dbPassword, dbName);

   return ociConnection;
}

class ThreadingTest : public QThread
{
public:
   ThreadingTest() {}
   virtual ~ThreadingTest() {}

   void run() override;

   inline void addError(const QString& message)
   {
      errorMessages.append(message);
   }

   inline void errorIfNot(bool predicate, const QString& message)
   {
      if (!predicate)
         errorMessages.append(message);
   }

   inline void errorIf(bool predicate, const QString& message)
   {
      if (predicate)
         errorMessages.append(message);
   }

   template<typename T1, typename T2>
   inline void errorIfNotEqualEx(const T1& left, const T2& right, const QString& message)
   {
      if (!(left == right))
         errorMessages.append(message.arg(toQString(left).left(200)).arg(toQString(right).left(200)));
   }

   template<typename T1, typename T2>
   inline void errorIfNotEqual(const T1& left, const T2& right, const QString& message)
   {
      if (!(left == right))
         errorMessages.append(message.arg(left).arg(right));
   }

public:
   QString dbName;
   QString dbAdmin;
   QString dbAdminPassword;
   QStringList errorMessages;
};

void ThreadingTest::run()
{
   QDbConnection connection = createConnection(dbName, dbAdmin, dbAdminPassword);

   T_ERROR_IF(connection.state().type() == QDbState::Error, connection.state().message());

   QDbCommand query(connection);

   query.setStatement("select test1, test2, test3, test4, test5, test6, test7, test8, test9 from cr_oci_test order by test1");

   T_ERROR_IF(!query.exec(), query.state().message());

   auto resultSet = query.result();

   T_IS_TRUE(resultSet.next());

   T_EQUALS_EX(resultSet.valueAt(QString("test1")), QDecimal(42));
   T_EQUALS_EX(resultSet.valueAt(QString("test2")), QDecimal(9999999999L));
   T_EQUALS_EX(resultSet.valueAt(QString("test3")), QDecimal(100.99));
   T_EQUALS(resultSet.valueAt(QString("test4")).toString().replace("\r", "\\r").replace("\n", "\\n"), "\\Path\\Name\\nLine\\\\nNew \\ä\\nö\\rü\\tß line\\");
   T_EQUALS(resultSet.valueAt(QString("test5")).toString().replace("\r", "\\r").replace("\n", "\\n"), "\\Path\\Name\\r\\nLine\\\\r\\nNew \\ä\\nö\\rü\\tß line\\");
   T_EQUALS(resultSet.valueAt(QString("test6")).toString().replace("\r", "\\r").replace("\n", "\\n"), "\\Path\\Name\\nLine\\\\nNew \\ä\\nö\\rü\\tß line\\");
   T_EQUALS(resultSet.valueAt(QString("test7")).toString().replace("\r", "\\r").replace("\n", "\\n"), "\\Path\\Name\\r\\nLine\\\\r\\nNew \\ä\\nö\\rü\\tß line\\");
   T_EQUALS_EX(resultSet.valueAt(QString("test8")), QDateTimeEx(2015, 1, 11, 17, 30, 0));
   T_IS_TRUE(resultSet.valueAt(QString("test9")) == QByteArray("Binärdaten"));

   T_ERROR_IF(query.state().type() == QDbState::Error, query.state().message());

   T_IS_TRUE(resultSet.next());

   T_EQUALS_EX(resultSet.valueAt(QString("test1")), QDecimal(123456));
   T_EQUALS_EX(resultSet.valueAt(QString("test2")), QDecimal(9999999999L));
   T_EQUALS_EX(resultSet.valueAt(QString("test3")), QDecimal(100.99));
   T_IS_TRUE(resultSet.valueAt(QString("test4")) == QString(3000, QLatin1Char('z')));
   T_IS_TRUE(resultSet.valueAt(QString("test5")) == QString("ř").repeated(1500));
   T_IS_TRUE(resultSet.valueAt(QString("test6")) == QString(5000, QLatin1Char('u')));
   T_IS_TRUE(resultSet.valueAt(QString("test7")) == QString(5000, QLatin1Char('n')));
   T_EQUALS_EX(resultSet.valueAt(QString("test8")), QDateTimeEx(2015, 1, 11, 17, 30, 0));
   T_IS_TRUE(resultSet.valueAt(QString("test9")) == QByteArray(5000, 'b'));

   T_ERROR_IF(query.state().type() == QDbState::Error, query.state().message());

   T_IS_TRUE(resultSet.next());

   T_EQUALS_EX(resultSet.valueAt(QString("test1")), QDecimal(987654));
   T_EQUALS_EX(resultSet.valueAt(QString("test2")), QDecimal(1));
   T_EQUALS_EX(resultSet.valueAt(QString("test3")), QDecimal(12.5));
   T_IS_TRUE(resultSet.valueAt(QString("test4")) == QString(4000, QLatin1Char('a')));
   T_IS_TRUE(resultSet.valueAt(QString("test5")) == QString("ß").repeated(2000));
   T_IS_TRUE(resultSet.valueAt(QString("test6")) == QString(100000, QLatin1Char('f')));
   T_IS_TRUE(resultSet.valueAt(QString("test7")) == QString(100000, QLatin1Char('g')));
   T_EQUALS_EX(resultSet.valueAt(QString("test8")), QDateTimeEx(2018, 7, 12, 5, 15, 0));
   T_IS_TRUE(resultSet.valueAt(QString("test9")) == QByteArray(100000, 'a'));

   T_ERROR_IF(query.state().type() == QDbState::Error, query.state().message());

   T_IS_TRUE(!resultSet.next());
}

void TestCases::init()
{
   TestFramework::init();

   T_ASSERT(!dataDir().isEmpty());

   QFile credentialsFile(QDir(dataDir()).filePath("OracleCredentials.ini"));

   T_ASSERT(credentialsFile.exists());

   QSettingsFile settings(&credentialsFile);

   _dbName = settings.value("Name").toString();
   _dbSchema = settings.value("Schema").toString();
   _dbAdmin = settings.value("Admin/User").toString();
   _dbAdminPassword = settings.value("Admin/Password").toString();
   _dbUser = settings.value("User/User").toString();
   _dbUserPassword = settings.value("User/Password").toString();
   _dbUserRole = settings.value("User/Role").toString();

   T_IS_TRUE(!_dbName.isEmpty());
   T_IS_TRUE(!_dbSchema.isEmpty());
   T_IS_TRUE(!_dbAdmin.isEmpty());
   T_IS_TRUE(!_dbAdminPassword.isEmpty());
   T_IS_TRUE(!_dbUser.isEmpty());
   T_IS_TRUE(!_dbUserPassword.isEmpty());
   T_IS_TRUE(!_dbUserRole.isEmpty());
}

void TestCases::deinit()
{
   TestFramework::deinit();
   _dbName.clear();
   _dbSchema.clear();
   _dbAdmin.clear();
   _dbAdminPassword.clear();
   _dbUser.clear();
   _dbUserPassword.clear();
   _dbUserRole.clear();
}

void TestCases::stmtParsingTests()
{
   QDbConnection connection(createConnection(_dbName, _dbAdmin, _dbAdminPassword));

   if (connection.state().type() == QDbState::Error)
   {
      addError(connection.state().message());
      return;
   }

   QDbCommand command(connection);

   command.setStatement("update cr_oci_test set test7 = :ph02, test6 = ?, test4 = ?, test5 = :ph04");
   command.parsePlaceholders();
   T_EQUALS(command.preparedStatement(), "update cr_oci_test set test7 = :ph02, test6 = :PH01, test4 = :PH03, test5 = :ph04");

   command.setStatement("--update cr_oci_test set test7 = :ph02, test6 = ?, test4 = ?, test5 = :ph04");
   command.parsePlaceholders();
   T_EQUALS(command.preparedStatement(), "--update cr_oci_test set test7 = :ph02, test6 = ?, test4 = ?, test5 = :ph04");

   command.setStatement("update cr_oci_test set test7 = :ph02, /*test6 = ?, */test4 = ?, test5 = :ph04");
   command.parsePlaceholders();
   T_EQUALS(command.preparedStatement(), "update cr_oci_test set test7 = :ph02, /*test6 = ?, */test4 = :PH01, test5 = :ph04");

   command.setStatement("  update cr_oci_test set test7 = :2, test6 = :ph01, test4 = :ph03, test5 = :");
   command.parsePlaceholders();
   T_EQUALS(command.preparedStatement(), "update cr_oci_test set test7 = :2, test6 = :ph01, test4 = :ph03, test5 = :");

   command.setStatement("select gpd_inhalt \"qpd:inhalt\", // This is a remark\ngpd_bemerkung from ihs_gp_dokumente where gpd_id = :gpd_id   ");
   command.parsePlaceholders();
   T_EQUALS(command.preparedStatement(), "select gpd_inhalt \"qpd:inhalt\", // This is a remark\ngpd_bemerkung from ihs_gp_dokumente where gpd_id = :gpd_id");
}

void TestCases::sqlTest()
{
   QDbConnection connection(createConnection(_dbName, _dbAdmin, _dbAdminPassword));

   if (connection.state().type() == QDbState::Error)
   {
      addError(connection.state().message());
      return;
   }

   QDbCommand query(connection);

#if 1
   query.setStatement(QString("create or replace procedure test_cr(pi IN varchar2, po OUT varchar2) as tmp varchar2(8000); begin tmp := pi || pi; po := tmp; END;"));
   if (!query.exec())
      addError(query.state().message());
   else
   {
      // Procedure call
      query.setProcedure(QString("test_cr"));

      query.setPlaceholderValue(1, QString(4000, QLatin1Char('x')));
      if (!query.exec())
         addError(query.state().message());
      else
         T_EQUALS(query.placeholderValue(2).toString(), QString(8000, QLatin1Char('x')));

      query.setPlaceholderValue(1, QString(""));
      query.setPlaceholderValue(2, Variant::null);
      if (!query.exec())
         addError(query.state().message());
      else
         T_EQUALS(query.placeholderValue(2).toString(), QString(""));

      // Drop procedure
      query.setStatement(QString("drop procedure test_cr"));
      T_ERROR_IF(!query.exec(), query.state().message());
   }

   query.setStatement(QString("create or replace procedure test_cr(pio IN OUT clob) as tmp clob; begin tmp := pio || pio; pio := tmp; END;"));
   if (!query.exec())
      addError(query.state().message());
   else
   {
      // Procedure call
      query.setProcedure(QString("test_cr"));
      query.setPlaceholderValue(1, QString(5000, QLatin1Char('x')));
      if (!query.exec())
         addError(query.state().message());
      else
         T_EQUALS(query.placeholderValue(1).toString(), QString(10000, QLatin1Char('x')));

      // Drop procedure
      query.setStatement(QString("drop procedure test_cr"));
      T_ERROR_IF(!query.exec(), query.state().message());
   }

   query.setStatement(QString("create or replace procedure test_cr(pio IN OUT nclob) as tmp nclob; begin tmp := pio || pio; pio := tmp; END;"));
   if (!query.exec())
      addError(query.state().message());
   else
   {
      // Procedure call
      query.setProcedure(QString("test_cr"));
      query.setPlaceholderValue(1, QString(5000, QLatin1Char('x')));
      if (!query.exec())
         addError(query.state().message());
      else
         T_EQUALS(query.placeholderValue(1).toString(), QString(10000, QLatin1Char('x')));

      // Drop procedure
      query.setStatement(QString("drop procedure test_cr"));
      T_ERROR_IF(!query.exec(), query.state().message());
   }

   query.setStatement(QString("create or replace procedure test_cr(pi IN blob, po OUT blob) as tmp blob; begin tmp := pi; po := tmp; END;"));
   if (!query.exec())
      addError(query.state().message());
   else
   {
      // Procedure call
      query.setProcedure(QString("test_cr"));
      query.setPlaceholderValue(1, QByteArray(5000, 'x'));
      if (!query.exec())
         addError(query.state().message());
      else
         T_IS_TRUE(query.placeholderValue(2) == QByteArray(5000, 'x'));

      // Drop procedure
      query.setStatement(QString("drop procedure test_cr"));
      T_ERROR_IF(!query.exec(), query.state().message());
   }

   query.setStatement(QString("create or replace procedure test_cr(pi IN clob, po OUT clob) as tmp clob; begin tmp := pi || pi; po := tmp; END;"));
   if (!query.exec())
      addError(query.state().message());
   else
   {
      // Procedure call
      query.setProcedure(QString("test_cr"));

      query.setPlaceholderValue(1, QString(4000, QLatin1Char('x')));
      if (!query.exec())
         addError(query.state().message());
      else
         T_EQUALS(query.placeholderValue(2).toString(), QString(8000, QLatin1Char('x')));

      query.setPlaceholderValue(1, QString(""));
      query.setPlaceholderValue(2, Variant::null);
      if (!query.exec())
         addError(query.state().message());
      else
         T_EQUALS(query.placeholderValue(2).toString(), QString(""));

      // Drop procedure
      query.setStatement(QString("drop procedure test_cr"));
      T_ERROR_IF(!query.exec(), query.state().message());
   }

   query.setStatement(QString("create or replace procedure test_cr(pi IN number, po OUT number) as tmp number; begin tmp := pi * 2; po := tmp; END;"));
   if (!query.exec())
      addError(query.state().message());
   else
   {
      // Procedure call
      query.setProcedure(QString("test_cr"));

      query.setPlaceholderValue(1, QDecimal(42.5));
      if (!query.exec())
         addError(query.state().message());
      else
         T_EQUALS_EX(query.placeholderValue(2).toDecimal(), QDecimal(85));

      query.setPlaceholderValue(1, QDecimal(0));
      query.setPlaceholderValue(2, Variant::null);
      if (!query.exec())
         addError(query.state().message());
      else
         T_EQUALS_EX(query.placeholderValue(2).toDecimal(), QDecimal(0));

      // Drop procedure
      query.setStatement(QString("drop procedure test_cr"));
      T_ERROR_IF(!query.exec(), query.state().message());
   }

#endif

#if 1 // CREATE_TEST_TABLE
   query.setStatement(QString(
      "create table cr_oci_test ( test1 number(8), test2 number(10), test3 number(30, 10), test4 varchar2(4000 byte), test5 nvarchar2(2000), test6 clob, test7 nclob, test8 date, test9 blob )"));
   T_ERROR_IF(!query.exec(), query.state().message());
#endif

#if 1
   query.setStatement(QString("insert into cr_oci_test (test1, test2, test3, test4, test5, test6, test7, test8, test9) values (:ph1, :ph2, :ph3, :ph4, :ph5, EMPTY_CLOB(), EMPTY_CLOB(), :ph8, EMPTY_BLOB()) RETURNING test4, test6, test7, test9 INTO :ph10, :ph6, :ph7, :ph9"));

   query.setPlaceholderValue(QString("ph1"), 123456);
   query.setPlaceholderValue(QString("ph2"), QDecimal(9999999999L));
   query.setPlaceholderValue(QString("ph3"), QDecimal(100.99));
   query.setPlaceholderValue(QString("ph4"), QString(3000, QLatin1Char('z')));
   query.setPlaceholderValue(QString("ph5"), QString("ř").repeated(1500), DT_NSTRING);
   query.setPlaceholderType(QString("ph6"), DT_LARGE_STRING);
   query.setPlaceholderType(QString("ph7"), DT_LARGE_NSTRING);
   query.setPlaceholderValue(QString("ph8"), QDateTime(QDate(2015, 1, 11), QTime(17, 30)));
   query.setPlaceholderType(QString("ph9"), DT_LARGE_BINARY);
   query.setPlaceholderType(QString("ph10"), DT_STRING);

   connection.beginTransaction();

   T_ERROR_IF(!query.exec(), query.state().message());

   query.setPlaceholderValue(QString("ph6"), QString(5000, QLatin1Char('u')));
   query.setPlaceholderValue(QString("ph7"), QString(5000, QLatin1Char('n')));
   query.setPlaceholderValue(QString("ph9"), QByteArray(5000, 'b'));

   connection.commitTransaction();

   T_ERROR_IF(query.state().type() == QDbState::Error, query.state().message());

   T_EQUALS(query.placeholderValue(QString("ph10")).toString(), QString(3000, QLatin1Char('z')));

   // Repeat insert

   //query.setStatement(QString("insert into cr_oci_test (test1, test2, test3, test4, test5, test6, test7, test8, test9) values (:ph1, :ph2, :ph3, :ph4, :ph5, EMPTY_CLOB(), EMPTY_CLOB(), :ph8, EMPTY_BLOB()) RETURNING test4, test6, test7, test9 INTO :ph10, :ph6, :ph7, :ph9"));

   query.setPlaceholderValue(QString("ph1"), 987654);
   query.setPlaceholderValue(QString("ph2"), QDecimal(1));
   query.setPlaceholderValue(QString("ph3"), QDecimal(12.5));
   query.setPlaceholderValue(QString("ph4"), QString(4000, QLatin1Char('a')));
   query.setPlaceholderValue(QString("ph5"), QString("ß").repeated(2000), DT_NSTRING);
   query.setPlaceholderType(QString("ph6"), DT_LARGE_STRING);
   query.setPlaceholderType(QString("ph7"), DT_LARGE_NSTRING);
   query.setPlaceholderValue(QString("ph8"), QDateTime(QDate(2018, 7, 12), QTime(5, 15)));
   query.setPlaceholderType(QString("ph9"), DT_LARGE_BINARY);
   query.setPlaceholderType(QString("ph10"), DT_STRING);

   connection.beginTransaction();

   T_ERROR_IF(!query.exec(), query.state().message());

   query.setPlaceholderValue(QString("ph6"), QString(100000, QLatin1Char('f')));
   query.setPlaceholderValue(QString("ph7"), QString(100000, QLatin1Char('g')));
   query.setPlaceholderValue(QString("ph9"), QByteArray(100000, 'a'));

   connection.commitTransaction();

   T_ERROR_IF(query.state().type() == QDbState::Error, query.state().message());

   T_EQUALS(query.placeholderValue(QString("ph10")).toString(), QString(4000, QLatin1Char('a')));

   // Insert with literals

   query.setStatement(QString("insert into cr_oci_test (test1, test2, test3, test4, test5, test6, test7, test8, test9) values (%1, %2, %3, %4, %5, %6, %7, %8, %9)")
      .arg(connection.toLiteral(DT_INT, 42))
      .arg(connection.toLiteral(DT_DECIMAL, QDecimal(9999999999L)))
      .arg(connection.toLiteral(DT_DECIMAL, QDecimal(100.99)))
      .arg(connection.toLiteral(DT_STRING, "\\Path\\Name\nLine\\\nNew \\ä\\nö\\rü\\tß line\\"))
      .arg(connection.toLiteral(DT_STRING, "\\Path\\Name\r\nLine\\\r\nNew \\ä\\nö\\rü\\tß line\\"))
      .arg(connection.toLiteral(DT_STRING, "\\Path\\Name\nLine\\\nNew \\ä\\nö\\rü\\tß line\\"))
      .arg(connection.toLiteral(DT_STRING, "\\Path\\Name\r\nLine\\\r\nNew \\ä\\nö\\rü\\tß line\\"))
      .arg(connection.toLiteral(DT_DATETIME, QDateTime(QDate(2015, 1, 11), QTime(17, 30))))
      .arg(connection.toLiteral(DT_BINARY, QByteArray("Binärdaten")))
   );
   T_ERROR_IF(!query.exec(), query.state().message());

#else
   query.setStatement(QString("insert into cr_oci_test (test1, test2, test3, test4, test5, test6, test7, test8, test9) values (:ph1, :ph2, :ph3, :ph4, :ph5, :ph6, :ph7, :ph8, :ph9)"));

   query.setPlaceholderValue(QString("ph1"), 123456);
   query.setPlaceholderValue(QString("ph2"), QDecimal(9999999999L));
   query.setPlaceholderValue(QString("ph3"), QDecimal(100.99));
   query.setPlaceholderValue(QString("ph4"), QString(4000, QLatin1Char('z')));
   query.setPlaceholderValue(QString("ph5"), eutf8("ř").repeated(2000), DT_NSTRING);
   query.setPlaceholderValue(QString("ph6"), QString(100000, QLatin1Char('u')));
   query.setPlaceholderValue(QString("ph7"), QString(100000, QLatin1Char('n')));
   query.setPlaceholderValue(QString("ph8"), QDateTime(QDate(2015, 01, 11), QTime(17, 30)));
   query.setPlaceholderValue(QString("ph9"), QByteArray(100000, 'b'));

   T_ERROR_IF(!query.exec(), query.state().message());
#endif

#if 1
   query.setStatement("select test1, test2, test3, test4, test5, test6, test7, test8, test9 from cr_oci_test order by test1");

   T_ERROR_IF(!query.exec(), query.state().message());

   auto resultSet = query.result();

   T_IS_TRUE(resultSet.next());

   T_EQUALS_EX(resultSet.valueAt(QString("test1")), QDecimal(42));
   T_EQUALS_EX(resultSet.valueAt(QString("test2")), QDecimal(9999999999L));
   T_EQUALS_EX(resultSet.valueAt(QString("test3")), QDecimal(100.99));
   T_EQUALS(resultSet.valueAt(QString("test4")).toString().replace("\r", "\\r").replace("\n", "\\n"), "\\Path\\Name\\nLine\\\\nNew \\ä\\nö\\rü\\tß line\\");
   T_EQUALS(resultSet.valueAt(QString("test5")).toString().replace("\r", "\\r").replace("\n", "\\n"), "\\Path\\Name\\r\\nLine\\\\r\\nNew \\ä\\nö\\rü\\tß line\\");
   T_EQUALS(resultSet.valueAt(QString("test6")).toString().replace("\r", "\\r").replace("\n", "\\n"), "\\Path\\Name\\nLine\\\\nNew \\ä\\nö\\rü\\tß line\\");
   T_EQUALS(resultSet.valueAt(QString("test7")).toString().replace("\r", "\\r").replace("\n", "\\n"), "\\Path\\Name\\r\\nLine\\\\r\\nNew \\ä\\nö\\rü\\tß line\\");
   T_EQUALS_EX(resultSet.valueAt(QString("test8")), QDateTimeEx(2015, 1, 11, 17, 30, 0));
   T_IS_TRUE(resultSet.valueAt(QString("test9")) == QByteArray("Binärdaten"));

   T_ERROR_IF(query.state().type() == QDbState::Error, query.state().message());

   T_IS_TRUE(resultSet.next());

   T_EQUALS_EX(resultSet.valueAt(QString("test1")) ,  QDecimal(123456));
   T_EQUALS_EX(resultSet.valueAt(QString("test2")) ,  QDecimal(9999999999L));
   T_EQUALS_EX(resultSet.valueAt(QString("test3")) ,  QDecimal(100.99));
   T_IS_TRUE(resultSet.valueAt(QString("test4")) == QString(3000, QLatin1Char('z')));
   T_IS_TRUE(resultSet.valueAt(QString("test5")) == QString("ř").repeated(1500));
   T_IS_TRUE(resultSet.valueAt(QString("test6")) == QString(5000, QLatin1Char('u')));
   T_IS_TRUE(resultSet.valueAt(QString("test7")) == QString(5000, QLatin1Char('n')));
   T_EQUALS_EX(resultSet.valueAt(QString("test8")) ,  QDateTimeEx(2015, 1, 11, 17, 30, 0));
   T_IS_TRUE(resultSet.valueAt(QString("test9")) == QByteArray(5000, 'b'));

   T_ERROR_IF(query.state().type() == QDbState::Error, query.state().message());

   T_IS_TRUE(resultSet.next());

   T_EQUALS_EX(resultSet.valueAt(QString("test1")), QDecimal(987654));
   T_EQUALS_EX(resultSet.valueAt(QString("test2")), QDecimal(1));
   T_EQUALS_EX(resultSet.valueAt(QString("test3")), QDecimal(12.5));
   T_IS_TRUE(resultSet.valueAt(QString("test4")) == QString(4000, QLatin1Char('a')));
   T_IS_TRUE(resultSet.valueAt(QString("test5")) == QString("ß").repeated(2000));
   T_IS_TRUE(resultSet.valueAt(QString("test6")) == QString(100000, QLatin1Char('f')));
   T_IS_TRUE(resultSet.valueAt(QString("test7")) == QString(100000, QLatin1Char('g')));
   T_EQUALS_EX(resultSet.valueAt(QString("test8")), QDateTimeEx(2018, 7, 12, 5, 15, 0));
   T_IS_TRUE(resultSet.valueAt(QString("test9")) == QByteArray(100000, 'a'));

   T_ERROR_IF(query.state().type() == QDbState::Error, query.state().message());

   T_IS_TRUE(!resultSet.next());
#endif

#if 1
   QList<ThreadingTest*> testThreads;

   for (auto r = 0; r < 2; ++r)
   {
      for (auto i = 0; i < 5; i++)
      {
         auto testThread = new ThreadingTest;

         testThread->dbName = _dbName;
         testThread->dbAdmin = _dbAdmin;
         testThread->dbAdminPassword = _dbAdminPassword;
         testThreads.append(testThread);
         testThread->start();
      }

      for (auto&& testThread : testThreads)
      {
         testThread->wait();
         for (auto&& errorMsg : testThread->errorMessages)
            addError(errorMsg);
         delete testThread;
      }

      testThreads.clear();
   }
#endif

#if 1
   query.setStatement(QString("update cr_oci_test set test7 = :ph02, test6 = :ph01, test4 = :ph03, test5 = :ph04 where test1 = 123456"));
   query.setPlaceholderValue(QString("ph01"), eutf8("ä").repeated(3000)); // CLOB
   query.setPlaceholderValue(QString("ph02"), eutf8("Ü").repeated(3000), DT_NSTRING); // NCLOB
   query.setPlaceholderValue(QString("ph03"), QTime(12, 30, 0)); // VARCHAR2
   query.setPlaceholderValue(QString("ph04"), eutf8("ý").repeated(2000), DT_NSTRING); // NVARCHAR2

   T_ERROR_IF(!query.exec(), query.state().message());
   T_EQUALS(query.affectedRows(), 1);

   query.setStatement(QString("SELECT test4, test5, test6, test7 FROM cr_oci_test where test1 = 123456"));

   T_ERROR_IF(!query.exec(), query.state().message());

   QDbResultSet result = query.result();

   T_IS_TRUE(result.next());

   auto text = result[QString("test4")].toString();
   T_IS_TRUE(text == "12:30:00");
   T_ERROR_IF(result.state().type() == QDbState::Error, result.state().message());
   text = result[QString("test5")].toString();
   T_IS_TRUE(text == eutf8("ý").repeated(2000));
   T_ERROR_IF(result.state().type() == QDbState::Error, result.state().message());
   text = result[QString("test6")].toString();
   T_IS_TRUE(text == eutf8("ä").repeated(3000));
   T_ERROR_IF(result.state().type() == QDbState::Error, result.state().message());
   text = result[QString("test7")].toString();
   T_IS_TRUE(text == eutf8("Ü").repeated(3000));
   T_ERROR_IF(result.state().type() == QDbState::Error, result.state().message());
#endif

#if 1
   query.setStatement(QString("insert into cr_oci_test (test1, test2, test3, test4, test5, test6, test7, test8, test9) values (:ph1, :ph2, :ph3, :ph4, :ph5, NULL, NULL, :ph8, NULL)"));

   query.setPlaceholderValue(QString("ph1"), 789);
   query.setPlaceholderValue(QString("ph2"), QDecimal(9999999999L));
   query.setPlaceholderValue(QString("ph3"), QDecimal(100.99));
   query.setPlaceholderValue(QString("ph4"), QTimeEx(6, 12, 0));
   query.setPlaceholderValue(QString("ph5"), eutf8("í").repeated(2000), DT_NSTRING);
   query.setPlaceholderValue(QString("ph8"), QDateTime(QDate(2015, 01, 11), QTime(17, 30)));

   T_ERROR_IF(!query.exec(), query.state().message());

   query.setStatement(QString("select test1, test2, test3, test4, test5, test6, test7, test8, test9 from cr_oci_test where test1 = 789"));

   T_ERROR_IF(!query.exec(), query.state().message());

   resultSet = query.result();

   T_IS_TRUE(resultSet.next());

   T_EQUALS_EX(resultSet.valueAt(QString("test1")) ,  QDecimal(789));
   T_EQUALS_EX(resultSet.valueAt(QString("test2")) ,  QDecimal(9999999999L));
   T_EQUALS_EX(resultSet.valueAt(QString("test3")) ,  QDecimal(100.99));
   T_IS_TRUE(resultSet.valueAt(QString("test4")) == "06:12:00");
   T_IS_TRUE(resultSet.valueAt(QString("test5")) == eutf8("í").repeated(2000));
   T_IS_TRUE(resultSet.valueAt(QString("test6")).isNull());
   T_IS_TRUE(resultSet.valueAt(QString("test7")).isNull());
   T_EQUALS_EX(resultSet.valueAt(QString("test8")), QDateTimeEx(2015, 01, 11, 17, 30, 0));
   T_IS_TRUE(resultSet.valueAt(QString("test9")).isNull());

   T_ERROR_IF(query.state().type() == QDbState::Error, query.state().message());

   T_IS_TRUE(!resultSet.next());
#endif

#if 1
   query.setStatement(QString("update cr_oci_test set test4 = :ph01, test6 = empty_clob(), test7 = empty_clob(), test9 = empty_blob() returning test6, test7, test9 into :ph02, :ph03, :ph04"));

   query.setPlaceholderValue(QString("ph01"), (const QDateTimeEx&)QTimeEx(18, 20, 00)); // VARCHAR2
   query.setPlaceholderType(QString("ph02"), DT_LARGE_STRING);
   query.setPlaceholderType(QString("ph03"), DT_LARGE_NSTRING);
   query.setPlaceholderType(QString("ph04"), DT_LARGE_BINARY);

   connection.beginTransaction();

   T_ERROR_IF(!query.exec(), query.state().message());

   query.setPlaceholderValue(QString("ph02"), QString(100000, QLatin1Char('u')));
   query.setPlaceholderValue(QString("ph03"), QString(100000, QLatin1Char('n')));
   query.setPlaceholderValue(QString("ph04"), QByteArray(100000, 'b'));

   connection.commitTransaction();

   T_ERROR_IF(query.state().type() == QDbState::Error, query.state().message());
   T_EQUALS(query.affectedRows(), 4);

   query.setStatement(QString("SELECT test4, test5, test6, test7 FROM cr_oci_test"));

   T_ERROR_IF(!query.exec(), query.state().message());

   query.setStatement(QString("select test1, test2, test3, test4, test5, test6, test7, test8, test9 from cr_oci_test where test1 = 789"));

   T_ERROR_IF(!query.exec(), query.state().message());

   resultSet = query.result();

   T_IS_TRUE(resultSet.next());

   T_EQUALS_EX(resultSet.valueAt(QString("test1")) ,  QDecimal(789));
   T_EQUALS_EX(resultSet.valueAt(QString("test2")) ,  QDecimal(9999999999L));
   T_EQUALS_EX(resultSet.valueAt(QString("test3")) ,  QDecimal(100.99));
   T_IS_TRUE(resultSet.valueAt(QString("test4")) == "18:20:00");
   T_IS_TRUE(resultSet.valueAt(QString("test5")) == eutf8("í").repeated(2000));
   T_IS_TRUE(resultSet.valueAt(QString("test6")) == QString(100000, QLatin1Char('u')));
   T_IS_TRUE(resultSet.valueAt(QString("test7")) == QString(100000, QLatin1Char('n')));
   T_EQUALS_EX(resultSet.valueAt(QString("test8")) ,  QDateTimeEx(2015, 01, 11, 17, 30, 0));
   T_IS_TRUE(resultSet.valueAt(QString("test9")) == QByteArray(100000, 'b'));

   T_IS_TRUE(!resultSet.next());
#endif

#if 1
   query.setStatement("create table cr_oci_test2 ( test1 number(8), test2 char(1) )");
   T_ERROR_IF(!query.exec(), query.state().message());

   query.setStatement("create or replace trigger trg_test_cr_broken before delete on cr_oci_test for each row declare dummy number(8); begin select test1 into dummy from cr_oci_test2 where test1 = 715; end;");

   T_ERROR_IF(!query.exec(), query.state().message());

   query.setStatement("delete from cr_oci_test where test1 = 789");

   T_IS_TRUE(!query.exec());

   T_EQUALS(query.state().text(), QString("ORA-01403: Keine Daten gefunden\nORA-06512: in \"%1.TRG_TEST_CR_BROKEN\", Zeile 1\nORA-04088: Fehler bei der Ausführung von Trigger '%1.TRG_TEST_CR_BROKEN'\n").arg(_dbSchema));

   query.setStatement("drop trigger trg_test_cr_broken");

   T_ERROR_IF(!query.exec(), query.state().message());

   connection.setBooleanStrings("F", "T");

   query.setStatement("insert into cr_oci_test2 (test1, test2) values (:ph1, :ph2)");

   query.setPlaceholderValue("ph1", 2000);
   query.setPlaceholderValue("ph2", true);

   T_ERROR_IF(!query.exec(), query.state().message());

   query.setStatement("select test1, test2 from cr_oci_test2");

   T_ERROR_IF(!query.exec(), query.state().message());

   T_IS_TRUE(query.result().next());

   T_EQUALS_EX(query.result().valueAt(1), "T");

   query.setStatement("drop table cr_oci_test2 cascade constraints");
   T_ERROR_IF(!query.exec(), query.state().message());
#endif

#if 1 // DESTROY_TEST_TABLE
   query.setStatement("drop table cr_oci_test cascade constraints");
   T_ERROR_IF(!query.exec(), query.state().message());
#endif
}

void TestCases::triggerTest()
{
   QDbConnection connection(createConnection(_dbName, _dbAdmin, _dbAdminPassword));

   if (connection.state().type() == QDbState::Error)
   {
      addError(connection.state().message());
      return;
   }

   QDbCommand query(connection);

   query.setStatement("create table cr_oci_test_trigger ( id number(10) not null, host_name varchar2(512 byte) not null )");
   T_ERROR_IF(!query.exec(), query.state().message());

   query.setStatement(R"(CREATE OR REPLACE TRIGGER TRG_INS_cr_oci_test_trigger
BEFORE INSERT ON cr_oci_test_trigger
FOR EACH ROW
BEGIN
   SELECT SYS_CONTEXT('USERENV','HOST') INTO :NEW.host_name FROM dual;
END;)");
   T_ERROR_IF(!query.exec(), query.state().message());

   query.setStatement("insert into cr_oci_test_trigger (id) values (112)");
   T_ERROR_IF(!query.exec(), query.state().message());

   query.setStatement("select id, host_name from cr_oci_test_trigger");
   T_ERROR_IF(!query.exec(), query.state().message());
   T_IS_TRUE(query.result().next());
   T_EQUALS(query.result().valueAt(0).toInt(), 112);
   T_IS_TRUE(!query.result().valueAt(1).toString().isEmpty());

   query.setStatement("insert into cr_oci_test_trigger (id) values (112) returning host_name into :ph1");
   query.setPlaceholderType("ph1", DT_STRING);
   T_ERROR_IF(!query.exec(), query.state().message());
   T_IS_TRUE(!query.placeholderValue("ph1").toString().isEmpty());

   query.setStatement(QString("drop table cr_oci_test_trigger cascade constraints"));
   T_ERROR_IF(!query.exec(), query.state().message());
}

void TestCases::schemaTests()
{
   // Create procedure, package and synonyms
   {
      QDbConnection connection(createConnection(_dbName, _dbAdmin, _dbAdminPassword));

      T_ASSERT(connection.state().type() != QDbState::Error);

      QDbCommand query(connection);

      query.setStatement(QString("create or replace procedure test_cr(pi_In IN varchar2, pi_Out OUT varchar2) as tmp varchar2(8000); begin tmp := pi_In || pi_In; pi_Out := tmp; END;"));
      T_ASSERT(query.exec());

      // Create synonym
      query.setStatement(QString("CREATE OR REPLACE PUBLIC SYNONYM TEST_CR FOR %1.TEST_CR").arg(_dbSchema));

      T_ERROR_IF(!query.exec(), query.state().message());

      // Add grants
      query.setStatement(QString("grant EXECUTE on %1.test_cr to %2").arg(_dbSchema).arg(_dbUserRole));

      T_ERROR_IF(!query.exec(), query.state().message());

      // Create test package
      query.setStatement(QString(R"(CREATE OR REPLACE package %1.cr_package_test AS
    FUNCTION testFunction(pi_Number IN NUMBER, pi_Date IN DATE, pi_String IN VARCHAR2, pi_Char IN CHAR DEFAULT 'T') RETURN NUMBER;
    PROCEDURE testProcedure(pi_Number IN NUMBER, pi_Date IN DATE, po_Number OUT NUMBER, po_String OUT VARCHAR2);
END cr_package_test;)").arg(_dbSchema));

      T_ERROR_IF(!query.exec(), query.state().message());

      query.setStatement(QString(R"(CREATE OR REPLACE package body %1.cr_package_test AS
    FUNCTION testFunction(pi_Number IN NUMBER, pi_Date IN DATE, pi_String IN VARCHAR2, pi_Char IN CHAR) RETURN NUMBER AS
        vk          NUMBER(16,2);
    BEGIN
        vk := 0;
        RETURN vk;
        EXCEPTION
        WHEN OTHERS THEN
            write_errors_to_log(SQLCODE, SQLERRM, 'cr_package_test');
            RAISE;
    END testFunction;
    procedure testProcedure(pi_Number IN number, pi_Date IN DATE, po_Number OUT number, po_String OUT VARCHAR2) as
    vk NUMBER(16,2);
    begin
        vk := pi_Number;
        po_Number := vk;
        po_String := 'Test';
    end testProcedure;
END cr_package_test;)").arg(_dbSchema));

      T_ERROR_IF(!query.exec(), query.state().message());

      // Create synonym
      query.setStatement(QString("CREATE OR REPLACE PUBLIC SYNONYM cr_package_test FOR %1.cr_package_test").arg(_dbSchema));

      T_ERROR_IF(!query.exec(), query.state().message());

      // Add grants
      query.setStatement(QString("grant EXECUTE on %1.cr_package_test to %2").arg(_dbSchema).arg(_dbUserRole));

      T_ERROR_IF(!query.exec(), query.state().message());
   }

   // Schema test
   {
      // Procedure test

      QDbConnection connection(createConnection(_dbName, _dbUser, _dbUserPassword));

      connection.enableSchemaCaching();

      T_ASSERT(connection.state().type() != QDbState::Error);

      QDbCommand query(connection);

      auto functionSchema = connection.functionSchema("test_error");

      T_IS_TRUE(functionSchema.name.isEmpty());

      functionSchema = connection.functionSchema("test_cr");

      T_EQUALS(functionSchema.schema, _dbSchema);
      T_EQUALS(functionSchema.name, "TEST_CR");
      T_EQUALS(functionSchema.arguments.count(), 2);

      functionSchema = connection.functionSchema(QString("%1.test_cr").arg(_dbSchema));

      T_EQUALS(functionSchema.schema, _dbSchema);
      T_EQUALS(functionSchema.name, "TEST_CR");
      T_EQUALS(functionSchema.arguments.count(), 2);

      // Package test
      auto packageSchema = connection.packageSchema("no_package");

      T_IS_TRUE(packageSchema.name.isEmpty());

      packageSchema = connection.packageSchema("cr_package_test");

      T_EQUALS(packageSchema.schema, _dbSchema);
      T_EQUALS(packageSchema.name, "CR_PACKAGE_TEST");
      T_EQUALS(packageSchema.subprograms.count(), 2);

      packageSchema = connection.packageSchema(QString("%1.cr_package_test").arg(_dbSchema));

      T_EQUALS(packageSchema.schema, _dbSchema);
      T_EQUALS(packageSchema.name, "CR_PACKAGE_TEST");
      T_EQUALS(packageSchema.subprograms.count(), 2);

      T_IS_TRUE(!packageSchema.subprogram("testFunction").name.isEmpty());

      functionSchema = connection.functionSchema("cr_package_test.testFunction");

      T_EQUALS(functionSchema.schema, _dbSchema);
      T_EQUALS(functionSchema.name, "TESTFUNCTION");
      T_EQUALS(functionSchema.arguments.count(), 4);

      functionSchema = connection.functionSchema(QString("%1.cr_package_test.testFunction").arg(_dbSchema));

      T_EQUALS(functionSchema.schema, _dbSchema);
      T_EQUALS(functionSchema.name, "TESTFUNCTION");
      T_EQUALS(functionSchema.arguments.count(), 4);

      functionSchema = connection.functionSchema(QString("CR_PACKAGE_TEST.%1.TESTFUNCTION").arg(_dbSchema));

      T_IS_TRUE(functionSchema.name.isEmpty());

      functionSchema = connection.functionSchema(QString("%1.TESTFUNCTION").arg(_dbSchema));

      T_IS_TRUE(functionSchema.name.isEmpty());

      functionSchema = connection.functionSchema("TESTFUNCTION");

      T_IS_TRUE(functionSchema.name.isEmpty());
   }

   // Drop procedure
   {
      QDbConnection connection(createConnection(_dbName, _dbAdmin, _dbAdminPassword));

      T_ASSERT(connection.state().type() != QDbState::Error);

      QDbCommand query(connection);

      query.setStatement(QString("DROP PUBLIC SYNONYM TEST_CR"));
      T_ERROR_IF(!query.exec(), query.state().message());

      query.setStatement(QString("drop procedure test_cr"));
      T_ERROR_IF(!query.exec(), query.state().message());

      query.setStatement(QString("DROP PUBLIC SYNONYM cr_package_test"));
      T_ERROR_IF(!query.exec(), query.state().message());

      query.setStatement(QString("drop package cr_package_test"));
      T_ERROR_IF(!query.exec(), query.state().message());
   }
}

void TestCases::numericLimitTest()
{
   QDbConnection connection(createConnection(_dbName, _dbAdmin, _dbAdminPassword));

   if (!connection.isValid())
      return;

   QDbCommand command(connection);

   // Create test table

   command.setStatement("create table cr_test ( id int not null, numeric_value number(32, 16), primary key (id) )");
   if (!command.exec())
      addError(command.state().message());

   // Fill test table

   command.setStatement("INSERT INTO CR_TEST (ID, NUMERIC_VALUE) VALUES (:ph00, :ph01)");

   command.setPlaceholderValue("ph00", 1);
   command.setPlaceholderValue("ph01", QDecimal(42.5));
   T_ERROR_IF(!command.exec(), command.state().message());

   command.setPlaceholderValue("ph00", 2);
   command.setPlaceholderValue("ph01", QDecimal("123456789012.1234567890123456"));
   T_ERROR_IF(!command.exec(), command.state().message());

   command.setPlaceholderValue("ph00", 3);
   command.setPlaceholderValue("ph01", QDecimal("1234567890123456.1234567890123456"));
   T_ERROR_IF(!command.exec(), command.state().message());

   // Read test table

   command.setStatement("SELECT numeric_value FROM cr_test ORDER BY id");
   T_ERROR_IF(!command.exec(), command.state().message());
   auto result = command.result();

   T_IS_TRUE(result.next());
   T_EQUALS_EX(result["numeric_value"].toDecimal(), 42.5);
   T_ERROR_IF(result.state().type() == QDbState::Error, result.state().message());

   T_IS_TRUE(result.next());
   T_EQUALS_EX(result["numeric_value"].toDecimal(), QDecimal("123456789012.1234567890123456"));
   T_ERROR_IF(result.state().type() == QDbState::Error, result.state().message());

   T_IS_TRUE(result.next());
   T_EQUALS_EX(result["numeric_value"].toDecimal(), QDecimal("1234567890123456.1234567890123456"));
   T_ERROR_IF(result.state().type() == QDbState::Error, result.state().message());

   // Destroy test table
   command.setStatement("drop table cr_test");
   T_ERROR_IF(!command.exec(), command.state().message());
}

BEGIN_TEST_SUITES
   TEST_SUITE(TestCases);
END_TEST_SUITES
