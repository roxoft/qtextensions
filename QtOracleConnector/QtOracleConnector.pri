# ----------------------------------------------------
# This file is generated by the Qt Visual Studio Add-in.
# ------------------------------------------------------

# This is a reminder that you are using a generated .pro file.
# Remove it when you are finished editing this file.
message("You are running qmake on a generated .pro file. This may not work!")


HEADERS += $$PWD/include/QOciConnection.h \
    $$PWD/include/qtoracleconnector_global.h \
    $$PWD/OciBuffers.h \
    $$PWD/OciDescribe.h \
    $$PWD/QOciEnvironment.h \
    $$PWD/QOciCommandEngine.h \
    $$PWD/TestCases.h
SOURCES += $$PWD/OciBuffers.cpp \
    $$PWD/OciDescribe.cpp \
    $$PWD/QOciConnection.cpp \
    $$PWD/QOciEnvironment.cpp \
    $$PWD/QOciCommandEngine.cpp \
    $$PWD/TestCases.cpp
