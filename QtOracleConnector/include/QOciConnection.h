#ifndef QOCICONNECTION_H
#define QOCICONNECTION_H

#include "qtoracleconnector_global.h"
#include <QDbConnectionData.h>
#include <QVector>

struct OCIError;
struct OCIAuthInfo;
struct OCISvcCtx;
struct OCIStmt;
struct OCIBind;
struct OCILobLocator;
struct OCINumber;
struct OCIParam;

struct OciOutputBuffer;

class PieceBuffer;
class BindValue;
class QByteArray;

class QTORACLECONNECTOR_EXPORT QOciConnection : public QDbConnectionData
{
   Q_DISABLE_COPY(QOciConnection)
   Q_DECLARE_TR_FUNCTIONS(QOciConnection)

public:
   QOciConnection(const QString &userName, const QString &password, const QString &connectionString);
    ~QOciConnection() override;

   bool isValid() const override;

   QString  connectionString() const override;
   QString  dataSourceName() const override;
   QString  driverName() const override;
   SqlStyle sqlStyle() const override;
   QDbConnection::PhType phType() const override;

   QDbEnvironment* environment() override;

   bool beginTransaction() override;
   bool rollbackTransaction() override;
   bool commitTransaction() override;

   bool abortExecution() override;

   bool tableExists(const QString &tableName) override;

   QChar phIndicator() const override;

   QString toBooleanLiteral(bool value) const override;
   QString toDateLiteral(const QDateEx &value) const override;
   QString toTimeLiteral(const QTimeEx &value) const override;
   QString toDateTimeLiteral(const QDateTimeEx &value) const override;
   QString toStringLiteral(QString value) const override;
   QString toBinaryLiteral(const QByteArray &value) const override;

   QDbSchema::Database  databaseSchema() override;
   QDbSchema::Synonym   synonymSchema(const QString &name) override;
   QDbSchema::Table     tableSchema(const QString &name) override;
   QDbSchema::Function  functionSchema(const QString &name) override;
   QDbSchema::Package   packageSchema(const QString &name) override;

   bool schemaCachingEnabled() const override;
   void enableSchemaCaching() override;
   void disableSchemaCaching() override;

public:
   QDbState    toVariant(unsigned char *buffer, int size, int vendorType, int scale, Variant &value) const;
   //QDbState    toOciNumber(OCINumber *ociNumber, const QDecimal &number);
   //QDbState ociString(OCIString **ociString, const QString &text);

   QDbState    prepareStatement(OCIStmt **stmthp, const wchar_t *statement); // The statement pointer must stay valid as long as stmthp is valid!
   QDbState    statementType(OCIStmt *stmthp, int &type);
   QDbState    bindParameter(OCIStmt *stmthp, int pos, BindValue *bindValue, int dmlCommand);
   QDbState    executeStatement(OCIStmt *stmthp, int iters);
   QDbState    rowCount(OCIStmt *stmthp, int &count) const;
   QDbState    defineOutputBuffers(OCIStmt *stmthp, QVector<OciOutputBuffer> &outputBuffers, int arraySize);
   QDbState    fetch(OCIStmt *stmthp, int& rows, bool& hasMore);
   bool        releaseOutputBuffers(QVector<OciOutputBuffer> &outputBuffers, int arraySize); // Retruns true on success
   QDbState    releaseStatement(OCIStmt *stmthp);

   QDbState    createLobLocator(OCILobLocator** lobLocator);
   bool        lobIsInitialized(OCILobLocator* lobLocator);
   bool        lobIsTemporary(OCILobLocator* lobLocator);
   bool        releaseLobLocator(OCILobLocator* lobLocator);
   QDbState    readCLob(OCILobLocator *lobLocator, QString &text) const;
   QDbState    writeCLob(OCILobLocator *lobLocator, const QString &text) const;
   QDbState    readBLob(OCILobLocator *lobLocator, QByteArray &blob) const;
   QDbState    writeBLob(OCILobLocator *lobLocator, const QByteArray &blob) const;

   QDbState    createTemporaryCLob(OCILobLocator* lobLocator, bool altCsForm);
   QDbState    createTemporaryBLob(OCILobLocator* lobLocator);

protected:
   QDbCommandEngine *newEngine() override;

private:
   void setServerCharacterEncodingFromDb(bool national);

   QDbState readLob(OCILobLocator *lobLocator, unsigned char csfrm, PieceBuffer& pieceBuffer) const;
   QDbState writeLob(OCILobLocator *lobLocator, unsigned char csfrm, const char* buffer, unsigned long long length) const;

private:
   OCIError* _errhp = nullptr;
   OCISvcCtx* _svchp = nullptr;
   QString _connectString;
   bool _transaction = false;

   // Caches
   bool _schemaCacheEnabled = false;
};

extern "C" {
   QTORACLECONNECTOR_EXPORT QDbConnectionData *newOracleConnection(const QString &userName, const QString &password, const QString &connectionString);
}

#endif // QOCICONNECTION_H
