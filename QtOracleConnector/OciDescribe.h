#ifndef OCIDESCRIBE_H
#define OCIDESCRIBE_H

#include <oci.h>
#include <QString>
#include <QDbSchema.h>
#include <QDbState.h>

class OciDescribe
{
public:
   OciDescribe(OCIError *errhp);
   ~OciDescribe();

   // Fetch the object description
   bool describe(OCISvcCtx *svchp, const QString& name, int type = OCI_PTYPE_UNK);

   int getType() const;

   void getCommonAttributes(QDbSchema::DbObject& dbObject) const;
   void getDatabaseAttributes(QDbSchema::Database& dbSchema) const;
   void getSynonymAttributes(QDbSchema::Synonym& synonymSchema) const;
   void getTableOrViewAttributes(QDbSchema::Table& tableSchema) const;
   void getProcedureOrFunctionAttributes(QDbSchema::Function& functionSchema) const;
   void getPackageAttributes(QDbSchema::Package& packageSchema) const;

   static void functionArguments(OCIError *errhp, OCIParam *parmh, QDbSchema::Function &funcInfo, bool isProcedure); // Throws an exception on error
   static QDbState columnAttributes(OCIError *errhp, OCIParam *parmh, QDbSchema::Column &colInfo, bool isExplicit);

private:
   OCIError    *_errhp;
   OCIDescribe *_dschp;
   OCIParam    *_parmh;
};

#endif // OCIDESCRIBE_H
