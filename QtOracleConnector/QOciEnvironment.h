#ifndef QOCIENVIRONMENT_H
#define QOCIENVIRONMENT_H

#include <QDbEnvironment>
#include <oci.h>
#include <QMutex>
#include <QThread>

#define checkenv(envhp, status) checkerr0((envhp), OCI_HTYPE_ENV, (status), false)
#define envexcpt(envhp, status) checkerr0((envhp), OCI_HTYPE_ENV, (status), true)

#define checkerr(errhp, status) checkerr0((errhp), OCI_HTYPE_ERROR, (status), false)
#define errexcpt(errhp, status) checkerr0((errhp), OCI_HTYPE_ERROR, (status), true)

QDbState checkerr0(void *handle, ub4 htype, sword status, bool throwOnError);

class SessionWatcher : public QThread
{
   Q_OBJECT

public:
   SessionWatcher();
   ~SessionWatcher() override;

   void run() override;

private slots:
   void onTimeout();
};

class QOciEnvironment : public QDbEnvironment
{
public:
   static QOciEnvironment  *theEnvironment;
   static int              environmentRefCount;

   static void attach();
   static void detach();

public:
   QOciEnvironment();
   ~QOciEnvironment() override;

   OCIEnv* envhp() { return _envhp; }

   QDbState getSession(OCISvcCtx** svchp, const QString& userName, const QString& password, const QString& connectionString);
   QDbState releaseSession(OCISvcCtx* svchp);
   void checkSessions();

private:
   class SessionInfo
   {
   public:
      QString connectionString;
      QString userName;
      QString password;
      bool busy = false;
      int timeout = 0;
      OCISvcCtx* svchp = nullptr;
   };

   class PoolInfo
   {
   public:
      QString connectionString;
      OCISPool* hp = nullptr;
      QString name;
   };

private: // Protected by _mutex
   int findSession(const QString& userName, const QString& password, const QString& connectionString);
   int createSession(const QString& userName, const QString& password, const QString& connectionString);

#if 0
   PoolInfo findPool(const QString& userName, const QString& password, const QString& connectionString);
   PoolInfo addPool(const QString& userName, const QString& password, const QString& connectionString);
#endif

private:
   OCIEnv*           _envhp = nullptr;
   OCIError*         _errhp = nullptr;
   QList<SessionInfo>_sessions;
   int               _minSessions = 2;
   QList<PoolInfo>   _pools;
   mutable QMutex    _mutex;
};

#endif // QOCIENVIRONMENT_H
