#ifndef OCIOUTPUTBUFFER_H
#define OCIOUTPUTBUFFER_H

#include <oci.h>
#include "QOciCommandEngine.h"
#include <QDbConnectionData.h>

class QOciConnection;

//
// struct OciOutputBuffer
//
struct OciOutputBuffer
{
   OciOutputBuffer() {}

   QDbSchema::Column schema;
   OCIDefine         *defhp = nullptr;
   void              *buffer = nullptr;
   int               size = 0u;
   sb2               *indicator = nullptr;
   ub2               *actualSize = nullptr;
};

enum Indicator { I_Null = -1, I_Normal = 0, I_Default = 1 };

//
// class PieceBuffer
//
class PieceBuffer
{
public:
   PieceBuffer(sb2 ind) : indicator(ind), retCode(0), count(0), index(0) {}
   ~PieceBuffer();

   bool  isEmpty() const { return length() == 0; }
   bool  hasValue() const { return indicator == I_Normal && !isEmpty(); }
   int   pieceCount() const { return count; }
   ub4   length() const;
   ub4   pieceLength() const;
   ub4   *pieceLengthPtr() const;
   void  *piece() const; // Returns the current piece for reading

   // Reserves at least size bytes for the current piece. If the buffer has to be allocated the greater value of size or defaultSize is used.
   // The length of the piece is at minimum size bytes and at maximum the capacity of the piece or the defaultSize whichever is smaller.
   // The used number of pieces (count) is set to index + 1.
   void setPieceLength(size_t size, size_t defaultSize = 0u);

   //void  *preLengthBuffer() const;
   //ub4   preLengthBufSize() const;

   void write(const ub1 *content, size_t size, size_t defaultPieceSize = 4000u);

   //bool atEnd() const { return index == count; }
   bool isLastPiece() const { return index + 1 >= count; }
   bool isFirstPiece() const { return index == 0; }
   //int  currentPieceIndex() const { return index; }
   void nextPiece() { if (index < count) ++index; }
   void resetPieceIndex() { index = 0; }

   void mergePieces();

   void clear() { indicator = I_Null; retCode = 0; index = 0; count = 0; }

   sb2 indicator;
   ub2 retCode;

private:
   QList<ub4 *>   pieces; // The first 4 bytes of each piece contains the capacity of the piece, the second 4 bytes the content size
   int            count; // Number of pieces (may be less than the number of pieces in the list
   int            index; // Current piece;
};

//
// interface BindValue
//
class BindValue
{
public:
   virtual ~BindValue() = default;

   virtual QDbDataType dbType() const = 0;
   virtual int vendorType() const = 0;
   virtual bool ociCanReceive() const = 0;
   virtual void readPiece(int i, void **buffer, ub4 *lenp, sb2** indp, ub1 *status) = 0;
   virtual void writePiece(int i, void **buffer, ub4 **lenp, sb2** indp, ub2** retcodep, ub1 *status) = 0;
   virtual PieceBuffer *pieceBuffer(int i = 0) const = 0;
   virtual size_t serverBufferSize() const = 0;
   virtual OCIBind** pBindhp() = 0;
};

//
// class OciPhInfo
//
class QOciCommandEngine::OciPhInfo : public PhInfo, public BindValue
{
   Q_DISABLE_COPY(OciPhInfo)
   Q_DECLARE_TR_FUNCTIONS(OciPhInfo)

public:
   OciPhInfo(const QDbSchema::Column& schema, QDbConnection::PhType phType, QOciConnection *c);
   ~OciPhInfo() override { clearValue(); }

   bool isValid() const override { return _schema.type != DT_UNKNOWN && (isNull() || pieceBuffer() && pieceBuffer()->piece()); }

   Variant value() const override { Variant v; value(v); return v; }
   QDbState setValue(const Variant& value) override { return setValue(value, -1); }

   QDbDataType dbType() const override { return _schema.type; }
   void setType(QDbDataType t) override;

   int vendorType() const override { return _schema.vendorType; }

   bool ociCanReceive() const override { return _schema.readable; }

   QDbState setValue(const Variant &value, int i); // Sets the value for all defined buffers
   void clearValue(int i = -1);
   bool hasValue(int i = 0) const { return i < buffers.count() && buffers.at(i)->hasValue(); }
   QDbState value(Variant& v) const;

   bool isNull(int i = 0) const { return i >= buffers.count() || buffers.at(i)->indicator == I_Null; }
   void setIsNull(bool isNull = true, int i = 0) { if (i < buffers.count()) buffers.at(i)->indicator = (isNull ? I_Null : I_Normal); }

   bool isDefault() const { return _isDefault; }
   void setIsDefault(bool isDefault = true) { _isDefault = isDefault; }

   bool isNormal(int i = 0) const { return i >= buffers.count() || buffers.at(i)->indicator == I_Normal; }

   void *readBuffer(int i = 0) const;
   ub4   bufSize(int i = 0) const { return (i < buffers.count()) ? buffers.at(i)->length() : 0; }

   PieceBuffer *pieceBuffer(int i = 0) const override { if (i < buffers.count()) return buffers.at(i); return nullptr; }

   //void nextPiece(int i) { if (i < buffers.count()) buffers.at(i)->nextPiece(); }

   void readPiece(int i, void **buffer, ub4 *lenp, sb2** indp, ub1 *status) override;
   void writePiece(int i, void **buffer, ub4 **lenp, sb2** indp, ub2** retcodep, ub1 *status) override;

   size_t serverBufferSize() const override;

   OCIBind** pBindhp() override { return &bindhp; }

private:
   void setVendorTypefromDbType();
   QDbState setValue(const Variant &value, PieceBuffer* pieceb);

private:
   QOciConnection* connection = nullptr;
   bool _isDefault = false;
   QList<PieceBuffer*> buffers;
   size_t _serverBufferSize = 0u;
   OCIBind* bindhp = nullptr;
};

#endif // OCIOUTPUTBUFFER_H
