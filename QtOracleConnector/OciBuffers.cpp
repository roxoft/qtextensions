#include "OciBuffers.h"
#include "QOciConnection.h"
#include <QTextCodec>

static QString toDbString(const Variant& value)
{
   if (value.type() == typeid(bool))
      return value.toBool() ? QLatin1String("1") : QLatin1String("0");
   if (value.type() == typeid(QTime))
      return value.toTime().toString(SQL92_TIME_FORMAT);
   return value.toString(QLocale::C);
}

PieceBuffer::~PieceBuffer()
{
   foreach(void *piece, pieces)
      free(piece);
   pieces.clear();

   count = 0;
   index = 0;
}

ub4 PieceBuffer::length() const
{
   ub4 l = 0;

   for (int i = 0; i < count; ++i)
      l += pieces.at(i)[1];

   return l;
}

ub4 PieceBuffer::pieceLength() const
{
   if (index < count)
      return pieces.at(index)[1];
   return 0;
}

ub4* PieceBuffer::pieceLengthPtr() const
{
   if (index < count)
      return pieces.at(index) + 1;
   return nullptr;
}

void *PieceBuffer::piece() const
{
   if (index < count)
      return pieces.at(index) + 2;
   return nullptr;
}

void PieceBuffer::setPieceLength(size_t size, size_t defaultSize)
{
   Q_ASSERT(index <= count);

   if (defaultSize < size)
      defaultSize = size;

   while (index < pieces.count() && *pieces.at(index) < size)
   {
      free(pieces.at(index));
      pieces.removeAt(index);
   }

   if (index == pieces.count())
   {
      pieces.append((ub4 *)malloc(defaultSize + 2 * sizeof(ub4)));
      pieces[index][0] = (ub4)defaultSize;
   }

   count = index + 1;
   pieces[index][1] = qMin(pieces[index][0], (ub4)defaultSize); // Set the current length to the buffer length or the default size if it is smaller
}

//void *PieceBuffer::preLengthBuffer() const
//{
//   if (count == 0)
//      return nullptr;
//   return pieces.first() + 1;
//}
//
//ub4 PieceBuffer::preLengthBufSize() const
//{
//   if (count == 0)
//      return 0;
//   return pieces.first()[1] + sizeof(ub4);
//}

void PieceBuffer::write(const ub1 *content, size_t size, size_t defaultPieceSize)
{
   index = 0;
   count = 0;

   auto pieceLen = defaultPieceSize;

   while (true)
   {
      if (size < pieceLen)
         pieceLen = size;

      setPieceLength(pieceLen);
      memcpy(piece(), content, pieceLen);

      content += pieceLen;
      size -= pieceLen;

      if (!size)
         break;

      nextPiece();
   }
}

void PieceBuffer::mergePieces()
{
   if (count > 1)
   {
      size_t   size = length();
      ub4      *buf = (ub4 *)malloc(size + 2 * sizeof(ub4));
      ub1      *part = (ub1 *)(buf + 2);

      buf[0] = (ub4)size;
      buf[1] = (ub4)size;

      for (int i = 0; i < count; ++i)
      {
         memcpy(part, pieces.at(i) + 2, pieces.at(i)[1]);
         part += pieces.at(i)[1];
      }

      foreach (void *piece, pieces)
         free(piece);
      pieces.clear();
      pieces.append(buf);
      count = 1;
      index = 0;
   }
}

static size_t minBufferSize(QDbDataType dbType)
{
   // Get the buffer size
   size_t bufSize = 0;

   switch (dbType)
   {
   case DT_INT:
      bufSize = sizeof(qint32);
      break;
   case DT_UINT:
      bufSize = sizeof(quint32);
      break;
   case DT_LONG:
   case DT_ULONG:
   case DT_DOUBLE:
   case DT_DECIMAL:
      bufSize = sizeof(OCINumber);
      break;
   case DT_BOOL:
      bufSize = sizeof(wchar_t);
      break;
   case DT_STRING:
   case DT_NSTRING:
      bufSize = 2u;
      break;
   case DT_LARGE_STRING:
   case DT_LARGE_NSTRING:
      bufSize = sizeof(OCILobLocator *);
      break;
   case DT_DATE:
   case DT_DATETIME:
      bufSize = sizeof(OCIDate);
      break;
   case DT_TIME:
      bufSize = 8u * sizeof(wchar_t);
      break;
   case DT_BINARY:
      bufSize = 1u;
      break;
   case DT_LARGE_BINARY:
      bufSize = sizeof(OCILobLocator *);
      break;
   }

   return bufSize;
}

QOciCommandEngine::OciPhInfo::OciPhInfo(const QDbSchema::Column& schema, QDbConnection::PhType phType, QOciConnection *c)
   : PhInfo(schema, phType), connection(c), _isDefault(schema.hasDefault)
{
   setVendorTypefromDbType();
}

void QOciCommandEngine::OciPhInfo::setType(QDbDataType t)
{
   if (_schema.type != DT_UNKNOWN)
      clearValue();

   PhInfo::setType(t);

   setVendorTypefromDbType();
}

QDbState QOciCommandEngine::OciPhInfo::setValue(const Variant &value, int i)
{
   _isDefault = false;

   if (i < 0)
   {
      // Set the value for all buffers

      if (buffers.isEmpty())
         buffers.append(new PieceBuffer(_isDefault ? I_Default : I_Null)); // Set the value at least for one buffer

      for (i = 0; i < buffers.count(); ++i)
      {
         auto dbState = setValue(value, buffers.at(i));

         if (dbState.type() == QDbState::Error)
            return dbState;
      }

      return QDbState();
   }

   return setValue(value, buffers.at(i));
}

void QOciCommandEngine::OciPhInfo::clearValue(int i)
{
   if (i == -1)
   {
      foreach (auto pieceb, buffers)
      {
         if (!pieceb->isEmpty() && (_schema.vendorType == SQLT_CLOB || _schema.vendorType == SQLT_BLOB) && connection)
            connection->releaseLobLocator(*(OCILobLocator **)pieceb->piece());
         delete pieceb;
      }
      buffers.clear();
   }
   else if (i < buffers.count() && !buffers.at(i)->isEmpty())
   {
      if (!buffers.at(i)->isEmpty() && (_schema.vendorType == SQLT_CLOB || _schema.vendorType == SQLT_BLOB) && connection)
         connection->releaseLobLocator(*(OCILobLocator **)buffers.at(i)->piece());
      buffers.at(i)->clear();
   }
}

QDbState QOciCommandEngine::OciPhInfo::value(Variant& v) const
{
   v.clear();
   if (!hasValue())
      return QDbState();
   if (connection == nullptr)
      return QDbState(QDbState::Error, tr("No connection"), QDbState::Connection);
   return connection->toVariant((unsigned char *)readBuffer(), (int)bufSize(), _schema.vendorType, _schema.scale, v);
}

void* QOciCommandEngine::OciPhInfo::readBuffer(int i) const
{
   if (i >= buffers.count())
      return nullptr;

   buffers.at(i)->mergePieces();

   return buffers.at(i)->piece();
}

void QOciCommandEngine::OciPhInfo::readPiece(int i, void** buffer, ub4* lenp, sb2** indp, ub1* status)
{
   // There must be a buffer for the indicator pointer
   while (i >= buffers.count())
      buffers.append(new PieceBuffer(_isDefault ? I_Default : I_Null));

   if (*status == OCI_FIRST_PIECE)
      buffers.at(i)->resetPieceIndex();
   else
      buffers.at(i)->nextPiece();

   *indp = &buffers.at(i)->indicator;

   if (buffers.at(i)->pieceLength() > 0 && (_schema.vendorType == SQLT_CLOB || _schema.vendorType == SQLT_BLOB))
      *buffer = *((OCILobLocator **)buffers.at(i)->piece());
   else
      *buffer = buffers.at(i)->piece();

   if (buffers.at(i)->isLastPiece())
      *status = buffers.at(i)->isFirstPiece() ? OCI_ONE_PIECE : OCI_LAST_PIECE;
   else
      *status = buffers.at(i)->isFirstPiece() ? OCI_FIRST_PIECE : OCI_NEXT_PIECE;

   *lenp = buffers.at(i)->pieceLength();
}

void QOciCommandEngine::OciPhInfo::writePiece(int i, void** buffer, ub4** lenp, sb2** indp, ub2** retcodep, ub1* status)
{
   while (i >= buffers.count())
      buffers.append(new PieceBuffer(_isDefault ? I_Default : I_Null));

   if (*status == OCI_ONE_PIECE)
      clearValue(i);
   else
      buffers.at(i)->nextPiece();

   *indp = &buffers.at(i)->indicator;
   *retcodep = &buffers.at(i)->retCode;

   // Get the estimated buffer size
   size_t minBufSize = minBufferSize(_schema.type);
   size_t maxBufSize = (_schema.type == DT_STRING || _schema.type == DT_NSTRING || _schema.type == DT_BINARY) ? 4000u : minBufSize;

   // Reserve buffer for the value
   if (minBufSize)
      buffers.at(i)->setPieceLength(minBufSize, maxBufSize);

   // Get the buffer (lob locators must be treated specially)
   if (buffers.at(i)->pieceLength() > 0 && (_schema.vendorType == SQLT_CLOB || _schema.vendorType == SQLT_BLOB))
   {
      Q_ASSERT(*status == OCI_ONE_PIECE);

      const auto lobLocatorPtr = (OCILobLocator **)buffers.at(i)->piece();

      if (!connection || connection->createLobLocator(lobLocatorPtr).type() == QDbState::Error)
         buffers.at(i)->clear();
      else
         *buffer = *lobLocatorPtr;
   }
   else
      *buffer = buffers.at(i)->piece();

   *status = buffers.at(i)->isFirstPiece() ? OCI_ONE_PIECE : OCI_LAST_PIECE;
   *lenp = buffers.at(i)->pieceLengthPtr();
}

// Calculates the needed buffer on the server side. Dynamic output binds for LONG types is not supported.
// In fact for dynamic output the code is to determine if the server buffer exceeds 4000 bytes or not!!!
size_t QOciCommandEngine::OciPhInfo::serverBufferSize() const
{
   auto bufSize = _serverBufferSize;

   if (bufSize == 0u)
   {
      bufSize = minBufferSize(_schema.type);

      switch (_schema.type)
      {
      case DT_BOOL:
         bufSize /= sizeof(wchar_t); // Assume that boolean strings are always ASCII
         break;
      case DT_TIME:
         bufSize /= sizeof(wchar_t); // Assume that time strings are always ASCII
         break;
      }
   }

   // Server buffer size for incoming data with variable length
   if (ociCanReceive() && (_schema.type == DT_STRING || _schema.type == DT_NSTRING || _schema.type == DT_BINARY) && bufSize < 4000u)
      bufSize = 4000u;

   Q_ASSERT(bufSize);

   return bufSize;
}

void QOciCommandEngine::OciPhInfo::setVendorTypefromDbType()
{
   // Set the correct vendor type
   switch (_schema.type)
   {
   case DT_INT:
      _schema.vendorType = SQLT_INT;
      break;
   case DT_UINT:
      _schema.vendorType = SQLT_UIN;
      break;
   case DT_LONG:
   case DT_ULONG:
   case DT_DOUBLE:
   case DT_DECIMAL:
      _schema.vendorType = SQLT_VNU;
      break;
   case DT_BOOL:
      _schema.vendorType = SQLT_AFC;
      break;
   case DT_STRING:
   case DT_NSTRING:
      _schema.vendorType = SQLT_CHR;
      break;
   case DT_LARGE_STRING:
   case DT_LARGE_NSTRING:
      _schema.vendorType = SQLT_CLOB;
      break;
   case DT_DATE:
   case DT_DATETIME:
      _schema.vendorType = SQLT_ODT;
      break;
   case DT_TIME:
      _schema.vendorType = SQLT_AFC;
      break;
   case DT_BINARY:
      _schema.vendorType = SQLT_BIN;
      break;
   case DT_LARGE_BINARY:
      _schema.vendorType = SQLT_BLOB;
      break;
   default:
      _schema.vendorType = 0;
      _schema.type = DT_UNKNOWN; // Error!
      break;
   }
}

QDbState QOciCommandEngine::OciPhInfo::setValue(const Variant& value, PieceBuffer* pieceb)
{
   QDbState dbState;

   pieceb->indicator = I_Null;
   _serverBufferSize = 0u; // Server buffer size for variable length values
   // The buffer size of the fixed length values must match minBufferSize()

   // Keep the buffer. It is needed for lobs (post written)!

   if (!value.isValueNull())
   {
      auto ok = false;

      switch (_schema.type)
      {
      case DT_INT:
         pieceb->setPieceLength(sizeof(qint32));
         *(qint32 *)pieceb->piece() = value.toInt(&ok);
         pieceb->indicator = I_Normal;
         break;
      case DT_UINT:
         pieceb->setPieceLength(sizeof(quint32));
         *(quint32 *)pieceb->piece() = value.toUInt(&ok);
         pieceb->indicator = I_Normal;
         break;
      case DT_LONG:
      case DT_ULONG:
      case DT_DOUBLE:
      case DT_DECIMAL:
         Q_ASSERT(!value.toDecimal().isNull()); // This is handled by Variant::isValueNull()
         pieceb->setPieceLength(sizeof(OCINumber));
         value.toDecimal(&ok).toOciNumber((OCINumber *)pieceb->piece());
         pieceb->indicator = I_Normal;
         break;
      case DT_BOOL:
         pieceb->setPieceLength(sizeof(wchar_t));
         *(wchar_t *)pieceb->piece() = value.toBool(&ok) ? L'1' : L'0';
         pieceb->indicator = I_Normal;
         break;
      case DT_STRING:
      case DT_NSTRING:
         {
            const QString stringValue = toDbString(value);

            Q_ASSERT(!stringValue.isNull()); // This is handled by Variant::isValueNull()

            ok = true; // Conversion to string is always lossless

            pieceb->write((const ub1*)stringValue.utf16(), stringValue.length() * sizeof(ushort));
            pieceb->indicator = I_Normal;

            if (_schema.type == DT_STRING)
            {
               if (connection->serverCharacterEncoding())
                  _serverBufferSize = connection->serverCharacterEncoding()->fromUnicode(stringValue).length();
               else
                  _serverBufferSize = stringValue.toUtf8().length();
            }
            else
            {
               if (connection->serverNationalCharacterEncoding())
               {
                  pieceb->mergePieces();
                  _serverBufferSize = connection->serverNationalCharacterEncoding()->fromUnicode(stringValue).length();
               }
               else
                  _serverBufferSize = stringValue.length() * sizeof(ushort);
            }
         }
         break;
      case DT_LARGE_STRING:
      case DT_LARGE_NSTRING:
         {
            const auto stringValue = toDbString(value);

            Q_ASSERT(!stringValue.isNull()); // This is handled by Variant::isValueNull()

            ok = true; // Conversion to string is always lossless

            // Create lob if piece is empty
            if (pieceb->isEmpty())
            {
               pieceb->setPieceLength(sizeof(OCILobLocator *));
               dbState = connection->createLobLocator((OCILobLocator **)pieceb->piece());
               if (dbState.type() == QDbState::Error)
                  pieceb->clear();
            }

            // Write to the lob
            if (!pieceb->isEmpty())
            {
               OCILobLocator *lobLocator = *(OCILobLocator **)pieceb->piece();

               // Check if the descriptor is valid!
               if (!connection->lobIsInitialized(lobLocator) || connection->lobIsTemporary(lobLocator))
                  dbState = connection->createTemporaryCLob(lobLocator, _schema.type == DT_LARGE_NSTRING);

               if (dbState.type() != QDbState::Error)
               {
                  dbState = connection->writeCLob(lobLocator, stringValue);
                  pieceb->indicator = I_Normal;
               }
            }
         }
         break;
      case DT_DATE:
      case DT_DATETIME:
         {
            const auto dateTime = value.toDateTimeEx(&ok);

            Q_ASSERT(!dateTime.isNull()); // This is handled by Variant::isValueNull()

            if (dateTime.maxPrecision() == DateTimePart::Millennium)
            {
               pieceb->setPieceLength(sizeof(OCIDate));
               OCIDate *dateMem = (OCIDate *)pieceb->piece();

               OCIDateSetDate(dateMem, dateTime.year(), qMax(dateTime.month(), 1), qMax(dateTime.day(), 1));
               OCIDateSetTime(dateMem, qMax(dateTime.hour(), 0), qMax(dateTime.minute(), 0), qMax(dateTime.second(), 0));

               pieceb->indicator = I_Normal;
            }
            else if (dateTime.maxPrecision() == DateTimePart::Hour)
            {
               // This is an error because Variant already stores the correct type
               qFatal("QTimeEx value with DT_DATETIME type");

               _schema.type = DT_TIME;
               _schema.vendorType = SQLT_AFC;

               const auto timeString = dateTime.toString(SQL92_TIME_FORMAT, QLocale::C);

               if (timeString.length() == 8)
               {
                  pieceb->setPieceLength(8 * sizeof(wchar_t));
                  memcpy(pieceb->piece(), timeString.utf16(), 8u * sizeof(wchar_t));
                  pieceb->indicator = I_Normal;
               }
            }
            else
            {
               qFatal("Invalid timestamp value");
            }
         }
         break;
      case DT_TIME:
         {
            const auto timeString = value.toDateTimeEx(&ok).toString(SQL92_TIME_FORMAT, QLocale::C);

            if (timeString.length() == 8)
            {
               pieceb->setPieceLength(8 * sizeof(wchar_t));
               memcpy(pieceb->piece(), timeString.utf16(), 8u * sizeof(wchar_t));
               pieceb->indicator = I_Normal;
            }
            else
            {
               qFatal("Invalid time value");
            }
         }
         break;
      case DT_BINARY:
         {
            const QByteArray byteArray(value.toByteArray(&ok));

            Q_ASSERT(!byteArray.isNull()); // This is handled by Variant::isValueNull()

            pieceb->write((const ub1*)byteArray.data(), byteArray.size());
            pieceb->indicator = I_Normal;

            _serverBufferSize = byteArray.size();
         }
         break;
      case DT_LARGE_BINARY:
         {
            const auto byteArray = value.toByteArray(&ok);

            Q_ASSERT(!byteArray.isNull()); // This is handled by Variant::isValueNull()

            // Create lob if piece is empty
            if (pieceb->isEmpty())
            {
               pieceb->setPieceLength(sizeof(OCILobLocator *));
               dbState = connection->createLobLocator((OCILobLocator **)pieceb->piece());
               if (dbState.type() == QDbState::Error)
                  pieceb->clear();
            }

            // Write to the lob
            if (!pieceb->isEmpty())
            {
               OCILobLocator *lobLocator = *(OCILobLocator **)pieceb->piece();

               // Check if the descriptor is valid!
               if (!connection->lobIsInitialized(lobLocator) || connection->lobIsTemporary(lobLocator))
                  dbState = connection->createTemporaryBLob(lobLocator);

               if (dbState.type() != QDbState::Error)
               {
                  dbState = connection->writeBLob(lobLocator, byteArray);
                  pieceb->indicator = I_Normal;
               }
            }
         }
         break;
      }

      if (!ok)
         dbState = QDbState(QDbState::Error, "Value cannot be converted to placeholder type without losing information");
   }

   return dbState;
}
