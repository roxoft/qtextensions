#include "OciDescribe.h"
#include "QOciEnvironment.h"

OciDescribe::OciDescribe(OCIError *errhp)
   : _errhp(errhp), _dschp(nullptr), _parmh(nullptr)
{
   // Create a description handle
   envexcpt(QOciEnvironment::theEnvironment->envhp(), OCIHandleAlloc((const dvoid *) QOciEnvironment::theEnvironment->envhp(), (dvoid **) &_dschp, (ub4) OCI_HTYPE_DESCRIBE, (size_t) 0, (dvoid **) 0));

   // Set an attribute to check for a synonym if the object couldn't be found
   ub2 flag = 1;

   errexcpt(_errhp, OCIAttrSet((dvoid *) _dschp, (ub4) OCI_HTYPE_DESCRIBE, (dvoid *) &flag, (ub4) 0, (ub4) OCI_ATTR_DESC_PUBLIC, _errhp));
}

OciDescribe::~OciDescribe()
{
   if (_dschp)
      OCIHandleFree((dvoid *) _dschp, OCI_HTYPE_DESCRIBE);
}

bool OciDescribe::describe(OCISvcCtx *svchp, const QString& name, int type)
{
   QDbState dbState = checkerr(_errhp, OCIDescribeAny(svchp, _errhp, (dvoid *) name.utf16(), (ub4) name.length() * sizeof(ushort), (ub1) OCI_OTYPE_NAME, (ub1) OCI_DEFAULT, (ub1) type, _dschp));
   if (dbState.type() == QDbState::Error)
   {
      if (dbState.number() == 4043) // object not found
         return false;
      throw dbState;
   }

   // Get parameter handle
   errexcpt(_errhp, OCIAttrGet((dvoid *) _dschp, (ub4) OCI_HTYPE_DESCRIBE, (dvoid *) &_parmh, (ub4 *) 0, (ub4) OCI_ATTR_PARAM, _errhp));

   return true;
}

int OciDescribe::getType() const
{
   ub1 objectType = OCI_PTYPE_UNK;

   if (_parmh)
      errexcpt(_errhp, OCIAttrGet((dvoid *) _parmh, (ub4) OCI_DTYPE_PARAM, (dvoid *) &objectType, (ub4 *) 0, (ub4) OCI_ATTR_PTYPE, _errhp));

   return objectType;
}

void OciDescribe::getCommonAttributes(QDbSchema::DbObject& dbObject) const
{
   if (!_parmh)
      return;

   text* textPar = nullptr;
   ub4   textParLen = 0;

   errexcpt(_errhp, OCIAttrGet((dvoid *) _parmh, (ub4) OCI_DTYPE_PARAM, (dvoid *) &textPar, (ub4 *) &textParLen, (ub4) OCI_ATTR_OBJ_NAME, _errhp));

   dbObject.name = QString::fromUtf16((const char16_t *)textPar, textParLen / sizeof(ushort));

   errexcpt(_errhp, OCIAttrGet((dvoid *) _parmh, (ub4) OCI_DTYPE_PARAM, (dvoid *) &textPar, (ub4 *) &textParLen, (ub4) OCI_ATTR_OBJ_SCHEMA, _errhp));

   dbObject.schema = QString::fromUtf16((const char16_t *)textPar, textParLen / sizeof(ushort));
}

void OciDescribe::getDatabaseAttributes(QDbSchema::Database& dbSchema) const
{
   if (!_parmh)
      return;
}

void OciDescribe::getSynonymAttributes(QDbSchema::Synonym& synonymSchema) const
{
   if (!_parmh)
      return;

   text        *textPar = nullptr;
   ub4         textParLen = 0;

   errexcpt(_errhp, OCIAttrGet((dvoid *) _parmh, (ub4) OCI_DTYPE_PARAM, (dvoid *) &textPar, (ub4 *) &textParLen, (ub4) OCI_ATTR_SCHEMA_NAME, _errhp));

   synonymSchema.translatedSchema = QString::fromUtf16((const char16_t *)textPar, textParLen / sizeof(ushort));

   errexcpt(_errhp, OCIAttrGet((dvoid *) _parmh, (ub4) OCI_DTYPE_PARAM, (dvoid *) &textPar, (ub4 *) &textParLen, (ub4) OCI_ATTR_NAME, _errhp));

   synonymSchema.translatedName = QString::fromUtf16((const char16_t *)textPar, textParLen / sizeof(ushort));
}

void OciDescribe::getTableOrViewAttributes(QDbSchema::Table& tableSchema) const
{
   if (!_parmh)
      return;

   // Get the column list and its size
   OCIParam *colList = nullptr;
   ub2      colCount = 0;

   errexcpt(_errhp, OCIAttrGet((dvoid *) _parmh, (ub4) OCI_DTYPE_PARAM, (dvoid *) &colList, (ub4 *) 0, (ub4) OCI_ATTR_LIST_COLUMNS, _errhp));
   errexcpt(_errhp, OCIAttrGet((dvoid *) _parmh, (ub4) OCI_DTYPE_PARAM, (dvoid *) &colCount, (ub4 *) 0, (ub4) OCI_ATTR_NUM_COLS, _errhp));

   // Get the column information
   for (int i = 1; i <= colCount; ++i)
   {
      OCIParam *column = nullptr;

      errexcpt(_errhp, OCIParamGet((dvoid *) colList, (ub4) OCI_DTYPE_PARAM, _errhp, (dvoid **) &column, (ub4) i));

      QDbSchema::Column colInfo;

      colInfo.schema = tableSchema.schema;

      auto dbState = columnAttributes(_errhp, column, colInfo, true);
      if (dbState.type() == QDbState::Error)
         throw dbState;

      tableSchema.columns.append(colInfo);
   }
}

void OciDescribe::getProcedureOrFunctionAttributes(QDbSchema::Function& functionSchema) const
{
   if (!_parmh)
      return;

   functionArguments(_errhp, _parmh, functionSchema, getType() == OCI_PTYPE_PROC);
}

void OciDescribe::getPackageAttributes(QDbSchema::Package& packageSchema) const
{
   if (!_parmh)
      return;

   // Get the subprograms
   OCIParam *subList = nullptr;
   ub2      subCount = 0;
   text*    textPar = nullptr;
   ub4      textParLen = 0;

   errexcpt(_errhp, OCIAttrGet((dvoid *) _parmh, (ub4) OCI_DTYPE_PARAM, (dvoid *) &subList, (ub4 *) 0, (ub4) OCI_ATTR_LIST_SUBPROGRAMS, _errhp));
   errexcpt(_errhp, OCIAttrGet((dvoid *) subList, (ub4) OCI_DTYPE_PARAM, (dvoid *) &subCount, (ub4 *) 0, (ub4) OCI_ATTR_NUM_PARAMS, _errhp));

   for (int subIndex = 0; subIndex < subCount; ++subIndex)
   {
      OCIParam *subprogram = nullptr;

      errexcpt(_errhp, OCIParamGet((dvoid *) subList, (ub4) OCI_DTYPE_PARAM, _errhp, (dvoid **) &subprogram, (ub4) subIndex));

      QDbSchema::Function funcInfo;

      funcInfo.schema = packageSchema.schema;
      funcInfo.packageName = packageSchema.name;

      errexcpt(_errhp, OCIAttrGet((dvoid *) subprogram, (ub4) OCI_DTYPE_PARAM, (dvoid *) &textPar, (ub4 *) &textParLen, (ub4) OCI_ATTR_NAME, _errhp));

      funcInfo.name = QString::fromUtf16((const char16_t *)textPar, textParLen / sizeof(ushort));

      ub2 overloadId;

      errexcpt(_errhp, OCIAttrGet((dvoid *) subprogram, (ub4) OCI_DTYPE_PARAM, (dvoid *) &overloadId, (ub4 *) 0, (ub4) OCI_ATTR_OVERLOAD_ID, _errhp));

      funcInfo.overloadId = overloadId;

      ub1 subType;

      errexcpt(_errhp, OCIAttrGet((dvoid *) subprogram, (ub4) OCI_DTYPE_PARAM, (dvoid *) &subType, (ub4 *) 0, (ub4) OCI_ATTR_PTYPE, _errhp));

      functionArguments(_errhp, subprogram, funcInfo, subType == OCI_PTYPE_PROC);

      packageSchema.subprograms.append(funcInfo);
   }
}

void OciDescribe::functionArguments(OCIError *errhp, OCIParam *parmh, QDbSchema::Function &funcInfo, bool isProcedure)
{
   // Get the argument list and its size
   OCIParam *argList = nullptr;
   ub2      argCount = 0;

   errexcpt(errhp, OCIAttrGet((dvoid *) parmh, (ub4) OCI_DTYPE_PARAM, (dvoid *) &argList, (ub4 *) 0, (ub4) OCI_ATTR_LIST_ARGUMENTS, errhp));
   errexcpt(errhp, OCIAttrGet((dvoid *) argList, (ub4) OCI_DTYPE_PARAM, (dvoid *) &argCount, (ub4 *) 0, (ub4) OCI_ATTR_NUM_PARAMS, errhp));

   // For a procedure, we begin with 1; for a function, we begin with 0
   int      argIndex = isProcedure ? 1 : 0;
   QDbState columnState;

   argCount += argIndex;
   for (; argIndex < argCount; ++argIndex)
   {
      OCIParam *argument = nullptr;

      errexcpt(errhp, OCIParamGet((dvoid *) argList, (ub4) OCI_DTYPE_PARAM, errhp, (dvoid **) &argument, (ub4) argIndex));

      QDbSchema::Column colInfo;

      colInfo.schema = funcInfo.schema;

      columnState = columnAttributes(errhp, argument, colInfo, true);
      if (columnState.type() == QDbState::Error)
         throw columnState;

      ub1 hasDefault;

      errexcpt(errhp, OCIAttrGet((dvoid*) argument, (ub4) OCI_DTYPE_PARAM, (dvoid*) &hasDefault,(ub4 *) 0, (ub4) OCI_ATTR_HAS_DEFAULT, errhp));

      colInfo.hasDefault = hasDefault;

      OCITypeParamMode iomode;

      errexcpt(errhp, OCIAttrGet((dvoid*) argument, (ub4) OCI_DTYPE_PARAM, (dvoid*) &iomode,(ub4 *) 0, (ub4) OCI_ATTR_IOMODE, errhp));

      switch (iomode)
      {
      case OCI_TYPEPARAM_IN:
         colInfo.writable = true;
         break;
      case OCI_TYPEPARAM_OUT:
         colInfo.readable = true;
         break;
      case OCI_TYPEPARAM_INOUT:
         colInfo.writable = true;
         colInfo.readable = true;
         break;
      }

      // To get the attributes of an argument of type record (arguments at the next level), traverse the argument list
      OCIParam *colList = nullptr;

      errexcpt(errhp, OCIAttrGet((dvoid *) argument, (ub4) OCI_DTYPE_PARAM, (dvoid *) &colList, (ub4 *) 0, (ub4) OCI_ATTR_LIST_ARGUMENTS, errhp));

      // Check if the current argument is a record
      if (colList)
      {
         ub2 colCount = 0;

         errexcpt(errhp, OCIAttrGet((dvoid *) colList, (ub4) OCI_DTYPE_PARAM, (dvoid *) &colCount, (ub4 *) 0, (ub4) OCI_ATTR_NUM_PARAMS, errhp));

         // Note that for functions and procedures, the next higher level arguments start from index 1
         for (int colIndex = 1; colIndex <= colCount; ++colIndex)
         {
            OCIParam *column = nullptr;

            errexcpt(errhp, OCIParamGet((dvoid *) colList, (ub4) OCI_DTYPE_PARAM, errhp, (dvoid **) &column, (ub4) colIndex));

            QDbSchema::Column recordColInfo;

            recordColInfo.schema = colInfo.schema;

            columnState = columnAttributes(errhp, column, recordColInfo, true);
            if (columnState.type() == QDbState::Error)
               throw columnState;

            colInfo.recordColumns.append(recordColInfo);
         }
      }

      if (argIndex == 0)
      {
         colInfo.name = funcInfo.name;
         funcInfo.retVal = colInfo;
      }
      else
         funcInfo.arguments.append(colInfo);
   }
}

QDbState OciDescribe::columnAttributes(OCIError *errhp, OCIParam *parmh, QDbSchema::Column &colInfo, bool isExplicit)
{
   QDbState dbState;

   // Get the column name
   text  *textPar = nullptr;
   ub4   textParLen = 0;

   dbState = checkerr(errhp, OCIAttrGet((dvoid *) parmh, (ub4) OCI_DTYPE_PARAM, (dvoid *) &textPar, (ub4 *) &textParLen, (ub4) OCI_ATTR_NAME, errhp));

   colInfo.name = QString::fromUtf16((const char16_t *)textPar, textParLen / sizeof(ushort));

   // Retrieve the data type attribute
   ub2 dtype = 0;

   if (dbState.type() != QDbState::Error)
      dbState = checkerr(errhp, OCIAttrGet((dvoid*) parmh, (ub4) OCI_DTYPE_PARAM, (dvoid*) &dtype, (ub4 *) 0, (ub4) OCI_ATTR_DATA_TYPE, errhp));

   // Get column form
   ub1 charsetForm = SQLCS_NCHAR;

   if (dbState.type() != QDbState::Error)
      dbState = checkerr(errhp, OCIAttrGet((dvoid*) parmh, (ub4) OCI_DTYPE_PARAM, (dvoid*) &charsetForm, (ub4 *) 0, (ub4) OCI_ATTR_CHARSET_FORM, errhp));

   // Specify data type
   colInfo.vendorType = (int)dtype;

   switch (colInfo.vendorType)
   {
   case SQLT_AFC:
   case SQLT_CHR:
   case SQLT_LNG:
      colInfo.type = (charsetForm == SQLCS_NCHAR) ? DT_NSTRING : DT_STRING;
      break;
   case SQLT_CLOB:
   case SQLT_CFILE:
      colInfo.type = (charsetForm == SQLCS_NCHAR) ? DT_LARGE_NSTRING : DT_LARGE_STRING;
      break;
   case SQLT_NUM:
      colInfo.type = DT_DECIMAL;
      break;
   case SQLT_INT:
      colInfo.type = DT_INT;
      break;
   case SQLT_UIN:
      colInfo.type = DT_UINT;
      break;
   case SQLT_FLT:
   case SQLT_BFLOAT:
   case SQLT_BDOUBLE:
      colInfo.type = DT_DOUBLE;
      break;
   case SQLT_DAT:
      colInfo.type = DT_DATETIME;
      break;
   case SQLT_BIN:
   case SQLT_LBI:
      colInfo.type = DT_BINARY;
      break;
   case SQLT_BLOB:
   case SQLT_BFILE:
      colInfo.type = DT_LARGE_BINARY;
      break;
   }

   ub4 charSemantics = 0;
   ub4 width = 0;

   /* Retrieve the length semantics for the column */
   if (dbState.type() != QDbState::Error)
      dbState = checkerr(errhp, OCIAttrGet((dvoid*) parmh, (ub4) OCI_DTYPE_PARAM, (dvoid*) &charSemantics, (ub4 *) 0, (ub4) OCI_ATTR_CHAR_USED, errhp));

   /* Retrieve the column width in bytes or characters */
   if (dbState.type() != QDbState::Error)
      dbState = checkerr(errhp, OCIAttrGet((dvoid*) parmh, (ub4) OCI_DTYPE_PARAM, (dvoid*) &width, (ub4 *) 0, (ub4) (charSemantics ? OCI_ATTR_CHAR_SIZE : OCI_ATTR_DATA_SIZE), errhp));

   colInfo.size = (int)width;

   if (dtype == SQLT_NUM)
   {
      if (dbState.type() != QDbState::Error)
      {
         if (isExplicit)
         {
            ub1 precision = 0;

            dbState = checkerr(errhp, OCIAttrGet((dvoid*) parmh, (ub4) OCI_DTYPE_PARAM, (dvoid*) &precision,(ub4 *) 0, (ub4) OCI_ATTR_PRECISION, errhp));
            colInfo.size = (int)precision;
         }
         else
         {
            sb2 precision = 0;

            dbState = checkerr(errhp, OCIAttrGet((dvoid*) parmh, (ub4) OCI_DTYPE_PARAM, (dvoid*) &precision,(ub4 *) 0, (ub4) OCI_ATTR_PRECISION, errhp));
            colInfo.size = (int)precision;
         }
      }

      if (dbState.type() != QDbState::Error)
      {
         sb1 scale;

         dbState = checkerr(errhp, OCIAttrGet((dvoid*) parmh, (ub4) OCI_DTYPE_PARAM, (dvoid*) &scale,(ub4 *) 0, (ub4) OCI_ATTR_SCALE, errhp));
         colInfo.scale = scale;
      }
   }

   if (dbState.type() != QDbState::Error)
   {
      ub1 nullable;

      dbState = checkerr(errhp, OCIAttrGet((dvoid*) parmh, (ub4) OCI_DTYPE_PARAM, (dvoid*) &nullable,(ub4 *) 0, (ub4) OCI_ATTR_IS_NULL, errhp));
      colInfo.nullable = nullable;
   }

   return dbState;
}
