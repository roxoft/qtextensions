#include "QOciEnvironment.h"
#include <QTimer>

QDbState checkerr0(void *handle, ub4 htype, sword status, bool throwOnError)
{
   QDbState dbState;

   const int   bufSize = 2048;
   wchar_t     errbuf[bufSize];
   sb4         errcode = 0;

   switch (status)
   {
   case OCI_SUCCESS:
      break;
   case OCI_SUCCESS_WITH_INFO:
      if (handle)
      {
         (void) OCIErrorGet(handle, 1, nullptr, &errcode, (OraText *) errbuf, (ub4) sizeof(errbuf), htype);
         errbuf[bufSize - 1] = 0;
         dbState = QDbState(QDbState::Information, QString::fromUtf16((const char16_t *)errbuf), QDbState::Unknown, errcode);
      }
      else
      {
         dbState = QDbState(QDbState::Information, "OCI_SUCCESS_WITH_INFO", QDbState::Unknown, status);
      }
      break;
   case OCI_NEED_DATA:
      dbState = QDbState(QDbState::Warning, "OCI_NEED_DATA", QDbState::Unknown, -status);
      break;
   case OCI_NO_DATA:
      if (handle)
      {
         (void) OCIErrorGet(handle, 1, nullptr, &errcode, (OraText *) errbuf, (ub4) sizeof(errbuf), htype);
         errbuf[bufSize - 1] = 0;
         dbState = QDbState(QDbState::Error, QString::fromUtf16((const char16_t *)errbuf), QDbState::Unknown, errcode);
      }
      else
      {
         dbState = QDbState(QDbState::Error, "OCI_NO_DATA", QDbState::Unknown, 1403);
      }
      break;
   case OCI_ERROR:
      if (handle)
      {
         (void) OCIErrorGet(handle, 1, nullptr, &errcode, (OraText *) errbuf, (ub4) sizeof(errbuf), htype);
         errbuf[bufSize - 1] = 0;
         dbState = QDbState(QDbState::Error, QString::fromUtf16((const char16_t *)errbuf), QDbState::Unknown, errcode);
      }
      else
      {
         dbState = QDbState(QDbState::Error, "NULL handle! Unable to extract detailed diagnostic information.", QDbState::Unknown, status);
      }
      break;
   case OCI_INVALID_HANDLE:
      dbState = QDbState(QDbState::Error, "OCI_INVALID_HANDLE", QDbState::Unknown, status);
      break;
   case OCI_STILL_EXECUTING:
      dbState = QDbState(QDbState::Warning, "OCI_STILL_EXECUTING", QDbState::Unknown, -status);
      break;
   case OCI_CONTINUE:
      dbState = QDbState(QDbState::Information, "OCI_CONTINUE", QDbState::Unknown, status);
      break;
   }

   if (throwOnError && dbState.type() == QDbState::Error)
      throw dbState;

   return dbState;
}

SessionWatcher::SessionWatcher()
{
}

SessionWatcher::~SessionWatcher()
{
}

void SessionWatcher::run()
{
   QTimer timer;

   connect(&timer, &QTimer::timeout, this, &SessionWatcher::onTimeout);
   timer.start(60000);

   QThread::run();

   timer.stop();
}

void SessionWatcher::onTimeout()
{
   if (QOciEnvironment::theEnvironment)
      QOciEnvironment::theEnvironment->checkSessions();
}

SessionWatcher    sessionWatcher;
QOciEnvironment   *QOciEnvironment::theEnvironment = nullptr;
int               QOciEnvironment::environmentRefCount = 0;

void QOciEnvironment::attach()
{
   if (theEnvironment == nullptr)
   {
      theEnvironment = new QOciEnvironment;
      sessionWatcher.start();
   }
   environmentRefCount++;
}

void QOciEnvironment::detach()
{
   environmentRefCount--;
   if (environmentRefCount == 0 && theEnvironment)
   {
      sessionWatcher.quit();
      sessionWatcher.wait();
      delete theEnvironment;
      theEnvironment = nullptr;
   }
}

QOciEnvironment::QOciEnvironment()
{
   // Initialize OCI by creating the OCI Environment handle
   //_dbState = checkenv(_envhp, OCIEnvCreate(&_envhp, OCI_THREADED, NULL, NULL, NULL, NULL, (size_t) 0, (void**) NULL));
   // Use the enhanced version to set the client encoding for strings
   _dbState = checkenv(_envhp, OCIEnvNlsCreate(&_envhp, OCI_THREADED, NULL, NULL, NULL, NULL, (size_t) 0, (void **) NULL, OCI_UTF16ID, OCI_UTF16ID));

   // Allocate an error handle
   if (_dbState.type() != QDbState::Error)
      _dbState = checkenv(_envhp, OCIHandleAlloc(_envhp, (dvoid**)&_errhp, OCI_HTYPE_ERROR, (size_t)0, (void **)NULL));
}

QOciEnvironment::~QOciEnvironment()
{
   for (auto&& session : _sessions)
   {
      if (session.svchp)
         (void)OCISessionRelease(session.svchp, _errhp, NULL, 0, OCI_DEFAULT);
   }
   for (auto&& pool : _pools)
   {
      (void)OCISessionPoolDestroy(pool.hp, _errhp, OCI_DEFAULT);
      (void)OCIHandleFree(pool.hp, OCI_HTYPE_SPOOL);
   }
   (void)OCIHandleFree(_errhp, OCI_HTYPE_ERROR);
   (void)OCIHandleFree(_envhp, OCI_HTYPE_ENV);
}

QDbState QOciEnvironment::getSession(OCISvcCtx** svchp, const QString& userName, const QString& password, const QString& connectionString)
{
   QMutexLocker locker(&_mutex);

   const auto sessionIndex = findSession(userName, password, connectionString);

   if (sessionIndex != -1)
   {
      auto& session = _sessions[sessionIndex];
      session.busy = true;
      session.timeout = 0;
      *svchp = session.svchp;
   }
   else
   {
      *svchp = nullptr;
   }

   return _dbState;
}

QDbState QOciEnvironment::releaseSession(OCISvcCtx* svchp)
{
   QMutexLocker locker(&_mutex);

   auto index = 0;

   while (index < _sessions.count() && _sessions.at(index).svchp != svchp)
      index++;

   if (index < _sessions.count())
   {
      _sessions[index].busy = false;
      _sessions[index].timeout = 5; // 5 Minutes
      auto dbState = checkerr(_errhp, OCIPing(svchp, _errhp, OCI_DEFAULT));
      if (dbState.type() == QDbState::Error)
         _sessions.removeAt(index); // Remove the session
      else
         svchp = nullptr; // Keep the session
   }

   if (svchp)
      (void)OCISessionRelease(svchp, _errhp, NULL, 0, OCI_DEFAULT);

   return _dbState;
}

void QOciEnvironment::checkSessions()
{
   QMutexLocker locker(&_mutex);

   // Drop broken connections

   for (auto index = _sessions.count(); index--; )
   {
      if (!_sessions.at(index).busy)
      {
         auto svchp = _sessions.at(index).svchp;
         auto dbState = checkerr(_errhp, OCIPing(svchp, _errhp, OCI_DEFAULT));

         if (dbState.type() == QDbState::Error)
         {
            // Remove the session
            (void)OCISessionRelease(svchp, _errhp, NULL, 0, OCI_DEFAULT);
            _sessions.removeAt(index);
         }
      }
   }

   // Remove excess connections

   for (auto index = _sessions.count(); index-- && _sessions.count() > _minSessions; )
   {
      if (_sessions[index].timeout)
         _sessions[index].timeout--;

      if (!_sessions.at(index).busy && _sessions[index].timeout == 0)
      {
         // Remove the session
         (void)OCISessionRelease(_sessions.at(index).svchp, _errhp, NULL, 0, OCI_DEFAULT);
         _sessions.removeAt(index);
      }
   }
}

int QOciEnvironment::findSession(const QString& userName, const QString& password, const QString& connectionString)
{
   for (int i = 0; i < _sessions.count(); ++i)
   {
      auto session = _sessions.at(i);
      if (!session.busy && session.connectionString.compare(connectionString, Qt::CaseInsensitive) == 0 && session.userName.compare(userName, Qt::CaseInsensitive) == 0)
         return i;
   }

   return createSession(userName, password, connectionString);
}

int QOciEnvironment::createSession(const QString& userName, const QString& password, const QString& connectionString)
{
   auto sessionIndex = -1;

   if (_dbState.type() == QDbState::Error)
      return sessionIndex;

   // Allocate authentication handle
   OCIAuthInfo* authp = nullptr;

   if (_dbState.type() != QDbState::Error)
      _dbState = checkenv(_envhp, OCIHandleAlloc(_envhp, (dvoid**)&authp, OCI_HTYPE_AUTHINFO, (size_t)0, (dvoid**)NULL));

   // Setup username and password
   if (_dbState.type() != QDbState::Error)
      _dbState = checkerr(_errhp, OCIAttrSet(authp, OCI_HTYPE_AUTHINFO, (dvoid*)userName.utf16(), (ub4)(userName.length() * sizeof(ushort)), OCI_ATTR_USERNAME, _errhp));
   if (_dbState.type() != QDbState::Error)
      _dbState = checkerr(_errhp, OCIAttrSet(authp, OCI_HTYPE_AUTHINFO, (dvoid*)password.utf16(), (ub4)(password.length() * sizeof(ushort)), OCI_ATTR_PASSWORD, _errhp));

   // Connect to the database
   if (_dbState.type() != QDbState::Error)
   {
      SessionInfo session;

      session.connectionString = connectionString;
      session.userName = userName;
      session.password = password;

      _dbState = checkerr(_errhp, OCISessionGet(_envhp, _errhp, &session.svchp, authp, (OraText *)connectionString.utf16(), (ub4)(connectionString.length() * sizeof(ushort)), NULL, 0, NULL, NULL, NULL, OCI_DEFAULT));
      if (_dbState.type() != QDbState::Error)
      {
         sessionIndex = _sessions.count();
         _sessions.insert(sessionIndex, session);
      }
   }

   (void)OCIHandleFree(authp, OCI_HTYPE_AUTHINFO);

   return sessionIndex;
}

#if 0
QOciEnvironment::PoolInfo QOciEnvironment::findPool(const QString& userName, const QString& password, const QString& connectionString)
{
   for (auto&& pool : _pools)
   {
      if (pool.connectionString.compare(connectionString, Qt::CaseInsensitive) == 0)
         return pool;
   }

   return addPool(userName, password, connectionString);
}

QOciEnvironment::PoolInfo QOciEnvironment::addPool(const QString& userName, const QString& password, const QString& connectionString)
{
   PoolInfo pool;

   if (_dbState.type() == QDbState::Error)
      return pool;

   pool.connectionString = connectionString;

   try
   {
      envexcpt(_envhp, OCIHandleAlloc((dvoid*)_envhp, (dvoid**)&pool.hp, OCI_HTYPE_SPOOL, (size_t)0, (dvoid**)NULL));

      OraText* poolName;
      ub4 poolLen;

      errexcpt(_errhp, OCISessionPoolCreate(_envhp, _errhp, pool.hp,
         &poolName, &poolLen,
         (const OraText *)connectionString.utf16(), (ub4)(connectionString.length() * sizeof(ushort)),
         (ub4)6, (ub4)16, (ub4)1, // conMin, conMax, conIncr
         (OraText *)userName.utf16(), (ub4)(userName.length() * sizeof(ushort)),
         (OraText *)password.utf16(), (ub4)(password.length() * sizeof(ushort)),
         OCI_SPC_HOMOGENEOUS));

      pool.name = QString::fromUtf16((const ushort*)poolName, poolLen / sizeof(ushort));

      _pools.append(pool);
   }
   catch (const QDbState& dbState)
   {
      _dbState = dbState;
   }

   return pool;
}
#endif
