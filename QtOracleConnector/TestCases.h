#ifndef TESTCASES_H
#define TESTCASES_H

#include <QLocaleEx.h> // Must be included before TestFramework to be able to resolve toQString(bool)
#include <TestFramework>

/*
 To execute the ORACLE test cases you must provide an ini-file in the test data directory called OracleCredentials.ini.
 The ini-file has the following structure:
     Name={Database name}
     Schema={Schema name to use for schema tests}
     [User]
     User={DB user with normal rights}
     Password={Password for the DB user with normal rights}
     Role={Role of the user}
     [Admin]
     User={DB user with admin rights}
     Password={Password for the DB user with admin rights}

 The names in curly braces are placeholders for your proper values.
 To have all unit tests succeed the database server must return error messages in the german locale.
 */
class TestCases : public TestFramework
{
   Q_OBJECT

public:
   TestCases(QObject *parent = 0) : TestFramework(parent) {}
   virtual ~TestCases() {}

   void init() override;
   void deinit() override;

public slots :
   void stmtParsingTests();
   void sqlTest();
   void triggerTest();
   void schemaTests();
   void numericLimitTest();

private:
   QString _dbName;
   QString _dbSchema;
   QString _dbAdmin;
   QString _dbAdminPassword;
   QString _dbUser;
   QString _dbUserPassword;
   QString _dbUserRole;
};

#endif // TESTCASES_H
