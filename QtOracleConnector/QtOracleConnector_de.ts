<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE">
<context>
    <name>OciPhInfo</name>
    <message>
        <location filename="OciBuffers.cpp" line="258"/>
        <source>No connection</source>
        <translation>Keine Verbindung</translation>
    </message>
</context>
<context>
    <name>QOciCommandEngine</name>
    <message>
        <location filename="QOciCommandEngine.cpp" line="116"/>
        <source>Invalid position of row counter</source>
        <translation>Ungültige Position des Datensatzzählers</translation>
    </message>
    <message>
        <location filename="QOciCommandEngine.cpp" line="118"/>
        <source>Index %1 not in result set</source>
        <translation>Der Index %1 ist nicht im Ergebnis</translation>
    </message>
    <message>
        <location filename="QOciCommandEngine.cpp" line="147"/>
        <source>Column name %1 not found in result set</source>
        <translation>Der Spaltenname %1 ist nicht im Ergebnis</translation>
    </message>
    <message>
        <location filename="QOciCommandEngine.cpp" line="165"/>
        <source>The data type of the placeholder at position %1 cannot be set after execution</source>
        <translation>Datentyp für den Platzhalter an Position %1 darf nach der Ausführung nicht gesetzt werden</translation>
    </message>
    <message>
        <location filename="QOciCommandEngine.cpp" line="225"/>
        <source>Placeholder at position %1 is not defined</source>
        <translation>Der Platzhalter an Position %1 ist nicht definiert</translation>
    </message>
</context>
<context>
    <name>QOciConnection</name>
    <message>
        <location filename="QOciConnection.cpp" line="672"/>
        <source>The placeholder at position %1 has no value</source>
        <translation>Der Platzhalter an Position %1 enthält keinen Wert</translation>
    </message>
</context>
</TS>
