#include "QOciConnection.h"
#include "QOciEnvironment.h"
#include "QOciCommandEngine.h"
#include "OciBuffers.h"
#include "OciDescribe.h"
#include <QRational>
#include <QByteArray>
#include <QDecimal>
#include <QTextCodec>

// One piece operations should not exceed 32768 bytes!
// The maximum size for a bind variable in a stored procedure is 32767 bytes (16383 double byte characters) even for a CLOB (see previous comment)!
static sb4 inCallBack(void *ictxp, OCIBind *bindp, ub4 iter, ub4 index, void **bufpp, ub4 *alenp, ub1 *piecep, void **indp)
{
   Q_ASSERT(bindp);
   Q_ASSERT(iter == 0); // More than one iteration is currently not supported
   Q_ASSERT(bufpp);

   BindValue *bindVal = (BindValue *)ictxp;

   bindVal->readPiece(index, bufpp, alenp, (sb2**)indp, piecep);

   return OCI_CONTINUE;
}

// Additional Notes About Callbacks
//
//When a callback function is called, the OCI_ATTR_ROWS_RETURNED attribute of the bind handle tells the application the number of rows being returned in that particular iteration. Thus, when the callback is called the first time in a particular iteration (i.e., index=0), the user can allocate space for all the rows which will be returned for that bind variable. When the callback is called subsequently (with index>0) within the same iteration, the user can merely increment the buffer pointer to the correct memory within the allocated space to retrieve the data.

static sb4 outCallBack(void *octxp, OCIBind *bindp, ub4 iter, ub4 index, void **bufpp, ub4 **alenp, ub1 *piecep, void **indp, ub2 **rcodep)
{
   Q_ASSERT(bindp);
   Q_ASSERT(iter == 0); // More than one iteration is currently not supported
   Q_ASSERT(bufpp);

   BindValue *bindVal = (BindValue *)octxp;

   //if (index == 0)
   //{
   //   // This attribute returns the number of rows that are going to be returned in the current iteration when we are in the OUT callback function for binding a DML statement with RETURNING clause.
   //   ub4 rowsReturned;

   //   auto status = checkerr(bindVal->errhp, OCIAttrGet((dvoid *) bindVal->bindhp, (ub4) OCI_HTYPE_BIND, (dvoid *) &rowsReturned, (ub4 *) 0, (ub4) OCI_ATTR_ROWS_RETURNED, bindVal->errhp));
   //}

   bindVal->writePiece(index, bufpp, alenp, (sb2**)indp, rcodep, piecep);

   return OCI_CONTINUE;
}

QOciConnection::QOciConnection(const QString &userName, const QString &password, const QString &connectionString)
{
   QOciEnvironment::attach();

   _dbState = QOciEnvironment::theEnvironment->dbState();

   // Allocate an error handle
   if (_dbState.type() != QDbState::Error)
   {
      OCIEnv *envhp = QOciEnvironment::theEnvironment->envhp();
      _dbState = checkenv(envhp, OCIHandleAlloc(envhp, (dvoid**)&_errhp, OCI_HTYPE_ERROR, (size_t)0, (dvoid**)NULL));
   }

   // Get a session
   if (_dbState.type() != QDbState::Error)
      _dbState = QOciEnvironment::theEnvironment->getSession(&_svchp, userName, password, connectionString);

   // The server character encodings are necessary to set the server buffer sizes!
   setServerCharacterEncodingFromDb(false);
   setServerCharacterEncodingFromDb(true);

   _connectString = connectionString;
}

QOciConnection::~QOciConnection()
{
   QOciEnvironment::theEnvironment->releaseSession(_svchp);
   (void)OCIHandleFree(_errhp, OCI_HTYPE_ERROR);

   QOciEnvironment::detach();
}

bool QOciConnection::isValid() const
{
   return _svchp;
}

QString QOciConnection::connectionString() const
{
   return _connectString;
}

QString QOciConnection::dataSourceName() const
{
   return _connectString;
}

QString QOciConnection::driverName() const
{
   return "Oracle in OraClient11g_home1";
}

SqlStyle QOciConnection::sqlStyle() const
{
   return SqlStyle::Oracle;
}

QDbConnection::PhType QOciConnection::phType() const
{
   return QDbConnection::PHT_NamedOnly;
}

QDbEnvironment* QOciConnection::environment()
{
   return QOciEnvironment::theEnvironment;
}

bool QOciConnection::beginTransaction()
{
   if (!_transaction)
   {
      _transaction = true;
      return true;
   }
   return false;
}

bool QOciConnection::rollbackTransaction()
{
   if (_transaction)
   {
      _transaction = false;
      _dbState = checkerr(_errhp, OCITransRollback(_svchp, _errhp, (ub4) 0)); // May throw an exception
   }
   return _dbState.type() != QDbState::Error;
}

bool QOciConnection::commitTransaction()
{
   if (_transaction)
   {
      _transaction = false;
      _dbState = checkerr(_errhp, OCITransCommit(_svchp, _errhp, (ub4) 0)); // May throw an exception
   }
   return _dbState.type() != QDbState::Error;
}

bool QOciConnection::abortExecution()
{
   // OCIBreak() + OCIReset()
   return false;
}

bool QOciConnection::tableExists(const QString &tableName)
{
   return tableSchema(tableName).isValid();
}

QChar QOciConnection::phIndicator() const
{
   return u':';
}

QString QOciConnection::toBooleanLiteral(bool value) const
{
   return value ? "'1'" : "'0'";
}

QString QOciConnection::toDateLiteral(const QDateEx& value) const
{
   return QString("TO_DATE('%1', 'YYYY-MM-DD')").arg(value.toString(SQL92_DATE_FORMAT));
}

QString QOciConnection::toTimeLiteral(const QTimeEx& value) const
{
   return QString("'%1'").arg(value.toString(SQL92_TIME_FORMAT));
}

QString QOciConnection::toDateTimeLiteral(const QDateTimeEx& value) const
{
   return QString("TO_TIMESTAMP('%1', 'YYYY-MM-DD HH24:MI:SS')").arg(value.toString(SQL92_DATETIME_FORMAT));
}

QString QOciConnection::toStringLiteral(QString value) const
{
   return QDbConnectionData::toStringLiteral(value);
}

QString QOciConnection::toBinaryLiteral(const QByteArray& value) const
{
   return QString("'%1'").arg(QString::fromLatin1(value.toHex()));
}

QDbSchema::Database QOciConnection::databaseSchema()
{
   return QDbSchema::Database();
}

QDbSchema::Synonym QOciConnection::synonymSchema(const QString &name)
{
   auto it = _schemaCache.synonyms.find(name);

   if (it != _schemaCache.synonyms.end())
      return it.value();

   QDbSchema::Synonym synonymInfo;

   try // The code is nicer if we put it in a try catch block
   {
      OciDescribe descriptor(_errhp);

      if (!descriptor.describe(_svchp, name, OCI_PTYPE_SYN))
         return synonymInfo;

      descriptor.getCommonAttributes(synonymInfo);
      descriptor.getSynonymAttributes(synonymInfo);

      if (_schemaCacheEnabled)
         _schemaCache.synonyms.insert(it, name, synonymInfo);
   }
   catch (const QDbState &dbState)
   {
      _dbState = dbState; // May throw an exception
   }

   return synonymInfo;
}

QDbSchema::Table QOciConnection::tableSchema(const QString &name)
{
   auto it = _schemaCache.tables.find(name);

   if (it != _schemaCache.tables.end())
      return it.value();

   QDbSchema::Table tableInfo;

   try // The code is nicer if we put it in a try catch block
   {
      OciDescribe descriptor(_errhp);

      if (!descriptor.describe(_svchp, name))
         return tableInfo;

      if (descriptor.getType() == OCI_PTYPE_SYN)
      {
         QDbSchema::Synonym synonym;

         descriptor.getSynonymAttributes(synonym);

         auto fullName = synonym.translatedSchema;

         if (!fullName.isEmpty())
            fullName += QChar(u'.');
         fullName += synonym.translatedName;

         if (!descriptor.describe(_svchp, fullName))
            return tableInfo;
      }

      if (descriptor.getType() != OCI_PTYPE_TABLE && descriptor.getType() != OCI_PTYPE_VIEW)
         return tableInfo;

      descriptor.getCommonAttributes(tableInfo);
      descriptor.getTableOrViewAttributes(tableInfo);

      if (_schemaCacheEnabled)
         _schemaCache.tables.insert(it, name, tableInfo);
   }
   catch (const QDbState &dbState)
   {
      _dbState = dbState; // May throw an exception
   }

   return tableInfo;
}

QDbSchema::Function QOciConnection::functionSchema(const QString &name)
{
   auto it = _schemaCache.functions.find(name);

   if (it != _schemaCache.functions.end())
      return it.value();

   QDbSchema::Function funcInfo;

   try // The code is nicer if we put it in a try catch block
   {
      OciDescribe descriptor(_errhp);

      auto firstDotPos = name.indexOf(QChar(u'.'));
      auto lastDotPos = name.lastIndexOf(QChar(u'.'));

      if (firstDotPos != lastDotPos || !descriptor.describe(_svchp, name))
      {
         // The name has two dots or the symbol couldn't be found

         if (lastDotPos == -1)
            return funcInfo; // The name had no dots so it can't be a package

         if (!descriptor.describe(_svchp, name.left(lastDotPos)))
            return funcInfo;
      }

      if (descriptor.getType() == OCI_PTYPE_SYN)
      {
         QDbSchema::Synonym synonym;

         descriptor.getSynonymAttributes(synonym);

         auto fullName = synonym.translatedSchema;

         if (!fullName.isEmpty())
            fullName += QChar(u'.');
         fullName += synonym.translatedName;

         if (!descriptor.describe(_svchp, fullName))
            return funcInfo;
      }

      if (descriptor.getType() == OCI_PTYPE_PKG)
      {
         QDbSchema::Package packageInfo;

         descriptor.getCommonAttributes(packageInfo);
         descriptor.getPackageAttributes(packageInfo);

         funcInfo = packageInfo.subprogram(name.mid(lastDotPos + 1));
      }
      else if (descriptor.getType() == OCI_PTYPE_PROC || descriptor.getType() == OCI_PTYPE_FUNC)
      {
         descriptor.getCommonAttributes(funcInfo);
         descriptor.getProcedureOrFunctionAttributes(funcInfo);
      }

      if (_schemaCacheEnabled && !funcInfo.name.isEmpty())
         _schemaCache.functions.insert(it, name, funcInfo);
   }
   catch (const QDbState &dbState)
   {
      _dbState = dbState; // May throw an exception
   }

   return funcInfo;
}

QDbSchema::Package QOciConnection::packageSchema(const QString &name)
{
   auto it = _schemaCache.packages.find(name);

   if (it != _schemaCache.packages.end())
      return it.value();

   QDbSchema::Package  packageInfo;

   try // The code is nicer if we put it in a try catch block
   {
      OciDescribe descriptor(_errhp);

      if (!descriptor.describe(_svchp, name))
         return packageInfo;

      if (descriptor.getType() == OCI_PTYPE_SYN)
      {
         QDbSchema::Synonym synonym;

         descriptor.getSynonymAttributes(synonym);

         auto fullName = synonym.translatedSchema;

         if (!fullName.isEmpty())
            fullName += QChar(u'.');
         fullName += synonym.translatedName;

         if (!descriptor.describe(_svchp, fullName, OCI_PTYPE_PKG))
            return packageInfo;
      }

      if (descriptor.getType() != OCI_PTYPE_PKG)
         return packageInfo;

      descriptor.getCommonAttributes(packageInfo);
      descriptor.getPackageAttributes(packageInfo);

      if (_schemaCacheEnabled)
         _schemaCache.packages.insert(it, name, packageInfo);
   }
   catch (const QDbState &dbState)
   {
      _dbState = dbState; // May throw an exception
   }

   return packageInfo;
}

bool QOciConnection::schemaCachingEnabled() const
{
   return _schemaCacheEnabled;
}

void QOciConnection::enableSchemaCaching()
{
   _schemaCacheEnabled = true;
}

void QOciConnection::disableSchemaCaching()
{
   _schemaCache.clear();
   _schemaCacheEnabled = false;
}

QDbState QOciConnection::toVariant(unsigned char *buffer, int size, int vendorType, int scale, Variant &value) const
{
   QDbState dbState;

   switch (vendorType)
   {
   case SQLT_CHR:
   case SQLT_AFC:
      // if indicator > 0: actual length BEFORE truncation; if indicator == -2: the actual length BEFORE truncation is greater than 32767
      value = QString::fromUtf16((const char16_t *)buffer, size / sizeof(ushort));
      Q_ASSERT(size > 0); // Not null ORACLE strings cannot be empty
      break;
   case SQLT_VNU:
      {
         QDecimal result;

         result.fromOciNumber((OCINumber *)buffer);

         if (scale >= 0)
            result.setMinFractionalDigits(scale);

         value = result;
      }
      break;
   case SQLT_INT:
      value = *(qint32 *)buffer;
      break;
   case SQLT_UIN:
      value = *(quint32 *)buffer;
      break;
   case SQLT_FLT:
      if (size == 4)
         value = *(float *)buffer;
      else if (size == 8)
         value = *(double *)buffer;
      break;
   case SQLT_BFLOAT:
      value = *(float *)buffer;
      break;
   case SQLT_BDOUBLE:
      value = *(double *)buffer;
      break;
   case SQLT_DAT:
      value = QDateTimeEx(((int)buffer[0] - 100) * 100 + (int)buffer[1] - 100, (int)buffer[2], (int) buffer[3], (int)buffer[4] - 1, (int)buffer[5] - 1, (int)buffer[6] - 1);
      break;
   case SQLT_ODT:
      {
         auto  dateMem = (const OCIDate *)buffer;
         sb2   year;
         ub1   month, day, hour, minute, second;

         OCIDateGetDate(dateMem, &year, &month, &day);
         OCIDateGetTime(dateMem, &hour, &minute, &second);

         value = QDateTimeEx(year, (int)month, (int)day, (int)hour, (int)minute, (int)second);
         break;
      }
   case SQLT_DATE:
   case SQLT_TIMESTAMP:
   case SQLT_TIMESTAMP_TZ:
   case SQLT_TIMESTAMP_LTZ:
      {
         auto  dateTimeMem = (const OCIDateTime *)buffer;
         sb2   year;
         ub1   month, day, hour, minute, second;
         ub4   fsec;
         sb1   tzHour, tzMinute;

         dbState = checkerr(_errhp, OCIDateTimeGetDate(_svchp, _errhp, dateTimeMem, &year, &month, &day));
         if (dbState.type() != QDbState::Error)
            dbState = checkerr(_errhp, OCIDateTimeGetTime(_svchp, _errhp, (OCIDateTime*)dateTimeMem, &hour, &minute, &second, &fsec)); // Is non const dateTimeMem an API error?
         if (dbState.type() != QDbState::Error)
            dbState = checkerr(_errhp, OCIDateTimeGetTimeZoneOffset(_svchp, _errhp, dateTimeMem, &tzHour, &tzMinute));
         if (dbState.type() != QDbState::Error)
         {
            if (scale > 0)
            {
               while (scale < 3)
               {
                  fsec *= 10U;
                  scale++;
               }
               while (scale > 4)
               {
                  fsec /= 10U;
                  scale--;
               }
               if (scale == 4)
               {
                  fsec += 5U;
                  fsec /= 10U;
               }
            }
            else
            {
               fsec = 0U;
            }

            value = QDateTimeEx(year, (int)month, (int)day, (int)hour, (int)minute, (int)second, (int)fsec, (int)tzHour * 60 + (int)tzMinute, false); // DST is unknown!
         }
      }
   case SQLT_LVC:
      {
         const quint32 *lvcMem = (const quint32 *)buffer - 1;

         value = QString::fromUtf16((const char16_t *)(lvcMem + 1), (int)(*lvcMem / sizeof(ushort)));
         break;
      }
   case SQLT_CLOB:
   case SQLT_CFILE:
      {
         QString text;

         dbState = readCLob(*(OCILobLocator **)buffer, text);

         if (text.isEmpty())
            value.clear(); // Empty clobs are null
         else
         value = text;
         break;
      }
   case SQLT_BIN:
      value = QByteArray((const char *)buffer, size);
      break;
   case SQLT_LVB:
      {
         const quint32 *lvbMem = (const quint32 *)buffer - 1;

         value = QByteArray((const char *)(lvbMem + 1), (int)*lvbMem);
         break;
      }
   case SQLT_BLOB:
   case SQLT_BFILE:
      {
         QByteArray blob;

         dbState = readBLob(*(OCILobLocator **)buffer, blob);

         if (blob.isEmpty())
            value.clear(); // Empty blobs are null
         else
            value = blob;
         break;
      }
   }

   return dbState;
}

//QDbState QOciConnection::toOciNumber(OCINumber *ociNumber, const QDecimal &number)
//{
//   long long n = number.toInt64();
//   return checkerr(_errhp, OCINumberFromInt(_errhp, (const dvoid *)&n, (uword)(sizeof(n)), OCI_NUMBER_SIGNED, ociNumber));
//}

//QDbState QOciConnection::ociString(OCIString **ociString, const QString &text)
//{
//   return checkerr(_errhp, OCIStringAssignText(_envhp, _errhp, (const oratext *) text.utf16(), (ub4) (text.length() * 2), ociString));
//}

QDbState QOciConnection::prepareStatement(OCIStmt **stmthp, const wchar_t *statement)
{
   return checkerr(_errhp, OCIStmtPrepare2(_svchp, stmthp, _errhp, (const text *) statement, (ub4) (wcslen(statement) * sizeof(wchar_t)), (const text *) NULL, (ub4) 0, (ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT));
}

QDbState QOciConnection::statementType(OCIStmt *stmthp, int &type)
{
   ub2      t = 0;
   QDbState dbState = checkerr(_errhp, OCIAttrGet((dvoid *) stmthp, (ub4) OCI_HTYPE_STMT, (dvoid *) &t, (ub4 *) 0, (ub4) OCI_ATTR_STMT_TYPE, _errhp));

   type = t;

   return dbState;
}

//Binding RETURNING...INTO variables
//
//Because both the UPDATE and DELETE statements can affect multiple rows in the table, and a DML statement can be executed multiple times in a single OCIExecute() call, how much data will be returned may not be known at runtime. As a result, the variables corresponding to the RETURNING...INTO placeholders must be bound in OCI_DATA_AT_EXEC mode. An application must define its own dynamic data handling callbacks rather than using a polling mechanism.
//
//The returning clause can be particularly useful when working with LOBs. Normally, an application must insert an empty LOB locator into the database, and then SELECT it back out again to operate on it. Using the RETURNING clause, the application can combine these two steps into a single statement:
//
//INSERT INTO some_table VALUES (:in_locator)
//    RETURNING lob_column
//    INTO :out_locator
//
//An OCI application implements the placeholders in the RETURNING clause as pure OUT bind variables. However, all binds in the RETURNING clause are initially IN and must be properly initialized. To provide a valid value, you can provide a NULL indicator and set that indicator to -1.
//
//An application must adhere to the following rules when working with bind variables in a RETURNING clause:
//
//    Bind RETURNING clause placeholders in OCI_DATA_AT_EXEC mode using OCIBindByName() or OCIBindByPos(), followed by a call to OCIBindDynamic() for each placeholder.
//
//    When binding RETURNING clause placeholders, supply a valid OUT bind function as the ocbfp parameter of the OCIBindDynamic() call. This function must provide storage to hold the returned data.
//
//    The icbfp parameter of OCIBindDynamic() call should provide a default function which returns NULL values when called.
//
//    The piecep parameter of OCIBindDynamic() must be set to OCI_ONE_PIECE.
//
//No duplicate binds are allowed in a DML statement with a RETURNING clause, and no duplication between bind variables in the DML section and the RETURNING section of the statement is allowed.
//
//Note:
//OCI only supports the callback mechanism for RETURNING clause binds. The polling mechanism is not supported.

// SQLT_LVC and SQLT_LVB are not supported.
// An error occurs if there is a non LONG or LOB bind with more than 2000 characters or more than 4000 bytes after a LONG or LOB column bind.
QDbState QOciConnection::bindParameter(OCIStmt *stmthp, int pos, BindValue *bindValue, int dmlCommand)
{
   QDbState dbState;

   // Be careful if setting dynamic to false because repeating commands may lead to invalid buffers!
   const bool dynamic = true; // There is actually no need to handle anything not dynamic

   // Calculate the server buffer size
   sb4 maxSize = INT_MAX;

   if (dmlCommand != OCI_STMT_BEGIN) // Not PL/SQL
      maxSize = (sb4)bindValue->serverBufferSize();

   if (dynamic)
   {
      /*
         Valid Datatypes for Piecewise Operations:
         =========================================

         Only some datatypes can be manipulated in pieces. OCI applications can perform piecewise fetches, inserts, or updates of all the following datatypes:
             VARCHAR2
             STRING
             LONG
             LONG RAW
             RAW
             CLOB
             BLOB

         Another way of using this feature for all datatypes is to provide data dynamically for array inserts or updates. The callbacks should always specify OCI_ONE_PIECE for the piecep parameter of the callback for datatypes that do not support piecewise operations.

         See: http://docs.oracle.com/cd/B19306_01/appdev.102/b14250/oci05bnd.htm#i427753
      */

      // Max size must also be specified in server buffer size because Oracle uses it to determine blobs (larger than 4000 bytes!)
      dbState = checkerr(_errhp, OCIBindByPos(stmthp, bindValue->pBindhp(), _errhp, (ub4) pos, (dvoid *) NULL, (sb4) maxSize, (ub2) bindValue->vendorType(), (dvoid *) NULL, (ub2 *) NULL, (ub2 *) NULL, (ub4) 0, (ub4 *) NULL, (ub4) OCI_DATA_AT_EXEC));
   }
   else
   {
      // Use static bind only for IN values!
      auto pieceb = bindValue->pieceBuffer();

      ub4   *alenp = nullptr;
      void  *buffer = nullptr;
      sb2   *indp = nullptr;
      ub2   *rcodep = nullptr;

      if (pieceb)
      {
         pieceb->mergePieces();
         alenp = pieceb->pieceLengthPtr();
         buffer = pieceb->piece();
         indp = &pieceb->indicator;
         rcodep = &pieceb->retCode;
      }

      if (pieceb == nullptr || *indp != I_Null && buffer == nullptr)
         dbState = QDbState(QDbState::Error, tr("The placeholder at position %1 has no value").arg(pos), QDbState::Statement);
      else
         dbState = checkerr(_errhp, OCIBindByPos(stmthp, bindValue->pBindhp(), _errhp, (ub4) pos, (dvoid *) buffer, (sb4) maxSize, (ub2) bindValue->vendorType(), (dvoid *) indp, (ub2 *) alenp, (ub2 *) rcodep, 0, (ub4 *) NULL, OCI_DEFAULT));
   }

   // Set OCI_ATTR_MAXDATA_SIZE to avoid Oracle calculating the wrong buffer sizes (see http://docs.oracle.com/cd/B19306_01/server.102/b14225/ch7progrunicode.htm#i1006356 chapter "OCI Unicode Data Expansion")
   if (dbState.type() != QDbState::Error)
   {
      // Remark:
      // A special consideration applies on the maximum size of bind variables that are neither LONG or LOB, but that appear after any LOB or LONG bind variable in the SQL statement.
      // You receive an ORA-24816 error from the Oracle Database if the maximum size for such bind variables exceeds 4000 bytes.
      // To avoid this error, you must set OCI_ATTR_MAXDATA_SIZE to 4000 bytes for any such binds whose maximum size may exceed 4000 bytes on the server side after character set conversion.
      // Alternatively, reorder the binds so that such binds are placed before any LONG or LOBs in the bind list.
      // See: http://docs.oracle.com/cd/E18283_01/appdev.112/e10646/oci05bnd.htm
      if (dmlCommand == OCI_STMT_INSERT || dmlCommand == OCI_STMT_UPDATE)
         dbState = checkerr(_errhp, OCIAttrSet((dvoid *) *bindValue->pBindhp(), (ub4) OCI_HTYPE_BIND, (dvoid *) &maxSize, (ub4) 0, (ub4)OCI_ATTR_MAXDATA_SIZE, _errhp)); // The size is interpreted as sb4*
   }

   // Set the charset form
   if (dbState.type() != QDbState::Error && (bindValue->dbType() == DT_NSTRING || bindValue->dbType() == DT_LARGE_NSTRING))
   {
      ub1 cform = SQLCS_NCHAR;

      dbState = checkerr(_errhp, OCIAttrSet((dvoid *) *bindValue->pBindhp(), (ub4) OCI_HTYPE_BIND, (dvoid *) &cform, (ub4) 0, (ub4) OCI_ATTR_CHARSET_FORM, _errhp));
   }

   // Bind the callbacks
   if (dynamic && dbState.type() != QDbState::Error)
      dbState = checkerr(_errhp, OCIBindDynamic(*bindValue->pBindhp(), _errhp, (dvoid *) bindValue, inCallBack, (dvoid *) bindValue, outCallBack));

   return dbState;
}

QDbState QOciConnection::executeStatement(OCIStmt *stmthp, int iters)
{
   return checkerr(_errhp, OCIStmtExecute(_svchp, stmthp, _errhp, (ub4) iters, (ub4) 0, (OCISnapshot *) NULL, (OCISnapshot *)NULL, (_transaction ? OCI_DEFAULT : OCI_COMMIT_ON_SUCCESS)));
}

QDbState QOciConnection::rowCount(OCIStmt *stmthp, int &count) const
{
   ub4      c = 0;
   QDbState dbState = checkerr(_errhp, OCIAttrGet((dvoid *) stmthp, (ub4) OCI_HTYPE_STMT, (dvoid *) &c, (ub4 *) 0, (ub4) OCI_ATTR_ROW_COUNT, _errhp));

   count = c;

   return dbState;
}

QDbState QOciConnection::defineOutputBuffers(OCIStmt *stmthp, QVector<OciOutputBuffer> &outputBuffers, int arraySize)
{
   QDbState dbError;

   // Get query parameter
   OCIParam *mypard = nullptr;
   sb4      parm_status = 0;
   ub4      pos = 1;
   ub1      cform = SQLCS_NCHAR;

   // Request a parameter descriptor for position 1 in the select-list
   parm_status = OCIParamGet(stmthp, OCI_HTYPE_STMT, _errhp, (void **) &mypard, pos);

   // Loop only if a descriptor was successfully retrieved for current position, starting at 1
   while (parm_status == OCI_SUCCESS)
   {
      OciOutputBuffer outBuffer;

      dbError = OciDescribe::columnAttributes(_errhp, mypard, outBuffer.schema, false);
      if (dbError.type() == QDbState::Error)
         break;

      outBuffer.size = outBuffer.schema.size;
      if (outBuffer.schema.vendorType == SQLT_AFC || outBuffer.schema.vendorType == SQLT_CHR)
         outBuffer.size *= sizeof(wchar_t);
      else if (outBuffer.schema.vendorType == SQLT_BLOB || outBuffer.schema.vendorType == SQLT_BFILE || outBuffer.schema.vendorType == SQLT_CLOB || outBuffer.schema.vendorType == SQLT_CFILE)
         outBuffer.size = sizeof(OCILobLocator *);
      else if (outBuffer.schema.vendorType == SQLT_NUM)
      {
         outBuffer.schema.vendorType = SQLT_VNU;
         outBuffer.size = sizeof(OCINumber);
      }
      outBuffer.buffer = malloc(outBuffer.size * arraySize);
      memset(outBuffer.buffer, 0, outBuffer.size * arraySize);
      outBuffer.indicator = new sb2[arraySize];
      outBuffer.actualSize = new ub2[arraySize];
      if (outBuffer.schema.vendorType == SQLT_BLOB || outBuffer.schema.vendorType == SQLT_BFILE || outBuffer.schema.vendorType == SQLT_CLOB || outBuffer.schema.vendorType == SQLT_CFILE)
      {
         // Initialize lob locators
         for (int i = 0; i < arraySize; ++i)
         {
            OCILobLocator *lobLocator = nullptr;

            dbError = createLobLocator(&lobLocator);

            if (dbError.type() == QDbState::Error)
               break;

            ((OCILobLocator **)outBuffer.buffer)[i] = lobLocator;
         }
         if (dbError.type() == QDbState::Error)
            break;
      }

      dbError = checkerr(_errhp, OCIDefineByPos(stmthp, &outBuffer.defhp, _errhp, pos, outBuffer.buffer, (sb4) outBuffer.size, (ub2) outBuffer.schema.vendorType, outBuffer.indicator, outBuffer.actualSize, (ub2 *) NULL, OCI_DEFAULT));
      if (dbError.type() == QDbState::Error)
         break;

      if (outBuffer.schema.type == DT_NSTRING || outBuffer.schema.type == DT_LARGE_NSTRING)
      {
         dbError = checkerr(_errhp, OCIAttrSet(outBuffer.defhp, OCI_HTYPE_DEFINE, &cform, 0, OCI_ATTR_CHARSET_FORM, _errhp));
         if (dbError.type() == QDbState::Error)
            break;
      }

      outputBuffers.append(outBuffer);

      /* increment pos and get next descriptor, if there is one */
      pos++;
      parm_status = OCIParamGet(stmthp, OCI_HTYPE_STMT, _errhp, (void **) &mypard, pos);
   }

   return dbError;
}

QDbState QOciConnection::fetch(OCIStmt *stmthp, int& rows, bool& hasMore)
{
   QDbState dbState;
   auto status = OCIStmtFetch(stmthp, _errhp, (ub4) rows, OCI_FETCH_NEXT, OCI_DEFAULT);

   if (status == OCI_NO_DATA)
   {
      hasMore = false;

      ub4 ociRows = 0;

      dbState = checkerr(_errhp, OCIAttrGet(stmthp, OCI_HTYPE_STMT, (void *)&ociRows, (ub4 *) NULL, OCI_ATTR_ROWS_FETCHED, _errhp));

      rows = (int)ociRows;
   }
   else
   {
      dbState = checkerr(_errhp, status);
      hasMore = true;
   }

   return dbState;
}

bool QOciConnection::releaseOutputBuffers(QVector<OciOutputBuffer> &outputBuffers, int arraySize)
{
   bool success = true;

   foreach(OciOutputBuffer buffer, outputBuffers)
   {
      if (buffer.buffer)
      {
         // Free lobs
         if (buffer.schema.vendorType == SQLT_BLOB || buffer.schema.vendorType == SQLT_BFILE || buffer.schema.vendorType == SQLT_CLOB || buffer.schema.vendorType == SQLT_CFILE)
         {
            for (int i = 0; i < arraySize; ++i)
            {
               if (!releaseLobLocator(((OCILobLocator **)buffer.buffer)[i]))
                  success = false;
            }
         }
         free(buffer.buffer);
      }
      delete buffer.indicator;
      delete buffer.actualSize;
   }

   return success;
}

QDbState QOciConnection::releaseStatement(OCIStmt *stmthp)
{
   return checkerr(_errhp, OCIStmtRelease(stmthp, _errhp, (OraText *) NULL, 0, OCI_DEFAULT));
}

QDbState QOciConnection::createLobLocator(OCILobLocator** lobLocator)
{
   QDbState dbState = checkenv(QOciEnvironment::theEnvironment->envhp(), OCIDescriptorAlloc(QOciEnvironment::theEnvironment->envhp(), (void **)lobLocator, OCI_DTYPE_LOB, 0, NULL));

   if (dbState.type() == QDbState::Error)
      return dbState;

   //ub4 lobEmpty = 0;

   //dbState = checkerr(errhp, OCIAttrSet((dvoid *)*lobLocator, OCI_DTYPE_LOB, &lobEmpty, 0, OCI_ATTR_LOBEMPTY, errhp));

   /* Using the LOB locator
   You create a new internal LOB by initializing a new LOB locator using
   OCIDescriptorAlloc(), calling OCIAttrSet() to set it to empty (using the
   OCI_ATTR_LOBEMPTY attribute), and then binding the locator to a
   placeholder in an INSERT statement. Doing so inserts the empty locator into a
   table with a LOB column or attribute. You can then SELECT...FOR UPDATE this
   row to get the locator, and write to it using one of the OCI LOB functions.
   For any LOB write command to be successful, a transaction must be open. If you
   commit a transaction before writing the data, you must lock the row again (by
   reissuing the SELECT...FOR UPDATE, for example), because the commit closes the
   transaction.

   Note: To modify a LOB column or attribute (write, copy, trim, and
   so forth), you must lock the row containing the LOB. One way to
   do this is to use a SELECT...FOR UPDATE statement to select the
   locator before performing the operation.
   */

   return dbState;
}

bool QOciConnection::lobIsInitialized(OCILobLocator *lobLocator)
{
   boolean isInitialized;

   QDbState state = checkerr(_errhp, OCILobLocatorIsInit(QOciEnvironment::theEnvironment->envhp(), _errhp, lobLocator, &isInitialized));

   return state.type() != QDbState::Error && isInitialized;
}

bool QOciConnection::lobIsTemporary(OCILobLocator *lobLocator)
{
   boolean isTemporary;

   QDbState state = checkerr(_errhp, OCILobIsTemporary(QOciEnvironment::theEnvironment->envhp(), _errhp, lobLocator, &isTemporary));

   return state.type() != QDbState::Error && isTemporary;
}

bool QOciConnection::releaseLobLocator(OCILobLocator *lobLocator)
{
   return OCI_SUCCESS == OCIDescriptorFree(lobLocator, OCI_DTYPE_LOB);
}

QDbState QOciConnection::readCLob(OCILobLocator *lobLocator, QString &text) const
{
   ub1 csfrm;

   auto dbState = checkerr(_errhp, OCILobCharSetForm(QOciEnvironment::theEnvironment->envhp(), _errhp, lobLocator, &csfrm));
   if (dbState.type() == QDbState::Error)
      return dbState;

   PieceBuffer pieceBuffer(I_Null);

   dbState = readLob(lobLocator, csfrm, pieceBuffer);
   if (dbState.type() != QDbState::Error)
   {
      pieceBuffer.mergePieces();
      text = QString::fromUtf16((const char16_t *)pieceBuffer.piece(), (int)(pieceBuffer.length() / sizeof(ushort)));
   }

   return dbState;
}

QDbState QOciConnection::writeCLob(OCILobLocator* lobLocator, const QString& text) const
{
   ub1 csfrm;

   auto state = checkerr(_errhp, OCILobCharSetForm(QOciEnvironment::theEnvironment->envhp(), _errhp, lobLocator, &csfrm));

   if (state.type() != QDbState::Error)
      state = writeLob(lobLocator, csfrm, (const char*)text.utf16(), text.length() * sizeof(ushort));

   return state;
}

QDbState QOciConnection::readBLob(OCILobLocator *lobLocator, QByteArray &blob) const
{
   PieceBuffer pieceBuffer(I_Null);

   auto dbState = readLob(lobLocator, 0, pieceBuffer);

   if (dbState.type() != QDbState::Error)
   {
      pieceBuffer.mergePieces();
      blob = QByteArray((const char *)pieceBuffer.piece(), (int)pieceBuffer.length());
   }

   return dbState;
}

QDbState QOciConnection::writeBLob(OCILobLocator* lobLocator, const QByteArray& blob) const
{
   return writeLob(lobLocator, 0, blob.data(), blob.length());
}

QDbState QOciConnection::createTemporaryCLob(OCILobLocator *lobLocator, bool altCsForm)
{
   if (lobIsTemporary(lobLocator))
   {
      QDbState state = checkerr(_errhp, OCILobFreeTemporary(_svchp, _errhp, lobLocator));
      if (state.type() == QDbState::Error)
         return state;
   }

   return checkerr(_errhp, OCILobCreateTemporary(_svchp, _errhp, lobLocator, OCI_UTF16ID, altCsForm ? SQLCS_NCHAR : SQLCS_IMPLICIT, OCI_TEMP_CLOB, OCI_ATTR_NOCACHE, OCI_DURATION_STATEMENT));
}

QDbState QOciConnection::createTemporaryBLob(OCILobLocator *lobLocator)
{
   if (lobIsTemporary(lobLocator))
   {
      QDbState state = checkerr(_errhp, OCILobFreeTemporary(_svchp, _errhp, lobLocator));
      if (state.type() == QDbState::Error)
         return state;
   }

   return checkerr(_errhp, OCILobCreateTemporary(_svchp, _errhp, lobLocator, OCI_UTF16ID, SQLCS_IMPLICIT, OCI_TEMP_BLOB, OCI_ATTR_NOCACHE, OCI_DURATION_STATEMENT));
}

QDbCommandEngine *QOciConnection::newEngine()
{
   return new QOciCommandEngine(this);
}

void QOciConnection::setServerCharacterEncodingFromDb(bool national)
{
   if (_dbState.type() == QDbState::Error)
      return;

   OCIStmt* stmthp = nullptr;
   QVector<OciOutputBuffer> outputBuffers;
   QString charsetName;

   try
   {
      auto sql = national ? L"SELECT UTL_I18N.MAP_CHARSET(value) FROM nls_database_parameters WHERE parameter = 'NLS_NCHAR_CHARACTERSET'" : L"SELECT UTL_I18N.MAP_CHARSET(value) FROM nls_database_parameters WHERE parameter = 'NLS_CHARACTERSET'";
      auto charsetParameter = national ? "NLS_NCHAR_CHARACTERSET" : "NLS_CHARACTERSET";

      auto dbState = prepareStatement(&stmthp, sql);
      if (dbState.type() == QDbState::Error)
         throw dbState;

      dbState = executeStatement(stmthp, 0);
      if (dbState.type() == QDbState::Error)
         throw dbState;

      dbState = defineOutputBuffers(stmthp, outputBuffers, 1);
      if (dbState.type() == QDbState::Error)
         throw dbState;

      int rows = 1;
      bool hasMore = false;

      dbState = fetch(stmthp, rows, hasMore);
      if (dbState.type() == QDbState::Error)
         throw dbState;
      if (rows != 1)
         throw QDbState(QDbState::Error, QString("No '%1' parameter in nls_database_parameters").arg(charsetParameter));
      if (outputBuffers.at(0).indicator[0] == -1)
         throw QDbState(QDbState::Error, QString("'%1' parameter in nls_database_parameters is NULL").arg(charsetParameter));

      Variant value;

      dbState = toVariant((unsigned char *)outputBuffers.at(0).buffer, outputBuffers.at(0).actualSize[0], outputBuffers.at(0).schema.vendorType, outputBuffers.at(0).schema.scale, value);
      if (dbState.type() == QDbState::Error)
         throw dbState;

      charsetName = value.toString();

      if (charsetName.isEmpty())
         throw QDbState(QDbState::Error, QString("'%1' parameter in nls_database_parameters is empty!").arg(charsetParameter));
   }
   catch (const QDbState& dbState)
   {
      _dbState = dbState;
   }

   releaseOutputBuffers(outputBuffers, 1);
   outputBuffers.clear();
   if (stmthp)
   {
      releaseStatement(stmthp);
      stmthp = nullptr;
   }

   if (_dbState.type() == QDbState::Error)
      return;

   if (national)
   {
      if (charsetName != eutf8("UTF-16BE"))
         _serverNationalCharacterEncoding = QTextCodec::codecForName(charsetName.toLatin1());
      else
         _serverNationalCharacterEncoding = nullptr;
   }
   else
   {
      if (charsetName != eutf8("UTF-8"))
         _serverCharacterEncoding = QTextCodec::codecForName(charsetName.toLatin1());
      else
         _serverCharacterEncoding = nullptr;
   }
}

QDbState QOciConnection::readLob(OCILobLocator* lobLocator, unsigned char csfrm, PieceBuffer& pieceBuffer) const
{
   auto dbState = checkerr(_errhp, OCILobOpen(_svchp, _errhp, lobLocator, OCI_LOB_READONLY));
   if (dbState.type() == QDbState::Error)
      return dbState;

   oraub8 byte_amtp = 0LL;
   oraub8 char_amtp = 0LL;

   // One piece operations should not exceed 32768 bytes!

   pieceBuffer.setPieceLength(4000u);

   auto status = OCILobRead2(_svchp, _errhp, lobLocator, &byte_amtp, &char_amtp, 1, pieceBuffer.piece(), pieceBuffer.pieceLength(), OCI_FIRST_PIECE, nullptr, nullptr, csfrm ? OCI_UTF16ID : 0, csfrm);

   pieceBuffer.setPieceLength(byte_amtp);

   while (status == OCI_NEED_DATA && pieceBuffer.length() < 0x80000000UL)
   {
      pieceBuffer.nextPiece();
      pieceBuffer.setPieceLength(4000u);
      byte_amtp = 0;
      char_amtp = 0;
      status = OCILobRead2(_svchp, _errhp, lobLocator, &byte_amtp, &char_amtp, 1, pieceBuffer.piece(), pieceBuffer.pieceLength(), OCI_NEXT_PIECE, nullptr, nullptr, csfrm ? OCI_UTF16ID : 0, csfrm);
      pieceBuffer.setPieceLength(byte_amtp);
   }

   if (status == OCI_NEED_DATA)
   {
      dbState = checkerr(_errhp, OCIBreak(_svchp, _errhp));
      dbState = QDbState(QDbState::Error, "LOB exceeds 0x80000000 bytes", QDbState::Statement);
   }
   else
      dbState = checkerr(_errhp, status);

   dbState = checkerr(_errhp, OCILobClose(_svchp, _errhp, lobLocator)); // If dbState is already error it is not overwritten

   return dbState;
}

QDbState QOciConnection::writeLob(OCILobLocator* lobLocator, unsigned char csfrm, const char* buffer, unsigned long long length) const
{
   QDbState dbState;

   if (buffer == nullptr || length == 0ULL)
      return dbState;

   dbState = checkerr(_errhp, OCILobOpen(_svchp, _errhp, lobLocator, OCI_LOB_READWRITE));
   if (dbState.type() == QDbState::Error)
      return dbState;

   // One piece operations should not exceed 32768 bytes!

   const oraub8 pieceLen = qMin(length, 4000ULL);
   oraub8 bytesWritten = length;

   auto status = OCILobWrite2(_svchp, _errhp, lobLocator, &bytesWritten, nullptr, 1, (void *)buffer, pieceLen, pieceLen < length ? OCI_FIRST_PIECE : OCI_ONE_PIECE, NULL, NULL, csfrm ? OCI_UTF16ID : 0, csfrm);

   Q_ASSERT(bytesWritten == pieceLen);

   buffer += pieceLen;
   length -= pieceLen;

   while (length > pieceLen && status == OCI_NEED_DATA)
   {
      bytesWritten = length;
      status = OCILobWrite2(_svchp, _errhp, lobLocator, &bytesWritten, nullptr, 1, (void *)buffer, pieceLen, OCI_NEXT_PIECE, NULL, NULL, csfrm ? OCI_UTF16ID : 0, csfrm);

      Q_ASSERT(bytesWritten == pieceLen);

      buffer += pieceLen;
      length -= pieceLen;
   }

   if (length && status == OCI_NEED_DATA)
   {
      bytesWritten = length;
      status = OCILobWrite2(_svchp, _errhp, lobLocator, &bytesWritten, nullptr, 1, (void *)buffer, length, OCI_LAST_PIECE, NULL, NULL, csfrm ? OCI_UTF16ID : 0, csfrm);

      Q_ASSERT(bytesWritten == length);
   }

   dbState = checkerr(_errhp, status);

   dbState = checkerr(_errhp, OCILobClose(_svchp, _errhp, lobLocator)); // If dbState is already error it is not overwritten

   return dbState;
}

extern "C" {
   QTORACLECONNECTOR_EXPORT QDbConnectionData *newOracleConnection(const QString &userName, const QString &password, const QString &connectionString)
   {
      return new QOciConnection(userName, password, connectionString);
   }
}
