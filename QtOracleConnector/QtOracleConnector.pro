TEMPLATE = lib
TARGET = QtOracleConnector
QT += core
QT -= gui
DEFINES += QTORACLECONNECTOR_LIB
TRANSLATIONS = QtOracleConnector_de.ts

INCLUDEPATH += include
DEPENDPATH += .

include(QtOracleConnector.pri)

LIBS += -L"$(ORACLE_HOME)/oci/lib/msvc" \
    -loci

contains(QT_ARCH, i386) {
    CONFIG(release, debug|release): INSTALL_LIB_PATH = $$quote($$system_path($$_PRO_FILE_PWD_/../x86/Release))
    else:CONFIG(debug, debug|release): INSTALL_LIB_PATH = $$quote($$system_path($$_PRO_FILE_PWD_/../x86/Debug))
} else {
    CONFIG(release, debug|release): INSTALL_LIB_PATH = $$quote($$system_path($$_PRO_FILE_PWD_/../x64/Release))
    else:CONFIG(debug, debug|release): INSTALL_LIB_PATH = $$quote($$system_path($$_PRO_FILE_PWD_/../x64/Debug))
}

LIBS += -L$${INSTALL_LIB_PATH}

LIBS += -lQtTestFramework

INCLUDEPATH += $$quote($$system_path($$_PRO_FILE_PWD_/../include/QtTestFramework))
DEPENDPATH += $$quote($$system_path($$_PRO_FILE_PWD_/../include/QtTestFramework))

LIBS += -lQtCoreEx

INCLUDEPATH += $$quote($$system_path($$_PRO_FILE_PWD_/../include/QtCoreEx))
DEPENDPATH += $$quote($$system_path($$_PRO_FILE_PWD_/../include/QtCoreEx))

INSTALL_HEADER_PATH = $$quote($$system_path($$_PRO_FILE_PWD_/../include/$${TARGET}))

win32 {
    CONFIG(release, debug|release): TARGET_PATH = $$quote($$system_path($$OUT_PWD/release/$${TARGET}))
    else:CONFIG(debug, debug|release): TARGET_PATH = $$quote($$system_path($$OUT_PWD/debug/$${TARGET}))

    QMAKE_POST_LINK += (if not exist $${INSTALL_LIB_PATH} $(MKDIR) $${INSTALL_LIB_PATH})
    QMAKE_POST_LINK += && $(COPY) $${TARGET_PATH}.lib $${INSTALL_LIB_PATH}
    QMAKE_POST_LINK += && $(COPY) $${TARGET_PATH}.dll $${INSTALL_LIB_PATH}
    CONFIG(debug, debug|release): QMAKE_POST_LINK += && $(COPY) $${TARGET_PATH}.pdb $${INSTALL_LIB_PATH}
    QMAKE_POST_LINK += && (if not exist $${INSTALL_HEADER_PATH} $(MKDIR) $${INSTALL_HEADER_PATH})
} else: unix {
    QMAKE_POST_LINK += $(MKDIR) $${INSTALL_LIB_PATH}
    QMAKE_POST_LINK += && $(COPY) -d $$OUT_PWD/lib$${TARGET}.* $${INSTALL_LIB_PATH}
    QMAKE_POST_LINK += && $(MKDIR) $${INSTALL_HEADER_PATH}
}
QMAKE_POST_LINK += && $(COPY) $$quote($$system_path($$_PRO_FILE_PWD_/include/*)) $${INSTALL_HEADER_PATH}

qtPrepareTool(LUPDATE, lupdate)
QMAKE_POST_LINK += && $$LUPDATE $$PWD/QtOracleConnector.pro
