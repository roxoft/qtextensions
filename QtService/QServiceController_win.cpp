#include "QServiceController.h"
#include <wchar.h>
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include <QVector>

class QServiceController::OsData
{
public:
   OsData() : _hSCM(nullptr), _hService(nullptr), _controlAccess(0), _serviceAccess(0), _errorCode(ERROR_SUCCESS) {}
   ~OsData() { close(); }

   const QString  &serviceName() const { return _serviceName; }
   void           setServiceName(const QString &serviceName) { if (serviceName != _serviceName) close(); _serviceName = serviceName; }

   bool           close();
   bool           installService(const QServiceController::ServiceParameter &parameter, bool gui = false);
   bool           isInstalled();
   ServiceState   status();
   bool           waitForStopped();
   QString        filePath();
   QString        description();
   bool           setFailureActions(const QServiceFailureActions &failureActions);
   bool           uninstall();
   bool           start(const QStringList &args);
   bool           pause();
   bool           resume();
   bool           sendCommand(int code);
   bool           stop();

   QList<QServiceController::ServiceInfo> installedServices();

   DWORD lastErrorCode() const { return _errorCode; }

private:
   DWORD waitTime(DWORD dwWaitHint); // If the status is to be polled use this value for the next poll
   bool  open(DWORD dwDesiredControlAccess, DWORD dwDesiredServiceAccess);
   bool  queryState(DWORD &dwCurrentState, DWORD &dwWaitHint, DWORD &dwCheckPoint);
   bool  stopDependentServices();

private:
   SC_HANDLE   _hSCM;
   SC_HANDLE   _hService;
   QString     _serviceName;
   DWORD       _controlAccess;
   DWORD       _serviceAccess;
   DWORD       _errorCode;
};

bool QServiceController::OsData::close()
{
   _errorCode = ERROR_SUCCESS;

   if (_hService)
   {
      if (!CloseServiceHandle(_hService))
         _errorCode = GetLastError();
      _hService = nullptr;
      _serviceAccess = 0;
   }

   if (_hSCM)
   {
      if (!CloseServiceHandle(_hSCM))
         _errorCode = GetLastError();
      _hSCM = nullptr;
      _controlAccess = 0;
   }

   return _errorCode == ERROR_SUCCESS;
}

bool QServiceController::OsData::installService(const QServiceController::ServiceParameter &parameter, bool gui)
{
   // Open the Service Control Manager
   if (open(SC_MANAGER_ALL_ACCESS, 0))
   {
      QString acc = parameter.accountName;

      DWORD     dwServiceType = parameter.shared ? SERVICE_WIN32_SHARE_PROCESS : SERVICE_WIN32_OWN_PROCESS;
      DWORD     dwStartType = parameter.autostart ? SERVICE_AUTO_START : SERVICE_DEMAND_START;
      wchar_t   *act = 0;
      wchar_t   *pwd = 0;

      if (!acc.isEmpty())
      {
         // The act string must contain a string of the format "Domain\UserName",
         // so if only a username was specified without a domain, default to the local machine domain.
         if (!acc.contains(QLatin1Char('\\'))) {
            acc.prepend(QLatin1String(".\\"));
         }
         if (!acc.endsWith(QLatin1String("\\LocalSystem")))
            act = (wchar_t*)acc.utf16();
      }
      if (!parameter.accountPassword.isEmpty() && act)
         pwd = (wchar_t*)parameter.accountPassword.utf16();

      // Prepare dependencies
      wchar_t  *szzDependencyList = nullptr;
      int      dependencyListSize = 0;

      foreach (const QString &dependency, parameter.dependencies)
         dependencyListSize += dependency.length() + 1;

      if (dependencyListSize)
      {
         szzDependencyList = new wchar_t[(size_t)dependencyListSize + 1]; // Reserve space for the double null

         wchar_t *szDependency = szzDependencyList;

         foreach (const QString &dependency, parameter.dependencies)
         {
            wcscpy_s(szDependency, (size_t)dependency.length() + 1, (const wchar_t*)dependency.utf16());
            szDependency += (size_t)dependency.length() + 1;
         }
         *szDependency = '\0'; // Terminate with a double null
      }

      // Only set INTERACTIVE if act is LocalSystem. (and act should be 0 if it is LocalSystem).
      if (!act && gui)
         dwServiceType |= SERVICE_INTERACTIVE_PROCESS;

      QString displayName = parameter.description;

      if (displayName.isEmpty())
         displayName = _serviceName;

      // Create the service
      _hService = CreateServiceW(_hSCM,
                                 (const wchar_t *)_serviceName.utf16(),
                                 (const wchar_t *)displayName.utf16(),
                                 SERVICE_ALL_ACCESS,
                                 dwServiceType,
                                 dwStartType, SERVICE_ERROR_NORMAL, (const wchar_t *)parameter.commandLine.utf16(),
                                 NULL, NULL, szzDependencyList,
                                 act, pwd);
      if (_hService)
      {
         _serviceAccess = SERVICE_ALL_ACCESS;

         if (!parameter.description.isEmpty())
         {
            SERVICE_DESCRIPTIONW sdesc;

            sdesc.lpDescription = (wchar_t *)parameter.description.utf16();
            if (!ChangeServiceConfig2W(_hService, SERVICE_CONFIG_DESCRIPTION, &sdesc))
               _errorCode = GetLastError();
         }
      }
      else
         _errorCode = GetLastError();
   }

   return _errorCode == ERROR_SUCCESS;
}

bool QServiceController::OsData::isInstalled()
{
   return open(SC_MANAGER_CONNECT, SERVICE_QUERY_CONFIG);
}

ServiceState QServiceController::OsData::status()
{
   ServiceState currentState = ServiceStopped;

   if (open(SC_MANAGER_CONNECT, SERVICE_QUERY_STATUS))
   {
      SERVICE_STATUS info;
      int            res = QueryServiceStatus(_hService, &info);

      if (res)
      {
         switch (info.dwCurrentState)
         {
         case SERVICE_STOPPED:
            currentState = ServiceStopped;
            break;
         case SERVICE_START_PENDING:
            currentState = ServiceStartPending;
            break;
         case SERVICE_RUNNING:
            currentState = ServiceRunning;
            break;
         case SERVICE_PAUSE_PENDING:
            currentState = ServicePausePending;
            break;
         case SERVICE_PAUSED:
            currentState = ServicePaused;
            break;
         case SERVICE_CONTINUE_PENDING:
            currentState = ServiceContinuePending;
            break;
         case SERVICE_STOP_PENDING:
            currentState = ServiceStopPending;
            break;
         }
      }
   }

   return currentState;
}

bool QServiceController::OsData::waitForStopped()
{
   DWORD dwCurrentState;
   DWORD dwWaitHint;
   DWORD dwCheckPoint;
   DWORD dwPrevCheckPoint = 0;
   DWORD dwStartTickCount = GetTickCount();

   while (queryState(dwCurrentState, dwWaitHint, dwCheckPoint))
   {
      if (dwCurrentState == SERVICE_STOPPED)
         return true;

      DWORD dwCurrentTickCount = GetTickCount();

      if (dwCheckPoint > dwPrevCheckPoint)
      {
         dwPrevCheckPoint = dwCheckPoint;
         dwStartTickCount = dwCurrentTickCount;
      }
      else if (dwWaitHint < dwCurrentTickCount - dwStartTickCount)
         break; // The service is not responding

      Sleep(waitTime(dwWaitHint));
   }

   return false;
}

QString QServiceController::OsData::filePath()
{
   QString result;

   if (open(SC_MANAGER_CONNECT, SERVICE_QUERY_CONFIG))
   {
      DWORD                   sizeNeeded = 0;
      DWORD                   bufSize = 0;
      LPQUERY_SERVICE_CONFIGW config = nullptr;

      if (!QueryServiceConfigW(_hService, NULL, 0, &sizeNeeded))
      {
         DWORD dwError = GetLastError();

         if (ERROR_INSUFFICIENT_BUFFER == dwError)
         {
            bufSize = sizeNeeded;
            config = (LPQUERY_SERVICE_CONFIGW) LocalAlloc(LMEM_FIXED, bufSize);
         }
      }

      if (config)
      {
         if (QueryServiceConfigW(_hService, config, bufSize, &sizeNeeded))
         {
            result = QString::fromUtf16((const char16_t *)config->lpBinaryPathName);
         }
         LocalFree(config);
      }
   }

   return result;
}

QString QServiceController::OsData::description()
{
   QString result;

   if (open(SC_MANAGER_CONNECT, SERVICE_QUERY_CONFIG))
   {
      DWORD                   sizeNeeded = 0;
      DWORD                   bufSize = 0;
      LPSERVICE_DESCRIPTIONW  desc = nullptr;

      if (!QueryServiceConfig2W(_hService, SERVICE_CONFIG_DESCRIPTION, NULL, 0, &sizeNeeded))
      {
         DWORD dwError = GetLastError();

         if (ERROR_INSUFFICIENT_BUFFER == dwError)
         {
            bufSize = sizeNeeded;
            desc = (LPSERVICE_DESCRIPTIONW) LocalAlloc(LMEM_FIXED, bufSize);
         }
      }

      if (desc)
      {
         if (QueryServiceConfig2W(_hService, SERVICE_CONFIG_DESCRIPTION, (LPBYTE)desc, bufSize, &sizeNeeded))
         {
            result = QString::fromUtf16((const char16_t *)desc->lpDescription);
         }
         LocalFree(desc);
      }
   }

   return result;
}

bool QServiceController::OsData::setFailureActions(const QServiceFailureActions &failureActions)
{
   DWORD desiredServiceAccess = SERVICE_CHANGE_CONFIG;

   foreach (const QServiceFailureActions::Action &action, failureActions.actions)
   {
      if (action.type == QServiceFailureActions::AT_Restart)
      {
         desiredServiceAccess |= SERVICE_START;
         break;
      }
   }

   if (open(SC_MANAGER_CONNECT, desiredServiceAccess))
   {
      SERVICE_FAILURE_ACTIONS fa;

      fa.dwResetPeriod = failureActions.resetPeriod < 0 ? INFINITE : (DWORD) failureActions.resetPeriod;
      fa.lpRebootMsg = failureActions.rebootMessage == QLatin1String("*") ? NULL : (wchar_t *) failureActions.rebootMessage.utf16();
      fa.lpCommand = failureActions.commandToRun == QLatin1String("*") ? NULL : (wchar_t *) failureActions.commandToRun.utf16();
      fa.cActions = (DWORD) failureActions.actions.count();
      fa.lpsaActions = NULL;
      if (!failureActions.actions.isEmpty())
      {
         fa.lpsaActions = new SC_ACTION[failureActions.actions.count()];
         for (int i = 0; i < failureActions.actions.count(); ++i)
         {
            switch (failureActions.actions.at(i).type)
            {
            case QServiceFailureActions::AT_Reboot:
               fa.lpsaActions[i].Type = SC_ACTION_REBOOT;
               break;
            case QServiceFailureActions::AT_Restart:
               fa.lpsaActions[i].Type = SC_ACTION_RESTART;
               break;
            case QServiceFailureActions::AT_RunCommand:
               fa.lpsaActions[i].Type = SC_ACTION_RUN_COMMAND;
               break;
            default:
               fa.lpsaActions[i].Type = SC_ACTION_NONE;
            }
            fa.lpsaActions[i].Delay = (DWORD) failureActions.actions.at(i).delay;
         }
      }

      bool success = ChangeServiceConfig2(_hService, SERVICE_CONFIG_FAILURE_ACTIONS, &fa) != FALSE;

      if (!success)
         _errorCode = GetLastError();

      if (fa.lpsaActions)
         delete fa.lpsaActions;

      return success;
   }

   return false;
}

bool QServiceController::OsData::uninstall()
{
   if (open(SC_MANAGER_CONNECT, DELETE))
   {
      if (DeleteService(_hService))
      {
         close();
         return true;
      }
   }
   return false;
}

bool QServiceController::OsData::start(const QStringList &args)
{
   if (open(SC_MANAGER_ALL_ACCESS, SERVICE_ALL_ACCESS))
   {
      SERVICE_STATUS_PROCESS  ssStatus;
      DWORD                   dwBytesNeeded;

      // Check the status in case the service is not stopped.

      if (!QueryServiceStatusEx(
            _hService,                     // handle to service
            SC_STATUS_PROCESS_INFO,         // information level
            (LPBYTE) &ssStatus,             // address of structure
            sizeof(SERVICE_STATUS_PROCESS), // size of structure
            &dwBytesNeeded ) )              // size needed if buffer is too small
      {
         ssStatus.dwCurrentState = 0;
         _errorCode = GetLastError();
      }

      // Check if the service is already running. It would be possible
      // to stop the service here, but for simplicity this example just returns.
      if (ssStatus.dwCurrentState == SERVICE_RUNNING)
         return true;

      if (ssStatus.dwCurrentState == SERVICE_STOPPED)
      {
         // Attempt to start the service.
         QVector<const wchar_t *> argv(args.size());

         for (int i = 0; i < args.size(); ++i)
               argv[i] = (const wchar_t*)args.at(i).utf16();

         if (StartServiceW(
               _hService,        // handle to service
               args.size(),      // number of arguments
               argv.data()) )    // arguments
         {
            return true;
         }
         _errorCode = GetLastError();
      }
   }

   return false;
}

bool QServiceController::OsData::pause()
{
   if (open(SC_MANAGER_CONNECT, SERVICE_PAUSE_CONTINUE))
   {
      SERVICE_STATUS status;

      if (ControlService(_hService, SERVICE_CONTROL_PAUSE, &status))
      {
         return true;
      }
   }

   return false;
}

bool QServiceController::OsData::resume()
{
   if (open(SC_MANAGER_CONNECT, SERVICE_PAUSE_CONTINUE))
   {
      SERVICE_STATUS status;

      if (ControlService(_hService, SERVICE_CONTROL_CONTINUE, &status))
      {
         return true;
      }
   }

   return false;
}

bool QServiceController::OsData::sendCommand(int code)
{
   if (code < 0 || code > 127)
      return false;

   if (open(SC_MANAGER_CONNECT, SERVICE_USER_DEFINED_CONTROL))
   {
      SERVICE_STATUS status;

      if (ControlService(_hService, 128 + code, &status))
         return true;
   }

   return false;
}

bool QServiceController::OsData::stop()
{
   if (open(SC_MANAGER_ALL_ACCESS, SERVICE_STOP | SERVICE_QUERY_STATUS | SERVICE_ENUMERATE_DEPENDENTS))
   {
      SERVICE_STATUS_PROCESS  ssp;
      DWORD                   dwBytesNeeded;

      // Make sure the service is not already stopped.

      if (QueryServiceStatusEx(
            _hService,
            SC_STATUS_PROCESS_INFO,
            (LPBYTE)&ssp,
            sizeof(SERVICE_STATUS_PROCESS),
            &dwBytesNeeded ) )
      {
         if (ssp.dwCurrentState == SERVICE_STOPPED)
            return true;

         if (ssp.dwCurrentState != SERVICE_STOP_PENDING)
         {
            // If the service is running, dependencies must be stopped first.
            stopDependentServices(); // Thats bad because it blocks

            // Send a stop code to the service.
            if (ControlService(
                  _hService,
                  SERVICE_CONTROL_STOP,
                  (LPSERVICE_STATUS) &ssp ) )
            {
               return true;
            }
         }
      }
   }

   return false;
}

QList<QServiceController::ServiceInfo> QServiceController::OsData::installedServices()
{
   QList<QServiceController::ServiceInfo> result;

   if (open(SC_MANAGER_ENUMERATE_SERVICE, 0))
   {
      ENUM_SERVICE_STATUS_PROCESS   serviceList[100];
      DWORD                         bytesNeeded = 0;
      DWORD                         serviceCount = 0;
      DWORD                         serviceIndex = 0;

      _errorCode = ERROR_MORE_DATA;
      while (_errorCode == ERROR_MORE_DATA)
      {
         if (!EnumServicesStatusEx(_hSCM, SC_ENUM_PROCESS_INFO, SERVICE_WIN32, SERVICE_STATE_ALL, (LPBYTE) serviceList, sizeof(serviceList), &bytesNeeded, &serviceCount, &serviceIndex, NULL))
            _errorCode = GetLastError();
         else
            _errorCode = ERROR_SUCCESS;

         if (_errorCode == ERROR_SUCCESS || _errorCode == ERROR_MORE_DATA)
         {
            for (unsigned i = 0; i < serviceCount; ++i)
            {
               QServiceController::ServiceInfo serviceInfo;

               serviceInfo.name = QString::fromUtf16((const char16_t *)serviceList[i].lpServiceName);
               serviceInfo.displayName = QString::fromUtf16((const char16_t *)serviceList[i].lpDisplayName);
               serviceInfo.shared = serviceList[i].ServiceStatusProcess.dwServiceType == SERVICE_WIN32_SHARE_PROCESS;
               switch (serviceList[i].ServiceStatusProcess.dwCurrentState)
               {
               case SERVICE_CONTINUE_PENDING:
                  serviceInfo.status = ServiceContinuePending;
                  break;
               case SERVICE_PAUSE_PENDING:
                  serviceInfo.status = ServicePausePending;
                  break;
               case SERVICE_PAUSED:
                  serviceInfo.status = ServicePaused;
                  break;
               case SERVICE_RUNNING:
                  serviceInfo.status = ServiceRunning;
                  break;
               case SERVICE_START_PENDING:
                  serviceInfo.status = ServiceStartPending;
                  break;
               case SERVICE_STOP_PENDING:
                  serviceInfo.status = ServiceStopPending;
                  break;
               case SERVICE_STOPPED:
                  serviceInfo.status = ServiceStopped;
                  break;
               }

               result.append(serviceInfo);
            }
         }
      }
   }

   return result;
}

DWORD QServiceController::OsData::waitTime(DWORD dwWaitHint)
{
   // Do not wait longer than the wait hint. A good interval is
   // one-tenth of the wait hint but not less than 1 second
   // and not more than 10 seconds.
   DWORD interval = dwWaitHint / 10;

   if (interval < 1000)
      interval = 1000;
   else if (interval > 10000)
      interval = 10000;

   return interval;
}

bool QServiceController::OsData::open(DWORD dwDesiredControlAccess, DWORD dwDesiredServiceAccess)
{
   _errorCode = ERROR_SUCCESS;

   if (_hSCM && (dwDesiredControlAccess & ~_controlAccess))
   {
      if (!close())
         return false;
   }
   if (_hService && (dwDesiredServiceAccess & ~_serviceAccess))
   {
      if (!CloseServiceHandle(_hService))
      {
         _errorCode = GetLastError();
         _hService = nullptr;
         return false;
      }
      _hService = nullptr;
   }

   if (_hSCM == nullptr && dwDesiredControlAccess)
   {
      _hSCM = OpenSCManager(  NULL,                      // local computer
                              NULL,                      // servicesActive database
                              dwDesiredControlAccess);   // access rights
      if (_hSCM)
         _controlAccess = dwDesiredControlAccess;
      else
         _errorCode = GetLastError();
   }

   if (_hSCM && _hService == nullptr && dwDesiredServiceAccess)
   {
      _hService = OpenServiceW(_hSCM, (const wchar_t *)_serviceName.utf16(), dwDesiredServiceAccess);
      if (_hService)
         _serviceAccess = dwDesiredServiceAccess;
      else
         _errorCode = GetLastError();
   }

   return _errorCode == ERROR_SUCCESS;
}

bool QServiceController::OsData::queryState(DWORD &dwCurrentState, DWORD &dwWaitHint, DWORD &dwCheckPoint)
{
   if (open(SC_MANAGER_CONNECT, SERVICE_QUERY_STATUS))
   {
      SERVICE_STATUS info;
      int            res = QueryServiceStatus(_hService, &info);

      if (res)
      {
         dwCurrentState = info.dwCurrentState;
         dwWaitHint = info.dwWaitHint;
         dwCheckPoint = info.dwCheckPoint;
         return true;
      }
   }

   return false;
}

bool QServiceController::OsData::stopDependentServices()
{
   DWORD i;
   DWORD dwBytesNeeded;
   DWORD dwCount;

   _errorCode = ERROR_SUCCESS;

   LPENUM_SERVICE_STATUS   lpDependencies = NULL;
   ENUM_SERVICE_STATUS     ess;
   SC_HANDLE               hDepService;
   SERVICE_STATUS_PROCESS  ssp;

   DWORD dwStartTime = GetTickCount();
   DWORD dwTimeout = 30000; // 30-second time-out

   // Pass a zero-length buffer to get the required buffer size.
   if (EnumDependentServices( _hService, SERVICE_ACTIVE, lpDependencies, 0, &dwBytesNeeded, &dwCount ))
   {
      // If the Enum call succeeds, then there are no dependent services, so do nothing.
      return true;
   }
   else
   {
      if (GetLastError() != ERROR_MORE_DATA)
      {
         _errorCode = GetLastError();
         return false; // Unexpected error
      }

      // Allocate a buffer for the dependencies.
      lpDependencies = (LPENUM_SERVICE_STATUS) HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, dwBytesNeeded);

      if ( !lpDependencies )
         return false;

      __try {
         // Enumerate the dependencies.
         if ( !EnumDependentServices( _hService, SERVICE_ACTIVE,
               lpDependencies, dwBytesNeeded, &dwBytesNeeded,
               &dwCount ) )
         return false;

         for ( i = 0; i < dwCount; i++ )
         {
               ess = *(lpDependencies + i);
               // Open the service.
               hDepService = OpenService( _hSCM,
                  ess.lpServiceName,
                  SERVICE_STOP | SERVICE_QUERY_STATUS );

               if ( !hDepService )
                  return false;

               __try {
                  // Send a stop code.
                  if ( !ControlService( hDepService,
                           SERVICE_CONTROL_STOP,
                           (LPSERVICE_STATUS) &ssp ) )
                  return false;

                  // Wait for the service to stop.
                  while ( ssp.dwCurrentState != SERVICE_STOPPED )
                  {
                     Sleep(waitTime(ssp.dwWaitHint));

                     if ( !QueryServiceStatusEx(
                              hDepService,
                              SC_STATUS_PROCESS_INFO,
                              (LPBYTE)&ssp,
                              sizeof(SERVICE_STATUS_PROCESS),
                              &dwBytesNeeded ) )
                     return false;

                     if ( ssp.dwCurrentState == SERVICE_STOPPED )
                        break;

                     if ( GetTickCount() - dwStartTime > dwTimeout )
                        return false;
                  }
               }
               __finally
               {
                  // Always release the service handle.
                  CloseServiceHandle( hDepService );
               }
         }
      }
      __finally
      {
         // Always free the enumeration buffer.
         HeapFree( GetProcessHeap(), 0, lpDependencies );
      }
   }
   return true;
}

//
// class QServiceController
//

QServiceController::QServiceController(QObject *parent) : QObject(parent), _osData(new OsData)
{
}

QServiceController::QServiceController(const QString &name, QObject *parent) : QObject(parent), _serviceName(name), _osData(new OsData)
{
   _osData->setServiceName(_serviceName);
}

QServiceController::~QServiceController()
{
   delete _osData;
}

void QServiceController::setServiceName(const QString &serviceName)
{
   _serviceName = serviceName;
   _osData->setServiceName(_serviceName);
}

bool QServiceController::isInstalled() const
{
   return _osData->isInstalled();
}

ServiceState QServiceController::status() const
{
   return _osData->status();
}

QList<QServiceController::ServiceInfo> QServiceController::installedServices() const
{
   return _osData->installedServices();
}

bool QServiceController::install(const ServiceParameter &parameter)
{
   if (parameter.commandLine.isEmpty())
      return false;

   return _osData->installService(parameter);
}

bool QServiceController::setFailureActions(const QServiceFailureActions &failureActions)
{
   return _osData->setFailureActions(failureActions);
}

bool QServiceController::uninstall()
{
   return _osData->uninstall();
}

bool QServiceController::start(const QStringList &args)
{
   return _osData->start(args);
}

bool QServiceController::stop()
{
   return _osData->stop();
}

bool QServiceController::pause()
{
   return _osData->pause();
}

bool QServiceController::resume()
{
   return _osData->resume();
}

bool QServiceController::sendCommand(int code)
{
   return _osData->sendCommand(code);
}

bool QServiceController::waitForStopped()
{
   return _osData->waitForStopped();
}

QString QServiceController::lastErrorText() const
{
   if (_osData->lastErrorCode() == ERROR_SUCCESS)
      return tr("Service Control Manager error: Unknown");

   return tr("Service Control Manager error (%1): %2").arg(_osData->lastErrorCode()).arg(qt_error_string(_osData->lastErrorCode()));
}
