#ifndef QSERVICEPROXY_H
#define QSERVICEPROXY_H

#include <QObject>
#include "QServiceBase.h"

class QServiceController;

// QServiceProxy must be instantiated in the thread qApp is running in (not in the service thread!)
class QServiceProxy : public QObject, public QServiceAuthority
{
   Q_OBJECT

public:
   QServiceProxy(QServiceFactory *factory, QObject *parent = 0);
   virtual ~QServiceProxy();

   QList<ArgDef> initArguments() const { return _factory->initArguments(); }
   QList<ArgDef> argumentDefinitions(ServiceTask task) const { return _factory->argumentDefinitions(task); }

   bool init(const QMultiMap<QString, QString>& args) { return _factory->init(args); }

   QString version() const { return _factory->version(); }

   bool install(QServiceController *controller, const QString& program, const QMultiMap<QString, QString>& args, bool shared) const { return _factory->install(controller, program, args, shared); }
   bool start(QServiceController* controller, const QMultiMap<QString, QString>& args) const { return _factory->start(controller, args); }

   bool registerServicHandler(bool shared);

   int   run(const QMultiMap<QString, QString>& args); // Called from a different thread and does not return until the service stopped
   void  stop(); // Must be called by the same thread that created this instance!

   QEventLog *eventLog() { return _factory->eventLog(); }

public:
   // Service authority interface
   QString serviceName() const override { return _factory->serviceName(); }
   bool reportStatus(ServiceState status, int waitHint = 0) override;
   ServiceState status() const override;
   void setAcceptPauseAndContinue(bool accept = true) override;

signals:
   void stopRequest();
   void pauseRequest();
   void resumeRequest();
   void commandRequest(int code);

private:
   class OSData;

private:
   void stopAsync(QServiceBase *service);
   void pauseAsync(QServiceBase *service);
   void resumeAsync(QServiceBase *service);
   void processCommandAsync(QServiceBase *service, int code);

private:
   QServiceFactory   *_factory;
   //QString     _serviceName;
   OSData            *_osData;
};

#endif // QSERVICEPROXY_H
