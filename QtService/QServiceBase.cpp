#include "QServiceBase.h"
#include "QServiceController.h"
#include <QEventLoop>

QServiceBase::QServiceBase(QServiceAuthority* authority, QEventLog* eventLog, QObject *parent)
   : QObject(parent), _authority(authority), _eventLog(eventLog)
{
}

QServiceBase::~QServiceBase()
{
}

void QServiceBase::start()
{
   if (_authority->reportStatus(ServiceStartPending, 3000))
   {
      Q_ASSERT(_eventLoop == nullptr);

      _eventLoop = new QEventLoop(this);

      _serviceCommand = CMD_Start;

      while (_serviceCommand != CMD_Stop && _serviceCommand != CMD_Abort)
         run();

      delete _eventLoop;
      _eventLoop = nullptr;
   }
}

void QServiceBase::stop()
{
   _eventLog->reportEvent(tr("Service shutting down..."), QEventLog::S_Information);
   _serviceCommand = CMD_Stop;
   exit();
}

void QServiceBase::pause()
{
   if (!_authority->reportStatus(ServicePaused))
   {
      stop();
   }
   else
   {
      _serviceCommand = CMD_Pause;
   }
}

void QServiceBase::resume()
{
   if (!_authority->reportStatus(ServiceRunning))
   {
      stop();
   }
   else
   {
      _serviceCommand = CMD_Resume;
   }
}

void QServiceBase::processCommand(int code)
{
   // _serviceCommand = CMD_...
   Q_UNUSED(code);
}

void QServiceBase::abort()
{
   _eventLog->reportEvent(tr("Service aborting..."), QEventLog::S_Warning);
   _serviceCommand = CMD_Abort;
   exit();
}

void QServiceBase::run()
{
   exec();
}

int QServiceBase::exec()
{
   int result = 0;

   if (_eventLoop)
   {
      _isEventDriven = true;
      if (_authority->reportStatus(ServiceRunning))
      {
         result = _eventLoop->exec(); // Doesn't return until the even loop is terminated
      }
      _isEventDriven = false;
   }

   return result;
}

void QServiceBase::exit(int returnCode)
{
   if (_eventLoop && _eventLoop->isRunning())
      _eventLoop->exit(returnCode);
}


QList<ArgDef> QServiceFactory::argumentDefinitions(ServiceTask task) const
{
   auto argDefs = initArguments();

   if (task == T_Install)
   {
      ArgDef argDef;

      argDef._valueName = "account";
      argDef._description = QServiceBase::tr("Service account");
      argDefs.append(argDef);
      argDef._valueName = "password";
      argDef._description = QServiceBase::tr("Service account password");
      argDefs.append(argDef);
   }

   return argDefs;
}

bool QServiceFactory::install(QServiceController *controller, const QString& program, const QMultiMap<QString, QString>& args, bool shared) const
{
   QServiceController::ServiceParameter parameter;

   parameter.commandLine = program;
   parameter.description = description();
   parameter.accountName = args.value("account");
   parameter.accountPassword = args.value("password");
   parameter.autostart = false;
   parameter.shared = shared;

   return controller->install(parameter);
}

bool QServiceFactory::start(QServiceController* controller, const QMultiMap<QString, QString>& /*args*/) const
{
   return controller->start();
}

void QServiceFactory::setServiceName(const QString &serviceName)
{
   _serviceName = serviceName;
   if (_serviceName.contains(QLatin1Char('\\')))
   {
      qWarning("QtService: 'name' contains backslashes '\\'.");
      _serviceName.replace(QLatin1Char('\\'), QLatin1Char('_'));
   }

   if (_serviceName.contains(QLatin1Char('/')))
   {
      qWarning("QtService: 'name' contains slashes '/'.");
      _serviceName.replace(QLatin1Char('/'), QLatin1Char('_'));
   }

   if (_serviceName.length() > 255)
   {
      qWarning("QServiceBase: 'name' is longer than 255 characters.");
      _serviceName.truncate(255);
   }
}
