HEADERS += $$PWD/QServiceProxy.h \
    $$PWD/include/qtservice_global.h \
    $$PWD/include/ServiceTools.h \
    $$PWD/include/QServiceManager.h \
    $$PWD/include/QServiceController.h \
    $$PWD/include/QServiceBase.h
SOURCES += $$PWD/QServiceBase.cpp \
    $$PWD/QServiceManager.cpp \
    $$PWD/QServiceProxy.cpp \
    $$PWD/ServiceTools.cpp
win32 {
SOURCES += $$PWD/QServiceController_win.cpp
} else {
SOURCES += $$PWD/QServiceController_unix.cpp
}
