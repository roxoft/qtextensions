#include "QServiceController.h"
#include <QSettings>
#include <QFile>
#include <QProcess>
#include <sys/types.h>
#include <unistd.h>
#include <dirent.h>
#include <errno.h>
#include <signal.h>

static QString serviceFilePath(const QString &serviceName)
{
   QSettings settings(QSettings::SystemScope, "QtSoftware");

   settings.beginGroup("services");
   settings.beginGroup(serviceName);

   QString path = settings.value("path").toString();

   settings.endGroup();
   settings.endGroup();

   return path;
}

static QString serviceDescription(const QString &serviceName)
{
   QSettings settings(QSettings::SystemScope, "QtSoftware");

   settings.beginGroup("services");
   settings.beginGroup(serviceName);

   QString desc = settings.value("description").toString();

   settings.endGroup();
   settings.endGroup();

   return desc;
}

static bool serviceIsAutostart(const QString &serviceName)
{
    QSettings settings(QSettings::SystemScope, "QtSoftware");

    settings.beginGroup("services");
    settings.beginGroup(serviceName);

    bool isAutostart = settings.value("startupType").toInt() != 0;

    settings.endGroup();
    settings.endGroup();

    return isAutostart;
}

static int getFirstPid(const QString &name)
{
   int ownPid = getpid();
   int pid = -1;

   if (name.isEmpty())
      return pid;

   // Open the /proc directory
   DIR *dp = opendir("/proc");

   if (dp != NULL)
   {
      // Enumerate all entries in directory until process found
      struct dirent *dirp;

      while (pid < 0 && (dirp = readdir(dp)))
      {
         // Skip non-numeric entries
         int id = atoi(dirp->d_name);

         if (id > 0 && id != ownPid)
         {
            // Read contents of virtual /proc/{pid}/cmdline file
            QString cmdPath = QString::fromLatin1("/proc/%1/cmdline").arg(QFile::decodeName(dirp->d_name));

            QFile cmdFile(cmdPath);

            if (cmdFile.open(QIODevice::ReadOnly))
            {
               QByteArray cmdLine = cmdFile.readLine().trimmed();

               if (!cmdLine.isEmpty())
               {
                  // Compare against requested process name
                  if (cmdLine.length() >= name.length() && cmdLine.right(name.length()) == name)
                     pid = id;
               }
            }
         }
      }
   }

   closedir(dp);

   return pid;
}


QServiceController::QServiceController(QObject *parent) : QObject(parent)
{
}

QServiceController::QServiceController(const QString &name, QObject *parent) : QObject(parent), _serviceName(name)
{
}

QServiceController::~QServiceController()
{
}

void QServiceController::setServiceName(const QString &serviceName)
{
   _serviceName = serviceName;
}

bool QServiceController::isInstalled() const
{
   QSettings settings(QSettings::SystemScope, "QtSoftware");

   settings.beginGroup("services");

   QStringList list = settings.childGroups();

   settings.endGroup();

   QStringListIterator it(list);

   while (it.hasNext())
   {
       if (it.next() == _serviceName)
           return true;
   }

   return false;
}

ServiceState QServiceController::status() const
{
   return (getFirstPid(serviceFilePath(_serviceName)) > 0) ? ServiceRunning : ServiceStopped;
}

QList<QServiceController::ServiceInfo> QServiceController::installedServices() const
{
   return QList<QServiceController::ServiceInfo>();
}

bool QServiceController::install(const ServiceParameter &parameter)
{
   if (parameter.commandLine.isEmpty())
      return false;

   QSettings settings(QSettings::SystemScope, "QtSoftware");

   settings.beginGroup("services");
   settings.beginGroup(_serviceName);

   settings.setValue("path", parameter.commandLine);
   settings.setValue("description", parameter.description);
   settings.setValue("automaticStartup", parameter.autostart ? 0 : 1);

   settings.endGroup();
   settings.endGroup();
   settings.sync();

   QSettings::Status ret = settings.status();

   if (ret == QSettings::AccessError)
   {
//          fprintf(stderr, "Cannot install \"%s\". Cannot write to: %s. Check permissions.\n",
//                  controller.serviceName().toLatin1().constData(),
//                  settings.fileName().toLatin1().constData());
   }
   return (ret == QSettings::NoError);
}

bool QServiceController::uninstall()
{
   QSettings settings(QSettings::SystemScope, "QtSoftware");

   settings.beginGroup("services");

   settings.remove(_serviceName);

   settings.endGroup();
   settings.sync();

   QSettings::Status ret = settings.status();

   if (ret == QSettings::AccessError)
   {
//             fprintf(stderr, "Cannot uninstall \"%s\". Cannot write to: %s. Check permissions.\n",
//                     serviceName().toLatin1().constData(),
//                     settings.fileName().toLatin1().constData());
   }
   return (ret == QSettings::NoError);
}

bool QServiceController::start(const QStringList &args)
{
   QString path = serviceFilePath(_serviceName);

   if (getFirstPid(path) > 0)
      return true;

   return QProcess::startDetached(path, args);
}

bool QServiceController::stop()
{
   int pid = getFirstPid(serviceFilePath(_serviceName));

   if (pid > 0)
      return kill(pid, SIGTERM) == 0;

   return false;
}

bool QServiceController::pause()
{
   return false;
}

bool QServiceController::resume()
{
   return false;
}

bool QServiceController::sendCommand(int code)
{
   if (code < 0 || code > 127)
      return false;

   return false;
}

bool QServiceController::waitForStopped()
{
   return false;
}

QString QServiceController::lastErrorText() const
{
   return tr("Unknown");
}
