#ifndef SERVICETOOLS_H
#define SERVICETOOLS_H

#include "qtservice_global.h"
#include <QList>
#include <QMutex>
#include <FunctionStatusBase.h>

class QDateTime;

enum ServiceTask { T_Run, T_Debug, T_Install, T_Uninstall, T_Status, T_Stop, T_Pause, T_Resume, T_Command, T_Help };

enum ServiceState { ServiceStopped, ServiceStartPending, ServiceRunning, ServicePausePending, ServicePaused, ServiceContinuePending, ServiceStopPending };

class QTSERVICE_EXPORT QEventLog
{
public:
   enum Severity { S_Debug, S_Information, S_Success, S_Warning, S_Error };

public:
   class QTSERVICE_EXPORT Logger
   {
      friend class QEventLog;

   public:
      Logger(QEventLog *parentEventLog = nullptr); // If a parentEventLog is given the logger will be destroyed when it is removed from the last event log
      virtual ~Logger();

      virtual void reportEvent(const QString &message, Severity severity, const QDateTime &timestamp) = 0; // Lock the _mutex before reporting an event!

      virtual bool transferHistory(Logger *logger) const;

   protected:
      mutable QMutex _mutex;

   private:
      void addParent(QEventLog *eventLog);
      void removeParent(QEventLog *eventLog);

   private:
      QList<QEventLog *>   _parents;
      bool                 _destroy;
   };

public:
   QEventLog();
   QEventLog(const QEventLog &other);
   ~QEventLog();

   QEventLog &operator=(const QEventLog &rhs);

   bool isEmpty() const { return _logger.isEmpty(); }

   Severity logLevel() const { return _logLevel; }
   void setLogLevel(Severity logLevel) { _logLevel = logLevel; }

   void reportEvent(const QString &message, Severity severity) const;

   void appendLog(Logger *logger);
   void removeLog(Logger *logger); // Destroys the logger if its controlled by QEventLogs

   void transferHistory(Logger *logger) const;

private:
   mutable QMutex    _mutex;
   QList<Logger *>   _logger;
   Severity          _logLevel;
};

class QTSERVICE_EXPORT SystemEventLog : public QEventLog::Logger
{
public:
   SystemEventLog(const QString &serviceName = QString(), bool essential = false, QEventLog* parentEventLog = nullptr);
   virtual ~SystemEventLog();

   bool registerSource(const QString &serviceName);

   void reportEvent(const QString &message, QEventLog::Severity severity, const QDateTime &timestamp) override;

private:
   class Data;

private:
   Data *_data;
   bool  _essential;
};

class QTSERVICE_EXPORT ConsoleEventLog : public QEventLog::Logger
{
public:
   ConsoleEventLog(bool essential = false, QEventLog* parentEventLog = nullptr) : Logger(parentEventLog), _essential(essential) {}
   virtual ~ConsoleEventLog() {}

   void reportEvent(const QString &message, QEventLog::Severity severity, const QDateTime &timestamp) override;

private:
   bool _essential;
};

class QTSERVICE_EXPORT ServiceFunctionStatus : public FunctionStatusBase
{
   Q_DISABLE_COPY(ServiceFunctionStatus)

public:
   ServiceFunctionStatus(QEventLog* eventLog, const QString& messageContextSeparator = ": ") : _eventLog(eventLog), _messageContextSeparator(messageContextSeparator) {}
   ~ServiceFunctionStatus() override = default;

   void add(const StatusMessage& statusMessage) override;

private:
   QEventLog* _eventLog = nullptr;
   QString _messageContextSeparator;
};

#endif // SERVICETOOLS_H
