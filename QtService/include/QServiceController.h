#ifndef QSERVICECONTROLLER_H
#define QSERVICECONTROLLER_H

#include "qtservice_global.h"
#include "ServiceTools.h"
#include <QObject>
#include <QString>
#include <QStringList>

class QServiceFailureActions
{
public:
   enum Type { AT_None, AT_Reboot, AT_Restart, AT_RunCommand };

   struct Action
   {
      Action() : type(AT_None), delay(0) {}
      Action(Type t, int d) : type(t), delay(d) {}

      Type  type;
      int   delay; // Milliseconds
   };

public:
   QServiceFailureActions() : resetPeriod(-1), rebootMessage(QLatin1String("*")), commandToRun(QLatin1String("*")) {}
   ~QServiceFailureActions() {}

   int            resetPeriod; // Seconds
   QString        rebootMessage;
   QString        commandToRun;
   QList<Action>  actions;

   void addAction(Type type, int delay) { actions.append(Action(type, delay)); }
};

class QTSERVICE_EXPORT QServiceController : public QObject
{
   Q_OBJECT
   Q_DISABLE_COPY(QServiceController)

public:
   struct ServiceParameter
   {
      ServiceParameter() : autostart(false), shared(false) {}

      QString     commandLine;
      QString     description;
      QString     accountName;
      QString     accountPassword;
      bool        autostart;
      bool        shared;
      QStringList dependencies;
   };

   struct ServiceInfo
   {
      ServiceInfo() : shared(false), status(ServiceStopped) {}

      QString        name;
      QString        displayName;
      bool           shared;
      ServiceState   status;
   };

public:
   QServiceController(QObject *parent = 0);
   QServiceController(const QString &name, QObject *parent = 0);
   virtual ~QServiceController();

   const QString  &serviceName() const { return _serviceName; }
   void           setServiceName(const QString &serviceName);

   bool           isInstalled() const;
   ServiceState   status() const;

   QList<ServiceInfo> installedServices() const;

   bool install(const ServiceParameter &parameter);
   bool setFailureActions(const QServiceFailureActions &failureActions);
   bool uninstall();
   bool start(const QStringList &args = QStringList());
   bool stop();
   bool pause();
   bool resume();
   bool sendCommand(int code);

   bool waitForStopped();

   QString lastErrorText() const;

private:
   class OsData;

private:
   QString _serviceName;
   OsData* _osData = nullptr;
};

#endif // QSERVICECONTROLLER_H
