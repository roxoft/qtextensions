#ifndef QSERVICEMANAGER_H
#define QSERVICEMANAGER_H

#include "qtservice_global.h"
#include "ServiceTools.h"
#include <QList>
#include <QStringList>
#include <QMap>

QT_BEGIN_NAMESPACE
   class QCoreApplication;
QT_END_NAMESPACE
class QServiceFactory;
class QServiceProxy;

class QTSERVICE_EXPORT QServiceManager
{
   Q_DECLARE_TR_FUNCTIONS(QServiceManager)
   Q_DISABLE_COPY(QServiceManager)

public:
   QServiceManager(int& argc, char** argv);
   virtual ~QServiceManager();

   const QString& applicationFilePath() const { return _filePath; }

   void addService(QServiceFactory *service);

   int exec();

   const QList<QServiceProxy *>& serviceList() const { return _serviceList; }

   const QMultiMap<QString, QString>& serviceArgs() const { return _serviceArgs; };

   const QStringList& args() const { return _args; }

   QEventLog *eventLog() const;

   void serviceIsStopping(QServiceProxy *service);

public:
   static QServiceManager *instance;

private:
   class OSData;

private:
   void stopServices();
   QStringList helpMessage(ServiceTask task) const;

private:
   QCoreApplication* _app = nullptr;
   QList<QServiceProxy *> _serviceList;
   QStringList _args;
   QString _filePath;
   QString _taskArg;
   ServiceTask _task = T_Run;
   QMultiMap<QString, QString> _serviceArgs;
   QEventLog* _eventLog = nullptr;
   OSData* _osData = nullptr;
};

#endif // QSERVICEMANAGER_H
