#ifndef QSERVICEBASE_H
#define QSERVICEBASE_H

#include "qtservice_global.h"
#include "ServiceTools.h"
#include <ConsoleTools.h>
#include <QDir>
#include <QMap>

QT_BEGIN_NAMESPACE
   class QEventLoop;
QT_END_NAMESPACE

class QServiceController;

class QServiceAuthority
{
public:
   virtual QString serviceName() const = 0;
   virtual bool reportStatus(ServiceState status, int waitHint = 0) = 0;
   virtual ServiceState status() const = 0;
   virtual void setAcceptPauseAndContinue(bool accept = true) = 0;
};

/*
 Base class for a service.

 Override run() and inform the authority of the service state.
  
 TODO: Add a target state (Run, Stop, Pause).
*/
class QTSERVICE_EXPORT QServiceBase : public QObject
{
   Q_OBJECT

public:
   QServiceBase(QServiceAuthority* authority, QEventLog* eventLog, QObject *parent = 0);
   virtual ~QServiceBase();

   QServiceAuthority *authority() const { return _authority; }

   QEventLog* eventLog() const { return _eventLog; }

   virtual void start(); // Establishes the message loop and calls run (does not return until the service stopped)

   bool isEventDriven() const { return _isEventDriven; }

public slots:
   virtual void stop(); // The service should terminate gracefully (the default implementation calls exit())
   virtual void pause(); // The service should suspend its tasks
   virtual void resume(); // The service should resume its tasks
   virtual void processCommand(int code);
   virtual void abort(); // The service should terminate immediately

protected:
   enum ServiceCommand { CMD_None, CMD_Start, CMD_Pause, CMD_Resume, CMD_Stop, CMD_Abort };

protected:
   virtual void run(); // If run() exits the service terminates

   int   exec(); // Executes the event loop
   void  exit(int returnCode = 0); // Terminates the event loop

protected:
   QServiceAuthority *_authority = nullptr;
   QEventLog         *_eventLog = nullptr;
   QEventLoop        *_eventLoop = nullptr;
   bool              _isEventDriven = false;
   ServiceCommand    _serviceCommand = CMD_None;
};

class QTSERVICE_EXPORT QServiceFactory
{
public:
   QServiceFactory() : _eventLog(new QEventLog) {}
   virtual ~QServiceFactory() { delete _eventLog; }

   virtual QList<ArgDef> initArguments() const = 0;
   virtual QList<ArgDef> argumentDefinitions(ServiceTask task) const; // includes initArguments()

   virtual bool init(const QMultiMap<QString, QString>& args) = 0; // Must call setServiceName()

   virtual QString version() const = 0;

   virtual QString description() const { return QString(); }

   virtual QServiceBase* createService(QServiceAuthority* authority, const QMultiMap<QString, QString>& args) const = 0; // Creates a service instance to be used in run()

   virtual bool install(QServiceController* controller, const QString& program, const QMultiMap<QString, QString>& args, bool shared) const;
   virtual bool start(QServiceController* controller, const QMultiMap<QString, QString>& args) const; // Start the service via SCM

   QEventLog *eventLog() const { return _eventLog; }

   const QString &serviceName() const { return _serviceName; }

protected:
   void setServiceName(const QString &serviceName);

private:
   QEventLog*  _eventLog = nullptr;
   QString     _serviceName;
};

#endif // QSERVICEBASE_H
