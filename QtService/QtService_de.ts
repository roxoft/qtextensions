<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE">
<context>
    <name>QServiceBase</name>
    <message>
        <location filename="QServiceBase.cpp" line="34"/>
        <source>Service shutting down...</source>
        <translation>Dienst wird angehalten...</translation>
    </message>
    <message>
        <location filename="QServiceBase.cpp" line="71"/>
        <source>Service aborting...</source>
        <translation>Dienst wird abgebrochen...</translation>
    </message>
    <message>
        <location filename="QServiceBase.cpp" line="114"/>
        <source>Service account</source>
        <translation>Konto für den Dienst</translation>
    </message>
    <message>
        <location filename="QServiceBase.cpp" line="117"/>
        <source>Service account password</source>
        <translation>Passwort für das Dienstekonto</translation>
    </message>
</context>
<context>
    <name>QServiceController</name>
    <message>
        <location filename="QServiceController_win.cpp" line="832"/>
        <source>Service Control Manager error (%1): %2</source>
        <translation>Service Control Manager Fehler (%1): %2</translation>
    </message>
    <message>
        <location filename="QServiceController_win.cpp" line="830"/>
        <source>Service Control Manager error: Unknown</source>
        <translation>Service Control Manager Fehler: Unbekannt</translation>
    </message>
    <message>
        <location filename="QServiceController_unix.cpp" line="249"/>
        <source>Unknown</source>
        <translation>Unbekannt</translation>
    </message>
</context>
<context>
    <name>QServiceManager</name>
    <message>
        <location filename="QServiceManager.cpp" line="126"/>
        <source>Received start request for &quot;%1&quot;.</source>
        <translation>Startanforderung von &quot;%1&quot; erhalten.</translation>
    </message>
    <message>
        <location filename="QServiceManager.cpp" line="139"/>
        <source>Registering the service at the Service Control Manager.</source>
        <translation>Registriere den Dienst beim Service Control Manager.</translation>
    </message>
    <message>
        <location filename="QServiceManager.cpp" line="156"/>
        <source>&quot;%1&quot; parameter error: %2.</source>
        <translation>&quot;%1&quot; Parameter Fehler: %2.</translation>
    </message>
    <message>
        <location filename="QServiceManager.cpp" line="164"/>
        <source>Registering failed.</source>
        <translation>Registrierung fehlgeschlagen.</translation>
    </message>
    <message>
        <location filename="QServiceManager.cpp" line="170"/>
        <source>Service %1 not found.</source>
        <translation>Dienst %1 nicht gefunden.</translation>
    </message>
    <message>
        <location filename="QServiceManager.cpp" line="176"/>
        <source>Missing service name.</source>
        <translation>Name des Dienstes fehlt.</translation>
    </message>
    <message>
        <location filename="QServiceManager.cpp" line="198"/>
        <source>Running the Windows Service Control Dispatcher</source>
        <translation>Starte Windows Service Control Dispatcher</translation>
    </message>
    <message>
        <location filename="QServiceManager.cpp" line="208"/>
        <source>The service could not connect to the service controller.</source>
        <translation>Der Dienst konnte sich nicht mit dem Service-Controller verbinden.</translation>
    </message>
    <message>
        <location filename="QServiceManager.cpp" line="213"/>
        <source>The service could not be started [%1]</source>
        <translation>Der Dienst konnte nicht gestartet werden [%1]</translation>
    </message>
    <message>
        <location filename="QServiceManager.cpp" line="221"/>
        <source>Service list is empty.</source>
        <translation>Die Liste der Dienste ist leer.</translation>
    </message>
    <message>
        <location filename="QServiceManager.cpp" line="374"/>
        <source>Service could not be initialized.</source>
        <translation>Der Dienst konnte nicht initialisiert werden.</translation>
    </message>
    <message>
        <location filename="QServiceManager.cpp" line="394"/>
        <location filename="QServiceManager.cpp" line="423"/>
        <source>Missing service start parameter.</source>
        <translation>Fehlender Startparameter.</translation>
    </message>
    <message>
        <location filename="QServiceManager.cpp" line="497"/>
        <location filename="QServiceManager.cpp" line="552"/>
        <source>Service thread could not be started.</source>
        <translation>Der Dienst-Thread konnte nicht gestartet werden.</translation>
    </message>
    <message>
        <location filename="QServiceManager.cpp" line="504"/>
        <location filename="QServiceManager.cpp" line="537"/>
        <location filename="QServiceManager.cpp" line="559"/>
        <source>No service.</source>
        <translation>Kein Dienst.</translation>
    </message>
    <message>
        <location filename="QServiceManager.cpp" line="523"/>
        <source>ConsoleEventHandler could not be installed.</source>
        <translation>Der ConsoleEventHandler konnte nicht installiert werden.</translation>
    </message>
    <message>
        <location filename="QServiceManager.cpp" line="586"/>
        <source>The service %1 is installed under: %2.</source>
        <translation>Der Dienst %1 wurde unter %2 installiert.</translation>
    </message>
    <message>
        <location filename="QServiceManager.cpp" line="591"/>
        <source>The service %1 could not be installed.</source>
        <translation>Der Dienst %1 konnte nicht installiert werden.</translation>
    </message>
    <message>
        <location filename="QServiceManager.cpp" line="602"/>
        <source>The service %1 is uninstalled.</source>
        <translation>Der Dienst %1 wurde deinstalliert.</translation>
    </message>
    <message>
        <location filename="QServiceManager.cpp" line="607"/>
        <source>The service %1 could not be uninstalled.</source>
        <translation>Der Dienst %1 konnte nicht deinstalliert werden.</translation>
    </message>
    <message>
        <location filename="QServiceManager.cpp" line="612"/>
        <source>The service %1 is not installed.</source>
        <translation>Der Dienst %1 ist nicht installiert.</translation>
    </message>
    <message>
        <location filename="QServiceManager.cpp" line="616"/>
        <source>The service %1 (%2) is %3 and %4.</source>
        <translation>Der Dienst %1 (%2) ist %3 und %4.</translation>
    </message>
    <message>
        <location filename="QServiceManager.cpp" line="616"/>
        <source>installed</source>
        <translation>installiert</translation>
    </message>
    <message>
        <location filename="QServiceManager.cpp" line="616"/>
        <source>not installed</source>
        <translation>nicht installiert</translation>
    </message>
    <message>
        <location filename="QServiceManager.cpp" line="616"/>
        <source>running</source>
        <translation>läuft</translation>
    </message>
    <message>
        <location filename="QServiceManager.cpp" line="616"/>
        <source>not running</source>
        <translation>läuft nicht</translation>
    </message>
    <message>
        <location filename="QServiceManager.cpp" line="627"/>
        <source>The service could not be started.</source>
        <translation>Der Dienst konnte nicht gestartet werden.</translation>
    </message>
    <message>
        <location filename="QServiceManager.cpp" line="622"/>
        <source>The service is started.</source>
        <translation>Der Dienst wurde gestartet.</translation>
    </message>
    <message>
        <location filename="QServiceManager.cpp" line="636"/>
        <source>The service could not be stopped.</source>
        <translation>Der Dienst konnte nicht angehalten werden.</translation>
    </message>
    <message>
        <location filename="QServiceManager.cpp" line="642"/>
        <source>The service does not respond.</source>
        <translation>Der Dienst reagiert nicht.</translation>
    </message>
    <message>
        <location filename="QServiceManager.cpp" line="647"/>
        <source>The service is stopped.</source>
        <translation>Der Dienst wurde angehalten.</translation>
    </message>
    <message>
        <location filename="QServiceManager.cpp" line="659"/>
        <source>The service could not be paused.</source>
        <translation>Der Dienst konnte nicht pausiert werden.</translation>
    </message>
    <message>
        <location filename="QServiceManager.cpp" line="654"/>
        <source>The service is paused.</source>
        <translation>Der Dienst ist pausiert.</translation>
    </message>
    <message>
        <location filename="QServiceManager.cpp" line="672"/>
        <source>The service could not be resumed.</source>
        <translation>Der Dienst konnte nicht wiederaufgenommen werden.</translation>
    </message>
    <message>
        <location filename="QServiceManager.cpp" line="667"/>
        <source>The service is resumed.</source>
        <translation>Dienst wieder aufgenommen.</translation>
    </message>
    <message>
        <location filename="QServiceManager.cpp" line="727"/>
        <source>Service thread could not be terminated.</source>
        <translation>Der Dienste-Thread konnte nicht beendet werden.</translation>
    </message>
    <message>
        <location filename="QServiceManager.cpp" line="847"/>
        <source> [-h | task parameters]</source>
        <translation> [-h | Aufgabenparameter]</translation>
    </message>
    <message>
        <location filename="QServiceManager.cpp" line="851"/>
        <source>Syntax:</source>
        <translation>Syntax:</translation>
    </message>
    <message>
        <location filename="QServiceManager.cpp" line="854"/>
        <source>Parameters:</source>
        <translation>Parameter:</translation>
    </message>
    <message>
        <location filename="QServiceManager.cpp" line="858"/>
        <source>	-i(nstall)	: Install the service</source>
        <translation>	-i(nstall)	: Installiere den Dienst</translation>
    </message>
    <message>
        <location filename="QServiceManager.cpp" line="859"/>
        <source>	-u(ninstall)	: Uninstall the service.</source>
        <translation>	-u(ninstall)	: Deinstalliere den Dienst.</translation>
    </message>
    <message>
        <location filename="QServiceManager.cpp" line="860"/>
        <source>	-e(xec)		: Run as a regular application. Useful for debugging.</source>
        <translation>	-e(xec)		: Als Anwendung ausführen. Nützlich zum Debuggen.</translation>
    </message>
    <message>
        <location filename="QServiceManager.cpp" line="861"/>
        <source>	-t(erminate)	: Stop the service.</source>
        <translation>	-t(erminate)	: Beende den Dienst.</translation>
    </message>
    <message>
        <location filename="QServiceManager.cpp" line="863"/>
        <source>	-v(ersion)	: Print version and status information.</source>
        <translation>	-v(ersion)	: Version und Status ausgeben.</translation>
    </message>
    <message>
        <location filename="QServiceManager.cpp" line="864"/>
        <source>	-h(elp)		: Show this help</source>
        <translation>	-h(elp)		: Zeige diese Hilfe</translation>
    </message>
    <message>
        <location filename="QServiceManager.cpp" line="866"/>
        <source>	-s(tart)	: Start the service via SCM.</source>
        <translation>	-s(tart)	: Starte den Dienst über SCM.</translation>
    </message>
    <message>
        <location filename="QServiceManager.cpp" line="868"/>
        <source>	-d(aemon)	: Start the service as daemon.</source>
        <translation>	-d(aemon)	: Starte den Dienst als Daemon.</translation>
    </message>
    <message>
        <location filename="QServiceManager.cpp" line="916"/>
        <source>	task parameters	: Additional arguments for the task.</source>
        <translation>	Aufgabenparameter: Zusätzliche Parameter für die Aufgabe.</translation>
    </message>
</context>
<context>
    <name>QServiceProxy</name>
    <message>
        <location filename="QServiceProxy.cpp" line="344"/>
        <source>Starting the service.</source>
        <translation>Starte den Dienst.</translation>
    </message>
</context>
</TS>
