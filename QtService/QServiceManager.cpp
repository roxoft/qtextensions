#include "QServiceManager.h"
#include "QServiceController.h"
#include "QServiceProxy.h"
#include <QCoreApplication>
#include <QFileInfo>
#include <ConsoleTools.h>
#if QT_VERSION >= QT_VERSION_CHECK(5,0,0)
#include <QAbstractNativeEventFilter>
#endif

static int explicitServiceRunner(QServiceProxy *service)
{
   int retCode = service->run(QServiceManager::instance->serviceArgs());

   QServiceManager::instance->serviceIsStopping(service);

   return retCode;
}

#if defined(Q_OS_UNIX)
   #include <sys/types.h>
   #include <unistd.h>
   #include <sys/stat.h>
   #include <signal.h>
   #include <pthread.h>

   class QServiceManager::OSData
   {
   public:
      OSData() {}
      ~OSData() {}

      QList<pthread_t>  _explicitThreadList;
   };

   static void *explicitServiceThread(void *parameter)
   {
      return (void *)(long long)explicitServiceRunner((QServiceProxy *)parameter);
   }

#elif defined(Q_OS_WIN)
   #define WIN32_LEAN_AND_MEAN
   #include <Windows.h>

#if QT_VERSION >= QT_VERSION_CHECK(5,0,0)
   static class MyEventFilter : public QAbstractNativeEventFilter
   {
   public:
      MyEventFilter() {}
      virtual ~MyEventFilter() {}

#if QT_VERSION_MAJOR < 6
      bool nativeEventFilter(const QByteArray &eventType, void *message, long *result) override;
#else
    bool nativeEventFilter(const QByteArray &eventType, void *message, qintptr *result) override;
#endif

   } myEventFilter;

   /*
     Ignore WM_ENDSESSION system events, since they make the Qt kernel quit
   */
#if QT_VERSION_MAJOR < 6
   bool MyEventFilter::nativeEventFilter(const QByteArray &, void *message, long *result)
#else
   bool MyEventFilter::nativeEventFilter(const QByteArray &, void *message, qintptr *result)
#endif
   {
      MSG* msg = reinterpret_cast<MSG*>(message);

      if (!msg || (msg->message != WM_ENDSESSION) || !(msg->lParam & ENDSESSION_LOGOFF))
         return false;

      if (result)
         *result = TRUE;

      return true;
   }
#else
   static QCoreApplication::EventFilter nextFilter = nullptr;

   /*
     Ignore WM_ENDSESSION system events, since they make the Qt kernel quit
   */
   static bool myEventFilter(void* message, long* result)
   {
      MSG* msg = reinterpret_cast<MSG*>(message);

      if (!msg || (msg->message != WM_ENDSESSION) || !(msg->lParam & ENDSESSION_LOGOFF))
         return nextFilter ? nextFilter(message, result) : false;

      if (nextFilter)
         nextFilter(message, result);
      if (result)
         *result = TRUE;

      return true;
   }
#endif

   class QServiceManager::OSData
   {
   public:
      OSData() {}
      ~OSData() { foreach (HANDLE hThread, _explicitThreadHandleList) CloseHandle(hThread); }

      QList<HANDLE> _explicitThreadHandleList;
   };

   static void WINAPI serviceMain(DWORD dwArgc, LPWSTR* wszArgv)
   {
      QStringList args;

      for (DWORD i = 0; i < dwArgc; ++i)
         args.append(QString((const QChar *)wszArgv[i]));

      // The first argument is the service name.
      // The following arguments are passed from ServiceStart (optional arguments on the properties page of the SCM).

      if (!args.isEmpty())
      {
         const auto serviceName = args.first();

         args.removeFirst();

         QServiceManager::instance->eventLog()->reportEvent(QServiceManager::tr("Received start request for \"%1\".").arg(serviceName), QEventLog::S_Information);

         QServiceProxy *service = nullptr;

         foreach (service, QServiceManager::instance->serviceList())
         {
            if (service->serviceName() == serviceName)
               break;
            service = nullptr;
         }

         if (service)
         {
            QServiceManager::instance->eventLog()->reportEvent(QServiceManager::tr("Registering the service at the Service Control Manager."), QEventLog::S_Debug);

            if (service->registerServicHandler(QServiceManager::instance->serviceList().count() > 1))
            {
               service->reportStatus(ServiceStartPending, 3000);

               QMultiMap<QString, QString> serviceArgs;
               auto errors = appArguments(service->argumentDefinitions(T_Run), QServiceManager::instance->args() + args, serviceArgs);

               if (errors.isEmpty())
               {
                  service->run(serviceArgs);
               }
               else
               {
                  for (auto&& error : errors)
                  {
                     QServiceManager::instance->eventLog()->reportEvent(QServiceManager::tr("\"%1\" parameter error: %2.").arg(serviceName).arg(error), QEventLog::S_Error);
                  }
               }

               service->reportStatus(ServiceStopped); // That must be the last command in the thread because the program terminates immediately after this call
            }
            else
            {
               QServiceManager::instance->eventLog()->reportEvent(QServiceManager::tr("Registering failed.").arg(serviceName), QEventLog::S_Error);
               exit(1); // Very fatal (service won't stop otherwise)
            }
         }
         else
         {
            QServiceManager::instance->eventLog()->reportEvent(QServiceManager::tr("Service %1 not found.").arg(serviceName), QEventLog::S_Error);
            exit(1); // Very fatal (service won't stop otherwise)
         }
      }
      else
      {
         QServiceManager::instance->eventLog()->reportEvent(QServiceManager::tr("Missing service name."), QEventLog::S_Error);
         exit(1); // Very fatal (service won't stop otherwise)
      }
   }

   static int serviceControlDispatcher(QServiceManager *serviceManager)
   {
      int retCode = EXIT_SUCCESS;
      int i = serviceManager->serviceList().count();

      if (i)
      {
         SERVICE_TABLE_ENTRYW *dispatchTable = new SERVICE_TABLE_ENTRYW[i + 1];

         dispatchTable[i].lpServiceName = NULL;
         dispatchTable[i].lpServiceProc = NULL;
         while (i--)
         {
            dispatchTable[i].lpServiceName = (wchar_t*)serviceManager->serviceList().at(i)->serviceName().utf16();
            dispatchTable[i].lpServiceProc = serviceMain;
         }

         serviceManager->eventLog()->reportEvent(QServiceManager::tr("Running the Windows Service Control Dispatcher"), QEventLog::S_Debug);

         // This call returns when the service has stopped.
         // The process should simply terminate when the call returns.
         if (!StartServiceCtrlDispatcherW(dispatchTable))
         {
            if (GetLastError() == ERROR_FAILED_SERVICE_CONTROLLER_CONNECT)
            {
               // Means we're started from console, not from service mgr
               // start() will ask the mgr to start another instance of us as a service instead
               serviceManager->eventLog()->reportEvent(QServiceManager::tr("The service could not connect to the service controller."), QEventLog::S_Warning);
               retCode = 2;
            }
            else
            {
               serviceManager->eventLog()->reportEvent(QServiceManager::tr("The service could not be started [%1]").arg(qt_error_string(GetLastError())), QEventLog::S_Error);
               retCode = EXIT_FAILURE;
            }
         }

         delete [] dispatchTable;
      }
      else
         serviceManager->eventLog()->reportEvent(QServiceManager::tr("Service list is empty."), QEventLog::S_Error);

      exitApplication();

      return retCode;
   }

   static DWORD WINAPI serviceControlDispatcherThread(LPVOID lpParameter)
   {
      return (DWORD)serviceControlDispatcher((QServiceManager *)lpParameter);
   }

   static DWORD WINAPI explicitServiceThread(LPVOID lpParameter)
   {
      return (DWORD)explicitServiceRunner((QServiceProxy *)lpParameter);
   }
#else
   // TODO: Code for other operating systems
#endif

//
// class QServiceManager
//

QServiceManager *QServiceManager::instance = nullptr;

QServiceManager::QServiceManager(int &argc, char **argv) : _app(new QCoreApplication(argc, argv)), _eventLog(new QEventLog), _osData(new OSData)
{
   instance = this;

#if defined(Q_OS_WIN)
#if QT_VERSION >= QT_VERSION_CHECK(5,0,0)
   _app->installNativeEventFilter(&myEventFilter);
#else
   nextFilter = app->setEventFilter(myEventFilter);
#endif
#endif

   _args = _app->arguments();
   _filePath = _app->applicationFilePath();

   // Remove the program path from the arguments
   if (!_args.isEmpty())
      _args.removeFirst();

   // Check the arguments for service manager tasks
   if (!_args.isEmpty())
   {
      _taskArg = _args.first();

#if defined(Q_OS_WIN)
      if (_taskArg.compare(QLatin1String("-s"), Qt::CaseInsensitive) == 0 || _taskArg.compare(QLatin1String("-start"), Qt::CaseInsensitive) == 0)
#elif defined(Q_OS_UNIX)
      if (_taskArg.compare(QLatin1String("-d"), Qt::CaseInsensitive) == 0 || _taskArg.compare(QLatin1String("-daemon"), Qt::CaseInsensitive) == 0)
#endif
         _task = T_Run;
      else if (_taskArg.compare(QLatin1String("-i"), Qt::CaseInsensitive) == 0 || _taskArg.compare(QLatin1String("-install"), Qt::CaseInsensitive) == 0)
         _task = T_Install;
      else if (_taskArg.compare(QLatin1String("-u"), Qt::CaseInsensitive) == 0 || _taskArg.compare(QLatin1String("-uninstall"), Qt::CaseInsensitive) == 0)
         _task = T_Uninstall;
      else if (_taskArg.compare(QLatin1String("-v"), Qt::CaseInsensitive) == 0 || _taskArg.compare(QLatin1String("-version"), Qt::CaseInsensitive) == 0)
         _task = T_Status;
      else if (_taskArg.compare(QLatin1String("-e"), Qt::CaseInsensitive) == 0 || _taskArg.compare(QLatin1String("-exec"), Qt::CaseInsensitive) == 0)
         _task = T_Debug;
      else if (_taskArg.compare(QLatin1String("-t"), Qt::CaseInsensitive) == 0 || _taskArg.compare(QLatin1String("-terminate"), Qt::CaseInsensitive) == 0)
         _task = T_Stop;
      else if (_taskArg.compare(QLatin1String("-p"), Qt::CaseInsensitive) == 0 || _taskArg.compare(QLatin1String("-pause"), Qt::CaseInsensitive) == 0)
         _task = T_Pause;
      else if (_taskArg.compare(QLatin1String("-r"), Qt::CaseInsensitive) == 0 || _taskArg.compare(QLatin1String("-resume"), Qt::CaseInsensitive) == 0)
         _task = T_Resume;
      else if (_taskArg.compare(QLatin1String("-c"), Qt::CaseInsensitive) == 0 || _taskArg.compare(QLatin1String("-command"), Qt::CaseInsensitive) == 0)
         _task = T_Command;
      else if (_taskArg.compare(QLatin1String("-h"), Qt::CaseInsensitive) == 0 || _taskArg.compare(QLatin1String("-help"), Qt::CaseInsensitive) == 0)
         _task = T_Help;
      else
         _taskArg.clear();

      if (!_taskArg.isEmpty())
         _args.removeFirst();
   }

   if (_task != T_Run)
      new ConsoleEventLog(true, _eventLog);
}

QServiceManager::~QServiceManager()
{
   qDeleteAll(_serviceList);
   _serviceList.clear();
   delete _osData;
   delete _eventLog;
   instance = nullptr;
   delete _app;

#if defined(Q_OS_UNIX)
   // exit(EXIT_SUCCESS); // At the end of the program exit might have to be called
#endif
}

void QServiceManager::addService(QServiceFactory *service)
{
   if (service == nullptr)
      return;

   *service->eventLog() = *_eventLog;
   _serviceList.append(new QServiceProxy(service));
}

int QServiceManager::exec()
{
   if (_task == T_Help || (_args.count() == 1 && _args.first() == "-h"))
   {
      auto helpMsg = helpMessage(_task);

      for (auto&& line : helpMsg)
         consoleOut(line);

      return EXIT_SUCCESS;
   }

   // Create the service manager system log here to separate it from the logs of the services (copied in addService())
   if (_task == T_Run)
      new SystemEventLog("QServiceManager", true, _eventLog);

   QList<ArgDef> argDefs;

   for (auto&& service : _serviceList)
   {
      argDefs.append(service->argumentDefinitions(_task));
   }

   auto errors = appArguments(argDefs, _args, _serviceArgs);

   if (!errors.isEmpty())
   {
      for (auto&& error : errors)
      {
         eventLog()->reportEvent(error, QEventLog::S_Error);
      }

      auto helpMsg = helpMessage(_task);

      consoleOut(QString());
      for (auto&& line : helpMsg)
         consoleOut(line);

      return EXIT_FAILURE;
   }

   for (auto&& service : _serviceList)
   {
      if (!service->init(_serviceArgs))
      {
         eventLog()->reportEvent(tr("Service could not be initialized."), QEventLog::S_Error);

         auto helpMsg = helpMessage(T_Help);

         consoleOut(QString());
         for (auto&& line : helpMsg)
            consoleOut(line);

         return EXIT_FAILURE;
      }

      if (_task == T_Run)
         new SystemEventLog(service->serviceName(), true, service->eventLog());
   }

   auto retCode = EXIT_SUCCESS;

   if (_task == T_Run)
   {
      if (_taskArg.isEmpty() && !_args.isEmpty())
         eventLog()->reportEvent(tr("Missing service start parameter."), QEventLog::S_Warning);

#if defined (Q_OS_WIN)
      // Run the service control dispatcher in another thread
      DWORD threadId;

      auto hDispatchThread = CreateThread(NULL, 0, &serviceControlDispatcherThread, this, 0, &threadId);

      retCode = _app->exec();
#ifdef _DEBUG
      WaitForSingleObject(hDispatchThread, INFINITE);
#else
      WaitForSingleObject(hDispatchThread, 30000);
#endif
      DWORD exitCode;
      if (GetExitCodeThread(hDispatchThread, &exitCode))
         retCode = (int)exitCode;
      CloseHandle(hDispatchThread);
      hDispatchThread = NULL;

      if (retCode != 2)
         return retCode;

      // We are not started from the SCM.
      // If the -s or -start parameter was supplied start the service via SCM.
      // Otherwise show the standard help

      if (_taskArg.isEmpty())
      {
         eventLog()->reportEvent(tr("Missing service start parameter."), QEventLog::S_Error);

         auto helpMsg = helpMessage(T_Help);

         consoleOut(QString());
         for (auto&& line : helpMsg)
            consoleOut(line);

         return EXIT_SUCCESS;
      }
#elif defined (Q_OS_UNIX)
      /* Our process ID and Session ID */
      pid_t pid, sid;

      /* Fork off the parent process */
      pid = fork();
      if (pid < 0)
         retCode = EXIT_FAILURE;
      /* If we got a good PID, then we can exit the parent process. */
      else if (pid > 0)
         retCode = EXIT_SUCCESS;
      else
      {
         /* Create a new SID for the child process */
         sid = setsid();
         if (sid < 0)
         {
            /* Log the failure */
            retCode = EXIT_FAILURE;
         }
         else
         {
            /* Set signal handlers (disable SIGHUP, SIGINT and SIGWINCH) */
            handle_signal(SIGHUP, SIG_IGN);
            handle_signal(SIGINT, SIG_IGN);
            handle_signal(SIGWINCH, SIG_IGN);
            handle_signal(SIGQUIT, terminateSignalHandler);
            handle_signal(SIGTERM, terminateSignalHandler);

            /* Terminate the child to prohibit any terminals */
            /* pid = fork();
               if (pid < 0)
                  exit(EXIT_FAILURE);
               if (pid > 0)
                  exit(EXIT_SUCCESS); */

            /* Change the current working directory */
            if ((chdir("/")) < 0)
            {
               /* Log the failure */
               retCode = EXIT_FAILURE;
            }
            else
            {
               /* Change the file mode mask */
               umask(0);

               /* Close out the standard file descriptors */
               close(STDIN_FILENO);
               close(STDOUT_FILENO);
               close(STDERR_FILENO);

               /* Open any logs here */
               //_eventLog.appendLog(new SystemEventLog(QLatin1String("QServiceManager"), true), true); // The log is opened when the service is started!!!!!!!!!

               /* Daemon-specific initialization goes here */

               /* Start the services in different threads */
               pthread_t thread;

               Q_ASSERT(_osData->_explicitThreadList.isEmpty());
               foreach (QServiceProxy *service, _serviceList)
               {
                  if (pthread_create(&thread, NULL, explicitServiceThread, (void *)service) != 0)
                     eventLog()->reportEvent(tr("Service thread could not be started."), QEventLog::S_Error);
                  else
                     _osData->_explicitThreadList.append(thread);
               }

               if (_osData->_explicitThreadList.isEmpty())
               {
                  eventLog()->reportEvent(tr("No service."), QEventLog::S_Error);
                  retCode = EXIT_FAILURE;
               }
               else
               {
                  retCode = _app->exec();
                  stopServices();
               }
            }
         }
      }

      return retCode;
#endif
   }

   if (_task == T_Debug)
   {
      if (!installConsoleEventHandler())
         eventLog()->reportEvent(tr("ConsoleEventHandler could not be installed."), QEventLog::S_Warning);

#if defined(Q_OS_WIN)
      DWORD threadId;

      Q_ASSERT(_osData->_explicitThreadHandleList.isEmpty());
      foreach (QServiceProxy *service, _serviceList)
      {
         // Run the controllers in separate threads
         _osData->_explicitThreadHandleList.append(CreateThread(NULL, 0, explicitServiceThread, service, 0, &threadId));
      }

      if (_osData->_explicitThreadHandleList.isEmpty())
      {
         eventLog()->reportEvent(tr("No service."), QEventLog::S_Error);
         retCode = EXIT_FAILURE;
      }
      else
      {
         retCode = _app->exec();
         stopServices();
      }
#elif defined(Q_OS_UNIX)
      pthread_t thread;

      Q_ASSERT(_osData->_explicitThreadList.isEmpty());
      foreach (QServiceProxy *service, _serviceList)
      {
         if (pthread_create(&thread, NULL, explicitServiceThread, (void *)service) != 0)
            eventLog()->reportEvent(tr("Service thread could not be started."), QEventLog::S_Error);
         else
            _osData->_explicitThreadList.append(thread);
      }

      if (_osData->_explicitThreadList.isEmpty())
      {
         eventLog()->reportEvent(tr("No service."), QEventLog::S_Error);
         retCode = EXIT_FAILURE;
      }
      else
      {
         retCode = _app->exec();
         stopServices();
      }
#endif

      return retCode;
   }

   foreach (QServiceProxy *service, _serviceList)
   {
      QServiceController controller(service->serviceName());

      switch (_task)
      {
      case T_Install:
         {
            Q_ASSERT(!_filePath.startsWith("\""));

            const auto program = "\"" + _filePath + "\"";

            if (service->install(&controller, program, _serviceArgs, _serviceList.count() > 1))
            {
               consoleOut(tr("The service %1 is installed under: %2.").arg(service->serviceName()).arg(_filePath));
            }
            else
            {
               retCode = EXIT_FAILURE;
               consoleOut(tr("The service %1 could not be installed.").arg(service->serviceName()), true);
               consoleOut(controller.lastErrorText(), true);
            }
         }
         break;

      case T_Uninstall:
         if (controller.isInstalled())
         {
            if (controller.uninstall())
            {
               consoleOut(tr("The service %1 is uninstalled.").arg(service->serviceName()));
            }
            else
            {
               retCode = EXIT_FAILURE;
               consoleOut(tr("The service %1 could not be uninstalled.").arg(service->serviceName()), true);
               consoleOut(controller.lastErrorText(), true);
            }
         }
         else
            consoleOut(tr("The service %1 is not installed.").arg(service->serviceName()));
         break;

      case T_Status:
         consoleOut(tr("The service %1 (%2) is %3 and %4.").arg(service->serviceName()).arg(QFileInfo(_filePath).fileName()).arg(controller.isInstalled() ? tr("installed") : tr("not installed")).arg(controller.status() == ServiceRunning ? tr("running") : tr("not running")));
         break;

      case T_Run:
         if (service->start(&controller, _serviceArgs))
         {
            consoleOut(tr("The service is started."));
         }
         else
         {
            retCode = EXIT_FAILURE;
            consoleOut(tr("The service could not be started."), true);
            consoleOut(controller.lastErrorText(), true);
         }
         break;

      case T_Stop:
         if (!controller.stop())
         {
            retCode = EXIT_FAILURE;
            consoleOut(tr("The service could not be stopped."), true);
            consoleOut(controller.lastErrorText(), true);
         }
         else if (!controller.waitForStopped())
         {
            retCode = 2;
            consoleOut(tr("The service does not respond."), true);
            consoleOut(controller.lastErrorText(), true);
         }
         else
         {
            consoleOut(tr("The service is stopped."));
         }
         break;

      case T_Pause:
         if (controller.pause())
         {
            consoleOut(tr("The service is paused."));
         }
         else
         {
            retCode = EXIT_FAILURE;
            consoleOut(tr("The service could not be paused."), true);
            consoleOut(controller.lastErrorText(), true);
         }
         break;

      case T_Resume:
         if (controller.resume())
         {
            consoleOut(tr("The service is resumed."));
         }
         else
         {
            retCode = EXIT_FAILURE;
            consoleOut(tr("The service could not be resumed."), true);
            consoleOut(controller.lastErrorText(), true);
         }
         break;

      case T_Command:
         controller.sendCommand(_args.isEmpty() ? 0 : _args.first().toInt());
         break;
      }
   }

   return retCode;
}

QEventLog* QServiceManager::eventLog() const
{
   return _eventLog;
}

void QServiceManager::serviceIsStopping(QServiceProxy *service)
{
#if defined(Q_OS_WIN)
   for (int i = 0; i < _osData->_explicitThreadHandleList.count(); ++i)
   {
      if (_serviceList.at(i) != service && WaitForSingleObject(_osData->_explicitThreadHandleList.at(i), 0) == WAIT_TIMEOUT)
         return;
   }
#elif defined(Q_OS_UNIX)
   void *thread_return;

   for (int i = 0; i < _osData->_explicitThreadList.count(); ++i)
   {
      if (_serviceList.at(i) != service && pthread_tryjoin_np(_osData->_explicitThreadList.at(i), &thread_return) != 0)
         return;
   }
#endif

   exitApplication();
}

void QServiceManager::stopServices()
{
   // Stop all services
   for (auto&& service : _serviceList)
   {
      service->stop();
   }

   // Wait for the threads to terminate
#if defined(Q_OS_UNIX)
   void *thread_return;

   for (pthread_t thread : _osData->_explicitThreadList)
   {
      if (pthread_join(thread, &thread_return) != 0)
         eventLog()->reportEvent(tr("Service thread could not be terminated."), QEventLog::S_Error);
   }

#elif defined (Q_OS_WIN)
   for (HANDLE hThread : _osData->_explicitThreadHandleList)
   {
#ifdef _DEBUG
      WaitForSingleObject(hThread, INFINITE);
#else
      WaitForSingleObject(hThread, 30000);
#endif
   }
#endif
}

QStringList QServiceManager::helpMessage(ServiceTask task) const
{
   QStringList help;

   QList<ArgDef> argDefs;

   for (auto&& service : _serviceList)
   {
      if (task == T_Help)
         argDefs.append(service->initArguments());
      else
         argDefs.append(service->argumentDefinitions(task));
   }

   auto commandLine = QFileInfo(_filePath).fileName();

   if (task == T_Help)
   {
      commandLine += " -";
#if defined(Q_OS_WIN)
      commandLine += "[i|u|e|t|v|h|s]"; // -[i|u|e|t|c|v|h|s]
#elif defined(Q_OS_UNIX)
      commandLine += "[i|u|e|t|v|h|d]"; // -[i|u|e|t|c|v|h|d]
#endif
   }
   else if (!_taskArg.isEmpty())
   {
      commandLine += " " + _taskArg;
   }
   else
   {
#if defined(Q_OS_WIN)
      commandLine += " -s";
#elif defined(Q_OS_UNIX)
      commandLine += " -d";
#endif
   }

   for (auto&& argDef : argDefs)
   {
      commandLine += " ";

      if (!argDef._options.isEmpty())
      {
         QStringList options;

         for (auto&& exclOpt : argDef._options)
         {
            if (!exclOpt._description.isEmpty())
               options.append(exclOpt._optName);
         }

         if (!options.isEmpty())
         {
            commandLine += "-";

            if (!argDef._isMandatory)
               commandLine += "[";

            commandLine += options.join(u'|');

            if (argDef._isMulti)
               commandLine += "*";

            if (!argDef._isMandatory)
               commandLine += "]";
         }
      }
      else if (!argDef._optName.isEmpty())
      {
         if (!argDef._description.isEmpty())
         {
            commandLine += "-";

            if (!argDef._isMandatory)
               commandLine += "[";

            commandLine += argDef._optName;

            if (argDef._isMulti)
               commandLine += "*";

            if (!argDef._valueName.isEmpty())
               commandLine += "={" + argDef._valueName + "}";

            if (!argDef._isMandatory)
               commandLine += "]";
         }
      }
      else if (!argDef._description.isEmpty())
      {
         if (!argDef._isMandatory)
            commandLine += "[";

         commandLine += "{" + argDef._valueName + "}";

         if (argDef._isMulti)
            commandLine += "*";

         if (!argDef._isMandatory)
            commandLine += "]";
      }
   }

   if (task == T_Help)
      commandLine += tr(" [-h | task parameters]");

   if (!_serviceList.isEmpty())
      help.append(_serviceList.first()->version()); // TODO: Find better way to add version information
   help.append(tr("Syntax:"));
   help.append(commandLine);
   help.append(QString());
   help.append(tr("Parameters:"));

   if (task == T_Help)
   {
      help.append(tr("\t-i(nstall)\t: Install the service"));
      help.append(tr("\t-u(ninstall)\t: Uninstall the service."));
      help.append(tr("\t-e(xec)\t\t: Run as a regular application. Useful for debugging."));
      help.append(tr("\t-t(erminate)\t: Stop the service."));
      //help.append(tr("\t-c(ommand) num\t: Send command code num to the service."));
      help.append(tr("\t-v(ersion)\t: Print version and status information."));
      help.append(tr("\t-h(elp)\t\t: Show this help"));
#if defined(Q_OS_WIN)
      help.append(tr("\t-s(tart)\t: Start the service via SCM."));
#elif defined(Q_OS_UNIX)
      help.append(tr("\t-d(aemon)\t: Start the service as daemon."));
#endif
   }

   for (auto&& argDef : argDefs)
   {
      if (!argDef._options.isEmpty())
      {
         for (auto&& exclOpt : argDef._options)
         {
            if (!exclOpt._description.isEmpty())
            {
               auto option = exclOpt._optName;
               if (argDef._isMulti)
                  option += "*";
               if (option.length() < 7)
                  option += "\t";
               help.append(QString("\t-%1\t: %2").arg(option).arg(exclOpt._description));
            }
         }
      }
      else if (!argDef._optName.isEmpty())
      {
         if (!argDef._description.isEmpty())
         {
            auto option = argDef._optName;
            if (argDef._isMulti)
               option += "*";
            if (!argDef._valueName.isEmpty())
               option += "=" + argDef._valueName;
            if (option.length() < 7)
               option += "\t";
            help.append(QString("\t-%1\t: %2").arg(option).arg(argDef._description));
         }
      }
      else if (!argDef._description.isEmpty())
      {
         auto valName = argDef._valueName;
         if (argDef._isMulti)
            valName += "*";
         if (valName.length() < 8)
            valName += "\t";
         help.append(QString("\t%1\t: %2").arg(valName).arg(argDef._description));
      }
   }

   if (task == T_Help)
   {
      help.append(tr("\ttask parameters\t: Additional arguments for the task."));
   }

   return help;
}
