#include "QServiceProxy.h"
#include <QCoreApplication>

#if defined(Q_OS_WIN)

   #define WIN32_LEAN_AND_MEAN
   #include <Windows.h>

   class QServiceProxy::OSData
   {
   public:
      OSData(QServiceProxy *sp);
      ~OSData();

      void setService(QServiceBase *service)
      {
         EnterCriticalSection(&_criticalSection);

         _service = service;

         LeaveCriticalSection(&_criticalSection);
      }

      bool registerServiceHandler(const QString &serviceName)
      {
         _hServiceStatus = RegisterServiceCtrlHandlerExW((const wchar_t *)serviceName.utf16(), serviceControlHandler, this);

         return _hServiceStatus != NULL;
      }

      void setServiceType(bool shared)
      {
         _status.dwServiceType = shared ? SERVICE_WIN32_SHARE_PROCESS : SERVICE_WIN32_OWN_PROCESS;
      }

      bool reportStatus(ServiceState status, int waitHint);

      ServiceState status() const;

      void setAcceptPauseAndContinue(bool accept = true) { _acceptPauseAndContinue = accept; }

      void stop() { handleControlRequest(SERVICE_CONTROL_SHUTDOWN); }

   private:
      int handleControlRequest(int code);

      static DWORD WINAPI serviceControlHandler(DWORD dwControl, DWORD /*dwEventType*/, LPVOID /*lpEventData*/, LPVOID lpContext)
      {
         OSData *osController = (OSData *)lpContext;

         return (DWORD)osController->handleControlRequest((int)dwControl);
      }

      bool reportStatus(DWORD currentState, DWORD exitCode, DWORD waitHint);

   private:
      QServiceProxy           *_serviceProxy;
      CRITICAL_SECTION        _criticalSection;
      QServiceBase            *_service;
      SERVICE_STATUS_HANDLE   _hServiceStatus;
      SERVICE_STATUS          _status;
      bool                    _acceptPauseAndContinue;
   };


   QServiceProxy::OSData::OSData(QServiceProxy *sp) : _serviceProxy(sp), _service(nullptr), _hServiceStatus(0), _acceptPauseAndContinue(false)
   {
      InitializeCriticalSection(&_criticalSection);

      _status.dwServiceType             = SERVICE_WIN32_OWN_PROCESS; // |SERVICE_INTERACTIVE_PROCESS
      _status.dwCurrentState            = SERVICE_STOPPED;
      _status.dwControlsAccepted        = 0;
      _status.dwWin32ExitCode           = NO_ERROR;
      _status.dwServiceSpecificExitCode = 0;
      _status.dwCheckPoint              = 0;
      _status.dwWaitHint                = 0;
   }

   QServiceProxy::OSData::~OSData()
   {
      DeleteCriticalSection(&_criticalSection);
   }

   bool QServiceProxy::OSData::reportStatus(ServiceState status, int waitHint)
   {
      DWORD currentState = SERVICE_STOPPED;

      switch (status)
      {
      case ServiceStopped:
         currentState = SERVICE_STOPPED;
         break;
      case ServiceStartPending:
         currentState = SERVICE_START_PENDING;
         break;
      case ServiceRunning:
         currentState = SERVICE_RUNNING;
         break;
      case ServicePausePending:
         currentState = SERVICE_PAUSE_PENDING;
         break;
      case ServicePaused:
         currentState = SERVICE_PAUSED;
         break;
      case ServiceContinuePending:
         currentState = SERVICE_CONTINUE_PENDING;
         break;
      case ServiceStopPending:
         currentState = SERVICE_STOP_PENDING;
         break;
      }

      return reportStatus(currentState, NO_ERROR, (DWORD)waitHint);
   }

   ServiceState QServiceProxy::OSData::status() const
   {
      ServiceState serviceState = ServiceStopped;

      switch (_status.dwCurrentState)
      {
      case SERVICE_STOPPED:
         serviceState = ServiceStopped;
         break;
      case SERVICE_START_PENDING:
         serviceState = ServiceStartPending;
         break;
      case SERVICE_RUNNING:
         serviceState = ServiceRunning;
         break;
      case SERVICE_PAUSE_PENDING:
         serviceState = ServicePausePending;
         break;
      case SERVICE_PAUSED:
         serviceState = ServicePaused;
         break;
      case SERVICE_CONTINUE_PENDING:
         serviceState = ServiceContinuePending;
         break;
      case SERVICE_STOP_PENDING:
         serviceState = ServiceStopPending;
         break;
      }

      return serviceState;
   }

   int QServiceProxy::OSData::handleControlRequest(int code)
   {
      int retCode = ERROR_CALL_NOT_IMPLEMENTED;

      EnterCriticalSection(&_criticalSection);

      switch (code)
      {
      case SERVICE_CONTROL_STOP: // 1
         if (_status.dwCurrentState != SERVICE_STOPPED && _status.dwCurrentState != SERVICE_STOP_PENDING)
         {
            reportStatus(SERVICE_STOP_PENDING, NO_ERROR, 0);
            _serviceProxy->stopAsync(_service);
         }
         retCode = NO_ERROR;
         break;
      case SERVICE_CONTROL_PAUSE: // 2
         if (_status.dwCurrentState != SERVICE_PAUSED && _status.dwCurrentState != SERVICE_PAUSE_PENDING)
         {
            reportStatus(SERVICE_PAUSE_PENDING, NO_ERROR, 0);
            _serviceProxy->pauseAsync(_service);
         }
         retCode = NO_ERROR;
         break;
      case SERVICE_CONTROL_CONTINUE: // 3
         if (_status.dwCurrentState != SERVICE_PAUSE_CONTINUE && _status.dwCurrentState != SERVICE_CONTINUE_PENDING && _status.dwCurrentState != SERVICE_RUNNING)
         {
            reportStatus(SERVICE_CONTINUE_PENDING, NO_ERROR, 0);
            _serviceProxy->resumeAsync(_service);
         }
         retCode = NO_ERROR;
         break;
      case SERVICE_CONTROL_INTERROGATE: // 4
         // Fall through to send current status.
         retCode = NO_ERROR;
         break;
      case SERVICE_CONTROL_SHUTDOWN: // 5
         if (_status.dwCurrentState != SERVICE_STOPPED && _status.dwCurrentState != SERVICE_STOP_PENDING)
         {
            reportStatus(SERVICE_STOP_PENDING, NO_ERROR, 0);
            _serviceProxy->stopAsync(_service);
         }
         retCode = NO_ERROR;
         break;
      default:
         if (_status.dwCurrentState == SERVICE_RUNNING)
         {
            if (code >= 128 && code <= 255)
               _serviceProxy->processCommandAsync(_service, code);
         }
         retCode = NO_ERROR;
         break;
      }

      // Always report the status
      if (_hServiceStatus && _status.dwCurrentState != SERVICE_STOPPED)
         SetServiceStatus(_hServiceStatus, &_status);

      LeaveCriticalSection(&_criticalSection);

      return retCode;
   }

   // Used internal only
   bool QServiceProxy::OSData::reportStatus(DWORD currentState, DWORD exitCode, DWORD waitHint)
   {
      bool wrongState = false;

      EnterCriticalSection(&_criticalSection);

      if (_status.dwCurrentState == SERVICE_STOP_PENDING && currentState != SERVICE_STOP_PENDING && currentState != SERVICE_STOPPED)
         wrongState = true;
      else if (_status.dwCurrentState != SERVICE_STOPPED || currentState == SERVICE_START_PENDING)
      {
         _status.dwCurrentState = currentState;
         _status.dwWin32ExitCode = exitCode;
         _status.dwWaitHint = waitHint;

         if (currentState == SERVICE_START_PENDING)
            _status.dwControlsAccepted = 0;
         else
         {
            _status.dwControlsAccepted = SERVICE_ACCEPT_STOP;
            if (_acceptPauseAndContinue)
               _status.dwControlsAccepted |= SERVICE_ACCEPT_PAUSE_CONTINUE;
         }

         if (currentState == SERVICE_RUNNING || currentState == SERVICE_STOPPED || currentState == SERVICE_PAUSED)
            _status.dwCheckPoint = 0;
         else
            _status.dwCheckPoint++;

         if (_hServiceStatus)
            SetServiceStatus(_hServiceStatus, &_status);
      }

      LeaveCriticalSection(&_criticalSection);

      return !wrongState;
   }

#elif defined(Q_OS_UNIX)

class QServiceProxy::OSData
{
public:
   OSData(QServiceProxy *sp);
   ~OSData();

   void setService(QServiceBase *service)
   {
      QMutexLocker locker(&_mutex);

      _service = service;
   }

   bool registerServiceHandler(const QString &serviceName)
   {
      Q_UNUSED(serviceName)
      return false;
   }

   void setServiceType(bool shared)
   {
      Q_UNUSED(shared)
   }

   bool reportStatus(ServiceState status, int waitHint);

   ServiceState status() const;

   void setAcceptPauseAndContinue(bool accept = true) { _acceptPauseAndContinue = accept; }

   void stop() { QMutexLocker locker(&_mutex); _serviceProxy->stopAsync(_service); }

private:
   QServiceProxy  *_serviceProxy = nullptr;
   QMutex         _mutex;
   QServiceBase   *_service = nullptr;
   ServiceState   _serviceState = ServiceStopped;
   bool           _acceptPauseAndContinue = false;
};


QServiceProxy::OSData::OSData(QServiceProxy *sp) : _serviceProxy(sp)
{
}

QServiceProxy::OSData::~OSData()
{
}

bool QServiceProxy::OSData::reportStatus(ServiceState status, int waitHint)
{
   Q_UNUSED(status)
   Q_UNUSED(waitHint)
   _serviceState = status;
   return true;
}

ServiceState QServiceProxy::OSData::status() const
{
   return _serviceState;
}

#endif

QServiceProxy::QServiceProxy(QServiceFactory *factory, QObject *parent) : QObject(parent), _factory(factory), _osData(new OSData(this))
{
}

QServiceProxy::~QServiceProxy()
{
   delete _factory;
   delete _osData;
}

bool QServiceProxy::registerServicHandler(bool shared)
{
   _osData->setServiceType(shared);
   return _osData->registerServiceHandler(serviceName());
}

int QServiceProxy::run(const QMultiMap<QString, QString>& args)
{
   QServiceBase *service = _factory->createService(this, args);

   if (service)
   {
      connect(this, &QServiceProxy::stopRequest, service, &QServiceBase::stop, Qt::QueuedConnection);
      connect(this, &QServiceProxy::pauseRequest, service, &QServiceBase::pause, Qt::QueuedConnection);
      connect(this, &QServiceProxy::resumeRequest, service, &QServiceBase::resume, Qt::QueuedConnection);
      connect(this, &QServiceProxy::commandRequest, service, &QServiceBase::processCommand, Qt::QueuedConnection);

      _osData->setService(service);

      eventLog()->reportEvent(tr("Starting the service."), QEventLog::S_Debug);

      service->start(); // Runs the service and doesn't stop until the service terminated

      _osData->setService(nullptr);
      delete service;
      service = nullptr;
   }

   return 0;
}

void QServiceProxy::stop()
{
   _osData->stop();
}

bool QServiceProxy::reportStatus(ServiceState status, int waitHint)
{
   return _osData->reportStatus(status, waitHint);
}

ServiceState QServiceProxy::status() const
{
   return _osData->status();
}

void QServiceProxy::setAcceptPauseAndContinue(bool accept)
{
   _osData->setAcceptPauseAndContinue(accept);
}

void QServiceProxy::stopAsync(QServiceBase *service)
{
   if (service)
   {
      if (service->isEventDriven())
         emit stopRequest();
      else
         service->stop();
   }
}

void QServiceProxy::pauseAsync(QServiceBase *service)
{
   if (service)
   {
      if (service->isEventDriven())
         emit pauseRequest();
      else
         service->pause();
   }
}

void QServiceProxy::resumeAsync(QServiceBase *service)
{
   if (service)
   {
      if (service->isEventDriven())
         emit resumeRequest();
      else
         service->resume();
   }
}

void QServiceProxy::processCommandAsync(QServiceBase *service, int code)
{
   if (service)
   {
      if (service->isEventDriven())
         emit commandRequest(code - 128);
      else
         service->processCommand(code - 128);
   }
}
