#include "ServiceTools.h"
#include <ConsoleTools.h>
#include <QDateTime>

#if defined(Q_OS_WIN)

   #define WIN32_LEAN_AND_MEAN
   #include <Windows.h>

   class SystemEventLog::Data
   {
   public:
      Data() : _hEventSource(NULL) {}
      ~Data() { if (_hEventSource) DeregisterEventSource(_hEventSource); }

      bool registerSource(const QString &serviceName)
      {
         if (!serviceName.isEmpty() && serviceName != _serviceName)
         {
            _serviceName = serviceName;
            if (_hEventSource)
               DeregisterEventSource(_hEventSource);
            _hEventSource = RegisterEventSourceW(NULL, (const wchar_t *)_serviceName.utf16());
         }
         return _hEventSource;
      }

      bool reportEvent(const QString &message, QEventLog::Severity severity) const
      {
         if (_hEventSource == NULL)
            return false;

         WORD eventType = EVENTLOG_ERROR_TYPE;

         switch (severity)
         {
         case QEventLog::S_Debug:
         case QEventLog::S_Information:
            eventType = EVENTLOG_INFORMATION_TYPE;
            break;
         case QEventLog::S_Success:
            eventType = EVENTLOG_SUCCESS;
            break;
         case QEventLog::S_Warning:
            eventType = EVENTLOG_WARNING_TYPE;
            break;
         case QEventLog::S_Error:
            eventType = EVENTLOG_ERROR_TYPE;
            break;
         }

         // Windows adds a period after the message
         int      msgLen = message.length();
         QString  displayText;

         if (msgLen && message.at(msgLen - 1) == QLatin1Char('.'))
            displayText = message.left(msgLen - 1);
         else
            displayText = message;

         const wchar_t* strings[2];

         strings[0] = (const wchar_t *)_serviceName.utf16();
         strings[1] = (const wchar_t *)displayText.utf16();

         return FALSE != ReportEventW(
            _hEventSource, // event log handle
            eventType,    // event type
            0,            // event category
            0,            // event identifier
            NULL,         // no security identifier
            2,            // size of lpszStrings array
            0,            // no binary data
            strings,      // array of strings
            NULL);        // no binary data
      }

   private:
      QString  _serviceName;
      HANDLE   _hEventSource;
   };

#elif defined(Q_OS_UNIX)
   #include <QFile>
   #include <syslog.h>

   class SystemEventLog::Data
   {
   public:
      Data() {}
      ~Data() { if (!_serviceName.isEmpty()) closelog(); }

      bool registerSource(const QString &serviceName)
      {
         if (!serviceName.isEmpty() && _serviceName.isEmpty())
         {
            _serviceName = serviceName;
            openlog(QFile::encodeName(serviceName), LOG_PID | LOG_CONS| LOG_NDELAY, LOG_DAEMON);
         }
         return true;
      }

      bool reportEvent(const QString &message, QEventLog::Severity severity)
      {
         if (_serviceName.isEmpty())
            return false;

         int eventType = LOG_ERR;

         switch (severity)
         {
         case QEventLog::S_Debug:
         case QEventLog::S_Information:
            eventType = LOG_INFO;
            break;
         case QEventLog::S_Success:
            eventType = LOG_NOTICE;
            break;
         case QEventLog::S_Warning:
            eventType = LOG_WARNING;
            break;
         case QEventLog::S_Error:
            eventType = LOG_ERR;
            break;
         }

         // Windows adds a period after the message
         int      msgLen = message.length();
         QString  displayText;

         if (msgLen && message.at(msgLen - 1) == QLatin1Char('.'))
            displayText = message.left(msgLen - 1);
         else
            displayText = message;

         syslog(eventType, "%s\n", displayText.toLocal8Bit().data());

         return true;
      }

   private:
      static QString _serviceName;
   };

   QString SystemEventLog::Data::_serviceName;

#endif

//
// QEventLog
//

QEventLog::Logger::Logger(QEventLog *parentEventLog) : _destroy(parentEventLog)
{
   if (parentEventLog)
      parentEventLog->appendLog(this);
}

QEventLog::Logger::~Logger()
{
   _destroy = false;
   foreach (QEventLog *eventLog, _parents)
      eventLog->removeLog(this);
}

bool QEventLog::Logger::transferHistory(Logger*) const
{
   return false;
}

void QEventLog::Logger::addParent(QEventLog* eventLog)
{
   _mutex.lock();
   _parents.append(eventLog);
   _mutex.unlock();
}

void QEventLog::Logger::removeParent(QEventLog* eventLog)
{
   bool removeLog = false;

   _mutex.lock();
   _parents.removeOne(eventLog);
   if (_parents.isEmpty() && _destroy)
      removeLog = true;
   _mutex.unlock();
   if (removeLog)
      delete this;
}

QEventLog::QEventLog() : _logLevel(S_Debug)
{
}

QEventLog::QEventLog(const QEventLog &other)
{
   other._mutex.lock();
   _logger = other._logger;
   foreach (Logger *log, _logger)
   {
      log->addParent(this);
   }
   other._mutex.unlock();
}

QEventLog::~QEventLog()
{
   foreach (Logger *log, _logger)
   {
      log->removeParent(this);
   }
}

QEventLog &QEventLog::operator=(const QEventLog &rhs)
{
   // Create a copy of the other loggers and bind them to us

   rhs._mutex.lock();

   QList<Logger *> logger = rhs._logger;

   foreach (Logger *log, logger)
   {
      log->addParent(this);
   }

   rhs._mutex.unlock();

   // Remove all our loggers and assign the copied list

   _mutex.lock();

   foreach (Logger *log, _logger)
   {
      log->removeParent(this);
   }

   _logger = logger;

   _mutex.unlock();

   return *this;
}

void QEventLog::reportEvent(const QString &message, Severity severity) const
{
   // Ignore events that should not be logged
   if (severity < _logLevel)
      return;

   QDateTime  timestamp = QDateTime::currentDateTime();

   _mutex.lock();

   foreach (Logger *log, _logger)
   {
      log->reportEvent(message, severity, timestamp);
   }

   _mutex.unlock();
}

void QEventLog::appendLog(Logger *logger)
{
   if (logger)
   {
      _mutex.lock();

      logger->addParent(this);
      _logger.append(logger);

      _mutex.unlock();
   }
}

void QEventLog::removeLog(Logger *logger)
{
   if (logger)
   {
      _mutex.lock();

      if (_logger.removeOne(logger))
      {
         logger->removeParent(this); // This may delete the logger!
         logger = nullptr;
      }

      _mutex.unlock();
   }
}

void QEventLog::transferHistory(Logger* logger) const
{
   QMutexLocker locker(&_mutex);

   foreach (Logger* log, _logger)
   {
      if (log->transferHistory(logger))
         break;
   }
}

//
// SystemEventLog
//

SystemEventLog::SystemEventLog(const QString &serviceName, bool essential, QEventLog* parentEventLog) : Logger(parentEventLog), _data(new Data), _essential(essential)
{
   registerSource(serviceName);
}

SystemEventLog::~SystemEventLog()
{
   delete _data;
}

bool SystemEventLog::registerSource(const QString &serviceName)
{
   if (!serviceName.isEmpty())
   {
      QMutexLocker locker(&_mutex);

      return _data->registerSource(serviceName);
   }
   return false;
}

void SystemEventLog::reportEvent(const QString &message, QEventLog::Severity severity, const QDateTime &timestamp)
{
   Q_UNUSED(timestamp);
   _mutex.lock();
   if (!_essential || severity == QEventLog::S_Success || severity == QEventLog::S_Error)
   {
      if (!_data->reportEvent(message, severity))
         consoleOut(message, severity == QEventLog::S_Error);
   }
   _mutex.unlock();
}

//
// ConsoleEventLog
//

void ConsoleEventLog::reportEvent(const QString &message, QEventLog::Severity severity, const QDateTime &timestamp)
{
   Q_UNUSED(timestamp);
   _mutex.lock();
   if (!_essential || severity == QEventLog::S_Success || severity == QEventLog::S_Error)
      consoleOut(message, severity == QEventLog::S_Error);
   _mutex.unlock();
}

//
// ServiceFunctionStatus
//

void ServiceFunctionStatus::add(const StatusMessage& statusMessage)
{
   if (_eventLog == nullptr)
      return;

   QEventLog::Severity severity = QEventLog::S_Error;

   switch (statusMessage.severity())
   {
   case StatusMessage::S_None:
      return;
   case StatusMessage::S_Debug:
      severity = QEventLog::S_Debug;
      break;
   case StatusMessage::S_Verbose:
      severity = QEventLog::S_Information;
      break;
   case StatusMessage::S_Information:
      severity = QEventLog::S_Success;
      break;
   case StatusMessage::S_Warning:
      severity = QEventLog::S_Warning;
      break;
   case StatusMessage::S_Error:
      severity = QEventLog::S_Error;
      break;
   case StatusMessage::S_Critical:
      severity = QEventLog::S_Error;
      break;
   case StatusMessage::S_All:
      return;
   }

   if (statusMessage.context().isEmpty())
      _eventLog->reportEvent(statusMessage.message(), severity);
   else
      _eventLog->reportEvent(QString("%1: %2").arg(statusMessage.context().join(_messageContextSeparator)).arg(statusMessage.message()), severity);
}
