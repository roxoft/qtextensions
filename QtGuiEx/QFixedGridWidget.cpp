#include "stdafx.h"
#include "QFixedGridWidget.h"
#include <QtWidgets>
#include "QFixedGridView.h"
#include "QTitleBarCloseButton.h"

QIcon QFixedGridWidget::iconCloseWidget;
QIcon QFixedGridWidget::iconEditText;
QIcon QFixedGridWidget::iconApplyTitleChange;

QFixedGridWidget::QFixedGridWidget(const QString& title, QWidget* parent) : QFrame(parent)
{
   setWindowTitle(title);
   init();
}

QFixedGridWidget::QFixedGridWidget(QWidget *parent) : QFrame(parent)
{
   init();
}

QFixedGridWidget::~QFixedGridWidget()
{
   qApp->removeEventFilter(this);
   for (auto&& titleBarWidget : _titleBarWidgets)
      disconnect(titleBarWidget, &QWidget::destroyed, this, &QFixedGridWidget::onTitleBarWidgetDestroyed);
}

bool QFixedGridWidget::isClosable() const
{
   return _closeWidgetAction;
}

void QFixedGridWidget::setIsClosable(bool isClosable)
{
   if (isClosable)
   {
      if (_titleBarCloseButton)
         return;

      // Add close button
      _titleBarCloseButton = new QTitleBarCloseButton;

      connect(_titleBarCloseButton, &QAbstractButton::clicked, this, &QFixedGridWidget::close);

      addTitleBarWidget(_titleBarCloseButton);

      _closeWidgetAction = new QAction(iconCloseWidget, tr("Delete widget"));

      connect(_closeWidgetAction, &QAction::triggered, this, &QFixedGridWidget::close);

      addAction(_closeWidgetAction);

      setContextMenuPolicy(Qt::ActionsContextMenu);
   }
   else
   {
      if (_titleBarCloseButton == nullptr)
         return;

      delete _titleBarCloseButton;
      _titleBarCloseButton = nullptr;
      delete _closeWidgetAction;
      _closeWidgetAction = nullptr;
   }
}

bool QFixedGridWidget::isTitleEditable() const
{
   return _changeTitleAction;
}

void QFixedGridWidget::setIsTitleEditable(bool isTitleEditable)
{
   if (isTitleEditable)
   {
      if (_changeTitleAction)
         return;

      _changeTitleAction = new QAction(iconEditText, tr("Edit title"));

      connect(_changeTitleAction, &QAction::triggered, this, &QFixedGridWidget::editTitle);

      addAction(_changeTitleAction);

      setContextMenuPolicy(Qt::ActionsContextMenu);
   }
   else
   {
      if (_changeTitleAction == nullptr)
         return;

      delete _changeTitleAction;
      _changeTitleAction = nullptr;
   }
}

void QFixedGridWidget::addTitleBarWidget(QWidget* w)
{
   if (!w)
      return;
   w->setParent(this);
   _titleBarWidgets.prepend(w);
   connect(w, &QWidget::destroyed, this, &QFixedGridWidget::onTitleBarWidgetDestroyed);
   positionTitleBarWidgets();
   if (isVisible())
      w->show();
   update(_titleArea);
}

QWidget* QFixedGridWidget::widget() const
{
   return _content;
}

void QFixedGridWidget::setWidget(QWidget* widget)
{
   _content = widget;
   if (_content)
      _content->setParent(this);
}

QByteArray QFixedGridWidget::saveState() const
{
   QByteArray result;

   QDataStream dataStream(&result, QIODevice::WriteOnly);

   dataStream << (qint32)1; // Version number

   dataStream.setVersion(QDataStream::Qt_5_6);

   dataStream << objectName() << windowTitle();

   return result;
}

void QFixedGridWidget::restoreState(const QByteArray& data)
{
   QDataStream dataStream(data);

   qint32 version;

   dataStream >> version;

   if (version != 1)
      return;

   dataStream.setVersion(QDataStream::Qt_5_6);

   QString name, title;

   dataStream >> name >> title;

   setObjectName(name);
   setWindowTitle(title);
}

void QFixedGridWidget::editTitle()
{
   auto editor = new QWidget(nullptr, Qt::Popup | Qt::Window);
   editor->setAttribute(Qt::WA_DeleteOnClose);

   auto lineEdit = new QLineEdit(windowTitle());

   auto pushButton = new QToolButton;

   pushButton->setIcon(iconApplyTitleChange);
   pushButton->setToolTip(tr("Apply"));

   auto layout = new QHBoxLayout;

   layout->setContentsMargins(0, 0, 0, 0);
   layout->addWidget(lineEdit);
   layout->addWidget(pushButton);
   editor->setLayout(layout);

   editor->resize(_titleArea.size());
   editor->move(mapToGlobal(_titleArea.topLeft()));

   connect(pushButton, &QPushButton::clicked, [this, lineEdit]() { setWindowTitle(lineEdit->text()); lineEdit->window()->close(); });
   connect(lineEdit, &QLineEdit::returnPressed, pushButton, &QPushButton::click);

   editor->show();
   lineEdit->setFocus();
   lineEdit->selectAll();
}

void QFixedGridWidget::mousePressEvent(QMouseEvent* event)
{
   QFrame::mousePressEvent(event);

   if (event->button() == Qt::LeftButton)
   {
#if QT_VERSION >= QT_VERSION_CHECK(6,0,0)
      _dragStartPosition = event->globalPosition().toPoint();
#else
      _dragStartPosition = event->globalPos();
#endif
      _dragStartGeometry = geometry();
      _startPosition = handleMousePosition(event->pos());

      raise();
   }
}

void QFixedGridWidget::mouseReleaseEvent(QMouseEvent* event)
{
   QFrame::mouseReleaseEvent(event);

   _startPosition = StartPosition::none;

   auto fixedGridView = qobject_cast<QFixedGridView*>(parentWidget());

#if QT_VERSION >= QT_VERSION_CHECK(6,0,0)
   if (fixedGridView && event->globalPosition().toPoint() != _dragStartPosition)
#else
   if (fixedGridView && event->globalPos() != _dragStartPosition)
#endif
      fixedGridView->snapIn(this);

   handleMousePosition(event->pos());
}

void QFixedGridWidget::wheelEvent(QWheelEvent* event)
{
   event->accept();
}

void QFixedGridWidget::focusInEvent(QFocusEvent* event)
{
   if (_content)
   {
      event->accept();
      if (_content->focusWidget())
         _content->focusWidget()->setFocus();
      return;
   }

   QFrame::focusInEvent(event);
}

void QFixedGridWidget::focusOutEvent(QFocusEvent* event)
{
   QFrame::focusOutEvent(event);

   if (_startPosition != StartPosition::none)
   {
      _startPosition = StartPosition::none;

      auto fixedGridView = qobject_cast<QFixedGridView*>(parentWidget());

      if (fixedGridView)
         fixedGridView->snapIn(this);

      unsetCursor();
   }
}

void QFixedGridWidget::resizeEvent(QResizeEvent* event)
{
   QFrame::resizeEvent(event);

   _titleArea.setWidth(this->width() - 6);

   positionTitleBarWidgets();

   if (_content)
   {
      _content->setGeometry(3, _titleArea.bottom() + 2, this->width() - 6, this->height() - _titleArea.bottom() - 5);
   }
}

void QFixedGridWidget::paintEvent(QPaintEvent* event)
{
   QFrame::paintEvent(event);

   QStylePainter p(this);
   // ### Add PixelMetric to change spacers, so style may show border

   // Title must be painted after the frame, since the areas overlap, and
   // the title may wish to extend out to all sides (eg. XP style)
   QStyleOptionDockWidget titleOpt;

   // If we are in a floating tab, init from the parent because the attributes and the geometry
   // of the title bar should be taken from the floating window.
   titleOpt.initFrom(this);
   titleOpt.rect = _titleArea;
   //if (!_titleBarWidgets.isEmpty())
   //   titleOpt.rect.setRight(_titleBarWidgets.first()->pos().x() - 1);
   titleOpt.title = windowTitle();
   titleOpt.closable = false;
   titleOpt.movable = false;
   titleOpt.floatable = false;

   titleOpt.verticalTitleBar = false;

   p.drawControl(QStyle::CE_DockWidgetTitle, titleOpt);
}

void QFixedGridWidget::changeEvent(QEvent* event)
{
   switch (event->type())
   {
   case QEvent::ModifiedChange:
   case QEvent::WindowTitleChange:
      update(_titleArea);
      break;
   default:
      break;
   }
   QFrame::changeEvent(event);
}

bool QFixedGridWidget::event(QEvent* event)
{
   switch (event->type())
   {
   case QEvent::WindowActivate:
   case QEvent::WindowDeactivate:
      update(_titleArea);
      break;
#if 0
      // return true after calling the handler since we don't want
      // them to be passed onto the default handlers
   case QEvent::MouseButtonPress:
      mousePressEvent(static_cast<QMouseEvent *>(event));
         return true;
      break;
   case QEvent::MouseButtonDblClick:
      mouseDoubleClickEvent(static_cast<QMouseEvent *>(event));
         return true;
      break;
   case QEvent::MouseMove:
      mouseMoveEvent(static_cast<QMouseEvent *>(event));
         return true;
      break;
   case QEvent::MouseButtonRelease:
      mouseReleaseEvent(static_cast<QMouseEvent *>(event));
         return true;
      break;
   case QEvent::NonClientAreaMouseMove:
   case QEvent::NonClientAreaMouseButtonPress:
   case QEvent::NonClientAreaMouseButtonRelease:
   case QEvent::NonClientAreaMouseButtonDblClick:
      //d->nonClientAreaMouseEvent(static_cast<QMouseEvent*>(event));
      return true;
   case QEvent::Move:
      moveEvent(static_cast<QMoveEvent*>(event));
      break;
#endif
   }
   return QFrame::event(event);
}

bool QFixedGridWidget::eventFilter(QObject* watched, QEvent* event)
{
   if (event->type() == QEvent::MouseMove)
   {
      if (handleMouseMove(static_cast<QMouseEvent*>(event)))
         return true;
   }

   return QFrame::eventFilter(watched, event);
}

void QFixedGridWidget::onTitleBarWidgetDestroyed(QObject* obj)
{
   _titleBarWidgets.removeAll(qobject_cast<QWidget*>(obj));
   positionTitleBarWidgets();
   update(_titleArea);
}

void QFixedGridWidget::init()
{
   setAttribute(Qt::WA_DeleteOnClose);
   setMouseTracking(true);
   setMinimumSize(30, 20);
   setFrameStyle(StyledPanel | Plain);
   QPalette p;
   p.setColor(QPalette::Window, QApplication::palette().color(QPalette::Window));
   setPalette(p);
   setAutoFillBackground(true);

   if (iconCloseWidget.isNull())
      iconCloseWidget = style()->standardIcon(QStyle::SP_TitleBarCloseButton);
   if (iconEditText.isNull())
      iconEditText = QIcon(":/icons/free/Modify.png");
   if (iconApplyTitleChange.isNull())
      iconApplyTitleChange = style()->standardIcon(QStyle::SP_DialogApplyButton);

   _titleArea.setRect(3, 1, 0, titleHeight());

   qApp->installEventFilter(this);

   setFocusPolicy(Qt::WheelFocus);
}

void QFixedGridWidget::positionTitleBarWidgets() const
{
   if (!_titleArea.isValid())
      return;

   auto x = _titleArea.right() + 1;
   for (auto i = _titleBarWidgets.count(); i--; )
   {
      if (!(_titleBarWidgets.at(i)->windowFlags() & Qt::WA_Resized)) // _titleBarWidgets.at(i)->height() > _titleArea.height()
      {
         _titleBarWidgets.at(i)->resize(_titleBarWidgets.at(i)->sizeHint().scaled(INT_MAX, _titleArea.height() - 2, Qt::KeepAspectRatio));
      }
      x -= _titleBarWidgets.at(i)->width();
      _titleBarWidgets.at(i)->move(x, _titleArea.top());
   }
}

bool QFixedGridWidget::handleMouseMove(QMouseEvent* event)
{
   if (!(event->buttons() & Qt::LeftButton))
   {
      // No drag, just change the cursor shape and return
      _startPosition = StartPosition::none;

#if QT_VERSION >= QT_VERSION_CHECK(6,0,0)
      auto pos = mapFromGlobal(event->globalPosition().toPoint());
#else
      auto pos = mapFromGlobal(event->globalPos());
#endif

      if (!rect().contains(pos))
         return false;

      handleMousePosition(pos);

      return false;
   }

   if (_startPosition == StartPosition::none)
      return false;

   if (_startPosition == StartPosition::move)
      setCursor(Qt::SizeAllCursor);

   auto rect = _dragStartGeometry;
#if QT_VERSION >= QT_VERSION_CHECK(6,0,0)
   auto dist = event->globalPosition().toPoint() - _dragStartPosition;
#else
   auto dist = event->globalPos() - _dragStartPosition;
#endif

   switch (_startPosition)
   {
   case StartPosition::topLeft:
      rect.setLeft(qMax(rect.left() + dist.x(), 0));
      rect.setTop(qMax(rect.top() + dist.y(), 0));
      break;
   case StartPosition::left:
      rect.setLeft(qMax(rect.left() + dist.x(), 0));
      break;
   case StartPosition::bottomLeft:
      rect.setLeft(qMax(rect.left() + dist.x(), 0));
      rect.setBottom(rect.bottom() + dist.y());
      break;
   case StartPosition::bottom:
      rect.setBottom(rect.bottom() + dist.y());
      break;
   case StartPosition::bottomRight:
      rect.setBottomRight(rect.bottomRight() + dist);
      break;
   case StartPosition::right:
      rect.setRight(rect.right() + dist.x());
      break;
   case StartPosition::topRight:
      rect.setRight(rect.right() + dist.x());
      rect.setTop(qMax(rect.top() + dist.y(), 0));
      break;
   case StartPosition::top:
      rect.setTop(qMax(rect.top() + dist.y(), 0));
      break;
   case StartPosition::move:
      rect.moveCenter(rect.center() + dist);
      if (rect.top() < 0)
         rect.moveTop(0);
      if (rect.left() < 0)
         rect.moveLeft(0);
      break;
   }

   setGeometry(rect);

   auto fixedGridView = qobject_cast<QFixedGridView*>(parentWidget());
   if (fixedGridView)
      fixedGridView->previewSnapArea(this);

   return true;
}

QFixedGridWidget::StartPosition QFixedGridWidget::handleMousePosition(const QPoint& pos)
{
   StartPosition startPos;

   if (pos.x() <= 3 && pos.y() <= 3)
   {
      startPos = StartPosition::topLeft;
      setCursor(Qt::SizeFDiagCursor);
   }
   else if (pos.x() <= 3 && pos.y() >= height() - 3)
   {
      startPos = StartPosition::bottomLeft;
      setCursor(Qt::SizeBDiagCursor);
   }
   else if (pos.x() >= width() - 3 && pos.y() <= 3)
   {
      startPos = StartPosition::topRight;
      setCursor(Qt::SizeBDiagCursor);
   }
   else if (pos.x() >= width() - 3 && pos.y() >= height() - 3)
   {
      startPos = StartPosition::bottomRight;
      setCursor(Qt::SizeFDiagCursor);
   }
   else if (pos.x() <= 3)
   {
      startPos = StartPosition::left;
      setCursor(Qt::SizeHorCursor);
   }
   else if (pos.x() >= width() - 3)
   {
      startPos = StartPosition::right;
      setCursor(Qt::SizeHorCursor);
   }
   else if (pos.y() <= 3)
   {
      startPos = StartPosition::top;
      setCursor(Qt::SizeVerCursor);
   }
   else if (pos.y() >= height() - 3)
   {
      startPos = StartPosition::bottom;
      setCursor(Qt::SizeVerCursor);
   }
   else if (_titleArea.contains(pos))
   {
      startPos = StartPosition::move;
      unsetCursor();
   }
   else
   {
      startPos = StartPosition::none;
      unsetCursor();
   }

   return startPos;
}

int QFixedGridWidget::titleHeight() const
{
   //int buttonHeight = qMax(perp(verticalTitleBar, closeSize), perp(verticalTitleBar, floatSize));

   QFontMetrics titleFontMetrics = fontMetrics();
   int mw = style()->pixelMetric(QStyle::PM_DockWidgetTitleMargin, 0, this);
   const auto buttonHeight = 16;

   return qMax(buttonHeight + 2, titleFontMetrics.height() + 2 * mw);
}
