#include "stdafx.h"
#include "QComboBoxEx.h"
#include <QApplication>
#include <QSize>
#include <QMouseEvent>
#include <QPaintEvent>
#include <QEvent>
#include <QPainter>
#include <QStyleOptionComboBox>
#include <QStyle>
#include <QLineEdit>

QComboBoxEx::QComboBoxEx(QWidget* parent) : QWidget(parent), _content(NULL), _isHovering(false), _arrowState(QStyle::State_None), _designer(false)
{
   setFocusPolicy(Qt::NoFocus);
   setSizePolicy(QSizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed, QSizePolicy::ComboBox));
   setAttribute(Qt::WA_Hover);

// For later use
//   QStyleOptionComboBox opt;
//
//   initStyleOption(&opt);
//   opt.subControls = QStyle::SC_All;
//   opt.rect.setRect(0, 0, 100, 13);
//
//   QRect editRect = style()->subControlRect(QStyle::CC_ComboBox, &opt, QStyle::SC_ComboBoxEditField, this);
//
//   this->setContentsMargins(editRect.left(), editRect.top(), 100 - editRect.right(), 13 - editRect.bottom());
}

void QComboBoxEx::setContent(QWidget* w)
{
   if (_content != NULL)
      setFocusProxy(NULL);

   _content = w;

   if (_content != NULL)
   {
      if (w->parent() != this)
         w->setParent(this);

      QLineEdit* lineEdit = qobject_cast<QLineEdit*>(w);

      if (lineEdit)
         lineEdit->setFrame(false);

      //w->setContextMenuPolicy(Qt::NoContextMenu);
      setFocusProxy(w);

      updateContentGeometry();

      if (isVisible())
         w->show();
   }

   update();
}

void QComboBoxEx::setReadOnly()
{
   _arrowState = QStyle::State_ReadOnly;
   if (QLineEdit* lineEdit = qobject_cast<QLineEdit*>(_content))
   {
      QPalette pal;
      pal.setColor(QPalette::Base, QColor(224, 223, 227));

      lineEdit->setPalette(pal);
      lineEdit->setReadOnly(true);
   }
}

bool QComboBoxEx::isInArrowField(const QPoint &point) const
{
   QStyleOptionComboBox opt;

   initStyleOption(&opt);

   return (style()->hitTestComplexControl(QStyle::CC_ComboBox, &opt, point, this) == QStyle::SC_ComboBoxArrow);
}

QSize QComboBoxEx::sizeHint() const
{
   QStyleOptionComboBox opt;

   initStyleOption(&opt);

   QSize size(50, 16);

   if (_content)
   {
      size = _content->sizeHint();
      if (qobject_cast<QLineEdit*>(_content))
         size -= QSize(4, 4); // QLineEdit returns the size with frame
   }

   size = style()->sizeFromContents(QStyle::CT_ComboBox, &opt, size, this);

   return size;
}

QSize QComboBoxEx::minimumSizeHint() const
{
   QStyleOptionComboBox opt;

   initStyleOption(&opt);

   QSize size(0, 16);

   if (_content)
   {
      size = _content->minimumSizeHint();
      if (qobject_cast<QLineEdit*>(_content))
         size -= QSize(4, 4); // QLineEdit returns the size with frame
   }

   size = style()->sizeFromContents(QStyle::CT_ComboBox, &opt, size, this);

   return size;
}

bool QComboBoxEx::event(QEvent *e)
{
   bool isHovering = _isHovering;

   switch(e->type())
   {
   case QEvent::HoverEnter:
   case QEvent::HoverMove:
      if (const QHoverEvent *he = static_cast<const QHoverEvent *>(e))
#if QT_VERSION >= QT_VERSION_CHECK(6,0,0)
         _isHovering = isInArrowField(he->position().toPoint());
#else
         _isHovering = isInArrowField(he->pos());
#endif
      break;
   case QEvent::HoverLeave:
      _isHovering = false;
      break;
   case QEvent::Leave:
      _isHovering = false;
      break;
   default:
      break;
   }

   if (isHovering != _isHovering)
   {
      QStyleOptionComboBox opt;

      initStyleOption(&opt);
      update(style()->subControlRect(QStyle::CC_ComboBox, &opt, QStyle::SC_ComboBoxArrow, this));
   }

   return QWidget::event(e);
}

void QComboBoxEx::mousePressEvent(QMouseEvent* e)
{
   QWidget::mousePressEvent(e);
   if (_arrowState != QStyle::State_ReadOnly && e->button() == Qt::LeftButton && isInArrowField(e->pos()))
   {
      updateArrow(QStyle::State_Sunken);
      emit clicked();
   }
}

void QComboBoxEx::mouseMoveEvent(QMouseEvent* e)
{
   QWidget::mouseMoveEvent(e);
}

void QComboBoxEx::mouseReleaseEvent(QMouseEvent* e)
{
   QWidget::mouseReleaseEvent(e);
   if (_arrowState != QStyle::State_ReadOnly)
      updateArrow(QStyle::State_None);
}

void QComboBoxEx::paintEvent(QPaintEvent* e)
{
   Q_UNUSED(e);

   QPainter             painter(this);
   QStyleOptionComboBox opt;

   initStyleOption(&opt);
   style()->drawComplexControl(QStyle::CC_ComboBox, &opt, &painter, this);
}

// For later use
//void QFrame::drawFrame(QPainter *p)
//{
//    Q_D(QFrame);
//    QStyleOptionFrameV3 opt;
//    opt.init(this);
//    int frameShape  = d->frameStyle & QFrame::Shape_Mask;
//    int frameShadow = d->frameStyle & QFrame::Shadow_Mask;
//    opt.frameShape = Shape(int(opt.frameShape) | frameShape);
//    opt.rect = frameRect();
//    switch (frameShape) {
//        case QFrame::Box:
//        case QFrame::HLine:
//        case QFrame::VLine:
//        case QFrame::StyledPanel:
//        case QFrame::Panel:
//            opt.lineWidth = d->lineWidth;
//            opt.midLineWidth = d->midLineWidth;
//            break;
//        default:
//            // most frame styles do not handle customized line and midline widths
//            // (see updateFrameWidth()).
//            opt.lineWidth = d->frameWidth;
//            break;
//    }
//
//    if (frameShadow == Sunken)
//        opt.state |= QStyle::State_Sunken;
//    else if (frameShadow == Raised)
//        opt.state |= QStyle::State_Raised;
//
//    style()->drawControl(QStyle::CE_ShapedFrame, &opt, p, this);
//}

void QComboBoxEx::resizeEvent(QResizeEvent *e)
{
   QWidget::resizeEvent(e);
   updateContentGeometry();
}

void QComboBoxEx::leaveEvent(QEvent *e)
{
   QWidget::leaveEvent(e);
   if (_arrowState != QStyle::State_ReadOnly)
      updateArrow(QStyle::State_None);
}

void QComboBoxEx::initStyleOption(QStyleOptionComboBox *option) const
{
   if (!option)
     return;

   option->initFrom(this);
   option->editable = true;
   option->frame = true;
   //if (hasFocus() && !option->editable)
   // option->state |= QStyle::State_Selected;
   option->subControls = QStyle::SC_All;
   if (_isHovering)
      option->activeSubControls = QStyle::SC_ComboBoxArrow;
   else
      option->activeSubControls = QStyle::SC_None;
   if (_arrowState == QStyle::State_Sunken)
      option->state |= _arrowState;
   else if (_arrowState == QStyle::State_ReadOnly)
      option->state &= ~QStyle::State_Enabled;
   //if (popup)
   //  option->state |= QStyle::State_On;
}

void QComboBoxEx::updateContentGeometry()
{
    if (!_content)
        return;

    QStyleOptionComboBox opt;

    initStyleOption(&opt);

    QRect editRect = style()->subControlRect(QStyle::CC_ComboBox, &opt, QStyle::SC_ComboBoxEditField, this);

    _content->setGeometry(editRect);
}

void QComboBoxEx::updateArrow(QStyle::StateFlag state)
{
    if (_arrowState == state)
        return;
    _arrowState = state;

    QStyleOptionComboBox opt;

    initStyleOption(&opt);
    update(style()->subControlRect(QStyle::CC_ComboBox, &opt, QStyle::SC_ComboBoxArrow, this));
}
