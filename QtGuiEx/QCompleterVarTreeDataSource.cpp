#include "stdafx.h"
#include "QCompleterVarTreeDataSource.h"

static bool containsAnyOf(const QString& text, const QStringList& list, Qt::CaseSensitivity caseSensitivity)
{
   for (auto&& s : list)
   {
      if (text.contains(s, caseSensitivity))
         return true;
   }

   return false;
}

static const HtNode* bestTreeMatch(const HtNode* node, const QStringList& path, int* bestIndicator = nullptr)
{
   if (path.isEmpty())
      return node;

   auto maxIndicator = 0;
   const HtNode* bestMatchNode = nullptr;

   if (!node)
      return bestMatchNode;

   for (auto&& child : node->children())
   {
      auto indicator = 0;
      const auto matchNode = bestTreeMatch(child, path.mid(1), &indicator);

      indicator += getCorrelationIndicator(child->attribute(0).toString(), path.first());

      if (indicator > maxIndicator)
      {
         maxIndicator = indicator;
         bestMatchNode = matchNode;
      }
   }

   if (bestIndicator)
      *bestIndicator = maxIndicator;

   return bestMatchNode;
}

static const HtNode* treeSearch(const HtNode* node, const QStringList& path, bool caseSensitive)
{
   if (path.isEmpty())
      return node;

   if (!node)
      return nullptr;

   Qt::MatchFlags matchFlags = Qt::MatchFixedString;

   if (caseSensitive)
      matchFlags |= Qt::MatchCaseSensitive;

   return treeSearch(node->find(0, path.first(), 0, matchFlags), path.mid(1), caseSensitive);
}

static void filterTreeDataSource(HtNode* parent, const QStringList& filter, bool isCaseSensitive)
{
   for (int i = parent->childCount(); i--; )
   {
      filterTreeDataSource(parent->child(i), filter, isCaseSensitive);

      if (parent->child(i)->childCount() == 0 && !containsAnyOf(parent->child(i)->attribute(0).toString(), filter, isCaseSensitive ? Qt::CaseSensitive : Qt::CaseInsensitive))
         parent->removeChild(i);
   }
}

QCompleterVarTreeDataSource::QCompleterVarTreeDataSource(const VarTree& varTree, const ResolverFunctionMap& functionMap, QObject *parent)
   : QCompleterHtDataSource(parent), _sourceTree(varTree), _functionMap(functionMap)
{
}

bool QCompleterVarTreeDataSource::filter(const QString& filterText, int filterColumn)
{
   // filterText may start with a '.' and end with a '.'
   auto filterPath = filterText.split(u'.');

   auto rootNode = _model.rootNode();

   rootNode->removeAllChildNodes();

   QStringList functionList;

   if (filterPath.first().isEmpty()) // filterText starts with a '.'
   {
      for (auto it = _functionMap.begin(); it != _functionMap.end(); ++it)
      {
         if (it.value().second)
         {
            functionList.append(it.key() + "()");
         }
      }
   }
   else if (filterPath.count() > 1 || filterPath.first() != "true" || filterPath.first() != "false")
   {
      if (filterPath.first() == "base")
         filterPath.removeFirst();

      if (!filterPath.isEmpty())
         filterPath.removeLast(); // The last element (may be empty) is not included in the search

      auto indexHierarchy = _sourceTree.indexHierarchy(filterPath);

      if (!indexHierarchy.isEmpty() && indexHierarchy.last() == -1)
         indexHierarchy.removeLast();

      auto variable = _sourceTree.root();

      for (auto&& index : indexHierarchy)
      {
         variable = variable->getItem(index);
      }

      if (filterPath.isEmpty())
         rootNode->appendChild()->setAttribute(0, "base");

      const auto names = variable->itemNames();

      for (int i = 0; i < names.count(); ++i)
      {
         auto name = names.at(i);
         if (variable->getItem(i)->basicType() == IVariable::BasicType::Function)
            name += "()";
         rootNode->appendChild()->setAttribute(0, name);
      }

      rootNode->sort(0);

      // Add functions

      for (auto it = _functionMap.begin(); it != _functionMap.end(); ++it)
      {
         if (it.value().second != indexHierarchy.isEmpty())
         {
            functionList.append(it.key() + "()");
         }
      }
   }

   if (rootNode->childCount() && !functionList.isEmpty())
      setSeparator(_model.indexFromNode(rootNode->appendChild()));

   functionList.sort();
   for (auto&& functionName : functionList)
   {
      rootNode->appendChild()->setAttribute(0, functionName);
   }

   // Signal finished

   emit dataReady(filterText, filterColumn);

   return true;
}

void QCompleterVarTreeDataSource::stopFilter()
{
}

QModelIndex QCompleterVarTreeDataSource::search(const QString& text, int column, bool caseSensitive) const
{
   //return _model.indexFromNode(treeSearch(_model.rootNode(), text.split(u'.'), caseSensitive));
   return QCompleterHtDataSource::search(text, column, caseSensitive);
}

QModelIndex QCompleterVarTreeDataSource::find(const QModelIndex& start, int role, const Variant& value, Qt::MatchFlags flags) const
{
   return QCompleterHtDataSource::find(start, role, value, flags);
}

QModelIndex QCompleterVarTreeDataSource::bestMatch(const QString& text, int column) const
{
   if (text.isEmpty())
      return{};

   auto elements = text.split(u'.');

   auto maxIndicator = 0;
   const HtNode* bestMatchNode = nullptr;

   for (auto&& child : _model.rootNode()->children())
   {
      const auto indicator = getCorrelationIndicator(child->attribute(0).toString(), elements.last());

      if (indicator > maxIndicator)
      {
         maxIndicator = indicator;
         bestMatchNode = child;
      }
   }

   return _model.indexFromNode(bestMatchNode);
}
