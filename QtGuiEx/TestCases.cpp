#include "stdafx.h"
#include "TestCases.h"
#include <HtStandardModel.h>
#include "QTableViewEx.h"
#include "QDialogEx.h"
#include <QHBoxLayout>
#include <QApplication>
#include <QNumberEdit.h>
#include <QDateEditEx.h>
#include "QRichText.h"
#include "QTextLayoutEx.h"
#include <QHeaderView>
#include "QTextDocumentTemplate.h"
#include "QTreeViewEx.h"
#include "QExtendedProxyStyle.h"
#include <QStyleFactory>
#include <QExpressionEditor.h>
#include <CPPResolverFunctions.h>
#include <CPPTokenizer.h>
#include <QtGuiVariantTypes.h>
#include "QSelectorComboDelegate.h"
#include "QCompleterEx.h"
#include "QLineEditDelegate.h"

testMenuTreeStyle::testMenuTreeStyle(QStyle* style) : QProxyStyle(style)
{
}

int testMenuTreeStyle::styleHint(const StyleHint hint, const QStyleOption* option, const QWidget* widget, QStyleHintReturn* returnData) const
{
   switch (hint)
   {
   case SH_DitherDisabledText:
      return int(false);
   case SH_EtchDisabledText:
      return int(true);
   default:
      return QProxyStyle::styleHint(hint, option, widget, returnData);
   }
}

void testMenuTreeStyle::drawPrimitive(const PrimitiveElement element, const QStyleOption* option, QPainter* painter, const QWidget* widget) const
{
   switch (element)
   {
   case PE_IndicatorBranch:
      if (option->state & State_Children)
      {
         painter->save();
         painter->setRenderHint(QPainter::Antialiasing);

         QPainterPath indicator(QPoint(-3, -3));
         indicator.lineTo(3, 0);
         indicator.lineTo(-3, 3);
         indicator.closeSubpath();
         if (option->state & QStyle::State_Open)
         {
            QTransform transform;

            transform.rotate(45);
            indicator = transform.map(indicator);
         }
         indicator.translate(option->rect.center() + QPoint(0, 1));

         painter->drawPath(indicator);
         if (option->state & State_MouseOver)
         {
            painter->fillPath(indicator, Qt::cyan);
         }

         painter->restore();
      }
      return;
   case PE_PanelItemViewRow:
   case PE_PanelItemViewItem:

      QProxyStyle::drawPrimitive(element, option, painter, widget);

      if (option->rect.bottomLeft().x() > 0)
      {
         painter->save();
         painter->setPen(option->palette.color(QPalette::Dark));
         painter->drawLine(option->rect.bottomLeft(), option->rect.bottomRight());
         painter->restore();
      }

      break;

   default:

      QProxyStyle::drawPrimitive(element, option, painter, widget);
   }
}

void GuiTest::textLayoutTest()
{
   if (!this->parentWindow())
   {
      addWarning(tr("textLayoutTest skipped"));
      return;
   }

   //auto fontIndex = QFontDatabase::addApplicationFont("s:\\Database\\ufonts.com_urw-chancery-l-medium-italic.ttf"); // Kingthings Christmas 2.2.ttf
   //auto fontFamilies = QFontDatabase::applicationFontFamilies(fontIndex);

   QTextLayoutEx textLayout(QString("Die Glocken von Isola Bella"), QFont("Arial", 16));

   textLayout.createLayout(250);

   T_ASSERT_EQUAL(textLayout.lineCount(), 2);

   auto size = textLayout.size();

   auto width = textLayout.lineAt(0).horizontalAdvance();

   textLayout.createLayout(size.width());

   T_ASSERT_EQUAL(textLayout.lineCount(), 2);
}

void GuiTest::tableViewDialog()
{
   if (!this->parentWindow())
   {
      addWarning(tr("tableViewDialog skipped"));
      return;
   }

   const bool hasOverrideCursor = QApplication::overrideCursor();

   if (hasOverrideCursor)
      QApplication::restoreOverrideCursor();

   QDialogEx dialog(parentWindow());

   dialog.resize(640, 480);

   auto comboDataSource = new QCompleterHtDataSource(&dialog);

   comboDataSource->hierarchicalTable()->rootNode()->appendChild()->setAttribute(0, "First");
   comboDataSource->hierarchicalTable()->rootNode()->appendChild()->setAttribute(0, "Second");

   QTableViewEx* tableView = new QTableViewEx;

   tableView->setSortingEnabled(true);
   tableView->setSelectionMode(QAbstractItemView::SingleSelection);
   tableView->setSelectionBehavior(QAbstractItemView::SelectRows);
   tableView->setAlternatingRowColors(true);
   tableView->verticalHeader()->show();
   tableView->setEditDelegateForColumn(2, new QSelectorComboDelegate(tableView, comboDataSource));
   tableView->setCheckOnMouseMove(true);

   HtStandardModel model;

   model.setHeader(0, QString());
   model.setHeader(1, QString("Text"));
   model.setHeader(2, QString("Combo"));
   model.setHeader(3, QString("Test2"));

   model.setSortRole(Qt::CheckStateRole, 0);

   model.rootNode()->addDefaultFlags(0, Qt::ItemIsUserCheckable, Qt::ItemFlagsRole);
   model.rootNode()->addDefaultFlags(2, Qt::ItemIsEditable, Qt::ItemFlagsRole);

   model.rootNode()->child(0)->setFlags(0, Qt::Unchecked, Qt::CheckStateRole);
   model.rootNode()->child(0)->setAttribute(1, QString("Erste Spalte"));
   model.rootNode()->child(0)->setAttribute(1, Qt::AlignCenter, Qt::TextAlignmentRole);
   model.rootNode()->child(0)->setAttribute(2, QString("First"));
   model.rootNode()->child(0)->setAttribute(3, QString("Zweite Spalte"));

   model.rootNode()->child(1)->setFlags(0, Qt::Checked, Qt::CheckStateRole);
   model.rootNode()->child(1)->setAttribute(0, QString(""));
   model.rootNode()->child(1)->addFlags(0, Qt::ItemIsEditable, Qt::ItemFlagsRole);
   model.rootNode()->child(1)->setAttribute(1, QString("Viel Text der auch mal umgebrochen werden kann."));
   model.rootNode()->child(1)->setAttribute(2, QString("Second"));
   model.rootNode()->child(1)->setAttribute(3, QString("Ein bisschen Text"));

   tableView->setModel(&model);
   tableView->resizeColumnsToContents();

   QHBoxLayout* layout = new QHBoxLayout;

   layout->addWidget(tableView);

   dialog.setLayout(layout);

   dialog.exec();

   if (hasOverrideCursor)
      QApplication::setOverrideCursor(Qt::WaitCursor);
}

void GuiTest::treeViewDialog()
{
   if (!this->parentWindow())
   {
      addWarning(tr("tableViewDialog skipped"));
      return;
   }

   const bool hasOverrideCursor = QApplication::overrideCursor();

   if (hasOverrideCursor)
      QApplication::restoreOverrideCursor();

   QDialogEx dialog(parentWindow());

   dialog.resize(600, 480);

   auto treeView = new QTreeViewEx;

   treeView->enableColumnResize(true);
   treeView->setAlternatingRowColors(true);
   treeView->setCheckOnMouseMove(true);

   HtStandardModel model;

   model.setHeader(0, "Name");
   model.setHeader(1, "Long text");

   model.rootNode()->addDefaultFlags(0, Qt::ItemIsUserCheckable, Qt::ItemFlagsRole);

   for (auto i = 0; i < 1000; ++i)
   {
      auto parentNode = model.rootNode()->appendChild();

      parentNode->setAttribute(0, QString("Parent %1").arg(i));
      parentNode->setAttribute(0, (int)Qt::Unchecked, Qt::CheckStateRole);

      parentNode->child(0)->setAttribute(0, QString("Zeile %1").arg(i));
      parentNode->child(0)->setAttribute(0, (int)Qt::Unchecked, Qt::CheckStateRole);
      parentNode->child(0)->setAttribute(
         1,
         R"(The Hypertext Transfer Protocol (HTTP) is an application-level protocol for distributed, collaborative, hypermedia
information systems. It is a generic, stateless, protocol which can be used for many tasks beyond its use for
hypertext, such as name servers and distributed object management systems, through extension of its request
methods, error codes and headers [47]. A feature of HTTP is the typing and negotiation of data representation,
allowing systems to be built independently of the data being transferred.)");
   }

   treeView->setModel(&model);
   treeView->setWordWrap(true);
   treeView->resizeColumnToContents(0);

   QHBoxLayout* layout = new QHBoxLayout;

   layout->addWidget(treeView);

   dialog.setLayout(layout);

   dialog.exec();

   if (hasOverrideCursor)
      QApplication::setOverrideCursor(Qt::WaitCursor);
}

void GuiTest::expressionEditor()
{
   if (!this->parentWindow())
   {
      addWarning(tr("tableViewDialog skipped"));
      return;
   }

   const bool hasOverrideCursor = QApplication::overrideCursor();

   if (hasOverrideCursor)
      QApplication::restoreOverrideCursor();

   VarTree varTree;

   varTree.set("Variable1", 42);
   varTree.set("Variable2", "Hallo");
   varTree.set("List.Var1", 5);
   varTree.set("List.Var2", "Welt");

   QExpressionEditor editor(varTree, createCppResolverFunctions(), createVersatileExpressionContext(), this->parentWindow());

   editor.exec();

   if (hasOverrideCursor)
      QApplication::setOverrideCursor(Qt::WaitCursor);
}

void GuiTest::testNumberEdit()
{
   if (!this->parentWindow())
   {
      addWarning(tr("testNumberEdit skipped"));
      return;
   }

   QNumberEdit numberEdit;

   numberEdit.setValue(0);
   numberEdit.setZeroAllowed(false);
   T_EQUALS(numberEdit.text(), QString("0,01"));

   numberEdit.setFractionalDigits(0);
   T_EQUALS(numberEdit.text(), QString("1"));

   numberEdit.setValue(12);
   numberEdit.setZeroAllowed(true);
   T_EQUALS(numberEdit.text(), QString("12"));
}

void GuiTest::testDateEditEx()
{
   if (!this->parentWindow())
   {
      addWarning(tr("testDateEditEx skipped"));
      return;
   }

   QDialogEx dlg(this->parentWindow());

   QDateEditEx dateEdit(&dlg);

   dlg.exec();
}

void GuiTest::testDocumentTemplate()
{
   VarTree varTree;

   varTree.set("Stammdaten.ISIN", "DE00001000");

   QTextDocument sourceDocument;

   sourceDocument.setHtml(R"(<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN" "http://www.w3.org/TR/REC-html40/strict.dtd">
<html><head><meta name="qrichtext" content="1" /><style type="text/css">
p, li { white-space: pre-wrap; }
</style></head><body style=" font-family:'MS Shell Dlg 2'; font-size:8.25pt; font-weight:400; font-style:normal;">
<p style=" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;">Testtext für die Freigabe mit ISIN {Stammdaten.ISIN}{{{Stammdaten.ISIN}}}.</p></body></html>)");

   QTextDocument expectedDocument;

   expectedDocument.setHtml(R"(<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN" "http://www.w3.org/TR/REC-html40/strict.dtd">
<html><head><meta name="qrichtext" content="1" /><style type="text/css">
p, li { white-space: pre-wrap; }
</style></head><body style=" font-family:'MS Shell Dlg 2'; font-size:8.25pt; font-weight:400; font-style:normal;">
<p style=" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;">Testtext für die Freigabe mit ISIN DE00001000{DE00001000}.</p></body></html>)");

   resolveDocumentVariables(&sourceDocument, varTree.root());

   T_EQUALS(sourceDocument.toHtml(), expectedDocument.toHtml());
}

void GuiTest::testQTreeViewEx()
{
   if (!this->parentWindow())
   {
      addWarning(tr("tableViewDialog skipped"));
      return;
   }

   const bool hasOverrideCursor = QApplication::overrideCursor();

   if (hasOverrideCursor)
      QApplication::restoreOverrideCursor();

   QDialogEx dialog(parentWindow());
   dialog.resize(640, 480);

   const auto model = new HtStandardModel();
   model->setHeader(0, tr("Function"));
   model->setHeader(1, tr("Key"));

   const auto treeView = new QTreeViewEx;
   treeView->header()->setDefaultSectionSize(30);

   const auto testStyle = new testMenuTreeStyle(new QExtendedProxyStyle(QExtendedProxyStyle::EntireCellCheckBoxPosition::Full, QStyleFactory::create("WindowsVista")));
   testStyle->setParent(treeView);
   treeView->setStyle(testStyle);

   QPalette pal;
   pal.setColor(QPalette::Dark, Qt::lightGray);
   treeView->setPalette(pal);

   treeView->setMinimumRowHeight(30);
   treeView->header()->setStretchLastSection(false);
   treeView->header()->hide();

   if (model)
   {
      treeView->setModel(model);

      treeView->header()->setSectionResizeMode(0, QHeaderView::Stretch);
      treeView->header()->setSectionResizeMode(1, QHeaderView::Fixed);
      treeView->showColumnSeparator(true);
      treeView->enableColumnResize(true);
   }

   const auto node = model->rootNode();

   const auto child1 = node->appendChild();
   child1->setAttribute(0, "Eintrag 1");
   child1->setAttribute(0,QIcon(":/icons/free/Boss.png"), Qt::DecorationRole);
   child1->addFlags(0, Qt::ItemIsEditable, Qt::ItemFlagsRole);
   child1->addFlags(0, Qt::ItemNeverHasChildren, Qt::ItemFlagsRole);

   const auto child2 = node->appendChild();
   child2->setAttribute(0, "Eintrag 2");
   child2->setAttribute(0,QIcon(":/icons/free/Error.png"), Qt::DecorationRole);
   child2->addFlags(0, Qt::ItemIsEditable, Qt::ItemFlagsRole);
   child2->addFlags(0, Qt::ItemNeverHasChildren, Qt::ItemFlagsRole);

   treeView->header()->resizeSection(1, 80);
   treeView->setRootIsDecorated(false);
   treeView->setEditDelegateForColumn(0, new QLineEditDelegate(&dialog));
   treeView->setEditTriggers(QAbstractItemView::SelectedClicked | QAbstractItemView::EditKeyPressed);

   const auto layout = new QVBoxLayout(&dialog);
   layout->setContentsMargins(3, 0, 3, 3);
   layout->addWidget(treeView);

   // Execute the dialog
   dialog.exec();

   if (hasOverrideCursor)
      QApplication::setOverrideCursor(Qt::WaitCursor);
}

BEGIN_TEST_SUITES
   TEST_SUITE(GuiTest);
END_TEST_SUITES
