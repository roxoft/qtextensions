#include "stdafx.h"
#include "QIncrementalSearchWidget.h"
#include "ui_QIncrementalSearchWidget.h"
#include "QHighlightItemDelegate.h"
#include <QLocaleEx.h>
#include <QModelIndex>
#include <QShowEvent>
#include <QHeaderView>
#include <QTableView>
#include <QTreeView>
#include <QTimer>
#include <QCompleter>

void QIncrementalSearchWidget::ViewInfo::visibleCellForward(int &row, int &column, QModelIndex &parent) const // row and column have to be visual indexes
{
   for (;;)
   {
      const int visibleRow = visibleRowForward(row, parent);

      if (visibleRow != row)
      {
         column = 0;
         row = visibleRow;
      }

      if (row >= 0 && row < rowCount(parent))
      {
         column = visibleColumnForward(column, parent);

         if (column >= 0 && column < columnCount(parent))
            break;

         parent = model()->index(logicalRow(row), 0, parent);
         if (rowCount(parent))
            row = 0;
         else
         {
            parent = parent.parent();
            row++;
         }
         column = 0;
      }
      else
      {
         // Try next parent row
         if (!parent.isValid())
         {
            row = -1;
            column = -1;
            break;
         }

         row = visualRow(parent.row());
         parent = parent.parent();
         row++;
      }
   }
}

void QIncrementalSearchWidget::ViewInfo::visibleCellBackward(int &row, int &column, QModelIndex &parent) const // row and column have to be visual indexes
{
   for (;;)
   {
      const int visibleRow = visibleRowBackward(row, parent);

      if (visibleRow != row)
      {
         column = columnCount(parent) - 1;
         row = visibleRow;
      }

      if (row >= 0 && row < rowCount(parent))
      {
         if (column == -1)
         {
            // Last child
            parent = model()->index(logicalRow(row), 0, parent);
            if (rowCount(parent))
               row = rowCount(parent) - 1;
            else
            {
               parent = parent.parent();
               column = columnCount(parent) - 1;
            }
         }

         if (column != -1)
         {
            column = visibleColumnBackward(column, parent);

            if (column >= 0 && column < columnCount(parent))
               break;

            row--;
            column = -1;
         }
      }
      else
      {
         if (!parent.isValid())
         {
            row = -1;
            column = -1;
            break;
         }

         row = visualRow(parent.row());
         parent = parent.parent();
         column = columnCount(parent) - 1;
      }
   }
}

int QIncrementalSearchWidget::TreeViewInfo::visualColumn(int column) const
{
   return _treeView->header()->visualIndex(column);
}

int QIncrementalSearchWidget::TreeViewInfo::logicalColumn(int column) const
{
   return _treeView->header()->logicalIndex(column);
}

int QIncrementalSearchWidget::TreeViewInfo::visibleRowForward(int row, const QModelIndex &parent) const
{
   int count = rowCount(parent);

   while (row < count && _treeView->isRowHidden(row, parent))
      row++;

   return row;
}

int QIncrementalSearchWidget::TreeViewInfo::visibleColumnForward(int column, const QModelIndex &parent) const
{
   int count = columnCount(parent);

   while (column < count && _treeView->isColumnHidden(logicalColumn(column)))
      column++;

   return column;
}

int QIncrementalSearchWidget::TreeViewInfo::visibleRowBackward(int row, const QModelIndex &parent) const
{
   while (row >= 0 && _treeView->isRowHidden(row, parent))
      row--;

   return row;
}

int QIncrementalSearchWidget::TreeViewInfo::visibleColumnBackward(int column, const QModelIndex &) const
{
   while (column >= 0 && _treeView->isColumnHidden(logicalColumn(column)))
      column--;

   return column;
}

int QIncrementalSearchWidget::TreeViewInfo::rowCount(const QModelIndex &parent) const
{
   return _treeView->model()->rowCount(parent);
}

int QIncrementalSearchWidget::TreeViewInfo::columnCount(const QModelIndex &parent) const
{
   return qMin(_treeView->model()->columnCount(parent), _treeView->header()->count());
}

const QAbstractItemModel *QIncrementalSearchWidget::TreeViewInfo::model() const
{
   return _treeView->model();
}

QHighlightItemDelegate* QIncrementalSearchWidget::TreeViewInfo::highlightDelegate() const
{
   return qobject_cast<QHighlightItemDelegate*>(_treeView->itemDelegate());
}


int QIncrementalSearchWidget::TableViewInfo::visualRow(int row) const
{
   return _tableView->verticalHeader()->visualIndex(row);
}

int QIncrementalSearchWidget::TableViewInfo::logicalRow(int row) const
{
   return _tableView->verticalHeader()->logicalIndex(row);
}

int QIncrementalSearchWidget::TableViewInfo::visualColumn(int column) const
{
   return _tableView->horizontalHeader()->visualIndex(column);
}

int QIncrementalSearchWidget::TableViewInfo::logicalColumn(int column) const
{
   return _tableView->horizontalHeader()->logicalIndex(column);
}

int QIncrementalSearchWidget::TableViewInfo::visibleRowForward(int row, const QModelIndex &parent) const
{
   int count = rowCount(parent);

   while (row < count && _tableView->isRowHidden(logicalRow(row)))
      row++;

   return row;
}

int QIncrementalSearchWidget::TableViewInfo::visibleColumnForward(int column, const QModelIndex &parent) const
{
   int count = columnCount(parent);

   while (column < count && _tableView->isColumnHidden(logicalColumn(column)))
      column++;

   return column;
}

int QIncrementalSearchWidget::TableViewInfo::visibleRowBackward(int row, const QModelIndex &) const
{
   while (row >= 0 && _tableView->isRowHidden(logicalRow(row)))
      row--;

   return row;
}

int QIncrementalSearchWidget::TableViewInfo::visibleColumnBackward(int column, const QModelIndex &) const
{
   while (column >= 0 && _tableView->isColumnHidden(logicalColumn(column)))
      column--;

   return column;
}

int QIncrementalSearchWidget::TableViewInfo::rowCount(const QModelIndex &parent) const
{
   return qMin(_tableView->model()->rowCount(parent), _tableView->verticalHeader()->count());
}

int QIncrementalSearchWidget::TableViewInfo::columnCount(const QModelIndex &parent) const
{
   return qMin(_tableView->model()->columnCount(parent), _tableView->horizontalHeader()->count());
}

const QAbstractItemModel *QIncrementalSearchWidget::TableViewInfo::model() const
{
   return _tableView->model();
}

QHighlightItemDelegate* QIncrementalSearchWidget::TableViewInfo::highlightDelegate() const
{
   return qobject_cast<QHighlightItemDelegate*>(_tableView->itemDelegate());
}

QIncrementalSearchWidget::QIncrementalSearchWidget(QWidget* parent)
   : QWidget(parent),
   _ui(new Ui::QIncrementalSearchWidget),
   _timer(new QTimer)
{
   _ui->setupUi(this);

   _ui->leftButton->setIcon(style()->standardIcon(QStyle::SP_ArrowLeft));
   _ui->rightButton->setIcon(style()->standardIcon(QStyle::SP_ArrowRight));

   _ui->columnOnlyCheckBox->setText(specialOptionText());

   _ui->lineEdit->installEventFilter(this);
   _ui->lineEdit->setClearButtonEnabled(true);
   QCompleter* completer = new QCompleter(QStringList(), _ui->lineEdit);
   completer->setCaseSensitivity(Qt::CaseInsensitive);
   _ui->lineEdit->setCompleter(completer);

   _timer->setInterval(2000);
   _timer->setSingleShot(true);
   connect(_timer, SIGNAL(timeout()), this, SLOT(clearInformationLabel()));
}

QIncrementalSearchWidget::~QIncrementalSearchWidget()
{
   delete _viewInfo;
   delete _timer;
   delete _ui;
}

void QIncrementalSearchWidget::setItemView(QTableView *tableView)
{
   _itemView = nullptr;
   delete _viewInfo;
   _viewInfo = nullptr;
   _viewOption = VO_Table;

   if (tableView == nullptr)
      return;

   _itemView = tableView;

   _viewInfo = new TableViewInfo(tableView);

   _caseSensitivity = _ui->caseSensitiveCheckBox->isChecked() ? Qt::CaseSensitive : Qt::CaseInsensitive;
   _matchWord = _ui->wordCheckBox->isChecked();
   _column = -1;
   _level = -1;
   _sectionHeader.clear();

   if (_viewInfo->highlightDelegate())
      _viewInfo->highlightDelegate()->setSearchWidget(this);

   _ui->columnOnlyCheckBox->setText(specialOptionText());

   on_lineEdit_textEdited(_ui->lineEdit->text());
}

void QIncrementalSearchWidget::setItemView(QTreeView *treeView, ViewOption viewOption)
{
   _itemView = nullptr;
   delete _viewInfo;
   _viewInfo = nullptr;
   _viewOption = viewOption;

   if (treeView == nullptr)
      return;

   _itemView = treeView;

   _viewInfo = new TreeViewInfo(treeView);

   _caseSensitivity = _ui->caseSensitiveCheckBox->isChecked() ? Qt::CaseSensitive : Qt::CaseInsensitive;
   _matchWord = _ui->wordCheckBox->isChecked();
   _column = -1;
   _level = -1;
   _sectionHeader.clear();

   if (_viewInfo->highlightDelegate())
      _viewInfo->highlightDelegate()->setSearchWidget(this);

   _ui->columnOnlyCheckBox->setText(specialOptionText());

   on_lineEdit_textEdited(_ui->lineEdit->text());
}

void QIncrementalSearchWidget::setColumnFilterEnabled(bool enabled)
{
   if (!enabled)
      on_columnOnlyCheckBox_toggled(false);
   _ui->columnOnlyCheckBox->setVisible(enabled);
}

bool QIncrementalSearchWidget::processKeyEvent(QKeyEvent* e)
{
   if (e->key() == Qt::Key_F && e->modifiers() == Qt::ControlModifier)
   {
      show();
      return true;
   }

   if (e->key() == Qt::Key_F3 && e->modifiers() == Qt::NoModifier)
   {
      if (isVisible())
         moveNext();
      else
         show();
      return true;
   }

   if(e->key() == Qt::Key_Enter || e->key() == Qt::Key_Return)
   {
      on_lineEdit_textEdited(_ui->lineEdit->text());
      return true;
   }

   return false;
}

QList<int> QIncrementalSearchWidget::match(const QModelIndex& index, const QString& displayText) const
{
   QList<int> matchPositions;

   if (!index.isValid() || _pattern.isEmpty())
      return matchPositions;

   // If the search is restricted to a column return if we are in another column
   if (_column != -1 && index.column() != _column)
      return matchPositions;

   // If the search is restricted to one level return if we are in a different level
   if (_level != -1)
   {
      int         level = _level;
      QModelIndex parent(index.sibling(index.row(), 0).parent());

      while (level && parent.isValid())
      {
         level--;
         parent = parent.parent();
      }
      if (level || parent.isValid())
         return matchPositions;
   }

   // If there is a section header (for BusinessPartnerAdministration) restrict the search to this section header
   if (!_sectionHeader.isEmpty())
   {
      QModelIndex parent(index.sibling(index.row(), 0).parent());

      if (parent.isValid())
      {
         if (parent.data().toString() != _sectionHeader)
            return matchPositions;
      }
      else
      {
         if (index.model()->headerData(0, Qt::Horizontal).toString() != _sectionHeader)
            return matchPositions;
      }
   }

   // Match the display text
   if (!_matchWord)
   {
      int pos = 0;

      while (pos < displayText.length())
      {
         pos = displayText.indexOf(_pattern, pos, _caseSensitivity);
         if (pos == -1)
            break;
         matchPositions.append(pos);
         pos += _pattern.length();
      }

      return matchPositions;
   }

   // Split the string into words
   int pos = 0;

   for (;;)
   {
      while (pos < displayText.length() && displayText.at(pos).isSpace())
         pos++;

      if (pos == displayText.length())
         break;

      int start = pos;

      while (pos < displayText.length() && !displayText.at(pos).isSpace())
         pos++;

      if (displayText.mid(start, pos - start).compare(_pattern, _caseSensitivity) == 0)
         matchPositions.append(start);
   }

   return matchPositions;
}

bool QIncrementalSearchWidget::moveNext()
{
   if (matchForward(true))
   {
      displayInformation(tr("Search started from top"));
      return true;
   }
   return false;
}

bool QIncrementalSearchWidget::movePrev()
{
   if (matchBackward(true))
   {
      displayInformation(tr("Search started from bottom"));
      return true;
   }
   return false;
}

bool QIncrementalSearchWidget::eventFilter(QObject* watched, QEvent* e)
{
   if (e->type() == QEvent::KeyPress)
   {
      QKeyEvent* keyEvent = static_cast<QKeyEvent*>(e);

      if (keyEvent->key() == Qt::Key_Escape)
      {
         clear();
         hide();
         return true;
      }
      return false;
   }
   return QWidget::eventFilter(watched, e);
}

void QIncrementalSearchWidget::showEvent(QShowEvent* e)
{
   if (!e->spontaneous())
   {
      _ui->lineEdit->setFocus();
      _ui->lineEdit->selectAll();
      on_lineEdit_textEdited(_ui->lineEdit->text());
   }
}

void QIncrementalSearchWidget::hideEvent(QHideEvent* e)
{
   if (!e->spontaneous() && _itemView)
   {
      _pattern.clear();
      _itemView->viewport()->update();
   }
}

void QIncrementalSearchWidget::on_closeButton_clicked()
{
   clear();
   hide();
}

void QIncrementalSearchWidget::on_lineEdit_textEdited(const QString& text)
{
   if (_itemView)
   {
      if (_pattern == text)
         addToHistory(text);

      _pattern = text;

      matchForward();
      if (_itemView)
         _itemView->viewport()->update();
   }
}

void QIncrementalSearchWidget::on_rightButton_clicked()
{
   moveNext();
}

void QIncrementalSearchWidget::on_leftButton_clicked()
{
   movePrev();
}

void QIncrementalSearchWidget::on_caseSensitiveCheckBox_toggled(bool checked)
{
   if (_itemView)
   {
      _caseSensitivity = checked ? Qt::CaseSensitive : Qt::CaseInsensitive;
      on_lineEdit_textEdited(_ui->lineEdit->text());
   }
}

void QIncrementalSearchWidget::on_wordCheckBox_toggled(bool checked)
{
   if (_itemView)
   {
      _matchWord = checked;
      on_lineEdit_textEdited(_ui->lineEdit->text());
   }
}

void QIncrementalSearchWidget::on_columnOnlyCheckBox_toggled(bool checked)
{
   if (_itemView)
   {
      if (checked)
      {
         QModelIndex index = _itemView->currentIndex();

         if (!index.isValid())
            _ui->columnOnlyCheckBox->setChecked(false);
         else
         {
            if (_viewOption == VO_SubSections)
            {
               // Calculate the depth (level) of our cell
               int level = 0;
               QModelIndex parent(index.sibling(index.row(), 0).parent());

               while (parent.isValid())
               {
                  level++;
                  parent = parent.parent();
               }

               if (level == 0)
                  _sectionHeader = _itemView->model()->headerData(0, Qt::Horizontal).toString();
               else if (level == 1)
                  _sectionHeader = index.sibling(index.row(), 0).data().toString();
               else
                  _sectionHeader = index.sibling(index.row(), 0).parent().data().toString();

               _ui->columnOnlyCheckBox->setText(specialOptionText(_sectionHeader));
            }
            else
            {
               _column = index.column();
               _ui->columnOnlyCheckBox->setText(specialOptionText(_itemView->model()->headerData(index.column(), Qt::Horizontal).toString()));
            }

            on_lineEdit_textEdited(_ui->lineEdit->text());
         }
      }
      else
      {
         _column = -1;
         _level = -1;
         _sectionHeader.clear();
         _ui->columnOnlyCheckBox->setText(specialOptionText());

         on_lineEdit_textEdited(_ui->lineEdit->text());
      }
   }
}

void QIncrementalSearchWidget::clearInformationLabel()
{
   _ui->informationLabel->clear();
}

void QIncrementalSearchWidget::displayInformation(const QString& text)
{
   _ui->informationLabel->setText(text);
   _timer->start();
}

QString QIncrementalSearchWidget::specialOptionText(const QString& column) const
{
   if (_viewOption == VO_SubSections)
   {
      if (column.isEmpty())
         return tr("inside group only");
      return tr("inside group %1").arg(column);
   }
   if (column.isEmpty())
      return tr("inside current column");
   return tr("inside column '%1'").arg(column);
}

bool QIncrementalSearchWidget::matchForward(bool step)
{
   if (_itemView == nullptr || _itemView->model() == nullptr || _pattern.isEmpty())
      return false;

   QModelIndex current(_itemView->currentIndex());
   int         row = 0; // Visual row!
   int         column = 0; // Visual column!
   QModelIndex parent;

   if (current.isValid())
   {
      row = _viewInfo->visualRow(current.row());
      column = _viewInfo->visualColumn(current.column());
      parent = current.parent();

      if (step)
         column++;
   }

   bool        wrapAround = false;
   QModelIndex startIndex;

   _ui->lineEdit->setPalette(QPalette());
   for (;;)
   {
      // Nearest visible cell
      _viewInfo->visibleCellForward(row, column, parent);

      if (row == -1 || column == -1)
      {
         row = 0;
         column = 0;
         parent = QModelIndex();
         wrapAround = true;

         _viewInfo->visibleCellForward(row, column, parent);
      }

      QModelIndex index;

      if (row != -1 && column != -1)
         index = _itemView->model()->index(_viewInfo->logicalRow(row), _viewInfo->logicalColumn(column), parent);

      if (!index.isValid())
         break;

      if (!match(index, toQString(index.data()).replace(QLatin1Char('\n'), QChar::LineSeparator)).isEmpty()) // From QStyledItemDelegateEx::displayText
      {
         _itemView->setCurrentIndex(index);
         break;
      }

      if (!startIndex.isValid())
         startIndex = index;
      else if (index == startIndex)
      {
         QPalette palette;

         palette.setColor(QPalette::Text, Qt::red);
         _ui->lineEdit->setPalette(palette);
         break;
      }

      // Next cell
      column++;
   }

   return wrapAround;
}

bool QIncrementalSearchWidget::matchBackward(bool step)
{
   if (_itemView == nullptr || _itemView->model() == nullptr || _pattern.isEmpty())
      return false;

   QModelIndex current(_itemView->currentIndex());
   int         row = -1; // Visual row!
   int         column = -1; // Visual column!
   QModelIndex parent;

   if (current.isValid())
   {
      row = _viewInfo->visualRow(current.row());
      column = _viewInfo->visualColumn(current.column());
      parent = current.parent();

      if (step)
         column--;
   }
   else
   {
      row = _viewInfo->rowCount(parent);
   }

   bool        wrapAround = false;
   QModelIndex startIndex;

   _ui->lineEdit->setPalette(QPalette());
   for (;;)
   {
      // Nearest visible cell
      if (column == -1)
         row--;
      _viewInfo->visibleCellBackward(row, column, parent);

      if (row == -1 || column == -1)
      {
         row = _viewInfo->rowCount(parent) - 1;
         column = -1;
         parent = QModelIndex();
         wrapAround = true;

         _viewInfo->visibleCellBackward(row, column, parent);
      }

      QModelIndex index;

      if (row != -1 && column != -1)
         index = _itemView->model()->index(_viewInfo->logicalRow(row), _viewInfo->logicalColumn(column), parent);

      if (!index.isValid())
         break;

      if (!match(index, toQString(index.data()).replace(QLatin1Char('\n'), QChar::LineSeparator)).isEmpty())
      {
         _itemView->setCurrentIndex(index);
         break;
      }

      if (!startIndex.isValid())
         startIndex = index;
      else if (index == startIndex)
      {
         QPalette palette;

         palette.setColor(QPalette::Text, Qt::red);
         _ui->lineEdit->setPalette(palette);
         break;
      }

      // Prev cell
      column--;
   }

   return wrapAround;
}

void QIncrementalSearchWidget::addToHistory(const QString& text)
{
    if (m_history.indexOf(text) != -1)
        return;
    if (_ui->lineEdit->completer() == nullptr)
        return;
    m_history.append(text);
    QStringListModel* model = qobject_cast<QStringListModel*>(_ui->lineEdit->completer()->model());
    if (model == nullptr)
    {
        Q_ASSERT(false);
        return;
    }
    model->setStringList(m_history);
}

void QIncrementalSearchWidget::clear()
{
   _pattern = QString();
   _ui->lineEdit->setText(_pattern);
   _ui->wordCheckBox->setChecked(false);
   _ui->columnOnlyCheckBox->setChecked(false);
   _ui->caseSensitiveCheckBox->setChecked(false);
}
