#include "stdafx.h"
#include "QDateOfYearValidator.h"

QValidator::State QDateOfYearValidator::validate(QString& input, int& pos) const
{
   if (input.isEmpty())
      return Acceptable;

   int pointPos = input.indexOf(_separator);

   if (pointPos == -1)
   {
      pointPos = pos;
      input.insert(pointPos, _separator);
   }
   else
   {
      for (;;)
      {
         int second = input.indexOf(_separator, pointPos + 1);

         if (second == -1)
            break;

         if (qAbs(pos - 1 - pointPos) < qAbs(pos - 1 - second))
         {
            input.remove(second, 1);
            if (pos > second)
               pos--;
         }
         else
         {
            input.remove(pointPos, 1);
            if (pos > pointPos)
               pos--;
            pointPos = second - 1;
         }
      }
   }

   int sectionLength = 0;
   int day = 0;
   int month = 0;

   for (int i = 0; i < input.length(); i++)
   {
      QChar c(input[i]);

      if (c == _separator)
      {
         sectionLength = 0;
         day = month;
         month = 0;
      }
      else if (c.isDigit())
      {
         if (sectionLength == 2)
         {
            input.remove(i, 1);
            if (pos > i)
               pos--;
            i--;
         }
         else
         {
            sectionLength++;
            month *= 10;
            month += c.digitValue();
         }
      }
      else
      {
         input.remove(i, 1);
         if (pos > i)
            pos--;
         i--;
      }
   }

   if (makeValid(day, month))
      return Intermediate;

   return Acceptable;
}

void QDateOfYearValidator::fixup(QString& input) const
{
   int   pointPos = input.indexOf(_separator);
   int   day = input.left(pointPos).toInt();
   int   month = input.mid(pointPos + 1).toInt();

   makeValid(day, month);

   input = QString::fromLatin1("%1.%2").arg(day, 2, 10, QLatin1Char('0')).arg(month, 2, 10, QLatin1Char('0'));
}

bool QDateOfYearValidator::makeValid(int& day, int& month) const
{
   bool changed = false;

   if (month < 1)
   {
      month = 1;
      changed = true;
   }
   if (month > 12)
   {
      month = 12;
      changed = true;
   }
   if (day < 1)
   {
      day = 1;
      changed = true;
   }
   switch (month)
   {
   case 1:
   case 3:
   case 5:
   case 7:
   case 8:
   case 10:
   case 12:
      if (day > 31)
      {
         day = 31;
         changed = true;
      }
      break;
   case 2:
      if (day > 28)
      {
         day = 28;
         changed = true;
      }
      break;
   case 4:
   case 6:
   case 9:
   case 11:
      if (day > 30)
      {
         day = 30;
         changed = true;
      }
      break;
   }

   return changed;
}
