#include "stdafx.h"
#include "QNumberEditDelegate.h"
#include "QNumberEdit.h"
#include <QDecimal.h>
#include <Variant.h>
#include "QRichText.h"

QNumberEditDelegate::QNumberEditDelegate(QObject *parent, int integerDigits, int fractionalDigits, QDecimal::GroupSeparatorMode groupSeparatorMode)
   : QEditItemDelegate(parent),
   _integerDigits(integerDigits),
   _fractionalDigits(fractionalDigits),
   _groupSeparatorMode(groupSeparatorMode),
   _emptyAllowed(false),
   _zeroAllowed(true),
   _negativeAllowed(false)
{
}

QNumberEditDelegate::~QNumberEditDelegate()
{
}

QRichText QNumberEditDelegate::formattedText(const QModelIndex &index, const QLocaleEx& locale) const
{
   const Variant value(index.data());

   if (value.type() == typeid(QDecimal))
      return formattedText(value.to<QDecimal>(), locale);

   return QEditItemDelegate::formattedText(index, locale);
}

QWidget *QNumberEditDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
   Q_UNUSED(option);
   Q_UNUSED(index);

   QNumberEdit *editor = new QNumberEdit(parent, _integerDigits, _fractionalDigits, _emptyAllowed);

   editor->setZeroAllowed(_zeroAllowed);
   editor->setNegativeAllowed(_negativeAllowed);
   editor->setGroupSeparatorMode(_groupSeparatorMode);

   return editor;
}

void QNumberEditDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
   QDecimal    number = Variant(index.data(Qt::EditRole)).to<QDecimal>();
   QNumberEdit *e = dynamic_cast<QNumberEdit*>(editor);

   if (_fractionalDigits < 0)
      e->setFractionalDigits(number.fractionalDigits());

   e->setValue(number);
}

void QNumberEditDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{
   model->setData(index, Variant(dynamic_cast<QNumberEdit*>(editor)->value()), Qt::EditRole);
}

void QNumberEditDelegate::updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
   QEditItemDelegate::updateEditorGeometry(editor, option, index);
}

QRichText QNumberEditDelegate::formattedText(const QDecimal& decimal, const QLocaleEx& locale) const
{
   return decimal.toString(_groupSeparatorMode, locale);
}
