#include "stdafx.h"
#include "QTableViewEx.h"
#include <QLabel>
#include <QResizeEvent>
#include "QHighlightItemDelegate.h"
#include <QHeaderView>
#include <HtItemModel.h>
#include <QToolTip>

#if 0
static QRect getNormalizedRect(int x1, int y1, int x2, int y2)
{
   if (x2 < x1)
      qSwap(x2, x1);
   if (y2 < y1)
      qSwap(y2, y1);

   QRect rect;

   rect.setCoords(x1, y1, x2, y2);

   return rect;
}
#endif

QTableViewEx::QTableViewEx(QWidget* parent) : QTableView(parent)
{
   verticalHeader()->setDefaultSectionSize(32);
   verticalHeader()->hide();

   QTableView::setItemDelegate(new QHighlightItemDelegate(this));
}

void QTableViewEx::showNoData(bool show)
{
   if (show)
   {
      if (_noDataWidget == nullptr)
      {
         QPalette pal;
         pal.setColor(QPalette::WindowText, Qt::gray);

         _noDataWidget = new QLabel(tr("No data"), this);

         _noDataWidget->setAlignment(Qt::AlignCenter);
         _noDataWidget->setPalette(pal);
         _noDataWidget->setFont(QFont(QLatin1String("Arial"), 32, QFont::Bold));
         _noDataWidget->setGeometry(0, horizontalHeader()->height(), width(), height() - horizontalHeader()->height());
         _noDataWidget->show();
      }
   }
   else if (_noDataWidget)
   {
      delete _noDataWidget;
      _noDataWidget = NULL;
   }
}

void QTableViewEx::setModel(QAbstractItemModel *model)
{
   _concealedTextIndexList.clear();
   QTableView::setModel(model);
}

void QTableViewEx::resizeColumnsToContentsEx()
{
   auto columnSizes = optimalSizes(true);

   //
   // Set the column sizes
   //

   for (auto col = 0; col < columnSizes.count(); col++)
   {
      setColumnWidth(col, columnSizes[col]);
   }
}

void QTableViewEx::resizeRowsToContentsEx()
{
   auto rowSizes = optimalSizes(false);

   //
   // Set the row sizes
   //

   for (auto row = 0; row < rowSizes.count(); row++)
   {
      setRowHeight(row, rowSizes[row]);
   }
}

bool QTableViewEx::isContentAutoAlign() const
{
   return dynamic_cast<QStyledItemDelegateEx *>(QTableView::itemDelegate())->isContentAutoAlign();
}

void QTableViewEx::setIsContentAutoAlign(bool autoAlign)
{
   dynamic_cast<QStyledItemDelegateEx *>(QTableView::itemDelegate())->setIsContentAutoAlign(autoAlign);
}

void QTableViewEx::setCheckOnMouseMove(bool checkOnMouseMove)
{
   if (_checkOnMouseMove == checkOnMouseMove)
      return;

   _checkOnMouseMove = checkOnMouseMove;

   dynamic_cast<QStyledItemDelegateEx *>(QTableView::itemDelegate())->setCheckOnMouseDown(_checkOnMouseMove);
}

void QTableViewEx::setEditDelegate(QEditItemDelegate* editDelegate)
{
   dynamic_cast<QStyledItemDelegateEx *>(QTableView::itemDelegate())->setEditDelegate(-1, -1, editDelegate);
}

QEditItemDelegate* QTableViewEx::editDelegate() const
{
   return dynamic_cast<QStyledItemDelegateEx *>(QTableView::itemDelegate())->editDelegate(-1, -1);
}

void QTableViewEx::setEditDelegateForColumn(int column, QEditItemDelegate* editDelegate)
{
   dynamic_cast<QStyledItemDelegateEx *>(QTableView::itemDelegate())->setEditDelegate(-1, column, editDelegate);
}

QEditItemDelegate* QTableViewEx::editDelegateForColumn(int column) const
{
   return dynamic_cast<QStyledItemDelegateEx *>(QTableView::itemDelegate())->editDelegate(-1, column);
}

void QTableViewEx::setEditDelegateForRow(int row, QEditItemDelegate* editDelegate)
{
   dynamic_cast<QStyledItemDelegateEx *>(QTableView::itemDelegate())->setEditDelegate(row, -1, editDelegate);
}

QEditItemDelegate* QTableViewEx::editDelegateForRow(int row) const
{
   return dynamic_cast<QStyledItemDelegateEx *>(QTableView::itemDelegate())->editDelegate(row, -1);
}

void QTableViewEx::setPaintDelegate(QPaintItemDelegate* paintDelegate)
{
   dynamic_cast<QStyledItemDelegateEx *>(QTableView::itemDelegate())->setPaintDelegate(-1, -1, paintDelegate);
}

QPaintItemDelegate* QTableViewEx::paintDelegate() const
{
   return dynamic_cast<QStyledItemDelegateEx *>(QTableView::itemDelegate())->paintDelegate(-1, -1);
}

void QTableViewEx::setPaintDelegateForColumn(int column, QPaintItemDelegate* paintDelegate)
{
   dynamic_cast<QStyledItemDelegateEx *>(QTableView::itemDelegate())->setPaintDelegate(-1, column, paintDelegate);
}

QPaintItemDelegate* QTableViewEx::paintDelegateForColumn(int column) const
{
   return dynamic_cast<QStyledItemDelegateEx *>(QTableView::itemDelegate())->paintDelegate(-1, column);
}

void QTableViewEx::setPaintDelegateForRow(int row, QPaintItemDelegate* paintDelegate)
{
   dynamic_cast<QStyledItemDelegateEx *>(QTableView::itemDelegate())->setPaintDelegate(row, -1, paintDelegate);
}

QPaintItemDelegate* QTableViewEx::paintDelegateForRow(int row) const
{
   return dynamic_cast<QStyledItemDelegateEx *>(QTableView::itemDelegate())->paintDelegate(row, -1);
}

bool QTableViewEx::viewportEvent(QEvent* e)
{
   if (e->type() == QEvent::ToolTip)
   {
      QHelpEvent *he = static_cast<QHelpEvent*>(e);
      const QModelIndex index = indexAt(he->pos());
      auto rect = visualRect(index);

      rect.setLeft(rect.right() - 14);
      if (rect.contains(he->pos()))
      {
         if (std::binary_search(_concealedTextIndexList.begin(), _concealedTextIndexList.end(), index))
         {
            auto concealedText = Variant(index.data()).toString();

            if (!concealedText.isEmpty())
            {
               QToolTip::showText(he->globalPos(), concealedText, this);
               return true;
            }
         }
      }
   }

   return QTableView::viewportEvent(e);
}

void QTableViewEx::paintEvent(QPaintEvent* e)
{
   static_cast<void>(std::remove_if(_concealedTextIndexList.begin(), _concealedTextIndexList.end(), [](const QPersistentModelIndex& index) { return !index.isValid(); }));
   QTableView::paintEvent(e);
}

void QTableViewEx::resizeEvent(QResizeEvent* e)
{
   QTableView::resizeEvent(e);
   if (_noDataWidget)
   {
      _noDataWidget->setGeometry(0, horizontalHeader()->height(), e->size().width(), e->size().height() - horizontalHeader()->height());
   }
}

void QTableViewEx::mousePressEvent(QMouseEvent* e)
{
   QVariant prevCheckState;

   if (_checkOnMouseMove)
   {
      if (e->button() == Qt::LeftButton && verticalHeader())
      {
         _mousePressedIndex = indexAt(e->pos());

         if (_mousePressedIndex.isValid())
         {
            prevCheckState = _mousePressedIndex.data(Qt::CheckStateRole);
         }
      }
   }
   else
   {
      _mousePressedIndex = currentIndex();
   }

   QTableView::mousePressEvent(e);

   if (_checkOnMouseMove)
   {
      if (!prevCheckState.isValid() || prevCheckState == _mousePressedIndex.data(Qt::CheckStateRole))
      {
         _mousePressedIndex = QModelIndex();
      }
   }
}

void QTableViewEx::mouseMoveEvent(QMouseEvent* e)
{
   QTableView::mouseMoveEvent(e);

   if (_checkOnMouseMove && _mousePressedIndex.isValid())
   {
      const auto index = indexAt(e->pos());

      if (index.isValid() && index.column() == _mousePressedIndex.column())
      {
         const auto checkState = _mousePressedIndex.data(Qt::CheckStateRole);

         if (checkState.isValid())
         {
            const auto origVisualIndex = verticalHeader()->visualIndex(_mousePressedIndex.row());
            const auto visualIndex = verticalHeader()->visualIndex(index.row());

            for (auto i = origVisualIndex + 1; i <= visualIndex; ++i)
            {
               model()->setData(model()->index(verticalHeader()->logicalIndex(i), index.column()), checkState, Qt::CheckStateRole);
            }
            for (auto i = visualIndex; i < origVisualIndex; ++i)
            {
               model()->setData(model()->index(verticalHeader()->logicalIndex(i), index.column()), checkState, Qt::CheckStateRole);
            }
         }
      }
   }
}

void QTableViewEx::mouseReleaseEvent(QMouseEvent* e)
{
   QVariant prevCheckState;

   if (!_checkOnMouseMove && _mousePressedIndex.isValid() && e->button() == Qt::LeftButton && e->modifiers() == Qt::ShiftModifier && verticalHeader())
   {
      const auto index = indexAt(e->pos());

      if (index.isValid())
      {
         prevCheckState = index.data(Qt::CheckStateRole);
      }
   }

   QTableView::mouseReleaseEvent(e);

   if (prevCheckState.isValid())
   {
      const auto index = indexAt(e->pos());

      if (index.isValid())
      {
         const auto checkState = index.data(Qt::CheckStateRole);

         if (checkState.isValid() && checkState != prevCheckState)
         {
            const auto origVisualIndex = verticalHeader()->visualIndex(_mousePressedIndex.row());
            const auto visualIndex = verticalHeader()->visualIndex(index.row());

            for (auto i = origVisualIndex; i < visualIndex; ++i)
            {
               model()->setData(model()->index(verticalHeader()->logicalIndex(i), index.column()), checkState, Qt::CheckStateRole);
            }
            for (auto i = visualIndex + 1; i <= origVisualIndex; ++i)
            {
               model()->setData(model()->index(verticalHeader()->logicalIndex(i), index.column()), checkState, Qt::CheckStateRole);
            }
         }
      }
   }

   _mousePressedIndex = QModelIndex();

   if (_doubleClicked)
   {
      _doubleClicked = false;
      emit doubleClickReleased(currentIndex());
   }
}

void QTableViewEx::mouseDoubleClickEvent(QMouseEvent* e)
{
   QTableView::mouseDoubleClickEvent(e);
   _doubleClicked = true;
}

QList<int> QTableViewEx::optimalSizes(bool forColumns)
{
   if (!model())
      return QList<int>();

   // Map for the column/row sizes so we can calculate the opitmal size

   QMap<int, QMap<int, int> > sectionSizeMap; // The first key is the column/row span and the second key is the column/row index, the value is the maximum column/row size hint

   auto sectionCount = forColumns ? model()->columnCount() : model()->rowCount();
   auto lineCount = forColumns ? model()->rowCount() : model()->columnCount();

   //
   // Initialize the size map with the header sizes
   //

   auto header = forColumns ? horizontalHeader() : verticalHeader();

   if (header && header->isVisible())
   {
      for (auto i = 0; i < header->count(); i++)
      {
         sectionSizeMap[1][i] = header->sectionSizeHint(i);
      }
   }

   //
   // Calculate the maximum section sizes from the cell sizes
   //

   QVector<int> lineSpans(sectionCount, 1);

   for (auto iLine = 0; iLine < lineCount; iLine++)
   {
      for (auto iSection = 0; iSection < sectionCount;)
      {
         lineSpans[iSection]--;
         if (lineSpans[iSection])
            continue;
         lineSpans[iSection] = forColumns ? rowSpan(iLine, iSection) : columnSpan(iSection, iLine);

         const auto sectspan = forColumns ? columnSpan(iLine, iSection) : rowSpan(iSection, iLine);

         auto delegate = dynamic_cast<const QStyledItemDelegateEx*>(QTableView::itemDelegate());
         QStyleOptionViewItem option;
#if QT_VERSION >= QT_VERSION_CHECK(6, 0, 0)
         initViewItemOption(&option);
#else
         option = viewOptions();
#endif
         auto sizeHint = 0;

         if (forColumns)
         {
            option.rect = QRect(1, 1, sectspan * 300, 300);

            sizeHint = delegate->sizeHint(option, model()->index(iLine, iSection)).width();
         }
         else
         {
            auto width = 0;
            for (auto iSpanLine = 0; iSpanLine < lineSpans[iSection]; iSpanLine++)
               width += columnWidth(iSpanLine + iLine);

            option.rect = QRect(1, 1, width, 300);

            sizeHint = delegate->sizeHint(option, model()->index(iSection, iLine)).height();
         }

         if (showGrid())
            sizeHint++;

         QMap<int, int>& sectionSizes = sectionSizeMap[sectspan];

         sectionSizes[iSection] = qMax(sectionSizes.value(iSection), sizeHint);

         iSection += sectspan;
      }
   }

   //
   // Calculate the optimal size for each column
   //

   auto sectionSizes = sectionSizeMap[1].values(); // List of all single column/row sizes ordred by column/row index
   auto keys = sectionSizeMap.keys(); // List of all different span values

   for (auto i = keys.count(); i--;) // Go through all spans starting with the greates and ending with 2 column spans
   {
      auto sectspan = keys[i];

      if (sectspan == 1)
         break; // No more merged columns/rows

      auto sectionSizeHints = sectionSizeMap[sectspan]; // Sizes of the sections spanning sectspan columns

      for (auto itSect = sectionSizeHints.begin(); itSect != sectionSizeHints.end(); ++itSect) // Go through the sections spanning sectspan columns
      {
         QList<QPair<int, int> > sectionRange; // Map of all single section indexes and their size inside the section span
         auto sizeHint = 0; // Size of all single sections inside the section span

         for (auto iSection = itSect.key(); iSection < itSect.key() + sectspan; iSection++) // Go through all single sections inside the section span
         {
            sectionRange.append(QPair<int, int>(iSection, sectionSizes[iSection]));
            sizeHint += sectionSizes[iSection];
         }

         if (sizeHint < itSect.value()) // Check if the cumulated size of all single sections in the span is less than the size of the section span
         {
            // Enlarge the single sections proportionally so that the sum matches the size of the spanned section

            auto extent = itSect.value() - sizeHint;

            // Sort the single sections by size ascending
            std::sort(sectionRange.begin(), sectionRange.end(), [](const QPair<int, int>& v1, const QPair<int, int>& v2) { return v1.second < v2.second; });

            // Go through the single sections inside the section span backwards (starting from the greatest to the spallest)
            for (auto iSection = sectionRange.count(); iSection--;)
            {
               auto s = sectionRange[iSection].second; // Size of the single section

               s += extent / (iSection + 1); // Add a part of the excessive size

               // Limit the size of a single column to 300
               if (s > 300)
                  s = 300;

               // Calculate the remaining extent
               extent -= s - sectionRange[iSection].second;

               // Set the new section size
               sectionRange[iSection].second = s;
            }

            // Change the section size in the list of single section sizes
            for (auto itr = sectionRange.begin(); itr != sectionRange.end(); ++itr)
            {
               sectionSizes[itr->first] = itr->second;
            }
         }
      }
   }

   return sectionSizes;
}

void QTableViewEx::setIndexIsConcealed(const QModelIndex& index, bool isConcealed) const
{
   auto lb = std::lower_bound(_concealedTextIndexList.begin(), _concealedTextIndexList.end(), index);

   if (lb != _concealedTextIndexList.end() && *lb == index)
   {
      if (!isConcealed)
         _concealedTextIndexList.erase(lb);
      return;
   }

   if (isConcealed)
      _concealedTextIndexList.insert(lb, index);
}
