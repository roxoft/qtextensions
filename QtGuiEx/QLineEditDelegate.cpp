#include "stdafx.h"
#include "QLineEditDelegate.h"
#include <QLineEdit>
#include <QStyleOption>

QLineEditDelegate::QLineEditDelegate(QObject *parent, int maxLength, QValidator *validator)
   : QEditItemDelegate(parent), _maxLength(maxLength), _validator(validator)
{
}

QLineEditDelegate::~QLineEditDelegate()
{
}

QWidget *QLineEditDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
   QStyleOptionViewItem opt = option;

   initStyleOption(&opt, index);

   const auto le = new QLineEdit(parent);
   auto pal = le->palette();

   pal.setColor(QPalette::Base, parent->palette().color(QPalette::Base));
   le->setPalette(pal);

   le->setMaxLength(_maxLength);

   if (_validator)
      le->setValidator(_validator);

   le->setAlignment(opt.displayAlignment);

   return le;
}

void QLineEditDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
   dynamic_cast<QLineEdit *>(editor)->setText(index.data(Qt::EditRole).toString());
}

void QLineEditDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{
   const auto le = dynamic_cast<QLineEdit *>(editor);
   QString value = le->text();

   if (!le->hasAcceptableInput() && _validator)
      _validator->fixup(value);

   model->setData(index, value, Qt::EditRole);
}

void QLineEditDelegate::updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
   QEditItemDelegate::updateEditorGeometry(editor, option, index);
}
