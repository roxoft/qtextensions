#include "stdafx.h"
#include "QHighlightItemDelegate.h"
#include "QIncrementalSearchWidget.h"
#include <QAbstractItemView>

QHighlightItemDelegate::QHighlightItemDelegate(QObject* parent)
   : QStyledItemDelegateEx(parent)
{
}

QRichText QHighlightItemDelegate::formattedText(const QModelIndex &index, const QLocaleEx& locale) const
{
   auto richText = QStyledItemDelegateEx::formattedText(index, locale);

   auto matches = matchPositions(index, richText);

   if (matches.isEmpty())
      return richText;

   return highlightedText(richText, matches);
}

QList<int> QHighlightItemDelegate::matchPositions(const QModelIndex &index, const QString& text) const
{
   if (_searchWidget && _searchWidget->itemView())
      return _searchWidget->match(index, text);

   return {};
}

QRichText QHighlightItemDelegate::highlightedText(const QString& text, const QList<int>& matchPositions) const
{
   QRichText richText = text;

   for (auto&& pos : matchPositions)
   {
      QTextLayout::FormatRange formatRange;

      formatRange.start = pos;
      formatRange.length = _searchWidget->patternLength();
      formatRange.format.setBackground(_searchWidget->itemView()->palette().color(QPalette::Highlight));
      formatRange.format.setForeground(_searchWidget->itemView()->palette().color(QPalette::HighlightedText));

      richText.addFormat(formatRange);
   }

   return richText;
}
