#include "stdafx.h"
#include "QSelectorPopupListView.h"
#include "QSortFilterProxyModelEx.h"
#include <QMouseEvent>
#include <QWheelEvent>
#include <QResizeEvent>
#include <QHeaderView>
#include "QStyledItemDelegateEx.h"

bool isPopupSeparator(const QModelIndex &index)
{
   return index.data(Qt::AccessibleDescriptionRole).toString() == QLatin1String("separator");
}

//
// QSelectorPopupDelegate
//

void QSelectorPopupDelegate::paint(QPainter *painter, const QStyleOptionViewItemEx& option, const QModelIndex &index) const
{
   if (isPopupSeparator(index))
   {
      if (index.column() == 0)
      {
         QRect rect = option.rect;

         if (const auto view = qobject_cast<const QAbstractItemView*>(option.widget))
         {
            const auto viewportRect = view->viewport()->geometry();

            rect.setWidth(viewportRect.width());
            rect.translate(-viewportRect.x(), 0);
         }

         QStyleOption opt;

         opt.rect = rect;
         _view->style()->drawPrimitive(QStyle::PE_IndicatorToolBarSeparator, &opt, painter, _view);
      }
   }
   else
   {
      QPaintItemDelegate::paint(painter, option, index);
   }
}

QSize QSelectorPopupDelegate::sizeHint(const QStyleOptionViewItemEx& option, const QModelIndex &index) const
{
   if (isPopupSeparator(index))
   {
      const auto pm = _view->style()->pixelMetric(QStyle::PM_DefaultFrameWidth, nullptr, _view);

      return {pm, pm + 4};
   }

   return QPaintItemDelegate::sizeHint(option, index);
}

//
// QSelectorPopupListView
//

QSelectorPopupListView::QSelectorPopupListView(bool sortEnabled, QWidget *parent) : QTreeViewEx(parent), _proxyModel(nullptr), _minimumLastColumnSize(30)
{
   setRootIsDecorated(false);
   setEditTriggers(NoEditTriggers);
   setSelectionMode(SingleSelection);
   setSelectionBehavior(SelectRows);
   setMouseTracking(true);
   setPaintDelegate(new QSelectorPopupDelegate(this));
   setMinimumSize(50, 20);

   if (sortEnabled)
   {
      QSortFilterProxyModel *proxyModel = new QSortFilterProxyModelEx(this);

      proxyModel->setDynamicSortFilter(true);
      _proxyModel = proxyModel;
   }

   header()->setDefaultSectionSize(30); // This must be set before setModel is called becaus otherwise the default section size of 100 can never be overridden again and there is always a scroll bar if the tree view width is less than 100!!!

   // Disable scroll bars to prevent erratic scroll bars on resizeColumnsToContents()
   setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
   setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

   header()->setStretchLastSection(false);

   connect(header(), SIGNAL(sectionResized(int, int, int)), this, SLOT(onColumnResized(int, int, int)));
   connect(header(), SIGNAL(sectionCountChanged(int, int)), this, SLOT(onSectionCountChanged(int, int)));
}

QSelectorPopupListView::~QSelectorPopupListView()
{
}

void QSelectorPopupListView::setModel(QAbstractItemModel *model)
{
   if (_proxyModel)
   {
      _proxyModel->setSourceModel(model);
      QTreeViewEx::setModel(_proxyModel);
   }
   else
      QTreeViewEx::setModel(model);
}

void QSelectorPopupListView::setMinimumLastColumnSize(int columnSize)
{
   _minimumLastColumnSize = columnSize;
   setLastColumnSize(viewport()->width());
}

void QSelectorPopupListView::select(const QModelIndex &index)
{
   QModelIndex proxyIndex;

   if (_proxyModel)
      proxyIndex = _proxyModel->mapFromSource(index);
   else
      proxyIndex = index;
   internalSelect(proxyIndex);
   scrollTo(proxyIndex);
}

void QSelectorPopupListView::moveSelection(int rowCount)
{
   auto selectedRows = selectionModel()->selectedRows();

   int row;

   if (selectedRows.isEmpty())
   {
      if (rowCount < 0)
         row = model()->rowCount();
      else
         row = -1;
   }
   else
   {
      auto index = rowCount > 0 ? selectedRows.last() : selectedRows.first();

      row = index.row();
   }

   row += rowCount;

   while (row >= 0 && row < model()->rowCount() && !model()->index(row, 0).flags().testFlag(Qt::ItemIsSelectable))
   {
      if (rowCount > 0)
         row++;
      else
         row--;
   }

   if (row < 0)
      row = 0;
   if (row >= model()->rowCount())
      row = model()->rowCount() - 1;

   const auto index = model()->index(row, 0);

   internalSelect(index);
   scrollTo(index);
}

int QSelectorPopupListView::selectedRow() const
{
   QModelIndex selectedIndex = selectionModel()->selectedRows().value(0);

   if (_proxyModel)
      return _proxyModel->mapToSource(selectedIndex).row();
   return selectedIndex.row();
}

QSize QSelectorPopupListView::sizeHint() const
{
   // Calculate the minimum dimensions to show the complete table
   QMargins contentMargins = contentsMargins();

   int minWidth = 0;
   int column = model()->columnCount();

   if (column--)
   {
      minWidth = minimumLastColumnSize();
      while (column--)
      {
         minWidth += columnWidth(column);
      }
   }

   minWidth += contentMargins.left();
   minWidth += contentMargins.right();

   auto height = 0;

   for (int i = 0; i < model()->rowCount(); ++i)
   {
      height += rowHeight(model()->index(i, 0));
   }

   if (model()->columnCount() > 1)
      height += header()->sizeHint().height(); // The header will be visible so add the height of the header

   height += contentMargins.top();
   height += contentMargins.bottom();

   return QSize(minWidth, height);
}

void QSelectorPopupListView::resizeEvent(QResizeEvent *e)
{
   QTreeViewEx::resizeEvent(e);
   setLastColumnSize(e->size().width());
}

void QSelectorPopupListView::wheelEvent(QWheelEvent *e)
{
   QTreeViewEx::wheelEvent(e);

#if QT_VERSION < QT_VERSION_CHECK(5,14,0)
   QModelIndex index = indexAt(e->pos());
#else
   QModelIndex index = indexAt(e->position().toPoint());
#endif

   if (index.isValid() && index.row() != selectionModel()->selectedRows().value(0).row())
      internalSelect(index);
}

void QSelectorPopupListView::mouseMoveEvent(QMouseEvent *e)
{
   QTreeViewEx::mouseMoveEvent(e);

   QModelIndex index = indexAt(e->pos());

   if (index.isValid() && index.row() != selectionModel()->selectedRows().value(0).row())
      internalSelect(index);
}

void QSelectorPopupListView::mouseReleaseEvent(QMouseEvent *e)
{
   QTreeViewEx::mouseReleaseEvent(e);

   QModelIndex index = indexAt(e->pos());

   if (index.isValid())
   {
      if (index.row() != selectionModel()->selectedRows().value(0).row())
         internalSelect(indexAt(e->pos()));

      emit selected(selectedRow());
   }
}

void QSelectorPopupListView::onColumnResized(int , int , int )
{
   setLastColumnSize(viewport()->width());
}

void QSelectorPopupListView::onSectionCountChanged(int , int )
{
   setLastColumnSize(viewport()->width());
}

QAbstractItemModel *QSelectorPopupListView::sourceModel()
{
   if (_proxyModel)
      return _proxyModel->sourceModel();
   return model();
}

void QSelectorPopupListView::setLastColumnSize(int totalWidth)
{
   QHeaderView *headerView = header();
   int         fixedWidth = 0;
   int         column = 0;

   for (; column + 1 < headerView->count(); ++column)
   {
      fixedWidth += headerView->sectionSize(column);
   }

   if (column < headerView->count())
   {
      int lastColumnWidth = totalWidth - fixedWidth;

      if (lastColumnWidth < _minimumLastColumnSize)
         lastColumnWidth = _minimumLastColumnSize;

      headerView->resizeSection(column, lastColumnWidth);
   }
}

void QSelectorPopupListView::internalSelect(const QModelIndex &index)
{
   selectionModel()->select(index, QItemSelectionModel::ClearAndSelect | QItemSelectionModel::Rows);
}
