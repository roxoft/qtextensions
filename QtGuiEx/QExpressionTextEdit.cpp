#include "stdafx.h"
#include "QExpressionTextEdit.h"
#include "QCompleterEx.h"
#include <StringTemplate.h>
#include <CPPTokenizer.h>

QExpressionTextEdit::QExpressionTextEdit(QWidget *parent)
   : QTextEditEx(parent)
{
   // Change the default font
   document()->setDefaultFont(QFont("Courier New", 8));

   // Change the tabstop distance to a lower value
#if QT_VERSION >= QT_VERSION_CHECK(5,10,0)
   setTabStopDistance(20);
#endif
}

QExpressionTextEdit::~QExpressionTextEdit()
{
}

QPair<int, int> QExpressionTextEdit::expressionRangeAtBlockPosition(const QTextBlock& block, int blockPos) const
{
   const auto text = block.text();

   if (text.trimmed().isEmpty())
      return { 0, 0 };

   if (!isDelimited())
      return{ 0, text.length() };

   StringTemplate stringTemplate;

   stringTemplate.parseTemplate(text, false, _startDelimiter, _endDelimiter);

   return stringTemplate.expressionRangeAt(blockPos);
}

QList<Token> QExpressionTextEdit::variableAtBlockPosition(const QTextBlock& block, int blockPos) const
{
   const auto tokenList = blockTokenList(block);

   auto iCurrentToken = 0;

   for (; iCurrentToken < tokenList.count(); ++iCurrentToken)
   {
      if (blockPos <= tokenList.at(iCurrentToken).endPos())
         break;
   }

   Q_ASSERT(tokenList.isEmpty() || iCurrentToken < tokenList.count());

   QList<Token> result;

   if (iCurrentToken == tokenList.count() ||
      tokenList.at(iCurrentToken).is<StringTemplateTextToken>() ||
      tokenList.at(iCurrentToken).is<Token::Separator>() ||
      tokenList.at(iCurrentToken).is<Token::WhiteSpace>() ||
      tokenList.at(iCurrentToken).is<Token::Comment>() ||
      (tokenList.at(iCurrentToken).is<CppOperatorToken>() && strcmp(tokenList.at(iCurrentToken).as<CppOperatorToken>()->op(), ".") != 0))
   {
      return result;
   }

   result.append(tokenList.at(iCurrentToken));

   // Expand the identifier to the left (include white space and comments)
   auto indexLevel = 0;

   for (auto i = iCurrentToken; i--; )
   {
      auto& token = tokenList.at(i);

      if (token.is<Token::Separator>() || token.is<StringTemplateTextToken>())
         break;

      const auto op = token.as<CppOperatorToken>();

      if (op)
      {
         if (strcmp(op->op(), "[") == 0)
            indexLevel--;
         else if (strcmp(op->op(), "]") == 0)
            indexLevel++;
         else if (strcmp(op->op(), ".") != 0)
            break;

         if (indexLevel < 0)
            break;
      }

      if (!token.is<Token::WhiteSpace>() && !token.is<Token::Comment>())
      {
         result.prepend(token);
      }
   }

   if (result.first().is<Token::Integer>())
      result.clear();

   return result;
}

void QExpressionTextEdit::keyPressEvent(QKeyEvent* event)
{
   if (!_completer)
   {
      QTextEditEx::keyPressEvent(event);
      return;
   }

   _lastKeyPressed = event->key();

   if (_lastKeyPressed == Qt::Key_Return || _lastKeyPressed == Qt::Key_Enter)
   {
      if (_completer->acceptSelectedItem() && !_completer->isFreeText())
      {
         event->accept();
         return;
      }
   }

   // Special key treatment
   auto keyProcessed = false;

   if (!textCursor().hasSelection() && event->text().length() == 1)
   {
      const auto key = event->text().at(0);
      auto tc = textCursor();

      if (handleSpecialKeys(tc, key, _startDelimiter, _endDelimiter))
      {
         event->accept();
         return;
      }
      if (handleSpecialKeys(tc, key, u'"', u'"'))
      {
         event->accept();
         return;
      }
      if (handleSpecialKeys(tc, key, u'(', u')'))
      {
         event->accept();
         return;
      }
      if (handleSpecialKeys(tc, key, u'[', u']'))
      {
         event->accept();
         return;
      }
      if (handleSpecialKeys(tc, key, QChar(), u'.'))
      {
         event->accept();
         keyProcessed = true;
      }
   }

   // Propagate key event

   const auto isShortcut = ((event->modifiers() & Qt::ControlModifier) && _lastKeyPressed == Qt::Key_E); // CTRL-E

   if (!isShortcut && !keyProcessed) // Do not process the shortcut if we have a completer
      QTextEdit::keyPressEvent(event);

   // Is there something to complete?

   const auto tc = textCursor();
   const auto block = tc.block();

   auto tokenList = variableAtBlockPosition(block, tc.position() - block.position());

   QStringList path;
   int elementIndex = -1;

   path.append(QString());

   for (auto&& token : tokenList)
   {
      if (const auto identifierToken = token.as<Token::Identifier>())
      {
         path.last() += identifierToken->identifier();
      }
      else if (const auto opToken = token.as<CppOperatorToken>())
      {
         if (strcmp(opToken->op(), ".") == 0 || strcmp(opToken->op(), "[") == 0)
         {
            path.append(QString());
         }
         else if (strcmp(opToken->op(), "]") == 0 && elementIndex >= 0)
         {
            path.last() += QString::number(elementIndex);
            elementIndex = -1;
         }
         else
         {
            break;
         }
      }
      else if (const auto intToken = token.as<Token::Integer>())
      {
         elementIndex = intToken->integer();
      }
      else
      {
         break;
      }
   }

   if (path.count() == 1 && path.at(0).isEmpty())
   {
      if (!isShortcut)
         _completer->hideSelectionList();
      return;
   }

   if ((event->modifiers() & Qt::ControlModifier) || event->text().isEmpty())
      return;

   // Show completer

   if (tokenList.last().is<CppOperatorToken>())
      _wordStartPos = tokenList.last().endPos() + block.position();
   else
      _wordStartPos = tokenList.last().startPos() + block.position();
   _wordEndPos = tokenList.last().endPos() + block.position();

   if (!_completer->updateSelectList(path.join(u'.')))
      this->setCursor(Qt::BusyCursor);
}

void QExpressionTextEdit::on_completerCurrentIndexChanged(const QModelIndex& index)
{
}

void QExpressionTextEdit::on_completerCurrentTextChanged(bool useCurrentIndex)
{
   auto tc = textCursor();

   if (!_completer->currentIndex().isValid())
   {
      _wordStartPos = 0;
      _wordEndPos = 0;
      tc.clearSelection();
      return;
   }

   const auto currentText = _completer->dataSource()->text(_completer->currentIndex());

   tc.setPosition(_wordStartPos);
   tc.setPosition(_wordEndPos, QTextCursor::KeepAnchor);

   if (useCurrentIndex || !_completer->isFreeText())
   {
      tc.insertText(currentText);
      if (currentText.endsWith("()"))
      {
         tc.movePosition(QTextCursor::PreviousCharacter);
         setTextCursor(tc);
      }
   }
}

void QExpressionTextEdit::on_completerMatchIndexChanged(const QModelIndex& index)
{
   const auto selectedText = _completer->dataSource()->text(index);

   auto tc = textCursor();

   tc.setPosition(_wordStartPos);
   tc.setPosition(_wordEndPos, QTextCursor::KeepAnchor);

   const auto completionPrefix = tc.selectedText();

   //if (completionPrefix.isEmpty() || completionPrefix == selectedText)
   //   resetTextColor();
   //else if (_completer->isFreeText())
   //   setTextColor(warningColor);
   //else
   //   setTextColor(errorColor);

   if (hasFocus() && _completer->itemCount() && (!_completer->isFreeText() || selectedText.startsWith(completionPrefix, Qt::CaseInsensitive)))
   {
#if 0
      // Suggest completion

      const auto cursorPos = this->cursorPosition();

      if (_lastKeyPressed != Qt::Key_Backspace && _lastKeyPressed != Qt::Key_Delete
         && !this->text().isEmpty()
         && cursorPos == this->text().length()
         && selectedText.startsWith(this->text()))
      {
         resetTextColor();
         this->setText(selectedText);
         this->setSelection(this->text().length(), cursorPos - this->text().length());
      }
#endif

      // Show pop-up

      QRect cr = cursorRect();

      cr.setWidth(50);
      _completer->showSelectionList(index, this->mapToGlobal(cr.bottomLeft()), cr.width());
   }
   else
   {
      _completer->hideSelectionList();
   }

   this->setCursor(Qt::IBeamCursor);
}

bool QExpressionTextEdit::handleSpecialKeys(QTextCursor& tc, const QChar& key, const QChar& startDelimiter, const QChar& endDelimiter)
{
   if (key != startDelimiter && key != endDelimiter)
      return false;

   if (!tc.movePosition(QTextCursor::NextCharacter, QTextCursor::KeepAnchor))
      return false;

   if (tc.selectedText() == key)
   {
      tc.clearSelection(); // Moves actually one character forward
   }
   else
   {
      tc.movePosition(QTextCursor::PreviousCharacter); // Move back
      tc.insertText(key);
      if (key == startDelimiter)
      {
         tc.insertText(endDelimiter);
         tc.movePosition(QTextCursor::PreviousCharacter);
      }
   }
   setTextCursor(tc);
   return true;
}

QList<Token> QExpressionTextEdit::blockTokenList(const QTextBlock& block) const
{
   QList<Token> tokenList;

   const auto text = block.text();

   if (text.trimmed().isEmpty())
      return tokenList;

   if (isDelimited())
   {
      StringTemplate stringTemplate;

      stringTemplate.parseTemplate(text, false, _startDelimiter, _endDelimiter);

      tokenList = stringTemplate.tokenList();
   }
   else
   {
      Tokenizer tokenizer(text, createVersatileExpressionContext());

      for (auto token = tokenizer.next(); token.isValid(); token = tokenizer.next())
      {
         tokenList.append(token);
      }
   }

   return tokenList;
}
