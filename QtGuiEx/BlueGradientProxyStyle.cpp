#include "stdafx.h"
#include "BlueGradientProxyStyle.h"
#include <QtWidgets>
#include <QPainter>
#include "QStyledItemDelegateEx.h"

static void drawBlueGradientBackground(QPainter* painter, const QRect& rect, const QColor& baseColor, const QColor& backgroundColor, bool highlight, bool hover)
{
   // Highlighting creates a blue frame around the background color

   auto outerColor = baseColor;
   auto innerColor = backgroundColor;
   auto startValue = 0.1;
   auto endValue = 0.9;

   if (highlight)
   {
      outerColor = backgroundColor;
      innerColor = baseColor;
      startValue = 0.0;
      endValue = 1.0;

      if (outerColor == innerColor)
      {
         if (outerColor.blue() < 128)
         {
            outerColor = QColor(outerColor.red(), outerColor.green() + 15, outerColor.blue() + 27).lighter();
         }
         else if (outerColor.red() > 128)
         {
            outerColor = QColor(outerColor.red() - 55, outerColor.green() - 39, outerColor.blue());
         }
      }
      else
      {
         if (outerColor.value() < 128)
            outerColor = outerColor.lighter();
         else
            outerColor = outerColor.darker(110);
      }

      if (hover)
      {
         innerColor = innerColor.blue() < 128 ? innerColor.lighter() : innerColor.lighter(105);
      }
   }
   else
   {
      if (innerColor == outerColor)
      {
         if (innerColor.blue() < 128)
         {
            innerColor = QColor(innerColor.red(), innerColor.green() + 15, innerColor.blue() + 27);
         }
         else if (innerColor.red() > 128)
         {
            innerColor = QColor(innerColor.red() - 19, innerColor.green() - 9, innerColor.blue());
         }
      }

      if (hover)
      {
         innerColor = innerColor.blue() < 128 ? innerColor.lighter(130) : innerColor.darker(105);
      }
   }

   if (rect.height() > 20)
   {
      auto upperRect = rect;

      upperRect.setHeight(10);

      QLinearGradient upperGradient(upperRect.topLeft(), upperRect.bottomLeft());

      upperGradient.setColorAt(startValue, outerColor);
      upperGradient.setColorAt(1, innerColor);

      painter->fillRect(upperRect, upperGradient);

      auto middleRect = rect.adjusted(0, 10, 0, -10);

      painter->fillRect(middleRect, innerColor);

      auto lowerRect = rect;

      lowerRect.setTop(lowerRect.bottom() - 10);

      QLinearGradient lowerGradient(lowerRect.topLeft(), lowerRect.bottomLeft());

      lowerGradient.setColorAt(0.0, innerColor);
      lowerGradient.setColorAt(endValue, outerColor);

      painter->fillRect(lowerRect, lowerGradient);

      return;
   }

   QLinearGradient gradient(rect.topLeft(), rect.bottomLeft());

   gradient.setColorAt(startValue, outerColor);
   gradient.setColorAt(0.5, innerColor);
   gradient.setColorAt(endValue, outerColor);

   painter->fillRect(rect, gradient);
}

BlueGradientProxyStyle::BlueGradientProxyStyle(QStyle* style) : QExtendedProxyStyle(style)
{
}

BlueGradientProxyStyle::BlueGradientProxyStyle(EntireCellCheckBoxPosition checkBoxPosition, QStyle* style) : QExtendedProxyStyle(checkBoxPosition, style)
{
}

void BlueGradientProxyStyle::polish(QPalette& palette)
{
   QExtendedProxyStyle::polish(palette);

   auto alternateBaseColor = palette.color(QPalette::Active, QPalette::AlternateBase);

   if (alternateBaseColor.blue() < 128)
   {
      alternateBaseColor = QColor(0, 40, 27);
   }
   else if (alternateBaseColor.red() > 128)
   {
      alternateBaseColor = QColor(240, 255, 250);
   }

   palette.setBrush(QPalette::AlternateBase, alternateBaseColor);
}

void BlueGradientProxyStyle::drawPrimitive(PrimitiveElement element, const QStyleOption* option, QPainter* painter, const QWidget* widget) const
{
   //QWindowsXPStyle::drawPrimitive(element, option, painter, widget);
   // The following code is copied from case PE_PanelItemViewItem in function
   // void QCommonStyle::drawPrimitive(PrimitiveElement pe, const QStyleOption *opt, QPainter *p, const QWidget *widget) const
   // in file "Src\qtbase\src\widgets\styles\qcommonstyle.cpp".

   if (element == QStyle::PE_PanelItemViewRow)
   {
      if (const QStyleOptionViewItem *vopt = qstyleoption_cast<const QStyleOptionViewItem *>(option))
      {
         QPalette::ColorGroup cg = (widget ? widget->isEnabled() : (vopt->state & QStyle::State_Enabled)) ? QPalette::Normal : QPalette::Disabled;

         if (cg == QPalette::Normal && !(vopt->state & QStyle::State_Active))
            cg = QPalette::Inactive;

         //if ((voptex->state & QStyle::State_Selected) && proxy()->styleHint(QStyle::SH_ItemView_ShowDecorationSelected, option, widget))
         //   painter->fillRect(voptex->rect, voptex->palette.brush(cg, QPalette::Highlight));
         //else if (voptex->features & QStyleOptionViewItem::Alternate)
         //   painter->fillRect(voptex->rect, voptex->palette.brush(cg, QPalette::AlternateBase));

         QColor backgroundColor;

         if (vopt->backgroundBrush.style() != Qt::NoBrush)
         {
            backgroundColor = vopt->backgroundBrush.color();
         }
         else if (vopt->features & QStyleOptionViewItem::Alternate)
         {
            backgroundColor = vopt->palette.color(cg, QPalette::AlternateBase);
         }
         else
         {
            backgroundColor = vopt->palette.color(cg, QPalette::Base);
         }

         drawBlueGradientBackground(painter, vopt->rect, vopt->palette.color(cg, QPalette::Base), backgroundColor, vopt->state & QStyle::State_Selected, false);

         return;
      }
   }
   else if (element == QStyle::PE_PanelItemViewItem)
   {
      // See qtbase\src\plugins\styles\windowsvista\qwindowsvistastyle.cpp line 671
      // QWindowsVistaStyle::drawPrimitive case PE_PanelItemViewItem

      if (const QStyleOptionViewItemEx *voptex = qstyleoption_cast<const QStyleOptionViewItemEx *>(option))
      {
         QPalette::ColorGroup cg = voptex->state & QStyle::State_Enabled ? QPalette::Normal : QPalette::Disabled;
         if (cg == QPalette::Normal && !(voptex->state & QStyle::State_Active))
            cg = QPalette::Inactive;

         QColor backgroundColor;

         if (voptex->backgroundBrush.style() != Qt::NoBrush)
         {
            backgroundColor = voptex->backgroundBrush.color();
         }
         else if (voptex->features & QStyleOptionViewItem::Alternate)
         {
            backgroundColor = voptex->palette.color(cg, QPalette::AlternateBase);
         }
         else
         {
            backgroundColor = voptex->palette.color(cg, QPalette::Base);
         }

         const auto newStyle = !qobject_cast<const QTableView*>(widget);

         drawBlueGradientBackground(painter, voptex->rect, voptex->palette.color(cg, QPalette::Base), backgroundColor, voptex->state & QStyle::State_Selected, newStyle && voptex->state & QStyle::State_MouseOver);

         return;
      }
   }
   else if (element == QStyle::PE_FrameFocusRect)
   {
       if (option->state & QStyle::State_Enabled)
       {
          painter->save();

          QRect focusRect = option->rect;

          focusRect.adjust(1, 1, -1, -1); // This is necessary because otherwise the frame is drawn into the other rows

          painter->setBrush(Qt::NoBrush);

          QColor focusColor(90, 170, 230); // (192, 225, 255)

          painter->setRenderHint(QPainter::Antialiasing);
          painter->setPen(QPen(focusColor, 1));
          painter->drawRoundedRect(focusRect, 4, 4);

          painter->restore();

          return;
       }
   }

   QExtendedProxyStyle::drawPrimitive(element, option, painter, widget);
}

void BlueGradientProxyStyle::drawControl(ControlElement control, const QStyleOption* option, QPainter* painter, const QWidget* widget) const
{
   if (control == QStyle::CE_ItemViewItem)
   {
      QStyleOptionViewItemEx opt;

      if (const QStyleOptionViewItemEx *voptex = qstyleoption_cast<const QStyleOptionViewItemEx *>(option))
      {
         opt = *voptex;
      }
      else if (const QStyleOptionViewItem *vopt = qstyleoption_cast<const QStyleOptionViewItem *>(option))
      {
         opt = *vopt;
         opt.richText = opt.text;
      }

      // In this style highlighted text is expected to have the same color as normal text
      opt.palette.setColor(QPalette::Disabled, QPalette::HighlightedText, opt.palette.color(QPalette::Disabled, QPalette::Text));
      opt.palette.setColor(QPalette::Active, QPalette::HighlightedText, opt.palette.color(QPalette::Active, QPalette::Text));
      opt.palette.setColor(QPalette::Inactive, QPalette::HighlightedText, opt.palette.color(QPalette::Inactive, QPalette::Text));

      opt.continuationHintStyle = QStyleOptionViewItemEx::ContinuationStyle::Icon;

      drawItemViewItem(&opt, painter, widget);

      return;
   }

   QExtendedProxyStyle::drawControl(control, option, painter, widget);
}
