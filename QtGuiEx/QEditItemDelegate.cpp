#include "stdafx.h"
#include "QEditItemDelegate.h"
#include "QStyledItemDelegateEx.h"

QEditItemDelegate::QEditItemDelegate(QObject *parent)
   : QObject(parent)
{
}

QEditItemDelegate::~QEditItemDelegate()
{
}

QRichText QEditItemDelegate::formattedText(const QModelIndex& index, const QLocaleEx& locale) const
{
   return QStyledItemDelegateEx::_formattedText(index, locale);
}

QWidget* QEditItemDelegate::createEditor(QWidget* parent, const QStyleOptionViewItem& option, const QModelIndex& index) const
{
   return _itemDelegate->QStyledItemDelegate::createEditor(parent, option, index);
}

void QEditItemDelegate::destroyEditor(QWidget* editor, const QModelIndex& index) const
{
   _itemDelegate->QStyledItemDelegate::destroyEditor(editor, index);
}

void QEditItemDelegate::setEditorData(QWidget* editor, const QModelIndex& index) const
{
   _itemDelegate->QStyledItemDelegate::setEditorData(editor, index);
}

void QEditItemDelegate::setModelData(QWidget* editor, QAbstractItemModel* model, const QModelIndex& index) const
{
   _itemDelegate->QStyledItemDelegate::setModelData(editor, model, index);
}

void QEditItemDelegate::updateEditorGeometry(QWidget* editor, const QStyleOptionViewItem& option, const QModelIndex& index) const
{
   //_itemDelegate->QStyledItemDelegate::updateEditorGeometry(editor, option, index); // This is too large!
   editor->setGeometry(option.rect);
}

void QEditItemDelegate::initStyleOption(QStyleOptionViewItem* option, const QModelIndex& index) const
{
   _itemDelegate->initStyleOption(option, index);
}
