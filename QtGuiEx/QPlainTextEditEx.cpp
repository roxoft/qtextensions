#include "stdafx.h"
#include "QPlainTextEditEx.h"
#include <QKeyEvent>

QPlainTextEditEx::QPlainTextEditEx(QWidget* parent) : QPlainTextEdit(parent)
{
}

void QPlainTextEditEx::setMaxLength(int maxLength)
{
   if (_maxLength != -1 && maxLength == -1)
      disconnect(this, SIGNAL(textChanged()), this, SLOT(onTextChanged()));
   else if (_maxLength == -1 && maxLength != -1)
      connect(this, SIGNAL(textChanged()), this, SLOT(onTextChanged()));

   _maxLength = maxLength;
}

void QPlainTextEditEx::keyPressEvent(QKeyEvent* e)
{
   if (!_returnChangesFocus || (e->modifiers() & Qt::ShiftModifier) || (e->modifiers() & Qt::ControlModifier) || e->key() != Qt::Key_Return && e->key() != Qt::Key_Enter)
      QPlainTextEdit::keyPressEvent(e);
   else
      focusNextChild();
}

void QPlainTextEditEx::onTextChanged()
{
   auto plainText = toPlainText();

   if (_maxLength >= 0 && plainText.length() > _maxLength)
   {
      setPlainText(plainText.left(_maxLength));
      moveCursor(QTextCursor::End);
   }
}
