#include "stdafx.h"
#include "QDateEditExDelegate.h"
#include "QComboBoxEx.h"
#include "QDateEditEx.h"

QDateEditExDelegate::QDateEditExDelegate(QObject *parent, bool calendarPopup, bool emptyAllowed)
   : QEditItemDelegate(parent), _calendarPopup(calendarPopup), _emptyAllowed(emptyAllowed)
{
}

QDateEditExDelegate::~QDateEditExDelegate()
{
}

QWidget *QDateEditExDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
   Q_UNUSED(option);
   Q_UNUSED(index);

   QWidget     *editWidget = nullptr;
   QDateEditEx *editor = nullptr;

   if (_calendarPopup)
   {
      editWidget = new QComboBoxEx(parent);
      editor = new QDateEditEx(editWidget);
   }
   else
   {
      editor = new QDateEditEx(parent);
      editWidget = editor;
   }

   if (_minimumDate.isValid())
      editor->setMinimumDate(_minimumDate);
   if (_maximumDate.isValid())
      editor->setMaximumDate(_maximumDate);
   editor->setEmptyAllowed(_emptyAllowed);

   return editWidget;
}

void QDateEditExDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
   if (_calendarPopup)
      dynamic_cast<QDateEditEx *>(dynamic_cast<QComboBoxEx *>(editor)->content())->setDate(index.data(Qt::EditRole).toDate());
   else
      dynamic_cast<QDateEditEx *>(editor)->setDate(index.data(Qt::EditRole).toDate());
}

void QDateEditExDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{
   if (_calendarPopup)
      model->setData(index, dynamic_cast<QDateEditEx *>(dynamic_cast<QComboBoxEx *>(editor)->content())->date(), Qt::EditRole);
   else
      model->setData(index, dynamic_cast<QDateEditEx *>(editor)->date(), Qt::EditRole);
}

void QDateEditExDelegate::updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
   QEditItemDelegate::updateEditorGeometry(editor, option, index);
}
