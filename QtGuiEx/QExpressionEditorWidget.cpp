#include "stdafx.h"
#include "QExpressionEditorWidget.h"
#include "ui_QExpressionEditorWidget.h"
#include "QExpressionTextEdit.h"
#include "QTreeViewEx.h"
#include "QCompleterVarTreeDataSource.h"
#include "QTextDocumentTemplate.h" // For insertVariable()
#include <HtStandardModel.h>
#include <CPPTokenizer.h>
#include <CPPExpression.h>
#include <QKeyEvent>
#include <QTextDocument>
#include <QTextBlock>
#include <QMenu>

static const int itemLimit = 5;
static const int stringLimit = 40;

static QString elideText(const QString& text, int maxLength)
{
   if (maxLength > 3 && text.length() > maxLength)
      return text.left(maxLength - 3) + "...";
   return text;
}

static QString pathFromNode(const HtNode* node)
{
   QStringList result;

   while (node->parent())
   {
      result.prepend(node->attribute(0).toString());
      node = node->parent();
   }

   return result.join(u'.');
}

static void addVariablesToModel(HtNode* parent, VarPtr varTree)
{
   const auto itemNames = varTree->itemNames();

   for (auto i = 0; i < itemNames.count(); ++i)
   {
      auto node = parent->appendChild();
      auto name = itemNames.at(i);
      auto variable = varTree->getItem(i);

      if (variable->basicType() == IVariable::BasicType::Function)
         name += "()";

      node->setAttribute(0, QText(name));

      if (variable->basicType() == IVariable::BasicType::Value)
      {
         node->setAttribute(1, toQString(variable->value()).simplified());
      }
      else if (variable->basicType() == IVariable::BasicType::Function)
      {
         const auto abstractFunction = variable->function().as<AbstractFunctionExpression>();

         if (abstractFunction)
            node->setAttribute(1, abstractFunction->returnType());
      }
      else if (variable->isList())
      {
         QStringList listItems;

         for (auto j = 0; j < variable->itemCount() && j < itemLimit; ++j) // Limit the number of displayed items
         {
            listItems.append(elideText(variable->getItem(j)->value().toString(), stringLimit)); // Limit the string lengths
         }

         node->setAttribute(1, listItems.join(" | "));
      }

      addVariablesToModel(node, varTree->getItem(i));
   }

   parent->sort(0);
}

static bool filterTree(QTreeView* tree, const HtItemModel* model, const HtNode* node, const QString& filter, bool isVisible = false)
{
   if (!isVisible)
      isVisible = node->attribute(0).toString().contains(filter, Qt::CaseInsensitive);

   auto childIsVisible = isVisible;

   for (auto&& child : node->children())
   {
      if (filterTree(tree, model, child, filter, isVisible))
         childIsVisible = true;
   }

   tree->setRowHidden(node->parent()->indexOf(node), model->indexFromNode(node->parent()), !childIsVisible);

   return childIsVisible;
}

static void fillVarTreeModel(HtNode* rootNode, const VarTree& varTree)
{
   rootNode->setAttribute(0, QExpressionEditorWidget::tr("Name"));
   rootNode->setAttribute(1, QExpressionEditorWidget::tr("Value"));

   addVariablesToModel(rootNode, varTree.root());
}

static VarPtr cloneReduced(const VarPtr& variable)
{
   VarPtr reducedClone;

   switch (variable->basicType())
   {
   case IVariable::BasicType::Collection:
      reducedClone = new VarTree::Variable(variable->typeName());
      {
         const auto itemNames = variable->itemNames();
         const auto items = variable->items();
         const auto itemCount = qMax(itemNames.count(), qMin(items.count(), itemLimit));

         for (auto i = 0; i < itemCount; ++i)
            reducedClone->setItem(itemNames.value(i), cloneReduced(items.at(i)));
      }
      break;
   case IVariable::BasicType::Value:
      reducedClone = new VarTree::Variable(Variant::null, variable->typeName());
      {
         const auto value = variable->value();

         if (value.type() == typeid(QString))
            reducedClone->setValue(elideText(value.toString(), stringLimit));
         else
            reducedClone->setValue(variable->value());
      }
      break;
   case IVariable::BasicType::Function:
      reducedClone = new VarTree::Variable(variable->function());
      break;
   }

   Q_ASSERT(reducedClone);

   return reducedClone;
}

QExpressionEditorWidget::QExpressionEditorWidget(const VarTree& varTree, const ResolverFunctionMap& functionMap, const Tokenizer::Context* context, QWidget *parent)
   : QWidget(parent), ui(new Ui::QExpressionEditorWidget), _varTree(varTree), _functionMap(functionMap), _tokenizer(context)
{
   ui->setupUi(this);

   ui->labelResult->setText(QString());
   ui->labelResultIcon->setText(QString());

   _varTreeModel = new HtStandardModel(this);

   fillVarTreeModel(_varTreeModel->rootNode(), varTree);

   ui->treeViewVariables->setModel(_varTreeModel);
   ui->treeViewVariables->setColumnWidth(0, 200);

   createFunctionModel(_functionMap);

   ui->treeViewFunctions->setModel(_functionTreeModel);
   ui->treeViewFunctions->header()->resizeSections(QHeaderView::ResizeToContents);
   ui->textEditExpression->setCompleterDataSource(new QCompleterVarTreeDataSource(_varTree, _functionMap));

   connect(ui->textEditExpression->document(), &QTextDocument::contentsChange, this, &QExpressionEditorWidget::onDocumentContentsChange);

   ui->textEditExpression->setContextMenuPolicy(Qt::CustomContextMenu);
   connect(ui->textEditExpression, &QExpressionTextEdit::customContextMenuRequested, this, &QExpressionEditorWidget::onExpressionContextMenu);
}

QExpressionEditorWidget::~QExpressionEditorWidget()
{
   delete _editHandler;
   delete ui;
}

void QExpressionEditorWidget::expandFirstLevel()
{
   for (auto row = 0; row < _varTreeModel->rootNode()->childCount(); ++row)
      ui->treeViewVariables->expand(_varTreeModel->index(row, 0));
   for (auto row = 0; row < _functionTreeModel->rootNode()->childCount(); ++row)
      ui->treeViewFunctions->expand(_functionTreeModel->index(row, 0));
   ui->treeViewFunctions->header()->resizeSections(QHeaderView::ResizeToContents);
}

void QExpressionEditorWidget::setEditHandler(AbstractEditHandler* editHandler)
{
   delete _editHandler;
   _editHandler = editHandler;
}

void QExpressionEditorWidget::setExpression(const QString& expression)
{
   _origExpression = expression;
   ui->textEditExpression->setPlainText(expression);
}

QString QExpressionEditorWidget::expression() const
{
   return ui->textEditExpression->toPlainText();
}

void QExpressionEditorWidget::loadSettings(QSettingsEx settings)
{
   auto splitterState = settings->value("SplitterVariablesFunctions").toByteArray();

   if (!splitterState.isEmpty())
      ui->splitterVariablesFunctions->restoreState(splitterState);

   splitterState = settings->value("SplitterFormula").toByteArray();

   if (!splitterState.isEmpty())
      ui->splitterFormula->restoreState(splitterState);

   splitterState = settings->value("SplitterResult").toByteArray();

   if (!splitterState.isEmpty())
      ui->splitterResult->restoreState(splitterState);
}

void QExpressionEditorWidget::saveSettings(QSettingsEx settings)
{
   if (settings)
   {
      settings->setValue("SplitterVariablesFunctions", ui->splitterVariablesFunctions->saveState());
      settings->setValue("SplitterFormula", ui->splitterFormula->saveState());
      settings->setValue("SplitterResult", ui->splitterResult->saveState());
   }
}

bool QExpressionEditorWidget::hasExpressionChanged() const
{
   return ui->textEditExpression->toPlainText() != _origExpression;
}

void QExpressionEditorWidget::on_lineEditVarFilter_textChanged(const QString& text)
{
   for (auto&& node : _varTreeModel->rootNode()->children())
   {
      filterTree(ui->treeViewVariables, _varTreeModel, node, text);
   }

   if (!text.isEmpty())
      ui->treeViewVariables->expandAll();
}

void QExpressionEditorWidget::on_lineEditFunctionFilter_textChanged(const QString& text)
{
   for (auto&& node : _functionTreeModel->rootNode()->children())
   {
      filterTree(ui->treeViewFunctions, _functionTreeModel, node, text);
   }

   if (!text.isEmpty())
      ui->treeViewFunctions->expandAll();
}

void QExpressionEditorWidget::on_treeViewVariables_doubleClicked(const QModelIndex& index)
{
   if (!index.isValid())
      return;

   ui->textEditExpression->insertPlainText(pathFromNode(_varTreeModel->nodeFromIndex(index)));
   ui->textEditExpression->setFocus();
}

void QExpressionEditorWidget::on_treeViewVariables_keyPressed(QKeyEvent* e)
{
   if (e->key() == Qt::Key_Return || e->key() == Qt::Key_Enter)
   {
      on_treeViewVariables_doubleClicked(ui->treeViewVariables->currentIndex());
      e->accept();
   }
}

void QExpressionEditorWidget::on_treeViewFunctions_doubleClicked(const QModelIndex& index)
{
   if (!index.isValid())
      return;

   auto functionName = index.sibling(index.row(), 0).data().toString();

   if (functionName.isEmpty())
      return;

   _tokenizer.setText(ui->textEditExpression->toPlainText());

   _tokenizer.setIgnoreWhiteSpace();
   _tokenizer.setIgnoreComments();

   auto cursor = ui->textEditExpression->textCursor();
   auto addDot = false;

   if (index.sibling(index.row(), 0).data(Qt::UserRole).toBool())
   {
      for (auto token = _tokenizer.next(); token.isValid(); token = _tokenizer.next())
      {
         if (cursor.position() <= token.endPos())
         {
            if (token.is<Token::Identifier>() || (token.is<CppOperatorToken>() && *token.as<CppOperatorToken>()->op() == ')'))
            {
               if (cursor.position() >= token.startPos())
               {
                  if (cursor.position() < token.endPos())
                     cursor.setPosition(token.endPos());

                  addDot = true;
               }
            }

            break;
         }
      }
   }

   if (addDot)
      functionName.prepend(u'.');
   functionName.append("()");

   cursor.insertText(functionName);

   cursor.movePosition(QTextCursor::PreviousCharacter);
   ui->textEditExpression->setTextCursor(cursor);

   ui->textEditExpression->setFocus();
}

void QExpressionEditorWidget::on_treeViewFunctions_keyPressed(QKeyEvent* e)
{
   if (e->key() == Qt::Key_Return || e->key() == Qt::Key_Enter)
   {
      on_treeViewVariables_doubleClicked(ui->treeViewFunctions->currentIndex());
      e->accept();
   }
}

void QExpressionEditorWidget::onDocumentContentsChange(int position, int charsRemoved, int charsAdded)
{
   ui->labelResult->setText(QString());

   const auto text = ui->textEditExpression->toPlainText();

   if (text.trimmed().isEmpty())
   {
      ui->labelResultIcon->setPixmap(QPixmap());
      return;
   }

   ui->labelResultIcon->setPixmap(QPixmap(":/icons/free/Error.png").scaled(16, 16, Qt::KeepAspectRatio, Qt::SmoothTransformation));

   _tokenizer.setText(text);
   _tokenizer.setIgnoreWhiteSpace(true);
   _tokenizer.setIgnoreComments(false);

   const auto tokenList = _tokenizer.tokenList();

   Token firstErrorToken;
   Token lastErrorToken;

   try
   {
      const auto expression = createCppExpressionTree(tokenList);

      const auto result = expression->resolve(cloneReduced(_varTree.root()), _functionMap);

      QTextDocument document;
      QTextCursor tc(&document);

      insertVariable(tc, result);

      ui->labelResult->setText(document.toHtml());
      ui->labelResultIcon->setPixmap(QPixmap(":/icons/free/OK.png").scaled(16, 16, Qt::KeepAspectRatio, Qt::SmoothTransformation));
   }
   catch (const TokenException& exception)
   {
      firstErrorToken = lastErrorToken = exception.token();

      ui->labelResult->setText(exception.message());
   }
   catch (const ExpressionException& exception)
   {
      if (const auto opExpr = exception.expr().as<AbstractOperatorExpression>())
      {
         firstErrorToken = lastErrorToken = dynamic_cast<const CppTokenOperator&>(opExpr->op()).token();
      }
      else if (const auto valExpr = exception.expr().as<AbstractValueExpression>())
      {
         firstErrorToken = lastErrorToken = dynamic_cast<const CppTokenValue&>(valExpr->value()).token();
      }

      ui->labelResult->setText(exception.message());
   }
   catch (const QBasicException& exception)
   {
      ui->labelResult->setText(exception.message());
   }

   QTextCharFormat commentFormat;
   commentFormat.setForeground(Qt::darkGreen);

   QTextCharFormat keywordFormat;
   keywordFormat.setFontWeight(QFont::Bold);

   QTextCharFormat textLiteralFormat;
   textLiteralFormat.setForeground(Qt::darkBlue);

   QTextCharFormat numericLiteralFormat;
   numericLiteralFormat.setForeground(Qt::darkRed);

   const auto document = ui->textEditExpression->document();
   auto iToken = 0;

   for (auto textBlock = document->begin(); textBlock.isValid(); textBlock = textBlock.next())
   {
      auto layout = textBlock.layout();
      auto ranges = layout->formats();
      auto formatsChanged = false;

      if (!ranges.isEmpty())
      {
         ranges.clear();
         formatsChanged = true;
      }

      while (iToken < tokenList.count() && tokenList.at(iToken).startPos() < textBlock.position() + textBlock.length())
      {
         const auto& token = tokenList.at(iToken);
         QTextLayout::FormatRange r;

         r.start = token.startPos() - textBlock.position();
         r.length = token.endPos() - token.startPos();
         if (r.start + r.length > textBlock.length())
            r.length = textBlock.length() - r.start;

         if (token.is<Token::Comment>())
         {
            r.format = commentFormat;
         }
         else if (token.is<Token::Text>() || token.is<Token::Binary>())
         {
            r.format = textLiteralFormat;
         }
         else if (token.is<Token::Integer>() || token.is<Token::Decimal>())
         {
            r.format = numericLiteralFormat;
         }
         else if (token.isIdentifier("base") || token.isIdentifier("true") || token.isIdentifier("false") || token.isIdentifier("this"))
         {
            r.format = keywordFormat;
         }

         if (token.startPos() < lastErrorToken.endPos() && token.endPos() > firstErrorToken.startPos())
         {
            r.format.setUnderlineColor(Qt::red);
            r.format.setUnderlineStyle(QTextCharFormat::SpellCheckUnderline);
         }

         if (!r.format.isEmpty())
         {
            ranges << r;
            formatsChanged = true;
         }

         if (token.endPos() > textBlock.position() + textBlock.length())
            break;

         iToken++;
      }

      if (formatsChanged)
      {
         layout->setFormats(ranges);
         document->markContentsDirty(textBlock.position(), textBlock.length());
      }
   }
}

void QExpressionEditorWidget::onExpressionContextMenu(const QPoint& pos)
{
   auto tc = ui->textEditExpression->cursorForPosition(pos);

   Token currentToken;

   if (!tc.isNull())
   {
      _tokenizer.setText(ui->textEditExpression->toPlainText());
      _tokenizer.setIgnoreWhiteSpace(true);
      _tokenizer.setIgnoreComments(false);

      for (auto token = _tokenizer.next(); token.isValid(); token = _tokenizer.next())
      {
         if (tc.position() <= token.endPos() && tc.position() >= token.startPos())
         {
            currentToken = token;
            break;
         }
      }
   }

   auto menu = ui->textEditExpression->createStandardContextMenu(pos);
   QAction* editAction = nullptr;

   if (_editHandler && currentToken.isValid() && _editHandler->canEdit(currentToken))
   {
      menu->addSeparator();
      editAction = menu->addAction(tr("Edit"));
      tc.setPosition(currentToken.startPos());
      tc.setPosition(currentToken.endPos(), QTextCursor::KeepAnchor);
   }

   const auto selectedAction = menu->exec(ui->textEditExpression->mapToGlobal(pos));

   if (selectedAction && selectedAction == editAction)
   {
      tc.insertText(_editHandler->edit(this, currentToken));
   }

   delete menu;
}

static void addFunctionDescription(HtNode* groupNode, const QString& name, const AbstractFunctionExpression& expr, bool isMember)
{
   auto node = groupNode->appendChild();
   node->setAttribute(0, name);
   node->setAttribute(0, expr.description(), Qt::ToolTipRole);
   node->setAttribute(0, isMember, Qt::UserRole);
   node->setAttribute(1, expr.returnType());

   QStringList parDesc;
   for (auto&& parameter : expr.mandatoryParameters().mid(isMember ? 1 : 0))
      parDesc.append(parameter.first);
   for (auto&& parameter : expr.optionalParameters())
      parDesc.append("[" + parameter.first + "]");

   node->setAttribute(2, parDesc.join(", "));
   node->setAttribute(3, expr.description());
}

void QExpressionEditorWidget::createFunctionModel(const ResolverFunctionMap& functionMap)
{
   _functionTreeModel = new HtStandardModel(this);

   _functionTreeModel->setHeader(0, tr("Function"));
   _functionTreeModel->setHeader(1, tr("Return value"));
   _functionTreeModel->setHeader(2, tr("Parameter"));
   _functionTreeModel->setHeader(3, tr("Description"));

   auto globalGroup = _functionTreeModel->rootNode()->appendChild();
   globalGroup->setAttribute(0, tr("Global functions"));

   auto dictGroup = _functionTreeModel->rootNode()->appendChild();
   dictGroup->setAttribute(0, tr("Dictionary functions"));

   auto listGroup = _functionTreeModel->rootNode()->appendChild();
   listGroup->setAttribute(0, tr("List functions"));

   //auto functionGroup = _functionTreeModel->rootNode()->appendChild();
   //functionGroup->setAttribute(0, tr("Function functions"));

   auto valueGroup = _functionTreeModel->rootNode()->appendChild();
   valueGroup->setAttribute(0, tr("Value functions"));

   for (auto it = functionMap.begin(); it != functionMap.end(); ++it)
   {
      const auto functionExpression = it->first.as<AbstractFunctionExpression>();

      if (!functionExpression)
         continue;

      QStringList memberTypes;

      if (it->second)
      {
         // Member function
#if QT_VERSION < QT_VERSION_CHECK(5,14,0)
         memberTypes = functionExpression->mandatoryParameters().first().second.split(u'|', QString::SkipEmptyParts);
#else
         memberTypes = functionExpression->mandatoryParameters().first().second.split(u'|', Qt::SkipEmptyParts);
#endif
      }

      if (memberTypes.isEmpty())
      {
         addFunctionDescription(globalGroup, it.key(), *functionExpression, false);
      }
      if (memberTypes.contains("Dictionary"))
      {
         addFunctionDescription(dictGroup, it.key(), *functionExpression, true);
         memberTypes.removeOne("Dictionary");
      }
      if (memberTypes.contains("List"))
      {
         addFunctionDescription(listGroup, it.key(), *functionExpression, true);
         memberTypes.removeOne("List");
      }
      //if (memberTypes.contains("Function"))
      //   addFunctionDescription(functionGroup, it.key(), *functionExpression, true);
      if (memberTypes.contains("Value"))
      {
         addFunctionDescription(valueGroup, it.key(), *functionExpression, true);
      }
      else
      {
         for (auto&& memberType : memberTypes)
         {
            HtNode* memberGroup = nullptr;

            for (auto i = 0; i < valueGroup->childCount(); ++i)
            {
               if (valueGroup->child(i)->attribute(0, Qt::UserRole).toString() == memberType)
               {
                  memberGroup = valueGroup->child(i);
                  break;
               }
            }

            if (memberGroup == nullptr)
            {
               memberGroup = valueGroup->appendChild();
               memberGroup->setAttribute(0, tr("%1 functions").arg(memberType));
               memberGroup->setAttribute(0, memberType, Qt::UserRole);
            }

            addFunctionDescription(memberGroup, it.key(), *functionExpression, true);
         }
      }
   }
}
