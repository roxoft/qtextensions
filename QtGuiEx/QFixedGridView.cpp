#include "stdafx.h"
#include "QFixedGridView.h"
#include "QFixedGridWidget.h"
#include <QScrollArea>
#include <QAction>
#include <QDialogEx>
#include <QSlider>
#include <QVBoxLayout>

struct FixedGridItemState
{
   QString type;
   QRect area;
   QByteArray state;
};

static QDataStream& operator<<(QDataStream& dataStream, const FixedGridItemState& itemState)
{
   return dataStream << itemState.type << itemState.area << itemState.state;
}

static QDataStream& operator>>(QDataStream& dataStream, FixedGridItemState& itemState)
{
   return dataStream >> itemState.type >> itemState.area >> itemState.state;
}

QFixedGridView::QFixedGridView(QWidget *parent) : QWidget(parent)
{
   QPalette p;

   p.setColor(QPalette::Window, Qt::white);
   setPalette(p);

   setAutoFillBackground(true);

   _actionChangeGridSize = new QAction(tr("Change grid size"));
   connect(_actionChangeGridSize, &QAction::triggered, this, &QFixedGridView::onChangeGridSize);
   addAction(_actionChangeGridSize);
   setContextMenuPolicy(Qt::ActionsContextMenu);
}

QFixedGridView::~QFixedGridView()
{
   for (auto i = _itemList.count(); i--;)
   {
      auto item = _itemList.at(i).item;
      _itemList.removeAt(i);
      delete item;
   }
}

QSize QFixedGridView::sizeHint() const
{
   QSize size; // The grid always starts with row 0 and column 0 therefor we only need the size

   for (auto&& itemInfo : _itemList)
   {
      if (itemInfo.area.right() + 1 > size.width())
         size.setWidth(itemInfo.area.right() + 1);
      if (itemInfo.area.bottom() + 1 > size.height())
         size.setHeight(itemInfo.area.bottom() + 1);
   }

   if (size.isEmpty())
      return QSize(_cellSize, _cellSize); // Minimum size of the widget

   return size * _cellSize;
}

QPoint QFixedGridView::cellFromPos(QPoint pos) const
{
   return pos / _cellSize;
}

void QFixedGridView::insertWidget(QWidget* w)
{
   if (!w)
      return;

   auto area = cellArea(QRect(QPoint(0, 0), w->size()));

   for (auto r = 0; r < 100; r++)
   {
      auto x = r;
      auto y = 0;

      while (y < r)
      {
         area.moveTopLeft(QPoint(x, y));
         if (areaIsFree(area))
            break;
         y++;
      }
      if (y < r)
         break;

      x = 0;
      while (x <= r)
      {
         area.moveTopLeft(QPoint(x, y));
         if (areaIsFree(area))
            break;
         x++;
      }
      if (x <= r)
         break;

      area.moveTopLeft(QPoint(0, 0));
   }

   insertWidget(w, area);
}

void QFixedGridView::insertWidget(QWidget* w, int row, int column, int rowSpan, int columnSpan)
{
   insertWidget(w, QRect(row, column, rowSpan, columnSpan));
}

void QFixedGridView::insertWidget(QWidget* w, const QRect& area)
{
   if (!w)
      return;

   w->setParent(this);
   connect(w, &QWidget::destroyed, this, &QFixedGridView::onWidgetDeleted);

   for (auto i = 0; i < 100; i++)
   {
      auto uniqueObjectName = w->objectName();

      if (i > 0)
         uniqueObjectName += QString::number(i);

      for (auto&& itemInfo : _itemList)
      {
         if (itemInfo.item->objectName() == uniqueObjectName)
         {
            uniqueObjectName.clear();
            break;
         }
      }
      if (!uniqueObjectName.isEmpty())
      {
         w->setObjectName(uniqueObjectName);
         break;
      }
   }

   ItemInfo itemInfo;

   itemInfo.item = w;
   if (area.isEmpty())
   {
      itemInfo.area = cellArea(QRect(QPoint(0, 0), w->size()));
      itemInfo.area.moveTopLeft(area.topLeft());
   }
   else
   {
      itemInfo.area = area;
   }

   _itemList.append(itemInfo);

   setMinimumSize(sizeHint());
   doLayout(itemInfo);
   if (isVisible())
      itemInfo.item->show();

   ensureWidgetVisible(itemInfo.item);
}

QList<QWidget*> QFixedGridView::gridWidgets() const
{
   QList<QWidget*> result;

   for (auto&& itemInfo : _itemList)
      result.append(itemInfo.item);

   return result;
}

void QFixedGridView::snapIn(QWidget* w)
{
   if (_movingItem == nullptr)
   {
      for (auto&& itemInfo : _itemList)
      {
         if (itemInfo.item == w)
         {
            _movingItem = &itemInfo;
            break;
         }
      }
   }

   if (_movingItem)
   {
      _movingItem->area = cellArea(w->geometry());
      doLayout(*_movingItem);
      setMinimumSize(sizeHint());
      update(); // Erase the grid
      ensureWidgetVisible(_movingItem->item);
      _movingItem = nullptr;
   }
}

void QFixedGridView::previewSnapArea(QWidget* w)
{
   if (_movingItem == nullptr)
   {
      for (auto&& itemInfo : _itemList)
      {
         if (itemInfo.item == w)
         {
            _movingItem = &itemInfo;
            break;
         }
      }
   }

   if (_movingItem)
   {
      _movingItem->area = cellArea(w->geometry());
      update(); // Show the grid and the area
   }
}

void QFixedGridView::ensureWidgetVisible(QWidget* widget)
{
   auto viewport = parentWidget();

   if (viewport)
   {
      auto scrollArea = qobject_cast<QScrollArea*>(viewport->parentWidget());

      if (scrollArea)
         scrollArea->ensureWidgetVisible(widget);
   }
}

QByteArray QFixedGridView::saveState() const
{
   QByteArray result;

   QMap<QString, QRect> itemState;

   for (auto&& itemInfo : _itemList)
   {
      itemState.insert(itemInfo.item->objectName(), itemInfo.area);
   }

   QDataStream dataStream(&result, QIODevice::WriteOnly);

   dataStream << (qint32)1; // Version number

   dataStream.setVersion(QDataStream::Qt_5_6);

   dataStream << _cellSize << _cellMargin << itemState;

   return result;
}

void QFixedGridView::restoreState(const QByteArray& data)
{
   QDataStream dataStream(data);

   qint32 version;

   dataStream >> version;

   if (version != 1)
      return;

   dataStream.setVersion(QDataStream::Qt_5_6);

   QMap<QString, QRect> itemState;

   dataStream >> _cellSize >> _cellMargin >> itemState;

   for (auto&& itemInfo : _itemList)
   {
      auto area = itemState.value(itemInfo.item->objectName());
      if (!area.isEmpty())
         itemInfo.area = area;
   }

   setMinimumSize(sizeHint());
   doLayout();
}

QByteArray QFixedGridView::saveState(const QFixedGridWidgetFactory* widgetFactory) const
{
   if (widgetFactory == nullptr)
      return saveState();

   QByteArray result;

   QList<FixedGridItemState> itemStates;

   for (auto&& itemInfo : _itemList)
   {
      auto widget = dynamic_cast<QFixedGridWidget*>(itemInfo.item);

      if (widget)
      {
         FixedGridItemState itemState;

         itemState.type = widgetFactory->widgetType(widget);

         if (!itemState.type.isEmpty())
         {
            itemState.area = itemInfo.area;
            itemState.state = widgetFactory->widgetState(widget);

            itemStates.append(itemState);
         }
      }
   }

   QDataStream dataStream(&result, QIODevice::WriteOnly);

   dataStream << (qint32)1; // Version number

   dataStream.setVersion(QDataStream::Qt_5_6);

   dataStream << _cellSize << _cellMargin << itemStates;

   return result;
}

void QFixedGridView::restoreState(const QByteArray& data, const QFixedGridWidgetFactory* widgetFactory)
{
   if (widgetFactory == nullptr)
   {
      restoreState(data);
      return;
   }

   while (!_itemList.isEmpty())
      delete _itemList.last().item;

   QDataStream dataStream(data);

   qint32 version;

   dataStream >> version;

   if (version != 1)
      return;

   dataStream.setVersion(QDataStream::Qt_5_6);

   QList<FixedGridItemState> itemStates;

   dataStream >> _cellSize >> _cellMargin >> itemStates;

   for (auto&& itemState : itemStates)
   {
      auto widget = widgetFactory->createWidget(itemState.type);

      if (widget)
      {
         widgetFactory->setWidgetState(widget, itemState.state);
         insertWidget(widget, itemState.area);
      }
   }
}

void QFixedGridView::setCellSize(int cellSize)
{
   if (cellSize == _cellSize)
      return;

   _cellSize = cellSize;
   setMinimumSize(sizeHint());
   doLayout();
   if (_showGrid)
      update();
}

void QFixedGridView::paintEvent(QPaintEvent* event)
{
   QWidget::paintEvent(event);

   if (!_movingItem && !_showGrid)
      return;

   QPainter painter(this);

   if (_movingItem)
      painter.fillRect(_movingItem->area.x() * _cellSize, _movingItem->area.y() * _cellSize, _movingItem->area.width() * _cellSize, _movingItem->area.height() * _cellSize, palette().color(QPalette::Dark));

   painter.setPen(gridColor());

   for (auto x = 0; x < width(); x += _cellSize)
   {
      painter.drawLine(x, 0, x, height() - 1);
   }

   for (auto y = 0; y < height(); y += _cellSize)
   {
      painter.drawLine(0, y, width() - 1, y);
   }
}

void QFixedGridView::onChangeGridSize()
{
   QDialogEx dlg(window());

   dlg.setWindowTitle(_actionChangeGridSize->text());

   dlg.resize(400, 32);

   auto slider = new QSlider(this);

   slider->setOrientation(Qt::Horizontal);
   slider->setRange(40, 200);
   slider->setSingleStep(5);
   slider->setPageStep(20);
   slider->setTickPosition(QSlider::TicksBelow);
   slider->setTickInterval(20);

   slider->setValue(cellSize());

   connect(slider, &QSlider::valueChanged, this, &QFixedGridView::setCellSize);

   auto layout = new QVBoxLayout;

   layout->addWidget(slider);

   dlg.setLayout(layout);

   _showGrid = true;
   dlg.exec();
   _showGrid = false;
}

void QFixedGridView::doLayout() const
{
   for (auto&& itemInfo : _itemList)
   {
      doLayout(itemInfo);
   }
}

void QFixedGridView::doLayout(const ItemInfo& itemInfo) const
{
   itemInfo.item->setGeometry(widgetArea(itemInfo.area));
}

QRect QFixedGridView::cellArea(const QRect& rect) const
{
   return QRect(
      (rect.x() + _cellSize / 2) / _cellSize,
      (rect.y() + _cellSize / 2) / _cellSize,
      qMax((rect.width() + _cellSize / 2) / _cellSize, 1),
      qMax((rect.height() + _cellSize / 2) / _cellSize, 1));
}

QRect QFixedGridView::widgetArea(const QRect& cellArea) const
{
   return QRect(
      cellArea.left() * _cellSize + _cellMargin,
      cellArea.top() * _cellSize + _cellMargin,
      cellArea.width() * _cellSize - 2 * _cellMargin,
      cellArea.height() * _cellSize - 2 * _cellMargin);
}

QList<QFixedGridView::ItemInfo> QFixedGridView::items(const QRect& cellArea) const
{
   QList<ItemInfo> result;

   for (auto&& itemInfo : _itemList)
   {
      if (itemInfo.area.intersects(cellArea))
         result.append(itemInfo);
   }

   return result;
}

bool QFixedGridView::areaIsFree(const QRect& cellArea) const
{
   for (auto&& itemInfo : _itemList)
   {
      if (itemInfo.area.intersects(cellArea))
         return false;
   }
   return true;
}

void QFixedGridView::onWidgetDeleted(QObject* obj)
{
   for (auto i = _itemList.count(); i--; )
   {
      if (_itemList.at(i).item == obj)
      {
         _itemList.removeAt(i);
      }
   }

   setMinimumSize(sizeHint());
}
