#include "stdafx.h"
#include "WidgetTools.h"
#include <QWidget>
#if QT_VERSION < QT_VERSION_CHECK(5,0,0)
#include <QDesktopWidget>
#endif
#include <QApplication>
#ifdef _WIN32
#include <Windows.h>
#endif

// This function is only valid for windows (isWindow() == true) and the window must already be positioned correctly
void adjustToScreen(QWidget *widget)
{
   if (widget == nullptr)
      return;

   QRect frameRect = widget->frameGeometry();

   // Get the major screen for the new rectangle (with the greatest overlapping area)
   QScreen* majorScreen = nullptr;
   auto maxAreaOnScreen = 0;
   for (auto&& screen : QGuiApplication::screens())
   {
      QRect sect = screen->geometry().intersected(frameRect);

      if (sect.isValid())
      {
         int area = sect.width() * sect.height();

         if (maxAreaOnScreen < area)
         {
            maxAreaOnScreen = area;
            majorScreen = screen;
         }
      }
   }

   if (majorScreen == nullptr)
      majorScreen = QGuiApplication::primaryScreen();

   const QRect screenGeometry = majorScreen->geometry();

   // Make sure the top right corner is visible
   if (frameRect.left() < screenGeometry.left())
      frameRect.moveLeft(screenGeometry.left());
   if (frameRect.bottom() > screenGeometry.bottom())
      frameRect.moveBottom(screenGeometry.bottom());
   if (frameRect.right() > screenGeometry.right())
      frameRect.moveRight(screenGeometry.right());
   if (frameRect.top() < screenGeometry.top())
      frameRect.moveTop(screenGeometry.top());

   if (frameRect.topLeft() != widget->frameGeometry().topLeft())
      widget->move(frameRect.topLeft());
}

void centerOnParent(QWidget *widget)
{
   if (widget == nullptr || widget->parentWidget() == nullptr)
      return;

   QWidget *parentWindow = widget->parentWidget()->window();

   if (parentWindow == nullptr)
      return;

   QRect    frameRect = widget->frameGeometry();
   QPoint   center;

   if (parentWindow->windowHandle() && parentWindow->windowHandle()->property("_q_embedded_native_parent_handle").value<WId>())
      center = parentWindow->frameGeometry().center();
   else
      center = parentWindow->mapToGlobal(QPoint(0, 0)) + QPoint(parentWindow->width() / 2, parentWindow->height() / 2);

   frameRect.moveCenter(center);

   widget->move(frameRect.topLeft());
}

#if QT_VERSION < QT_VERSION_CHECK(5,0,0)
void adjustPosition(QWidget *widget, bool centerOnParent)
{
   if (widget == nullptr)
      return;

   QRect frameRect = widget->frameGeometry();

   if (!widget->isWindow() && widget->parentWidget())
      frameRect.moveTo(widget->parentWidget()->mapToGlobal(frameRect.topLeft()));

   if (centerOnParent)
   {
      QWidget *parentWindow = widget->parentWidget();

      if (parentWindow)
      {
         parentWindow = parentWindow->window();

         frameRect.moveCenter(parentWindow->mapToGlobal(parentWindow->rect().center()));
      }
   }

   // Get the major screen for the new rectangle (with the greatest overlapping area)
   QDesktopWidget* desktop = QApplication::desktop();

   int majorScreen = desktop->primaryScreen();
   int maxAreaOnScreen = 0;

   for (int i = 0; i < desktop->screenCount(); ++i)
   {
      QRect sect = desktop->screenGeometry(i).intersected(frameRect);

      if (sect.isValid())
      {
         int area = sect.width() * sect.height();

         if (maxAreaOnScreen < area)
         {
            maxAreaOnScreen = area;
            majorScreen = i;
         }
      }
   }

   QRect screenGeometry = desktop->availableGeometry(majorScreen);

   // Make sure the top right corner is visible
   if (frameRect.left() < screenGeometry.left())
      frameRect.moveLeft(screenGeometry.left());
   if (frameRect.bottom() > screenGeometry.bottom())
      frameRect.moveBottom(screenGeometry.bottom());
   if (frameRect.right() > screenGeometry.right())
      frameRect.moveRight(screenGeometry.right());
   if (frameRect.top() < screenGeometry.top())
      frameRect.moveTop(screenGeometry.top());

   // Move the widget to the new position (take differences between screen coordinates and widget coordinates into account)
   if (!widget->isWindow() && widget->parentWidget())
      frameRect.moveTo(widget->parentWidget()->mapFromGlobal(frameRect.topLeft()));

   widget->move(frameRect.topLeft());
}
#endif
