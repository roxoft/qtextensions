#include "stdafx.h"
#include "QDateEditEx.h"
#include <QKeyEvent>
#include <QFocusEvent>
#include <QMouseEvent>
#include "QComboBoxEx.h"
#include <QCalendarWidget>
#include <QStyleOptionFrame>
#include <QApplication>

static const QDate absoluteMinimumDate(100, 1, 1);
static const QDate defaultMinimumDate(1752, 9, 14);
static const QDate absoluteMaximumDate(9999, 12, 31);

QDateEditEx::QDateEditEx(QWidget *parent)
   : QLineEdit(parent),
   _alignTo(this),
   _calendarWidget(nullptr),
   _minimumDate(defaultMinimumDate),
   _maximumDate(absoluteMaximumDate),
   _emptyAllowed(true),
   _isEditing(false)
{
   _validator = new QDateValidator(this);
   this->setValidator(_validator);

   connect((QLineEdit*)this, SIGNAL(cursorPositionChanged(int, int)), this, SLOT(onCursorPositionChanged(int, int)));
   connect((QLineEdit*)this, SIGNAL(textEdited(const QString&)), this, SLOT(onTextEdited(const QString&)));
   connect((QLineEdit*)this, SIGNAL(textChanged(const QString&)), this, SLOT(onTextChanged(const QString&)));
   connect((QLineEdit*)this, SIGNAL(editingFinished()), this, SLOT(onEditingFinished()));

   // If parent is a QComboBoxEx, connect to it
   QComboBoxEx* combo = qobject_cast<QComboBoxEx*>(parent);

   if (combo)
   {
      combo->setContent(this);
      connect(combo, SIGNAL(clicked()), this, SLOT(showPopup()));
      _alignTo = combo;
   }
}

QDateEditEx::~QDateEditEx()
{
   delete _calendarWidget;
}

QDate QDateEditEx::date() const
{
   return _validator->toDate(this->text());
}

void QDateEditEx::setDate(const QDate& d)
{
   // setting the date during editing is not allowed
   if (!_isEditing)
   {
      _oldDate = d;
      _isEditing = true;
      this->setText(d.toString(_validator->dateFormat()));
      _isEditing = false;
   }
}

void QDateEditEx::setMinimumDate(const QDate& date)
{
   if (date.isValid())
      _minimumDate = date;
   else
      _minimumDate = absoluteMinimumDate;

   if (_calendarWidget != nullptr)
      _calendarWidget->setMinimumDate(_minimumDate);

   if (_maximumDate < _minimumDate)
      setMaximumDate(_minimumDate);
}

void QDateEditEx::clearMinimumDate()
{
   setMinimumDate(defaultMinimumDate);
}

void QDateEditEx::setMaximumDate(const QDate& date)
{
   if (date.isValid())
      _maximumDate = date;
   else
      _maximumDate = absoluteMaximumDate;

   if (_calendarWidget != nullptr)
      _calendarWidget->setMaximumDate(_maximumDate);

   if (_maximumDate < _minimumDate)
      setMinimumDate(_maximumDate);
}

void QDateEditEx::clearMaximumDate()
{
   setMaximumDate(absoluteMaximumDate);
}

void QDateEditEx::setDateRange(const QDate& min, const QDate& max)
{
   if (max < min)
   {
      setMinimumDate(max);
      setMaximumDate(min);
   }
   else
   {
      setMinimumDate(min);
      setMaximumDate(max);
   }
}

QSize QDateEditEx::sizeHint() const
{
   return minimumSizeHint();
}

QSize QDateEditEx::minimumSizeHint() const
{
   const int verticalMargin = 1; // Defined in qlineedit_p.cpp
   const int horizontalMargin = 2; // Defined in qlineedit_p.cpp

   ensurePolished();

   QSize size = QLineEdit::fontMetrics().size(Qt::TextSingleLine, QDate(2000, 12, 24).toString(_validator->dateFormat()));

   if (size.height() < 14)
      size.setHeight(14);

   size.rheight() += 2 * verticalMargin;
   size.rheight() += QLineEdit::textMargins().top() + QLineEdit::textMargins().bottom();
   size.rheight() += QLineEdit::contentsMargins().top() + QLineEdit::contentsMargins().bottom();
   size.rwidth() += 2 * horizontalMargin;
   size.rwidth() += QLineEdit::textMargins().left() + QLineEdit::textMargins().right();
   size.rwidth() += QLineEdit::contentsMargins().left() + QLineEdit::contentsMargins().right();

   QStyleOptionFrame opt;

   QLineEdit::initStyleOption(&opt);

   return style()->sizeFromContents(QStyle::CT_LineEdit, &opt, size, this);
}

bool QDateEditEx::event(QEvent* e)
{
   // Parent changes
   if (e->type() == QEvent::ParentAboutToChange)
   {
      QComboBoxEx* combo = qobject_cast<QComboBoxEx*>(this->parent());

      if (combo)
      {
         disconnect(combo, SIGNAL(clicked()), this, SLOT(showPopup()));
         combo->setContent(nullptr);
      }
      if (_alignTo == this->parent())
         _alignTo = this;
   }
   else if (e->type() == QEvent::ParentChange)
   {
      QComboBoxEx* combo = qobject_cast<QComboBoxEx*>(this->parent());

      if (combo)
      {
         combo->setContent(this);
         connect(combo, SIGNAL(clicked()), this, SLOT(showPopup()));
         if (_alignTo == this)
            _alignTo = combo;
      }
   }

   return QLineEdit::event(e);
}

void QDateEditEx::showPopup()
{
   if (_calendarWidget == nullptr)
   {
      _calendarWidget = new QCalendarWidget(nullptr);

      _calendarWidget->setWindowFlags(_calendarWidget->windowFlags() | Qt::Popup);

      _calendarWidget->setFocusProxy(this);

      _calendarWidget->setDateEditEnabled(false);

      _calendarWidget->setMinimumDate(_minimumDate);
      _calendarWidget->setMaximumDate(_maximumDate);

      connect(_calendarWidget, SIGNAL(activated(const QDate&)), this, SLOT(onCalendarDateSelected(const QDate&)));
      connect(_calendarWidget, SIGNAL(clicked(const QDate&)), this, SLOT(onCalendarDateSelected(const QDate&)));
   }

   _calendarWidget->setSelectedDate(this->date());

   // Calculte popup position
   QPoint topLeft = _alignTo->parentWidget()->mapToGlobal(_alignTo->geometry().bottomLeft());

   _calendarWidget->move(topLeft);
   _calendarWidget->show();
}

void QDateEditEx::keyPressEvent(QKeyEvent* e)
{
   int                        cursorPos = this->cursorPosition();
   QDateValidator::Section*   section = _validator->sectionFromPos(cursorPos);

   if (section)
   {
      if (e->key() == Qt::Key_Backspace)
      {
         if (cursorPos == section->begin && !hasSelectedText())
         {
            // Move the cursor to the previous section
            section = _validator->prevSection(section);
            if (section)
            {
               cursorPos -= section->delimiter.length();
               this->setCursorPosition(cursorPos);
            }
            e->accept();
            return;
         }
      }
      else if (e->key() == Qt::Key_Delete)
      {
         // Do not delete delimiters
         if (cursorPos >= section->endOfContent(e->text()) && !hasSelectedText())
         {
            e->accept();
            return;
         }
      }
      else if (!e->text().isEmpty())
      {
         // Handle section delimiter keys
         if (this->hasSelectedText() || cursorPos == section->begin)
         {
            // Ignore the key if the previous character is a section delimiter and matches the key
            auto prevSection = _validator->prevSection(section);

            if (prevSection && prevSection->delimiter.startsWith(e->text()))
            {
               e->accept();
               return;
            }
         }
         else if (section->delimiter.startsWith(e->text()))
         {
            // Move the cursor to the beginning of the next section and select the next section
            e->accept();
            this->setCursorPosition(section->end(this->text()));
            selectSection(section->next());
            return;
         }

         // Change the edit text
         _isEditing = true;
         QLineEdit::keyPressEvent(e);

         // Move the cursor to the next section if the cursor is at the end of the section and the maximum number of characters have been entered
         cursorPos = this->cursorPosition();
         section = _validator->sectionFromPos(cursorPos);

         QString  text = this->text();
         int      end = section->endOfContent(text);

         if (cursorPos >= end && end - section->begin == section->maxLength)
         {
            this->setCursorPosition(section->end(text));
            selectSection(section->next());
         }
         return;
      }
   }

   // Change the edit text
   QLineEdit::keyPressEvent(e);
}

void QDateEditEx::focusInEvent(QFocusEvent* e)
{
   QLineEdit::focusInEvent(e);
   selectSection(_validator->firstSection());
}

void QDateEditEx::mouseReleaseEvent(QMouseEvent* e)
{
   QLineEdit::mouseReleaseEvent(e);
   //selectSection(_validator->sectionFromPos(this->cursorPosition()));
}

void QDateEditEx::onCursorPositionChanged(int oldPos, int newPos)
{
   // Fixup the old section
   QDateValidator::Section* oldSection = _validator->sectionFromPos(oldPos);
   QDateValidator::Section* newSection = _validator->sectionFromPos(newPos);

   if (oldSection && newSection != oldSection)
   {
      QString text(this->text());

      if (oldSection->fixup(text, newPos))
      {
         this->setText(text);
         this->setCursorPosition(newPos);
      }
   }
}

void QDateEditEx::onTextEdited(const QString&)
{
   _isEditing = true;
}

void QDateEditEx::onTextChanged(const QString& text)
{
   if (!_isEditing && !text.isEmpty())
   {
      QString                    changedText = text;
      int                        cursorPos = this->cursorPosition();
      QDateValidator::Section*   section = _validator->firstSection();
      bool                       changed = false;

      while (section)
      {
         if (section->fixup(changedText, cursorPos))
            changed = true;
         section = section->next();
      }

      if (_validator->fixupDate(changedText))
         changed = true;

      // Move the date into the valid range
      QDate dateValue(date());

      if (dateValue.isValid() && dateValue < _minimumDate)
      {
         changedText = _minimumDate.toString(_validator->dateFormat());
         changed = true;
      }
      else if (dateValue.isValid() && dateValue > _maximumDate)
      {
         changedText = _maximumDate.toString(_validator->dateFormat());
         changed = true;
      }

      if (changed)
      {
         _isEditing = true;
         this->setText(changedText);
         this->setCursorPosition(cursorPos);
         _isEditing = false;
      }
   }
}

void QDateEditEx::onEditingFinished()
{
   _isEditing = false;

   QString  text(this->text());
   bool     changed = false;

   if (!text.isEmpty())
   {
      int                        cursorPos = this->cursorPosition();
      QDateValidator::Section*   section = _validator->sectionFromPos(cursorPos);

      // Fixup the current section
      if (section)
         changed = section->fixup(text, cursorPos);

      // Fixup the complete date
      if (_validator->fixupDate(text))
         changed = true;
   }

   // Move the date into the valid range
   QDate dateValue = _validator->toDate(text);

   if (dateValue.isValid())
   {
      if (dateValue < _minimumDate)
      {
         text = _minimumDate.toString(_validator->dateFormat());
         changed = true;
      }
      else if (dateValue > _maximumDate)
      {
         text = _maximumDate.toString(_validator->dateFormat());
         changed = true;
      }
   }
   else if (!_emptyAllowed && _oldDate.isValid())
   {
      text = _oldDate.toString(_validator->dateFormat());
      changed = true;
   }

   if (changed)
   {
      _isEditing = true;
      this->setText(text);
      _isEditing = false;
   }

   _oldDate = date();
}

void QDateEditEx::onCalendarDateSelected(const QDate& date)
{
   _calendarWidget->hide();
   setDate(date);
   this->setFocus();
}

void QDateEditEx::selectSection(QDateValidator::Section* section)
{
   if (section)
   {
      int sectionBegin = section->begin;
      int sectionEnd = section->endOfContent(this->text());

      if (sectionBegin < sectionEnd)
      {
         this->blockSignals(true);
         this->setSelection(sectionBegin, sectionEnd - sectionBegin);
         this->blockSignals(false);
      }
   }
}
