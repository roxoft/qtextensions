#include "stdafx.h"
#include "QtGuiVariantTypes.h"

//
// Conversion functions
//

static bool convert(QByteArray& lhs, const QIcon& rhs, const QLocaleEx&)
{
   QDataStream s(&lhs, QIODevice::WriteOnly);
   s.setVersion(QDataStream::Qt_4_7);
   s << rhs;
   return true;
}

static bool convert(QIcon& lhs, const QByteArray& rhs, const QLocaleEx&)
{
   QDataStream s(rhs);
   s.setVersion(QDataStream::Qt_4_7);
   s >> lhs;
   return true;
}

static bool convert(QByteArray& lhs, const QImage& rhs, const QLocaleEx&)
{
   QDataStream s(&lhs, QIODevice::WriteOnly);
   s.setVersion(QDataStream::Qt_5_5);
   s << rhs;
   return true;
}

static bool convert(QImage& lhs, const QByteArray& rhs, const QLocaleEx&)
{
   QDataStream s(rhs);
   s.setVersion(QDataStream::Qt_5_5);
   s >> lhs;
   return true;
}

static bool convert(QVariant& lhs, Qt::GlobalColor rhs, const QLocaleEx&)
{
   lhs = QColor(rhs);
   return true;
}

//
// Variant operators
//

QTGUIEX_EXPORT SerStream& operator<<(SerStream& stream, const QIcon& icon)
{
   QByteArray a;
   QDataStream s(&a, QIODevice::WriteOnly);
   s.setVersion(QDataStream::Qt_4_7);
   s << icon;
   stream << a;
   return stream;
}

QTGUIEX_EXPORT SerStream& operator>>(SerStream& stream, QIcon& icon)
{
   QDataStream s(stream.readData());
   s.setVersion(QDataStream::Qt_4_7);
   s >> icon;
   return stream;
}

QTGUIEX_EXPORT SerStream& operator<<(SerStream& stream, const QImage& image)
{
   QByteArray a;
   QDataStream s(&a, QIODevice::WriteOnly);
   s.setVersion(QDataStream::Qt_5_5);
   s << image;
   stream << a;
   return stream;
}

QTGUIEX_EXPORT SerStream& operator>>(SerStream& stream, QImage& image)
{
   QDataStream s(stream.readData());
   s.setVersion(QDataStream::Qt_5_5);
   s >> image;
   return stream;
}

QTGUIEX_EXPORT bool operator<(const QColor& lhs, const QColor& rhs)
{
   if (lhs.hue() < rhs.hue())
      return true;
   if (rhs.hue() < lhs.hue())
      return false;
   if (lhs.saturation() < rhs.saturation())
      return true;
   if (rhs.saturation() < lhs.saturation())
      return false;
   return lhs.value() < rhs.value();
}

QTGUIEX_EXPORT SerStream& operator<<(SerStream& stream, const QColor& color)
{
   stream << color.red();
   stream << color.green();
   stream << color.blue();
   stream << color.alpha();
   return stream;
}

QTGUIEX_EXPORT SerStream& operator>>(SerStream& stream, QColor& color)
{
   int red, green, blue, alpha;

   stream >> red;
   stream >> green;
   stream >> blue;
   stream >> alpha;
   color.setRgb(red, green, blue, alpha);
   return stream;
}

BEGIN_VARIANT_REGISTRATION
Variant::registerType<QFont>();
Variant::registerType<QIcon>();
Variant::registerType<QPixmap>();
Variant::registerType<QImage>();
Variant::registerType<QColor>();

Variant::registerConversion<QByteArray, QIcon>();
Variant::registerConversion<QIcon, QByteArray>();
Variant::registerQVariantConversion<QFont>();
Variant::registerQVariantConversion<QIcon>();
Variant::registerQVariantConversion<QPixmap>();
Variant::registerQVariantConversion<QImage>();
Variant::registerQVariantConversion<QColor>();
Variant::registerConversion<QByteArray, QImage>();
Variant::registerConversion<QImage, QByteArray>();
Variant::registerConversion<QVariant, Qt::GlobalColor>();
END_VARIANT_REGISTRATION
