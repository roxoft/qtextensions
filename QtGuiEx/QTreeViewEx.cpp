#include "stdafx.h"
#include "QTreeViewEx.h"

#include <QEvent>
#include <QKeyEvent>
#include <QMouseEvent>
#include <QLabel>
#include <QHeaderView>
#include <QApplication>
#include <QLocaleEx.h>
#include "QHighlightItemDelegate.h"
#include <QPainter>
#include <QScrollBar>
#include <Variant.h>
#include <SerStream.h>
#include <QToolTip>
#include <QMenu>
#include <HtItemModel.h>
#include <QMessageBox>
#include <QtGuiVariantTypes.h>

static void nextRow(QModelIndex& index)
{
   if (!index.isValid())
      return;

   const auto model = index.model();
   const int column = index.column();

   index = index.sibling(index.row(), 0);

   for (;;)
   {
      if (model->hasChildren(index))
      {
         index = model->index(0, 0, index);
      }
      else
      {
         for (;;)
         {
            int row = index.row();

            row++;
            if (row < model->rowCount(index.parent()))
            {
               index = index.sibling(row, 0);
               break;
            }
            if (!index.parent().isValid())
            {
               // warp around
               index = model->index(0, 0);
               break;
            }
            index = index.parent();
         }
      }

      const auto colIndex = index.sibling(index.row(), column);

      if (colIndex.isValid())
      {
         index = colIndex;
         break;
      }
   }
}

static QModelIndex findRow(const QModelIndex &start, int role, const QVariant &value, int hits = 1, Qt::MatchFlags flags = Qt::MatchFlags(Qt::MatchStartsWith|Qt::MatchWrap))
{
   Q_UNUSED(hits);

   if (role != Qt::DisplayRole)
      return QModelIndex();

   uint matchType = flags & 0x0F;
   Qt::CaseSensitivity cs = flags & Qt::MatchCaseSensitive ? Qt::CaseSensitive : Qt::CaseInsensitive;
   //bool recurse = flags & Qt::MatchRecursive;
   //bool wrap = flags & Qt::MatchWrap;
   //bool allHits = (hits == -1);
   QString text; // only convert to a string if it is needed
   QModelIndex index = start;

   while (index.isValid())
   {
      // Compare
      QVariant v(index.data());

      if (matchType == Qt::MatchExactly)
      {
         // QVariant based matching
         if (value == v)
            break;
      }
      else
      {
         // QString based matching
         if (text.isEmpty()) // lazy conversion
            text = toQString(value);

         QString t = toQString(v);

#if QT_VERSION < QT_VERSION_CHECK(5,15,0)
         if (matchType == Qt::MatchRegExp)
#else
         if (matchType == Qt::MatchRegularExpression)
#endif
         {
#if QT_VERSION < QT_VERSION_CHECK(5, 15, 0)
            if (QRegExp(text, cs).exactMatch(t))
#else
            if (QRegularExpression(text, flags & Qt::MatchCaseSensitive ? QRegularExpression::NoPatternOption : QRegularExpression::CaseInsensitiveOption).
                match(t).hasMatch())
#endif
               break;
         }
         else if (matchType == Qt::MatchWildcard)
         {
#if QT_VERSION < QT_VERSION_CHECK(5,15,0)
            if (QRegExp(text, cs, QRegExp::Wildcard).exactMatch(t))
#else
            if (QRegularExpression(QRegularExpression::wildcardToRegularExpression(text),
                                   flags & Qt::MatchCaseSensitive ? QRegularExpression::NoPatternOption : QRegularExpression::CaseInsensitiveOption).match(t).
	            hasMatch())
#endif
               break;
         }
         else if (matchType == Qt::MatchStartsWith)
         {
            if (t.startsWith(text, cs))
               break;
         }
         else if (matchType == Qt::MatchEndsWith)
         {
            if (t.endsWith(text, cs))
               break;
         }
         else if (matchType == Qt::MatchFixedString)
         {
            if (t.compare(text, cs) == 0)
               break;
         }
         else // if (matchType == Qt::MatchContains)
         {
            if (t.contains(text, cs))
               break;
         }
      }

      nextRow(index);

      if (index == start)
         index = QModelIndex();
   }

   return index;
}

QTreeViewEx::QTreeViewEx(QWidget *parent) : QTreeView(parent)
{
   auto styledItemDelegate = new QHighlightItemDelegate(this);

   styledItemDelegate->setHeightForWidth(true);

   QTreeView::setItemDelegate(styledItemDelegate);

   // Do not connect to the header because the header may change in derived classes!
}

int QTreeViewEx::minimumRowHeight() const
{
   const auto styledItemDelegate = dynamic_cast<QStyledItemDelegateEx*>(QTreeView::itemDelegate());

   if (styledItemDelegate)
      return styledItemDelegate->minimumHeight();

   return 0;
}

void QTreeViewEx::setMinimumRowHeight(int minHeight)
{
   auto styledItemDelegate = dynamic_cast<QStyledItemDelegateEx*>(QTreeView::itemDelegate());

   if (styledItemDelegate)
      styledItemDelegate->setMinimumHeight(qMax(minHeight, 0));
}

Qt::DropActions QTreeViewEx::allowedDragActions() const
{
   return _allowedDragActions;
}

void QTreeViewEx::setAllowedDragActions(Qt::DropActions dragActions)
{
   _allowedDragActions = dragActions;
   setDragEnabled(dragActions);
}

Qt::DropActions QTreeViewEx::allowedDropActions() const
{
   return _allowedDropActions;
}

void QTreeViewEx::setAllowedDropActions(Qt::DropActions dropActions)
{
   _allowedDropActions = dropActions;
   setAcceptDrops(dropActions);
}

bool QTreeViewEx::isColumnResizeEnabled() const
{
   return _resizeColumns;
}

void QTreeViewEx::enableColumnResize(bool enable)
{
   _resizeColumns = enable;
   setMouseTracking(enable);
   if (enable)
   {
      if (header())
         header()->installEventFilter(this);
      if (verticalScrollBar())
         verticalScrollBar()->installEventFilter(this);
      if (horizontalScrollBar())
         horizontalScrollBar()->installEventFilter(this);
   }
   else
   {
      if (header())
         header()->removeEventFilter(this);
      if (verticalScrollBar())
         verticalScrollBar()->removeEventFilter(this);
      if (horizontalScrollBar())
         horizontalScrollBar()->removeEventFilter(this);
      if (_logicalCursorColumn != -1)
      {
         _logicalCursorColumn = -1;
         unsetCursor();
      }
   }
}

void QTreeViewEx::setCheckOnMouseMove(bool checkOnMouseMove)
{
   if (_checkOnMouseMove == checkOnMouseMove)
      return;

   _checkOnMouseMove = checkOnMouseMove;

   dynamic_cast<QStyledItemDelegateEx *>(QTreeView::itemDelegate())->setCheckOnMouseDown(_checkOnMouseMove);
}

void QTreeViewEx::showNoData(bool show)
{
   if (show)
   {
      if (_noDataWidget == nullptr)
      {
         QPalette pal;
         pal.setColor(QPalette::WindowText, Qt::gray);

         _noDataWidget = new QLabel(tr("No data"), this);

         _noDataWidget->setAlignment(Qt::AlignCenter);
         _noDataWidget->setPalette(pal);
         _noDataWidget->setFont(QFont(QLatin1String("Arial"), 32, QFont::Bold));
         _noDataWidget->setGeometry(0, header()->height(), width(), height() - header()->height());
         _noDataWidget->show();
      }
   }
   else if (_noDataWidget)
   {
      delete _noDataWidget;
      _noDataWidget = NULL;
   }
}

bool QTreeViewEx::isContentAutoAlign() const
{
   return dynamic_cast<QStyledItemDelegateEx *>(QTreeView::itemDelegate())->isContentAutoAlign();
}

void QTreeViewEx::setIsContentAutoAlign(bool autoAlign)
{
   dynamic_cast<QStyledItemDelegateEx *>(QTreeView::itemDelegate())->setIsContentAutoAlign(autoAlign);
}

void QTreeViewEx::setEditDelegate(QEditItemDelegate* editDelegate)
{
   dynamic_cast<QStyledItemDelegateEx *>(QTreeView::itemDelegate())->setEditDelegate(-1, -1, editDelegate);
}

QEditItemDelegate* QTreeViewEx::editDelegate() const
{
   return dynamic_cast<QStyledItemDelegateEx *>(QTreeView::itemDelegate())->editDelegate(-1, -1);
}

void QTreeViewEx::setEditDelegateForColumn(int column, QEditItemDelegate* editDelegate)
{
   dynamic_cast<QStyledItemDelegateEx *>(QTreeView::itemDelegate())->setEditDelegate(-1, column, editDelegate);
}

QEditItemDelegate* QTreeViewEx::editDelegateForColumn(int column) const
{
   return dynamic_cast<QStyledItemDelegateEx *>(QTreeView::itemDelegate())->editDelegate(-1, column);
}

void QTreeViewEx::setEditDelegateForRow(int row, QEditItemDelegate* editDelegate)
{
   dynamic_cast<QStyledItemDelegateEx *>(QTreeView::itemDelegate())->setEditDelegate(row, -1, editDelegate);
}

QEditItemDelegate* QTreeViewEx::editDelegateForRow(int row) const
{
   return dynamic_cast<QStyledItemDelegateEx *>(QTreeView::itemDelegate())->editDelegate(row, -1);
}

void QTreeViewEx::setPaintDelegate(QPaintItemDelegate* paintDelegate)
{
   dynamic_cast<QStyledItemDelegateEx *>(QTreeView::itemDelegate())->setPaintDelegate(-1, -1, paintDelegate);
}

QPaintItemDelegate* QTreeViewEx::paintDelegate() const
{
   return dynamic_cast<QStyledItemDelegateEx *>(QTreeView::itemDelegate())->paintDelegate(-1, -1);
}

void QTreeViewEx::setPaintDelegateForColumn(int column, QPaintItemDelegate* paintDelegate)
{
   dynamic_cast<QStyledItemDelegateEx *>(QTreeView::itemDelegate())->setPaintDelegate(-1, column, paintDelegate);
}

QPaintItemDelegate* QTreeViewEx::paintDelegateForColumn(int column) const
{
   return dynamic_cast<QStyledItemDelegateEx *>(QTreeView::itemDelegate())->paintDelegate(-1, column);
}

void QTreeViewEx::setPaintDelegateForRow(int row, QPaintItemDelegate* paintDelegate)
{
   dynamic_cast<QStyledItemDelegateEx *>(QTreeView::itemDelegate())->setPaintDelegate(row, -1, paintDelegate);
}

QPaintItemDelegate* QTreeViewEx::paintDelegateForRow(int row) const
{
   return dynamic_cast<QStyledItemDelegateEx *>(QTreeView::itemDelegate())->paintDelegate(row, -1);
}

void QTreeViewEx::setModel(QAbstractItemModel *model)
{
   _concealedTextIndexList.clear();
   QTreeView::setModel(model);
}

bool QTreeViewEx::eventFilter(QObject *watched, QEvent *e)
{
   switch (e->type())
   {
   case QEvent::HoverMove:
      if (_logicalCursorColumn != -1)
      {
         _logicalCursorColumn = -1;
         unsetCursor();
      }
      break;
   }

   return QTreeView::eventFilter(watched, e);
}

void QTreeViewEx::keyboardSearch(const QString &search)
{
   QAbstractItemModel *m = model();

   if (!m->rowCount() || !m->columnCount())
      return;

   QModelIndex start(currentIndex());

   if (!start.isValid())
      start = m->index(0, 0);

   if (!start.isValid())
      return;

   qApp->setOverrideCursor(Qt::WaitCursor);

   QTime now(QTime::currentTime());
   bool  skipRow = false;

   if (search.isEmpty() || (_keyboardInputTime.msecsTo(now) > QApplication::keyboardInputInterval()))
   {
      _keyboardInput = search;
      skipRow = true;
   }
   else
      _keyboardInput += search;

   _keyboardInputTime = now;

   // special case for searches with same key like 'aaaaa'
   QString searchString;

   if (_keyboardInput.length() > 1 && _keyboardInput.count(_keyboardInput.at(_keyboardInput.length() - 1)) == _keyboardInput.length())
   {
      // Same key
      skipRow = true;
      searchString = QString(_keyboardInput.at(0));
   }
   else
      searchString = _keyboardInput;

   // skip if we are searching for the same key or a new search started
   if (skipRow)
      nextRow(start);

   QModelIndex index;

   index = findRow(start, Qt::DisplayRole, searchString);

   if (index.isValid())
   {
      QItemSelectionModel::SelectionFlags flags =
         (selectionMode() == SingleSelection ?
            QItemSelectionModel::SelectionFlags(QItemSelectionModel::ClearAndSelect) :
            QItemSelectionModel::SelectionFlags(QItemSelectionModel::NoUpdate)
         );
      selectionModel()->setCurrentIndex(index, flags);
   }

   qApp->restoreOverrideCursor();
}

void QTreeViewEx::scrollTo(const QModelIndex &index, QAbstractItemView::ScrollHint hint)
{
   auto vScrollBarPosition = verticalScrollBar()->value();
   auto preSelectedItemRect = visualRect(index);

   QTreeView::scrollTo(index, hint);

   if (!_minimizeScrollRange && verticalScrollMode() == ScrollPerPixel)
   {
      QSize viewportSize = viewport()->size();
      if (!viewportSize.isValid())
          viewportSize = QSize(0, 0);

      verticalScrollBar()->setMaximum(verticalScrollBar()->maximum() + viewportSize.height() - 20);

      auto selectedItemRect = visualRect(index);

      switch (hint)
      {
      case EnsureVisible:
         if (preSelectedItemRect.top() < 0)
         {
            vScrollBarPosition = qMax(verticalScrollBar()->value() + selectedItemRect.top() - 10, 0);
         }
         else if (preSelectedItemRect.bottom() > viewportSize.height())
         {
            vScrollBarPosition = qMax(verticalScrollBar()->value() + selectedItemRect.bottom() - viewportSize.height() + 10, 0);
         }
         break;
      case PositionAtTop:
         vScrollBarPosition = qMax(verticalScrollBar()->value() + selectedItemRect.top(), 0);
         break;
      case PositionAtBottom:
         vScrollBarPosition = qMax(verticalScrollBar()->value() + selectedItemRect.bottom() - viewportSize.height(), 0);
         break;
      case PositionAtCenter:
         vScrollBarPosition = qMax(verticalScrollBar()->value() + (selectedItemRect.top() + selectedItemRect.bottom() - viewportSize.height()) / 2, 0);
         break;
      }

      verticalScrollBar()->setValue(vScrollBarPosition);
      viewport()->update();
   }
}

void QTreeViewEx::dataChanged(const QModelIndex &topLeft, const QModelIndex &bottomRight, const QVector<int> &roles)
{
   auto vScrollBarPosition = verticalScrollBar()->value();

   QTreeView::dataChanged(topLeft, bottomRight, roles);

   if (!_minimizeScrollRange && verticalScrollMode() == QAbstractItemView::ScrollPerPixel)
   {
      QSize viewportSize = viewport()->size();
      if (!viewportSize.isValid())
          viewportSize = QSize(0, 0);

      verticalScrollBar()->setMaximum(verticalScrollBar()->maximum() + viewportSize.height() - 20);
      verticalScrollBar()->setValue(vScrollBarPosition);
      viewport()->update();
   }
}

QByteArray QTreeViewEx::saveState() const
{
   SerStream stream;

   stream << (qint32)1; // Version number

   auto h = header();

   stream << '{';
   if (h)
   {
      // Save header state

      stream << h->isSortIndicatorShown();
      stream << (qint32)h->sortIndicatorSection();
      stream << (qint32)h->sortIndicatorOrder();
      for (auto column = 0; column < h->count(); ++column)
      {
         stream << '{';
         stream << (qint32)h->visualIndex(column);
         stream << (qint32)h->sectionSize(column);
         stream << h->isSectionHidden(column);
         stream << '}';
      }
   }
   stream << '}';

   // Save expanded state

   saveExpandedState(stream);

   // Save current index

   auto index = currentIndex();
   QList<int> currentPath;

   while (index.isValid())
   {
      currentPath.prepend(index.row());
      index = index.parent();
   }

   stream << '{';
   for (auto&& row : currentPath)
   {
      stream << row;
   }
   stream << '}';

   // Replace '~' by ',' to have nicer stream for Variants

   auto state = stream.internalBuffer();

   state.replace('~', ','); // There are only numbers!

   return state;
}

void QTreeViewEx::restoreState(const QByteArray& data)
{
   auto state = data;

   state.replace(',', '~');

   SerStream stream(state);

   qint32 version;
   stream >> version;
   if (version != 1)
      return;

   reset();

   char c;

   // Load column state
   auto h = header();

   stream.readRaw(&c, 1LL);
   if (c == '{')
   {
      stream.peekRaw(&c, 1LL);
      if (c == '}')
      {
         stream.readRaw(&c, 1LL);
      }
      else
      {
         bool isSortIndicatorShown;
         qint32 sortIndicatorSection;
         qint32 sortIndicatorOrder;

         stream >> isSortIndicatorShown >> sortIndicatorSection >> sortIndicatorOrder;

         if (h)
         {
            if (sortIndicatorSection < h->count())
               h->setSortIndicator(sortIndicatorSection, (Qt::SortOrder)sortIndicatorOrder);
            h->setSortIndicatorShown(isSortIndicatorShown);
         }
      }

      QVector<int> visualIndexList(h ? h->count() : 0);

      for (auto i = 0; i < visualIndexList.count(); ++i)
      {
         visualIndexList[i] = i;
      }

      auto column = 0;
      while (!stream.atEnd())
      {
         stream.readRaw(&c, 1LL);
         if (c == '}')
            break;
         if (c != '{')
         {
            stream.setError(__FUNCTION__, "Invalid tree columns state");
            break;
         }

         qint32 visualIndex, sectionSize;
         bool isSectionHidden;

         stream >> visualIndex >> sectionSize >> isSectionHidden;

         if (!stream.isValid())
         {
            stream.setError(__FUNCTION__, "Invalid tree columns state");
            break;
         }

         if (h && column < h->count())
         {
            h->resizeSection(column, sectionSize);
            if (h->sectionsMovable() && visualIndex < visualIndexList.count())
               visualIndexList[visualIndex] = column;
            h->setSectionHidden(column, isSectionHidden);
         }

         column++;

         // Skip closing brace
         stream.readRaw(&c, 1LL);
         if (c != '}')
         {
            stream.setError(__FUNCTION__, "Invalid tree columns state");
            break;
         }
      }

      if (h && h->sectionsMovable())
      {
         for (auto i = 0; i + 1 < visualIndexList.count(); ++i)
         {
            h->moveSection(h->visualIndex(visualIndexList.at(i)), i);
         }
      }
   }

   // Load expanded state

   stream.readRaw(&c, 1LL);
   if (c == '{')
      restoreExpandedState(stream);

   // Restore current index

   stream.readRaw(&c, 1LL);
   if (c == '{')
   {
      QModelIndex index;

      while (!stream.atEnd())
      {
         stream.peekRaw(&c, 1LL);
         if (c == '}')
         {
            stream.readRaw(&c, 1LL);
            break;
         }

         qint32 row;
         stream >> row;
         if (!stream.isValid())
         {
            stream.setError(__FUNCTION__, "Invalid current row");
            break;
         }

         index = model()->index(row, 0, index);
         if (!index.isValid())
            break;
      }

      setCurrentIndex(index);
   }
}

void QTreeViewEx::saveExpandedState(SerStream& stream, const QModelIndex& parent) const
{
   stream << '{';
   for (auto row = 0; row < model()->rowCount(parent); ++row)
   {
      auto index = model()->index(row, 0, parent);

      if (isExpanded(index))
      {
         stream << (qint32)row;
         saveExpandedState(stream, index);
      }
   }
   stream << '}';
}

void QTreeViewEx::restoreExpandedState(SerStream& stream, const QModelIndex& parent)
{
   char c;

   while (!stream.atEnd())
   {
      stream.peekRaw(&c, 1LL);
      if (c == '}')
      {
         stream.readRaw(&c, 1LL);
         break;
      }

      qint32 row;
      stream >> row;
      if (!stream.isValid())
      {
         stream.setError(__FUNCTION__, "Invalid tree expand state");
         break;
      }

      const auto index = model()->index(row, 0, parent);

      expand(index);

      // Skip opening brace
      stream.readRaw(&c, 1LL);
      if (c != '{')
      {
         stream.setError(__FUNCTION__, "Invalid tree expand state");
         break;
      }

      restoreExpandedState(stream, index);
   }
}

void QTreeViewEx::keyPressEvent(QKeyEvent *e)
{
   e->setAccepted(false);

   QModelIndex index = currentIndex();

   if (index.isValid())
   {
      int col = index.column();

      switch (e->key())
      {
      case Qt::Key_Right:
         while (++col < model()->columnCount() && this->isColumnHidden(col));
         if (col < model()->columnCount())
            setCurrentIndex(model()->sibling(index.row(), col, index));
         e->accept();
         return;
      case Qt::Key_Left:
         while (--col >= 0 && this->isColumnHidden(col));
         if (col >= 0)
            setCurrentIndex(model()->sibling(index.row(), col, index));
         e->accept();
         return;
      case Qt::Key_Up:
      case Qt::Key_Down:
      case Qt::Key_PageUp:
      case Qt::Key_PageDown:
      case Qt::Key_Home:
      case Qt::Key_End:
         QTreeView::keyPressEvent(e);
         if (e->isAccepted())
            setCurrentIndex(model()->sibling(currentIndex().row(), col, currentIndex())); // currentIndex may have changed
         return;
      case Qt::Key_Minus:
         if (itemsExpandable())
            collapse(index.sibling(index.row(), 0));
         return;
      case Qt::Key_Plus:
         if (itemsExpandable())
            expand(index.sibling(index.row(), 0));
         return;
      case Qt::Key_Asterisk:
         if (itemsExpandable())
         {
            QStack<QModelIndex> parents;

            parents.push(index.sibling(index.row(), 0));

            while (!parents.isEmpty())
            {
               const QModelIndex parent = parents.pop();

               if (parent.isValid())
               {
                  expand(parent);

                  for (int row = 0; row < model()->rowCount(parent); ++row)
                  {
                     parents.push(model()->index(row, 0, parent));
                  }
               }
            }
         }
         return;
      }
   }

   emit keyPressed(e);

   if (!e->isAccepted())
      QTreeView::keyPressEvent(e);
}

bool QTreeViewEx::viewportEvent(QEvent* e)
{
   if (e->type() == QEvent::ToolTip)
   {
      QHelpEvent *he = static_cast<QHelpEvent*>(e);
      const QModelIndex index = indexAt(he->pos());
      auto rect = visualRect(index);

      rect.setLeft(rect.right() - 14);
      if (rect.contains(he->pos()))
      {
         if (std::binary_search(_concealedTextIndexList.begin(), _concealedTextIndexList.end(), index))
         {
            auto concealedText = Variant(index.data()).toString();

            if (!concealedText.isEmpty())
            {
               QToolTip::showText(he->globalPos(), concealedText, this);
               return true;
            }
         }
      }
   }

   return QTreeView::viewportEvent(e);
}

void QTreeViewEx::resizeEvent(QResizeEvent *e)
{
   QTreeView::resizeEvent(e);
   if (_noDataWidget)
   {
      _noDataWidget->setGeometry(0, header()->height(), e->size().width(), e->size().height() - header()->height());
   }
}

void QTreeViewEx::paintEvent(QPaintEvent* e)
{
   static_cast<void>(std::remove_if(_concealedTextIndexList.begin(), _concealedTextIndexList.end(), [](const QPersistentModelIndex& index) { return !index.isValid(); }));

   QTreeView::paintEvent(e);

   if (_showColumnSeparator)
   {
      QPainter painter(viewport());
      QPoint offset = dirtyRegionOffset();

      painter.setPen(palette().color(QPalette::Dark));

      int max = header()->length() - header()->offset() - 1;
      const QRegion region = e->region().translated(offset);

      for (auto dirtyArea : region)
      {
         dirtyArea.setRight(qMin(dirtyArea.right(), max));

         int leftColumn = header()->visualIndexAt(dirtyArea.left());
         int rightColumn = header()->visualIndexAt(dirtyArea.right());

         if (leftColumn < 1)
            leftColumn = 1;
         if (rightColumn == -1)
            rightColumn = header()->count() - 1;

         for (int i = leftColumn; i <= rightColumn; ++i)
         {
            int col = header()->logicalIndex(i);

            if (!header()->isSectionHidden(col))
            {
               int x = header()->sectionPosition(col);

               x += offset.x();
               x -= horizontalScrollBar()->value();

               painter.drawLine(x, dirtyArea.top(), x, dirtyArea.bottom());
            }
         }
      }
   }

   if (_dropPosition != DropInvalid && showDropIndicator())
   {
      auto dropPosition = _dropPosition;
      auto dropItemRect = _dropItemRect;

      if (dropItemRect.isNull())
      {
         // Last visible item
         auto index = model()->index(model()->rowCount() - 1, 0);

         while (index.row() > 0 && isRowHidden(index.row(), rootIndex()))
         {
            index = model()->index(index.row() - 1, 0);
         }

         while (index.isValid() && isExpanded(index))
         {
            index = model()->index(model()->rowCount(index) - 1, 0, index);

            while (index.row() > 0 && isRowHidden(index.row(), index.parent()))
            {
               index = model()->index(index.row() - 1, 0, index.parent());
            }
         }

         if (index.isValid())
         {
            dropItemRect = visualRect(index);
            dropItemRect.setLeft(viewport()->rect().left());
            dropItemRect.setRight(viewport()->rect().right());
            dropPosition = DropBelowItem;
         }
      }

      if (dropItemRect.isNull())
         return;

      QPainter painter(viewport());

      if (_dropIndicatorStyle == DropIndicatorStyle::Standard)
      {
         QStyleOption opt;

         opt.initFrom(this);

         switch (dropPosition)
         {
         case DropInvalid:
            break;
         case DropAboveItem:
            opt.rect = QRect(dropItemRect.left(), dropItemRect.top(), dropItemRect.width(), 0);
            break;
         case DropBelowItem:
            opt.rect = QRect(dropItemRect.left(), dropItemRect.bottom(), dropItemRect.width(), 0);
            break;
         case DropReplaceItem:
            opt.rect = dropItemRect;
            break;
         case DropIntoItem:
            opt.rect = QRect(dropItemRect.left() + indentation(), dropItemRect.bottom(), dropItemRect.width(), 0);
            break;
         }

         if (!opt.rect.isNull())
            style()->drawPrimitive(QStyle::PE_IndicatorItemViewItemDrop, &opt, &painter, this);
      }
      else
      {
         switch (dropPosition)
         {
         case DropInvalid:
            break;
         case DropAboveItem:
            painter.setPen(QPen(QColor(0, 255, 0, 128), 6));
            painter.drawLine(dropItemRect.topLeft(), dropItemRect.topRight());
            break;
         case DropBelowItem:
            painter.setPen(QPen(QColor(0, 255, 0, 128), 6));
            painter.drawLine(dropItemRect.bottomLeft(), dropItemRect.bottomRight());
            break;
         case DropReplaceItem:
            painter.fillRect(dropItemRect, QColor(255, 128, 0, 128));
            break;
         case DropIntoItem:
            painter.setPen(QPen(QColor(0, 255, 0, 128), 6, Qt::DotLine));
            painter.drawLine(dropItemRect.bottomLeft() + QPoint(indentation(), 0), dropItemRect.bottomRight());
            break;
         }
      }
   }
}

void QTreeViewEx::drawRow(QPainter *painter, const QStyleOptionViewItem &options, const QModelIndex &index) const
{
   QStyleOptionViewItem opt = options;

   if (const auto htModel = dynamic_cast<const HtItemModel*>(model()))
   {
      auto node = htModel->nodeFromIndex(index);
      auto variantColor = node->effectiveDefaultAttribute(-1, Qt::BackgroundRole);

      if (!variantColor.isNull())
      {
         opt.backgroundBrush = variantColor.to<QColor>();
      }
   }

   QTreeView::drawRow(painter, opt, index);
}

void QTreeViewEx::mousePressEvent(QMouseEvent* e)
{
   _dragStartPosition = e->pos();

   if (_logicalCursorColumn != -1)
   {
      if (_resizeRightColumn)
#if QT_VERSION >= QT_VERSION_CHECK(6,0,0)
         _sectionSizeOffset = qRound(e->position().x()) + header()->sectionSize(_logicalCursorColumn);
      else
         _sectionSizeOffset = qRound(e->position().x()) - header()->sectionSize(_logicalCursorColumn);
#else
         _sectionSizeOffset = e->x() + header()->sectionSize(_logicalCursorColumn);
      else
         _sectionSizeOffset = e->x() - header()->sectionSize(_logicalCursorColumn);
#endif
      e->accept();
      return;
   }

   QVariant prevCheckState;

   if (_checkOnMouseMove)
   {
      if (e->button() == Qt::LeftButton)
      {
         _mousePressedIndex = indexAt(e->pos());

         if (_mousePressedIndex.isValid())
         {
            prevCheckState = _mousePressedIndex.data(Qt::CheckStateRole);
         }
      }
   }
   else
   {
      _mousePressedIndex = currentIndex();
   }

   QTreeView::mousePressEvent(e);

   if (_checkOnMouseMove)
   {
      if (!prevCheckState.isValid() || prevCheckState == _mousePressedIndex.data(Qt::CheckStateRole))
      {
         _mousePressedIndex = QModelIndex();
      }
   }
}

void QTreeViewEx::mouseMoveEvent(QMouseEvent* e)
{
   if (resizeColumn(e))
      return;

   if (_allowedDragActions == Qt::IgnoreAction || state() != DraggingState)
   {
      QTreeView::mouseMoveEvent(e); // Let Qt determine when the drag starts (see QAbstractItemView::mouseMoveEvent())

      if (_checkOnMouseMove && _mousePressedIndex.isValid())
      {
         const auto index = indexAt(e->pos());

         if (index.isValid() && index.column() == _mousePressedIndex.column() && index.parent() == _mousePressedIndex.parent())
         {
            const auto checkState = _mousePressedIndex.data(Qt::CheckStateRole);

            if (checkState.isValid())
            {
               const auto origVisualIndex = _mousePressedIndex.row();
               const auto visualIndex = index.row();

               for (auto i = origVisualIndex + 1; i <= visualIndex; ++i)
               {
                  if (!isRowHidden(i, index.parent()))
                     model()->setData(model()->index(i, index.column(), index.parent()), checkState, Qt::CheckStateRole);
               }
               for (auto i = visualIndex; i < origVisualIndex; ++i)
               {
                  if (!isRowHidden(i, index.parent()))
                     model()->setData(model()->index(i, index.column(), index.parent()), checkState, Qt::CheckStateRole);
               }
            }
         }
      }

      return;
   }

   // Drag & Drop

   const auto startIndex = indexAt(_dragStartPosition);

   if (!startIndex.isValid())
   {
      setState(NoState);
      return;
   }

   auto m = dynamic_cast<HtItemModel*>(model());

   if (m == nullptr)
   {
      setState(NoState);
      return;
   }

   if ((e->pos() - _dragStartPosition).manhattanLength() < QApplication::startDragDistance())
      return;

   QList<const HtNode*> nodeList;

   for (auto&& index : selectedIndexes())
   {
      if (index.column() == 0 && (m->flags(index) & Qt::ItemIsDragEnabled))
      {
         nodeList.append(m->nodeFromIndex(index));
      }
   }

   // Make sure the selected nodes have no parent/child relations
   for (auto i = nodeList.count(); i--; )
   {
      auto parent = nodeList.at(i)->parent();

      while (parent)
      {
         if (nodeList.contains(parent))
         {
            nodeList.removeAt(i);
            break;
         }
         parent = parent->parent();
      }
   }

   if (nodeList.isEmpty())
   {
      if ((m->flags(startIndex) & Qt::ItemIsSelectable) == 0 || (m->flags(startIndex) & Qt::ItemIsDragEnabled) == 0)
      {
         setState(NoState);
         return;
      }

      nodeList.append(m->nodeFromIndex(startIndex));
   }

   auto drag = new QDrag(this);

   setDragPicture(drag, m, nodeList);

   const auto mimeData = new HtMimeData(nodeList, m->nodeFromIndex(currentIndex()));

   drag->setMimeData(mimeData);

   if (drag->exec(_allowedDragActions, Qt::IgnoreAction) == Qt::MoveAction && (_allowedDragActions & Qt::MoveAction))
   {
      auto target = drag->target();
      QAbstractItemView* targetView = nullptr;

      while (target)
      {
         targetView = dynamic_cast<QAbstractItemView*>(target);
         if (targetView)
            break;
         target = target->parent();
      }

      if (targetView)
      {
         for (auto&& node : nodeList)
            node->parent()->removeChild(node->parent()->indexOf(node));
      }
   }

   setState(NoState);

   // TODO: stop auto scroll
}

void QTreeViewEx::mouseReleaseEvent(QMouseEvent* e)
{
   if (e->button() == Qt::LeftButton && _logicalCursorColumn != -1)
   {
      e->accept();
      // If the column width has changed the items preferred heights may have changed too
      if (model() && wordWrap())
      {
         auto firstVisibleIndex = indexAt(rect().topLeft());

         if (firstVisibleIndex.isValid())
            emit QTreeView::itemDelegate()->sizeHintChanged(firstVisibleIndex); // This updates the row heights of the complete tree!
      }
      return;
   }

   QVariant prevCheckState;

   if (!_checkOnMouseMove && _mousePressedIndex.isValid() && e->button() == Qt::LeftButton && e->modifiers() == Qt::ShiftModifier)
   {
      const auto index = indexAt(e->pos());

      if (index.isValid() && index.parent() == _mousePressedIndex.parent())
      {
         prevCheckState = index.data(Qt::CheckStateRole);
      }
   }

   QTreeView::mouseReleaseEvent(e);

   if (prevCheckState.isValid())
   {
      const auto index = indexAt(e->pos());

      if (index.isValid())
      {
         const auto checkState = index.data(Qt::CheckStateRole);

         if (checkState.isValid() && checkState != prevCheckState)
         {
            const auto origVisualIndex = _mousePressedIndex.row();
            const auto visualIndex = index.row();

            for (auto i = origVisualIndex; i < visualIndex; ++i)
            {
               if (!isRowHidden(i, index.parent()))
                  model()->setData(model()->index(i, index.column(), index.parent()), checkState, Qt::CheckStateRole);
            }
            for (auto i = visualIndex + 1; i <= origVisualIndex; ++i)
            {
               if (!isRowHidden(i, index.parent()))
                  model()->setData(model()->index(i, index.column(), index.parent()), checkState, Qt::CheckStateRole);
            }
         }
      }
   }

   _mousePressedIndex = QModelIndex();

   if (_doubleClicked)
   {
      _doubleClicked = false;
      emit doubleClickReleased(currentIndex());
   }
}

void QTreeViewEx::mouseDoubleClickEvent(QMouseEvent* e)
{
   QTreeView::mouseDoubleClickEvent(e);
   _doubleClicked = true;
}

void QTreeViewEx::dragEnterEvent(QDragEnterEvent* event)
{
   if (_allowedDropActions == Qt::IgnoreAction)
   {
      QTreeView::dragEnterEvent(event);
      return;
   }

   setEventDropAction(event);
   if (event->isAccepted())
      setState(DraggingState);
}

void QTreeViewEx::dragMoveEvent(QDragMoveEvent* event)
{
   if (_allowedDropActions == Qt::IgnoreAction)
   {
      QTreeView::dragMoveEvent(event);
      return;
   }

   setEventDropAction(event);

   if (!event->isAccepted())
      return;

   auto m = dynamic_cast<HtItemModel*>(model());

   if (m == nullptr)
      return;

#if QT_VERSION >= QT_VERSION_CHECK(6,0,0)
   const auto index = indexAt(event->position().toPoint());
#else
   const auto index = indexAt(event->pos());
#endif

   const auto node = m->nodeFromIndex(index);

   if (node == nullptr)
      return;

   event->ignore(); // Ignore by default

   _dropPosition = DropInvalid;
   _dropItemRect = QRect();

   auto dropAction = event->dropAction();

   const auto mimeData = dynamic_cast<const HtMimeData*>(event->mimeData());

   if (mimeData)
   {
#if QT_VERSION >= QT_VERSION_CHECK(6,0,0)
      setDropPosition(mimeData->nodeList(), m, node, event->position().toPoint(), dropAction);
#else
      setDropPosition(mimeData->nodeList(), m, node, event->pos(), dropAction);
#endif

      emit canDropNodes(mimeData->nodeList(), node, dropAction, _dropPosition);

      event->setDropAction(dropAction);
      if (_dropPosition != DropInvalid)
         event->accept();
   }

   // TODO: start auto scroll
#if QT_VERSION >= QT_VERSION_CHECK(6,3,0)
   QHoverEvent hoverEvent(QEvent::HoverMove, event->position(), event->position(), event->position());
#elif QT_VERSION >= QT_VERSION_CHECK(6,0,0)
   QHoverEvent hoverEvent(QEvent::HoverMove, event->position(), event->position());
#else
   QHoverEvent hoverEvent(QEvent::HoverMove, event->posF(), event->posF());
#endif

   viewportEvent(&hoverEvent);

   viewport()->update();
}

void QTreeViewEx::dragLeaveEvent(QDragLeaveEvent* event)
{
   if (_allowedDropActions == Qt::IgnoreAction)
   {
      QTreeView::dragLeaveEvent(event);
      return;
   }

   _dropPosition = DropInvalid;
   _dropItemRect = QRect();

   // TODO: stop auto scroll

   setState(NoState);

#if QT_VERSION >= QT_VERSION_CHECK(6,3,0)
   QHoverEvent hoverEvent(QEvent::HoverLeave, QPointF(-1, -1), QPointF(-1, -1), QPointF(-1, -1));
#else
   QHoverEvent hoverEvent(QEvent::HoverLeave, QPointF(-1, -1), QPointF(-1, -1));
#endif

   viewportEvent(&hoverEvent);

   viewport()->update();
}

void QTreeViewEx::dropEvent(QDropEvent* event)
{
   if (_allowedDropActions == Qt::IgnoreAction)
   {
      QTreeView::dropEvent(event);
      return;
   }

   setEventDropAction(event);

   if (!event->isAccepted())
      return;

   auto m = dynamic_cast<HtItemModel*>(model());

   if (m == nullptr)
      return;

#if QT_VERSION >= QT_VERSION_CHECK(6,0,0)
   const auto index = indexAt(event->position().toPoint());
#else
   const auto index = indexAt(event->pos());
#endif

   const auto targetNode = m->nodeFromIndex(index);

   if (targetNode == nullptr)
      return;

   event->ignore(); // Ignore by default

   auto dropAction = event->dropAction();

   const auto mimeData = dynamic_cast<const HtMimeData*>(event->mimeData());

   if (mimeData && !mimeData->nodeList().isEmpty())
   {
#if QT_VERSION >= QT_VERSION_CHECK(6,0,0)
      setDropPosition(mimeData->nodeList(), m, targetNode, event->position().toPoint(), dropAction);
#else
      setDropPosition(mimeData->nodeList(), m, targetNode, event->pos(), dropAction);
#endif

      emit canDropNodes(mimeData->nodeList(), targetNode, dropAction, _dropPosition);

      if (_dropPosition != DropInvalid)
      {
#if QT_VERSION >= QT_VERSION_CHECK(6,0,0)
         if (event->buttons() == Qt::RightButton)
#else
         if (event->mouseButtons() == Qt::RightButton)
#endif
         {
            const auto possibleActions = _allowedDropActions & event->possibleActions();

            QMenu menu;
            QAction* copyAction = nullptr;
            QAction* moveAction = nullptr;
            QAction* linkAction = nullptr;

            if (possibleActions & Qt::CopyAction)
               copyAction = menu.addAction(tr("Copy"));
            if (possibleActions & Qt::MoveAction)
               moveAction = menu.addAction(tr("Move"));
            if (possibleActions & Qt::LinkAction)
               linkAction = menu.addAction(tr("Link"));
            menu.addSeparator();
            menu.addAction(tr("Abort"));

#if QT_VERSION >= QT_VERSION_CHECK(6,0,0)
            const auto action = menu.exec(mapToGlobal(event->position().toPoint()));
#else
            const auto action = menu.exec(mapToGlobal(event->pos()));
#endif

            if (copyAction && action == copyAction)
               dropAction = Qt::CopyAction;
            else if (moveAction && action == moveAction)
               dropAction = Qt::MoveAction;
            else if (linkAction && action == linkAction)
               dropAction = Qt::LinkAction;
            else
               _dropPosition = DropInvalid;
         }
         else if (_dropPosition == DropReplaceItem)
         {
            if (QMessageBox::Yes != QMessageBox::question(window(), tr("Drag & Drop"), tr("Replace item?")))
               _dropPosition = DropInvalid;
         }
      }

      auto row = 0;
      auto parent = _dropPosition == DropIntoItem ? targetNode : targetNode->parent();

      switch (_dropPosition)
      {
      case DropInvalid:
         parent = nullptr;
         break;
      case DropAboveItem:
         row = parent->indexOf(targetNode);
         break;
      case DropBelowItem:
         row = parent->indexOf(targetNode) + 1;
         break;
      case DropReplaceItem:
         row = parent->indexOf(targetNode);
         break;
      case DropIntoItem:
         row = parent->childCount();
         break;
      }

      if (parent)
      {
         HtNode* currentNode = nullptr;

         for (auto&& droppedNode : mimeData->nodeList())
         {
            const auto node = parent->insertChild(row, droppedNode, dropAction != Qt::CopyAction);

            if (droppedNode == mimeData->draggedNode())
               currentNode = node;

            emit nodeDropped(node, droppedNode, dropAction);

            row++;
         }

         if (_dropPosition == DropReplaceItem)
         {
            if (dropAction == Qt::MoveAction && m == mimeData->nodeList().first()->model()) // Internal move
            {
               auto dropNodeList = mimeData->nodeList();

               for (int i = dropNodeList.count(); i--; )
               {
                  auto droppedParent = dropNodeList.at(i)->parent();

                  while (droppedParent)
                  {
                     if (droppedParent == targetNode)
                        break;

                     droppedParent = droppedParent->parent();
                  }

                  if (droppedParent)
                     dropNodeList.removeAt(i);
               }

               dropAction = Qt::TargetMoveAction;

               for (auto&& droppedNode : dropNodeList)
                  droppedNode->parent()->removeChild(droppedNode->parent()->indexOf(droppedNode));
            }

            targetNode->remove();
         }

         event->accept();

         if (currentNode)
         {
            setCurrentIndex(m->indexFromNode(currentNode));
            setFocus();
         }
      }

      event->setDropAction(dropAction);
   }

   _dropPosition = DropInvalid;
   _dropItemRect = QRect();

   // TODO: stop auto scroll

   setState(NoState);

   viewport()->update();
}

void QTreeViewEx::updateGeometries()
{
   auto vScrollBarPosition = verticalScrollBar()->value();

   QTreeView::updateGeometries();

   if (!_minimizeScrollRange && verticalScrollMode() == QAbstractItemView::ScrollPerPixel)
   {
      QSize viewportSize = viewport()->size();
      if (!viewportSize.isValid())
          viewportSize = QSize(0, 0);

      verticalScrollBar()->setMaximum(verticalScrollBar()->maximum() + viewportSize.height() - 20);
      verticalScrollBar()->setValue(vScrollBarPosition);
      viewport()->update();
   }
}

int QTreeViewEx::sizeHintForColumn(int column) const
{
   auto styledItemDelegate = dynamic_cast<QStyledItemDelegateEx*>(QTreeView::itemDelegate());

   styledItemDelegate->setHeightForWidth(false);

   const auto sizeHint = QTreeView::sizeHintForColumn(column);

   styledItemDelegate->setHeightForWidth(true);

   return sizeHint;
}

bool QTreeViewEx::resizeColumn(QMouseEvent* e)
{
   if (!_resizeColumns)
      return false;

   // Resize columns by dragging the vertical separator

   if (e->buttons() == Qt::NoButton)
   {
      _logicalCursorColumn = -1;
      _resizeRightColumn = false;

      int x = dirtyRegionOffset().x() - horizontalScrollBar()->value();
      int end = header()->count();

      if (header()->stretchLastSection())
         end--;

      for (int visual_index = 0; visual_index < end; visual_index++)
      {
         int logical_index = header()->logicalIndex(visual_index);

         if (!header()->isSectionHidden(logical_index))
         {
            if (header()->sectionResizeMode(logical_index) == QHeaderView::Stretch)
            {
               _resizeRightColumn = true;
            }
            else
            {
               if (!_resizeRightColumn)
                  x += header()->sectionSize(logical_index);

#if QT_VERSION >= QT_VERSION_CHECK(6,0,0)
               if (qRound(e->position().x()) >= x - 3 && qRound(e->position().x()) <= x + 3)
#else
               if (e->x() >= x - 3 && e->x() <= x + 3)
#endif
               {
                  _logicalCursorColumn = logical_index;
                  break;
               }
            }
            if (_resizeRightColumn)
               x += header()->sectionSize(logical_index);
         }
      }
      if (_logicalCursorColumn != -1)
         setCursor(Qt::SplitHCursor);
      else
         unsetCursor();
   }
   else if (e->buttons() == Qt::LeftButton && _logicalCursorColumn != -1)
   {
      int size;

      if (_resizeRightColumn)
#if QT_VERSION >= QT_VERSION_CHECK(6,0,0)
         size = _sectionSizeOffset - qRound(e->position().x());
      else
         size = qRound(e->position().x()) - _sectionSizeOffset;
#else
         size = _sectionSizeOffset - e->x();
      else
         size = e->x() - _sectionSizeOffset;
#endif

      if (size < 5)
         size = 5;
      header()->resizeSection(_logicalCursorColumn, size);
      e->accept();
      return true;
   }

   return false;
}

void QTreeViewEx::setDragPicture(QDrag* drag, const HtItemModel* model, const QList<const HtNode*>& dragNodeList) const
{
   QList<QPair<QModelIndex, QRect>> cells;
   const auto viewportRect = viewport()->rect();
   QRect viewRect;

   for (auto&& node : dragNodeList)
   {
      for (auto col = 0; col < model->columnCount(); ++col)
      {
         const auto index = model->indexFromNode(node, col);
         auto rect = visualRect(index);

         if (rect.intersects(viewportRect))
         {
            viewRect |= rect;
            cells.append(QPair<QModelIndex, QRect>(index, rect));
         }
      }
   }
   viewRect &= viewportRect;

   auto scale = 1.0;

   if (auto w = window())
      if (auto wh = w->windowHandle())
         scale = wh->devicePixelRatio();

   QPixmap pixmap(viewRect.size() * scale);

   pixmap.setDevicePixelRatio(scale);
   pixmap.fill(Qt::transparent);

   QPainter painter(&pixmap);

   painter.setOpacity(0.6);

   QStyleOptionViewItem option;
#if QT_VERSION >= QT_VERSION_CHECK(6, 0, 0)
   initViewItemOption(&option);
#else
   option = viewOptions();
#endif

   option.state |= QStyle::State_Selected;
   option.showDecorationSelected = (selectionBehavior() & SelectRows) || option.showDecorationSelected;

   for (auto&& cell : cells)
   {
      option.rect = cell.second.translated(-viewRect.topLeft());
      if (model->columnCount() == 1 || isFirstColumnSpanned(cell.first.row(), cell.first.parent()))
         option.viewItemPosition = QStyleOptionViewItem::OnlyOne;
      else if (cell.first.column() == 0)
         option.viewItemPosition = QStyleOptionViewItem::Beginning;
      else if (cell.first.column() + 1 == model->columnCount())
         option.viewItemPosition = QStyleOptionViewItem::End;
      else
         option.viewItemPosition = QStyleOptionViewItem::Middle;

      QTreeView::itemDelegate()->paint(&painter, option, cell.first);
   }

   drag->setPixmap(pixmap);
   drag->setHotSpot(_dragStartPosition - viewRect.topLeft());
}

QString QTreeViewEx::getMimeText(const QModelIndexList& indexList)
{
   QRect cellsRect;

   for (auto&& index : indexList)
   {
      cellsRect |= QRect(index.column(), index.row(), 1, 1);
   }

   QVector<QVector<QString>> stringTable(cellsRect.height());

   for (auto i = 0; i < stringTable.count(); ++i)
   {
      stringTable[i].resize(cellsRect.width());
   }

   for (auto&& index : indexList)
   {
      stringTable[index.row() - cellsRect.top()][index.column() - cellsRect.left()] = index.data().toString();
   }

   QStringList textRows;

   for (auto&& row : stringTable)
   {
      textRows.append(row.toList().join(" | "));
   }

   return textRows.join("\n");
}

void QTreeViewEx::setEventDropAction(QDropEvent* event) const
{
#if QT_VERSION >= QT_VERSION_CHECK(6,0,0)
   if (event->modifiers() && (event->proposedAction() & _allowedDropActions))
#else
   if (event->keyboardModifiers() && (event->proposedAction() & _allowedDropActions))
#endif
   {
      event->acceptProposedAction();
      return;
   }

   event->ignore(); // Ignore by default

   const auto possibleActions = _allowedDropActions & event->possibleActions();

   if (!possibleActions)
      return;

   Qt::DropAction dropAction;

   if (event->source() == this && (possibleActions & Qt::MoveAction))
      dropAction = Qt::MoveAction;
   else if (possibleActions & Qt::LinkAction)
      dropAction = Qt::LinkAction;
   else if (possibleActions & Qt::CopyAction)
      dropAction = Qt::CopyAction;
   else
      dropAction = Qt::MoveAction;

   event->setDropAction(dropAction);
   event->accept();
}

void QTreeViewEx::setDropPosition(const QList<const HtNode*>& dropNodeList, const HtItemModel* model, const HtNode* targetNode, const QPoint& pos, Qt::DropAction dropAction)
{
   _dropPosition = DropInvalid;
   _dropItemRect = QRect();

   // If its an internal move or link the parents of the node must not be part of the dragged nodes

   if (dropAction != Qt::CopyAction && !dropNodeList.isEmpty() && model == dropNodeList.first()->model())
   {
      auto parentNode = targetNode;

      while (parentNode)
      {
         if (dropNodeList.contains(parentNode))
            break;
         parentNode = parentNode->parent();
      }

      if (parentNode)
         return;
   }

   auto leadIndex = model->indexFromNode(targetNode);

   if (leadIndex != rootIndex())
   {
      const auto margin = qMax(minimumRowHeight(), 12) / 3;
      const auto rect = visualRect(leadIndex);

      _dropItemRect = rect;

      if (_dropItemRect.left() < viewport()->rect().left())
      {
         _dropItemRect.setLeft(viewport()->rect().left());
         _dropItemRect.setRight(viewport()->rect().right());
      }

      auto lastVisibleColumn = header()->count();

      while (lastVisibleColumn--)
      {
         if (!header()->isSectionHidden(header()->logicalIndex(lastVisibleColumn)))
            break;
      }

      if (lastVisibleColumn != -1)
      {
         lastVisibleColumn = header()->logicalIndex(lastVisibleColumn);

         _dropItemRect.setRight(header()->sectionViewportPosition(lastVisibleColumn) + header()->sectionSize(lastVisibleColumn) - 1);
      }

      if (_dropItemRect.right() > viewport()->rect().right())
      {
         _dropItemRect.setRight(viewport()->rect().right());
      }

      _dropItemRect.adjust(0, 0, -1, 0);

      if (pos.y() > rect.top() + margin && pos.y() < rect.bottom() - margin)
      {
         if (dragDropOverwriteMode() &&
            (pos.x() < rect.left() + indentation() || (model->flags(leadIndex) & Qt::ItemNeverHasChildren)) &&
            (model->flags(leadIndex.parent()) & Qt::ItemIsDropEnabled))
         {
            _dropPosition = DropReplaceItem;
         }
         else if (pos.x() >= rect.left() + indentation() &&
            (model->flags(leadIndex) & Qt::ItemIsDropEnabled) &&
            !(model->flags(leadIndex) & Qt::ItemNeverHasChildren))
         {
            _dropPosition = DropIntoItem;
         }
      }

      if (_dropPosition == DropInvalid)
      {
         if (pos.y() < rect.center().y())
         {
            if ((model->flags(leadIndex.parent()) & Qt::ItemIsDropEnabled))
               _dropPosition = DropAboveItem;
         }
         else if (pos.x() >= rect.left() + indentation() &&
            (model->flags(leadIndex) & Qt::ItemIsDropEnabled) &&
            !(model->flags(leadIndex) & Qt::ItemNeverHasChildren))
         {
            _dropPosition = DropIntoItem;
         }
         else
         {
            if (model->flags(leadIndex.parent()) & Qt::ItemIsDropEnabled)
               _dropPosition = DropBelowItem;
         }
      }
   }
   else
   {
      // Root node
      if (model->flags(leadIndex) & Qt::ItemIsDropEnabled)
      {
         _dropPosition = DropIntoItem;
      }
   }
}

void QTreeViewEx::setIndexIsConcealed(const QModelIndex& index, bool isConcealed) const
{
   auto lb = std::lower_bound(_concealedTextIndexList.begin(), _concealedTextIndexList.end(), index);

   if (lb != _concealedTextIndexList.end() && *lb == index)
   {
      if (!isConcealed)
         _concealedTextIndexList.erase(lb);
      return;
   }

   if (isConcealed)
      _concealedTextIndexList.insert(lb, index);
}
