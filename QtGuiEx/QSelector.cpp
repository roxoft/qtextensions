#include "stdafx.h"
#include "QSelector.h"

#include "QComboBoxEx.h"
#include <QEvent>
#include <QKeyEvent>
#include <QApplication>
#include <QStyleOptionFrame>

static const QColor errorColor(Qt::red);
static const QColor warningColor(Qt::blue);

QSelectorGroup::QSelectorGroup(QObject *parent) : QObject(parent)
{
}

void QSelectorGroup::setDataSource(QCompleterDataSource* dataSource)
{
   _dataSource = dataSource;
   if (_dataSource->parent() == nullptr)
      _dataSource->setParent(this);
}

void QSelectorGroup::addSelector(QSelector *selector)
{
   _selectors.append(selector);
   selector->setDataSource(_dataSource);
   connect(selector, SIGNAL(currentIndexChanged(QModelIndex)), this, SLOT(onCurrentIndexChanged(QModelIndex)));
}

void QSelectorGroup::onCurrentIndexChanged(const QModelIndex &index)
{
   if (_processingSignal)
      return;

   _processingSignal = true;

   foreach (QSelector *selector, _selectors)
   {
      selector->setCurrentRow(index.row());
   }

   _processingSignal = false;

   emit currentRowChanged(index.row());
}


QSelector::QSelector(QWidget* parent, QCompleterDataSource* dataSource, int column, QWidget* alignTo)
   : QLineEdit(parent), _alignTo(alignTo)
{
   _completer = new QCompleterEx(this, dataSource, column, this);

   connect(_completer, &QCompleterEx::currentIndexChanged, this, &QSelector::on_completerCurrentIndexChanged);
   connect(_completer, &QCompleterEx::currentTextChanged, this, &QSelector::on_completerCurrentTextChanged);
   connect(_completer, &QCompleterEx::matchIndexChanged, this, &QSelector::on_completerMatchIndexChanged);

   connect((QLineEdit*)this, SIGNAL(editingFinished()), this, SLOT(on_editingFinished()));
   connect((QLineEdit*)this, SIGNAL(textChanged(const QString&)), this, SLOT(on_textChanged(const QString&)));
   connect((QLineEdit*)this, SIGNAL(textEdited(const QString&)), this, SLOT(on_textEdited(const QString&)));

   // If parent is a QComboBoxEx, connect to it
   QComboBoxEx* combo = qobject_cast<QComboBoxEx*>(parent);

   if (combo)
   {
      combo->setContent(this);
      connect(combo, SIGNAL(clicked()), this, SLOT(showSelectionList()));
      if (_alignTo == nullptr)
         _alignTo = combo;
   }
   else if (_alignTo == nullptr)
      _alignTo = this;
}

QModelIndex QSelector::addEditText()
{
   return _completer->addText(text());
}

QModelIndex QSelector::insertItem(int row, const QString& text, const Variant& userData)
{
   return insertItem(row, QIcon(), text, userData);
}

QIcon QSelector::itemIcon(int row) const
{
   return _completer->itemIcon(row);
}

bool QSelector::event(QEvent* e)
{
   // Eat focus out (keeps the cursor alive)
   if (e->type() == QEvent::FocusOut && _completer->isPopupVisible())
      return true;

   // Parent changes
   if (e->type() == QEvent::ParentAboutToChange)
   {
      QComboBoxEx* combo = qobject_cast<QComboBoxEx*>(this->parent());

      if (combo)
      {
         disconnect(combo, SIGNAL(clicked()), this, SLOT(showSelectionList()));
         combo->setContent(nullptr);
      }
      if (_alignTo == this->parent())
         _alignTo = this;
   }
   else if (e->type() == QEvent::ParentChange)
   {
      QComboBoxEx* combo = qobject_cast<QComboBoxEx*>(this->parent());

      if (combo)
      {
         combo->setContent(this);
         connect(combo, SIGNAL(clicked()), this, SLOT(showSelectionList()));
         if (_alignTo == this)
            _alignTo = combo;
      }
   }

   return QLineEdit::event(e);
}

QSize QSelector::sizeHint() const
{
   const QFontMetrics fontMetrics(font());
   auto maxTextWidth = fontMetrics.horizontalAdvance(QLatin1Char('x')) * qMin(qMax(maxLength(), 1), 17);

   if (_completer->hasDataSource())
   {
      const auto m = _completer->model();

      for (int row = 0; row < m->rowCount(); ++row)
      {
         const auto text = toQString(m->data(m->index(row, _completer->column())));
         const auto width = fontMetrics.horizontalAdvance(text);

         if (maxTextWidth < width)
            maxTextWidth = width;
      }
   }

   // Code copied from Qt5.13.0\5.13.0\Src\qtbase\src\widgets\widgets\qlineedit.cpp

   const int verticalMargin(1);
   const int horizontalMargin(2);

   ensurePolished();
   const int iconSize = style()->pixelMetric(QStyle::PM_SmallIconSize, 0, this);
   int h = qMax(fontMetrics.height(), iconSize - 2);
   h += 2 * verticalMargin;
   h += textMargins().top() + textMargins().bottom();
   h += contentsMargins().top() + contentsMargins().bottom();
   int w = maxTextWidth;
   w += 2 * horizontalMargin;
   w += textMargins().left() + textMargins().right();
   w += contentsMargins().left() + contentsMargins().right();
   QStyleOptionFrame opt;
   initStyleOption(&opt);
   return style()->sizeFromContents(QStyle::CT_LineEdit, &opt, QSize(w, h), this);
}

void QSelector::clearAll()
{
   _completer->clear();
   geometryChanged();
}

void QSelector::clearEditText()
{
   setCurrentIndex(QModelIndex());
}

void QSelector::setCurrentIndex(const QModelIndex& index)
{
   _completer->setCurrentIndex(index);
}

void QSelector::setCurrentRow(int row)
{
   _completer->setCurrentRow(row);
}

void QSelector::setEditText(const QString &text)
{
   this->setText(text);
}

void QSelector::showSelectionList()
{
   if (this->isReadOnly())
      return;

   _completer->showSelectionList(this->text(), _alignTo->parentWidget()->mapToGlobal(_alignTo->geometry().bottomLeft()), _alignTo->width());
}

void QSelector::keyPressEvent(QKeyEvent* e)
{
   _lastKeyPressed = e->key();

   if (_lastKeyPressed == Qt::Key_Down || _lastKeyPressed == Qt::Key_Up)
   {
      e->accept();
      showSelectionList();
      return;
   }
   if (_lastKeyPressed == Qt::Key_Return || _lastKeyPressed == Qt::Key_Enter)
   {
      if (_completer->acceptSelectedItem() && !_completer->isFreeText())
      {
         e->accept();
         return;
      }
   }
   QLineEdit::keyPressEvent(e);
}

void QSelector::mousePressEvent(QMouseEvent* e)
{
   QLineEdit::mousePressEvent(e);
   if (e->button() == Qt::LeftButton && !this->hasSelectedText() && _completer->hasDataSource() && !_completer->dataSource()->isFilterMode())
      showSelectionList();
}

void QSelector::focusOutEvent(QFocusEvent *e)
{
   this->setCursorPosition(0);
   QLineEdit::focusOutEvent(e);
   on_editingFinished(); // Make sure editing has finished (nothing happens if the state is not S_Editing)
   if (qobject_cast<QComboBoxEx*>(parent()))
      qApp->notify(parent(), e);
}

void QSelector::on_editingFinished()
{
   if (_isEditing)
   {
      _isEditing = false;
      _completer->setCurrentText(this->text());
   }
}

void QSelector::on_textEdited(const QString& text)
{
   if (!text.isEmpty() && !hasAcceptableInput()) // An empty text is always acceptable
      return;

   _isEditing = true;

   if (!_completer->updateSelectList(text)) // This may invalidate the index
   {
      setTextColor(Qt::gray);
      this->setCursor(Qt::BusyCursor);
   }
}

void QSelector::on_textChanged(const QString& text)
{
   if (!_isEditing && _completer->hasDataSource())
   {
      if (text != _completer->dataSource()->text(_completer->currentIndex()))
      {
         _completer->updateSelectList(text);
         _completer->setCurrentText(text);
      }
   }
}

void QSelector::on_completerCurrentIndexChanged(const QModelIndex& index)
{
   emit currentIndexChanged(index);
}

void QSelector::on_completerCurrentTextChanged(bool useCurrentIndex)
{
   Q_ASSERT(hasFocus() || !_isEditing);

   const auto wasEditing = useCurrentIndex ? false : _isEditing;

   _isEditing = true;

   // Reset error marker
   resetTextColor();

   const auto oldLength = this->text().length();
   const auto oldPos = this->cursorPosition();
   const auto currentText = _completer->dataSource()->text(_completer->currentIndex());

   if (useCurrentIndex || !_completer->isFreeText())
      this->setText(currentText);
   else if (!this->text().isEmpty() && this->text() != currentText)
      setTextColor(warningColor);

   if (hasFocus())
   {
      if (oldLength && oldPos == oldLength)
         this->setCursorPosition(this->text().length());
      else if (oldPos < this->text().length())
         this->setCursorPosition(oldPos);
   }

   _isEditing = wasEditing;
}

void QSelector::on_completerMatchIndexChanged(const QModelIndex& index)
{
   const auto selectedText = _completer->dataSource()->text(index);

   if (this->text().isEmpty() || this->text() == selectedText)
      resetTextColor();
   else if (_completer->isFreeText())
      setTextColor(warningColor);
   else
      setTextColor(errorColor);

   if (_isEditing && hasFocus() && _completer->itemCount() && (!_completer->isFreeText() || selectedText.startsWith(this->text(), Qt::CaseInsensitive)))
   {
      // Suggest completion

      const auto cursorPos = this->cursorPosition();

      if (_lastKeyPressed != Qt::Key_Backspace && _lastKeyPressed != Qt::Key_Delete
         && !this->text().isEmpty()
         && cursorPos == this->text().length()
         && selectedText.startsWith(this->text()))
      {
         resetTextColor();
         this->setText(selectedText);
         this->setSelection(this->text().length(), cursorPos - this->text().length());
      }

      // Show pop-up

      _completer->showSelectionList(index, _alignTo->parentWidget()->mapToGlobal(_alignTo->geometry().bottomLeft()), _alignTo->width());
   }
   else
   {
      _completer->hideSelectionList();
   }

   this->setCursor(Qt::IBeamCursor);
}

void QSelector::geometryChanged()
{
   QComboBoxEx* combo = qobject_cast<QComboBoxEx*>(parent());

   if (combo)
      combo->updateGeometry();
   else
      updateGeometry();
}

void QSelector::setTextColor(const QColor &color)
{
   QPalette p;

   if (color.isValid())
   {
      p.setColor(QPalette::Text, color);
      _hasColor = true;
   }
   else
   {
      _hasColor = false;
   }

   setPalette(p);
}

void QSelector::resetTextColor()
{
   if (_hasColor)
      setTextColor(QColor());
}
