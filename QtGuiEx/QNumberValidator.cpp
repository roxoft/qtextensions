#include "stdafx.h"
#include "QNumberValidator.h"
#include <QLocaleEx.h>

QNumberValidator::QNumberValidator(int integerDigits, int fractionalDigits, QObject* parent)
   : QValidator(parent),
   _integerDigits(integerDigits < 1 ? 16 : integerDigits),
   _fractionalDigits(fractionalDigits < 0 ? 2 : fractionalDigits),
   _groupSeparatorMode(QDecimal::GS_Auto),
   _emptyAllowed(false),
   _zeroAllowed(true),
   _negativeAllowed(false)
{
   _decimalSeparator = defaultLocale().decimalPoint();
   _thousandsSeparator = defaultLocale().groupSeparator();
   _zeroDigit = defaultLocale().zeroDigit();
   _negativeSign = defaultLocale().negativeSign();
}

QString QNumberValidator::defaultText() const
{
   if (_emptyAllowed)
      return QString();

   QString text(_zeroDigit);

   if (_fractionalDigits)
      text += QString(_decimalSeparator) + QString(_fractionalDigits, _zeroDigit);

   return text;
}

QValidator::State QNumberValidator::validate(QString& input, int& pos) const
{
   // Assert that there are no more than one decimal separator
   int pointPos = input.indexOf(_decimalSeparator);

   if (_fractionalDigits == 0)
   {
      while (pointPos != -1)
      {
         input.remove(pointPos, 1);
         if (pos > pointPos)
            pos--;
         pointPos = input.indexOf(_decimalSeparator, pointPos);
      }
   }
   else if (pointPos == -1)
   {
      if (!_emptyAllowed || !input.isEmpty())
      {
         pointPos = pos;
         input.insert(pointPos, _decimalSeparator);
      }
   }
   else
   {
      for (;;)
      {
         int second = input.indexOf(_decimalSeparator, pointPos + 1);

         if (second == -1)
            break;

         if (qAbs(pos - 1 - pointPos) < qAbs(pos - 1 - second))
         {
            input.remove(second, 1);
            if (pos > second)
               pos--;
         }
         else
         {
            input.remove(pointPos, 1);
            if (pos > pointPos)
               pos--;
            pointPos = second - 1;
         }
      }
   }

   // Remove invalid characters, leading zeros and reduce length of integer and fractional part if necessary
   int   digitCount = 0;
   bool  isIntegerPart = true;
   int   charPos = 0;
   bool  isNotZero = false;
   //bool  isNegative = false;

   pointPos = 0;

   while (charPos < input.length())
   {
      QChar c(input[charPos]);

      if (isNegativeSign(c))
      {
         if (_negativeAllowed && charPos == 0)
         {
            //isNegative = true;
            charPos++;
         }
         else
         {
            input.remove(charPos, 1);
            if (pos > charPos)
               pos--;
         }
      }
      else if (c == _decimalSeparator)
      {
         isIntegerPart = false;
         // Make sure there is at least one integer digit before the decimal point
         if (digitCount == 0)
         {
            input.insert(charPos, _zeroDigit);
            pos++;
            charPos++;
         }
         pointPos = charPos;
         digitCount = 0;
         charPos++;
      }
      else if (c == _thousandsSeparator)
      {
         if (useGroupSeparator() && isNotZero && isIntegerPart)
            charPos++; // Correct placement is verified later
         else
         {
            input.remove(charPos, 1);
            if (pos > charPos)
               pos--;
         }
      }
      else if (c.isDigit())
      {
         if (isIntegerPart && digitCount == 1 && input[charPos - 1] == _zeroDigit)
         {
            if (c != _zeroDigit)
               isNotZero = true;
            // Remove leading zero
            input.remove(charPos - 1, 1);
            if (pos > charPos - 1)
               pos--;
         }
         else if (digitCount == (isIntegerPart ? _integerDigits : _fractionalDigits))
         {
            input.remove(charPos, 1);
            if (pos > charPos)
               pos--;
         }
         else
         {
            if (c != _zeroDigit)
               isNotZero = true;
            digitCount++;
            charPos++;
         }
      }
      else
      {
         input.remove(charPos, 1);
         if (pos > charPos)
            pos--;
      }
   }

   if (input.isEmpty() && !_emptyAllowed)
      return QValidator::Intermediate;

   // Adjust fractional digit length
   if (!input.isEmpty() && digitCount < _fractionalDigits)
      input.append(QString(_fractionalDigits - digitCount, _zeroDigit));

   if (!isNotZero && !_zeroAllowed)
      return QValidator::Intermediate;

   // Set group separators
   if (useGroupSeparator())
   {
      digitCount = 0;
      charPos = pointPos ? pointPos : input.length();
      while (charPos--)
      {
         QChar c(input[charPos]);

         if (isNegativeSign(c))
            break;

         if (c == _thousandsSeparator)
         {
            if (digitCount != 3)
            {
               input.remove(charPos, 1);
               if (pos > charPos)
                  pos--;
            }
            else
               digitCount = 0;
         }
         else if (digitCount == 3 && _thousandsSeparator.isPrint())
         {
            input.insert(charPos + 1, _thousandsSeparator);
            if (pos > charPos + 1)
               pos++;
            digitCount = 0;
         }
         else
            digitCount++;
      }
   }

   return QValidator::Acceptable;
}

void QNumberValidator::fixup(QString &input) const
{
   if (!_emptyAllowed && input.isEmpty())
      input = defaultText();

   if (!_zeroAllowed)
   {
      foreach (QChar qChar, input)
      {
         if (qChar.isDigit() && qChar != _zeroDigit)
            return;
      }

      input[input.length() - 1] = QLatin1Char('1');
   }
}

bool QNumberValidator::isNegativeSign(QChar qChar) const
{
   return qChar == _negativeSign || qChar == QLatin1Char('-') || qChar == QChar(0x2212);
}
