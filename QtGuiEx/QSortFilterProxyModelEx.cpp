#include "stdafx.h"
#include "QSortFilterProxyModelEx.h"
#include <Variant.h>

QSortFilterProxyModelEx::QSortFilterProxyModelEx(QObject *parent) : QSortFilterProxyModel(parent)
{
}

QSortFilterProxyModelEx::~QSortFilterProxyModelEx()
{
}

bool QSortFilterProxyModelEx::lessThan(const QModelIndex &left, const QModelIndex &right) const
{
   return Variant(left.data(sortRole())) < Variant(right.data(sortRole()));
}
