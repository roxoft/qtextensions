#include "stdafx.h"
#include "QTextEditEx.h"
#include "QCompleterEx.h"
#include <QAbstractItemView>
#include <QKeyEvent>
#include <QScrollBar>

QTextEditEx::QTextEditEx(QWidget *parent)
   : QTextEdit(parent)
{
}

QTextEditEx::~QTextEditEx()
{
}

void QTextEditEx::setCompleterDataSource(QCompleterDataSource* dataSource)
{
   if (dataSource)
   {
      if (_completer)
      {
         _completer->setDataSource(dataSource);
      }
      else
      {
         _completer = new QCompleterEx(this, dataSource, 0, this);

         connect(_completer, &QCompleterEx::currentIndexChanged, this, &QTextEditEx::on_completerCurrentIndexChanged);
         connect(_completer, &QCompleterEx::currentTextChanged, this, &QTextEditEx::on_completerCurrentTextChanged);
         connect(_completer, &QCompleterEx::matchIndexChanged, this, &QTextEditEx::on_completerMatchIndexChanged);
      }
   }
   else
   {
      delete _completer;
      _completer = nullptr;
   }
}

QCompleterDataSource* QTextEditEx::completerDataSource() const
{
   if (_completer == nullptr)
      return nullptr;
   return _completer->dataSource();
}

bool QTextEditEx::event(QEvent* event)
{
   // Eat focus out (keeps the cursor alive)
   if (event->type() == QEvent::FocusOut && _completer && _completer->isPopupVisible())
      return true;

   if (event->type() == QEvent::MouseButtonPress && _completer && _completer->isPopupVisible())
      _completer->hideSelectionList();

   return QTextEdit::event(event);
}

void QTextEditEx::keyPressEvent(QKeyEvent* event)
{
   if (!_completer)
   {
      QTextEdit::keyPressEvent(event);
      return;
   }

   _lastKeyPressed = event->key();

   if (event->modifiers() & Qt::ControlModifier && (_lastKeyPressed == Qt::Key_Return || _lastKeyPressed == Qt::Key_Enter))
   {
      if (_completer->acceptSelectedItem() && !_completer->isFreeText())
      {
         event->accept();
         return;
      }
   }

   const auto isShortcut = ((event->modifiers() & Qt::ControlModifier) && _lastKeyPressed == Qt::Key_E); // CTRL-E

   if (!_completer || !isShortcut) // Do not process the shortcut if we have a completer
      QTextEdit::keyPressEvent(event);

   const bool ctrlOrShift = event->modifiers() & (Qt::ControlModifier | Qt::ShiftModifier);
   if (!_completer || (ctrlOrShift && event->text().isEmpty()))
      return;

   static QString eow(" ~!@#$%^&*()_+{}|:\"<>?,./;'[]\\-="); // end of word

   const bool hasModifier = (event->modifiers() != Qt::NoModifier) && !ctrlOrShift;

   auto tc = textCursor();

   tc.select(QTextCursor::WordUnderCursor);

   QString completionPrefix = tc.selectedText();

   if (!isShortcut && (hasModifier || event->text().isEmpty() || completionPrefix.length() < 3|| eow.contains(event->text().right(1))))
   {
      _completer->hideSelectionList();
      return;
   }

   if (!_completer->updateSelectList(completionPrefix))
      this->setCursor(Qt::BusyCursor);
}

void QTextEditEx::mousePressEvent(QMouseEvent* event)
{
   QTextEdit::mousePressEvent(event);
   //if (event->button() == Qt::LeftButton && !this->hasSelectedText() && _completer->hasDataSource() && !_completer->dataSource()->isFilterSupported())
   //{
   //   showSelectionList();
   //}
}

void QTextEditEx::focusOutEvent(QFocusEvent* event)
{
   QTextEdit::focusOutEvent(event);
   //on_editingFinished(); // Make sure editing has finished (nothing happens if the state is not S_Editing)
}

void QTextEditEx::on_completerCurrentIndexChanged(const QModelIndex& index)
{
}

void QTextEditEx::on_completerCurrentTextChanged(bool useCurrentIndex)
{
   // Reset error marker
   //resetTextColor();

   if (!_completer->currentIndex().isValid())
      return;

   const auto currentText = _completer->dataSource()->text(_completer->currentIndex());
   auto tc = textCursor();

   tc.select(QTextCursor::WordUnderCursor);

   auto completionPrefix = tc.selectedText();

   if (useCurrentIndex || !_completer->isFreeText())
   {
      tc.removeSelectedText();
      tc.insertText(currentText);
      setTextCursor(tc);
   }
   //else if (!completionPrefix.isEmpty() && completionPrefix != currentText)
   //   setTextColor(warningColor);

   //if (useCurrentIndex)
   //   _isEditing = false;
}

void QTextEditEx::on_completerMatchIndexChanged(const QModelIndex& index)
{
   const auto selectedText = _completer->dataSource()->text(index);

   auto tc = textCursor();

   tc.select(QTextCursor::WordUnderCursor);

   auto completionPrefix = tc.selectedText();

   //if (completionPrefix.isEmpty() || completionPrefix == selectedText)
   //   resetTextColor();
   //else if (_completer->isFreeText())
   //   setTextColor(warningColor);
   //else
   //   setTextColor(errorColor);

   if (hasFocus() && _completer->itemCount() && (!_completer->isFreeText() || selectedText.startsWith(completionPrefix, Qt::CaseInsensitive)))
   {
#if 0
      // Suggest completion

      const auto cursorPos = this->cursorPosition();

      if (_lastKeyPressed != Qt::Key_Backspace && _lastKeyPressed != Qt::Key_Delete
         && !this->text().isEmpty()
         && cursorPos == this->text().length()
         && selectedText.startsWith(this->text()))
      {
         resetTextColor();
         this->setText(selectedText);
         this->setSelection(this->text().length(), cursorPos - this->text().length());
      }
#endif

      // Show pop-up

      QRect cr = cursorRect();

      cr.setWidth(50);
      _completer->showSelectionList(index, this->mapToGlobal(cr.bottomLeft()), cr.width());
   }
   else
   {
      _completer->hideSelectionList();
   }

   this->setCursor(Qt::IBeamCursor);
}
