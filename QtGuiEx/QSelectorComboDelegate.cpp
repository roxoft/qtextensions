#include "stdafx.h"
#include "QSelectorComboDelegate.h"
#include "QComboBoxEx.h"
#include "QSelector.h"
#include <Variant.h>

QSelectorComboDelegate::QSelectorComboDelegate(QObject *parent, QCompleterDataSource *dataSource)
   : QEditItemDelegate(parent), _dataSource(dataSource), _isFreeText(false), _itemKeyRole(Qt::EditRole), _dataSourceKeyRole(Qt::DisplayRole)
{
   if (_dataSource && _dataSource->parent() == nullptr)
      _dataSource->setParent(this);
}

QSelectorComboDelegate::QSelectorComboDelegate(QCompleterDataSource *dataSource, int itemKeyRole, int dataSourceKeyRole, QObject *parent)
   : QEditItemDelegate(parent), _dataSource(dataSource), _isFreeText(false), _itemKeyRole(itemKeyRole), _dataSourceKeyRole(dataSourceKeyRole)
{
   if (_dataSource && _dataSource->parent() == nullptr)
      _dataSource->setParent(this);
}

QSelectorComboDelegate::~QSelectorComboDelegate()
{
}

void QSelectorComboDelegate::setDataSource(QCompleterDataSource *dataSource)
{
   _dataSource = dataSource;
   if (_dataSource && _dataSource->parent() == nullptr)
      _dataSource->setParent(this);
}

QWidget *QSelectorComboDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
   Q_UNUSED(option);

   if (_dataSource)
      _dataSource->hideInactiveItems(index.data(_itemKeyRole));

   QComboBoxEx *combo = new QComboBoxEx(parent);

   QSelector *selector = new QSelector(combo);

   selector->setDataSource(_dataSource);
   selector->setIsFreeText(_isFreeText);

   return combo;
}

void QSelectorComboDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
   QModelIndex currentIndex;

   if (_dataSource)
      currentIndex = _dataSource->find(_dataSource->model()->index(0, 0), _dataSourceKeyRole, index.data(_itemKeyRole));

   dynamic_cast<QSelector *>(dynamic_cast<QComboBoxEx *>(editor)->content())->setCurrentIndex(currentIndex);
}

void QSelectorComboDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{
   QModelIndex currentIndex = dynamic_cast<QSelector *>(dynamic_cast<QComboBoxEx *>(editor)->content())->currentIndex();

   if (!currentIndex.isValid() && !_defaultItemKey.isNull() && _dataSource)
   {
      currentIndex = _dataSource->find(_dataSource->model()->index(0, 0), _dataSourceKeyRole, _defaultItemKey);
   }

   if (currentIndex.isValid())
   {
      model->setData(index, _dataSource->text(currentIndex), Qt::EditRole);
      model->setData(index, currentIndex.data(Qt::DecorationRole), Qt::DecorationRole);
      model->setData(index, currentIndex.data(Qt::ToolTipRole), Qt::ToolTipRole);
      model->setData(index, currentIndex.data(Qt::FontRole), Qt::FontRole);
      model->setData(index, currentIndex.data(Qt::BackgroundRole), Qt::BackgroundRole);
      model->setData(index, currentIndex.data(Qt::ForegroundRole), Qt::ForegroundRole);
   }
   else
   {
      model->setData(index, QVariant(), Qt::EditRole);
      model->setData(index, QVariant(), Qt::DecorationRole);
      model->setData(index, QVariant(), Qt::ToolTipRole);
      model->setData(index, QVariant(), Qt::FontRole);
      model->setData(index, QVariant(), Qt::BackgroundRole);
      model->setData(index, QVariant(), Qt::ForegroundRole);
   }

   if (_itemKeyRole != Qt::EditRole && _itemKeyRole != Qt::DisplayRole)
   {
      if (currentIndex.isValid())
         model->setData(index, currentIndex.data(_dataSourceKeyRole), _itemKeyRole);
      else
         model->setData(index, QVariant(), _itemKeyRole);
   }
}

void QSelectorComboDelegate::updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
   QEditItemDelegate::updateEditorGeometry(editor, option, index);
}
