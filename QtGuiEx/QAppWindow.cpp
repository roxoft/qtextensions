#include "stdafx.h"
#include "QAppWindow.h"
#include <QEvent>
#include <QCloseEvent>
#include <QApplication>
#include <QAccessible>
#include <QWhatsThis>
#include "WidgetTools.h"

QAppWindow::QAppWindow(QWidget *parent, Qt::WindowFlags flags) :
   QMainWindow(parent, flags | ((flags & Qt::WindowType_Mask) == 0 ? Qt::Dialog : Qt::WindowType(0)))
{
   setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);
   if (windowFlags() & Qt::Dialog)
      setAttribute(Qt::WA_DeleteOnClose, true);
}

void QAppWindow::setVisible(bool visible)
{
   if (visible)
   {
      if (testAttribute(Qt::WA_WState_ExplicitShowHide) && !testAttribute(Qt::WA_WState_Hidden))
         return;

#if QT_VERSION >= QT_VERSION_CHECK(5,0,0)
      if (!testAttribute(Qt::WA_Moved))
         centerOnParent(this);
#else
      if (!testAttribute(Qt::WA_WState_Created))
         QMainWindow::create();

      adjustPosition(this, !testAttribute(Qt::WA_Moved)); // Make sure the window is visible
#endif

      QMainWindow::setVisible(true);

      QWidget *fw = window()->focusWidget();

      if (!fw)
         fw = this;

      if (fw && !fw->hasFocus()) {
         QFocusEvent e(QEvent::FocusIn, Qt::TabFocusReason);
         QApplication::sendEvent(fw, &e);
      }

#ifndef QT_NO_ACCESSIBILITY
#if QT_VERSION >= QT_VERSION_CHECK(5,0,0)
      QAccessibleEvent event(this, QAccessible::DialogStart);
      QAccessible::updateAccessibility(&event);
#else
      QAccessible::updateAccessibility(this, 0, QAccessible::DialogStart);
#endif
#endif

   }
   else
   {
      if (testAttribute(Qt::WA_WState_ExplicitShowHide) && testAttribute(Qt::WA_WState_Hidden))
         return;

#ifndef QT_NO_ACCESSIBILITY
      if (isVisible())
#if QT_VERSION >= QT_VERSION_CHECK(5,0,0)
      {
         QAccessibleEvent event(this, QAccessible::DialogEnd);
         QAccessible::updateAccessibility(&event);
      }
#else
         QAccessible::updateAccessibility(this, 0, QAccessible::DialogEnd);
#endif
#endif

      // Reimplemented to exit a modal event loop when the dialog is hidden.
      QMainWindow::setVisible(false);
      if (_eventLoop)
         _eventLoop->exit();
   }
}

void QAppWindow::exec()
{
   if (_eventLoop)
   {
      qWarning("QAppWindow::exec: Recursive call detected");
      return;
   }

   bool deleteOnClose = testAttribute(Qt::WA_DeleteOnClose);
   setAttribute(Qt::WA_DeleteOnClose, false);

   bool wasShowModal = testAttribute(Qt::WA_ShowModal);
   setAttribute(Qt::WA_ShowModal, true);

   show();

   QEventLoop eventLoop;

   _eventLoop = &eventLoop;

   QPointer<QAppWindow> guard = this;

   eventLoop.exec(QEventLoop::DialogExec);

   if (guard.isNull())
      return;

   _eventLoop = 0;

   setAttribute(Qt::WA_ShowModal, wasShowModal);

   if (deleteOnClose)
      delete this;
}

#if QT_VERSION >= QT_VERSION_CHECK(5,0,0)
void QAppWindow::showEvent(QShowEvent *e)
{
   QMainWindow::showEvent(e);
   if (!e->spontaneous())
      adjustToScreen(this); // Make sure the window is visible
}
#endif

void QAppWindow::closeEvent(QCloseEvent *e)
{
   if (isModal() && QWhatsThis::inWhatsThisMode())
      QWhatsThis::leaveWhatsThisMode();
   if (isVisible())
   {
      QPointer<QObject> that = this;

      hide();
      if (!_eventLoop.isNull())
         _eventLoop->exit();

      if (that && isVisible())
         e->ignore();
   }
   else
   {
      e->accept();
   }
}
