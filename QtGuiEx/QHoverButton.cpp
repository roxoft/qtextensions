#include "stdafx.h"
#include "QHoverButton.h"

#include <QEvent>
#include <QWidget>

// Initialize static members
qreal QHoverButton::s_globalTransparencyLevel = 1.0;
qreal QHoverButton::s_globalIconTransparencyLevel = 1.0;
QString QHoverButton::s_globalButtonText = "Hover";
QIcon QHoverButton::s_globalButtonIcon = QIcon();

QHoverButton::QHoverButton(QWidget* parent) : QPushButton(parent)
{
   setAttribute(Qt::WA_Hover, true); // Enable hover events
   setFlat(true); // Remove button borders
   setStyleSheet("border: none;"); // Remove border explicitly

   // Set default text and icon if not set
   setText(s_globalButtonText);
   setIcon(s_globalButtonIcon);

   // no focus
   setFocusPolicy(Qt::NoFocus);
}

qreal QHoverButton::transparencyLevel() const
{
   return m_transparencyLevel < 0 ? s_globalTransparencyLevel : m_transparencyLevel;
}

void QHoverButton::setTransparencyLevel(const qreal level)
{
   if (level >= 0.0 && level <= 1.0)
   {
      m_transparencyLevel = level;
      emit transparencyLevelChanged(level);
   }
}

qreal QHoverButton::globalTransparencyLevel()
{
   return s_globalTransparencyLevel;
}

void QHoverButton::setGlobalTransparencyLevel(const qreal level)
{
   if (level >= 0.0 && level <= 1.0)
   {
      s_globalTransparencyLevel = level;
   }
}

qreal QHoverButton::iconTransparency() const
{
   return m_iconTransparency < 0 ? s_globalIconTransparencyLevel : m_iconTransparency;
}

void QHoverButton::setIconTransparency(const qreal level)
{
   if (level >= 0.0 && level <= 1.0)
   {
      m_iconTransparency = level;
      emit iconTransparencyChanged(level);
   }
}

qreal QHoverButton::globalIconTransparencyLevel()
{
   return s_globalIconTransparencyLevel;
}

void QHoverButton::setGlobalIconTransparencyLevel(const qreal level)
{
   if (level >= 0.0 && level <= 1.0)
   {
      s_globalIconTransparencyLevel = level;
   }
}

void QHoverButton::setButtonText(const QString& text)
{
   setText(text);
}

void QHoverButton::setButtonIcon(const QIcon& icon)
{
   setIcon(icon);
}

void QHoverButton::setGlobalButtonText(const QString& text)
{
   s_globalButtonText = text;
}

void QHoverButton::setGlobalButtonIcon(const QIcon& icon)
{
   s_globalButtonIcon = icon;
}

bool QHoverButton::event(QEvent* e)
{
   QWidget* dialog = parentWidget();
   if (!dialog)
   {
      return QPushButton::event(e);
   }

   if (e->type() == QEvent::HoverEnter)
   {
      // Set transparency of the parent dialog using the global or instance-specific level
      dialog->setWindowOpacity(transparencyLevel());
      return true;
   }
   else if (e->type() == QEvent::HoverLeave)
   {
      // Reset the transparency of the parent dialog on hover leave (fully opaque)
      dialog->setWindowOpacity(1.0);
      return true;
   }

   return QPushButton::event(e);
}

void QHoverButton::mousePressEvent(QMouseEvent* e)
{
   e->ignore(); // Ignore click events
}
