#include "stdafx.h"
#include "QStyledItemDelegateEx.h"
#include <QVariant>
#include <QLocale>
#include <Variant.h>
#include <QLocaleEx.h>
#include <QString>
#include <QDecimal.h>
#include <QValue.h>
#include "QTreeViewEx.h"
#include <QHeaderView>
#include <QApplication>

// Get the alignment from the content type if the alignment wasn't specified expclicitely
static void adjustAlignment(Qt::Alignment &alignment, const QModelIndex &index)
{
   // Was the alignment specified explicitely?
   if (!index.data(Qt::TextAlignmentRole).isNull())
      return;

   // Get the content type
   QVariant       qVariant = index.data();
   auto vt = qVariant.userType();

   // Determine the alignment from the content type
   if (vt == QMetaType::QDate || vt == QMetaType::QTime || vt == QMetaType::QDateTime || vt == QMetaType::Bool)
   {
      // Date, time and booleans are centred
      alignment &= ~Qt::AlignHorizontal_Mask;
      alignment |= Qt::AlignHCenter;
   }
   else if (vt == QMetaType::Int || vt == QMetaType::UInt || vt == QMetaType::LongLong || vt == QMetaType::ULongLong)
   {
      // Primitive numbers are right aligned
      alignment &= ~Qt::AlignHorizontal_Mask;
      alignment |= Qt::AlignRight;
   }
   else if (qVariant.canConvert<Variant>())
   {
      const std::type_info &variantType = qVariant.value<Variant>().type();

      if (variantType == typeid(QDateTimeEx) || variantType == typeid(QDateEx) || variantType == typeid(QTimeEx))
      {
         // Date/Time values are centered
         alignment &= ~Qt::AlignHorizontal_Mask;
         alignment |= Qt::AlignHCenter;
      }
      else if (variantType == typeid(QDecimal) || variantType == typeid(QValue))
      {
         // Numeric values are right aligned
         alignment &= ~Qt::AlignHorizontal_Mask;
         alignment |= Qt::AlignRight;
      }
   }
}

QStyledItemDelegateEx::QStyledItemDelegateEx(QObject* parent) : QStyledItemDelegate(parent)
{
}

void QStyledItemDelegateEx::setEditDelegate(int row, int column, QEditItemDelegate* editDelegate)
{
   if (row >= 0)
   {
      if (editDelegate)
         _rowEditDelegateMap.insert(row, editDelegate);
      else
         _rowEditDelegateMap.remove(row);
   }
   else if (column >= 0)
   {
      if (editDelegate)
         _columnEditDelegateMap.insert(column, editDelegate);
      else
         _rowEditDelegateMap.remove(column);
   }
   else
   {
      _editItemDelegate = editDelegate;
   }
}

QEditItemDelegate* QStyledItemDelegateEx::editDelegate(int row, int column) const
{
   if (row >= 0)
      return _rowEditDelegateMap.value(row);
   if (column >= 0)
      return _columnEditDelegateMap.value(column);
   return _editItemDelegate;
}

void QStyledItemDelegateEx::setPaintDelegate(int row, int column, QPaintItemDelegate* paintDelegate)
{
   if (row >= 0)
   {
      if (paintDelegate)
         _rowPaintDelegateMap.insert(row, paintDelegate);
      else
         _rowPaintDelegateMap.remove(row);
   }
   else if (column >= 0)
   {
      if (paintDelegate)
         _columnPaintDelegateMap.insert(column, paintDelegate);
      else
         _rowPaintDelegateMap.remove(column);
   }
   else
   {
      _paintItemDelegate = paintDelegate;
   }
}

QPaintItemDelegate* QStyledItemDelegateEx::paintDelegate(int row, int column) const
{
   if (row >= 0)
      return _rowPaintDelegateMap.value(row);
   if (column >= 0)
      return _columnPaintDelegateMap.value(column);
   return _paintItemDelegate;
}

QRichText QStyledItemDelegateEx::formattedText(const QModelIndex &index, const QLocaleEx& locale) const
{
   QEditItemDelegate* editDelegate = _rowEditDelegateMap.value(index.row());

   if (!editDelegate)
      editDelegate = _columnEditDelegateMap.value(index.column());
   if (!editDelegate)
      editDelegate = _editItemDelegate;

   if (editDelegate)
   {
      editDelegate->_itemDelegate = this;
      return editDelegate->formattedText(index, locale);
   }

   return _formattedText(index, locale);
}

void QStyledItemDelegateEx::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
   // The following code is copied from the function
   // void QStyledItemDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
   // in "Src\qtbase\src\widgets\itemviews\qstyleditemdelegate.cpp".

   Q_ASSERT(index.isValid());

   QStyleOptionViewItemEx opt = option;

   initStyleOption(&opt, index);
   opt.richText = formattedText(index, toQLocaleEx(opt.locale));

   QPaintItemDelegate* paintDelegate = _rowPaintDelegateMap.value(index.row());

   if (!paintDelegate)
      paintDelegate = _columnPaintDelegateMap.value(index.column());
   if (!paintDelegate)
      paintDelegate = _paintItemDelegate;

   if (paintDelegate)
   {
      paintDelegate->_itemDelegate = this;
      paintDelegate->paint(painter, opt, index);
   }
   else
   {
      _paint(painter, opt, index);
   }
}

QSize QStyledItemDelegateEx::sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const
{
   Q_ASSERT(index.isValid());

   QVariant vSizeHint = index.data(Qt::SizeHintRole);

   if (vSizeHint.isValid())
      return qvariant_cast<QSize>(vSizeHint).expandedTo(QSize(0, _minHeight));

   QStyleOptionViewItemEx opt = option;

   initStyleOption(&opt, index);
   opt.richText = formattedText(index, toQLocaleEx(opt.locale));
   if (!opt.rect.isValid() && _heightForWidth)
   {
      // This is necessary to circumvent a bug in QTreeView (Src\qtbase\src\widgets\itemviews\qtreeview.cpp line 2979: Hack to speed up the function)
      const auto treeView = qobject_cast<const QTreeView*>(parent());

      if (treeView && treeView->header())
      {
         opt.rect.setSize(QSize(treeView->header()->sectionSize(index.column()), 300)); // Limit to a reasonable height

         if (index.column() == 0)
         {
            if (treeView->rootIsDecorated())
               opt.rect.adjust(treeView->indentation(), 0, 0, 0);

            auto parent = index.parent();

            while (parent.isValid())
            {
               opt.rect.adjust(treeView->indentation(), 0, 0, 0);
               parent = parent.parent();
            }
         }
      }
   }

   QPaintItemDelegate* paintDelegate = _rowPaintDelegateMap.value(index.row());

   if (!paintDelegate)
      paintDelegate = _columnPaintDelegateMap.value(index.column());
   if (!paintDelegate)
      paintDelegate = _paintItemDelegate;

   if (paintDelegate)
   {
      paintDelegate->_itemDelegate = this;
      return paintDelegate->sizeHint(opt, index).expandedTo(QSize(0, _minHeight));
   }

   return _sizeHint(opt, index).expandedTo(QSize(0, _minHeight));
}

QWidget* QStyledItemDelegateEx::createEditor(QWidget* parent, const QStyleOptionViewItem& option, const QModelIndex& index) const
{
   QEditItemDelegate* editDelegate = _rowEditDelegateMap.value(index.row());

   if (!editDelegate)
      editDelegate = _columnEditDelegateMap.value(index.column());
   if (!editDelegate)
      editDelegate = _editItemDelegate;

   if (editDelegate)
   {
      editDelegate->_itemDelegate = this;
      return editDelegate->createEditor(parent, option, index);
   }

   return QStyledItemDelegate::createEditor(parent, option, index);
}

void QStyledItemDelegateEx::destroyEditor(QWidget* editor, const QModelIndex& index) const
{
   QEditItemDelegate* editDelegate = _rowEditDelegateMap.value(index.row());

   if (!editDelegate)
      editDelegate = _columnEditDelegateMap.value(index.column());
   if (!editDelegate)
      editDelegate = _editItemDelegate;

   if (editDelegate)
   {
      editDelegate->_itemDelegate = this;
      editDelegate->destroyEditor(editor, index);
   }
   else
   {
      QStyledItemDelegate::destroyEditor(editor, index);
   }
}

void QStyledItemDelegateEx::setEditorData(QWidget* editor, const QModelIndex& index) const
{
   QEditItemDelegate* editDelegate = _rowEditDelegateMap.value(index.row());

   if (!editDelegate)
      editDelegate = _columnEditDelegateMap.value(index.column());
   if (!editDelegate)
      editDelegate = _editItemDelegate;

   if (editDelegate)
   {
      editDelegate->_itemDelegate = this;
      editDelegate->setEditorData(editor, index);
   }
   else
   {
      QStyledItemDelegate::setEditorData(editor, index);
   }
}

void QStyledItemDelegateEx::setModelData(QWidget* editor, QAbstractItemModel* model, const QModelIndex& index) const
{
   QEditItemDelegate* editDelegate = _rowEditDelegateMap.value(index.row());

   if (!editDelegate)
      editDelegate = _columnEditDelegateMap.value(index.column());
   if (!editDelegate)
      editDelegate = _editItemDelegate;

   if (editDelegate)
   {
      editDelegate->_itemDelegate = this;
      editDelegate->setModelData(editor, model, index);
   }
   else
   {
      QStyledItemDelegate::setModelData(editor, model, index);
   }
}

void QStyledItemDelegateEx::updateEditorGeometry(QWidget* editor, const QStyleOptionViewItem& option, const QModelIndex& index) const
{
   QEditItemDelegate* editDelegate = _rowEditDelegateMap.value(index.row());

   if (!editDelegate)
      editDelegate = _columnEditDelegateMap.value(index.column());
   if (!editDelegate)
      editDelegate = _editItemDelegate;

   if (editDelegate)
   {
      editDelegate->_itemDelegate = this;
      editDelegate->updateEditorGeometry(editor, option, index);
   }
   else
   {
      QStyledItemDelegate::updateEditorGeometry(editor, option, index);
   }
}

void QStyledItemDelegateEx::initStyleOption(QStyleOptionViewItem *option, const QModelIndex &index) const
{
   QStyledItemDelegate::initStyleOption(option, index);

   // If the foreground color was explicitly set, use it also for highlighted text
   QVariant value = index.data(Qt::ForegroundRole);
   if (value.canConvert<QBrush>())
      option->palette.setBrush(QPalette::HighlightedText, qvariant_cast<QBrush>(value));

   // Recalculate the alignment if _autoalign was enabled
   if (_autoAlign)
      adjustAlignment(option->displayAlignment, index);
}

bool QStyledItemDelegateEx::editorEvent(QEvent* event, QAbstractItemModel* model, const QStyleOptionViewItem& option, const QModelIndex& index)
{
   if (!_checkOnMouseDown)
      return QStyledItemDelegate::editorEvent(event, model, option, index);

   // Copied from src\widgets\itemviews\qstyleditemdelegate.cpp -> editorEvent()

   Q_ASSERT(event);
   Q_ASSERT(model);

   // make sure that the item is checkable
   Qt::ItemFlags flags = model->flags(index);

   if (!(flags & Qt::ItemIsUserCheckable) || !(option.state & QStyle::State_Enabled) || !(flags & Qt::ItemIsEnabled))
   {
      return false;
   }

   // make sure that we have a check state
   QVariant value = index.data(Qt::CheckStateRole);

   if (!value.isValid())
   {
      return false;
   }

   // make sure that we have the right event type
   if (event->type() == QEvent::KeyPress)
   {
      if (static_cast<QKeyEvent*>(event)->key() != Qt::Key_Space && static_cast<QKeyEvent*>(event)->key() != Qt::Key_Select)
      {
         return false;
      }
   }
   else if (event->type() == QEvent::MouseButtonPress)
   {
      QStyleOptionViewItem viewOpt(option);

      initStyleOption(&viewOpt, index);

      const QWidget *widget = option.widget;
      QStyle *style = widget ? widget->style() : QApplication::style();

      const auto checkRect = style->subElementRect(QStyle::SE_ItemViewItemCheckIndicator, &viewOpt, widget);
      const auto me = static_cast<QMouseEvent*>(event);

      if (me->button() != Qt::LeftButton || !checkRect.contains(me->pos()))
         return false;
   }
   else
   {
      return false;
   }

   // Toggle the check mark

   auto checkState = static_cast<Qt::CheckState>(value.toInt());

   if (flags & Qt::ItemIsUserTristate)
      checkState = ((Qt::CheckState)((checkState + 1) % 3));
   else
      checkState = (checkState == Qt::Checked) ? Qt::Unchecked : Qt::Checked;

   return model->setData(index, checkState, Qt::CheckStateRole);
}

QRichText QStyledItemDelegateEx::_formattedText(const QModelIndex &index, const QLocaleEx& locale)
{
   QRichText richText;
   Variant value(index.data());

   if (value.type() == typeid(QRichText))
      richText = value.to<QRichText>(nullptr, locale);
   else
      richText = value.toString(locale);

   // convert new lines into line separators
   richText.replace(QLatin1Char('\n'), QChar::LineSeparator);

   return richText;
}

void QStyledItemDelegateEx::_paint(QPainter *painter, const QStyleOptionViewItemEx &option, const QModelIndex &index) const
{
   const QWidget  *widget = option.widget;
   QStyle         *style = widget ? widget->style() : QApplication::style();

   style->drawControl(QStyle::CE_ItemViewItem, &option, painter, widget);
}

QSize QStyledItemDelegateEx::_sizeHint(const QStyleOptionViewItemEx &option, const QModelIndex &index) const
{
   const QWidget  *widget = option.widget;
   QStyle         *style = widget ? widget->style() : QApplication::style();

   return style->sizeFromContents(QStyle::CT_ItemViewItem, &option, QSize(), widget);
}

QString QStyledItemDelegateEx::displayText(const QVariant& value, const QLocale& locale) const
{
   auto text = Variant(value).toString(toQLocaleEx(locale));

   // convert new lines into line separators
   text.replace(QLatin1Char('\n'), QChar::LineSeparator);

   return text;
}
