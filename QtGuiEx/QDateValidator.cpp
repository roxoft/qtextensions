#include "stdafx.h"
#include "QDateValidator.h"

//
// Helper functions
//
static int fullYear(int partialYear)
{
   if (partialYear < 100)
   {
      int year = QDate::currentDate().year();

      year -= partialYear;
      year += 50;
      year /= 100;
      year *= 100;
      year += partialYear;

      return year;
   }

   return partialYear;
}


//
// class QDateValidator::Section
//

int QDateValidator::Section::toNumber(const QString& text) const
{
   int number = 0;

   // Begin and end of the section
   int pos = begin;
   int end = this->endOfContent(text);

   while (pos < end && text[pos].isDigit())
   {
      number *= 10;
      number += text[pos].digitValue();
      pos++;
   }

   return number;
}

void QDateValidator::Section::postInit()
{
   if (subType == 1)
   {
      if (type == QLatin1Char('y'))
         subType = 2;
      isNumeric = true;
      maxLength = 2;
   }
   else if (subType == 2)
   {
      isNumeric = true;
      maxLength = 2;
   }
   else if (subType == 3)
   {
      if (type == QLatin1Char('y'))
      {
         subType = 4;
         isNumeric = true;
         maxLength = 4;
      }
      else
      {
         isNumeric = false;
         maxLength = 3;
      }
   }
   else if (subType == 4)
   {
      if (type == QLatin1Char('y'))
      {
         isNumeric = true;
         maxLength = 4;
      }
      else
      {
         isNumeric = false;
         maxLength = 0xFFFF;
      }
   }

   if (defaultValue.isEmpty())
      defaultValue = QDate::currentDate().toString(QString(subType, type));

   if (_next)
      _next->postInit();
}

bool QDateValidator::Section::fixup(QString& text, int& cursorPos) const
{
   if (isNumeric)
   {
      // Numeric value of the section
      int number = toNumber(text);

      // Make the section value valid (independant of other sections)
      if (type == QLatin1Char('d'))
      {
         if (number < 1)
            number = 1;
         else if (number > 31)
            number = 31;
      }
      else if (type == QLatin1Char('M'))
      {
         if (number < 1)
            number = 1;
         else if (number > 12)
            number = 12;
      }
      else if (type == QLatin1Char('y'))
      {
         if (subType == 2)
         {
            if (number > 99)
               number %= 100;
         }
         else if (subType == 4)
         {
            if (number > 9999)
               number = 9999;
            number = fullYear(number);
         }
      }

      // Convert the value into section text
      QString sectionText = QString::number(number);

      if (sectionText.length() < subType)
         sectionText.prepend(QString(subType - sectionText.length(), _zeroDigit));

      // Set the corrected section text
      int end = this->endOfContent(text);

      if (text.mid(begin, end - begin) != sectionText)
      {
         if (cursorPos >= end)
         {
            cursorPos -= end - begin - sectionText.length();
         }
         text.replace(begin, end - begin, sectionText);
         return true;
      }
   }

   return false;
}

void QDateValidator::Section::appendDefault(QString& text)
{
   text.append(defaultValue);
   text.append(delimiter);
}

//
// class QDateValidator
//
QDateValidator::QDateValidator(QObject *parent)
   : QValidator(parent)
{
   QChar zeroDigit = u'0';

   _dateFormat = "dd.MM.yyyy";

   // Create the sections by parsing the date format (set type, subType and delimiter)
   Section* section = nullptr;
   bool     isLiteral = false;

   for (int charPos = 0; charPos < _dateFormat.length(); charPos++)
   {
      QChar qchar(_dateFormat[charPos]);

      if (isLiteral)
      {
         if (qchar == QLatin1Char('\''))
            isLiteral = false;
         else
            section->delimiter += qchar;
      }
      else if (section && qchar == section->type)
      {
         if (section->subType < 4)
            section->subType++;
      }
      else
      {
         // A section always starts with a section type
         if (qchar == QLatin1Char('d') || qchar == QLatin1Char('M') || qchar == QLatin1Char('y'))
         {
            if (section == nullptr)
            {
               _firstSection = new Section(zeroDigit);
               section = _firstSection;
            }
            else
            {
               section->setNext(new Section(zeroDigit));
               section = section->next();
            }
            section->type = qchar;
            section->subType++;
         }
         else if (qchar == QLatin1Char('\''))
         {
            isLiteral = true;
         }
         else
         {
            section->delimiter += qchar;
         }
      }
   }

   if (_firstSection)
      _firstSection->postInit(); // Set the other attributes
}

QValidator::State QDateValidator::validate(QString& input, int& pos) const
{
   if (_firstSection == nullptr)
      return QValidator::Acceptable;

   Section* section = const_cast<Section*>(_firstSection);

   if (input.isEmpty())
   {
      while (section)
      {
         section->begin = 0;
         section = section->next();
      }
      return QValidator::Acceptable;
   }

   // Remove invalid characters and update the sections begin
   int charPos = 0;

   while (charPos < input.length())
   {
      QChar qchar(input[charPos]);

      if (  ((section->isNumeric && qchar.isDigit())
            || (!section->isNumeric && qchar.isLetter()))
          && charPos - section->begin < section->maxLength)
      {
         // Character is part of the current section
         charPos++;
      }
      else if (charPos + section->delimiter.length() <= input.length() // Assert midRef() parameters are valid
               && input.mid(charPos, section->delimiter.length()) == section->delimiter)
      {
         // The character is the begin of the section delimiter
         charPos += section->delimiter.length();
         // Move to the next section
         section = section->next();
         // Leave if there are no more sections
         if (section == nullptr)
         {
            input.resize(charPos);
            if (pos > charPos)
               pos = charPos;
            break;
         }
         // Set the beginning of the section
         section->begin = charPos;
      }
      else
      {
         // Invalid character
         input.remove(charPos, 1);
         if (pos > charPos)
            pos--;
      }
   }
   // Terminate the section
   if (section && charPos != section->begin)
   {
      input.append(section->delimiter);
      charPos += section->delimiter.length();
      section = section->next();
      if (section)
         section->begin = charPos;
   }

   // Append missing sections
   while (section)
   {
      section->appendDefault(input);
      section = section->next();
      if (section)
         section->begin = input.length();
   }

   return QValidator::Acceptable;
}

bool QDateValidator::fixupDate(QString& input)
{
   // Always fixup beginning with the year, then the month and finally the day
   int year = findSection(QLatin1Char('y'))->toNumber(input);
   int month = findSection(QLatin1Char('M'))->toNumber(input);
   int day = findSection(QLatin1Char('d'))->toNumber(input);

   QDate date(year, month, 1);

   if (day > date.daysInMonth())
   {
      Section* section = findSection(QLatin1Char('d'));

      day = date.daysInMonth();
      input.replace(section->begin, section->endOfContent(input) - section->begin, QString::number(day));
      return true;
   }

   return false;
}

QDateValidator::Section* QDateValidator::sectionFromPos(int pos)
{
   Section* section = _firstSection;
   Section* prevSection = nullptr;

   while (section)
   {
      if (pos < section->begin)
         break;
      prevSection = section;
      section = section->next();
   }
   return prevSection;
}

QDate QDateValidator::toDate(const QString& input) const
{
   int year = findSection(QLatin1Char('y'))->toNumber(input);
   int month = findSection(QLatin1Char('M'))->toNumber(input);
   int day = findSection(QLatin1Char('d'))->toNumber(input);

   return QDate(year, month, day);
}

QDateValidator::Section* QDateValidator::findSection(QChar type)
{
   Section* section = _firstSection;

   while (section && section->type != type)
      section = section->next();

   return section;
}

const QDateValidator::Section* QDateValidator::findSection(QChar type) const
{
   const Section* section = _firstSection;

   while (section && section->type != type)
      section = section->next();

   return section;
}
