#include "stdafx.h"
#include "QCompleterEx.h"
#include "QSizeGripEx.h"
#include <QWidget>
#include <QLabel>
#include <QEvent>
#include <QKeyEvent>
#include <QMouseEvent>
#include <QAbstractItemView>
#include <QScrollBar>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QRect>
#include <QSize>
#include <QApplication>
#include <QAbstractItemModel>
#include "QSelectorPopupListView.h"
#include <QHeaderView>
#include <HtStandardNode.h>

static QPoint globalPosition(QMouseEvent* mouseEvent)
{
#if QT_VERSION_MAJOR < 6
   return mouseEvent->globalPos();
#else
   return mouseEvent->globalPosition().toPoint();
#endif
}

class QTGUIEX_EXPORT QCompleterModelWrapper : public QCompleterDataSource
{
public:
   QCompleterModelWrapper(QAbstractItemModel *model, QObject *parent = nullptr) : QCompleterDataSource(parent), _model(model)
   {
      if (!_model->parent())
         _model->setParent(this);
   }
   virtual ~QCompleterModelWrapper() {}

public:
   QAbstractItemModel *model() override { return _model; }
   const QAbstractItemModel *model() const override { return _model; }

protected:
   QAbstractItemModel *_model;
};

////////////////////////////////
// class QCompleterDataSource //
////////////////////////////////

bool QCompleterDataSource::filter(const QString &filterText, int filterColumn)
{
   emit dataReady(filterText, filterColumn);
   return true;
}

void QCompleterDataSource::stopFilter()
{
}

bool QCompleterDataSource::waitForModelReady(int)
{
   return true;
}

QString QCompleterDataSource::text(const QModelIndex &index) const
{
   return index.isValid() ? toQString(index.data()) : QString();
}

QModelIndex QCompleterDataSource::search(const QString& text, int column, bool caseSensitive) const
{
   const QAbstractItemModel *m = this->model();

   if (m && m->rowCount() > 0)
   {
      QModelIndexList   result;
      Qt::MatchFlags    matchFlags = Qt::MatchFixedString;

      if (caseSensitive)
         matchFlags |= Qt::MatchCaseSensitive;

      result = m->match(m->index(0, column), Qt::DisplayRole, text, 1, matchFlags);

      if (!result.isEmpty())
         return result.first();
   }

   return QModelIndex();
}

QModelIndex QCompleterDataSource::find(const QModelIndex& start, int role, const Variant& value, Qt::MatchFlags flags) const
{
   const QAbstractItemModel *m = this->model();

   if (m && m->rowCount() > 0)
   {
      auto result = m->match(start, role, value, 1, flags);

      if (!result.isEmpty())
         return result.first();
   }

   return {};
}

QModelIndex QCompleterDataSource::bestMatch(const QString& text, int column) const
{
   QModelIndex result;
   const QAbstractItemModel *m = this->model();

   if (!m || !m->columnCount())
      return result;

   auto row = 0;
   auto col = 0;
   auto matchIndicator = 0;

   while (row < m->rowCount())
   {
      auto other = toQString(m->data(m->index(row, col))).trimmed();
      auto indicator = getCorrelationIndicator(text, other);

      if (col == column)
         indicator++;

      if (matchIndicator < indicator)
      {
         result = m->index(row, col);
         matchIndicator = indicator;
      }

      col++;
      if (col == m->columnCount())
      {
         col = 0;
         row++;
      }
   }

   return result;
}

void QCompleterDataSource::addSeparator()
{
   insertSeparator(model()->rowCount());
}

bool QCompleterDataSource::isSeparator(const QModelIndex &index) const
{
   return isPopupSeparator(index);
}

bool QCompleterDataSource::isSeparator(int row)
{
   return isPopupSeparator(model()->index(row, 0));
}

void QCompleterDataSource::setSeparator(const QModelIndex &index)
{
   model()->setData(index, QString::fromLatin1("separator"), Qt::AccessibleDescriptionRole);
   if (auto m = qobject_cast<QStandardItemModel*>(model()))
   {
      if (QStandardItem *item = m->itemFromIndex(index))
         item->setFlags(item->flags() & ~(Qt::ItemIsSelectable | Qt::ItemIsEnabled));
   }
   else
   {
      model()->setData(index, index.data(Qt::ItemFlagsRole).toInt() & ~(Qt::ItemIsSelectable | Qt::ItemIsEnabled), Qt::ItemFlagsRole);
   }
}

void QCompleterDataSource::insertSeparator(int row)
{
   row = qBound(0, row, model()->rowCount());
   model()->insertRow(row);
   for (int column = 0; column < model()->columnCount(); ++column)
      setSeparator(model()->index(row, column));
}

//////////////////////////////////
// class QCompleterHtDataSource //
//////////////////////////////////

QCompleterHtDataSource::QCompleterHtDataSource(QObject *parent) : QCompleterDataSource(parent)
{
}

QModelIndex QCompleterHtDataSource::search(const QString& text, int column, bool caseSensitive) const
{
   Qt::MatchFlags matchFlags = Qt::MatchFixedString;

   if (caseSensitive)
      matchFlags |= Qt::MatchCaseSensitive;

   int row = _model.rootNode()->findIndex(column, text, 0, matchFlags, Qt::DisplayRole);

   if (row == -1)
      return QModelIndex();

   return _model.index(row, column);
}

QModelIndex QCompleterHtDataSource::find(const QModelIndex& start, int role, const Variant& value, Qt::MatchFlags flags) const
{
   if (!start.isValid())
      return QModelIndex();

   const HtNode *parent = _model.itemFromIndex(start.parent());

   if (parent == nullptr)
      return QModelIndex();

   int row = parent->findIndex(start.column(), value, start.row(), flags, role);

   if (row == -1)
      return QModelIndex();

   return _model.index(row, start.column());
}

HtNode *QCompleterHtDataSource::item(int row)
{
   if (row < 0)
      return nullptr;
   return _model.rootNode()->child(row);
}

const HtNode *QCompleterHtDataSource::item(int row) const
{
   if (row < 0)
      return nullptr;
   return _model.rootNode()->child(row);
}

////////////////////////
// class QCompleterEx //
////////////////////////

QCompleterEx::QCompleterEx(QWidget* widget, QCompleterDataSource* dataSource, int column, QObject* parent)
   : QObject(parent),
   _widget(widget),
   _column(column)
{
   setDataSource(dataSource);
}

QCompleterEx::~QCompleterEx()
{
   delete _popup;
}

QCompleterDataSource* QCompleterEx::dataSource()
{
   if (_dataSource == nullptr)
      setDataSource(new QCompleterHtDataSource(this));
   return _dataSource;
}

void QCompleterEx::setDataSource(QCompleterDataSource* source)
{
   if (source == _dataSource)
      return;

   QString currentText;

   if (_currentIndex.isValid())
   {
      currentText = _dataSource->text(_currentIndex);
      _currentIndex = QModelIndex();
      emit currentIndexChanged(_currentIndex);
   }
   if (_dataSource)
   {
      if (_popup)
      {
         delete _popup;
         _popup = nullptr;
         _statusBarLabel = nullptr;
         _matchListView = nullptr;
         _rowCount = -1;
         _columnCount = -1;
      }

      disconnectModel();
   }

   _dataSource = source;

   if (_dataSource)
   {
      if (_dataSource->parent() == nullptr)
         _dataSource->setParent(this);

      connectModel();

      _currentIndex = _dataSource->search(currentText, _column, true);
      if (!_currentIndex.isValid())
         _currentIndex = _dataSource->search(currentText, _column, false);
      if (_defaultItem >= 0 && !_currentIndex.isValid())
         _currentIndex = _dataSource->model()->index(_defaultItem, _column);
   }

   emit currentTextChanged(false);
   if (_currentIndex.isValid())
      emit currentIndexChanged(_currentIndex);
}

QAbstractItemModel* QCompleterEx::model()
{
   if (_dataSource == nullptr)
      setDataSource(new QCompleterHtDataSource(this));
   return _dataSource->model();
}

void QCompleterEx::setModel(QAbstractItemModel *model)
{
   QCompleterModelWrapper* wrapper = nullptr;

   if (model)
      wrapper = new QCompleterModelWrapper(model, this);
   setDataSource(wrapper);
}

QModelIndex QCompleterEx::addItem(const QString &text, const Variant &userData)
{
   return addItem(QIcon(), text, userData);
}

QModelIndex QCompleterEx::addItem(const QIcon &icon, const QString &text, const Variant &userData)
{
   QAbstractItemModel   *m = model();
   const int            currentRow = _currentIndex.row();
   int                  row = -1;

   switch (_insertPolicy)
   {
   case NoInsert:
      row = -1;
      break;
   case InsertAtTop:
      row = 0;
      break;
   case InsertAtCurrent:
      row = currentRow;
      break;
   case InsertAtBottom:
      row = m->rowCount();
      break;
   case InsertAfterCurrent:
      row = (currentRow != -1) ? currentRow + 1 : 0;
      break;
   case InsertBeforeCurrent:
      row = (currentRow != -1) ? currentRow : m->rowCount();
      break;
   case InsertAlphabetically:
      row = 0;
      while (row < m->rowCount() && toQString(m->index(row, _column).data()).compare(text, Qt::CaseInsensitive) <= 0)
         row++;
      break;
   }

   return insertItem(row, icon, text, userData);
}

void QCompleterEx::addItems(const QStringList &texts)
{
   if (_insertPolicy == InsertAlphabetically)
   {
      foreach(const QString &text, texts)
         addItem(text);
      return;
   }

   int row = -1;
   int currentRow = _currentIndex.row();

   switch (_insertPolicy)
   {
   case NoInsert:
      row = -1;
      break;
   case InsertAtTop:
      row = 0;
      break;
   case InsertAtCurrent:
      row = currentRow;
      break;
   case InsertAtBottom:
      row = model()->rowCount();
      break;
   case InsertAfterCurrent:
      row = (currentRow != -1) ? currentRow + 1 : 0;
      break;
   case InsertBeforeCurrent:
      row = (currentRow != -1) ? currentRow : model()->rowCount();
      break;
   case InsertAlphabetically:
      row = -1;
      break;
   }
   insertItems(row, texts);
}

void QCompleterEx::addSeparator()
{
   if (_dataSource)
      _dataSource->insertSeparator(_dataSource->model()->rowCount());
}

QModelIndex QCompleterEx::addText(const QString& text)
{
   QString newText = text.simplified();

   if (newText.isEmpty())
      return {};

   // Prevent setting the text of the current item accidentally
   if (!_currentIndex.isValid() || newText != _dataSource->text(_currentIndex))
      setCurrentIndex(addItem(newText));

   return _currentIndex;
}

QModelIndex QCompleterEx::insertItem(int row, const QString &text, const Variant &userData)
{
   return insertItem(row, QIcon(), text, userData);
}

QModelIndex QCompleterEx::insertItem(int row, const QIcon &icon, const QString &text, const Variant &userData)
{
   QModelIndex index;

   if (row < 0)
      return index;

   QAbstractItemModel   *m = model();
   int                  currentRow = -1;
   int                  currentColumn = -1;

   if (row > m->rowCount())
      row = m->rowCount();

   if (_insertPolicy != InsertAtCurrent || row == m->rowCount())
   {
      currentRow = _currentIndex.row();
      currentColumn = _currentIndex.column();

      while (m->columnCount() <= _column)
         m->insertColumn(m->columnCount());
      m->insertRow(row);
      if (row <= currentRow)
         currentRow++;
   }

   index = m->index(row, _column);
   m->setData(index, text);
   m->setData(index, userData, Qt::UserRole);
   if (!icon.isNull())
      m->setData(index, icon, Qt::DecorationRole);

   if (currentRow != -1)
      setCurrentIndex(m->index(currentRow, currentColumn));

   return index;
}

void QCompleterEx::insertItems(int row, const QStringList &list)
{
   if (row < 0)
      return;

   foreach(const QString &text, list)
      insertItem(row++, text);
}

void QCompleterEx::insertSeparator(int row)
{
   if (_dataSource)
      _dataSource->insertSeparator(row);
}

void QCompleterEx::removeItem(int row)
{
   QAbstractItemModel *m = model();

   if (row >= 0 && row < m->rowCount())
   {
      int currentRow = _currentIndex.row();
      int currentColumn = _currentIndex.column();

      if (currentRow == row)
      {
         setCurrentIndex(QModelIndex());
         currentRow = -1;
      }

      m->removeRow(row);
      if (row < currentRow)
         currentRow--;
      if (currentRow != -1)
         setCurrentIndex(m->index(currentRow, currentColumn));
   }
}

Variant QCompleterEx::itemData(int row, int role) const
{
   QAbstractItemModel *m = model();

   if (row >= 0 && row < m->rowCount())
      return m->data(m->index(row, _column), role);
   return Variant();
}

void QCompleterEx::setItemData(int row, const Variant &value, int role)
{
   QAbstractItemModel *m = model();

   if (row >= 0 && row < m->rowCount())
   {
      m->setData(m->index(row, _column), value, role);
   }
}

QIcon QCompleterEx::itemIcon(int row) const
{
   QAbstractItemModel *m = model();

   if (row >= 0 && row < m->rowCount())
      return m->data(m->index(row, _column), Qt::DecorationRole).value<QIcon>();
   return QIcon();
}

void QCompleterEx::setItemIcon(int row, const QIcon &icon)
{
   QAbstractItemModel *m = model();

   if (row >= 0 && row < m->rowCount())
   {
      m->setData(m->index(row, _column), icon, Qt::DecorationRole);
   }
}

QString QCompleterEx::itemText(int row) const
{
   QAbstractItemModel *m = model();

   if (row >= 0 && row < m->rowCount())
      return toQString(m->data(m->index(row, _column), Qt::DisplayRole));
   return QString();
}

void QCompleterEx::setItemText(int row, const QString &text)
{
   QAbstractItemModel *m = model();

   if (row >= 0 && row < m->rowCount())
   {
      m->setData(m->index(row, _column), text);
   }
}

QModelIndex QCompleterEx::find(const Variant& value, int role) const
{
   if (_dataSource == nullptr)
      return{};

   const Qt::MatchFlags matchFlags = (role == Qt::DisplayRole) ? (Qt::MatchFixedString | Qt::MatchCaseSensitive) : Qt::MatchExactly;

   return _dataSource->find(_dataSource->model()->index(0, _column), role, value, matchFlags);
}

QModelIndex QCompleterEx::bestMatch(const QString& text) const
{
   if (!_dataSource)
      return {};

   if (_dataSource->text(_currentIndex) == text)
      return _currentIndex;

   return _dataSource->bestMatch(text, _column);
}

void QCompleterEx::showSelectionList(const QString& text, const QPoint& topLeft, int width)
{
   if (_dataSource == nullptr)
      return;

   if (text.isEmpty() && _dataSource->model()->rowCount() == 0)
   {
      // There is a good chance that a filter model wasn't initialized yet
      if (!_dataSource->filter(QString(), _column)) // Always emits dataReady
         waitForModelReady();
   }

   showSelectionList(bestMatch(text), topLeft, width);
}

void QCompleterEx::showSelectionList(const QModelIndex& index, const QPoint& topLeft, int width)
{
   if (_dataSource == nullptr)
      return;

   if (_dataSource->model()->rowCount() == 0)
      return;

   showPopup(topLeft, width);

   if (_matchListView && index.isValid())
      _matchListView->select(index);
}

void QCompleterEx::hideSelectionList()
{
   if (_popup && _popup->isVisible())
      _popup->hide();
}

bool QCompleterEx::updateSelectList(const QString& text)
{
   if (_currentIndex.isValid())
   {
      _currentIndex = QModelIndex();
      emit currentIndexChanged(_currentIndex);
   }

   return dataSource()->filter(text, _column); // Always emits dataReady
}

void QCompleterEx::setCurrentText(const QString& text)
{
   if (!_dataSource)
      return;

   waitForModelReady();

   QModelIndex currentIndex;

   if (_dataSource->text(_currentIndex) == text)
   {
      currentIndex = _currentIndex;
   }
   else if (!_isFreeText)
   {
      currentIndex = bestMatch(text);
   }
   else
   {
      currentIndex = _dataSource->search(text, _column, true);
      if (!currentIndex.isValid())
         currentIndex = _dataSource->search(text, _column, false);
   }

   if (currentIndex.isValid() && currentIndex.column() != _column)
      currentIndex = currentIndex.sibling(currentIndex.row(), _column);

   if (_defaultItem >= 0 && !currentIndex.isValid())
      currentIndex = _dataSource->model()->index(_defaultItem, _column);

   if (currentIndex != _currentIndex)
   {
      _currentIndex = currentIndex;
      emit currentTextChanged(false);
      emit currentIndexChanged(_currentIndex);
   }
}

bool QCompleterEx::acceptSelectedItem()
{
   waitForModelReady();

   if (!_popup || !_popup->isVisible())
      return false;

   int row = _matchListView->selectedRow();

   _popup->hide();

   if (row == -1)
      return false;

   setCurrentIndex(model()->index(row, _column));
   return true;
}

bool QCompleterEx::isPopupVisible() const
{
   return _popup && _popup->isVisible();
}

void QCompleterEx::clear()
{
   if (_dataSource)
   {
      QAbstractItemModel *m = _dataSource->model();

      setCurrentIndex(QModelIndex());
      m->removeRows(0, m->rowCount());
   }
}

void QCompleterEx::setCurrentIndex(const QModelIndex& index)
{
   if (_dataSource)
   {
      _dataSource->stopFilter();

      if (index != _currentIndex)
      {
         _currentIndex = index;
         emit currentTextChanged(true);
         emit currentIndexChanged(_currentIndex);
      }
   }
}

void QCompleterEx::setCurrentRow(int row)
{
   if (row == _currentIndex.row())
      return;

   QModelIndex newIndex;

   if (_dataSource && row >= 0)
      newIndex = _dataSource->model()->index(row, _column);

   setCurrentIndex(newIndex);
}

bool QCompleterEx::eventFilter(QObject *o, QEvent *e)
{
   if (o == _popup) // This implies that the popup is visible
   {
      if (e->type() == QEvent::KeyPress)
      {
         const auto keyEvent = static_cast<QKeyEvent*>(e);
         const auto key = keyEvent->key();

         if (key == Qt::Key_Escape)
            return QObject::eventFilter(o, e);

         if (key == Qt::Key_Tab
            || (key == Qt::Key_F4 && (keyEvent->modifiers() & Qt::AltModifier)) // This key combination is never received ???
            || key == Qt::Key_Backtab)
         {
            _popup->hide();
         }
         else if (key == Qt::Key_Up)
         {
            _matchListView->moveSelection(-1);
            return true;
         }
         else if (key == Qt::Key_Down)
         {
            _matchListView->moveSelection(1);
            return true;
         }
         else if (key == Qt::Key_End || key == Qt::Key_Home)
         {
            if (keyEvent->modifiers() & Qt::ControlModifier)
               return false;
         }
         else if (key == Qt::Key_PageUp || key == Qt::Key_PageDown)
         {
            return false;
         }

         (static_cast<QObject *>(_widget))->event(keyEvent);

         return true;
      }

      if (e->type() == QEvent::MouseButtonPress)
      {
         const auto mouseEvent = static_cast<QMouseEvent*>(e);

         if (!_popup->rect().contains(mouseEvent->pos()))
         {
            if (_widget->rect().contains(_widget->mapFromGlobal(globalPosition(mouseEvent))))
            {
               (static_cast<QObject *>(_widget))->event(mouseEvent);
               return true;
            }
            _popup->setAttribute(Qt::WA_NoMouseReplay, true); // Prevent combo box from showing again
         }
      }
      else if (e->type() == QEvent::MouseButtonRelease)
      {
         const auto mouseEvent = static_cast<QMouseEvent*>(e);

         if (!_popup->rect().contains(mouseEvent->pos()))
         {
            if (_widget->rect().contains(_widget->mapFromGlobal(globalPosition(mouseEvent))))
            {
               (static_cast<QObject *>(_widget))->event(mouseEvent);
               return true;
            }
         }
      }
      else if (e->type() == QEvent::MouseButtonDblClick)
      {
         const auto mouseEvent = static_cast<QMouseEvent*>(e);

         if (!_popup->rect().contains(mouseEvent->pos()))
         {
            if (_widget->rect().contains(_widget->mapFromGlobal(globalPosition(mouseEvent))))
            {
               (static_cast<QObject *>(_widget))->event(mouseEvent);
               return true;
            }
         }
      }
      else if (e->type() == QEvent::MouseMove)
      {
         const auto mouseEvent = static_cast<QMouseEvent*>(e);

         if (!_popup->rect().contains(mouseEvent->pos()))
         {
            if (_widget->rect().contains(_widget->mapFromGlobal(globalPosition(mouseEvent))))
            {
               (static_cast<QObject *>(_widget))->event(mouseEvent);
               return true;
            }
         }
      }
   }

   return QObject::eventFilter(o, e);
}

void QCompleterEx::onDataSourceReady(const QString& text, int column)
{
   if (column != _column)
   {
      if (_currentIndex.isValid())
      {
         _currentIndex = QModelIndex();
         emit currentIndexChanged(_currentIndex);
      }

      return;
   }

   emit matchIndexChanged(bestMatch(text)); // May call showSelectionList(bestIndex...
}

void QCompleterEx::onItemSelected(int row)
{
   if (row != -1 && _popup && _popup->isVisible())
   {
      _popup->hide();
      setCurrentIndex(model()->index(row, _column));
   }
}

void QCompleterEx::onModelColumnsInserted(const QModelIndex &, int, int)
{
   updateCurrentIndex(_currentIndex.row());
}

void QCompleterEx::onModelColumnsMoved(const QModelIndex & sourceParent, int, int, const QModelIndex & destinationParent, int)
{
   Q_UNUSED(sourceParent);
   Q_UNUSED(destinationParent);
   Q_ASSERT(sourceParent == destinationParent);

   updateCurrentIndex(_currentIndex.row());
}

void QCompleterEx::onModelColumnsRemoved(const QModelIndex &, int, int)
{
   updateCurrentIndex(_currentIndex.row());
}

void QCompleterEx::onModelDataChanged(const QModelIndex & topLeft, const QModelIndex & bottomRight)
{
   // TODO: find best matching index
   Q_UNUSED(topLeft);
   Q_UNUSED(bottomRight);
}

void QCompleterEx::onModelLayoutChanged()
{
   updateCurrentIndex(-1);
}

void QCompleterEx::onModelModelReset()
{
   updateCurrentIndex(-1);
}

void QCompleterEx::onModelRowsInserted(const QModelIndex & parent, int start, int end)
{
   Q_UNUSED(parent);
   Q_ASSERT(!parent.isValid());

   int row = _currentIndex.row();

   if (row >= start)
      row += end - start + 1;

   updateCurrentIndex(row);
}

void QCompleterEx::onModelRowsMoved(const QModelIndex & sourceParent, int sourceStart, int sourceEnd, const QModelIndex & destinationParent, int destinationRow)
{
   Q_UNUSED(sourceParent);
   Q_UNUSED(destinationParent);
   Q_ASSERT(!sourceParent.isValid() && !destinationParent.isValid());

   int row = _currentIndex.row();

   if (row >= sourceStart && row <= sourceEnd)
      row += destinationRow - sourceStart;
   else
   {
      if (row > sourceEnd)
         row -= sourceEnd - sourceStart + 1;
      if (row >= destinationRow)
         row += sourceEnd - sourceStart + 1;
   }

   updateCurrentIndex(row);
}

void QCompleterEx::onModelRowsRemoved(const QModelIndex &, int start, int end)
{
   int row = _currentIndex.row();

   if (row >= start)
   {
      if (row <= end)
         row = -1;
      else
         row -= end - start + 1;
   }

   updateCurrentIndex(row);
}

void QCompleterEx::connectModel()
{
   connect(_dataSource->model(), SIGNAL(columnsInserted(const QModelIndex &, int, int)), this, SLOT(onModelColumnsInserted(const QModelIndex &, int, int)));
   connect(_dataSource->model(), SIGNAL(columnsMoved(const QModelIndex &, int, int, const QModelIndex &, int)), this, SLOT(onModelColumnsMoved(const QModelIndex &, int, int, const QModelIndex &, int)));
   connect(_dataSource->model(), SIGNAL(columnsRemoved(const QModelIndex &, int, int)), this, SLOT(onModelColumnsRemoved(const QModelIndex &, int, int)));
   connect(_dataSource->model(), SIGNAL(dataChanged(const QModelIndex &, const QModelIndex &)), this, SLOT(onModelDataChanged(const QModelIndex &, const QModelIndex &)));
   connect(_dataSource->model(), SIGNAL(layoutChanged()), this, SLOT(onModelLayoutChanged()));
   connect(_dataSource->model(), SIGNAL(modelReset()), this, SLOT(onModelModelReset()));
   connect(_dataSource->model(), SIGNAL(rowsInserted(const QModelIndex &, int, int)), this, SLOT(onModelRowsInserted(const QModelIndex &, int, int)));
   connect(_dataSource->model(), SIGNAL(rowsMoved(const QModelIndex &, int, int, const QModelIndex &, int)), this, SLOT(onModelRowsMoved(const QModelIndex &, int, int, const QModelIndex &, int)));
   connect(_dataSource->model(), SIGNAL(rowsRemoved(const QModelIndex &, int, int)), this, SLOT(onModelRowsRemoved(const QModelIndex &, int, int)));
   connect(_dataSource, &QCompleterDataSource::dataReady, this, &QCompleterEx::onDataSourceReady);
}

void QCompleterEx::disconnectModel()
{
   disconnect(_dataSource->model(), 0, this, 0);
   disconnect(_dataSource, 0, this, 0);
}

void QCompleterEx::waitForModelReady()
{
   if (_dataSource && !_dataSource->waitForModelReady(100))
   {
      QApplication::setOverrideCursor(Qt::WaitCursor);
      _dataSource->waitForModelReady();
      QApplication::restoreOverrideCursor();
   }
}

void QCompleterEx::updateCurrentIndex(int row)
{
   if (_dataSource == nullptr)
      return;

   QModelIndex newIndex;

   if (row >= 0)
      newIndex = _dataSource->model()->index(row, _column);

   if (_currentIndex != newIndex)
      setCurrentIndex(newIndex);
}

void QCompleterEx::showPopup(QPoint topLeft, int width)
{
   if (_dataSource == nullptr || _widget == nullptr)
      return;

   if (width <= 0)
   {
      topLeft = _widget->parentWidget()->mapToGlobal(_widget->geometry().bottomLeft());
      width = _widget->width();
   }

   QApplication::setOverrideCursor(Qt::WaitCursor);

   // Create popup window
   if (_popup == nullptr)
   {
      // Create the view

      // Create a table
      _matchListView = new QSelectorPopupListView(_sortMode != SortDisabled);

      //_matchListView->setUniformRowHeights(true); // Its problematic with separators
      _matchListView->setModel(_dataSource->model());

      if (_matchListView->header()->count() == 1 && _sortMode != SortEnabled)
         _matchListView->header()->hide();
      else
      {
         _matchListView->setIsContentAutoAlign(true);
         if (_sortMode != SortDisabled)
         {
            _matchListView->setSortingEnabled(true);
            _matchListView->sortByColumn(_column, Qt::AscendingOrder);
         }
      }

      _matchListView->setFocusProxy(_widget);

      resizeColumnsToContents();

      // Connect table signals
      connect(_matchListView, SIGNAL(selected(int)), this, SLOT(onItemSelected(int)));

      // Create size grip
      QHBoxLayout* sizeGripLayout = new QHBoxLayout;

      sizeGripLayout->setContentsMargins(0, 0, 0, 0);
      sizeGripLayout->setSpacing(0);

      _statusBarLabel = new QLabel;

      sizeGripLayout->addWidget(_statusBarLabel, 1);
      sizeGripLayout->addWidget(new QSizeGripEx);

      // Popup layout
      QVBoxLayout* layout = new QVBoxLayout;

      layout->setContentsMargins(0, 0, 0, 0);
      layout->setSpacing(0);
      layout->addWidget(_matchListView);
      layout->addLayout(sizeGripLayout);

      // Popup widget
      _popup = new QWidget(nullptr, Qt::Popup);

      _popup->setFocusPolicy(Qt::NoFocus);
      _popup->setMouseTracking(true);
      _popup->setLayout(layout);
      _popup->installEventFilter(this);
      _popup->setFocusProxy(_widget);

      // Reenable scroll bars
      _matchListView->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
      _matchListView->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
   }

   if (!_matchListView->currentIndex().isValid())
      _matchListView->setCurrentIndex(_matchListView->model()->index(0, 0));

   if (_dataSource->isFilterMode())
   {
      // Create and set the status text
      const auto rowCount = _dataSource->model()->rowCount();

      QString units;

      if (rowCount == 1)
         units = tr("Entry");
      else
         units = tr("Entries");

      QString statusText = eutf8("%1 %2").arg(rowCount).arg(units);

      if (_dataSource->hasMore())
      {
         statusText += QLatin1String(" ");
         statusText += tr("(more available)");
      }
      _statusBarLabel->setText(statusText);
   }

   // Get the screen boundaries
   QRect screen = QApplication::screenAt(topLeft)->geometry();
   int   maxHeight = screen.y() + screen.height() - topLeft.y();
   int   maxWidth = screen.x() + screen.width() - topLeft.x();

   // Recalculate the popup size if the model has changed
   int rowCount = _dataSource->model()->rowCount();
   int columnCount = _dataSource->model()->columnCount();

   QSize popupSize = _popup->size();

   if (_rowCount != rowCount || _columnCount != columnCount)
   {
      auto sizeHint = _matchListView->sizeHint();

      sizeHint.rheight() += _popup->layout()->itemAt(1)->sizeHint().height(); // Space for the size grip

                                                                              // Add scroll bars
      bool horizontalScrollBar = false;

      if (maxWidth < sizeHint.width())
      {
         horizontalScrollBar = true;
         //popupHeight += 13; // Horizontal srcoll bar
         sizeHint.rheight() += _matchListView->horizontalScrollBar()->sizeHint().height(); // Horizontal srcoll bar
      }

      if (maxHeight < sizeHint.height())
      {
         // Add space for a vertical scroll bar
         //popupWidth += 13;
         sizeHint.rwidth() += _matchListView->verticalScrollBar()->sizeHint().width();

         if (!horizontalScrollBar && maxWidth < sizeHint.width())
            sizeHint.rheight() += _matchListView->horizontalScrollBar()->sizeHint().height(); // Horizontal srcoll bar
      }

      // Popup size

      if (_rowCount == -1)
      {
         // Take the originating widget height as minimum height
         popupSize.setHeight(sizeHint.height());
      }
      else
      {
         if (_rowCount < rowCount && popupSize.height() < sizeHint.height())
            popupSize.setHeight(sizeHint.height());
         if (rowCount < _rowCount && sizeHint.height() < popupSize.height())
            popupSize.setHeight(sizeHint.height());
      }

      if (_columnCount == -1)
      {
         // Take the originating widget width as minimum width
         popupSize.setWidth(qMax(width, sizeHint.width()));
      }
      else
      {
         if (_columnCount < columnCount && popupSize.width() < sizeHint.width())
            popupSize.setWidth(sizeHint.width());
         if (columnCount < _columnCount && sizeHint.width() < popupSize.width())
            popupSize.setWidth(sizeHint.width());
      }

      _rowCount = rowCount;
      _columnCount = columnCount;
   }

   // Reduce the popup to screen boundaries
   if (maxHeight < popupSize.height())
      popupSize.setHeight(maxHeight);
   if (maxWidth < popupSize.width())
      popupSize.setWidth(maxWidth);

   _popup->setGeometry(QRect(topLeft, popupSize));

   QApplication::restoreOverrideCursor();

   // Show popup window
   if (!_popup->isVisible())
   {
      //    if (QApplication::isEffectEnabled(Qt::UI_AnimateCombo) && !_alignTo->parentWidget()->testAttribute(Qt::WA_DontShowOnScreen))
      //       qScrollEffect(_popup, QEffects::DownScroll, 300);
      _popup->show();
   }
}

void QCompleterEx::resizeColumnsToContents()
{
   if (_matchListView)
   {
      QHeaderView *headerView = _matchListView->header();

      for (int column = headerView->count(); column--; )
      {
         int columnWidth = qMax(headerView->sectionSizeHint(column), ((QAbstractItemView*)_matchListView)->sizeHintForColumn(column));

         if (column + 1 < headerView->count())
            _matchListView->setColumnWidth(column, columnWidth);
         else
            _matchListView->setMinimumLastColumnSize(columnWidth);
      }
   }
}
