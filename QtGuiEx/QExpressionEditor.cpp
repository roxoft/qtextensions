#include "stdafx.h"
#include "QExpressionEditor.h"
#include <QPushButton>
#include <QLayout>
#include <QMessageBox>

QExpressionEditor::QExpressionEditor(const VarTree& varTree, const ResolverFunctionMap& functionMap, const Tokenizer::Context* context, QWidget *parent)
   : QDialogEx(parent)
{
   setWindowTitle("Expression editor");
   resize(1024, 768);

   _expressionEditor = new QExpressionEditorWidget(varTree, functionMap, context);

   auto pushButtonOk = new QPushButton("&OK");
   auto pushButtonCancel = new QPushButton("&Cancel");
   auto pushButtonHelp = new QPushButton("&Help");

   auto buttonLayout = new QHBoxLayout();

   buttonLayout->addWidget(pushButtonOk);
   buttonLayout->addStretch();
   buttonLayout->addWidget(pushButtonCancel);
   buttonLayout->addWidget(pushButtonHelp);

   auto dialogLayout = new QVBoxLayout();

   dialogLayout->addWidget(_expressionEditor);
   dialogLayout->addLayout(buttonLayout);

   setLayout(dialogLayout);

   connect(pushButtonOk, &QPushButton::clicked, this, &QExpressionEditor::on_pushButtonOk_clicked);
   connect(pushButtonCancel, &QPushButton::clicked, this, &QExpressionEditor::on_pushButtonCancel_clicked);
   connect(pushButtonHelp, &QPushButton::clicked, this, &QExpressionEditor::on_pushButtonHelp_clicked);
}

QExpressionEditor::~QExpressionEditor()
{
   saveGeometrySettings("WindowGeometry");
   _expressionEditor->saveSettings(dlgSettings());
}

void QExpressionEditor::setSettingsHandler(QSettingsExBase* settings)
{
   if (!settings)
      return;

   setDlgSettings(settings);
   loadGeometrySettings("WindowGeometry");
   _expressionEditor->loadSettings(dlgSettings());
}

void QExpressionEditor::expandFirstLevel()
{
   _expressionEditor->expandFirstLevel();
}

void QExpressionEditor::setEditHandler(AbstractEditHandler* editHandler)
{
   _expressionEditor->setEditHandler(editHandler);
}

AbstractEditHandler* QExpressionEditor::editHandler() const
{
   return _expressionEditor->editHandler();
}

void QExpressionEditor::setExpression(const QString &expression)
{
   _expressionEditor->setExpression(expression);
}

QString QExpressionEditor::expression() const
{
   return _expressionEditor->expression();
}

void QExpressionEditor::keyPressEvent(QKeyEvent* event)
{
   if (event->key() == Qt::Key_Escape)
   {
      if (!_expressionEditor->hasExpressionChanged() || QMessageBox::question(this, windowTitle(), tr("Discard changes?")) == QMessageBox::Yes)
         reject();
      return;
   }

   QDialogEx::keyPressEvent(event);
}

void QExpressionEditor::closeEvent(QCloseEvent* event)
{
   if (!_expressionEditor->hasExpressionChanged() || QMessageBox::question(this, windowTitle(), tr("Discard changes?")) == QMessageBox::Yes)
      event->accept();
   else
      event->ignore();
}

void QExpressionEditor::on_pushButtonOk_clicked()
{
   accept();
}

void QExpressionEditor::on_pushButtonCancel_clicked()
{
   reject();
}

void QExpressionEditor::on_pushButtonHelp_clicked()
{
   emit helpRequest();
}
