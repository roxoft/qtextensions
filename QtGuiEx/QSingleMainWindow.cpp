#include "stdafx.h"
#include "QSingleMainWindow.h"

#define WIN32_LEAN_AND_MEAN
#include <Windows.h>

struct WindowSearchInfo
{
   HWND     hOurWnd;
   QString  windowTitle;
   HWND     hOtherWnd;
};

static BOOL CALLBACK EnumWindowsProc(HWND hwnd, LPARAM lParam)
{
   WindowSearchInfo *windowInfo = (WindowSearchInfo*)lParam;

   if (hwnd != windowInfo->hOurWnd)
   {
      wchar_t windowText[256];

      if (GetWindowText(hwnd, windowText, 256))
      {
         if (windowInfo->windowTitle == QString::fromUtf16((const char16_t *)windowText))
         {
            wchar_t ourClassName[256];
            wchar_t theirClassName[256];

            GetClassName(windowInfo->hOurWnd, ourClassName, 256);
            GetClassName(hwnd, theirClassName, 256);

            if (wcscmp(ourClassName, theirClassName) == 0)
            {
               windowInfo->hOtherWnd = hwnd;
               return FALSE;
            }
         }
      }
   }
   return TRUE;
}

bool QSingleMainWindow::sendCommand(int command)
{
   WindowSearchInfo searchInfo;

   searchInfo.hOurWnd = (HWND)winId();
   searchInfo.windowTitle = windowTitle();
   searchInfo.hOtherWnd = NULL;

   EnumWindows(EnumWindowsProc, (LPARAM)&searchInfo);

   if (searchInfo.hOtherWnd)
   {
      if (!PostMessageW(searchInfo.hOtherWnd, WM_APP + 9, (WPARAM)command, 0))
         throw tr("Command could not be send");
      return true;
   }

   return false;
}

#if QT_VERSION >= QT_VERSION_CHECK(6,0,0)
bool QSingleMainWindow::nativeEvent(const QByteArray &, void *message, qintptr* result)
#elif QT_VERSION >= QT_VERSION_CHECK(5,0,0)
bool QSingleMainWindow::nativeEvent(const QByteArray &, void *message, long *result)
#else
bool QSingleMainWindow::winEvent(MSG *message, long *result)
#endif
{
   MSG *msg = (MSG*)message;

   if (msg->message == WM_APP + 9)
   {
      appCommand((int)msg->wParam);
      *result = 0;
      return true;
   }
   return false;
}
