#include "stdafx.h"
#include "QTextDocumentTemplate.h"
#include <QTextDocument>
#include <QTextCursor>
#include <CPPTokenizer.h>
#include <CPPExpression.h>

class TokenizerDocumentDataSource : public CharacterStreamReader
{
public:
   TokenizerDocumentDataSource(const QTextCursor& cursor) : _cursor(cursor) {}
   virtual ~TokenizerDocumentDataSource() = default;

   QChar readChar() override;
   int pos() override;

   const QTextCursor& cursor() const { return _cursor; }

private:
   QTextCursor _cursor;
};

QChar TokenizerDocumentDataSource::readChar()
{
   QChar qChar;

   if (_cursor.movePosition(QTextCursor::NextCharacter, QTextCursor::KeepAnchor))
   {
      auto text = _cursor.selectedText();
      qChar = text.at(text.length() - 1);
   }

   return qChar;
}

int TokenizerDocumentDataSource::pos()
{
   return _cursor.position();
}

void resolveDocumentVariables(QTextDocument* document, const VarPtr& variables, const ResolverFunctionMap& functionMap, const QChar& startDelimiter, const QChar& endDelimiter)
{
   if (!document)
      return;

   QTextCursor cursor; // The cursor is used to iterate through the document

   while (true)
   {
      cursor = document->find(startDelimiter, cursor);

      if (cursor.isNull())
         break;

      TokenizerDocumentDataSource documentDataSource(cursor); // Use the cursor as data source!!!
      const auto context = createVersatileExpressionContext();

      Tokenizer tokenizer(&documentDataSource, context);

      tokenizer.setIgnoreWhiteSpace();
      tokenizer.setIgnoreComments();

      // Get all tokens up to the closing brace

      QList<Token> tokenList;
      auto braceLevel = 1;
      auto isEscaped = false;

      for (auto token = tokenizer.next(); token.isValid(); token = tokenizer.next())
      {
         auto op = token.as<CppOperatorToken>();

         if (op)
         {
            if (*op->op() == startDelimiter)
            {
               if (tokenList.isEmpty())
               {
                  // Escaped open brace
                  isEscaped = true;
                  braceLevel = 0;
                  break;
               }
               braceLevel++;
            }
            else if (*op->op() == endDelimiter)
            {
               braceLevel--;
            }
         }

         if (braceLevel == 0)
            break;

         tokenList.append(token);
      }

      if (braceLevel != 0)
         throw QBasicException(QString("Embedded expression not terminated with '%1'").arg(endDelimiter));

      cursor = documentDataSource.cursor();

      if (isEscaped)
      {
         // Replace the current double start delimiter with one start delimiter
         cursor.insertText(startDelimiter, cursor.charFormat());
      }
      else if (!tokenList.isEmpty())
      {
         // Resolve the formula

         const auto result = createCppExpressionTree(tokenList)->resolve(variables, functionMap);

         insertVariable(cursor, result);
      }
   }

   while (true)
   {
      cursor = document->find(QString(2, endDelimiter), cursor);

      if (cursor.isNull())
         break;

      cursor.insertText(endDelimiter, cursor.charFormat());
   }
}

void insertVariable(QTextCursor& cursor, const VarPtr& variable)
{
   const auto charFormat = cursor.charFormat(); // Format of the last character of the selection

   // Replace the text
   // Decide if its plain text, a list or a table

   if (variable->basicType() == IVariable::BasicType::Value)
   {
      auto text = variable->value().toString();

      text.replace("\r\n", QString(QChar::ParagraphSeparator));
      text.replace(u'\n', QChar::ParagraphSeparator);
      text.replace(u'\r', QChar::LineSeparator);

      cursor.insertText(text, charFormat);
   }
   else if (variable->isList())
   {
      QStringList texts;

      for (auto i = 0; i < variable->itemCount(); ++i)
      {
         auto text = variable->getItem(i)->value().toString();

         text.replace("\r\n", QString(QChar::LineSeparator));
         text.replace(u'\n', QChar::LineSeparator);
         text.replace(u'\r', QChar::LineSeparator);

         texts.append(text);
      }

      if (!cursor.currentList())
      {
         cursor.removeSelectedText();
         cursor.createList(QTextListFormat::ListDisc);
      }

      cursor.insertText(texts.join(QChar::ParagraphSeparator), charFormat);
   }
   else if (variable->isDictionary())
   {
      cursor.removeSelectedText();

      auto columns = variable->itemNames();

      if (!columns.isEmpty())
      {
         const auto rowCount = variable->getItem(0)->itemCount();

         if (cursor.currentTable() == nullptr)
         {
            QTextTableFormat tableFormat;

            tableFormat.setHeaderRowCount(1);
            tableFormat.setBorderStyle(QTextFrameFormat::BorderStyle_Solid);
            tableFormat.setCellSpacing(0);
            tableFormat.setCellPadding(2);

            if (rowCount)
               cursor.insertTable(rowCount + 1, columns.count(), tableFormat); // row +1 for the header
            else
               cursor.insertTable(2, columns.count(), tableFormat); // header and values
         }

         auto headerCharFormat = charFormat;

         headerCharFormat.setFontWeight(QFont::Bold);

         for (auto&& columnName : columns)
         {
            cursor.insertText(columnName, headerCharFormat);
            cursor.movePosition(QTextCursor::NextCell);
         }

         if (rowCount)
         {
            for (auto row = 0; row < rowCount; ++row)
            {
               for (auto col = 0; col < columns.count(); ++col)
               {
                  if (row < variable->getItem(col)->itemCount())
                     cursor.insertText(variable->getItem(col)->getItem(row)->value().toString(), charFormat);
                  cursor.movePosition(QTextCursor::NextCell);
               }
            }
         }
         else
         {
            for (auto col = 0; col < columns.count(); ++col)
            {
               cursor.insertText(variable->getItem(col)->value().toString(), charFormat);
               cursor.movePosition(QTextCursor::NextCell);
            }
         }
      }
   }
}
