#include "stdafx.h"
#include "QSizeGripEx.h"
#include <QMouseEvent>
#include <QPaintEvent>
#include <QSizePolicy>
#include <QPainter>
#include <QStyleOption>
#include <QApplication>

QSizeGripEx::QSizeGripEx(QWidget* parent) : QWidget(parent), _mousePressed(false)
{
   setCursor(Qt::SizeFDiagCursor);
   setSizePolicy(QSizePolicy(QSizePolicy::Minimum, QSizePolicy::Fixed));
}

QSize QSizeGripEx::sizeHint() const
{
   QStyleOption opt(0);

   opt.initFrom(this);

   return (style()->sizeFromContents(QStyle::CT_SizeGrip, &opt, QSize(13, 13), this));
}

void QSizeGripEx::mousePressEvent(QMouseEvent* e)
{
   _mousePressed = true;
#if QT_VERSION >= QT_VERSION_CHECK(6,0,0)
   _offsetX = qRound(e->globalPosition().x()) - parentWidget()->width();
   _offsetY = qRound(e->globalPosition().y()) - parentWidget()->height();
#else
   _offsetX = e->globalX() - parentWidget()->width();
   _offsetY = e->globalY() - parentWidget()->height();
#endif
}

void QSizeGripEx::mouseMoveEvent(QMouseEvent* e)
{
   if (_mousePressed)
   {
#if QT_VERSION >= QT_VERSION_CHECK(6,0,0)
      int width = qRound(e->globalPosition().x()) - _offsetX;
      int height = qRound(e->globalPosition().y()) - _offsetY;
#else
      int width = e->globalX() - _offsetX;
      int height = e->globalY() - _offsetY;
#endif

      if (width < 1)
         width = 1;
      if (height < 1)
         height = 1;
      parentWidget()->resize(width, height);
   }
}

void QSizeGripEx::paintEvent(QPaintEvent*)
{
   QPainter painter(this);
   QStyleOptionSizeGrip opt;

   opt.initFrom(this);
   opt.corner = Qt::BottomRightCorner;
   style()->drawControl(QStyle::CE_SizeGrip, &opt, &painter, this);
}
