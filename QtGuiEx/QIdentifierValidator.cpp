#include "stdafx.h"
#include "QIdentifierValidator.h"

QValidator::State QIdentifierValidator::validate(QString& input, int& pos) const
{
   // An empty input is acceptable
   if (input.isEmpty())
      return Acceptable;

   // Only one space is allowed between characters and the first character must not be a space
   auto prevIsSpace = false;
   for (int i = 0; i < input.length(); ++i)
   {
      if (input[i].isSpace())
      {
         if (i == 0 || prevIsSpace)
         {
            input.remove(i, 1);
            if (pos > i)
               pos--;
            i--;
         }
         else
         {
            if (input[i] != u' ')
               input[i] = u' ';
            prevIsSpace = true;
         }
      }
      else
         prevIsSpace = false;
   }

   // If the last character is a space the result is intermediate
   return prevIsSpace ? Intermediate : Acceptable;
}

void QIdentifierValidator::fixup(QString& input) const
{
   input = input.trimmed();
}
