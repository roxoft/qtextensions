#ifndef QSORTFILTERPROXYMODELEX_H
#define QSORTFILTERPROXYMODELEX_H

#include "qtguiex_global.h"
#include <QSortFilterProxyModel>

class QTGUIEX_EXPORT QSortFilterProxyModelEx : public QSortFilterProxyModel
{
   Q_OBJECT

public:
   explicit QSortFilterProxyModelEx(QObject *parent = 0);
   virtual ~QSortFilterProxyModelEx();

protected:
   virtual bool lessThan(const QModelIndex &left, const QModelIndex &right) const override;
};

#endif // QSORTFILTERPROXYMODELEX_H
