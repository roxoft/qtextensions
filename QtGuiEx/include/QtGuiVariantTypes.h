#ifndef QTGUIVARIANTTYPES_H
#define QTGUIVARIANTTYPES_H

#include "qtguiex_global.h"
#include <Variant.h>
#include <QFont>
#include <QIcon>
#include <QPixmap>
#include <QImage>
#include <QColor>

// QFont

inline SerStream& operator<<(SerStream& stream, const QFont& font) { stream << font.toString(); return stream; }
inline SerStream& operator>>(SerStream& stream, QFont& font) { font.fromString(stream.readText()); return stream; }

// QIcon

template <> inline bool isVariantNull<QIcon>(const QIcon& value) { return value.isNull(); }
template <> inline bool variantEquals<QIcon>(const QIcon& value1, const QIcon& value2) { return value1.cacheKey() == value2.cacheKey(); }
template <> inline bool variantLess<QIcon>(const QIcon& lhs, const QIcon& rhs) { return lhs.cacheKey() < rhs.cacheKey(); }

QTGUIEX_EXPORT SerStream& operator<<(SerStream& stream, const QIcon& icon);
QTGUIEX_EXPORT SerStream& operator>>(SerStream& stream, QIcon& icon);

// QPixmap

template <> inline bool isVariantNull<QPixmap>(const QPixmap& value) { return value.isNull(); }
template <> inline bool variantEquals<QPixmap>(const QPixmap& value1, const QPixmap& value2) { return value1.cacheKey() == value2.cacheKey(); }
template <> inline bool variantLess<QPixmap>(const QPixmap& lhs, const QPixmap& rhs) { return lhs.cacheKey() < rhs.cacheKey(); }

// QImage

template <> inline bool isVariantNull<QImage>(const QImage& value) { return value.isNull(); }

inline bool operator<(const QImage& lhs, const QImage& rhs) { return lhs.cacheKey() < rhs.cacheKey(); }

QTGUIEX_EXPORT SerStream& operator<<(SerStream& stream, const QImage& image);
QTGUIEX_EXPORT SerStream& operator>>(SerStream& stream, QImage& image);

// QColor

template <> inline bool isVariantNull<QColor>(const QColor& value) { return !value.isValid(); }

QTGUIEX_EXPORT bool operator<(const QColor& lhs, const QColor& rhs);

QTGUIEX_EXPORT SerStream& operator<<(SerStream& stream, const QColor& color);
QTGUIEX_EXPORT SerStream& operator>>(SerStream& stream, QColor& color);

#endif // QTGUIVARIANTTYPES_H
