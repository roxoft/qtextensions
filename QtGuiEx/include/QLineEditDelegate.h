#ifndef QLINEEDITDELEGATE_H
#define QLINEEDITDELEGATE_H

#include "qtguiex_global.h"
#include "QEditItemDelegate.h"

class QValidator;

class QTGUIEX_EXPORT QLineEditDelegate : public QEditItemDelegate
{
   Q_OBJECT
   Q_DISABLE_COPY(QLineEditDelegate)

public:
   QLineEditDelegate(QObject *parent = nullptr, int maxLength = 0x7FFF, QValidator *validator = nullptr);
   ~QLineEditDelegate() override;

   QValidator  *validator() const { return _validator; }
   void        setValidator(QValidator *validator) { _validator = validator; }

protected:
   QWidget   *createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const override;
   void      setEditorData(QWidget *editor, const QModelIndex &index) const override;
   void      setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const override;
   void      updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const override;

private:
   int         _maxLength;
   QValidator *_validator;
};

#endif // QLINEEDITDELEGATE_H
