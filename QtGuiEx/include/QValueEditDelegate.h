#ifndef QVALUEEDITDELEGATE_H
#define QVALUEEDITDELEGATE_H

#include "qtguiex_global.h"
#include "QNumberEditDelegate.h"
#include <Variant.h>
#include <QDecimal.h>

class QTGUIEX_EXPORT QValueEditDelegate : public QNumberEditDelegate
{
   Q_OBJECT

public:
   QValueEditDelegate(QObject *parent = nullptr, int integerDigits = 14, int fractionalDigits = -1, QDecimal::GroupSeparatorMode groupSeparatorMode = QDecimal::GS_Auto) : QNumberEditDelegate(parent, integerDigits, fractionalDigits, groupSeparatorMode) {}
   ~QValueEditDelegate() override = default;

   QString  unit() const;
   void     setUnit(const QString &unit);

protected:
   QRichText formattedText(const QModelIndex &index, const QLocaleEx& locale = defaultLocale()) const override;
   void      setEditorData(QWidget *editor, const QModelIndex &index) const override;
   void      setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const override;
   void      updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const override;

private:
   QString _suffix;
};

#endif // QVALUEEDITDELEGATE_H
