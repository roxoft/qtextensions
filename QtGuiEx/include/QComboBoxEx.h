#ifndef QCOMBOBOXEX_H
#define QCOMBOBOXEX_H

#include "qtguiex_global.h"
#include <QWidget>
#include <QStyle>

class QSize;
class QMouseEvent;
class QPaintEvent;
class QEvent;
class QStyleOptionComboBox;

// QComboBoxEx is a container for another control.
// It has a frame and a button.
// The button can be used to show a selection dialog for the embedded control.
// Usually the QComboBoxEx contains a QSelector or a QDateEditEx.
class QTGUIEX_EXPORT QComboBoxEx : public QWidget
{
   Q_OBJECT

public:
   QComboBoxEx(QWidget* parent = 0);
   ~QComboBoxEx() override = default;

   void setContent(QWidget* w);
   QWidget* content() { return _content; }

   void setReadOnly();

   bool isInArrowField(const QPoint &point) const;

public: // Special flag set in designer interface
   void setDesignerMode(bool designer = true) { _designer = designer; } // Set when used in Qt designer to fix frameless widget problem

public:
   QSize sizeHint() const override;
   QSize minimumSizeHint() const override;
   bool event(QEvent *e) override;

signals:
   void clicked();

protected:
   void mousePressEvent(QMouseEvent* e) override;
   void mouseMoveEvent(QMouseEvent* e) override;
   void mouseReleaseEvent(QMouseEvent* e) override;
   void paintEvent(QPaintEvent* e) override;
   void resizeEvent(QResizeEvent *e) override;
   void leaveEvent(QEvent* e) override;

private:
   void initStyleOption(QStyleOptionComboBox* option) const;
   void updateContentGeometry();
   void updateArrow(QStyle::StateFlag state);

private:
   QWidget*          _content;
   bool              _isHovering;
   QStyle::StateFlag _arrowState;
   bool              _designer;
};

#endif // QCOMBOBOXEX_H
