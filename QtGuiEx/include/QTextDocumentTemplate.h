#pragma once

#include "qtguiex_global.h"
#include "ExpressionBase.h"

class QTextDocument;
class QTextCursor;

QTGUIEX_EXPORT void resolveDocumentVariables(QTextDocument* document, const VarPtr& variables, const ResolverFunctionMap& functionMap = ResolverFunctionMap(), const QChar& startDelimiter = u'{', const QChar& endDelimiter = u'}');
QTGUIEX_EXPORT void insertVariable(QTextCursor& cursor, const VarPtr& variable);
