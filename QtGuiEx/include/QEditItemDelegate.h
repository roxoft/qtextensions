#pragma once

#include "qtguiex_global.h"
#include <QObject>

class QStyleOptionViewItem;
class QStyledItemDelegateEx;
class QAbstractItemModel;
class QRichText;
class QLocaleEx;

class QTGUIEX_EXPORT QEditItemDelegate : public QObject
{
   Q_OBJECT
   Q_DISABLE_COPY(QEditItemDelegate)
   friend class QStyledItemDelegateEx;

public:
   QEditItemDelegate(QObject *parent = nullptr);
   ~QEditItemDelegate() override;

protected:
   virtual QRichText formattedText(const QModelIndex &index, const QLocaleEx& locale) const;
   virtual QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const;
   virtual void destroyEditor(QWidget *editor, const QModelIndex &index) const;
   virtual void setEditorData(QWidget *editor, const QModelIndex &index) const;
   virtual void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const;
   virtual void updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const;

   void initStyleOption(QStyleOptionViewItem *option, const QModelIndex &index) const;

private:
   const QStyledItemDelegateEx* _itemDelegate = nullptr;
};
