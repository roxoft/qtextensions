#ifndef WIDGETTOOLS_H
#define WIDGETTOOLS_H

#include "qtguiex_global.h"

class QWidegt;

QTGUIEX_EXPORT void adjustToScreen(QWidget *widget);
QTGUIEX_EXPORT void centerOnParent(QWidget *widget);
#if QT_VERSION < QT_VERSION_CHECK(5,0,0)
void adjustPosition(QWidget *widget, bool centerOnParent);
#endif

#endif // WIDGETTOOLS_H
