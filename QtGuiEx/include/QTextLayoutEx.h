#ifndef QTEXTLAYOUTEX_H
#define QTEXTLAYOUTEX_H

#include "qtguiex_global.h"
#include <QTextLayout>

class QRichText;

class QTGUIEX_EXPORT QTextLayoutEx : public QTextLayout
{
public:
   QTextLayoutEx(const QRichText& richText, const QFont &font);
   ~QTextLayoutEx() = default;

   void setWordWrap(bool wordWrap = true) { _textOption.setWrapMode(wordWrap ? QTextOption::WordWrap : QTextOption::ManualWrap); }
   void setTextDirection(Qt::LayoutDirection direction) { _textOption.setTextDirection(direction); }
   void setAlignment(Qt::Alignment alignment) { _textOption.setAlignment(alignment); }

   qreal leading() const { return _leading; }
   void setLeading(qreal leading) { _leading = leading; }

   void createLayout(qreal maxWidth, qreal maxHeight = 0.0);

   const QSizeF& size() const { return _size; }

private:
   qreal lineWidth(const QTextLine& line) const;

private:
   QTextOption _textOption;
   qreal       _leading = 0.0;
   QSizeF      _size;
};

#endif // QTEXTLAYOUTEX_H
