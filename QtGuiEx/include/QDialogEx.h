#ifndef QDIALOGEX_H
#define QDIALOGEX_H

#include "qtguiex_global.h"
#include <QDialog>
#include <QSettingsEx.h>


class QTGUIEX_EXPORT QDialogEx : public QDialog
{
   Q_OBJECT

public:
   explicit QDialogEx(QWidget *parent = 0, Qt::WindowFlags f = Qt::WindowFlags());
   virtual ~QDialogEx();

   QSettingsEx dlgSettings() { return _dlgSettings; }
   void setDlgSettings(QSettingsEx dlgSettings);
   void setDlgSettings(QSettingsExBase* dlgSettings);

   void loadGeometrySettings(const QString& key);
   void saveGeometrySettings(const QString& key);

protected:
#if QT_VERSION >= QT_VERSION_CHECK(5,0,0)
   void showEvent(QShowEvent *e) override;
#endif

private:
   QSettingsEx _dlgSettings;
};

#endif // QDIALOGEX_H
