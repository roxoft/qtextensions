#ifndef QINCREMENTALSEARCHWIDGET_H
#define QINCREMENTALSEARCHWIDGET_H

#include "qtguiex_global.h"
#include <QWidget>

namespace Ui {
   class QIncrementalSearchWidget;
}

// Forward declaration of Qt classes (NOT QtExtension classes)
QT_BEGIN_NAMESPACE
   class QAbstractItemView;
   class QAbstractItemModel;
   class QModelIndex;
   class QTableView;
   class QTreeView;
   class QEvent;
   class QShowEvent;
   class QHideEvent;
   class QKeyEvent;
   class QTimer;
QT_END_NAMESPACE

class QHighlightItemDelegate;

// Enables searches in Item Views (QAbstractItemModel)
//
// {QIncrementalSearchWidget.png}
//
// The corresponding view (QAbstractItemView) must have a QHighlightItemDelegate as delegate.
// QTreeViewEx and QTableViewEx have them by default.
//
// On entering text in the search widget the found elements in the view are highlighted (through the QHighlightItemDelegate). The current item is set to the first found item.
// With the arrow buttons the current item can be set to the next or previous matching item. The search jumps from the end to the beggining of the list and vice versa.
//
// The search traverses the elements hierarchically. If the current element is a parent element the next tested element is the child element on the right or left side. The previous element would be the element on the right or left side of the parent.
//
// The search includes elements in collapsed trees.
//
// The search is only case sensitive if the corresponding check box is set.
//
// If the "complete word" check box is set the search text must match a complete word.
//
// If the "only current column" check box is set the search comprises only the column with the current item (dotted frame). The text of the check box changes to "only column \[ColumnName]".
// The restriction to that column reamains in effect until the check box is unchecked. Changing the current item has no effect on the search.
//
// For tree views there is an additional search constraint:
//
// {QIncrementalSearchWidgetLevel.png}
//
// This constraint is very specialized for one purpose where the tree view was separated in sections.
//
// On setting "only section" the search is restricted to the section with the current item. The text of the check box changes into "only section \[SectionName]".
//
class QTGUIEX_EXPORT QIncrementalSearchWidget : public QWidget
{
   Q_OBJECT
   Q_DISABLE_COPY(QIncrementalSearchWidget)

public:
   enum ViewOption { VO_Table, VO_SubSections };

public:
   QIncrementalSearchWidget(QWidget* parent = nullptr);
   ~QIncrementalSearchWidget() override;

   void setItemView(QTableView* tableView);
   void setItemView(QTreeView* treeView, ViewOption viewOption = VO_Table);
   QAbstractItemView* itemView() const { return _itemView; }

   void setColumnFilterEnabled(bool enabled = true);

   bool processKeyEvent(QKeyEvent* e);

   QList<int> match(const QModelIndex& index, const QString& displayText) const;

   int patternLength() const { return _pattern.length(); }

   bool moveNext();
   bool movePrev();

   void addToHistory(const QString& text);

public:
   bool eventFilter(QObject* watched, QEvent* e) override;

protected:
   void showEvent(QShowEvent* e) override;
   void hideEvent(QHideEvent* e) override;

private:
   class ViewInfo
   {
   public:
      ViewInfo() = default;
      virtual ~ViewInfo() = default;

      virtual int visualRow(int row) const = 0;
      virtual int logicalRow(int row) const = 0;
      virtual int visualColumn(int column) const = 0;
      virtual int logicalColumn(int column) const = 0;

      virtual int visibleRowForward(int row, const QModelIndex &parent) const = 0;
      virtual int visibleColumnForward(int column, const QModelIndex &parent) const = 0;
      virtual int visibleRowBackward(int row, const QModelIndex &parent) const = 0;
      virtual int visibleColumnBackward(int column, const QModelIndex &parent) const = 0;

      virtual int rowCount(const QModelIndex &parent) const = 0;
      virtual int columnCount(const QModelIndex &parent) const = 0;

      virtual const QAbstractItemModel *model() const = 0;
      virtual QHighlightItemDelegate* highlightDelegate() const = 0;

      void visibleCellForward(int &row, int &column, QModelIndex &parent) const;
      void visibleCellBackward(int &row, int &column, QModelIndex &parent) const;
   };

   class TreeViewInfo : public ViewInfo
   {
   public:
      TreeViewInfo(const QTreeView *treeView) : _treeView(treeView) {}
      ~TreeViewInfo() override = default;

      int visualRow(int row) const override { return row; }
      int logicalRow(int row) const override { return row; }
      int visualColumn(int column) const override;
      int logicalColumn(int column) const override;

      int visibleRowForward(int row, const QModelIndex &parent) const override;
      int visibleColumnForward(int column, const QModelIndex &parent) const override;
      int visibleRowBackward(int row, const QModelIndex &parent) const override;
      int visibleColumnBackward(int column, const QModelIndex &) const override;

      int rowCount(const QModelIndex &parent) const override;
      int columnCount(const QModelIndex &parent) const override;

      const QAbstractItemModel *model() const override;
      QHighlightItemDelegate* highlightDelegate() const override;

   private:
      const QTreeView *_treeView;
   };

   class TableViewInfo : public ViewInfo
   {
   public:
      TableViewInfo(const QTableView *tableView) : _tableView(tableView) {}
      ~TableViewInfo() override = default;

      int visualRow(int row) const override;
      int logicalRow(int row) const override;
      int visualColumn(int column) const override;
      int logicalColumn(int column) const override;

      int visibleRowForward(int row, const QModelIndex &parent) const override;
      int visibleColumnForward(int column, const QModelIndex &parent) const override;
      int visibleRowBackward(int row, const QModelIndex &) const override;
      int visibleColumnBackward(int column, const QModelIndex &) const override;

      int rowCount(const QModelIndex &parent) const override;
      int columnCount(const QModelIndex &parent) const override;

      const QAbstractItemModel *model() const override;
      QHighlightItemDelegate* highlightDelegate() const override;

   private:
      const QTableView *_tableView;
   };

private slots:
   void on_closeButton_clicked();
   void on_lineEdit_textEdited(const QString& text);
   void on_leftButton_clicked();
   void on_rightButton_clicked();
   void on_caseSensitiveCheckBox_toggled(bool checked);
   void on_wordCheckBox_toggled(bool checked);
   void on_columnOnlyCheckBox_toggled(bool checked);

   void clearInformationLabel();
   void clear();

private:
   void     displayInformation(const QString& text);
   QString  specialOptionText(const QString& column = QString()) const;
   bool     matchForward(bool step = false);
   bool     matchBackward(bool step = false);

private:
   Ui::QIncrementalSearchWidget* _ui;

   QAbstractItemView* _itemView = nullptr;
   ViewOption _viewOption = VO_Table;
   ViewInfo* _viewInfo = nullptr;
   QTimer* _timer = nullptr;

   QString _pattern;
   Qt::CaseSensitivity _caseSensitivity = Qt::CaseInsensitive;
   bool _matchWord = false;
   int _column = -1;
   int _level = -1;
   QString _sectionHeader;

   QStringList m_history;
};

#endif // QINCREMENTALSEARCHWIDGET_H
