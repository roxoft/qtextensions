#ifndef QFIXEDGRIDWIDGET_H
#define QFIXEDGRIDWIDGET_H

#include "qtguiex_global.h"
#include <QFrame>

class QAbstractButton;

class QTGUIEX_EXPORT QFixedGridWidget : public QFrame
{
   Q_OBJECT

public:
   static QIcon iconCloseWidget;
   static QIcon iconEditText;
   static QIcon iconApplyTitleChange;

public:
   QFixedGridWidget(const QString& title, QWidget *parent = 0);
   QFixedGridWidget(QWidget *parent = 0);
   virtual ~QFixedGridWidget();

   bool isClosable() const;
   void setIsClosable(bool isClosable = true);

   bool isTitleEditable() const;
   void setIsTitleEditable(bool isTitleEditable = true);

   void addTitleBarWidget(QWidget* w);

   QWidget* widget() const;
   void setWidget(QWidget* widget);

   virtual QByteArray saveState() const;
   virtual void restoreState(const QByteArray& data);

public slots:
   void editTitle();

protected:
   void mousePressEvent(QMouseEvent* event) override;
   void mouseReleaseEvent(QMouseEvent* event) override;
   void wheelEvent(QWheelEvent* event) override;
   void focusInEvent(QFocusEvent* event) override;
   void focusOutEvent(QFocusEvent* event) override;
   void resizeEvent(QResizeEvent* event) override;
   void paintEvent(QPaintEvent* event) override;
   void changeEvent(QEvent* event) override;
   bool event(QEvent* event) override;
   bool eventFilter(QObject* watched, QEvent* event) override;

private:
   enum class StartPosition { none, topLeft, left, bottomLeft, bottom, bottomRight, right, topRight, top, move };

private slots:
   void onTitleBarWidgetDestroyed(QObject* obj);

private:
   void init();
   void positionTitleBarWidgets() const;
   bool handleMouseMove(QMouseEvent* event);
   StartPosition handleMousePosition(const QPoint& pos);
   int titleHeight() const;

private:
   QAbstractButton* _titleBarCloseButton = nullptr;
   QAction* _closeWidgetAction = nullptr;
   QAction* _changeTitleAction = nullptr;
   QPoint _dragStartPosition;
   QRect _dragStartGeometry;
   StartPosition _startPosition = StartPosition::none;
   QRect _titleArea;
   QList<QWidget*> _titleBarWidgets;
   QWidget* _content = nullptr;
};

class QFixedGridWidgetFactory
{
public:
   virtual ~QFixedGridWidgetFactory() = default;

   virtual QString widgetType(QFixedGridWidget* widget) const = 0;
   virtual QFixedGridWidget* createWidget(const QString& type) const = 0;

   virtual QByteArray widgetState(QFixedGridWidget* widget) const { return widget->saveState(); }
   virtual void setWidgetState(QFixedGridWidget* widget, const QByteArray& state) const { widget->restoreState(state); }
};

#endif // QFIXEDGRIDWIDGET_H
