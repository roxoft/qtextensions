#ifndef OVERRIDECURSOR_H
#define OVERRIDECURSOR_H

#include "qtguiex_global.h"
#include <QCursor>
#include <QList>

class QTGUIEX_EXPORT OverrideCursor
{
public:
   OverrideCursor(const QCursor& cursor = Qt::WaitCursor);
   ~OverrideCursor();

   void restore();

private:
   bool _isShown;
};

class QTGUIEX_EXPORT NormalCursor
{
public:
   NormalCursor();
   ~NormalCursor();

   void set();
   void restore();

private:
   QList<QCursor> _cursorStack;
};

#endif // OVERRIDECURSOR_H
