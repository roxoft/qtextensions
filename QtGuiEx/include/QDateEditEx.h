#ifndef QDATEEDITEX_H
#define QDATEEDITEX_H

#include "qtguiex_global.h"
#include <QLineEdit>
#include <QDate>
#include "QDateValidator.h"

class QDateValidator;
class QKeyEvent;
class QFocusEvent;
class QMouseEvent;
class QCalendarWidget;

// Control to enter date values. It can be combined with a QComboBoxEx to show a calendar popup widget.
//
// QDateEditEx has the following properties:
// - Getting focus selects the complete date
// - A double click on an element of the date selects the element (day, month or year).
// - A triple click selects the complete date.
// - Characters are inserted at the cursor position.
// - The cursor moves over the dot to the next element and selects it.
// - It can be configured if the date edit is allowed to be empty.
// - A maximum and minimum value can be defined.
//
class QTGUIEX_EXPORT QDateEditEx : public QLineEdit
{
   Q_OBJECT

   Q_PROPERTY(QDate minimumDate READ minimumDate WRITE setMinimumDate RESET clearMinimumDate)
   Q_PROPERTY(QDate maximumDate READ maximumDate WRITE setMaximumDate RESET clearMaximumDate)
   Q_PROPERTY(bool emptyAllowed READ emptyAllowed WRITE setEmptyAllowed)

public:
   QDateEditEx(QWidget *parent = nullptr);
   ~QDateEditEx() override;

   QDate date() const;
   void setDate(const QDate& d);

   const QDate& minimumDate() const { return _minimumDate; }
   void setMinimumDate(const QDate& date);
   void clearMinimumDate();

   const QDate& maximumDate() const { return _maximumDate; }
   void setMaximumDate(const QDate& date);
   void clearMaximumDate();

   void setDateRange(const QDate& min, const QDate& max);

   bool emptyAllowed() const { return _emptyAllowed; }
   void setEmptyAllowed(bool allowed) { _emptyAllowed = allowed; }

public:
   virtual QSize sizeHint() const override;
   virtual QSize minimumSizeHint() const override;
   virtual bool event(QEvent* e) override;

public slots:
   void showPopup();

protected:
   virtual void keyPressEvent(QKeyEvent* e) override;
   virtual void focusInEvent(QFocusEvent* e) override;
   virtual void mouseReleaseEvent(QMouseEvent* e) override;

private slots:
   void onCursorPositionChanged(int oldPos, int newPos);
   void onTextEdited(const QString& text);
   void onTextChanged(const QString& text);
   void onEditingFinished();
   void onCalendarDateSelected(const QDate& date);

private:
   QDate             _oldDate;
   QDateValidator*   _validator;
   QWidget*          _alignTo;
   QCalendarWidget*  _calendarWidget;
   QDate             _minimumDate;
   QDate             _maximumDate;
   bool              _emptyAllowed;
   bool              _isEditing;

   void selectSection(QDateValidator::Section* section);
};

#endif // QDATEEDITEX_H
