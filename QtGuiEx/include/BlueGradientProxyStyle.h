#pragma once

#include "qtguiex_global.h"
#include "QExtendedProxyStyle.h"

class QTGUIEX_EXPORT BlueGradientProxyStyle : public QExtendedProxyStyle
{
   Q_OBJECT

public:
   BlueGradientProxyStyle(QStyle* style = nullptr);
   BlueGradientProxyStyle(EntireCellCheckBoxPosition checkBoxPosition, QStyle* style = nullptr);
   ~BlueGradientProxyStyle() override = default;

   void polish(QPalette& palette) override;

   void drawPrimitive(PrimitiveElement element, const QStyleOption *option, QPainter *painter, const QWidget *widget) const Q_DECL_OVERRIDE;
   void drawControl(ControlElement control, const QStyleOption *option, QPainter *painter, const QWidget *widget) const Q_DECL_OVERRIDE;
};
