#ifndef QNUMBEREDITDELEGATE_H
#define QNUMBEREDITDELEGATE_H

#include "qtguiex_global.h"
#include "QEditItemDelegate.h"
#include <QDecimal.h>

class QTGUIEX_EXPORT QNumberEditDelegate : public QEditItemDelegate
{
   Q_OBJECT
   Q_DISABLE_COPY(QNumberEditDelegate)

public:
   QNumberEditDelegate(QObject *parent = 0, int integerDigits = 14, int fractionalDigits = -1, QDecimal::GroupSeparatorMode groupSeparatorMode = QDecimal::GS_Auto);
   virtual ~QNumberEditDelegate();

   int   integerDigits() const { return _integerDigits; }
   void  setIntegerDigits(int integerDigits) { _integerDigits = integerDigits; }

   int   fractionalDigits() const { return _fractionalDigits; }
   void  setFractionalDigits(int fractionalDigits) { _fractionalDigits = fractionalDigits; }

   QDecimal::GroupSeparatorMode groupSeparatorMode() const { return _groupSeparatorMode; }
   void setGroupSeparatorMode(QDecimal::GroupSeparatorMode groupSeparatorMode) { _groupSeparatorMode = groupSeparatorMode; }

   bool  emptyAllowed() const { return _emptyAllowed; }
   void  setEmptyAllowed(bool allowed = true) { _emptyAllowed = allowed; }

   bool  zeroAllowed() const { return _zeroAllowed; }
   void  setZeroAllowed(bool allowed = true) { _zeroAllowed = allowed; }

   bool  negativeAllowed() const { return _negativeAllowed; }
   void  setNegativeAllowed(bool allowed = true) { _negativeAllowed = allowed; }

protected:
   QRichText formattedText(const QModelIndex &index, const QLocaleEx& locale = defaultLocale()) const override;
   QWidget   *createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const override;
   void      setEditorData(QWidget *editor, const QModelIndex &index) const override;
   void      setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const override;
   void      updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const override;

protected:
   QRichText formattedText(const QDecimal &decimal, const QLocaleEx& locale = defaultLocale()) const;

private:
   int   _integerDigits;
   int   _fractionalDigits; // -1 = auto
   QDecimal::GroupSeparatorMode  _groupSeparatorMode;
   bool  _emptyAllowed;
   bool  _zeroAllowed;
   bool  _negativeAllowed;
};

#endif // QNUMBEREDITDELEGATE_H
