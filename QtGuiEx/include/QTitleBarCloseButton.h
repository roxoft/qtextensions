#ifndef QTITLEBARCLOSEBUTTON_H
#define QTITLEBARCLOSEBUTTON_H

#include "qtguiex_global.h"
#include <QAbstractButton>

class QTGUIEX_EXPORT QTitleBarCloseButton : public QAbstractButton
{
   Q_OBJECT

public:
   QTitleBarCloseButton(QWidget* parent = 0);
   ~QTitleBarCloseButton();

   QSize sizeHint() const;
   QSize minimumSizeHint() const { return sizeHint(); }

#if QT_VERSION_MAJOR < 6
   void enterEvent(QEvent *event);
#else
   void enterEvent(QEnterEvent *event);
#endif
   void leaveEvent(QEvent *event);
   void paintEvent(QPaintEvent *event);
};

#endif // QTITLEBARCLOSEBUTTON_H
