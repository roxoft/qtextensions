#ifndef QHOVERBUTTON_H
#define QHOVERBUTTON_H

#include <QIcon>
#include <QPushButton>

#include "qtguiex_global.h"

class QTGUIEX_EXPORT QHoverButton : public QPushButton
{
   Q_OBJECT
   Q_PROPERTY(qreal transparencyLevel READ transparencyLevel WRITE setTransparencyLevel NOTIFY transparencyLevelChanged)
   Q_PROPERTY(qreal iconTransparency READ iconTransparency WRITE setIconTransparency NOTIFY iconTransparencyChanged)

public:
   QHoverButton(QWidget* parent = nullptr);

   // Getter and setter for dialog transparency level
   qreal transparencyLevel() const;
   void setTransparencyLevel(qreal level);

   // Static getter and setter for global transparency level
   static qreal globalTransparencyLevel();
   static void setGlobalTransparencyLevel(qreal level);

   // Getter and setter for icon transparency
   qreal iconTransparency() const;
   void setIconTransparency(qreal level);

   // Static getter and setter for global icon transparency level
   static qreal globalIconTransparencyLevel();
   static void setGlobalIconTransparencyLevel(qreal level);

   // Methods to set button text and icon
   void setButtonText(const QString& text);
   void setButtonIcon(const QIcon& icon);

   // Static methods to set global text and icon
   static void setGlobalButtonText(const QString& text);
   static void setGlobalButtonIcon(const QIcon& icon);

signals:
   void transparencyLevelChanged(qreal level);
   void iconTransparencyChanged(qreal level);

protected:
   bool event(QEvent* e) override;
   void mousePressEvent(QMouseEvent* e) override;

private:
   qreal m_transparencyLevel = -1; // Instance-specific transparency (default to -1, meaning "use global")
   qreal m_iconTransparency = -1; // Instance-specific icon transparency (default to -1, meaning "use global")

   static qreal s_globalTransparencyLevel; // Static global transparency level
   static qreal s_globalIconTransparencyLevel; // Static global icon transparency level
   static QString s_globalButtonText; // Static global button text
   static QIcon s_globalButtonIcon; // Static global button icon
};


#endif // QHOVERBUTTON_H
