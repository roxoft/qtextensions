#ifndef QAPPWINDOW_H
#define QAPPWINDOW_H

#include "qtguiex_global.h"
#include <QMainWindow>
#include <QPointer>
#include <QEventLoop>

class QTGUIEX_EXPORT QAppWindow : public QMainWindow
{
   Q_OBJECT
public:
   explicit QAppWindow(QWidget *parent = 0, Qt::WindowFlags flags = Qt::Widget);
   virtual ~QAppWindow() { try { hide(); } catch(...) {} }

   virtual void setVisible(bool visible) override;

   void setModal(bool modal) { setAttribute(Qt::WA_ShowModal, modal); }

public slots:
   void exec();

protected:
#if QT_VERSION >= QT_VERSION_CHECK(5,0,0)
   virtual void showEvent(QShowEvent *e) override;
#endif
   virtual void closeEvent(QCloseEvent *e) override;

private:
   QPointer<QEventLoop> _eventLoop;
};

#endif // QAPPWINDOW_H
