#ifndef QTABLEVIEWEX_H
#define QTABLEVIEWEX_H

#include "qtguiex_global.h"
#include <QTableView>

class QPaintItemDelegate;
class QEditItemDelegate;
class QLabel;
class QResizeEvent;

/*
 QTableViewEx extends QTableView by some useful functions.
 QTableViewEx uses internally QHighlightItemDelegate (indirectly derived from QStyledItemDelegate) to draw its content.
 If you want to provide your own delegates, derive them from QEditItemDelegate or QPaintItemDelegate!
 This way the background style, the content auto-align and the search highlighting is not affected.

 The following functions are added:
 - If the table is empty a "No data" Text can be shown.
 - An own selection model can be used when the data model is an HtItemModel.
   In the new model the data model items are marked as selected instead of maintaining a separate selection model.
   This way the selection is preserved even if the rows are sorted or rows are added or deleted.
   On the downside the Qt selection mechanism must be disabled.
 - Separate delegates for editing and painting.
 - Multiple check boxes can be toggled using shift-click.
 - Optionally check boxes can be toggled by click-moving the mouse over check boxes. This disables the shift-click feature.
*/
class QTGUIEX_EXPORT QTableViewEx : public QTableView
{
   friend class QExtendedProxyStyle;

   Q_OBJECT
   Q_DISABLE_COPY(QTableViewEx)

   Q_PROPERTY(bool contentAutoAlign READ isContentAutoAlign WRITE setIsContentAutoAlign)
   Q_PROPERTY(bool checkOnMouseMove READ checkOnMouseMove WRITE setCheckOnMouseMove)

   // Hide members
   void setItemDelegate(QAbstractItemDelegate* itemDelegate);
   QAbstractItemDelegate *itemDelegate() const;
   void setItemDelegateForColumn(int column, QAbstractItemDelegate* itemDelegate);
   QAbstractItemDelegate *itemDelegateForColumn(int column) const;
   void setItemDelegateForRow(int row, QAbstractItemDelegate* itemDelegate);
   QAbstractItemDelegate *itemDelegateForRow(int row) const;

public:
   QTableViewEx(QWidget* parent = nullptr);
   ~QTableViewEx() override = default;

   void showNoData(bool show = true);

   void setModel(QAbstractItemModel *model) override;

   void resizeColumnsToContentsEx();
   void resizeRowsToContentsEx();

   bool  isContentAutoAlign() const;
   void  setIsContentAutoAlign(bool autoAlign);

   bool checkOnMouseMove() const { return _checkOnMouseMove; }
   void setCheckOnMouseMove(bool checkOnMouseMove);

   // The following functions replace the functions setItemDelegate(), setItemDelegateForColumn(), setItemDelegateForRow() in the base class QAbstractItemView
   void  setEditDelegate(QEditItemDelegate* editDelegate);
   QEditItemDelegate* editDelegate() const;
   void  setEditDelegateForColumn(int column, QEditItemDelegate* editDelegate);
   QEditItemDelegate* editDelegateForColumn(int column) const;
   void  setEditDelegateForRow(int row, QEditItemDelegate* editDelegate);
   QEditItemDelegate* editDelegateForRow(int row) const;
   void  setPaintDelegate(QPaintItemDelegate* paintDelegate);
   QPaintItemDelegate* paintDelegate() const;
   void  setPaintDelegateForColumn(int column, QPaintItemDelegate* paintDelegate);
   QPaintItemDelegate* paintDelegateForColumn(int column) const;
   void  setPaintDelegateForRow(int row, QPaintItemDelegate* paintDelegate);
   QPaintItemDelegate* paintDelegateForRow(int row) const;

signals:
   void doubleClickReleased(const QModelIndex& index);

protected:
   bool viewportEvent(QEvent* e) override;
   void paintEvent(QPaintEvent* e) override;
   void resizeEvent(QResizeEvent *e) override;
   void mousePressEvent(QMouseEvent *e) override;
   void mouseMoveEvent(QMouseEvent *e) override;
   void mouseReleaseEvent(QMouseEvent *e) override;
   void mouseDoubleClickEvent(QMouseEvent* e) override;

private:
   QList<int> optimalSizes(bool forColumns);

   void setIndexIsConcealed(const QModelIndex& index, bool isConcealed) const; // Used by drawItemViewItem() in QStyledItemDelegateEx.cpp

private:
   bool _checkOnMouseMove = false;
   QLabel* _noDataWidget = nullptr;

   mutable QList<QPersistentModelIndex> _concealedTextIndexList;

   bool _doubleClicked = false;

   QModelIndex _mousePressedIndex;
};

#endif // QTABLEVIEWEX_H
