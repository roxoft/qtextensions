#ifndef QSELECTORCOMBODELEGATE_H
#define QSELECTORCOMBODELEGATE_H

#include "qtguiex_global.h"
#include "QEditItemDelegate.h"
#include <Variant.h>

class QCompleterDataSource;

class QTGUIEX_EXPORT QSelectorComboDelegate : public QEditItemDelegate
{
   Q_OBJECT
   Q_DISABLE_COPY(QSelectorComboDelegate)

public:
   QSelectorComboDelegate(QObject *parent = nullptr, QCompleterDataSource *dataSource = nullptr);
   QSelectorComboDelegate(QCompleterDataSource *dataSource, int itemKeyRole, int dataSourceKeyRole, QObject *parent = 0); // Do not use Qt::EditRole or Qt::DisplayRole for the itemKeyRole!
   ~QSelectorComboDelegate() override;

   QCompleterDataSource*   dataSource() const { return _dataSource; }
   void                    setDataSource(QCompleterDataSource *dataSource);

   const Variant  &defaultItemKey() const { return _defaultItemKey; }
   void           setDefaultItemKey(const Variant &defaultItemKey) { _defaultItemKey = defaultItemKey; }

   bool  isFreeText() const { return _isFreeText; }
   void  setIsFreeText(bool isFreeText) { _isFreeText = isFreeText; }

   int   itemKeyRole() const { return _itemKeyRole; }
   int   dataSourceKeyRole() const { return _dataSourceKeyRole; }
   void  setKeyRoles(int itemKeyRole, int dataSourceKeyRole) { _itemKeyRole = itemKeyRole; _dataSourceKeyRole = dataSourceKeyRole; }

protected:
   QWidget* createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const override;
   void     setEditorData(QWidget *editor, const QModelIndex &index) const override;
   void     setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const override;
   void     updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const override;

private:
   QCompleterDataSource* _dataSource;
   Variant _defaultItemKey;
   bool _isFreeText;
   int _itemKeyRole;
   int _dataSourceKeyRole;
};

#endif // QSELECTORCOMBODELEGATE_H
