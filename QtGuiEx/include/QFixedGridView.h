#ifndef QFIXEDGRIDVIEW_H
#define QFIXEDGRIDVIEW_H

#include "qtguiex_global.h"
#include <QWidget>

class QFixedGridWidgetFactory;

class QTGUIEX_EXPORT QFixedGridView : public QWidget
{
   Q_OBJECT

public:
   QFixedGridView(QWidget *parent = 0);
   virtual ~QFixedGridView();

   int cellSize() const { return _cellSize; }

   QColor gridColor() const { if (_gridColor.isValid()) return _gridColor; return palette().color(QPalette::WindowText); }
   void setGridColor(QColor color) { _gridColor = color; }

   QSize sizeHint() const override;

   QPoint cellFromPos(QPoint pos) const;

   void insertWidget(QWidget* w); // Inserts the widget and snpas it to the next free space
   void insertWidget(QWidget* w, int row, int column, int rowSpan = 0, int columnSpan = 0); // If rowSpan or columnSpan is 0 the size is calculated from the items size hint
   void insertWidget(QWidget* w, const QRect& area);

   QList<QWidget*> gridWidgets() const;

   void snapIn(QWidget* w);
   void previewSnapArea(QWidget* w);

   void ensureWidgetVisible(QWidget* widget);

   QByteArray saveState() const;
   void restoreState(const QByteArray& data);

   QByteArray saveState(const QFixedGridWidgetFactory* widgetFactory) const; // Saves the state of FixedGridWidgets
   void restoreState(const QByteArray& data, const QFixedGridWidgetFactory* widgetFactory); // Recreates the stored FixedGridWidgets

public slots:
   void setCellSize(int cellSize);

protected:
   void paintEvent(QPaintEvent* event) override;

private slots:
   void onChangeGridSize();

private:
   struct ItemInfo
   {
      QRect area;
      QWidget* item = nullptr;
   };

private:
   void doLayout() const;
   void doLayout(const ItemInfo& itemInfo) const;
   QRect cellArea(const QRect& rect) const;
   QRect widgetArea(const QRect& cellArea) const;
   QList<ItemInfo> items(const QRect& cellArea) const;
   bool areaIsFree(const QRect& cellArea) const;

private slots:
   void onWidgetDeleted(QObject* obj);

private:
   qint32 _cellSize = 100;
   qint32 _cellMargin = 5;
   QColor _gridColor;
   QList<ItemInfo> _itemList;
   QAction* _actionChangeGridSize = nullptr;
   ItemInfo* _movingItem = nullptr;
   bool _showGrid = false;
};

#endif // QFIXEDGRIDVIEW_H
