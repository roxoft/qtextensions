#pragma once
#include "qtguiex_global.h"
#include <QDialogEx>
#include "QExpressionEditorWidget.h"

class QTGUIEX_EXPORT QExpressionEditor : public QDialogEx
{
   Q_OBJECT

public:
   QExpressionEditor(const VarTree& varTree, const ResolverFunctionMap& functionMap, const Tokenizer::Context* context, QWidget *parent = nullptr); // Takes ownership of the context
   ~QExpressionEditor() override;

   void setSettingsHandler(QSettingsExBase* settings); // Takes ownership of the settings

   void expandFirstLevel();

   void setEditHandler(AbstractEditHandler* editHandler); // Takes ownership of the handler
   AbstractEditHandler* editHandler() const;

   void setExpression(const QString& expression);
   QString expression() const;

signals:
   void helpRequest();

protected:
   void keyPressEvent(QKeyEvent* event) override;
   void closeEvent(QCloseEvent* event) override;

private slots:
   void on_pushButtonOk_clicked();
   void on_pushButtonCancel_clicked();
   void on_pushButtonHelp_clicked();

private:
   QExpressionEditorWidget* _expressionEditor = nullptr;
};
