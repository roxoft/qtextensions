#pragma once

#include "qtguiex_global.h"
#include "QTextEditEx.h"
#include <Tokenizer>

class QTGUIEX_EXPORT QExpressionTextEdit : public QTextEditEx
{
   Q_OBJECT

public:
   QExpressionTextEdit(QWidget *parent = nullptr);
   ~QExpressionTextEdit() override;

   bool isDelimited() const { return !_startDelimiter.isNull() && !_endDelimiter.isNull(); }
   void setIsDelimited(const QChar& startDelimiter = u'{', const QChar& endDelimiter = u'}') { _startDelimiter = startDelimiter; _endDelimiter = endDelimiter; }

   QPair<int, int> expressionRangeAtBlockPosition(const QTextBlock& block, int blockPos) const;
   QList<Token> variableAtBlockPosition(const QTextBlock& block, int blockPos) const;

protected:
   void keyPressEvent(QKeyEvent* event) override;

   void on_completerCurrentIndexChanged(const QModelIndex& index) override;
   void on_completerCurrentTextChanged(bool useCurrentIndex) override;
   void on_completerMatchIndexChanged(const QModelIndex& index) override;

private:
   bool handleSpecialKeys(QTextCursor& tc, const QChar& key, const QChar& startDelimiter, const QChar& endDelimiter);

   QList<Token> blockTokenList(const QTextBlock& block) const; // The token list includes white space and comment tokens

private:
   QChar _startDelimiter;
   QChar _endDelimiter;

   int _wordStartPos = -1;
   int _wordEndPos = -1;
};
