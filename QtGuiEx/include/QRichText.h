#ifndef QRICHTEXT_H
#define QRICHTEXT_H

#include "qtguiex_global.h"
#include <QTextLayout>

class QTGUIEX_EXPORT QRichText : public QString
{
public:
   QRichText() {}
   QRichText(const QRichText &other) : QString(other), _format(other._format) {}
   QRichText(const QString &other) : QString(other) {}
   ~QRichText() {}

   QRichText &operator=(const QRichText &rhs) { QString::operator=(rhs); _format = rhs._format; return *this; }
   QRichText &operator=(const QString &rhs) { QString::operator=(rhs); _format.clear(); return *this; }

   bool operator==(const QRichText &rhs) const { return compare(rhs) == 0; }
   bool operator<(const QRichText &rhs) const { return compare(rhs) < 0; }

   void clearFormat() { _format.clear(); }

   void addFormat(const QTextLayout::FormatRange& formatRange) { _format.append(formatRange); }

   const QVector<QTextLayout::FormatRange> &format() const { return _format; }
   void setFormat(const QVector<QTextLayout::FormatRange> &format) { _format = format; }

private:
   QVector<QTextLayout::FormatRange> _format;
};

#endif // QRICHTEXT_H
