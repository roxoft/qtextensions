#ifndef QDATEOFYEARVALIDATOR_H
#define QDATEOFYEARVALIDATOR_H

#include "qtguiex_global.h"
#include <QValidator>

class QTGUIEX_EXPORT QDateOfYearValidator : public QValidator
{
   Q_OBJECT

public:
   QDateOfYearValidator(QObject *parent = 0) : QValidator(parent), _separator(QLatin1Char('.')) {}
   virtual ~QDateOfYearValidator() {}

   virtual State validate(QString& input, int& pos) const override;
   virtual void fixup(QString& input) const override;

private:
   bool makeValid(int& day, int& month) const;

private:
   QChar _separator;
};

#endif // QDATEOFYEARVALIDATOR_H
