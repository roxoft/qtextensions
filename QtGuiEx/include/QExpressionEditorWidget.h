#pragma once
#include "qtguiex_global.h"
#include <QWidget>
#include <QSettingsEx>
#include <VarTree.h>
#include <ExpressionBase.h>
#include <Tokenizer.h>

class QCompleterDataSource;
class VarTree;
class HtNode;
class HtItemModel;

namespace Ui {
class QExpressionEditorWidget;
}

class AbstractEditHandler
{
public:
   virtual ~AbstractEditHandler() = default;

   virtual bool canEdit(const Token& token) = 0;
   virtual QString edit(QWidget* parent, const Token& token) = 0;
};

class QTGUIEX_EXPORT QExpressionEditorWidget : public QWidget
{
   Q_OBJECT

public:
   explicit QExpressionEditorWidget(const VarTree& varTree, const ResolverFunctionMap& functionMap, const Tokenizer::Context* context, QWidget *parent = nullptr); // Takes ownership of the context
   ~QExpressionEditorWidget() override;

   void expandFirstLevel();

   void setEditHandler(AbstractEditHandler* editHandler); // Takes ownership of the handler
   AbstractEditHandler* editHandler() const { return _editHandler; }

   void setExpression(const QString& expression);
   QString expression() const;

   void loadSettings(QSettingsEx settings);
   void saveSettings(QSettingsEx settings);

   bool hasExpressionChanged() const;

private slots:
   void on_lineEditVarFilter_textChanged(const QString& text);
   void on_lineEditFunctionFilter_textChanged(const QString& text);

   void on_treeViewVariables_doubleClicked(const QModelIndex& index);
   void on_treeViewVariables_keyPressed(QKeyEvent *e);

   void on_treeViewFunctions_doubleClicked(const QModelIndex& index);
   void on_treeViewFunctions_keyPressed(QKeyEvent *e);

   void onDocumentContentsChange(int position, int charsRemoved, int charsAdded);
   void onExpressionContextMenu(const QPoint& pos);

private:
   void createFunctionModel(const ResolverFunctionMap& functionMap);

private:
   Ui::QExpressionEditorWidget *ui = nullptr;

   VarTree _varTree;
   ResolverFunctionMap _functionMap;
   Tokenizer _tokenizer;
   HtItemModel* _varTreeModel = nullptr;
   HtItemModel* _functionTreeModel = nullptr;
   AbstractEditHandler* _editHandler = nullptr;
   QString _origExpression;
};
