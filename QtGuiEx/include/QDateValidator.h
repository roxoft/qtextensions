#ifndef QDATEVALIDATOR_H
#define QDATEVALIDATOR_H

#include "qtguiex_global.h"
#include <QValidator>
#include <QString>
#include <QChar>

class QTGUIEX_EXPORT QDateValidator : public QValidator
{
   Q_OBJECT

public:
   // The Sections are defined by the date format
   class Section
   {
   public:
      Section(QChar zeroDigit) : subType(0), isNumeric(false), maxLength(0), begin(0), _next(NULL), _zeroDigit(zeroDigit) {}
      ~Section() { if (_next) delete _next; }

      Section* next() { return _next; }
      const Section* next() const { return _next; }
      void setNext(Section* next) { if (_next) delete _next; _next = next; }

      int toNumber(const QString& text) const;

      void postInit();
      bool fixup(QString& text, int& cursorPos) const;
      void appendDefault(QString& text);

   public:
      QChar    type;
      int      subType;
      QString  delimiter;
      bool     isNumeric;
      int      maxLength;
      int      begin;
      QString  defaultValue;

      int      end(const QString& text) const { if (_next) return _next->begin; return text.length(); }
      int      endOfContent(const QString& text) const { return end(text) - delimiter.length(); }

   private:
      Section* _next;
      QChar    _zeroDigit;
   };

public:
   QDateValidator(QObject *parent = nullptr); // Does not use the locale from QValidator!
   ~QDateValidator() override { if (_firstSection) delete _firstSection; }

   Section* firstSection() { return _firstSection; }

   virtual State validate(QString& input, int& pos) const;

   bool fixupDate(QString& input);

   Section* sectionFromPos(int pos);
   Section* prevSection(Section* section) const { Section* prevSection = _firstSection; if (section == prevSection) return NULL; while (prevSection->next() != section) prevSection = prevSection->next(); return prevSection; }

   const QString& dateFormat() const { return _dateFormat; }
   QDate          toDate(const QString& input) const;

private:
   QString  _dateFormat;
   Section* _firstSection = nullptr;

   Section* findSection(QChar type);
   const Section* findSection(QChar type) const;
};

#endif // QDATEVALIDATOR_H
