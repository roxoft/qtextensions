#pragma once

#include <qtguiex_global.h>
#include <QTextEdit>

class QCompleterDataSource;
class QCompleterEx;

class QTGUIEX_EXPORT QTextEditEx : public QTextEdit
{
   Q_OBJECT

public:
   QTextEditEx(QWidget *parent = nullptr);
   virtual ~QTextEditEx();

   void setCompleterDataSource(QCompleterDataSource* dataSource);
   QCompleterDataSource* completerDataSource() const;

protected:
   bool event(QEvent* event) override;
   void keyPressEvent(QKeyEvent* event) override;
   void mousePressEvent(QMouseEvent* event) override;
   void focusOutEvent(QFocusEvent* event) override;

protected slots:
   virtual void on_completerCurrentIndexChanged(const QModelIndex& index);
   virtual void on_completerCurrentTextChanged(bool useCurrentIndex);
   virtual void on_completerMatchIndexChanged(const QModelIndex& index);

protected:
   QCompleterEx* _completer = nullptr;
   int _lastKeyPressed = 0;
};
