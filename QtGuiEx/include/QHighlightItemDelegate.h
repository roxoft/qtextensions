#ifndef QHIGHLIGHTITEMDELEGATE_H
#define QHIGHLIGHTITEMDELEGATE_H

#include "qtguiex_global.h"
#include "QStyledItemDelegateEx.h"

class QModelIndex;
class QIncrementalSearchWidget;

class QTGUIEX_EXPORT QHighlightItemDelegate : public QStyledItemDelegateEx
{
   Q_OBJECT
   Q_DISABLE_COPY(QHighlightItemDelegate)

public:
   QHighlightItemDelegate(QObject* parent = nullptr);
   ~QHighlightItemDelegate() override = default;

   QIncrementalSearchWidget* searchWidget() const { return _searchWidget; }
   void setSearchWidget(QIncrementalSearchWidget* searchWidget) { _searchWidget = searchWidget; }

public:
   QRichText formattedText(const QModelIndex &index, const QLocaleEx& locale) const override;

protected:
   QList<int> matchPositions(const QModelIndex& index, const QString& text) const;
   QRichText highlightedText(const QString& text, const QList<int>& matchPositions) const;

private:
   QIncrementalSearchWidget *_searchWidget = nullptr;
};

#endif // QHIGHLIGHTITEMDELEGATE_H
