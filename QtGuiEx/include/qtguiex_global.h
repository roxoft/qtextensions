#ifndef QTGUIEX_GLOBAL_H
#define QTGUIEX_GLOBAL_H

#include <QtCore/qglobal.h>

#ifdef QTGUIEX_LIB
# define QTGUIEX_EXPORT Q_DECL_EXPORT
#else
# define QTGUIEX_EXPORT Q_DECL_IMPORT
#endif

#endif // QTGUIEX_GLOBAL_H
