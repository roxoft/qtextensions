#ifndef QPLAINTEXTEDITEX_H
#define QPLAINTEXTEDITEX_H

#include "qtguiex_global.h"
#include <QPlainTextEdit>

// Extends QPlainTextEdit with a maximum length and different return key behaviour
class QTGUIEX_EXPORT QPlainTextEditEx : public QPlainTextEdit
{
   Q_OBJECT

   Q_PROPERTY(int maxLength READ maxLength WRITE setMaxLength)

public:
   QPlainTextEditEx(QWidget* parent = 0);
   virtual ~QPlainTextEditEx() {}

   int maxLength() const { return _maxLength; }
   void setMaxLength(int maxLength);

   bool returnChangesFocus() const { return _returnChangesFocus; }
   void setReturnChangesFocus(bool enabled) { _returnChangesFocus = enabled; }

protected:
   virtual void keyPressEvent(QKeyEvent * e) override;

private slots:
   void onTextChanged();

private:
   int _maxLength = -1;
   bool _returnChangesFocus = false;
};

#endif // QPLAINTEXTEDITEX_H
