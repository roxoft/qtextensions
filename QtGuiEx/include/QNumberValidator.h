#ifndef QNUMBERVALIDATOR_H
#define QNUMBERVALIDATOR_H

#include "qtguiex_global.h"
#include <QValidator>
#include <QDecimal.h>

class QTGUIEX_EXPORT QNumberValidator : public QValidator
{
   Q_OBJECT

public:
   QNumberValidator(int integerDigits = 16, int fractionalDigits = 2, QObject* parent = 0); // Uses group separators when fractionalDigits is not 0
   virtual ~QNumberValidator() {}

   int integerDigits() const { return _integerDigits; }
   void setIntegerDigits(int integerDigits) { if (integerDigits > 0 && integerDigits + _fractionalDigits < 505) _integerDigits = integerDigits; }

   int fractionalDigits() const { return _fractionalDigits; }
   void setFractionalDigits(int fractionalDigits) { if (fractionalDigits >= 0 && fractionalDigits < 256 && _integerDigits + fractionalDigits < 505) _fractionalDigits = fractionalDigits; }

   QDecimal::GroupSeparatorMode groupSeparatorMode() const { return _groupSeparatorMode; }
   void setGroupSeparatorMode(QDecimal::GroupSeparatorMode groupSeparatorMode) { _groupSeparatorMode = groupSeparatorMode; }
   bool useGroupSeparator() const { if (_groupSeparatorMode == QDecimal::GS_Auto) return _fractionalDigits > 0; return _groupSeparatorMode == QDecimal::GS_UseGroupSeparator; }

   bool emptyAllowed() const { return _emptyAllowed; }
   void setEmptyAllowed(bool allowed = true) { _emptyAllowed = allowed; }

   bool zeroAllowed() const { return _zeroAllowed; }
   void setZeroAllowed(bool allowed = true) { _zeroAllowed = allowed; }

   bool negativeAllowed() const { return _negativeAllowed; }
   void setNegativeAllowed(bool allowed = true) { _negativeAllowed = allowed; }

   QString defaultText() const;

   //virtual void fixup(QString& input) const;
   virtual State  validate(QString& input, int& pos) const override;
   virtual void   fixup(QString &input) const override;

   QChar decimalSeparator() const { return _decimalSeparator; }

   bool isNegativeSign(QChar qChar) const;

private:
   int   _integerDigits;
   int   _fractionalDigits;
   QDecimal::GroupSeparatorMode  _groupSeparatorMode;
   bool  _emptyAllowed;
   bool  _zeroAllowed;
   bool  _negativeAllowed;
   QChar _decimalSeparator;
   QChar _thousandsSeparator;
   QChar _zeroDigit;
   QChar _negativeSign;
};

#endif // QNUMBERVALIDATOR_H
