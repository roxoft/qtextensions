#pragma once

#include "qtguiex_global.h"
#include "QCompleterEx.h"
#include <VarTree.h>
#include <ExpressionBase>

class QTGUIEX_EXPORT QCompleterVarTreeDataSource : public QCompleterHtDataSource
{
public:
   QCompleterVarTreeDataSource(const VarTree& varTree, const ResolverFunctionMap& functionMap, QObject *parent = nullptr);
   ~QCompleterVarTreeDataSource() override = default;

   bool isFilterMode() const override { return true; }
   bool filter(const QString &filterText, int filterColumn) override; // Change the model according to the filter arguments and emit dataReady; return false if dataReady is emitted later
   void stopFilter() override;
   bool waitForModelReady(int timeout = -1) override { Q_UNUSED(timeout); return true; }
   bool hasMore() const override { return false; } // Returns true if there is more data than in the model returned by model()

   QModelIndex search(const QString &text, int column, bool caseSensitive) const override; // Returns the first model index which display text matches the given text
   QModelIndex find(const QModelIndex &start, int role, const Variant &value, Qt::MatchFlags flags = Qt::MatchFlags(Qt::MatchExactly)) const override; // Finds the first item matching value for the column given in start
   QModelIndex bestMatch(const QString& text, int column) const override;

private:
   VarTree _sourceTree;
   ResolverFunctionMap _functionMap;
};
