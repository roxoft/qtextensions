#pragma once

#include "qtguiex_global.h"
#include <QWidget>
#include <HtmlTools.h>

class QAbstractButton;
class QCompleterDataSource;
class QHBoxLayout;
class QTextCharFormat;
class QToolButton;
class QSelector;
class QExpressionTextEdit;

class QTGUIEX_EXPORT QTextEditControl : public QWidget
{
   Q_OBJECT

public:
   class QTGUIEX_EXPORT EmbeddedResourceManager : public AbstractHtmlEmbeddedResourceManager
   {
   public:
      EmbeddedResourceManager(QTextEditControl* textControl) : _textControl(textControl) {}
      ~EmbeddedResourceManager() override = default;

      QList<QPair<QString, QByteArray>> getResourceImages(const QString& html) const;

   protected:
      QString addResource(int index, const QByteArray& data, const QString& imgType) override;
      QPair<QByteArray, QString> resource(const QString& name) const override;

   private:
      QTextEditControl* _textControl;
   };

public:
   enum class ToolIcon { Bold, Italic, Underline, TextLeft, TextCenter, TextRight, TextJustify, Undo, Redo, Cut, Copy, Paste, TextHtml, TrueTypeFont, BitmapFont };

   static QList<QIcon> iconset;

public:
   QTextEditControl(QWidget *parent = nullptr, bool htmlOnly = false);
   virtual ~QTextEditControl();

   void setCompleterDataSource(QCompleterDataSource* completerDataSource, const QChar& startDelimiter = u'{', const QChar& endDelimiter = u'}');

   bool isHtml() const;
   void setIsHtml(bool isHtml);

   QString text() const;

   void setText(const QString& text);
   void setPlainText(const QString& text);
   void setHtmlText(const QString& text);

   void appendPlainText(const QString& text);
   void appendHtmlText(const QString& text);

   void insertText(const QString& text);
   void insertPlainText(const QString& text);
   void insertHtmlText(const QString& text);

   void addResource(const QImage& image, const QString& name);
   QImage getResourceImage(const QString& name);

   void addToggleHtmlButton();
   void addAction(QAction* action, QToolButton** pButton = nullptr);
   void addSeparator();

   QString currentExpression(); // The expression including its delimiters is selected and returned

signals:
   void textChanged();

private slots:
   // Rich edit slots
   void on_styleSelector_currentIndexChanged(const QModelIndex& index);
   void on_fontSelector_currentIndexChanged(const QModelIndex& index);
   void on_sizeSelector_currentIndexChanged(const QModelIndex& index);

   void on_colorToolButton_clicked();

   void on_boldToolButton_toggled(bool checked);
   void on_italicToolButton_toggled(bool checked);
   void on_underlineToolButton_toggled(bool checked);

   void on_textAlignment_clicked(QAbstractButton* button);
   void on_html_toggled(bool checked);

   void on_contentTextEdit_currentCharFormatChanged(const QTextCharFormat &format);
   void on_contentTextEdit_cursorPositionChanged();
   void on_contentTextEdit_textChanged();

   void on_clipboardData_changed();

private:
   void setupTextActions();

   void fontChanged(const QFont &f);
   void colorChanged(const QColor &c);
   void alignmentChanged(Qt::Alignment a);

private:
   QAction* _boldAction = nullptr;
   QAction* _italicAction = nullptr;
   QAction* _underlineAction = nullptr;
   QAction* _alignLeftAction = nullptr;
   QAction* _alignCenterAction = nullptr;
   QAction* _alignRightAction = nullptr;
   QAction* _alignAdjustedAction = nullptr;
   QAction* _undoAction = nullptr;
   QAction* _redoAction = nullptr;
   QAction* _cutAction = nullptr;
   QAction* _copyAction = nullptr;
   QAction* _pasteAction = nullptr;
   QAction* _isHtmlAction = nullptr;
   QHBoxLayout* _toolBarLayout = nullptr;
   QSelector* _styleSelector = nullptr;
   QSelector* _fontSelector = nullptr;
   QSelector* _sizeSelector = nullptr;
   QToolButton* _colorToolButton = nullptr;
   QToolButton* _alignLeftToolButton = nullptr;
   QToolButton* _alignCenterToolButton = nullptr;
   QToolButton* _alignRightToolButton = nullptr;
   QToolButton* _alignAdjustedToolButton = nullptr;
   QToolButton* _isHtmlToolButton = nullptr;
   QExpressionTextEdit* _contentTextEdit = nullptr;
};
