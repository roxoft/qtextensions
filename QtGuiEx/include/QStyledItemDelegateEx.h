#ifndef QSTYLEDITEMDELEGATEEX_H
#define QSTYLEDITEMDELEGATEEX_H

#include "qtguiex_global.h"
#include <QStyledItemDelegate>
#include <QLocaleEx.h>
#include "QRichText.h"
#include "QEditItemDelegate.h"
#include "QPaintItemDelegate.h"

class QStyleOptionViewItemEx;
class QTreeView;

/*
 To use heightForWidth correctly in tree views the parent must be the tree view
 because the section sizes are needed to calculate the corrects heights of
 multiline tree cells as for example in QtTestRunner.
 */
class QTGUIEX_EXPORT QStyledItemDelegateEx : public QStyledItemDelegate
{
   Q_OBJECT
   Q_DISABLE_COPY(QStyledItemDelegateEx)
   friend class QEditItemDelegate;
   friend class QPaintItemDelegate;

public:
   QStyledItemDelegateEx(QObject* parent = nullptr);
   ~QStyledItemDelegateEx() override = default;

   int minimumHeight() const { return _minHeight; }
   void setMinimumHeight(int minHeight) { _minHeight = minHeight; }

   void setHeightForWidth(bool heightForWidth) { _heightForWidth = heightForWidth; }

   void setCheckOnMouseDown(bool checkOnMouseDown) { _checkOnMouseDown = checkOnMouseDown; }

   bool  isContentAutoAlign() const { return _autoAlign; }
   void  setIsContentAutoAlign(bool autoAlign) { _autoAlign = autoAlign; }

   void setEditDelegate(int row, int column, QEditItemDelegate* editDelegate);
   QEditItemDelegate* editDelegate(int row, int column) const;
   void setPaintDelegate(int row, int column, QPaintItemDelegate* paintDelegate);
   QPaintItemDelegate* paintDelegate(int row, int column) const;

   virtual QRichText formattedText(const QModelIndex &index, const QLocaleEx& locale) const;

   // Painting

   void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const override;
   QSize sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const override;

   // Editing

   QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const override;
   void destroyEditor(QWidget *editor, const QModelIndex &index) const override;
   void setEditorData(QWidget *editor, const QModelIndex &index) const override;
   void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const override;
   void updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const override;

protected:
   void initStyleOption(QStyleOptionViewItem *option, const QModelIndex &index) const override;

   bool editorEvent(QEvent* event, QAbstractItemModel* model, const QStyleOptionViewItem& option, const QModelIndex& index) override;

private:
   static QRichText _formattedText(const QModelIndex &index, const QLocaleEx& locale);
   void _paint(QPainter *painter, const QStyleOptionViewItemEx &option, const QModelIndex &index) const;
   QSize _sizeHint(const QStyleOptionViewItemEx &option, const QModelIndex &index) const;

private:
   QString displayText(const QVariant& value, const QLocale& locale) const override;

private:
   bool _autoAlign = false;
   int  _minHeight = 0;
   bool _heightForWidth = false; // Use the tree view header for the column widths
   bool _checkOnMouseDown = false; // Check boxes are triggered on mouse down instead of mouse up

   QEditItemDelegate* _editItemDelegate = nullptr;
   QPaintItemDelegate* _paintItemDelegate = nullptr;

   QMap<int, QEditItemDelegate*> _columnEditDelegateMap;
   QMap<int, QEditItemDelegate*> _rowEditDelegateMap;
   QMap<int, QPaintItemDelegate*> _columnPaintDelegateMap;
   QMap<int, QPaintItemDelegate*> _rowPaintDelegateMap;
};

class QStyleOptionViewItemEx : public QStyleOptionViewItem
{
public:
   enum StyleOptionVersion { Version = 5 };
   enum class ContinuationStyle { Elide, Icon };

   QRichText richText;
   ContinuationStyle continuationHintStyle = ContinuationStyle::Elide;

   QStyleOptionViewItemEx() : QStyleOptionViewItem(Version) {}
   QStyleOptionViewItemEx(const QStyleOptionViewItemEx &other) : QStyleOptionViewItem(Version) { *this = other; }
   QStyleOptionViewItemEx(const QStyleOptionViewItem &other) : QStyleOptionViewItem(Version) { QStyleOptionViewItem::operator=(other); }
};

#endif // QSTYLEDITEMDELEGATEEX_H
