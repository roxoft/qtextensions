#pragma once

#include "qtguiex_global.h"
#include <QProxyStyle>

class QStyleOptionViewItemEx;

class QTGUIEX_EXPORT QExtendedProxyStyle : public QProxyStyle
{
   Q_OBJECT

public:
   enum class EntireCellCheckBoxPosition { Default, Centered, Full };

public:
   QExtendedProxyStyle(QStyle* style = nullptr);
   QExtendedProxyStyle(EntireCellCheckBoxPosition checkBoxPosition, QStyle* style = nullptr);
   ~QExtendedProxyStyle() override = default;

   void drawControl(ControlElement control, const QStyleOption *option, QPainter *painter, const QWidget *widget) const Q_DECL_OVERRIDE;
   QSize sizeFromContents(ContentsType ct, const QStyleOption* opt, const QSize& contentsSize, const QWidget* w) const override;
   QRect subElementRect(SubElement subElement, const QStyleOption* option, const QWidget* widget) const override;
   QIcon standardIcon(StandardPixmap standardIcon, const QStyleOption* option, const QWidget* widget) const override;

protected:
   void drawItemViewItem(const QStyleOptionViewItemEx *option, QPainter *painter, const QWidget *widget) const;

private:
   EntireCellCheckBoxPosition _checkBoxPosition = EntireCellCheckBoxPosition::Default;
};
