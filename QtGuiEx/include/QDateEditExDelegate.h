#ifndef QDATEEDITEXDELEGATE_H
#define QDATEEDITEXDELEGATE_H

#include "qtguiex_global.h"
#include "QEditItemDelegate.h"
#include <QDate>

class QTGUIEX_EXPORT QDateEditExDelegate : public QEditItemDelegate
{
   Q_OBJECT

public:
   QDateEditExDelegate(QObject *parent = nullptr, bool calendarPopup = false, bool emptyAllowed = true);
   ~QDateEditExDelegate() override;

   const QDate& minimumDate() const { return _minimumDate; }
   void setMinimumDate(const QDate& date) { _minimumDate = date; }
   void clearMinimumDate() { _minimumDate = QDate(); }

   const QDate& maximumDate() const { return _maximumDate; }
   void setMaximumDate(const QDate& date) { _maximumDate = date; }
   void clearMaximumDate() { _maximumDate = QDate(); }

   void setDateRange(const QDate& min, const QDate& max) { _minimumDate = min; _maximumDate = max; }

   bool emptyAllowed() const { return _emptyAllowed; }
   void setEmptyAllowed(bool allowed) { _emptyAllowed = allowed; }

protected:
   QWidget* createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const override;
   void setEditorData(QWidget *editor, const QModelIndex &index) const override;
   void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const override;
   void updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const override;

private:
   bool  _calendarPopup;
   QDate _minimumDate;
   QDate _maximumDate;
   bool  _emptyAllowed;
};

#endif // QDATEEDITEXDELEGATE_H
