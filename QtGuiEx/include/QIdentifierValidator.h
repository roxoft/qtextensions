#ifndef QIDENTIFIERVALIDATOR_H
#define QIDENTIFIERVALIDATOR_H

#include "qtguiex_global.h"
#include <QValidator>

class QTGUIEX_EXPORT QIdentifierValidator : public QValidator
{
   Q_OBJECT

public:
   QIdentifierValidator(QObject *parent = 0) : QValidator(parent) {}
   ~QIdentifierValidator() override = default;

   State validate(QString& input, int& pos) const override;

   void fixup(QString& input) const override;
};

#endif // QIDENTIFIERVALIDATOR_H
