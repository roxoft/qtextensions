#ifndef QTREEVIEWEX_H
#define QTREEVIEWEX_H

#include "qtguiex_global.h"
#include <QTreeView>
#include <QTime>
#include <HtNode.h>

class QPaintItemDelegate;
class QEditItemDelegate;
class SerStream;
class QLabel;
class HtItemModel;

/*
 QTreeViewEx extends QTreeView by many useful functions.
 QTreeViewEx uses internally QHighlightItemDelegate (indirectly derived from QStyledItemDelegate) to draw its content.
 If you want to provide your own delegates, derive them from QEditItemDelegate or QPaintItemDelegate!
 This way the background style, the content auto-align, the search highlighting and the minimum row height is not affected.
 
 The following features are added to QTreeView:
 - Minimum row height
 - New Drag & Drop implementation:
   - setAllowedDragActions() with valid DropActions activates the new Drag&Drop mechanism for dragging.
   - setAllowedDropActions() with valid DropActions activates the new Drag&Drop mechanism for drops.
   - setAllowedDragActions() and setAllowedDropActions() call setDragEnabled() and setAcceptDrops() accordingly.
   - With active dragDropOverwriteMode() the drop position indicator changes to a rectangle if the item can be replaced (for group nodes hoover the mouse before the indentation).
   - QAbstractItemView drag&drop settings are ignored (defaultDropAction(), dragDropMode()).
   - QAbstractItemModel drag&drop functions are ignored (mimeTypes(), mimeData(), canDropMimeData(), dropMimeData(), supportedDropActions(), supportedDragActions()).
   - Dropping with right mouse button shows a context menu.
   - Moving nodes is only possible if the target is derived from QAbstractItemView. Dropping on other targets would not remove the original nodes.
   - There is a new drop-indicator which can be enabled by calling setDropIndicatorStyle(QTreeViewEx::DropIndicatorStyle::Enhanced).
   - Not yet working is auto expand and auto scroll.
 - Optional "No data" text.
 - Optional column separator with resize functionality.
 - Reimplemented keyboard search.
 - Save and restore the treeview state.
 - Display a tool tip if text is hidden.
 - New double click signal which is emitted after the button was released.
 - Separate delegates for editing and painting.
 - New scroll behavior in ScrollPerPixel mode. It enables scrolling the last element to the top of the viewport and prevents undesired scrolling when tree elements are collapsed. By setting the MinimizeScrollRange option the old behavior can be restored.
 - Multiple check boxes can be toggled using shift-click.
 - Optionally check boxes can be toggled by click-moving the mouse over check boxes. This disables the shift-click feature.
*/
class QTGUIEX_EXPORT QTreeViewEx : public QTreeView
{
   friend class QExtendedProxyStyle;

   Q_OBJECT

   Q_PROPERTY(bool contentAutoAlign READ isContentAutoAlign WRITE setIsContentAutoAlign)
   Q_PROPERTY(bool columnSizeGrip READ isColumnResizeEnabled WRITE enableColumnResize)
   Q_PROPERTY(bool checkOnMouseMove READ checkOnMouseMove WRITE setCheckOnMouseMove)
   Q_PROPERTY(bool minimizeScrollRange READ minimizeScrollRange WRITE setMinimizeScrollRange)

   // Hide members
   void setItemDelegate(QAbstractItemDelegate* itemDelegate);
   QAbstractItemDelegate *itemDelegate() const;
   QAbstractItemDelegate *itemDelegate(const QModelIndex& index) const;
   void setItemDelegateForColumn(int column, QAbstractItemDelegate* itemDelegate);
   QAbstractItemDelegate *itemDelegateForColumn(int column) const;
   void setItemDelegateForRow(int row, QAbstractItemDelegate* itemDelegate);
   QAbstractItemDelegate *itemDelegateForRow(int row) const;

public:
   enum DropPosition { DropInvalid, DropAboveItem, DropBelowItem, DropReplaceItem, DropIntoItem };
   enum class DropIndicatorStyle { Standard, Enhanced };

public:
   QTreeViewEx(QWidget *parent = nullptr);
   ~QTreeViewEx() override = default;

   int minimumRowHeight() const;
   void setMinimumRowHeight(int minHeight);

   Qt::DropActions allowedDragActions() const;
   void setAllowedDragActions(Qt::DropActions dragActions);

   Qt::DropActions allowedDropActions() const;
   void setAllowedDropActions(Qt::DropActions dropActions);

   DropIndicatorStyle dropIndicatorStyle() const { return _dropIndicatorStyle; }
   void setDropIndicatorStyle(DropIndicatorStyle dropIndicatorStyle) { _dropIndicatorStyle = dropIndicatorStyle; }

   bool isColumnResizeEnabled() const;
   void enableColumnResize(bool enable = true);

   bool minimizeScrollRange() const { return _minimizeScrollRange; }
   void setMinimizeScrollRange(bool minimizeScrollRange) { _minimizeScrollRange = minimizeScrollRange; }

   bool checkOnMouseMove() const { return _checkOnMouseMove; }
   void setCheckOnMouseMove(bool checkOnMouseMove);

   void showNoData(bool show = true);
   void showColumnSeparator(bool show = true) { _showColumnSeparator = show; }

   bool  isContentAutoAlign() const;
   void  setIsContentAutoAlign(bool autoAlign);

   // The following functions replace the functions setItemDelegate(), setItemDelegateForColumn(), setItemDelegateForRow() in the base class QAbstractItemView
   void  setEditDelegate(QEditItemDelegate* editDelegate);
   QEditItemDelegate* editDelegate() const;
   void  setEditDelegateForColumn(int column, QEditItemDelegate* editDelegate);
   QEditItemDelegate* editDelegateForColumn(int column) const;
   void  setEditDelegateForRow(int row, QEditItemDelegate* editDelegate);
   QEditItemDelegate* editDelegateForRow(int row) const;
   void  setPaintDelegate(QPaintItemDelegate* paintDelegate);
   QPaintItemDelegate* paintDelegate() const;
   void  setPaintDelegateForColumn(int column, QPaintItemDelegate* paintDelegate);
   QPaintItemDelegate* paintDelegateForColumn(int column) const;
   void  setPaintDelegateForRow(int row, QPaintItemDelegate* paintDelegate);
   QPaintItemDelegate* paintDelegateForRow(int row) const;

   void setModel(QAbstractItemModel *model) override;

   bool eventFilter(QObject *watched, QEvent *e) override;

   void keyboardSearch(const QString &search) override;

   void scrollTo(const QModelIndex &index, ScrollHint hint = EnsureVisible) override;

   void dataChanged(const QModelIndex &topLeft, const QModelIndex &bottomRight, const QVector<int> &roles = QVector<int>()) override;

   virtual QByteArray saveState() const;
   virtual void restoreState(const QByteArray& data);

   void saveExpandedState(SerStream& stream, const QModelIndex& parent = QModelIndex()) const;
   void restoreExpandedState(SerStream& stream, const QModelIndex& parent = QModelIndex());

signals:
   void keyPressed(QKeyEvent *e);
   void doubleClickReleased(const QModelIndex& index);
   void canDropNodes(const QList<const HtNode*>& dropNodeList, const HtNode* targetNode, Qt::DropAction& dropAction, DropPosition& dropPosition);
   void nodeDropped(HtNode* node, const HtNode* sourceNode, Qt::DropAction dropAction);

protected:
   void keyPressEvent(QKeyEvent* e) override;
   bool viewportEvent(QEvent* e) override;
   void resizeEvent(QResizeEvent *e) override;
   void paintEvent(QPaintEvent* e) override;
   void drawRow(QPainter *painter, const QStyleOptionViewItem &options, const QModelIndex &index) const override;

   void mousePressEvent(QMouseEvent* e) override;
   void mouseMoveEvent(QMouseEvent* e) override;
   void mouseReleaseEvent(QMouseEvent* e) override;
   void mouseDoubleClickEvent(QMouseEvent* e) override;

   void dragEnterEvent(QDragEnterEvent* event) override;
   void dragMoveEvent(QDragMoveEvent* event) override;
   void dragLeaveEvent(QDragLeaveEvent* event) override;
   void dropEvent(QDropEvent* event) override;

   void updateGeometries() override;

   int sizeHintForColumn(int column) const override;

private:
   bool resizeColumn(QMouseEvent* e);
   void setDragPicture(QDrag* drag, const HtItemModel* model, const QList<const HtNode*>& dragNodeList) const;
   static QString getMimeText(const QModelIndexList& indexList);
   void setEventDropAction(QDropEvent* event) const;
   void setDropPosition(const QList<const HtNode*>& dropNodeList, const HtItemModel* model, const HtNode* targetNode, const QPoint& pos, Qt::DropAction dropAction);

   void setIndexIsConcealed(const QModelIndex& index, bool isConcealed) const; // Used by drawItemViewItem() in QStyledItemDelegateEx.cpp

private:
   bool     _minimizeScrollRange = false;
   bool     _checkOnMouseMove = false;
   QLabel*  _noDataWidget = nullptr;
   QString  _keyboardInput;
   QTime    _keyboardInputTime;
   bool     _showColumnSeparator = false;

   DropIndicatorStyle _dropIndicatorStyle = DropIndicatorStyle::Standard;
   Qt::DropActions _allowedDragActions = Qt::IgnoreAction;
   Qt::DropActions _allowedDropActions = Qt::IgnoreAction;
   QPoint _dragStartPosition;
   DropPosition _dropPosition = DropInvalid;
   QRect _dropItemRect;

   mutable QList<QPersistentModelIndex> _concealedTextIndexList;

   bool _doubleClicked = false;

   QModelIndex _mousePressedIndex;

   // Column resize
   bool  _resizeColumns = false;
   int   _logicalCursorColumn = -1;
   bool  _resizeRightColumn = false;
   int   _sectionSizeOffset = 0;
};

#endif // QTREEVIEWEX_H
