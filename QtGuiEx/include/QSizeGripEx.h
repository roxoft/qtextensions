#ifndef QSIZEGRIPEX_H
#define QSIZEGRIPEX_H

#include "qtguiex_global.h"
#include <QWidget>

class QMouseEvent;
class QPaintEvent;

class QTGUIEX_EXPORT QSizeGripEx : public QWidget
{
   Q_OBJECT

public:
   QSizeGripEx(QWidget* parent = 0);
   ~QSizeGripEx() {}

   virtual QSize sizeHint() const;

protected:
   virtual void mousePressEvent(QMouseEvent* e);
   virtual void mouseMoveEvent(QMouseEvent* e);
   virtual void paintEvent(QPaintEvent* e);

private:
   bool     _mousePressed;
   int      _offsetX;
   int      _offsetY;
};

#endif // QSIZEGRIPEX_H
