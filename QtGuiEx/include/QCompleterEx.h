#pragma once

#include "qtguiex_global.h"
#include <QObject>
#include <QPoint>
#include <HtStandardModel.h>

class QSelectorPopupListView;
class QLabel;

////////////////////////////////
// class QCompleterDataSource //
////////////////////////////////

class QTGUIEX_EXPORT QCompleterDataSource : public QObject
{
   Q_OBJECT
   Q_DISABLE_COPY(QCompleterDataSource)

public:
   QCompleterDataSource(QObject *parent = nullptr) : QObject(parent) {}
   ~QCompleterDataSource() override = default;

   virtual bool isFilterMode() const { return false; }
   virtual bool filter(const QString &filterText, int filterColumn); // Change the model according to the filter arguments and emit dataReady; return false if dataReady is emitted later
   virtual void stopFilter();
   virtual bool waitForModelReady(int timeout = -1);
   virtual bool hasMore() const { return false; } // Returns true if there is more data than in the model returned by model()

   virtual QString      text(const QModelIndex &index) const; // Returns the text to be displayed
   virtual QModelIndex  search(const QString &text, int column, bool caseSensitive) const; // Returns the first model index which display text matches the given text
   virtual QModelIndex  find(const QModelIndex &start, int role, const Variant &value, Qt::MatchFlags flags = Qt::MatchFlags(Qt::MatchExactly)) const; // Finds the first item matching value for the column given in start
   virtual QModelIndex  bestMatch(const QString& text, int column = 0) const; // Returns the best matching index

   virtual void hideInactiveItems(const Variant& selectedId) { Q_UNUSED(selectedId); } // Prepares the model for combo boxes with inactive items (the selectedId should always be visible)

   virtual QAbstractItemModel       *model() = 0; // Returns a pointer to the model (this way QSelector attaches the model to the views)
   virtual const QAbstractItemModel *model() const = 0;

   void addSeparator();
   bool isSeparator(const QModelIndex &index) const;
   bool isSeparator(int row);
   void setSeparator(const QModelIndex &index);
   void insertSeparator(int row);

signals:
   void dataReady(const QString& text, int column);
};

//////////////////////////////////
// class QCompleterHtDataSource //
//////////////////////////////////

class QTGUIEX_EXPORT QCompleterHtDataSource : public QCompleterDataSource
{
   Q_OBJECT
   Q_DISABLE_COPY(QCompleterHtDataSource)

public:
   QCompleterHtDataSource(QObject *parent = nullptr);
   ~QCompleterHtDataSource() override = default;

public:
   QModelIndex search(const QString &text, int column, bool caseSensitive) const override;
   QModelIndex find(const QModelIndex &start, int role, const Variant &value, Qt::MatchFlags flags = Qt::MatchFlags(Qt::MatchExactly)) const override;

   QAbstractItemModel       *model() override { return &_model; }
   const QAbstractItemModel *model() const override { return &_model; }

   HtNode         *item(int row);
   const HtNode   *item(int row) const;

   HtStandardModel* hierarchicalTable() { return &_model; }

protected:
   HtStandardModel _model;
};

////////////////////////
// class QCompleterEx //
////////////////////////

class QTGUIEX_EXPORT QCompleterEx : public QObject
{
   Q_OBJECT
   Q_DISABLE_COPY(QCompleterEx)

public:
   enum InsertPolicy { NoInsert, InsertAtTop, InsertAtCurrent, InsertAtBottom, InsertAfterCurrent, InsertBeforeCurrent, InsertAlphabetically };
   enum SortMode { SortIfColumns, SortEnabled, SortDisabled };

public:
   QCompleterEx(QWidget* widget, QCompleterDataSource* dataSource = nullptr, int column = 0, QObject* parent = nullptr);
   ~QCompleterEx() override;

   QCompleterDataSource* dataSource() const { return _dataSource; }
   bool hasDataSource() const { return _dataSource != nullptr; }
   QCompleterDataSource* dataSource();
   void setDataSource(QCompleterDataSource *source);

   QAbstractItemModel* model() const { if (_dataSource) return _dataSource->model(); return nullptr; }
   QAbstractItemModel* model();
   void setModel(QAbstractItemModel *model);

   int rowCount() const { if (_dataSource) return _dataSource->model()->rowCount(); return 0; }
   int itemCount() const { return rowCount(); }

   QModelIndex addItem(const QString &text, const Variant &userData = Variant());
   QModelIndex addItem(const QIcon &icon, const QString &text, const Variant &userData = Variant());
   void        addItems(const QStringList &texts);
   void        addSeparator();

   // If the simplified text is not empty it is added as new item to the drop down list and set as current item
   QModelIndex addText(const QString& text);

   QModelIndex insertItem(int row, const QString &text, const Variant &userData = Variant());
   QModelIndex insertItem(int row, const QIcon &icon, const QString &text, const Variant &userData = Variant());
   void        insertItems(int row, const QStringList &list);
   void        insertSeparator(int row);

   void removeItem(int row);

   InsertPolicy   insertPolicy() const { return _insertPolicy; }
   void           setInsertPolicy(InsertPolicy policy) { _insertPolicy = policy; }

   int   column() const { return _column; }
   void  setColumn(int col) { _column = col; }
   int   modelColumn() const { return _column; }
   void  setModelColumn(int column) { _column = column; }

   int   defaultItem() const { return _defaultItem; }
   void  setDefaultItem(int defaultItem) { _defaultItem = defaultItem; }

   bool isFreeText() const { return _isFreeText; }
   void setIsFreeText(bool isFree) { _isFreeText = isFree; }

   SortMode sortMode() const { return _sortMode; }
   void     setSortMode(SortMode sortMode) { _sortMode = sortMode; }

   const QModelIndex &currentIndex() const { return _currentIndex; }

   Variant  itemData(int row, int role = Qt::UserRole) const;
   void     setItemData(int row, const Variant &value, int role = Qt::UserRole);
   QIcon    itemIcon(int row) const;
   void     setItemIcon(int row, const QIcon &icon);
   QString  itemText(int row) const;
   void     setItemText(int row, const QString &text);

   // Find the model index matching value. If role is Qt::DisplayRole the search is string based (case sensitive). For the other roles the search is Variant based.
   // If there is need to search differently use dataSource()->find().
   QModelIndex find(const Variant& value, int role = Qt::DisplayRole) const;

   QModelIndex bestMatch(const QString& text) const;

   void showSelectionList(const QString& text, const QPoint& topLeft = QPoint(), int width = 0);
   void showSelectionList(const QModelIndex& index, const QPoint& topLeft = QPoint(), int width = 0); // index must be valid to show the popup
   void hideSelectionList();

   bool updateSelectList(const QString& text); // Return true if matchIndexChanged was emitted immediately
   void setCurrentText(const QString& text);
   bool acceptSelectedItem(); // Returns false if the popup is not visible or no item was selected

   bool isPopupVisible() const;

signals:
   void currentIndexChanged(const QModelIndex& index);
   void currentTextChanged(bool useCurrentIndex);
   void matchIndexChanged(const QModelIndex& index);

public slots:
   void clear();
   void setCurrentIndex(const QModelIndex& index);
   void setCurrentRow(int row);

protected:
   bool eventFilter(QObject* watched, QEvent* event) override;

private slots:
   void onDataSourceReady(const QString& text, int column);

   void onItemSelected(int row);

   void onModelColumnsInserted(const QModelIndex & parent, int start, int end);
   void onModelColumnsMoved(const QModelIndex & sourceParent, int sourceStart, int sourceEnd, const QModelIndex & destinationParent, int destinationColumn);
   void onModelColumnsRemoved(const QModelIndex & parent, int start, int end);
   void onModelDataChanged(const QModelIndex & topLeft, const QModelIndex & bottomRight);
   void onModelLayoutChanged();
   void onModelModelReset();
   void onModelRowsInserted(const QModelIndex & parent, int start, int end);
   void onModelRowsMoved(const QModelIndex & sourceParent, int sourceStart, int sourceEnd, const QModelIndex & destinationParent, int destinationRow);
   void onModelRowsRemoved(const QModelIndex & parent, int start, int end);

private:
   void connectModel();
   void disconnectModel();
   void waitForModelReady();
   void updateCurrentIndex(int row);
   void showPopup(QPoint topLeft, int width);
   void resizeColumnsToContents();

private:
   QWidget* _widget = nullptr;
   QCompleterDataSource* _dataSource = nullptr;
   QModelIndex _currentIndex;

   // Pop-up attributes
   QWidget*                _popup = nullptr;
   QSelectorPopupListView* _matchListView = nullptr;
   QLabel*                 _statusBarLabel = nullptr;
   int                     _rowCount = -1;
   int                     _columnCount = -1;

   // Selector behaviour
   InsertPolicy   _insertPolicy = InsertAtBottom;
   int            _column = 0; // Column used for display
   int            _defaultItem = -1;
   bool           _isFreeText = false;
   SortMode       _sortMode = SortIfColumns;
};
