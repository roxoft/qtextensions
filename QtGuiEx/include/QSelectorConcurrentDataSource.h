#ifndef QSELECTORCONCURRENTDATASOURCE_H
#define QSELECTORCONCURRENTDATASOURCE_H

#include "qtguiex_global.h"
#include "QSelector.h"
#include <WorkerThread>
#include <HtStandardModel>

class HtStandardNode;

class QTGUIEX_EXPORT QSelectorItemLoader : public WorkerThread
{
   Q_OBJECT
   Q_DISABLE_COPY(QSelectorItemLoader)

public:
   QSelectorItemLoader(QObject *parent = nullptr);
   ~QSelectorItemLoader() override;

   virtual QStringList headerList() const { return QStringList(); }

   const QString  &filterText() const { return _filterText; }
   int            filterColumn() const { return _filterColumn; }
   void           setFilter(const QString &filterText, int filterColumn) { _filterText = filterText; _filterColumn = filterColumn; }

   void replaceList(HtStandardNode *rootNode);
   bool hasMore() const { return _hasMore; }

protected:
   int      _maxItems;
   QString  _filterText;
   int      _filterColumn;
   bool     _hasMore;

   QVector<HtNode *> _itemList;
};

// QSelectorConcurrentDataSource is a filtering data source. Matches items with a startsWith semantics instead of matchIndicator semantics.
class QTGUIEX_EXPORT QSelectorConcurrentDataSource : public QCompleterHtDataSource
{
   Q_OBJECT
   Q_DISABLE_COPY(QSelectorConcurrentDataSource)

public:
   explicit QSelectorConcurrentDataSource(QSelectorItemLoader *loader = nullptr, QObject *parent = nullptr);
   ~QSelectorConcurrentDataSource() override;

   void setLoader(QSelectorItemLoader *loader);

   bool isFilterMode() const override { return true; }
   bool filter(const QString &filterText, int filterColumn) override; // Sets the filter for the data source, model() should only contain entries matching the filter
   void stopFilter() override;
   bool waitForModelReady(int timeout = -1) override;
   bool hasMore() const override { return _hasMore; } // Returns true if there is more data than in the model returned by model()

   QModelIndex bestMatch(const QString& text, int column) const override;

protected:
   QSelectorItemLoader *_loader;

private slots:
   void onItemListReady();

private:
   void prepareModel();
   void fillModel();

private:
   QString  _filterText;
   int      _filterColumn;
   bool     _hasMore;

   bool  _resultPending;
   int   _prepSigCount;
};

#endif // QSELECTORCONCURRENTDATASOURCE_H
