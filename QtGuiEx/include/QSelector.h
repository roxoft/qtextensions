#ifndef QSELECTOR_H
#define QSELECTOR_H

#include "qtguiex_global.h"
#include <QLineEdit>
#include <QModelIndex>
#include "QCompleterEx.h"

class QSelector;

class QTGUIEX_EXPORT QSelectorGroup : public QObject
{
   Q_OBJECT
   Q_DISABLE_COPY(QSelectorGroup)

public:
   QSelectorGroup(QObject *parent = 0);
   ~QSelectorGroup() override = default;

   QCompleterDataSource* dataSource() { return _dataSource; }
   void setDataSource(QCompleterDataSource *dataSource);

   void addSelector(QSelector *selector);

signals:
   void currentRowChanged(int row);

private slots:
   void onCurrentIndexChanged(const QModelIndex &index);

private:
   QList<QSelector *> _selectors;
   QCompleterDataSource* _dataSource = nullptr;
   bool _processingSignal = false;
};

/*
 QSelector is an edit field (QLineEdit) with a popup that shows a list of items the user can select from.
 Combined with QComboBoxEx this is similar to a QComboBox.

 QSelector extends the popup of a QComboBox with the following features:
 - The view can be a list or a sortable table
 - The popup can be resized by the user
 - Optionally only items matching the text in the edit line are displayed (item filter)
 - Optionally free text can be entered and added to the list
 - Dynamic filtering: Possible items are loaded on demand (when the user enters text).
   The number of loaded items together with a hint if more items are available is displayed at the bottom of the list (beside the size grip).
*/
class QTGUIEX_EXPORT QSelector : public QLineEdit
{
   Q_OBJECT
   Q_DISABLE_COPY(QSelector)

   Q_PROPERTY(int defaultItem READ defaultItem WRITE setDefaultItem) // Row index of an item to be displayed instead of an invalid index
   Q_PROPERTY(bool isFreeText READ isFreeText WRITE setIsFreeText)
   Q_ENUMS(SortMode)
   Q_PROPERTY(SortMode sortMode READ sortMode WRITE setSortMode)
   Q_ENUMS(InsertPolicy)
   Q_PROPERTY(InsertPolicy insertPolicy READ insertPolicy WRITE setInsertPolicy)

public:
   enum InsertPolicy { NoInsert, InsertAtTop, InsertAtCurrent, InsertAtBottom, InsertAfterCurrent, InsertBeforeCurrent, InsertAlphabetically };
   enum SortMode { SortIfColumns, SortEnabled, SortDisabled };

public:
   QSelector(QWidget *parent = nullptr, QCompleterDataSource *dataSource = nullptr, int column = 0, QWidget *alignTo = nullptr);
   ~QSelector() override = default;

   QCompleterDataSource* dataSource() const { return const_cast<const QCompleterEx*>(_completer)->dataSource(); }
   QCompleterDataSource* dataSource() { return _completer->dataSource(); }
   void setDataSource(QCompleterDataSource *source) { _completer->setDataSource(source); geometryChanged(); }

   QAbstractItemModel   *model() const { return const_cast<const QCompleterEx*>(_completer)->model(); }
   QAbstractItemModel   *model() { return _completer->model(); }
   void                 setModel(QAbstractItemModel *model) { _completer->setModel(model); }

   int rowCount() const { return _completer->rowCount(); }
   int itemCount() const { return rowCount(); }

   QModelIndex addItem(const QString &text, const Variant &userData = Variant()) { return _completer->addItem(text, userData); }
   QModelIndex addItem(const QIcon &icon, const QString &text, const Variant &userData = Variant()) { return _completer->addItem(icon, text, userData); }
   void        addItems(const QStringList &texts) { _completer->addItems(texts); }
   void        addSeparator() { _completer->addSeparator(); }

   QModelIndex addEditText(); // Adds the displayed text as new item to the drop down list if it is not empty (the text will be simplified) and set it as current item

   QModelIndex insertItem(int row, const QString& text, const Variant& userData = Variant());
   QModelIndex insertItem(int row, const QIcon &icon, const QString &text, const Variant &userData = Variant()) { auto index = _completer->insertItem(row, icon, text, userData); geometryChanged(); return index; }
   void        insertItems(int row, const QStringList &list) { _completer->insertItems(row, list); }
   void        insertSeparator(int row) { _completer->insertSeparator(row); }

   void removeItem(int row) { _completer->removeItem(row); geometryChanged(); }

   InsertPolicy   insertPolicy() const { return (InsertPolicy)_completer->insertPolicy(); }
   void           setInsertPolicy(InsertPolicy policy) { _completer->setInsertPolicy((QCompleterEx::InsertPolicy)policy); }

   int   column() const { return _completer->column(); }
   void  setColumn(int col) { _completer->setColumn(col); }
   int   modelColumn() const { return column(); }
   void  setModelColumn(int column) { setColumn(column); }

   int   defaultItem() const { return _completer->defaultItem(); }
   void  setDefaultItem(int defaultItem) { _completer->setDefaultItem(defaultItem); }

   bool isFreeText() const { return _completer->isFreeText(); }
   void setIsFreeText(bool isFree) { _completer->setIsFreeText(isFree); }

   SortMode sortMode() const { return (SortMode)_completer->sortMode(); }
   void     setSortMode(SortMode sortMode) { _completer->setSortMode((QCompleterEx::SortMode)sortMode); }

   QString currentText() const { return text(); }

   const QModelIndex &currentIndex() const { return _completer->currentIndex(); }

   Variant  itemData(int row, int role = Qt::UserRole) const { return _completer->itemData(row, role); }
   void     setItemData(int row, const Variant &value, int role = Qt::UserRole) { _completer->setItemData(row, value, role); geometryChanged(); }
   QIcon    itemIcon(int row) const;
   void     setItemIcon(int row, const QIcon &icon) { _completer->setItemIcon(row, icon); geometryChanged(); }
   QString  itemText(int row) const { return _completer->itemText(row); }
   void     setItemText(int row, const QString &text) { _completer->setItemText(row, text); geometryChanged(); }

   // Find the model index matching value. If role is Qt::DisplayRole the search is string based (case sensitive). For the other roles the search is Variant based.
   // If there is need to search differently use dataSource()->find().
   QModelIndex find(const Variant& value, int role = Qt::DisplayRole) const { return _completer->find(value, role); }

   QModelIndex bestMatch(const QString& text) const { return _completer->bestMatch(text); }

public:
   bool   event(QEvent* e) override;
   QSize  sizeHint() const override;

public slots:
   void clear() { clearAll(); }
   void clearAll();
   void clearEditText();
   void setCurrentIndex(const QModelIndex& index);
   void setCurrentRow(int row);
   void setEditText(const QString &text);
   void showSelectionList();

signals:
   void currentIndexChanged(const QModelIndex& index);

protected:
   void keyPressEvent(QKeyEvent* e) override;
   void mousePressEvent(QMouseEvent* e) override;
   void focusOutEvent(QFocusEvent *e) override;

private slots:
   void on_editingFinished();
   void on_textEdited(const QString& text);
   void on_textChanged(const QString& text);

   void on_completerCurrentIndexChanged(const QModelIndex& index);
   void on_completerCurrentTextChanged(bool useCurrentIndex);
   void on_completerMatchIndexChanged(const QModelIndex& index);

private:
   void geometryChanged();
   void setTextColor(const QColor &color);
   void resetTextColor();

private:
   QCompleterEx* _completer = nullptr;
   QWidget* _alignTo = nullptr; // Widget to align the pop-up to
   int _lastKeyPressed = 0;
   bool _isEditing = false;
   bool _hasColor = false;
};

#endif // QSELECTOR_H
