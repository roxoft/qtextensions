#ifndef TESTDIALOG_H
#define TESTDIALOG_H

#include "qtguiex_global.h"
#include <QMainWindow>
#include <TestManager.h>

class HtNode;
class HtItemModel;

namespace Ui { class TestDialog; };

class QTGUIEX_EXPORT TestDialog : public QMainWindow
{
   Q_OBJECT

public:
   TestDialog(QWidget *parent = 0);
   ~TestDialog();

   void setTestWithGui(bool testWithGui);

   void loadTestDll(const QString& dllPath, const QString& dataPath = QString());
   void setTestDataDir(const QString& path);

protected:
   void showEvent(QShowEvent* e) override;

signals:
   void closeSignal();

private slots:
   void on_actionOpenTestDll_triggered();
   void on_actionOpenDataDirectory_triggered();
   void on_actionExecuteAll_triggered();
   void on_actionExecuteSelected_triggered();

   void on_testTreeView_doubleClicked(const QModelIndex& index);

private:
   static void clearTree(HtNode* node, int level = 0);
   StatusMessage::Severity executeTestModule(HtNode* testModuleNode);
   StatusMessage::Severity executeTestSuite(TestModule* testModule, HtNode* testSuiteNode);
   static StatusMessage::Severity executeSingleTest(TestSuite suite, HtNode* testCaseNode);

private:
   Ui::TestDialog *ui;

   TestManager   _testManager;
   HtItemModel*  _testModel;
};

#endif // TESTDIALOG_H
