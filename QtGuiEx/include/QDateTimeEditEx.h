#ifndef QDATETIMEEDITEX_H
#define QDATETIMEEDITEX_H

#include "qtguiex_global.h"
#include <QDateTimeEdit>
#include <QLineEdit>

class QTGUIEX_EXPORT QDTEEXLineEdit : public QLineEdit
{
   Q_OBJECT

public:
   QDTEEXLineEdit(QWidget *parent = 0) : QLineEdit(parent) {}
   ~QDTEEXLineEdit() {}

signals:
   void mouseButtonUp(QMouseEvent *event);

protected:
   virtual void mouseReleaseEvent(QMouseEvent *event);
   virtual void mouseDoubleClickEvent(QMouseEvent *event);
};

// Control to enter date and time values. It can be combined with a QComboBoxEx to show a calendar popup widget.
//
// QDateTimeEditEx has the following properties:
// - Getting focus selects the day
// - A single click on an element selects the element (day, month or year etc.).
// - A double click has no special effect.
// - A triple click is not supported.
// - The value of elements can be incremented and decremented using the mouse wheel or the arrow keys.
// - Characters are inserted at the cursor position.
// - Is an element complete no further characters can be added.
// - The cursor moves over the dot to the next element and selects it.
// - It can be configured if the date edit is allowed to be empty.
// - A maximum and minimum value can be defined.
//
class QTGUIEX_EXPORT QDateTimeEditEx : public QDateTimeEdit
{
   Q_OBJECT

public:
    QDateTimeEditEx(QWidget *parent = 0);
    ~QDateTimeEditEx() {}

protected slots:
   void mouseButtonUp(QMouseEvent *event);
};

#endif // QDATETIMEEDITEX_H
