#pragma once

#include "qtguiex_global.h"
#include <QObject>

class QStyleOptionViewItemEx;
class QStyledItemDelegateEx;
class QPainter;

class QTGUIEX_EXPORT QPaintItemDelegate : public QObject
{
   Q_OBJECT
   Q_DISABLE_COPY(QPaintItemDelegate)
   friend class QStyledItemDelegateEx;

public:
   QPaintItemDelegate(QObject* parent = nullptr);
   ~QPaintItemDelegate() override;

protected:
   virtual void paint(QPainter *painter, const QStyleOptionViewItemEx& option, const QModelIndex &index) const;
   virtual QSize sizeHint(const QStyleOptionViewItemEx& option, const QModelIndex &index) const;

private:
   const QStyledItemDelegateEx* _itemDelegate = nullptr;
};
