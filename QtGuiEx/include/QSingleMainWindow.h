#ifndef QSINGLEMAINWINDOW_H
#define QSINGLEMAINWINDOW_H

#include "qtguiex_global.h"
#include <QMainWindow>

class QTGUIEX_EXPORT QSingleMainWindow : public QMainWindow
{
   Q_OBJECT

public:
   QSingleMainWindow(QWidget *parent = 0, Qt::WindowFlags flags = Qt::Widget) : QMainWindow(parent, flags) {}

   bool sendCommand(int command); // Must be called before show!

protected:
#if QT_VERSION >= QT_VERSION_CHECK(6,0,0)
   virtual bool nativeEvent(const QByteArray &eventType, void *message, qintptr* result) override;
#elif QT_VERSION >= QT_VERSION_CHECK(5,0,0)
   virtual bool nativeEvent(const QByteArray &eventType, void *message, long *result) override;
#else
   virtual bool winEvent(MSG *message, long *result) override;
#endif

   virtual void appCommand(int command) = 0;
};

#endif // QSINGLEMAINWINDOW_H
