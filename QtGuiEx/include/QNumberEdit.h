#ifndef QNUMBEREDIT_H
#define QNUMBEREDIT_H

#include "qtguiex_global.h"
#include <QLineEdit>
#include <QDecimal.h>

class QNumberValidator;
class QMouseEvent;
class QWheelEvent;

// QNumberEdit is for editing amounts.
//
// The total number of characters (including sign, decimal and thousand separators) must conform to the limits of QLineEdit.
//
// Configurable:
// - Maximum number of integer digits
// - Number of fractional digits
// - If negative numbers are allowed
// - If group separators (thousand separators) are displayed
// - If empty is allowed
// - If zero is allowed
//
// Behavior:
// - A mouse click in the control before the number select the integer part of the number (or the complete number if there is no fractional part)
// - If the number of integer or fractional digits is exhausted additional digits are still inserted at the cursor position but the excess digits are removed at the end
// - The comma key insert a decimal point at the cursor position and completes or cuts the fractional digits accordingly
// - A double click selects a section. A section comprises all digits between two separators (decimal or thousand separator)
// - A triple click selects the complete number
// - Leading zeros are not displayed
//
class QTGUIEX_EXPORT QNumberEdit : public QLineEdit
{
   Q_OBJECT

   Q_PROPERTY(int integerDigits READ integerDigits WRITE setIntegerDigits)
   Q_PROPERTY(int fractionalDigits READ fractionalDigits WRITE setFractionalDigits)
   Q_PROPERTY(GroupSeparatorMode groupSeparatorMode READ guiGroupSeparatorMode WRITE setGuiGroupSeparatorMode)
   Q_PROPERTY(bool emptyAllowed READ emptyAllowed WRITE setEmptyAllowed)
   Q_PROPERTY(bool zeroAllowed READ zeroAllowed WRITE setZeroAllowed)
   Q_PROPERTY(bool negativeAllowed READ negativeAllowed WRITE setNegativeAllowed)

public:
   enum GroupSeparatorMode { AutoGroupSeparators, UseGroupSeparators, OmitGroupSeparators }; // TODO: Check QLocale::NumberOptions
   Q_ENUMS(GroupSeparatorMode)

public:
   QNumberEdit(QWidget *parent = 0, int integerDigits = 14, int fractionalDigits = 2, bool emptyAllowed = false);
   virtual ~QNumberEdit();

   QDecimal value() const;
   void     setValue(const QDecimal &value);

   int integerDigits() const;
   void setIntegerDigits(int integerDigits);

   int fractionalDigits() const;
   void setFractionalDigits(int fractionalDigits);

   QDecimal::GroupSeparatorMode groupSeparatorMode() const;
   void setGroupSeparatorMode(QDecimal::GroupSeparatorMode groupSeparatorMode);

   bool emptyAllowed() const;
   void setEmptyAllowed(bool allowed = true);

   bool zeroAllowed() const;
   void setZeroAllowed(bool allowed = true);

   bool negativeAllowed() const;
   void setNegativeAllowed(bool allowed = true);

   int optimalWidth() const;

protected:
   virtual void mouseReleaseEvent(QMouseEvent* e) override;
   virtual void wheelEvent(QWheelEvent *e) override;

private:
   // Wrapper of the group separator mode for the designer
   GroupSeparatorMode guiGroupSeparatorMode() const;
   void setGuiGroupSeparatorMode(GroupSeparatorMode groupSeparatorMode);

   void onPropertyChanged();

private:
   QNumberValidator* _validator;
};

Q_DECLARE_METATYPE(QNumberEdit::GroupSeparatorMode)

#endif // QNUMBEREDIT_H
