#include "stdafx.h"
#include "TestDialog.h"

#include <HtItemModel.h>
#include <HtStandardModel.h>
#include <QFileDialog>
#include <QFileInfo>
#include <QtGuiVariantTypes.h>

#include "ui_TestDialog.h"
#ifdef Q_OS_WIN
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#endif

static void addResults(HtNode* rootNode, FunctionStatus status)
{
   QStringList currentPath;
   HtNode* currentNode = rootNode;

   for (auto&& message : status.messages())
   {
      if (message.context() != currentPath)
      {
         currentNode = rootNode;
         for (auto component : message.context())
         {
            if (!currentNode->hasChildren() || currentNode->child(currentNode->childCount() - 1)->attribute(0).toString() != component)
            {
               currentNode = currentNode->appendChild();
               currentNode->setAttribute(0, component);
            }
            else
            {
               currentNode = currentNode->child(currentNode->childCount() - 1);
            }
         }

         currentPath = message.context();
      }

      auto node = currentNode->appendChild();

      node->setAttribute(0, message.severity() == StatusMessage::S_Warning ? "Warnung" : "Fehler");
      node->setAttribute(1, message.message());
      //node->setAttribute(1, message.severity() == StatusMessage::S_Warning ? Qt::yellow : Qt::red, Qt::BackgroundRole);
      node->setAttribute(1, QIcon(message.severity() == StatusMessage::S_Warning ? ":/icons/free/Warning.png" : ":/icons/free/Error.png"), Qt::DecorationRole);
   }
}

TestDialog::TestDialog(QWidget *parent)
    : QMainWindow(parent), _testModel(nullptr)
{
   ui = new Ui::TestDialog();
   ui->setupUi(this);

   _testModel = new HtStandardModel(this);

   _testModel->setHeader(0, tr("Test"));
   _testModel->setHeader(1, tr("Result"));
   _testModel->setHeader(2, tr("Data folder"));

   ui->testTreeView->setModel(_testModel);
   ui->testTreeView->setVerticalScrollMode(QTreeView::ScrollPerPixel);

   _testManager.setParentWindow(this);

   ui->actionGUI->setChecked(true);
}

TestDialog::~TestDialog()
{
   delete ui;
}

void TestDialog::setTestWithGui(bool testWithGui)
{
   ui->actionGUI->setChecked(testWithGui);
}

void TestDialog::loadTestDll(const QString& dllPath, const QString& dataPath)
{
   if (!dllPath.isEmpty())
   {
#ifdef Q_OS_WIN
      SetErrorMode(0); // This enables displaying a message box when loading a DLL and depending DLLs are missing!
#endif

      FunctionStatus status;
      QList<TestModule*> loadedModules;
      auto offset = _testManager.testModules().count();

      if (dllPath.endsWith(".csv", Qt::CaseInsensitive))
         loadedModules = _testManager.addModulesFromFile(status, dllPath);
      else
         loadedModules << _testManager.addModule(status, dllPath);

      for (auto iModule = 0; iModule < loadedModules.count(); iModule++)
      {
         auto testModule = loadedModules.at(iModule);

         if (testModule)
         {
            if (!dataPath.isEmpty())
               testModule->setDataDirectory(dataPath);

            auto testSuites = testModule->testSuites();
            auto moduleNode = _testModel->rootNode()->appendChild();
            moduleNode->setAttribute(0, QFileInfo(testModule->libraryFile()).fileName());
            moduleNode->setAttribute(0, testModule->libraryFile(), Qt::ToolTipRole);
            moduleNode->setAttribute(0, offset + iModule, Qt::UserRole);
            moduleNode->setAttribute(2, QFileInfo(testModule->dataDirectory()).fileName());
            moduleNode->setAttribute(2, testModule->dataDirectory(), Qt::ToolTipRole);
            for (int iSuite = 0; iSuite < testSuites.count(); ++iSuite)
            {
               auto suiteNode = moduleNode->appendChild();

               suiteNode->setAttribute(0, testSuites[iSuite].name());
               suiteNode->setAttribute(0, iSuite, Qt::UserRole);

               for (int iMethod = 0; iMethod < testSuites[iSuite].methodCount(); ++iMethod)
               {
                  auto testNode = suiteNode->appendChild();

                  testNode->setAttribute(0, testSuites[iSuite].methodSignature(iMethod));
                  testNode->setAttribute(0, iMethod, Qt::UserRole);
               }
            }

            ui->testTreeView->expandAll();
         }
      }

      if (status.hasError())
         statusBar()->showMessage(status.completeMessage(), 10000);
   }
}

void TestDialog::setTestDataDir(const QString& path)
{
   if (path.isEmpty())
      return;

   for (int i = 0; i < _testModel->rootNode()->childCount(); ++i)
   {
      auto testModuleNode = _testModel->rootNode()->child(i);
      const auto iModule = testModuleNode->attribute(0, Qt::UserRole).toInt();
      auto testModule = _testManager.testModules().at(iModule);

      testModule->setDataDirectory(path);

      testModuleNode->setAttribute(2, QFileInfo(testModule->dataDirectory()).fileName());
      testModuleNode->setAttribute(2, testModule->dataDirectory(), Qt::ToolTipRole);
   }
}

void TestDialog::showEvent(QShowEvent* e)
{
   QMainWindow::showEvent(e);
   ui->testTreeView->setColumnWidth(0, 250);
   ui->testTreeView->setColumnWidth(1, ui->testTreeView->width() - 400);
}

void TestDialog::on_actionOpenTestDll_triggered()
{
   QString dirOpen;

   if (_testModel->rootNode()->childCount())
      dirOpen = QFileInfo(_testModel->rootNode()->child(_testModel->rootNode()->childCount() - 1)->attribute(0).toString()).path();

   QString fileName = QFileDialog::getOpenFileName(this, tr("Open test DLL"), dirOpen, tr("DLL (*.dll *.so)"));

   loadTestDll(fileName);
}

void TestDialog::on_actionOpenDataDirectory_triggered()
{
   auto currentIndex = ui->testTreeView->currentIndex();

   if (!currentIndex.isValid())
      currentIndex = _testModel->index(0, 0);

   if (!currentIndex.isValid())
      return;

   while (currentIndex.parent().isValid())
      currentIndex = currentIndex.parent();

   auto libNode = _testModel->nodeFromIndex(currentIndex);
   auto dirOpen = libNode->attribute(2, Qt::ToolTipRole).toString();

   if (dirOpen.isEmpty())
      dirOpen = QFileInfo(libNode->attribute(0, Qt::ToolTipRole).toString()).path();

   const QString dataDir = QFileDialog::getExistingDirectory(this, tr("Select data folder"), dirOpen);

   if (dataDir.isEmpty())
      return;

   setCursor(Qt::WaitCursor);

   const auto iModule = libNode->attribute(0, Qt::UserRole).toInt();
   auto testModule = _testManager.testModules().at(iModule);

   testModule->setDataDirectory(dataDir);

   libNode->setAttribute(2, QFileInfo(testModule->dataDirectory()).fileName());
   libNode->setAttribute(2, testModule->dataDirectory(), Qt::ToolTipRole);

   unsetCursor();
}

void TestDialog::on_actionExecuteAll_triggered()
{
   setCursor(Qt::WaitCursor);

   for (int i = 0; i < _testModel->rootNode()->childCount(); ++i)
   {
      executeTestModule(_testModel->rootNode()->child(i));
   }

   unsetCursor();
}

void TestDialog::on_actionExecuteSelected_triggered()
{
   on_testTreeView_doubleClicked(ui->testTreeView->currentIndex());
}

void TestDialog::on_testTreeView_doubleClicked(const QModelIndex& index)
{
   if (!index.isValid())
      return;

   setCursor(Qt::WaitCursor);

   HtNode *currentNode = _testModel->nodeFromIndex(index);

   auto depth = 0;
   auto parentNode = currentNode->parent();

   while (parentNode)
   {
      depth++;
      parentNode = parentNode->parent();
   }

   while (depth > 3)
   {
      currentNode = currentNode->parent();
      depth--;
   }

   switch (depth)
   {
   case 1:
      executeTestModule(currentNode);
      break;
   case 2:
   {
      auto testModule = _testManager.testModules().at(currentNode->parent()->attribute(0, Qt::UserRole).toInt());
      clearTree(currentNode, 2);
      executeTestSuite(testModule, currentNode);
   }
   break;
   case 3:
   {
      auto testModule = _testManager.testModules().at(currentNode->parent()->parent()->attribute(0, Qt::UserRole).toInt());
      auto testSuite = testModule->testSuites().at(currentNode->parent()->attribute(0, Qt::UserRole).toInt());
      testSuite.setParentWindow(ui->actionGUI->isChecked() ? this : nullptr);

      clearTree(currentNode, 3);

      FunctionStatus status;

      testSuite.init(status);
      if (status.hasError())
      {
         currentNode->setAttribute(1, status.completeMessage());
         //currentNode->setAttribute(1, status.hasError() ? Qt::red : Qt::yellow, Qt::BackgroundRole);
         currentNode->setAttribute(1, QIcon(":/icons/free/Error.png"), Qt::DecorationRole);
      }
      else
      {
         executeSingleTest(testSuite, currentNode);
         testSuite.deinit(status);
         if (status.hasMessages())
         {
            auto node = currentNode->appendChild();

            node->setAttribute(0, status.severity() == StatusMessage::S_Warning ? "Warnung" : "Fehler");
            node->setAttribute(1, status.completeMessage());
            //node->setAttribute(1, message.severity() == StatusMessage::S_Warning ? Qt::yellow : Qt::red, Qt::BackgroundRole);
            node->setAttribute(1, QIcon(status.severity() == StatusMessage::S_Warning ? ":/icons/free/Warning.png" : ":/icons/free/Error.png"), Qt::DecorationRole);
         }
      }
   }
   break;
   }

   unsetCursor();
}

void TestDialog::clearTree(HtNode* node, int level)
{
   node->setAttribute(1, Variant::null);
   node->setAttribute(1, Variant::null, Qt::DecorationRole);

   if (level == 3)
   {
      node->removeAllChildNodes();
   }
   else
   {
      for (auto&& child : node->children())
      {
         clearTree(child, level + 1);
      }
   }
}

StatusMessage::Severity TestDialog::executeTestModule(HtNode* testModuleNode)
{
   if (!testModuleNode)
      return StatusMessage::S_Critical;

   clearTree(testModuleNode, 1);

   auto iModule = testModuleNode->attribute(0, Qt::UserRole).toInt();
   auto testModule = _testManager.testModules().at(iModule);

   auto severity = StatusMessage::S_None;

   for (int i = 0; i < testModuleNode->childCount(); ++i)
      severity = qMax(severity, executeTestSuite(testModule, testModuleNode->child(i)));

   if (severity == StatusMessage::S_Error)
      testModuleNode->setAttribute(1, QIcon(":/icons/free/Error.png"), Qt::DecorationRole);
   else if (severity == StatusMessage::S_Warning)
      testModuleNode->setAttribute(1, QIcon(":/icons/free/Warning.png"), Qt::DecorationRole);
   else if (severity)
      testModuleNode->setAttribute(1, QIcon(":/icons/free/Info.png"), Qt::DecorationRole);
   else
      testModuleNode->setAttribute(1, QIcon(":/icons/free/Yes.png"), Qt::DecorationRole);

   return severity;
}

StatusMessage::Severity TestDialog::executeTestSuite(TestModule* testModule, HtNode* testSuiteNode)
{
   if (!testSuiteNode)
      return StatusMessage::S_Critical;

   int iSuite = testSuiteNode->attribute(0, Qt::UserRole).toInt();
   auto testSuite = testModule->testSuites().at(iSuite);

   testSuite.setParentWindow(ui->actionGUI->isChecked() ? this : nullptr);

   FunctionStatus status;

   testSuite.init(status);

   auto severity = status.severity();

   if (severity < StatusMessage::S_Error)
   {
      for (int i = 0; i < testSuiteNode->childCount(); ++i)
         severity = qMax(severity, executeSingleTest(testSuite, testSuiteNode->child(i)));

      testSuite.deinit(status);
      severity = qMax(severity, status.severity());
   }

   if (status.hasMessages())
      testSuiteNode->setAttribute(1, status.completeMessage());
   else
      testSuiteNode->setAttribute(1, Variant::null);

   if (severity == StatusMessage::S_Error)
      testSuiteNode->setAttribute(1, QIcon(":/icons/free/Error.png"), Qt::DecorationRole);
   else if (severity == StatusMessage::S_Warning)
      testSuiteNode->setAttribute(1, QIcon(":/icons/free/Warning.png"), Qt::DecorationRole);
   else if (severity)
      testSuiteNode->setAttribute(1, QIcon(":/icons/free/Info.png"), Qt::DecorationRole);
   else
      testSuiteNode->setAttribute(1, QIcon(":/icons/free/Yes.png"), Qt::DecorationRole);

   return severity;
}

StatusMessage::Severity TestDialog::executeSingleTest(TestSuite suite, HtNode* testCaseNode)
{
   if (!testCaseNode)
      return StatusMessage::S_Critical;

   auto iTestCase = testCaseNode->attribute(0, Qt::UserRole).toInt();

   FunctionStatus status;

   suite.executeMethod(iTestCase, status);

   auto severity = status.severity();

   if (severity)
   {
      testCaseNode->setAttribute(1, QString("%1 Fehler und %2 Warnungen").arg(status.errorCount()).arg(status.warningCount()));
      //testCaseNode->setAttribute(1, status.hasError() ? Qt::red : Qt::yellow, Qt::BackgroundRole);
      testCaseNode->setAttribute(1, QIcon(severity >= StatusMessage::S_Error ? ":/icons/free/Error.png" : ":/icons/free/Warning.png"), Qt::DecorationRole);
      addResults(testCaseNode, status);
   }
   else
   {
      testCaseNode->setAttribute(1, Variant::null);
      //testCaseNode->setAttribute(1, Qt::green, Qt::BackgroundRole);
      testCaseNode->setAttribute(1, QIcon(":/icons/free/Yes.png"), Qt::DecorationRole);
   }

   return severity;
}
