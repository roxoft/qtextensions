#include "stdafx.h"
#include "QValueEditDelegate.h"
#include <Variant.h>
#include "QNumberEdit.h"
#include <QValue.h>
#include "QRichText.h"
#include <QStyleOption>

QString QValueEditDelegate::unit() const
{
   return _suffix.isEmpty() ? QString() : _suffix.mid(1);
}

void QValueEditDelegate::setUnit(const QString &unit)
{
   _suffix = unit.isEmpty() ? QString() : QString::fromLatin1(" ") + unit;
}

QRichText QValueEditDelegate::formattedText(const QModelIndex &index, const QLocaleEx& locale) const
{
   QRichText text;
   const Variant value(index.data());

   if (value.isNull())
      return text;

   QString s = _suffix;

   if (value.type() == typeid(QValue))
   {
      const auto vt = value.to<QValue>();

      // Use the text part for QDecimal. The suffix is always appended by QValueEditDelegate!
      text = QNumberEditDelegate::formattedText(vt, locale);

      if (!vt.unit().isEmpty())
         s = QString::fromLatin1(" ") + vt.unit();
   }
   else
   {
      text = QNumberEditDelegate::formattedText(index, locale);
   }

   if (!s.isEmpty())
      text += s;

   return text;
}

void QValueEditDelegate::setEditorData(QWidget* editor, const QModelIndex& index) const
{
   QNumberEditDelegate::setEditorData(editor, index);
}

void QValueEditDelegate::setModelData(QWidget* editor, QAbstractItemModel* model, const QModelIndex& index) const
{
   if (index.isValid() && index.data(Qt::EditRole).canConvert<Variant>())
   {
      auto v = index.data(Qt::EditRole).value<Variant>();

      if (v.type() == typeid(QValue))
      {
         auto value = v.to<QValue>();

         value = dynamic_cast<QNumberEdit*>(editor)->value();
         model->setData(index, Variant(value), Qt::EditRole);
         return;
      }
   }

   QNumberEditDelegate::setModelData(editor, model, index);
}

void QValueEditDelegate::updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
   QStyleOptionViewItem opt(option);

   if (index.isValid())
   {
      Variant value(index.data(Qt::EditRole));
      QString s;

      if (value.type() == typeid(QValue))
      {
         auto vt = value.to<QValue>();

         if (!vt.unit().isEmpty())
            s = QString::fromLatin1(" ") + vt.unit();
      }
      if (s.isEmpty())
         s = _suffix;

      opt.rect.setWidth(opt.rect.width() - option.fontMetrics.horizontalAdvance(s));
      QEditItemDelegate::updateEditorGeometry(editor, opt, index);
   }
}
