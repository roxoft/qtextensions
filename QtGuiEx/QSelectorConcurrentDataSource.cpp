#include "stdafx.h"
#include "QSelectorConcurrentDataSource.h"
#include <HtStandardNode>

QSelectorItemLoader::QSelectorItemLoader(QObject *parent) : WorkerThread(parent), _maxItems(50), _filterColumn(-1), _hasMore(false)
{
}

QSelectorItemLoader::~QSelectorItemLoader()
{
   qDeleteAll(_itemList);
}

void QSelectorItemLoader::replaceList(HtStandardNode *rootNode)
{
   if (rootNode)
   {
      rootNode->setChildList(_itemList);
      _itemList.clear();
   }
}


QSelectorConcurrentDataSource::QSelectorConcurrentDataSource(QSelectorItemLoader *loader, QObject *parent)
   : QCompleterHtDataSource(parent),
   _loader(nullptr),
   _filterColumn(-1),
   _hasMore(false),
   _resultPending(false),
   _prepSigCount(0)
{
   setLoader(loader);
}

QSelectorConcurrentDataSource::~QSelectorConcurrentDataSource()
{
   if (_loader)
      _loader->stop();
}

void QSelectorConcurrentDataSource::setLoader(QSelectorItemLoader *loader)
{
   if (_loader)
      _loader->stop();
   _loader = loader;
   prepareModel();
}

bool QSelectorConcurrentDataSource::filter(const QString &filterText, int filterColumn)
{
   _filterText = filterText;
   _filterColumn = filterColumn;

   if (_resultPending)
      return false;

   if (_loader == nullptr)
   {
      emit dataReady(filterText, filterColumn);
      return true;
   }

   if (_filterText == _loader->filterText() && _filterColumn == _loader->filterColumn())
   {
      emit dataReady(filterText, filterColumn);
      return true;
   }

   if (!_loader->hasMore() && _filterColumn == _loader->filterColumn() && _filterText.startsWith(_loader->filterText()))
   {
      emit dataReady(filterText, filterColumn);
      return true;
   }

   _loader->setFilter(_filterText, _filterColumn);
   _resultPending = true;
   _loader->startTask();

   return waitForModelReady(100);
}

void QSelectorConcurrentDataSource::stopFilter()
{
   _resultPending = false;
   waitForModelReady();
   _filterColumn = -1;
}

bool QSelectorConcurrentDataSource::waitForModelReady(int timeout)
{
   for (;;)
   {
      if (!_loader->waitForIdle(timeout))
         return false;

      if (!_resultPending)
         break;

      fillModel();
      _prepSigCount++;
   }

   return true;
}

QModelIndex QSelectorConcurrentDataSource::bestMatch(const QString& text, int column) const
{
   QModelIndex result;
   const QAbstractItemModel *m = this->model();

   if (!m || !m->columnCount())
      return result;

   auto index = m->index(0, column);
   auto minLength = INT_MAX;
   auto exactMatch = false;

   while (index.isValid())
   {
      QString optText = toQString(index.data());

      if (exactMatch)
      {
         if (optText.length() < minLength && optText.startsWith(text, Qt::CaseSensitive))
         {
            minLength = optText.length();
            result = index;
            if (minLength == text.length())
               break;
         }
      }
      else if (optText.length() < minLength)
      {
         if (optText.startsWith(text, Qt::CaseInsensitive))
         {
            minLength = optText.length();
            result = index;
            exactMatch = optText.startsWith(text, Qt::CaseSensitive);
         }
      }
      else if (optText.startsWith(text, Qt::CaseSensitive))
      {
         minLength = optText.length();
         result = index;
         exactMatch = true;
      }

      index = index.sibling(index.row() + 1, column);
   }

   return result;
}

void QSelectorConcurrentDataSource::onItemListReady()
{
   if (_prepSigCount)
      --_prepSigCount;
   else
      fillModel();
}

void QSelectorConcurrentDataSource::prepareModel()
{
   if (_loader)
   {
      QStringList headerList = _loader->headerList();

      for (int i = 0; i < headerList.count(); ++i)
         _model.setHeader(i, headerList.at(i));

      connect(_loader, SIGNAL(taskFinished()), this, SLOT(onItemListReady()), Qt::QueuedConnection);
   }
}

void QSelectorConcurrentDataSource::fillModel()
{
   if (!_resultPending)
      return;

   if (_filterText != _loader->filterText() || _filterColumn != _loader->filterColumn())
   {
      _loader->setFilter(_filterText, _filterColumn);
      _loader->startTask();
      return;
   }
   _resultPending = false;

   _hasMore = _loader->hasMore();
   _loader->replaceList(dynamic_cast<HtStandardNode *>(_model.rootNode()));

   emit dataReady(_filterText, _filterColumn);
}
