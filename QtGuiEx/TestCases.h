#ifndef TESTCASES_H
#define TESTCASES_H

#include <TestFramework.h>

#include <QProxyStyle>

class testMenuTreeStyle : public QProxyStyle
{
   Q_OBJECT

   public:
   static QPalette palette();

   public:
   testMenuTreeStyle(QStyle* style = nullptr);
   ~testMenuTreeStyle() override = default;

   int styleHint(StyleHint hint, const QStyleOption *option, const QWidget *widget, QStyleHintReturn *returnData) const override;
   void drawPrimitive(PrimitiveElement element, const QStyleOption *option, QPainter *painter, const QWidget *widget) const override;
};

class GuiTest : public TestFramework
{
   Q_OBJECT

public:
   GuiTest(QObject *parent = nullptr) : TestFramework(parent) {}
   ~GuiTest() override {}

public slots:
   void textLayoutTest();
   void tableViewDialog();
   void treeViewDialog();
   void expressionEditor();
   void testNumberEdit();
   void testDateEditEx();
   void testDocumentTemplate();
   void testQTreeViewEx();
};

#endif // TESTCASES_H
