#include "stdafx.h"
#include "QExtendedProxyStyle.h"
#include "QStyledItemDelegateEx"
#include <QtWidgets>
#include <QPainter>
#include "QTreeViewEx.h"
#include "QTableViewEx.h"
#include "QTextLayoutEx.h"

#ifdef Q_OS_WIN
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <Uxtheme.h>
#endif

// The hint path must be right aligned and vertically centered around (0, 0)
static QPainterPath createHintPath()
{
   QPainterPath hintPath;

   hintPath.moveTo(-5, -4);
   hintPath.lineTo(0, 0);
   hintPath.lineTo(-5, 4);
   hintPath.closeSubpath();

   return hintPath;
}

// The hint path must be right aligned and vertically centered around (0, 0)
static QPainterPath createHintBackgroundPath()
{
   QPainterPath hintPath;

   hintPath.moveTo(-7, -7);
   hintPath.lineTo(0, -4);
   hintPath.lineTo(0, 4);
   hintPath.lineTo(-7, 7);
   hintPath.closeSubpath();

   return hintPath;
}

/* Copied from: Src\qtbase\src\widgets\styles\qcommonstyle.cpp
compute the position for the different component of an item (pixmap, text, checkbox)

Set sizehint to false to layout the elements inside opt->rect. Set sizehint to true to ignore
opt->rect and return rectangles in infinite space

Code duplicated in QItemDelegate::doLayout
*/
void viewItemLayout(const QStyle *style, const QStyleOptionViewItemEx *opt, QExtendedProxyStyle::EntireCellCheckBoxPosition checkBoxPosition, QRect *checkRect, QRect *pixmapRect, QRect *textRect, bool asSizeHint)
{
   const int focusFrameMargin = style->pixelMetric(QStyle::PM_FocusFrameHMargin, 0, opt->widget) + 1;

   Q_ASSERT(checkRect && pixmapRect && textRect);

   //
   // Icon rect
   //

   if (opt->features & QStyleOptionViewItem::HasDecoration)
      pixmapRect->setSize(opt->decorationSize);

   //
   // Text rect
   //

   if (opt->features & QStyleOptionViewItem::HasDisplay)
   {
      int fittingWidth = 0; // This is the width the text should fit in
      const bool wordWrap = opt->features & QStyleOptionViewItem::WrapText;

      if (wordWrap)
      {
         if (opt->decorationPosition == QStyleOptionViewItem::Top || opt->decorationPosition == QStyleOptionViewItem::Bottom)
         {
            if (opt->rect.isValid())
               fittingWidth = opt->rect.width();
            else
               fittingWidth = opt->decorationSize.width();
         }
         else
         {
            // opt.decorationPosition == QStyleOptionViewItem::Left || opt.decorationPosition == QStyleOptionViewItem::Right
            if (opt->rect.isValid())
            {
               fittingWidth = opt->rect.width();

               if (opt->features & QStyleOptionViewItem::HasDecoration)
               {
                  fittingWidth -= opt->decorationSize.width();
                  fittingWidth -= 2 * focusFrameMargin;
               }
            }
            else
               fittingWidth = 300;

            fittingWidth -= 2 * focusFrameMargin;
         }
      }

      // If the text can't be wrapped use the maximum width to fit the text into
      if (fittingWidth <= 0)
         fittingWidth = (INT_MAX / 256);

      QTextLayoutEx textLayout(opt->richText, opt->font);

      textLayout.setWordWrap(wordWrap);

      textLayout.createLayout(fittingWidth);

      textRect->setWidth(qCeil(textLayout.size().width()) + 2 * focusFrameMargin);
      textRect->setHeight(qCeil(textLayout.size().height()));

      Q_ASSERT(textRect->height() >= opt->fontMetrics.height());
   }

   //
   // Check mark rect
   //

   if (opt->features & QStyleOptionViewItem::HasCheckIndicator)
   {
      checkRect->setWidth(style->pixelMetric(QStyle::PM_IndicatorWidth, opt, opt->widget));
      checkRect->setHeight(style->pixelMetric(QStyle::PM_IndicatorHeight, opt, opt->widget));
   }

   //
   // Combine the rectangles
   //

   const bool hasCheck = checkRect->isValid();
   const bool hasPixmap = pixmapRect->isValid();
   const bool hasText = textRect->isValid();
   const int textMargin = hasText ? focusFrameMargin : 0;
   const int pixmapMargin = hasPixmap ? focusFrameMargin : 0;
   const int checkMargin = hasCheck ? focusFrameMargin : 0;
   int x = opt->rect.left();
   int y = opt->rect.top();
   int w, h;

   if (textRect->height() == 0 && (!hasPixmap || !asSizeHint))
   {
      // If there is no text, we still want to have a decent height for the item sizeHint and the editor size
      textRect->setHeight(opt->fontMetrics.height());
   }

   QSize pm(0, 0);
   if (hasPixmap)
   {
      pm = pixmapRect->size();
      pm.rwidth() += 2 * pixmapMargin;
   }

   if (asSizeHint)
   {
      h = qMax(checkRect->height(), qMax(textRect->height(), pm.height()));
      if (opt->decorationPosition == QStyleOptionViewItem::Left || opt->decorationPosition == QStyleOptionViewItem::Right)
         w = textRect->width() + pm.width();
      else
         w = qMax(textRect->width(), pm.width());
   }
   else
   {
      w = opt->rect.width();
      h = opt->rect.height();
      if (!hasPixmap && !hasText && checkBoxPosition == QExtendedProxyStyle::EntireCellCheckBoxPosition::Full)
      {
         // Use the complete cell as active area
         checkRect->setWidth(w - 2 * checkMargin);
         checkRect->setHeight(h);
      }
   }

   int cw = 0;
   QRect check;

   if (hasCheck)
   {
      cw = checkRect->width() + 2 * checkMargin;
      if (asSizeHint)
         w += cw;
      if (!hasPixmap && !hasText && checkBoxPosition == QExtendedProxyStyle::EntireCellCheckBoxPosition::Centered)
         check.setRect(x + (w - cw) / 2, y, cw, h);
      else if (opt->direction == Qt::RightToLeft)
         check.setRect(x + w - cw, y, cw, h);
      else
         check.setRect(x, y, cw, h);
   }

   QRect display;
   QRect decoration;

   switch (opt->decorationPosition)
   {
   case QStyleOptionViewItem::Top:
      if (hasPixmap)
         pm.setHeight(pm.height() + pixmapMargin); // add space
      h = asSizeHint ? textRect->height() : h - pm.height();

      if (opt->direction == Qt::RightToLeft)
      {
         decoration.setRect(x, y, w - cw, pm.height());
         display.setRect(x, y + pm.height(), w - cw, h);
      }
      else
      {
         decoration.setRect(x + cw, y, w - cw, pm.height());
         display.setRect(x + cw, y + pm.height(), w - cw, h);
      }
      break;
   case QStyleOptionViewItem::Bottom:
      if (hasText)
         textRect->setHeight(textRect->height() + textMargin); // add space
      h = asSizeHint ? textRect->height() + pm.height() : h;

      if (opt->direction == Qt::RightToLeft)
      {
         display.setRect(x, y, w - cw, textRect->height());
         decoration.setRect(x, y + textRect->height(), w - cw, h - textRect->height());
      }
      else
      {
         display.setRect(x + cw, y, w - cw, textRect->height());
         decoration.setRect(x + cw, y + textRect->height(), w - cw, h - textRect->height());
      }
      break;
   case QStyleOptionViewItem::Left:
      if (opt->direction == Qt::LeftToRight)
      {
         decoration.setRect(x + cw, y, pm.width(), h);
         display.setRect(decoration.right() + 1, y, w - pm.width() - cw, h);
      }
      else
      {
         display.setRect(x, y, w - pm.width() - cw, h);
         decoration.setRect(display.right() + 1, y, pm.width(), h);
      }
      break;
   case QStyleOptionViewItem::Right:
      if (opt->direction == Qt::LeftToRight)
      {
         display.setRect(x + cw, y, w - pm.width() - cw, h);
         decoration.setRect(display.right() + 1, y, pm.width(), h);
      }
      else
      {
         decoration.setRect(x, y, pm.width(), h);
         display.setRect(decoration.right() + 1, y, w - pm.width() - cw, h);
      }
      break;
   default:
      qWarning("doLayout: decoration position is invalid");
      decoration = *pixmapRect;
      break;
   }

   if (!asSizeHint)
   {
      // we only need to do the internal layout if we are going to paint
      *checkRect = QStyle::alignedRect(opt->direction, Qt::AlignCenter, checkRect->size(), check);
      *pixmapRect = QStyle::alignedRect(opt->direction, opt->decorationAlignment, pixmapRect->size(), decoration);
      // the text takes up all available space, unless the decoration is not shown as selected
      if (opt->showDecorationSelected)
         *textRect = display;
      else
         *textRect = QStyle::alignedRect(opt->direction, opt->displayAlignment, textRect->size().boundedTo(display.size()), display);
   }
   else
   {
      *checkRect = check;
      *pixmapRect = decoration;
      *textRect = display;
   }
}

#ifdef Q_OS_WIN
static bool gUseVistaStyle = true;
#else
static bool gUseVistaStyle = false;
#endif

QExtendedProxyStyle::QExtendedProxyStyle(QStyle* style) : QProxyStyle(style)
{
#ifdef Q_OS_WIN
   if (style)
      gUseVistaStyle =
         strcmp(typeid(*style).name(), "class QFusionStyle") != 0 &&
         QOperatingSystemVersion::current() >= QOperatingSystemVersion::Windows7 &&
         IsThemeActive() &&
         IsAppThemed();
#endif
}

QExtendedProxyStyle::QExtendedProxyStyle(EntireCellCheckBoxPosition checkBoxPosition, QStyle* style) : QProxyStyle(style), _checkBoxPosition(checkBoxPosition)
{
#ifdef Q_OS_WIN
   if (style)
      gUseVistaStyle =
         strcmp(typeid(*style).name(), "class QFusionStyle") != 0 &&
         QOperatingSystemVersion::current() >= QOperatingSystemVersion::Windows7 &&
         IsThemeActive() &&
         IsAppThemed();
#endif
}

void QExtendedProxyStyle::drawControl(ControlElement control, const QStyleOption* option, QPainter* painter, const QWidget* widget) const
{
   if (control == QStyle::CE_ItemViewItem)
   {
      QStyleOptionViewItemEx opt;

      if (const QStyleOptionViewItemEx *voptex = qstyleoption_cast<const QStyleOptionViewItemEx *>(option))
      {
         opt = *voptex;
      }
      else if (const QStyleOptionViewItem *vopt = qstyleoption_cast<const QStyleOptionViewItem *>(option))
      {
         opt = *vopt;
         opt.richText = opt.text;
      }

      // There is a very special case for CE_ItemViewItem in the function
      // void QWindowsVistaStyle::drawControl(ControlElement element, const QStyleOption *option, QPainter *painter, const QWidget *widget) const
      // in file "Src\qtbase\src\plugins\styles\windowsvista\qwindowsvistastyle.cpp"
      // where for the "newStyle" the palette is changed!!!

      const QAbstractItemView *view = qobject_cast<const QAbstractItemView *>(widget);

      if (!qobject_cast<const QTableView*>(widget) && view && gUseVistaStyle)
      {
         QPalette palette = opt.palette;

         opt.palette.setColor(QPalette::All, QPalette::HighlightedText, palette.color(QPalette::Active, QPalette::Text));
         // Note that setting a saturated color here results in ugly XOR colors in the focus rect
         opt.palette.setColor(QPalette::All, QPalette::Highlight, palette.base().color().darker(108));

         // We hide the focusrect in singleselection as it is not required
         if ((view->selectionMode() == QAbstractItemView::SingleSelection) && !(opt.state & QStyle::State_KeyboardFocusChange))
            opt.state &= ~QStyle::State_HasFocus;
      }

      //#ifndef QT_NO_ACCESSIBILITY
      //   if (!view && !widget)
      //   {
      //      newStyle = !QStyleHelper::hasAncestor(opt.styleObject, QAccessible::MenuItem);
      //   }
      //#endif

      drawItemViewItem(&opt, painter, widget);

      return;
   }

   QProxyStyle::drawControl(control, option, painter, widget);
}

QSize QExtendedProxyStyle::sizeFromContents(ContentsType ct, const QStyleOption* opt, const QSize& contentsSize, const QWidget* w) const
{
   if (ct == QStyle::CT_ItemViewItem)
   {
      if (const QStyleOptionViewItemEx *vopt = qstyleoption_cast<const QStyleOptionViewItemEx *>(opt))
      {
         QRect decorationRect, displayRect, checkRect;
         viewItemLayout(this->proxy(), vopt, _checkBoxPosition, &checkRect, &decorationRect, &displayRect, true);
         return (decorationRect | displayRect | checkRect).size();
      }

      if (const QStyleOptionViewItem *vopt = qstyleoption_cast<const QStyleOptionViewItem *>(opt))
      {
         QStyleOptionViewItemEx option = *vopt;

         option.richText = option.text;

         QRect decorationRect, displayRect, checkRect;
         viewItemLayout(this->proxy(), &option, _checkBoxPosition, &checkRect, &decorationRect, &displayRect, true);
         return (decorationRect | displayRect | checkRect).size();
      }
   }

   return QProxyStyle::sizeFromContents(ct, opt, contentsSize, w);
}

QRect QExtendedProxyStyle::subElementRect(SubElement subElement, const QStyleOption* option, const QWidget* widget) const
{
   if (subElement == SE_ItemViewItemText)
   {
      if (const QStyleOptionViewItemEx *voptex = qstyleoption_cast<const QStyleOptionViewItemEx *>(option))
      {
         return voptex->rect;
      }
   }
   else if (subElement == SE_ItemViewItemCheckIndicator)
   {
      if (const QStyleOptionViewItemEx *vopt = qstyleoption_cast<const QStyleOptionViewItemEx *>(option))
      {
         QRect decorationRect, displayRect, checkRect;
         viewItemLayout(this->proxy(), vopt, _checkBoxPosition, &checkRect, &decorationRect, &displayRect, false);
         return checkRect;
      }

      if (const QStyleOptionViewItem *vopt = qstyleoption_cast<const QStyleOptionViewItem *>(option))
      {
         QStyleOptionViewItemEx option = *vopt;

         option.richText = option.text;

         QRect decorationRect, displayRect, checkRect;
         viewItemLayout(this->proxy(), &option, _checkBoxPosition, &checkRect, &decorationRect, &displayRect, false);
         return checkRect;
      }
   }

   return QProxyStyle::subElementRect(subElement, option, widget);
}

QIcon QExtendedProxyStyle::standardIcon(StandardPixmap standardIcon, const QStyleOption* option, const QWidget* widget) const
{
   if (standardIcon == SP_ArrowDown)
      return QIcon(":/icons/free/Down.png");
   if (standardIcon == SP_ArrowUp)
      return QIcon(":/icons/free/Up.png");
   return QProxyStyle::standardIcon(standardIcon, option, widget);
}

void QExtendedProxyStyle::drawItemViewItem(const QStyleOptionViewItemEx *option, QPainter *painter, const QWidget *widget) const
{
   const int textMargin = this->proxy()->pixelMetric(QStyle::PM_FocusFrameHMargin, 0, widget) + 1;

   // The following code is copied from the case CE_ItemViewItem in function
   // "void QCommonStyle::drawControl(ControlElement element, const QStyleOption *opt, QPainter *p, const QWidget *widget) const"
   // in the file "Src\qtbase\src\widgets\styles\qcommonstyle.cpp".

   painter->save();
   painter->setClipRect(option->rect);

   QRect checkRect;
   QRect iconRect;
   QRect textRect;

   viewItemLayout(this->proxy(), option, _checkBoxPosition, &checkRect, &iconRect, &textRect, false);

   // Get the color group

   auto cg = (option->state & QStyle::State_Enabled) && (widget == nullptr || widget->isEnabled()) ? QPalette::Normal : QPalette::Disabled;

   if (cg == QPalette::Normal && !(option->state & QStyle::State_Active))
      cg = QPalette::Inactive;

   //
   // Draw the background
   //

   this->proxy()->drawPrimitive(QStyle::PE_PanelItemViewItem, option, painter, widget);

   //
   // Draw the check mark
   //

   if (option->features & QStyleOptionViewItem::HasCheckIndicator)
   {
      QStyleOptionViewItem checkMarkOpt(*option);

      checkMarkOpt.rect = checkRect;
      checkMarkOpt.state = checkMarkOpt.state & ~QStyle::State_HasFocus;

      switch (option->checkState) {
      case Qt::Unchecked:
         checkMarkOpt.state |= QStyle::State_Off;
         break;
      case Qt::PartiallyChecked:
         checkMarkOpt.state |= QStyle::State_NoChange;
         break;
      case Qt::Checked:
         checkMarkOpt.state |= QStyle::State_On;
         break;
      }
      this->proxy()->drawPrimitive(QStyle::PE_IndicatorItemViewItemCheck, &checkMarkOpt, painter, widget);
   }

   //
   // Draw the icon
   //

   if (option->features & QStyleOptionViewItem::HasDecoration)
   {
      QIcon::Mode mode = QIcon::Normal;

      if (!(option->state & QStyle::State_Enabled))
         mode = QIcon::Disabled;
      else if (option->state & QStyle::State_Selected)
         mode = QIcon::Selected;

      QIcon::State state = option->state & QStyle::State_Open ? QIcon::On : QIcon::Off;

      option->icon.paint(painter, iconRect, option->decorationAlignment, mode, state);
   }

   //
   // Draw the text
   //

   if ((option->features & QStyleOptionViewItem::HasDisplay) && !option->richText.isEmpty())
   {
      // Calculate the text rect with a floating point rectangle because QRect returns the wrong values for bottom and right!
      QRectF displayRect = textRect.adjusted(textMargin, 0, -textMargin, 0);

      // Find the text color

      QColor textColor;

      if ((option->state & QStyle::State_Selected) && !(option->state & QStyle::State_Editing))
         textColor = option->palette.color(cg, QPalette::HighlightedText);
      else
         textColor = option->palette.color(cg, QPalette::Text);

      painter->setPen(textColor);

      if (option->state & QStyle::State_Editing)
         painter->drawRect(displayRect.adjusted(0, 0, -1, -1));

      QTextLayoutEx  textLayout(option->richText, option->font);
      Qt::Alignment  alignment = QStyle::visualAlignment(option->direction, option->displayAlignment);

      textLayout.setWordWrap(option->features & QStyleOptionViewItem::WrapText);
      textLayout.setTextDirection(option->direction);
      textLayout.setAlignment(alignment);

      auto maxHeight = displayRect.height() + option->fontMetrics.height() / 4.0;

      textLayout.createLayout(displayRect.width(), maxHeight);

      auto isConcealed = false;
      auto lineCount = textLayout.lineCount();

      if (lineCount > 1 && textLayout.size().height() > maxHeight)
      {
         isConcealed = true;
         lineCount--;
      }
      else if (lineCount > 0 && textLayout.size().width() > displayRect.width())
      {
         isConcealed = true;
      }

      // Draw the elided text
      if (isConcealed && option->continuationHintStyle != QStyleOptionViewItemEx::ContinuationStyle::Icon)
      {
         const auto lastVisibleLine = textLayout.lineAt(lineCount - 1);
         const auto textStart = lastVisibleLine.textStart();
         auto textEnd = textStart + lastVisibleLine.textLength();
         if (textLayout.text().at(textEnd - 1) == QChar::LineSeparator)
            textEnd--;
         auto elidedText = option->fontMetrics.elidedText(textLayout.text().mid(textStart, textEnd - textStart), Qt::ElideRight, (int)displayRect.width());
         if (!elidedText.endsWith("..."))
            elidedText = option->fontMetrics.elidedText(textLayout.text().mid(textStart, textEnd - textStart) + "...", Qt::ElideRight, (int)displayRect.width());
         textLayout.setText(option->richText.left(textStart) + elidedText);
         textLayout.createLayout(displayRect.width());
      }

      QRectF textBoundingRect = textLayout.boundingRect();

      // Calculate the text position (x, y)

      auto x = displayRect.left();
      auto y = 0.;

      if (alignment & Qt::AlignTop)
         y = displayRect.top();
      else if (alignment & Qt::AlignBottom)
         y = displayRect.bottom() - textBoundingRect.height();
      else // Qt::AlignVCenter
         y = (displayRect.top() + displayRect.bottom() - textBoundingRect.height()) / 2;

      if (y < displayRect.top())
         y = displayRect.top();

      // Draw the text

      painter->setClipRect(displayRect);
      textLayout.draw(painter, QPointF(x, y)); //textLayout.draw(painter, topLeft, QVector<QTextLayout::FormatRange>(), rect);
      painter->setClipRect(option->rect);

      // Set item view concealed item index

      if (auto treeView = qobject_cast<const QTreeViewEx*>(widget))
      {
         treeView->setIndexIsConcealed(option->index, isConcealed);
      }
      else if (auto tableView = qobject_cast<const QTableViewEx*>(widget))
      {
         tableView->setIndexIsConcealed(option->index, isConcealed);
      }

      // Draw the continuation hint
      if (isConcealed && option->continuationHintStyle == QStyleOptionViewItemEx::ContinuationStyle::Icon)
      {
#if 0
         // Text fade out to make place for the continuation hint
         QRectF fadeRect(textLayoutRect.right() - 15, textLayoutRect.top(), 15, textLayoutRect.height());

         QLinearGradient gradient(fadeRect.topLeft(), fadeRect.topRight());

         gradient.setColorAt(0, Qt::transparent);
         gradient.setColorAt(0.6, Qt::white);

         painter->setPen(Qt::NoPen);
         painter->setBrush(gradient);
         painter->drawRect(fadeRect);
#endif

         // Create hint (fade the text around the hint)

         QPainterPath hintPath = createHintPath();
         QPainterPath hintBackgroundPath = createHintBackgroundPath();

         // Place the hint sign

         hintPath.translate(displayRect.right(), (displayRect.top() + displayRect.bottom()) / 2);
         hintBackgroundPath.translate(displayRect.right(), (displayRect.top() + displayRect.bottom()) / 2);

         // Background brush

         QBrush hintBackgroundBrush;

         if (option->backgroundBrush.style() != Qt::NoBrush)
            hintBackgroundBrush = option->backgroundBrush;
         else
            hintBackgroundBrush = option->palette.brush(cg, QPalette::Base);

         // Draw the hint sign

         QPainter::RenderHints prevRenderHints = painter->renderHints();

         painter->setRenderHint(QPainter::Antialiasing);

         painter->setPen(Qt::NoPen);
         painter->setBrush(hintBackgroundBrush);
         painter->setOpacity(0.7);
         painter->drawPath(hintBackgroundPath);
         painter->setOpacity(1.0);
         painter->setBrush(QBrush(textColor));
         painter->drawPath(hintPath);

         painter->setRenderHints(prevRenderHints);
      }
   }

   //
   // draw the focus rect
   //

   if (option->state & QStyle::State_HasFocus)
   {
      QStyleOptionFocusRect o;

      o.QStyleOption::operator=(*option);
      o.rect = (option->features & QStyleOptionViewItem::HasDisplay) ? textRect : option->rect;
      o.state |= QStyle::State_KeyboardFocusChange;
      o.state |= QStyle::State_Item;

      o.backgroundColor = option->palette.color(cg, (option->state & QStyle::State_Selected) ? QPalette::Highlight : QPalette::Window);
      this->proxy()->drawPrimitive(QStyle::PE_FrameFocusRect, &o, painter, option->widget);
   }

   painter->restore();
}
