#include "stdafx.h"
#include "QDialogEx.h"
#include "WidgetTools.h"

QDialogEx::QDialogEx(QWidget *parent, Qt::WindowFlags f)
   : QDialog(parent, f)
{
   setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);
}

QDialogEx::~QDialogEx()
{
   if (_dlgSettings && _dlgSettings->isChanged())
      _dlgSettings->save();
}

void QDialogEx::setDlgSettings(QSettingsEx dlgSettings)
{
   _dlgSettings = dlgSettings;
   if (_dlgSettings && !_dlgSettings->isOpen())
   {
      if (_dlgSettings->open(windowTitle()))
         _dlgSettings->load();
   }
}

void QDialogEx::setDlgSettings(QSettingsExBase* dlgSettings)
{
   _dlgSettings = dlgSettings;
   if (_dlgSettings && !_dlgSettings->isOpen())
   {
      if (_dlgSettings->open(windowTitle()))
         _dlgSettings->load();
   }
}

void QDialogEx::loadGeometrySettings(const QString& key)
{
   if (!_dlgSettings)
      return;

   QByteArray windowSettings = _dlgSettings->value(key).toByteArray();

   if (!windowSettings.isEmpty())
      restoreGeometry(windowSettings);
}

void QDialogEx::saveGeometrySettings(const QString& key)
{
   if (_dlgSettings)
      _dlgSettings->setValue(key, saveGeometry());
}

#if QT_VERSION >= QT_VERSION_CHECK(5,0,0)
void QDialogEx::showEvent(QShowEvent *e)
{
   //QDialog::showEvent(e); // replaced to position correctly even if the parent has an invalid size
   if (!e->spontaneous() && !testAttribute(Qt::WA_Moved))
   {
      Qt::WindowStates state = windowState();

      centerOnParent(this);

      setAttribute(Qt::WA_Moved, false); // not really an explicit position
      if (state != windowState())
         setWindowState(state);
   }

   adjustToScreen(this); // Make sure the window is visible
}
#endif
