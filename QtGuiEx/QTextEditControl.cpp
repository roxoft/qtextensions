#include "stdafx.h"
#include "QTextEditControl.h"
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QToolButton>
#include <QButtonGroup>
#include <QComboBoxEx>
#include <QSelector>
#include <QExpressionTextEdit>
#include <QAction>
#include <QFontDatabase>
#include <QApplication>
#include <QMessageBox>
#include <QTextCursor>
#include <QTextList>
#include <QColorDialog>
#include <QClipboard>
#include <QtGuiVariantTypes.h>

QList<QPair<QString, QByteArray>> QTextEditControl::EmbeddedResourceManager::getResourceImages(const QString& html) const
{
   QList<QPair<QString, QByteArray>> result;

   for (auto&& imageName : imageSources(html))
   {
      const auto image = _textControl->getResourceImage(imageName);

      if (!image.isNull())
      {
         QByteArray data;
         QBuffer buffer(&data);

         buffer.open(QIODevice::WriteOnly);

         if (image.save(&buffer, "PNG"))
         {
            result.append(QPair<QString, QByteArray>(imageName, data));
         }
      }
   }

   return result;
}

QString QTextEditControl::EmbeddedResourceManager::addResource(int index, const QByteArray& data, const QString& imgType)
{
   auto name = QString("embedded_image%1.png").arg(index);
   auto image = QImage::fromData(data, imgType.toUpper().toLatin1());

   if (image.isNull())
      return QString();

   _textControl->addResource(image, name);

   return name;
}

QPair<QByteArray, QString> QTextEditControl::EmbeddedResourceManager::resource(const QString& name) const
{
   const auto image = _textControl->getResourceImage(name);

   if (image.isNull())
      return QPair<QByteArray, QString>();

   QByteArray data;
   QBuffer buffer(&data);

   buffer.open(QIODevice::WriteOnly);

   if (image.save(&buffer, "PNG"))
      return QPair<QByteArray, QString>(data, "PNG");

   return QPair<QByteArray, QString>();
}

QList<QIcon> QTextEditControl::iconset;

QTextEditControl::QTextEditControl(QWidget *parent, bool htmlOnly)
   : QWidget(parent)
{
   // Make sure the iconset has enough icons
   while (iconset.count() <= (int)ToolIcon::BitmapFont)
   {
      iconset.append(QIcon(":/icons/free/Wrench.png"));
   }

   setObjectName("QTextEditControl");
   //resize(820, 600);

   _toolBarLayout = new QHBoxLayout;

   auto styleComboBox = new QComboBoxEx;
   _styleSelector = new QSelector(styleComboBox);
   _styleSelector->setToolTip(tr("Paragraph"));

   connect(_styleSelector, &QSelector::currentIndexChanged, this, &QTextEditControl::on_styleSelector_currentIndexChanged);

   _toolBarLayout->addWidget(styleComboBox);

   auto fontComboBox = new QComboBoxEx;
   _fontSelector = new QSelector(fontComboBox);
   _fontSelector->setToolTip(tr("Font"));

   connect(_fontSelector, &QSelector::currentIndexChanged, this, &QTextEditControl::on_fontSelector_currentIndexChanged);

   _toolBarLayout->addWidget(fontComboBox);

   auto sizeComboBox = new QComboBoxEx;
   QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
   sizePolicy.setHorizontalStretch(0);
   sizePolicy.setVerticalStretch(0);
   sizePolicy.setHeightForWidth(sizeComboBox->sizePolicy().hasHeightForWidth());
   sizeComboBox->setSizePolicy(sizePolicy);
   sizeComboBox->setMinimumSize(QSize(40, 0));
   sizeComboBox->setMaximumSize(QSize(40, 16777215));
   _sizeSelector = new QSelector(sizeComboBox);
   _sizeSelector->setToolTip(tr("Size"));

   connect(_sizeSelector, &QSelector::currentIndexChanged, this, &QTextEditControl::on_sizeSelector_currentIndexChanged);

   _toolBarLayout->addWidget(sizeComboBox);

   _colorToolButton = new QToolButton;
   _colorToolButton->setToolButtonStyle(Qt::ToolButtonFollowStyle);
   _colorToolButton->setToolTip(tr("Color"));

   connect(_colorToolButton, &QToolButton::clicked, this, &QTextEditControl::on_colorToolButton_clicked);

   _toolBarLayout->addWidget(_colorToolButton);

   _boldAction = new QAction(iconset.at((int)ToolIcon::Bold), tr("Bold"), this);
   _boldAction->setCheckable(true);
   addAction(_boldAction);

   connect(_boldAction, &QAction::toggled, this, &QTextEditControl::on_boldToolButton_toggled);

   _italicAction = new QAction(iconset.at((int)ToolIcon::Italic), tr("Italic"), this);
   _italicAction->setCheckable(true);
   addAction(_italicAction);
   
   connect(_italicAction, &QAction::toggled, this, &QTextEditControl::on_italicToolButton_toggled);

   _underlineAction = new QAction(iconset.at((int)ToolIcon::Underline), tr("Underline"), this);
   _underlineAction->setCheckable(true);
   addAction(_underlineAction);

   connect(_underlineAction, &QAction::toggled, this, &QTextEditControl::on_underlineToolButton_toggled);

   addSeparator();

   _alignLeftAction = new QAction(iconset.at((int)ToolIcon::TextLeft), tr("Align left"), this);
   _alignLeftAction->setCheckable(true);
   addAction(_alignLeftAction, &_alignLeftToolButton);

   _alignCenterAction = new QAction(iconset.at((int)ToolIcon::TextCenter), tr("Align center"), this);
   _alignCenterAction->setCheckable(true);
   addAction(_alignCenterAction, &_alignCenterToolButton);

   _alignRightAction = new QAction(iconset.at((int)ToolIcon::TextRight), tr("Align right"), this);
   _alignRightAction->setCheckable(true);
   addAction(_alignRightAction, &_alignRightToolButton);

   _alignAdjustedAction = new QAction(iconset.at((int)ToolIcon::TextJustify), tr("Align justified"), this);
   _alignAdjustedAction->setCheckable(true);
   addAction(_alignAdjustedAction, &_alignAdjustedToolButton);

   addSeparator();

   _undoAction = new QAction(iconset.at((int)ToolIcon::Undo), tr("Undo"), this);
   addAction(_undoAction);

   _redoAction = new QAction(iconset.at((int)ToolIcon::Redo), tr("Redo"), this);
   addAction(_redoAction);

   _cutAction = new QAction(iconset.at((int)ToolIcon::Cut), tr("Cut"), this);
   addAction(_cutAction);

   _copyAction = new QAction(iconset.at((int)ToolIcon::Copy), tr("Copy"), this);
   addAction(_copyAction);

   _pasteAction = new QAction(iconset.at((int)ToolIcon::Paste), tr("Paste"), this);
   addAction(_pasteAction);

   _isHtmlAction = new QAction(iconset.at((int)ToolIcon::TextHtml), tr("Formatted (HTML)"), this);
   _isHtmlAction->setCheckable(true);
   _isHtmlAction->setChecked(true);
   // Do not add the action yet (use addToggleHtmlButton)

   auto verticalLayout = new QVBoxLayout;

   verticalLayout->setContentsMargins(0, 0, 0, 0);
   verticalLayout->addLayout(_toolBarLayout);

   _contentTextEdit = new QExpressionTextEdit;

   connect(_contentTextEdit, &QTextEdit::currentCharFormatChanged, this, &QTextEditControl::on_contentTextEdit_currentCharFormatChanged);
   connect(_contentTextEdit, &QTextEdit::cursorPositionChanged, this, &QTextEditControl::on_contentTextEdit_cursorPositionChanged);
   connect(_contentTextEdit, &QTextEdit::textChanged, this, &QTextEditControl::on_contentTextEdit_textChanged);

   verticalLayout->addWidget(_contentTextEdit);

   setLayout(verticalLayout);

   setFocusProxy(_contentTextEdit);

   setupTextActions();
}

QTextEditControl::~QTextEditControl()
{
}

void QTextEditControl::setCompleterDataSource(QCompleterDataSource* completerDataSource, const QChar& startDelimiter, const QChar& endDelimiter)
{
   _contentTextEdit->setIsDelimited(startDelimiter, endDelimiter);
   _contentTextEdit->setCompleterDataSource(completerDataSource);
}

bool QTextEditControl::isHtml() const
{
   return _isHtmlAction->isChecked();
}

void QTextEditControl::setIsHtml(bool isHtml)
{
   _isHtmlAction->setChecked(isHtml);
}

QString QTextEditControl::text() const
{
   if (isHtml() && !_contentTextEdit->document()->isEmpty())
      return _contentTextEdit->toHtml();
   return _contentTextEdit->toPlainText();
}

void QTextEditControl::setText(const QString& text)
{
   auto isHtml = true;

   if (_isHtmlToolButton)
   {
      if (!text.isEmpty())
         isHtml = isTextHtml(text);

      _isHtmlAction->setChecked(isHtml);
   }

   if (isHtml)
      _contentTextEdit->setHtml(text);
   else
      _contentTextEdit->setPlainText(text);
}

void QTextEditControl::setPlainText(const QString& text)
{
   setIsHtml(false);
   _contentTextEdit->setPlainText(text);
}

void QTextEditControl::setHtmlText(const QString& text)
{
   setIsHtml(true);
   _contentTextEdit->setHtml(text);
}

void QTextEditControl::appendPlainText(const QString& text)
{
   auto tc = _contentTextEdit->textCursor();
   tc.movePosition(QTextCursor::End);
   tc.insertText(text);
}

void QTextEditControl::appendHtmlText(const QString& text)
{
   auto tc = _contentTextEdit->textCursor();
   tc.movePosition(QTextCursor::End);
   tc.insertHtml(text);
}

void QTextEditControl::insertText(const QString& text)
{
   if (isTextHtml(text))
      _contentTextEdit->insertHtml(text);
   else
      _contentTextEdit->insertPlainText(text);
}

void QTextEditControl::insertPlainText(const QString& text)
{
   _contentTextEdit->insertPlainText(text);
}

void QTextEditControl::insertHtmlText(const QString& text)
{
   _contentTextEdit->insertHtml(text);
}

void QTextEditControl::addResource(const QImage& image, const QString& name)
{
   _contentTextEdit->document()->addResource(QTextDocument::ImageResource, QUrl(name), image);
}

QImage QTextEditControl::getResourceImage(const QString& name)
{
   return _contentTextEdit->document()->resource(QTextDocument::ImageResource, QUrl(name)).value<QImage>();
}

void QTextEditControl::addToggleHtmlButton()
{
   if (!_isHtmlToolButton)
   {
      addSeparator();
      addAction(_isHtmlAction, &_isHtmlToolButton);
   }
}

void QTextEditControl::addAction(QAction* action, QToolButton** pButton)
{
   QWidget::addAction(action);

   auto toolButton = new QToolButton;

   toolButton->setDefaultAction(action);
   toolButton->setToolButtonStyle(Qt::ToolButtonFollowStyle);

   _toolBarLayout->addWidget(toolButton);

   if (pButton)
      *pButton = toolButton;
}

void QTextEditControl::addSeparator()
{
   auto line = new QFrame;
   line->setFrameShape(QFrame::VLine);
   line->setFrameShadow(QFrame::Sunken);

   _toolBarLayout->addWidget(line);
}

QString QTextEditControl::currentExpression()
{
   auto tc = _contentTextEdit->textCursor();
   auto block = tc.block();

   const auto expressionRange = _contentTextEdit->expressionRangeAtBlockPosition(block, tc.position() - block.position());

   if (expressionRange.first >= expressionRange.second)
      return QString();

   tc.setPosition(expressionRange.first + block.position());
   tc.setPosition(expressionRange.second + block.position(), QTextCursor::KeepAnchor);

   _contentTextEdit->setTextCursor(tc);

   return tc.selectedText();
}

void QTextEditControl::on_styleSelector_currentIndexChanged(const QModelIndex& index)
{
   if (index.isValid())
   {
      int                     styleIndex = index.row();
      QTextListFormat::Style  textStyle = QTextListFormat::ListStyleUndefined;

      switch (styleIndex)
      {
      case 1:
         textStyle = QTextListFormat::ListDisc;
         break;
      case 2:
         textStyle = QTextListFormat::ListCircle;
         break;
      case 3:
         textStyle = QTextListFormat::ListSquare;
         break;
      case 4:
         textStyle = QTextListFormat::ListDecimal;
         break;
      case 5:
         textStyle = QTextListFormat::ListLowerAlpha;
         break;
      case 6:
         textStyle = QTextListFormat::ListUpperAlpha;
         break;
      case 7:
         textStyle = QTextListFormat::ListLowerRoman;
         break;
      case 8:
         textStyle = QTextListFormat::ListUpperRoman;
         break;
      }

      QTextCursor cursor = _contentTextEdit->textCursor();

      if (textStyle != QTextListFormat::ListStyleUndefined)
      {
         cursor.beginEditBlock();

         QTextBlockFormat  blockFmt = cursor.blockFormat();
         QTextListFormat   listFmt;

         if (cursor.currentList())
         {
            listFmt = cursor.currentList()->format();
         }
         else
         {
            listFmt.setIndent(blockFmt.indent() + 1);
            blockFmt.setIndent(0);
            cursor.setBlockFormat(blockFmt);
         }
         listFmt.setStyle(textStyle);
         cursor.createList(listFmt);
         cursor.endEditBlock();
      }
      else
      {
         QTextBlockFormat  blockFmt = cursor.blockFormat();

         auto list = cursor.currentList();
         if (list)
         {
            list->remove(cursor.block());
            blockFmt.setIndent(0);
         }

         blockFmt.setObjectIndex(-1);
         cursor.setBlockFormat(blockFmt);
      }
   }
}

void QTextEditControl::on_fontSelector_currentIndexChanged(const QModelIndex& index)
{
   if (index.isValid())
   {
      QTextCharFormat fmt;
      fmt.setFontFamilies(index.data().toStringList());
      _contentTextEdit->mergeCurrentCharFormat(fmt);
   }
}

void QTextEditControl::on_sizeSelector_currentIndexChanged(const QModelIndex& index)
{
   if (index.isValid())
   {
      qreal pointSize = index.data().toString().toFloat();

      if (pointSize > 0)
      {
         QTextCharFormat fmt;
         fmt.setFontPointSize(pointSize);
         _contentTextEdit->mergeCurrentCharFormat(fmt);
      }
   }
}

void QTextEditControl::on_colorToolButton_clicked()
{
   QColor col = QColorDialog::getColor(_contentTextEdit->textColor(), this);

   if (!col.isValid())
      return;

   QTextCharFormat fmt;
   fmt.setForeground(col);
   _contentTextEdit->mergeCurrentCharFormat(fmt);
   colorChanged(col);
}

void QTextEditControl::on_boldToolButton_toggled(bool checked)
{
   QTextCharFormat fmt;
   fmt.setFontWeight(checked ? QFont::Bold : QFont::Normal);
   _contentTextEdit->mergeCurrentCharFormat(fmt);
}

void QTextEditControl::on_italicToolButton_toggled(bool checked)
{
   QTextCharFormat fmt;
   fmt.setFontItalic(checked);
   _contentTextEdit->mergeCurrentCharFormat(fmt);
}

void QTextEditControl::on_underlineToolButton_toggled(bool checked)
{
   QTextCharFormat fmt;
   fmt.setFontUnderline(checked);
   _contentTextEdit->mergeCurrentCharFormat(fmt);
}

void QTextEditControl::on_textAlignment_clicked(QAbstractButton* button)
{
   if (button == _alignLeftToolButton)
      _contentTextEdit->setAlignment(Qt::AlignLeft | Qt::AlignAbsolute);
   else if (button == _alignCenterToolButton)
      _contentTextEdit->setAlignment(Qt::AlignHCenter);
   else if (button == _alignRightToolButton)
      _contentTextEdit->setAlignment(Qt::AlignRight | Qt::AlignAbsolute);
   else if (button == _alignAdjustedToolButton)
      _contentTextEdit->setAlignment(Qt::AlignJustify);
}

void QTextEditControl::on_html_toggled(bool checked)
{
   if (!checked)
   {
      auto text = _contentTextEdit->toPlainText();

      if (!text.isEmpty())
      {
         if (QMessageBox::question(this, windowTitle(), tr("Remove all formats?")) != QMessageBox::Yes)
         {
            _isHtmlAction->blockSignals(true);
            _isHtmlAction->setChecked(true);
            _isHtmlAction->blockSignals(false);
            return;
         }
         _contentTextEdit->clear();
      }
      _contentTextEdit->setCurrentCharFormat(QTextCharFormat());
      _contentTextEdit->setCurrentFont(QFont());
      _contentTextEdit->setPlainText(text);
   }

   for (int i = _toolBarLayout->count(); i--; )
   {
      auto widget = _toolBarLayout->itemAt(i)->widget();
      if (widget && widget != _isHtmlToolButton)
         widget->setEnabled(checked);
   }

   if (checked)
   {
      _undoAction->setEnabled(_contentTextEdit->document()->isUndoAvailable());
      _redoAction->setEnabled(_contentTextEdit->document()->isRedoAvailable());

      _cutAction->setEnabled(false);
      _copyAction->setEnabled(false);
#ifndef QT_NO_CLIPBOARD
      _pasteAction->setEnabled(!QApplication::clipboard()->text().isEmpty());
#endif

      emit textChanged();
   }
}

void QTextEditControl::on_contentTextEdit_currentCharFormatChanged(const QTextCharFormat &format)
{
   fontChanged(format.font());
   colorChanged(format.foreground().color());
}

void QTextEditControl::on_contentTextEdit_cursorPositionChanged()
{
   alignmentChanged(_contentTextEdit->alignment());

   auto styleIndex = 0;

   auto list = _contentTextEdit->textCursor().currentList();
   if (list)
   {
      switch (list->format().style())
      {
      case QTextListFormat::ListDisc:
         styleIndex = 1;
         break;
      case QTextListFormat::ListCircle:
         styleIndex = 2;
         break;
      case QTextListFormat::ListSquare:
         styleIndex = 3;
         break;
      case QTextListFormat::ListDecimal:
         styleIndex = 4;
         break;
      case QTextListFormat::ListLowerAlpha:
         styleIndex = 5;
         break;
      case QTextListFormat::ListUpperAlpha:
         styleIndex = 6;
         break;
      case QTextListFormat::ListLowerRoman:
         styleIndex = 7;
         break;
      case QTextListFormat::ListUpperRoman:
         styleIndex = 8;
         break;
      case QTextListFormat::ListStyleUndefined:
         styleIndex = 0;
         break;
      default:
         break;
      }
   }
   _styleSelector->setCurrentRow(styleIndex);
}

void QTextEditControl::on_contentTextEdit_textChanged()
{
   emit textChanged();
}

void QTextEditControl::on_clipboardData_changed()
{
#ifndef QT_NO_CLIPBOARD
   _pasteAction->setEnabled(!QApplication::clipboard()->text().isEmpty());
#endif
}

void QTextEditControl::setupTextActions()
{
   auto grp = new QButtonGroup(this);

   connect(grp, SIGNAL(buttonClicked(QAbstractButton*)), this, SLOT(on_textAlignment_clicked(QAbstractButton*)));

   grp->addButton(_alignLeftToolButton);
   grp->addButton(_alignCenterToolButton);
   grp->addButton(_alignRightToolButton);
   grp->addButton(_alignAdjustedToolButton);

   QPixmap pix(16, 16);

   pix.fill(Qt::black);
   _colorToolButton->setIcon(pix);

   _styleSelector->addItem("Standard");
   _styleSelector->addItem("Bullet List (Disc)");
   _styleSelector->addItem("Bullet List (Circle)");
   _styleSelector->addItem("Bullet List (Square)");
   _styleSelector->addItem("Ordered List (Decimal)");
   _styleSelector->addItem("Ordered List (Alpha lower)");
   _styleSelector->addItem("Ordered List (Alpha upper)");
   _styleSelector->addItem("Ordered List (Roman lower)");
   _styleSelector->addItem("Ordered List (Roman upper)");

   _styleSelector->setCurrentRow(0);

   QFontDatabase db;

   auto fontDataSource = new QCompleterHtDataSource;
   auto model = fontDataSource->hierarchicalTable();

   for (auto&& family : db.families())
   {
      auto node = model->rootNode()->appendChild();

      node->setAttribute(0, family);
      node->setAttribute(0, iconset.at((int)(db.isSmoothlyScalable(family) ? ToolIcon::TrueTypeFont : ToolIcon::BitmapFont)), Qt::DecorationRole);
      node->setAttribute(0, QFont(family, 14), Qt::FontRole);
   }

   _fontSelector->setDataSource(fontDataSource);

   for (auto&& size : db.standardSizes())
      _sizeSelector->addItem(QString::number(size));

   //_sizeSelector->setCurrentIndex(_sizeSelector->find(QString::number(QApplication::font().pointSize())));

   fontChanged(_contentTextEdit->font());
   colorChanged(_contentTextEdit->textColor());
   alignmentChanged(_contentTextEdit->alignment());

   connect(_contentTextEdit->document(), &QTextDocument::undoAvailable, _undoAction, &QAction::setEnabled);
   connect(_contentTextEdit->document(), &QTextDocument::redoAvailable, _redoAction, &QAction::setEnabled);

   _undoAction->setEnabled(_contentTextEdit->document()->isUndoAvailable());
   _redoAction->setEnabled(_contentTextEdit->document()->isRedoAvailable());

   connect(_undoAction, &QAction::triggered, _contentTextEdit, &QTextEdit::undo);
   connect(_redoAction, &QAction::triggered, _contentTextEdit, &QTextEdit::redo);

   _cutAction->setEnabled(false);
   _copyAction->setEnabled(false);
#ifndef QT_NO_CLIPBOARD
   _pasteAction->setEnabled(!QApplication::clipboard()->text().isEmpty());
#endif

   connect(_cutAction, &QAction::triggered, _contentTextEdit, &QTextEdit::cut);
   connect(_copyAction, &QAction::triggered, _contentTextEdit, &QTextEdit::copy);
   connect(_pasteAction, &QAction::triggered, _contentTextEdit, &QTextEdit::paste);

   connect(_isHtmlAction, &QAction::toggled, this, &QTextEditControl::on_html_toggled);

   connect(_contentTextEdit, &QTextEdit::copyAvailable, _cutAction, &QAction::setEnabled);
   connect(_contentTextEdit, &QTextEdit::copyAvailable, _copyAction, &QAction::setEnabled);
#ifndef QT_NO_CLIPBOARD
   connect(QApplication::clipboard(), &QClipboard::dataChanged, this, &QTextEditControl::on_clipboardData_changed);
#endif
}

void QTextEditControl::fontChanged(const QFont &f)
{
   _fontSelector->blockSignals(true);
   _fontSelector->setCurrentIndex(_fontSelector->find(QFontInfo(f).family()));
   _fontSelector->blockSignals(false);

   _sizeSelector->blockSignals(true);
   _sizeSelector->setCurrentIndex(_sizeSelector->find(QString::number(f.pointSize())));
   _sizeSelector->blockSignals(false);

   _boldAction->blockSignals(true);
   _boldAction->setChecked(f.bold());
   _boldAction->blockSignals(false);

   _italicAction->blockSignals(true);
   _italicAction->setChecked(f.italic());
   _italicAction->blockSignals(false);

   _underlineAction->blockSignals(true);
   _underlineAction->setChecked(f.underline());
   _underlineAction->blockSignals(false);
}

void QTextEditControl::colorChanged(const QColor &c)
{
   QPixmap pix(16, 16);

   pix.fill(c);
   _colorToolButton->setIcon(pix);
}

void QTextEditControl::alignmentChanged(Qt::Alignment a)
{
   if (a & Qt::AlignLeft)
      _alignLeftAction->setChecked(true);
   else if (a & Qt::AlignHCenter)
      _alignCenterAction->setChecked(true);
   else if (a & Qt::AlignRight)
      _alignRightAction->setChecked(true);
   else if (a & Qt::AlignJustify)
      _alignAdjustedAction->setChecked(true);
}
