#include "stdafx.h"
#include "QPaintItemDelegate.h"
#include "QStyledItemDelegateEx.h"

QPaintItemDelegate::QPaintItemDelegate(QObject* parent)
   : QObject(parent)
{
}

QPaintItemDelegate::~QPaintItemDelegate()
{
}

void QPaintItemDelegate::paint(QPainter* painter, const QStyleOptionViewItemEx& option, const QModelIndex& index) const
{
   _itemDelegate->_paint(painter, option, index);
}

QSize QPaintItemDelegate::sizeHint(const QStyleOptionViewItemEx& option, const QModelIndex& index) const
{
   return _itemDelegate->_sizeHint(option, index);
}
