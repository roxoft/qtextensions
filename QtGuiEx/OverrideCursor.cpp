#include "stdafx.h"
#include "OverrideCursor.h"

#include <QApplication>

OverrideCursor::OverrideCursor(const QCursor& cursor) : _isShown(true)
{
   QApplication::setOverrideCursor(cursor);
}

OverrideCursor::~OverrideCursor()
{
   restore();
}

void OverrideCursor::restore()
{
   if (_isShown)
   {
      QApplication::restoreOverrideCursor();
      _isShown = false;
   }
}


NormalCursor::NormalCursor()
{
   set();
}

NormalCursor::~NormalCursor()
{
   restore();
}

void NormalCursor::set()
{
   if (_cursorStack.isEmpty())
   {
      QCursor *currentCursor;

      while ((currentCursor = QApplication::overrideCursor()))
      {
         _cursorStack.append(*currentCursor);
         QApplication::restoreOverrideCursor();
      }
   }
}

void NormalCursor::restore()
{
   while (!_cursorStack.isEmpty())
   {
      QApplication::setOverrideCursor(_cursorStack.last());
      _cursorStack.pop_back();
   }
}
