#ifndef QSELECTORPOPUPLISTVIEW_H
#define QSELECTORPOPUPLISTVIEW_H

#include "QTreeViewEx.h"
#include "QPaintItemDelegate.h"
#include "HtItemModel.h"

QT_BEGIN_NAMESPACE
   class QModelIndex;
   class QResizeEvent;
   class QWheelEvent;
   class QMouseEvent;
   class QAbstractProxyModel;
QT_END_NAMESPACE

bool isPopupSeparator(const QModelIndex &index);

class QSelectorPopupDelegate : public QPaintItemDelegate
{
   Q_OBJECT

public:
   QSelectorPopupDelegate(QWidget *view = nullptr) : QPaintItemDelegate(view), _view(view) {}
   ~QSelectorPopupDelegate() override = default;

protected:
   void paint(QPainter *painter, const QStyleOptionViewItemEx& option, const QModelIndex &index) const override;

   QSize sizeHint(const QStyleOptionViewItemEx& option, const QModelIndex &index) const override;

private:
   QWidget *_view;
};

class QSelectorPopupListView : public QTreeViewEx
{
   Q_OBJECT

public:
   QSelectorPopupListView(bool sortEnabled, QWidget *parent = nullptr);
   ~QSelectorPopupListView() override;

   void setModel(QAbstractItemModel *model) override;

   int   minimumLastColumnSize() const { return _minimumLastColumnSize; }
   void  setMinimumLastColumnSize(int columnSize);

   void  select(const QModelIndex &index);

   void  moveSelection(int rowCount);

   int   selectedRow() const;

   virtual QSize sizeHint() const override;

signals:
   void selected(int row);

protected:
   void resizeEvent(QResizeEvent *e) override;
   void wheelEvent(QWheelEvent *e) override;
   void mouseMoveEvent(QMouseEvent *e) override;
   void mouseReleaseEvent(QMouseEvent *e) override;

private slots:
   void onColumnResized(int logicalIndex, int oldSize, int newSize);
   void onSectionCountChanged(int oldCount, int newCount);

private:
   QAbstractItemModel *sourceModel();

   void setLastColumnSize(int totalWidth);
   void internalSelect(const QModelIndex &index);

private:
   QAbstractProxyModel  *_proxyModel;
   int                  _minimumLastColumnSize;
};

#endif // QSELECTORPOPUPLISTVIEW_H
