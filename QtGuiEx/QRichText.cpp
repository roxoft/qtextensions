#include "stdafx.h"
#include "QRichText.h"
#include <Variant.h>
#include <QLocaleEx.h>

static bool convert(QString &lhs, const QRichText &rhs, const QLocaleEx &locale)
{
   Q_UNUSED(locale);
   lhs = rhs;
   return true;
}

static bool convert(QRichText &lhs, const QString &rhs, const QLocaleEx &locale)
{
   Q_UNUSED(locale);
   lhs = rhs;
   return true;
}

BEGIN_VARIANT_REGISTRATION
   Variant::registerType<QRichText>();

   Variant::registerConversion<QRichText, QString>();
   Variant::registerConversion<QString, QRichText>();
END_VARIANT_REGISTRATION
