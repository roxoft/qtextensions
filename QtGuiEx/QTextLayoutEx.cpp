#include "stdafx.h"
#include "QRichText.h"
#include "QTextLayoutEx.h"
#include <QFontMetricsF>
#include <qmath.h>

QTextLayoutEx::QTextLayoutEx(const QRichText &richText, const QFont &font) : QTextLayout(richText, font)
{
   QFontMetricsF fm(font);

   _leading = fm.leading();

   setFormats(richText.format());
   for (auto&& f : richText.format())
   {
      fm = QFontMetricsF(f.format.font());

      _leading = qMax(_leading, fm.leading());
   }
}

void QTextLayoutEx::createLayout(qreal maxWidth, qreal maxHeight)
{
   setTextOption(_textOption);

   auto height = -_leading;
   auto textWidth = 0.;

   beginLayout();
   while (true)
   {
      QTextLine line = createLine();

      if (!line.isValid())
         break;

      line.setLineWidth(maxWidth);
      height += _leading;
      height = qCeil(height); // Make sure lines are positioned on whole pixels
      line.setPosition(QPointF(0., height));
      height += line.height();
      textWidth = qMax(textWidth, lineWidth(line));

      if (maxHeight > 0.0 && height > maxHeight)
         break;
   }
   endLayout();

   _size = QSizeF(textWidth, height);
}

qreal QTextLayoutEx::lineWidth(const QTextLine& line) const
{
   auto textEnd = line.textStart() + line.textLength();

   if ((textOption().alignment() & Qt::AlignJustify) && textEnd < text().length())
      return line.width();

   auto width = line.naturalTextWidth();

   // If its not the last character of our text we must add the right bearing of the last character of the line
   if (textEnd < text().length())
   {
      textEnd--; // Position of last character

      auto lastChar = text().at(textEnd);

      if (!(this->textOption().flags() & QTextOption::IncludeTrailingSpaces) && lastChar == u' ')
      {
         while (textEnd > line.textStart() && lastChar.isSpace())
         {
            textEnd--;
            lastChar = text().at(textEnd);
         }

         auto charFont = font();

         for (auto&& f : formats())
         {
            if (f.start <= textEnd && textEnd < f.start + f.length)
            {
               charFont = f.format.font();
            }
         }

         QFontMetricsF fm(charFont);

         width -= qMin(fm.rightBearing(lastChar), 0.);
      }
   }

   return width;
}
