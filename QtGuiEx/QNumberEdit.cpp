#include "stdafx.h"
#include "QNumberEdit.h"
#include "QNumberValidator.h"
#include <QDecimal.h>
#include <QMouseEvent>
#include <QWheelEvent>

QNumberEdit::QNumberEdit(QWidget *parent, int integerDigits, int fractionalDigits, bool emptyAllowed)
   : QLineEdit(parent), _validator(NULL)
{
   this->setAlignment(Qt::AlignRight | Qt::AlignVCenter);
   _validator = new QNumberValidator(integerDigits, fractionalDigits, this);
   _validator->setEmptyAllowed(emptyAllowed);
   this->setText(_validator->defaultText());
   this->setValidator(_validator);
}

QNumberEdit::~QNumberEdit()
{
}

QDecimal QNumberEdit::value() const
{
   QDecimal result(this->text());

   //result.setUseGroupSeparator(_validator->useGroupSeparator());

   return result;
}

void QNumberEdit::setValue(const QDecimal &value)
{
   this->setText(value.toString(_validator->groupSeparatorMode()));
}

int QNumberEdit::integerDigits() const
{
   return _validator->integerDigits();
}

void QNumberEdit::setIntegerDigits(int integerDigits)
{
   if (integerDigits != _validator->integerDigits())
   {
      _validator->setIntegerDigits(integerDigits);
      onPropertyChanged();
   }
}

int QNumberEdit::fractionalDigits() const
{
   return _validator->fractionalDigits();
}

void QNumberEdit::setFractionalDigits(int fractionalDigits)
{
   if (fractionalDigits != _validator->fractionalDigits())
   {
      _validator->setFractionalDigits(fractionalDigits);
      onPropertyChanged();
   }
}

QDecimal::GroupSeparatorMode QNumberEdit::groupSeparatorMode() const
{
   return _validator->groupSeparatorMode();
}

void QNumberEdit::setGroupSeparatorMode(QDecimal::GroupSeparatorMode groupSeparatorMode)
{
   if (groupSeparatorMode != _validator->groupSeparatorMode())
   {
      _validator->setGroupSeparatorMode(groupSeparatorMode);
      onPropertyChanged();
   }
}

bool QNumberEdit::emptyAllowed() const
{
   return _validator->emptyAllowed();
}

void QNumberEdit::setEmptyAllowed(bool allowed)
{
   if (allowed != _validator->emptyAllowed())
   {
      _validator->setEmptyAllowed(allowed);
      onPropertyChanged();
   }
}

bool QNumberEdit::zeroAllowed() const
{
   return _validator->zeroAllowed();
}

void QNumberEdit::setZeroAllowed(bool allowed)
{
   if (allowed != _validator->zeroAllowed())
   {
      _validator->setZeroAllowed(allowed);
      onPropertyChanged();
   }
}

bool QNumberEdit::negativeAllowed() const
{
   return _validator->negativeAllowed();
}

void QNumberEdit::setNegativeAllowed(bool allowed)
{
   if (allowed != _validator->negativeAllowed())
   {
      _validator->setNegativeAllowed(allowed);
      onPropertyChanged();
   }
}

int QNumberEdit::optimalWidth() const
{
   int minWidth = fontMetrics().horizontalAdvance(defaultLocale().zeroDigit()) * (integerDigits() + fractionalDigits());

   if (groupSeparatorMode() == QDecimal::GS_UseGroupSeparator && defaultLocale().groupSeparator().isPrint() && integerDigits() > 3)
      minWidth += fontMetrics().horizontalAdvance(defaultLocale().groupSeparator()) * ((integerDigits() - 1) / 3);

   if (fractionalDigits())
      minWidth += fontMetrics().horizontalAdvance(defaultLocale().decimalPoint());

   if (negativeAllowed())
      minWidth += fontMetrics().horizontalAdvance(defaultLocale().negativeSign());

   return minWidth + 8;
}

void QNumberEdit::mouseReleaseEvent(QMouseEvent* e)
{
   QLineEdit::mouseReleaseEvent(e);
   if (this->cursorPosition() == 0)
   {
      this->blockSignals(true);
      this->setSelection(0, this->fractionalDigits() > 0 ? text().indexOf(_validator->decimalSeparator()) : text().length());
      this->blockSignals(false);
   }
}

void QNumberEdit::wheelEvent(QWheelEvent *e)
{
   if (hasFocus())
   {
      e->accept();

      QDecimal newValue = value();

#if QT_VERSION < QT_VERSION_CHECK(5,0,0)
      int numSteps = e->delta() / 120;

      newValue += numSteps;
#else
      QPoint numSteps = e->angleDelta() / 120;

      newValue += numSteps.y();
#endif

      if (!_validator->negativeAllowed() && newValue.isNegative())
         newValue = 0;
      if (!_validator->zeroAllowed() && newValue.isZero())
         newValue = 1;

      setValue(newValue);
   }
   else
      e->ignore();
}

QNumberEdit::GroupSeparatorMode QNumberEdit::guiGroupSeparatorMode() const
{
   switch (_validator->groupSeparatorMode())
   {
      case QDecimal::GS_Auto: return AutoGroupSeparators;
      case QDecimal::GS_UseGroupSeparator: return UseGroupSeparators;
      case QDecimal::GS_OmitGroupSeparator: return OmitGroupSeparators;
      default: break;
   }
   return AutoGroupSeparators;
}

void QNumberEdit::setGuiGroupSeparatorMode(QNumberEdit::GroupSeparatorMode groupSeparatorMode)
{
   switch (groupSeparatorMode)
   {
      case AutoGroupSeparators: setGroupSeparatorMode(QDecimal::GS_Auto);
      case UseGroupSeparators: setGroupSeparatorMode(QDecimal::GS_UseGroupSeparator);
      case OmitGroupSeparators: setGroupSeparatorMode(QDecimal::GS_OmitGroupSeparator);
      default: break;
   }
}

void QNumberEdit::onPropertyChanged()
{
   if (_validator == nullptr)
      return;

   auto text = this->text();
   int pos = 0;

   _validator->validate(text, pos);
   _validator->fixup(text);

   this->setText(text);
}
