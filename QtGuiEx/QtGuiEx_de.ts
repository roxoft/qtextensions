<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE">
<context>
    <name>GuiTest</name>
    <message>
        <location filename="TestCases.cpp" line="99"/>
        <source>textLayoutTest skipped</source>
        <translation>textLayoutTest ausgelassen</translation>
    </message>
    <message>
        <location filename="TestCases.cpp" line="125"/>
        <location filename="TestCases.cpp" line="197"/>
        <location filename="TestCases.cpp" line="261"/>
        <location filename="TestCases.cpp" line="353"/>
        <source>tableViewDialog skipped</source>
        <translation>tableViewDialog ausgelassen</translation>
    </message>
    <message>
        <location filename="TestCases.cpp" line="289"/>
        <source>testNumberEdit skipped</source>
        <translation>testNumberEdit ausgelassen</translation>
    </message>
    <message>
        <location filename="TestCases.cpp" line="311"/>
        <source>testDateEditEx skipped</source>
        <translation>testDateEditEx ausgelassen</translation>
    </message>
    <message>
        <location filename="TestCases.cpp" line="366"/>
        <source>Function</source>
        <translation type="unfinished">Funktion</translation>
    </message>
    <message>
        <location filename="TestCases.cpp" line="367"/>
        <source>Key</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QCompleterEx</name>
    <message>
        <location filename="QCompleterEx.cpp" line="1073"/>
        <source>Entry</source>
        <translation>Eintrag</translation>
    </message>
    <message>
        <location filename="QCompleterEx.cpp" line="1075"/>
        <source>Entries</source>
        <translation>Einträge</translation>
    </message>
    <message>
        <location filename="QCompleterEx.cpp" line="1082"/>
        <source>(more available)</source>
        <translation>(weitere vorhanden)</translation>
    </message>
</context>
<context>
    <name>QExpressionEditor</name>
    <message>
        <location filename="QExpressionEditor.cpp" line="83"/>
        <location filename="QExpressionEditor.cpp" line="93"/>
        <source>Discard changes?</source>
        <translation>Änderungen verwerfen?</translation>
    </message>
</context>
<context>
    <name>QExpressionEditorWidget</name>
    <message>
        <location filename="QExpressionEditorWidget.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="QExpressionEditorWidget.ui" line="34"/>
        <source>Variables</source>
        <translation>Variablen</translation>
    </message>
    <message>
        <location filename="QExpressionEditorWidget.ui" line="40"/>
        <location filename="QExpressionEditorWidget.ui" line="67"/>
        <source>Filter</source>
        <translation>Filter</translation>
    </message>
    <message>
        <location filename="QExpressionEditorWidget.ui" line="61"/>
        <source>Functions</source>
        <translation>Funktionen</translation>
    </message>
    <message>
        <location filename="QExpressionEditorWidget.ui" line="96"/>
        <source>Expression</source>
        <translation>Ausdruck</translation>
    </message>
    <message>
        <location filename="QExpressionEditorWidget.ui" line="106"/>
        <location filename="QExpressionEditorWidget.ui" line="137"/>
        <source>Result</source>
        <translation>Ergebnis</translation>
    </message>
    <message>
        <location filename="QExpressionEditorWidget.ui" line="112"/>
        <source>icon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="QExpressionEditorWidget.cpp" line="103"/>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <location filename="QExpressionEditorWidget.cpp" line="104"/>
        <source>Value</source>
        <translation>Wert</translation>
    </message>
    <message>
        <location filename="QExpressionEditorWidget.cpp" line="509"/>
        <source>Edit</source>
        <translation>Bearbeiten</translation>
    </message>
    <message>
        <location filename="QExpressionEditorWidget.cpp" line="546"/>
        <source>Function</source>
        <translation>Funktion</translation>
    </message>
    <message>
        <location filename="QExpressionEditorWidget.cpp" line="547"/>
        <source>Return value</source>
        <translation>Rückgabewert</translation>
    </message>
    <message>
        <location filename="QExpressionEditorWidget.cpp" line="548"/>
        <source>Parameter</source>
        <translation>Parameter</translation>
    </message>
    <message>
        <location filename="QExpressionEditorWidget.cpp" line="549"/>
        <source>Description</source>
        <translation>Beschreibung</translation>
    </message>
    <message>
        <location filename="QExpressionEditorWidget.cpp" line="552"/>
        <source>Global functions</source>
        <translation>Globale-Funktionen</translation>
    </message>
    <message>
        <location filename="QExpressionEditorWidget.cpp" line="555"/>
        <source>Dictionary functions</source>
        <translation>Verzeichnis-Funktionen</translation>
    </message>
    <message>
        <location filename="QExpressionEditorWidget.cpp" line="558"/>
        <source>List functions</source>
        <translation>Listen-Funktionen</translation>
    </message>
    <message>
        <location filename="QExpressionEditorWidget.cpp" line="564"/>
        <source>Value functions</source>
        <translation>Werte-Funktionen</translation>
    </message>
    <message>
        <location filename="QExpressionEditorWidget.cpp" line="623"/>
        <source>%1 functions</source>
        <translation>%1-Funktionen</translation>
    </message>
</context>
<context>
    <name>QFixedGridView</name>
    <message>
        <location filename="QFixedGridView.cpp" line="36"/>
        <source>Change grid size</source>
        <translation>Gittergröße ändern</translation>
    </message>
</context>
<context>
    <name>QFixedGridWidget</name>
    <message>
        <location filename="QFixedGridWidget.cpp" line="48"/>
        <source>Delete widget</source>
        <translation>Widget löschen</translation>
    </message>
    <message>
        <location filename="QFixedGridWidget.cpp" line="80"/>
        <source>Edit title</source>
        <translation>Titel ändern</translation>
    </message>
    <message>
        <location filename="QFixedGridWidget.cpp" line="169"/>
        <source>Apply</source>
        <translation>Übernehmen</translation>
    </message>
</context>
<context>
    <name>QIncrementalSearchWidget</name>
    <message>
        <location filename="QIncrementalSearchWidget.ui" line="14"/>
        <source>QIncrementalSearchWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="QIncrementalSearchWidget.ui" line="38"/>
        <source>Find previous</source>
        <translation>Finde vorheriges</translation>
    </message>
    <message>
        <location filename="QIncrementalSearchWidget.ui" line="45"/>
        <source>Find next</source>
        <translation>Finde nächstes</translation>
    </message>
    <message>
        <location filename="QIncrementalSearchWidget.ui" line="52"/>
        <source>Case sensitive</source>
        <translation>Groß-/Kleinschreibung</translation>
    </message>
    <message>
        <location filename="QIncrementalSearchWidget.ui" line="59"/>
        <source>Complete word</source>
        <translation>ganzes Wort</translation>
    </message>
    <message>
        <location filename="QIncrementalSearchWidget.ui" line="66"/>
        <source>extendedOption</source>
        <translation></translation>
    </message>
    <message>
        <location filename="QIncrementalSearchWidget.cpp" line="463"/>
        <source>Search started from top</source>
        <translation>Suche beginnt von vorne</translation>
    </message>
    <message>
        <location filename="QIncrementalSearchWidget.cpp" line="473"/>
        <source>Search started from bottom</source>
        <translation>Suche beginnt von hinten</translation>
    </message>
    <message>
        <location filename="QIncrementalSearchWidget.cpp" line="634"/>
        <source>inside group only</source>
        <translation>nur Bereich</translation>
    </message>
    <message>
        <location filename="QIncrementalSearchWidget.cpp" line="635"/>
        <source>inside group %1</source>
        <translation>nur Bereich %1</translation>
    </message>
    <message>
        <location filename="QIncrementalSearchWidget.cpp" line="638"/>
        <source>inside current column</source>
        <translation>nur aktuelle Spalte</translation>
    </message>
    <message>
        <location filename="QIncrementalSearchWidget.cpp" line="639"/>
        <source>inside column &apos;%1&apos;</source>
        <translation>nur Spalte &apos;%1&apos;</translation>
    </message>
</context>
<context>
    <name>QSingleMainWindow</name>
    <message>
        <location filename="QSingleMainWindow.cpp" line="56"/>
        <source>Command could not be send</source>
        <translation>Commando konnte nicht gesendet werden</translation>
    </message>
</context>
<context>
    <name>QTableViewEx</name>
    <message>
        <location filename="QTableViewEx.cpp" line="43"/>
        <source>No data</source>
        <translation>Keine Daten</translation>
    </message>
</context>
<context>
    <name>QTextEditControl</name>
    <message>
        <location filename="QTextEditControl.cpp" line="94"/>
        <source>Paragraph</source>
        <translation>Absatzformat</translation>
    </message>
    <message>
        <location filename="QTextEditControl.cpp" line="102"/>
        <source>Font</source>
        <translation>Schriftart</translation>
    </message>
    <message>
        <location filename="QTextEditControl.cpp" line="117"/>
        <source>Size</source>
        <translation>Schriftgröße</translation>
    </message>
    <message>
        <location filename="QTextEditControl.cpp" line="125"/>
        <source>Color</source>
        <translation>Textfarbe</translation>
    </message>
    <message>
        <location filename="QTextEditControl.cpp" line="131"/>
        <source>Bold</source>
        <translation>Fett</translation>
    </message>
    <message>
        <location filename="QTextEditControl.cpp" line="137"/>
        <source>Italic</source>
        <translation>Kursiv</translation>
    </message>
    <message>
        <location filename="QTextEditControl.cpp" line="143"/>
        <source>Underline</source>
        <translation>Unterstrichen</translation>
    </message>
    <message>
        <location filename="QTextEditControl.cpp" line="151"/>
        <source>Align left</source>
        <translation>Linksbündig</translation>
    </message>
    <message>
        <location filename="QTextEditControl.cpp" line="155"/>
        <source>Align center</source>
        <translation>Zentriert</translation>
    </message>
    <message>
        <location filename="QTextEditControl.cpp" line="159"/>
        <source>Align right</source>
        <translation>Rechtsbündig</translation>
    </message>
    <message>
        <location filename="QTextEditControl.cpp" line="163"/>
        <source>Align justified</source>
        <translation>Blocksatz</translation>
    </message>
    <message>
        <location filename="QTextEditControl.cpp" line="169"/>
        <source>Undo</source>
        <translation>Rückgängig</translation>
    </message>
    <message>
        <location filename="QTextEditControl.cpp" line="172"/>
        <source>Redo</source>
        <translation>Wiederherstellen</translation>
    </message>
    <message>
        <location filename="QTextEditControl.cpp" line="175"/>
        <source>Cut</source>
        <translation>Ausschneiden</translation>
    </message>
    <message>
        <location filename="QTextEditControl.cpp" line="178"/>
        <source>Copy</source>
        <translation>Kopieren</translation>
    </message>
    <message>
        <location filename="QTextEditControl.cpp" line="181"/>
        <source>Paste</source>
        <translation>Einfügen</translation>
    </message>
    <message>
        <location filename="QTextEditControl.cpp" line="184"/>
        <source>Formatted (HTML)</source>
        <translation>Formatierter Text (HTML)</translation>
    </message>
    <message>
        <location filename="QTextEditControl.cpp" line="513"/>
        <source>Remove all formats?</source>
        <translation>Möchten Sie die gesamte Formatierung entfernen?</translation>
    </message>
</context>
<context>
    <name>QTreeViewEx</name>
    <message>
        <location filename="QTreeViewEx.cpp" line="264"/>
        <source>No data</source>
        <translation>Keine Daten</translation>
    </message>
    <message>
        <location filename="QTreeViewEx.cpp" line="1434"/>
        <source>Copy</source>
        <translation>Kopieren</translation>
    </message>
    <message>
        <location filename="QTreeViewEx.cpp" line="1436"/>
        <source>Move</source>
        <translation>Verschieben</translation>
    </message>
    <message>
        <location filename="QTreeViewEx.cpp" line="1438"/>
        <source>Link</source>
        <translation>Verknüpfung erzeugen</translation>
    </message>
    <message>
        <location filename="QTreeViewEx.cpp" line="1440"/>
        <source>Abort</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location filename="QTreeViewEx.cpp" line="1459"/>
        <source>Drag &amp; Drop</source>
        <translation>Drag &amp; Drop</translation>
    </message>
    <message>
        <location filename="QTreeViewEx.cpp" line="1459"/>
        <source>Replace item?</source>
        <translation>Eintrag ersetzen?</translation>
    </message>
</context>
<context>
    <name>TestDialog</name>
    <message>
        <location filename="TestDialog.ui" line="14"/>
        <source>TestDialog</source>
        <translation>Testdialog</translation>
    </message>
    <message>
        <location filename="TestDialog.ui" line="54"/>
        <source>Tests</source>
        <translation>Tests</translation>
    </message>
    <message>
        <location filename="TestDialog.ui" line="97"/>
        <source>F5</source>
        <translation>F5</translation>
    </message>
    <message>
        <location filename="TestDialog.ui" line="47"/>
        <source>File</source>
        <translation>Datei</translation>
    </message>
    <message>
        <location filename="TestDialog.ui" line="85"/>
        <source>connect to DLL</source>
        <translation>mit Test DLL verbinden</translation>
    </message>
    <message>
        <location filename="TestDialog.ui" line="94"/>
        <source>Execute all</source>
        <translation>Alle ausführen</translation>
    </message>
    <message>
        <location filename="TestDialog.ui" line="115"/>
        <source>Execute selected</source>
        <translation>Auswahl ausführen</translation>
    </message>
    <message>
        <location filename="TestDialog.ui" line="118"/>
        <source>F9</source>
        <translation>F9</translation>
    </message>
    <message>
        <location filename="TestDialog.ui" line="130"/>
        <source>GUI</source>
        <translation>GUI</translation>
    </message>
    <message>
        <location filename="TestDialog.cpp" line="59"/>
        <source>Test</source>
        <translation>Test</translation>
    </message>
    <message>
        <location filename="TestDialog.cpp" line="60"/>
        <source>Result</source>
        <translation>Ergebnis</translation>
    </message>
    <message>
        <location filename="TestDialog.cpp" line="61"/>
        <source>Data folder</source>
        <translation>Datenverzeichnis</translation>
    </message>
    <message>
        <location filename="TestDialog.cpp" line="171"/>
        <source>Open test DLL</source>
        <translation>DLL Testprojekt öffnen</translation>
    </message>
    <message>
        <location filename="TestDialog.cpp" line="171"/>
        <source>DLL (*.dll *.so)</source>
        <translation>DLL (*.dll *.so)</translation>
    </message>
    <message>
        <location filename="TestDialog.ui" line="106"/>
        <location filename="TestDialog.cpp" line="195"/>
        <source>Select data folder</source>
        <translation>Datenverzeichnis auswählen</translation>
    </message>
</context>
</TS>
