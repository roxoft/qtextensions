#include "stdafx.h"
#include "QTitleBarCloseButton.h"
#include <QStyle>
#include <QStyleOptionToolButton>
#include <QPainter>

QTitleBarCloseButton::QTitleBarCloseButton(QWidget* parent)
   : QAbstractButton(parent)
{
   setFocusPolicy(Qt::NoFocus);
   setIcon(style()->standardIcon(QStyle::SP_TitleBarCloseButton));
}

QTitleBarCloseButton::~QTitleBarCloseButton()
{
}

QSize QTitleBarCloseButton::sizeHint() const
{
   ensurePolished();

   int size = 2 * style()->pixelMetric(QStyle::PM_DockWidgetTitleBarButtonMargin, 0, this);

   if (!icon().isNull())
   {
      int    iconSize = style()->pixelMetric(QStyle::PM_SmallIconSize, 0, this);
      QSize  sz = icon().actualSize(QSize(iconSize, iconSize));

      size += qMax(sz.width(), sz.height());
   }

   return QSize(size, size);
}

#if QT_VERSION_MAJOR < 6
void QTitleBarCloseButton::enterEvent(QEvent *event)
#else
void QTitleBarCloseButton::enterEvent(QEnterEvent *event)
#endif
{
   if (isEnabled())
      update();
   QAbstractButton::enterEvent(event);
}

void QTitleBarCloseButton::leaveEvent(QEvent *event)
{
   if (isEnabled())
      update();
   QAbstractButton::leaveEvent(event);
}

void QTitleBarCloseButton::paintEvent(QPaintEvent *)
{
   QPainter p(this);

   QStyleOptionToolButton opt;

   opt.initFrom(this);
   opt.state |= QStyle::State_AutoRaise;

   if (style()->styleHint(QStyle::SH_DockWidget_ButtonsHaveFrame, 0, this))
   {
      if (isEnabled() && underMouse() && !isChecked() && !isDown())
         opt.state |= QStyle::State_Raised;
      if (isChecked())
         opt.state |= QStyle::State_On;
      if (isDown())
         opt.state |= QStyle::State_Sunken;
      style()->drawPrimitive(QStyle::PE_PanelButtonTool, &opt, &p, this);
   }

   opt.icon = icon();
   opt.subControls = QStyle::SC_None;
   opt.activeSubControls = QStyle::SC_None;
   opt.features = QStyleOptionToolButton::None;
   opt.arrowType = Qt::NoArrow;
   int size = style()->pixelMetric(QStyle::PM_SmallIconSize, 0, this);
   opt.iconSize = QSize(size, size);

   style()->drawComplexControl(QStyle::CC_ToolButton, &opt, &p, this);
}
