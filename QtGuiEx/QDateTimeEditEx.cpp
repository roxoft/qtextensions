#include "stdafx.h"
#include "QDateTimeEditEx.h"
#include "QLocaleEx.h"
#include <QMouseEvent>

void QDTEEXLineEdit::mouseReleaseEvent(QMouseEvent *event)
{
   QLineEdit::mouseReleaseEvent(event);
   emit mouseButtonUp(event);
}

void QDTEEXLineEdit::mouseDoubleClickEvent(QMouseEvent *event)
{
   event->accept();
}

QDateTimeEditEx::QDateTimeEditEx(QWidget *parent)
   : QDateTimeEdit(parent)
{
   setDisplayFormat(defaultLocale().dateTimeFormat());
   setLineEdit(new QDTEEXLineEdit);
   connect(lineEdit(), SIGNAL(mouseButtonUp(QMouseEvent*)), this, SLOT(mouseButtonUp(QMouseEvent*)));
}

void QDateTimeEditEx::mouseButtonUp(QMouseEvent *event)
{
   Q_UNUSED(event);

   setSelectedSection(currentSection());
}
