TestFramework
=============

Diese statische Bibliothek definiert die Schnittstellen zum Schreiben von Unit Tests.

Dieses Framework kann allerdings nur in dynamisch gebundenen Bibliotheken oder in Programmen genutzt werden. Unit Tests in statischen Bibliotheken müssen von einer DLL oder einem Programm veröffentlicht werden.

Unit Tests in DLLs
------------------

Vorbereitung:

- Die DLL muss mit dieser Bibliothek verlinkt werden.
- Der Header TestFramework muss zur Verfügung stehen.
- Es muss die QtCore Bibliothek zur Verfügung stehen.

Nun kann man seine eigenen Testfälle wie folgt definieren:

Erstellen einer neuen Klasse, die von TestFramework ableitet. Sie muss das `Q_OBJECT` Makro enthalten, also mit dem Qt moc-Kompiler übersetzt werden. Diese Klasse stellt dann eine neue Test Suite dar, die verschiedene Testfälle enthalten kann.

Der Konstruktor wird wie bei allen von `QObject` abgeleiteten Klassen definiert: `MyTestSuite(QObject *parent = 0) : TestFramework(parent) {}`

Die Testfälle werden als öffentliche slots definiert:

    public slots:
       void TestCase1();
       void TestCase2();

Neben der Implementation der Testfälle muss in der CPP noch die Test Suite veröffentlicht werden:

    BEGIN_TEST_SUITES
       TEST_SUITE(MyTestSuite);
    END_TEST_SUITES

Fehler die in einem Testfall auftreten werden mit der Member-Funktion `addError()` dem TestFramework mitgeteilt. `addError()` unterbricht die Ausführung des Testfalles nicht, so dass mehrere Fehler signalisiert werden können.

Wird in einem Testfall `addError()` nie aufgerufen, gilt der Testfall als erfolgreich.

Um die Testfälle einfacher und übersichtlicher zu halten stellt das TestFramework 2 Kategorien von Makros zur Verfügung:

- Unterbrechende Makros
- Nicht unterbrechende Makros

Unterbrechende Makros enthalten den Begriff `ASSERT`:

- `T_ASSERT(predicate)`: Wenn das `predicate` `false` ergibt wird die Ausführeng abgebrochen.
- `T_ASSERT_EQUAL(leftSide, rightSide)`: Wenn die beiden Seiten ungleich sind wird die Ausführung abgebrochen.

`ASSERT`-Makros werfen die Ausnahme `TestException` wenn die Bedingung nicht erfüllt wird. Die Ausnahme kann auch direkt vom Programmierer geworfen werden um einen Fehler zu signalisieren.

Der Testmanager fängt alle Ausnahmen ab und erzeugt entsprechende Fehlermeldungen.

Nicht unterbrechende Makros geben nur eine Fehlermeldung aus und setzen die Programmausführung fort:

- `T_EQUALS(leftSide, rightSide)`: Wenn die linke und rechte Seite ungleich sind (Vergleich mit dem == Operator), wird eine Fehlermeldung ausgegeben.
- `T_IS_TRUE(predicate)`: Wenn die Bedingung (`predicate`) `false` ergibt wird eine Fehlermeludng ausgegeben.
- `T_ERROR_IF(predicate, message)`: Wenn die Bedingung (`predicate`) `false` ergibt wird die Meldung `message` ausgegeben.

Nicht unterbrechende Makros rufen `addError()` auf um Meldungen auszugeben. `addError()` kann jeder Zeit auch vom Programmierer aufgerufen werden. Der Testfall gilt dann als nicht erfolgreich.

Beispiel eines Testfalles:

    void MyTestSuite::TestCase1()
    {
       QString text;
       int     i = 42;
     
       T_ASSERT(text.isEmpty());
       T_EQUALS(i, 42);
       try
       {
          foo();
       }
       catch (...)
       {
          addError("foo erzeugte eine Ausnahme");
       }
    }

Code zur Vorbereitung der Test-Suite kann in der virtuellen Funktion `init()` definiert werden.

Werden für die Ausführung der Tests zusätzliche Daten benötigt (Testdaten), können diese aus dem Verzeichnis `dataDir()` geholt werden. Das Testdatenverzeichnis wird vom Tester festgelegt.
