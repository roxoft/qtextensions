#ifndef TESTFRAMEWORK_H
#define TESTFRAMEWORK_H

#define STRINGIFY2(X) #X
#define STRINGIFY(X) STRINGIFY2(X)

#define BEGIN_TEST_SUITES extern "C" { Q_DECL_EXPORT void createTestSuites(QList<TestFramework*>* testSuiteList) { if (!testSuiteList) return; 
#define TEST_SUITE(suiteName) testSuiteList->append(new suiteName)
#define END_TEST_SUITES } Q_DECL_EXPORT void deleteTestSuite(TestFramework* testSuite) { delete testSuite; } }

#define T_ASSERT(predicate) TestFramework::fatalIfNot(predicate, "Line " STRINGIFY(__LINE__) ": " #predicate)
#define T_ASSERT_EQUAL(leftSide, rightSide) TestFramework::fatalIfNotEqual(leftSide, rightSide, "Line " STRINGIFY(__LINE__) ": " #leftSide " (%1) <> " #rightSide " (%2)")

#define T_IS_TRUE(predicate) if (predicate != true) addError("Line " STRINGIFY(__LINE__) ": " #predicate)
#define T_ERROR_IF(predicate, message) if (predicate) addError(QString("Line " STRINGIFY(__LINE__) ": %1").arg(message))
#define T_EQUALS(leftSide, rightSide) errorIfNotEqual(leftSide, rightSide, "Line " STRINGIFY(__LINE__) ": " #leftSide " (%1) <> " #rightSide " (%2)")
#define T_EQUALS_EX(leftSide, rightSide) errorIfNotEqualEx(leftSide, rightSide, "Line " STRINGIFY(__LINE__) ": " #leftSide " (%1) <> " #rightSide " (%2)")

#define T_SIGNAL_ERROR(message) addError(QString::fromUtf8("Line " STRINGIFY(__LINE__) ": ") + message)
#define T_SIGNAL_WARNING(message) addWarning(QString::fromUtf8("Line " STRINGIFY(__LINE__) ": ") + message)

#include <QObject>
#include <QString>
#include <utility>

class QWidget;

class TestException : public std::exception
{
public:
   TestException(QString message) : _message(std::move(message)) {}
   TestException(const char* message) : _message(QString::fromUtf8(message)) {}
   ~TestException() = default;

   const QString& message() const { return _message; }

private:
   QString _message;
};

/*
  TestFramework is the base class for creating your own tests.

  Executing the tests is done via the signal/slot mechanism of Qt.
  Therefore the test methods must be slots.

  Example of a test suite header:

      #include TestFramework.h

      class MyTestSuite : public TestFramework
      {
         Q_OBJECT

         public:
            MyTestSuite(QObject* parent = 0) : TestFramework(parent) {} // Called once on loading the test runner!
            virtual ~MyTestSuite() {} // Called when the test runner terminates!

            void init() override; // Executed every time before the tests of this class are executed. Can be used to set a common environment for the tests.
            void deinit() override; // Executed every time after the tests have been executed. Can be used to clean up after the tests.

         public slots:
            void testCase();
      };

  The test suites are instantiated by the test runner and destroyed when the test runner terminates.

  The functions `init()` and `deinit()` are always called befor and after the testes are executed.

  Example of a test suite implementation:

      void MyTestSuite::testCase()
      {
         QString text;
         int i = 42;

         T_ASSERT(text.isEmpty());
         T_EQUALS(i, 42);
         T_IS_TRUE(i);
      }

      BEGIN_TEST_SUITES
         TEST_SUITE(MyTestSuite);
      END_TEST_SUITES

  In the `BEGIN_TEST_SUITES` ... `END_TEST_SUITES` section the test suites are registered for execution.

  To signal test success or failure there are some macros (to be extended):
  - T_IS_TRUE
  - T_EQUALS
  - T_ASSERT
  - T_ASSERT_EQUAL

  The difference between the ASSERT macros and the other macros is that the ASSERT macros terminate test execution by throwing a `TestException` when the test fails.
  The other functions just add an error entry on failure.
  The test can call `addError()` to add an error entry. Throwing a `TestException` also makes an error entry with the given text.

  All exceptions occuring in a test are catched and reported.

  Additionally the are variants of the T_EQUALS macro:
  - T_EQUALS_EX: The arguments of the macro are converted to string with the `toQString()` method (which has to be defined elsewhere).
  - T_EQUALS_TS: The first two arguments of the macro are converted to string with the function given as third argument.
*/
class TestFramework : public QObject
{
   Q_OBJECT

public:
   TestFramework(QObject* parent = 0);
   virtual ~TestFramework();

   void           setDataDir(const QString& path) { _dataDir = path; }
   const QString& dataDir() const { return _dataDir; }

   void     setParentWindow(QWidget* parent) { _parentWindow = parent; }
   QWidget* parentWindow() const { return _parentWindow; }

   virtual void init();
   virtual void deinit();

   inline static void fatalIfNot(bool predicate, const char* message)
   {
      if (!predicate)
         throw TestException(message);
   }

   inline static void fatalIfNot(bool predicate, const QString& message)
   {
      if (!predicate)
         throw TestException(message);
   }

   inline static void fatalIf(bool predicate, const char* message)
   {
      if (predicate)
         throw TestException(message);
   }

   inline static void fatalIf(bool predicate, const QString& message)
   {
      if (predicate)
         throw TestException(message);
   }

   template<typename T1, typename T2>
   inline static void fatalIfNotEqual(const T1& left, const T2& right, const QString& message)
   {
      if (!(left == right))
         throw TestException(message.arg(left).arg(right));
   }

   template<typename T1, typename T2>
   inline static void fatalIfNotEqualEx(const T1& left, const T2& right, const QString& message)
   {
      if (!(left == right))
         throw TestException(message.arg(toQString(left).left(200)).arg(toQString(right).left(200)));
   }

   inline void errorIfNot(bool predicate, const char* message)
   {
      if (!predicate)
         addError(message);
   }

   inline void errorIfNot(bool predicate, const QString& message)
   {
      if (!predicate)
         addError(message);
   }

   inline void errorIf(bool predicate, const char* message)
   {
      if (predicate)
         addError(message);
   }

   inline void errorIf(bool predicate, const QString& message)
   {
      if (predicate)
         addError(message);
   }

   template<typename T1, typename T2>
   inline void errorIfNotEqual(const T1& left, const T2& right, const QString& message)
   {
      if (!(left == right))
         addError(message.arg(left).arg(right));
   }

   template<typename T1, typename T2>
   inline void errorIfNotEqualEx(const T1& left, const T2& right, const QString& message)
   {
      if (!(left == right))
         addError(message.arg(toQString(left).left(200)).arg(toQString(right).left(200)));
   }

signals:
   void onError(const QString& message);
   void onWarning(const QString& message);

protected:
   void addError(const char* szError) { addError(QString::fromUtf8(szError)); }
   void addError(const QString& error) { emit onError(error); }
   void addWarning(const char* szWarning) { addWarning(QString::fromUtf8(szWarning)); }
   void addWarning(const QString& warning) { emit onWarning(warning); }

private:
   QString _dataDir;
   QWidget* _parentWindow;
};

#endif // TESTFRAMEWORK_H
