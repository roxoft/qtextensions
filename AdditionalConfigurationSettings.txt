Configure linux (/opt/Qt/5.x/gcc_64)
------------------------------------

Install build-essential if not yet installed.

Kubuntu 16.04 and newer: Additionally install the libgl1-mesa-dev library (includes mesa-common-dev) from the repository.

To compile QtGuiExDesignerPlugin give write access to the following subdirectories of QTDIR:
  lib
  plugins/designer

To compile QtODBCConnector unixodbc-dev must be installed.

Installation
------------

Kubuntu 16.04: To install with jom add -j1 to the command line to force single thread: -j1 install

Unit test configuration
-----------------------

### Testing QtExtensions projects ###

Qt Creator:
   Executable: QtTestRunner.exe
   Arguments: [QtGuiEx.dll] [[%{sourceDir}\..\TestData]]
   Working directory: %PWD%\..\x86\Debug

Visual Studio:
   Command: $(SolutionDir)$(PlatformTarget)\$(Configuration)\QtTestRunner.exe
   Arguments: QtGuiEx.dll $(SolutionDir)TestData
   Working directory: $(TargetDir)

#### Testing DbConnector projects ####

To test the projects QtOracleConnector, QtODBCConnector or QtDB2Connector a connection to an existing database has to be established.

To do this there are additional .ini files in the TestData folder:

**DB2Credentials.ini:

    Alias=<Db Alias>
    Host=<ServerAddress>
    Database=<DbName>
    User=<UserName>
    Password=<Password>
    Schema=<OptionalSchema>

**OracleCredentials.ini:

    Name=<DatabaseAlias>
    Schema=<SchemaName>
    [User]
    User=<UserName>
    Password=<Password>
    Role=<Role>
    [Admin]
    User=<UserName>
    Password=<Password>

**SqlServerCredentials.ini

    Driver=<DriverName> # For example {ODBC Driver 17 for SQL Server} (see /etc/odbcinst.ini)
    Host=<ServerAddress>
    Database=<DatabaseName>
    User=<UserName>
    Password=<Password>

### Testing programs using QtExtensions ###

Visual Studio:
   Command: $(QTXDIR)\$(PlatformTarget)\$(Configuration)\QtTestRunner.exe
   Arguments: $(TargetDir)\YourModule.dll $(SolutionDir)TestData
   Working directory: $(QTDIR)
