#include "QDB2Environment.h"
#include <QTextCodec>

QDbState checkerr(SQLRETURN status, SQLHANDLE handle, SQLSMALLINT type, bool throwOnError)
{
   QDbState dbState;

   if (status != SQL_SUCCESS)
   {
      SQLINTEGER  i = 0;
      SQLINTEGER  native = 0;
      QString     message;
      int         vendorCode = 0;

      SQLWCHAR    state[7];
      SQLWCHAR    text[256];
      SQLSMALLINT len = 0;
      SQLRETURN   ret = 0;

      do
      {
         ret = SQLGetDiagRec(type, handle, ++i, state, &native, text, sizeof(text) / sizeof(*text), &len);
         if (SQL_SUCCEEDED(ret))
         {
            auto sqlDiag = QString::fromUtf16((const char16_t *)text);

            if (len >= sizeof(text) / sizeof(*text))
               sqlDiag += "...";
            if (!vendorCode)
               vendorCode = native;
            if (!message.isEmpty())
               message += QLatin1String("\n");
            message += QString::fromLatin1("%1:%2:%3:%4").arg(QString::fromUtf16((const char16_t *)state)).arg(i).arg(native).arg(sqlDiag);
         }
      }
      while (ret == SQL_SUCCESS);

      QDbState::Type stateType = QDbState::Error;

      switch (status)
      {
      case SQL_SUCCESS_WITH_INFO:
         stateType = QDbState::Information;
         break;
      case SQL_NO_DATA:
         stateType = QDbState::Warning;
         break;
      }

      QDbState::Source stateSource = QDbState::Unknown;

      switch (type)
      {
      case SQL_HANDLE_ENV:
         stateSource = QDbState::Connection;
         break;
      case SQL_HANDLE_DBC:
         stateSource = QDbState::Connection;
         break;
      case SQL_HANDLE_STMT:
         stateSource = QDbState::Statement;
         break;
      case SQL_HANDLE_DESC:
         stateSource = QDbState::Statement;
         break;
      }

      dbState = QDbState(stateType, message, stateSource, vendorCode);
   }

   if (throwOnError && dbState.type() == QDbState::Error)
      throw dbState;

   return dbState;
}

QDB2Environment* QDB2Environment::theEnvironment = nullptr;
int QDB2Environment::environmentRefCount = 0;

QDB2Environment::QDB2Environment() : _env(0)
{
   /* Allocate an environment handle */
   _dbState = checkerr(SQLAllocHandle(SQL_HANDLE_ENV, SQL_NULL_HANDLE, &_env), 0, SQL_NULL_HANDLE, false);
   if (_dbState.type() != QDbState::Error)
   {
      /* We want ODBC 3 support */
      _dbState = checkerr(SQLSetEnvAttr(_env, SQL_ATTR_ODBC_VERSION, (void *) SQL_OV_ODBC3, 0), _env, SQL_HANDLE_ENV, false);
   }
}

QDB2Environment::~QDB2Environment()
{
   SQLFreeHandle(SQL_HANDLE_ENV, _env);
}

void QDB2Environment::attach()
{
   if (theEnvironment == nullptr)
      theEnvironment = new QDB2Environment;
   environmentRefCount++;
}

void QDB2Environment::detach()
{
   environmentRefCount--;
   if (environmentRefCount == 0 && theEnvironment)
   {
      delete theEnvironment;
      theEnvironment = nullptr;
   }
}
