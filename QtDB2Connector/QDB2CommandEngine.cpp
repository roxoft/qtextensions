#include "QDB2CommandEngine.h"
#include <QTextCodec>

QDB2CommandEngine::QDB2CommandEngine(QDB2Connection* connection, SQLHSTMT hstmt) : _connection(connection), _hstmt(hstmt)
{
}

QDB2CommandEngine::~QDB2CommandEngine()
{
   qDeleteAll(_resultList);
   if (_hstmt)
   {
      SQLFreeHandle(SQL_HANDLE_STMT, _hstmt);
      _hstmt = NULL;
   }
}

QDbState QDB2CommandEngine::rowCount(int &count) const
{
   QDbState dbState(_statement);

   if (_executed)
   {
      SQLLEN rc = 0;

      dbState = checkerr(SQLRowCount(_hstmt, &rc), _hstmt, SQL_HANDLE_STMT, false);
      count = rc;
   }

   return dbState;
}

bool QDB2CommandEngine::atEnd() const
{
   return (unsigned int)_rowIndex == _rowsFetched;
}

QDbState QDB2CommandEngine::next()
{
   QDbState dbState(_statement);

   if (_rowIndex < (int)_rowsFetched)
      _rowIndex++;

   if (_rowIndex == (int)_rowsFetched && _hasMore)
   {
      _rowIndex = 0;
      _rowsFetched = 0;
      _hasMore = false;

      SQLRETURN sqlRet = SQLFetchScroll(_hstmt, SQL_FETCH_NEXT, 0);

      if (sqlRet == SQL_NO_DATA)
      {
         _rowsFetched = 0;
      }
      else
      {
         dbState = checkerr(sqlRet, _hstmt, SQL_HANDLE_STMT, false);
         if (dbState.type() == QDbState::Error)
            return dbState;
      }

      if (_rowsFetched == 0)
      {
         dbState = checkerr(SQLCloseCursor(_hstmt), _hstmt, SQL_HANDLE_STMT, false);
      }
      else
      {
         if (_rowStatusList.count() == 1)
         {
            // Immediately get the values by piecewise get
            for (int i = 0; i < _resultList.count(); ++i)
            {
               dbState = _resultList[i]->getValuePiecewise(_hstmt, i);
               if (dbState.type() == QDbState::Error)
                  return dbState;
            }
         }

         _hasMore = true;
      }
   }

   if (_rowIndex < (int)_rowsFetched && (SQLSMALLINT)_rowStatusList[_rowIndex] == SQL_ERROR)
   {
      dbState = QDbState(QDbState::Error, tr("Row error"), QDbState::Connection);
   }

   return dbState;
}

QDbSchema::Table QDB2CommandEngine::resultSchema() const
{
   QDbSchema::Table table;

   table.name = QLatin1String("SELECT_LIST");
   for (auto&& bindValue : _resultList)
      table.columns.append(bindValue->schema());

   return table;
}

QDbState QDB2CommandEngine::columnValue(int index, Variant &value) const
{
   QDbState dbState(_statement);

   if (_rowIndex >= (int)_rowsFetched)
      dbState = QDbState(QDbState::Error, tr("Invalid position of row counter"), QDbState::Statement);
   else if (index < 0 || index >= _resultList.count())
      dbState = QDbState(QDbState::Error, tr("Index %1 not in result set").arg(index), QDbState::Statement);
   else
   {
      if (_rowStatusList.at(_rowIndex) == SQL_SUCCESS || _rowStatusList.at(_rowIndex) == SQL_SUCCESS_WITH_INFO)
         value = _resultList.at(index)->value(_rowIndex);
      else
         value.clear();
   }

   return dbState;
}

QDbState QDB2CommandEngine::columnValue(const QString &name, Variant &value) const
{
   for (int i = 0; i < _resultList.count(); ++i)
   {
      if (_resultList.at(i)->schema().name.compare(name, Qt::CaseInsensitive) == 0)
         return columnValue(i, value);
   }

   return QDbState(QDbState::Error, tr("Column name %1 not found in result set").arg(name), QDbState::Statement);
}

QDB2CommandEngine::DB2PhInfo::DB2PhInfo(const QDbSchema::Column& schema, QDbConnection::PhType phType, QDB2Connection* c)
   : PhInfo(schema, phType), _connection(c)
{
   updateValueBufferSize();
}

void QDB2CommandEngine::DB2PhInfo::setType(QDbDataType type)
{
   PhInfo::setType(type);

   switch (type)
   {
   case DT_UNKNOWN:
      break;
   case DT_BOOL:
      _schema.vendorType = SQL_BIT;
      break;
   case DT_INT:
   case DT_UINT:
      _schema.vendorType = SQL_INTEGER;
      break;
   case DT_LONG:
   case DT_ULONG:
      _schema.vendorType = SQL_BIGINT;
      break;
   case DT_DOUBLE:
      _schema.vendorType = SQL_DOUBLE;
      break;
   case DT_DECIMAL:
      _schema.vendorType = SQL_NUMERIC;
      break;
   case DT_STRING:
      _schema.vendorType = _connection && _connection->serverCharacterEncoding() ? SQL_VARCHAR : SQL_WVARCHAR;
      break;
   case DT_LARGE_STRING:
      _schema.vendorType = _connection && _connection->serverCharacterEncoding() ? SQL_LONGVARCHAR : SQL_WLONGVARCHAR;
      break;
   case DT_NSTRING:
      _schema.vendorType = SQL_WVARCHAR;
      break;
   case DT_LARGE_NSTRING:
      _schema.vendorType = SQL_WLONGVARCHAR;
      break;
   case DT_DATE:
      _schema.vendorType = SQL_TYPE_DATE;
      break;
   case DT_TIME:
      _schema.vendorType = SQL_TYPE_TIME;
      break;
   case DT_DATETIME:
      _schema.vendorType = SQL_TYPE_TIMESTAMP;
      break;
   case DT_BINARY:
      _schema.vendorType = SQL_VARBINARY;
      break;
   case DT_LARGE_BINARY:
      _schema.vendorType = SQL_LONGVARBINARY;
      break;
   }

   updateValueBufferSize();
}

QDbState QDB2CommandEngine::DB2PhInfo::bindResult(SQLHSTMT hstmt, int index)
{
   if (targetType() == SQL_C_NUMERIC)
      _valueBufferSize = 16; // DB2 reduces the size of NUMERIC buffers fetched into a result set to 16 bytes!

   auto pos = index + 1; // Column positions start with 1

   auto dbState = checkerr(SQLBindCol(hstmt, pos, targetType(), valueBuffer(), _valueBufferSize, indicatorBuffer()), hstmt, SQL_HANDLE_STMT, false);

   if (dbState.type() != QDbState::Error && targetType() == SQL_C_NUMERIC)
   {
      dbState = setNumericDescriptor(hstmt, pos, false, true);
   }

   return dbState;
}

QDbState QDB2CommandEngine::DB2PhInfo::bindParameter(SQLHSTMT hstmt, int index)
{
   // https://docs.microsoft.com/en-us/sql/odbc/reference/develop-app/binding-parameters-by-name-named-parameters?view=sql-server-ver15

   SQLSMALLINT paramType = (!_schema.readable ? SQL_PARAM_INPUT : (!_schema.writable ? SQL_PARAM_OUTPUT : SQL_PARAM_INPUT_OUTPUT));
   SQLPOINTER  buffer = nullptr;
   SQLLEN      bufSize = 0;

   auto pos = index + 1; // Column positions start with 1

   //if (_schema.readable)
   //{
   //   if (_schema.vendorType == SQL_C_CHAR || _schema.vendorType == SQL_C_WCHAR || _schema.vendorType == SQL_C_DBCHAR || _schema.vendorType == SQL_C_BINARY)
   //   {
   //      TODO: Streamed output parameters
   //      See https://www.columbia.edu/sec/acis/db2/db2l0/db2l066.htm
   //      See https://www.ibm.com/support/producthub/db2/docs/content/SSEPGG_11.5.0/com.ibm.db2.luw.apdv.cli.doc/doc/r0000604.html
   //      See IBM\SQLLIB\samples\cli\dtlob.c
   //   }
   //   else
   //   {
   //      buffer = valueBuffer();
   //      bufSize = _valueBufferSize;
   //   }
   //}
   //else
   {
      buffer = valueBuffer();
      bufSize = _valueBufferSize;
   }

   auto dbState = checkerr(SQLBindParameter(hstmt, pos, paramType, targetType(), (SQLSMALLINT)_schema.vendorType, (SQLULEN)_schema.size, _schema.scale, buffer, bufSize, indicatorBuffer()), hstmt, SQL_HANDLE_STMT, false);

   if (dbState.type() != QDbState::Error && targetType() == SQL_C_NUMERIC)
   {
      dbState = setNumericDescriptor(hstmt, pos, true, true);
   }

   return dbState;
}

QDbState QDB2CommandEngine::DB2PhInfo::setNumericDescriptor(SQLHSTMT hstmt, int pos, bool asParameter, bool withBuffer)
{
   // see https://msdn.microsoft.com/en-us/library/ms712567%28v=vs.85%29.aspx

   SQLHDESC hdesc = NULL;

   auto dbState = checkerr(SQLGetStmtAttr(hstmt, asParameter ? SQL_ATTR_APP_PARAM_DESC : SQL_ATTR_APP_ROW_DESC, &hdesc, 0, NULL), hstmt, SQL_HANDLE_STMT, false);

   if (dbState.type() != QDbState::Error)
      dbState = checkerr(SQLSetDescField(hdesc, pos, SQL_DESC_TYPE, (SQLPOINTER) SQL_C_NUMERIC, 0), hdesc, SQL_HANDLE_DESC, false);
   if (dbState.type() != QDbState::Error)
      dbState = checkerr(SQLSetDescField(hdesc, pos, SQL_DESC_CONCISE_TYPE, (SQLPOINTER) SQL_C_NUMERIC, 0), hdesc, SQL_HANDLE_DESC, false);
   if (dbState.type() != QDbState::Error)
      dbState = checkerr(SQLSetDescField(hdesc, pos, SQL_DESC_PRECISION, (SQLPOINTER)(intptr_t)_schema.size, 0), hdesc, SQL_HANDLE_DESC, false);
   if (dbState.type() != QDbState::Error)
      dbState = checkerr(SQLSetDescField(hdesc, pos, SQL_DESC_SCALE, (SQLPOINTER)(intptr_t)_schema.scale, 0), hdesc, SQL_HANDLE_DESC, false);
   if (withBuffer)
   {
      if (dbState.type() != QDbState::Error)
         dbState = checkerr(SQLSetDescField(hdesc, pos, SQL_DESC_DATA_PTR, valueBuffer(), _valueBufferSize), hdesc, SQL_HANDLE_DESC, false);
      if (dbState.type() != QDbState::Error)
         dbState = checkerr(SQLSetDescField(hdesc, pos, SQL_DESC_INDICATOR_PTR, indicatorBuffer(), 0), hdesc, SQL_HANDLE_DESC, false);
   }

   return dbState;
}

QDbState QDB2CommandEngine::DB2PhInfo::getValuePiecewise(SQLHSTMT hstmt, int index)
{
   QDbState dbState;

   auto targetType = this->targetType();

   if (targetType == SQL_C_NUMERIC)
      targetType = SQL_ARD_TYPE; // Use the type specified in the SQL_DESC_CONCISE_TYPE field of the ARD

   const auto pos = index + 1; // Column positions start with 1

   Q_ASSERT(_rowCount == 1);

   SQLLEN currentValueBufferSize = 0;

   for (;;)
   {
      // Get the next chunk from the database

      const SQLRETURN retCode = SQLGetData(hstmt, pos, targetType, (char*)valueBuffer() + (size_t)currentValueBufferSize, _valueBufferSize - currentValueBufferSize, indicatorBuffer());

      // Adjust the indicator buffer to contain the total size of the data

      if (indicatorBuffer()[0] >= 0)
         indicatorBuffer()[0] += currentValueBufferSize;

      // Check if the fetch is terminated

      if (retCode == SQL_SUCCESS || retCode == SQL_NO_DATA)
      {
         if (indicatorBuffer()[0] == SQL_NO_TOTAL)
         {
            indicatorBuffer()[0] = _valueBufferSize;
         }

         Q_ASSERT(indicatorBuffer()[0] == targetType == SQL_C_CHAR ? _valueBufferSize - 1 : targetType == SQL_C_WCHAR || targetType == SQL_C_DBCHAR ? _valueBufferSize - 2 : _valueBufferSize);

         break;
      }

      // Check for errors

      if (retCode != SQL_SUCCESS_WITH_INFO)
      {
         dbState = checkerr(retCode, hstmt, SQL_HANDLE_STMT, false);
         break;
      }

      // Get the total size of the received data (SQLGetData always appends a terminating 0 character)

      currentValueBufferSize = _valueBufferSize; // Fetched so far
      if (targetType == SQL_C_CHAR)
         currentValueBufferSize--;
      else if (targetType == SQL_C_WCHAR || targetType == SQL_C_DBCHAR)
         currentValueBufferSize -= (SQLLEN)sizeof(SQLWCHAR);

      // Calculate the new size of the buffer for the next chunk or the rest of the data

      _valueBufferSize = indicatorBuffer()[0]; // Total size to fetch
      if (_valueBufferSize == SQL_NO_TOTAL)
      {
         // The total size is unknown and we just add a chunk
         _valueBufferSize = currentValueBufferSize + _defaultChunkSize;
      }
      else
      {
         // Append the size for a terminating 0 character
         if (targetType == SQL_C_CHAR)
            _valueBufferSize++;
         else if (targetType == SQL_C_WCHAR || targetType == SQL_C_DBCHAR)
            _valueBufferSize += (SQLLEN)sizeof(SQLWCHAR);
      }

      Q_ASSERT(currentValueBufferSize < _valueBufferSize); // The new buffer must be larger
   }

   return dbState;
}

Variant QDB2CommandEngine::DB2PhInfo::value(int row) const
{
   Variant v;
   SQLLEN lengthIndicator = SQL_NULL_DATA;

   if (_buffer && row < _rowCount)
      lengthIndicator = indicatorBuffer()[row];

   if (lengthIndicator >= 0)
   {
      const void *rawValue = valueBuffer(row);

      // See https://www.ibm.com/support/knowledgecenter/en/SSEPGG_11.1.0/com.ibm.db2.luw.apdv.cli.doc/doc/r0000527.html#r0000527__tbdstrc

      switch (targetType())
      {
      case SQL_C_CHAR:
         if (_connection == nullptr || _connection->serverCharacterEncoding() == nullptr)
            v = QString::fromLocal8Bit((const char *)(const SQLCHAR *)rawValue, qMin(lengthIndicator, _valueBufferSize - 1)); // Fallback
         else
            v = _connection->serverCharacterEncoding()->toUnicode((const char *)(const SQLCHAR *)rawValue, qMin(lengthIndicator, _valueBufferSize - 1));
         break;
      case SQL_C_WCHAR:
         if (sizeof(SQLWCHAR) == sizeof(ushort))
            v = QString::fromUtf16((const char16_t *)rawValue, qMin(lengthIndicator, _valueBufferSize - (SQLLEN)sizeof(ushort)) / (SQLLEN)sizeof(ushort));
         else
            v = QString::fromWCharArray((const wchar_t*)rawValue, qMin(lengthIndicator, _valueBufferSize - (SQLLEN)sizeof(SQLWCHAR)) / (SQLLEN)sizeof(SQLWCHAR));
         break;
      case SQL_C_DBCHAR:
         if (sizeof(SQLDBCHAR) == sizeof(ushort))
            v = QString::fromUtf16((const char16_t *)rawValue, qMin(lengthIndicator, _valueBufferSize - (SQLLEN)sizeof(ushort)) / (SQLLEN)sizeof(ushort));
         else
            v = QString::fromWCharArray((const wchar_t*)rawValue, qMin(lengthIndicator, _valueBufferSize - (SQLLEN)sizeof(SQLDBCHAR)) / (SQLLEN)sizeof(SQLDBCHAR));
         break;
      case SQL_C_SHORT:
         v = (int)*(const SQLSMALLINT *)rawValue;
         break;
      case SQL_C_USHORT:
         v = (unsigned int)*(const SQLUSMALLINT *)rawValue;
         break;
      case SQL_C_TINYINT:
         v = (int)*(const SQLCHAR *)rawValue;
         break;
      case SQL_C_UTINYINT:
         v = (unsigned int)*(const UCHAR *)rawValue;
         break;
      case SQL_C_LONG:
         v = *(const SQLINTEGER *)rawValue;
         break;
      case SQL_C_ULONG:
         v = *(const SQLUINTEGER *)rawValue;
         break;
      case SQL_C_FLOAT:
         v = (double)*(const SQLREAL *)rawValue;
         break;
      case SQL_C_DOUBLE:
         v = *(const SQLDOUBLE *)rawValue;
         break;
      case SQL_C_BIT:
         v = (*(const SQLCHAR *)rawValue != 0);
         break;
      case SQL_C_SBIGINT:
         v = *(const SQLBIGINT *)rawValue;
         break;
      case SQL_C_UBIGINT:
         v = *(const SQLUBIGINT *)rawValue;
         break;
      case SQL_C_NUMERIC: // Windows only
         {
            const auto number = (const SQL_NUMERIC_STRUCT *)rawValue;

            // In result sets the buffer size for NUMERIC values is less than the size for an SQL_NUMERIC_STRUCT (see bindResult())!
            // This reduces the size of the mantissa:
            const auto numericSize = SQL_MAX_NUMERIC_LEN + _valueBufferSize - (int)sizeof(SQL_NUMERIC_STRUCT);

            QDecimal decimal;

            decimal.fromOdbcNumber(number->val, numericSize, number->scale, number->sign != 1);

            if (_schema.scale >= 0)
               decimal.setMinFractionalDigits(_schema.scale);

            v = decimal;
         }
         break;
      case SQL_C_DECIMAL64:
         {
            const auto number = (const SQLDECIMAL64 *)rawValue;

            // TODO: Convert the number
         }
         break;
      case SQL_C_DECIMAL128:
         {
            const auto number = (const SQLDECIMAL128 *)rawValue;

            // TODO: Convert the number
         }
         break;
      case SQL_C_BINARY:
         v = QByteArray((const char *)(const SQLCHAR *)rawValue, qMin(lengthIndicator, _valueBufferSize));
      case SQL_C_BINARYXML:
         v = QByteArray((const char *)(const SQLCHAR *)rawValue, qMin(lengthIndicator, _valueBufferSize));
         break;
      case SQL_C_TYPE_DATE:
         {
            const auto date = (const SQL_DATE_STRUCT *)rawValue;

            v = QDateEx(date->year, date->month, date->day);
         }
         break;
      case SQL_C_TYPE_TIME:
         {
            const auto time = (const SQL_TIME_STRUCT *)rawValue;

            v = QTimeEx(time->hour, time->minute, time->second);
         }
         break;
      case SQL_C_TYPE_TIMESTAMP:
         {
            // see https://www.ibm.com/support/knowledgecenter/SSEPGG_11.5.0/com.ibm.db2.luw.apdv.cli.doc/doc/r0000527.html
            // and https://www.ibm.com/support/knowledgecenter/en/SSEPEK_12.0.0/odbc/src/tpc/db2z_odbcvariabletimestamps.html
            const auto timestamp = (const SQL_TIMESTAMP_STRUCT *)rawValue;

            v = QDateTimeEx(timestamp->year, timestamp->month, timestamp->day, timestamp->hour, timestamp->minute, timestamp->second, ((int)timestamp->fraction + 500000) / 1000000);
         }
         break;
      case SQL_C_TYPE_TIMESTAMP_EXT:
         {
            // see https://www.ibm.com/support/knowledgecenter/SSEPGG_11.5.0/com.ibm.db2.luw.apdv.cli.doc/doc/r0000527.html
            // and https://www.ibm.com/support/knowledgecenter/en/SSEPEK_12.0.0/odbc/src/tpc/db2z_odbcvariabletimestamps.html
            const auto timestamp = (const TIMESTAMP_STRUCT_EXT *)rawValue;

            v = QDateTimeEx(timestamp->year, timestamp->month, timestamp->day, timestamp->hour, timestamp->minute, timestamp->second, ((int)timestamp->fraction + 500000) / 1000000); // timestamp->fraction2 can be ignored as we do not handle picoseconds
         }
         break;
      case SQL_C_TYPE_TIMESTAMP_EXT_TZ:
         {
            // see https://www.ibm.com/support/knowledgecenter/SSEPGG_11.5.0/com.ibm.db2.luw.apdv.cli.doc/doc/r0000527.html
            const auto timestamp = (const TIMESTAMP_STRUCT_EXT_TZ *)rawValue;

            v = QDateTimeEx(timestamp->year, timestamp->month, timestamp->day, timestamp->hour, timestamp->minute, timestamp->second, ((int)timestamp->fraction + 500000) / 1000000, timestamp->timezone_hour * 60 + timestamp->timezone_minute, false); // timestamp->fraction2 can be ignored as we do not handle picoseconds
         }
         break;
      case SQL_C_BLOB_LOCATOR:
         v = *(const SQLINTEGER *)rawValue;
         break;
      case SQL_C_CLOB_LOCATOR:
         v = *(const SQLINTEGER *)rawValue;
         break;
      case SQL_C_DBCLOB_LOCATOR:
         v = *(const SQLINTEGER *)rawValue;
         break;
      case SQL_C_CURSORHANDLE:
         v = *(const SQLINTEGER *)rawValue;
         break;
      }
   }

   return v;
}

QDbState QDB2CommandEngine::DB2PhInfo::setValue(const Variant &value, int row)
{
   if (value.isValueNull())
   {
      indicatorBuffer()[row] = SQL_NULL_DATA;
      return QDbState();
   }

   auto ok = false;

   switch (targetType())
   {
   case SQL_C_CHAR:
   case SQL_C_WCHAR:
   case SQL_C_DBCHAR:
      {
         const std::type_info& valueType = value.type();
         QString text;

         if (valueType == typeid(bool))
            text = value.toBool() ? u'1' : u'0';
         else if (valueType == typeid(QTime))
            text = value.toTime().toString(SQL92_TIME_FORMAT);
         else if (valueType == typeid(QDateTimeEx) || valueType == typeid(QTimeEx))
            text = value.toDateTimeEx().toString(SQL92_TIME_FORMAT);
         else
            text = value.toString(QLocale::C);
         ok = true; // Conversion to string is always lossless

         Q_ASSERT(!text.isNull()); // This is handled by Variant::isValueNull()

         _schema.size = qMax(text.length(), _schema.size);
         _schema.scale = 0;
         if (targetType() == SQL_C_CHAR)
         {
            QByteArray data;

            if (_connection == nullptr || _connection->serverCharacterEncoding() == nullptr)
               data = text.toLocal8Bit(); // Fallback
            else
               data = _connection->serverCharacterEncoding()->fromUnicode(text);

            _valueBufferSize = qMax((SQLLEN)data.length(), _valueBufferSize); // qMax is necessary for in/out parameters of stored procedures
            indicatorBuffer()[row] = (SQLLEN)data.length();
            memcpy(valueBuffer(row), data.data(), (size_t)data.length());
         }
         else if (targetType() == SQL_C_WCHAR && sizeof(SQLWCHAR) == sizeof(ushort) || targetType() == SQL_C_DBCHAR && sizeof(SQLDBCHAR) == sizeof(ushort))
         {
            _valueBufferSize = qMax((SQLLEN)text.length() * (SQLLEN)sizeof(ushort), _valueBufferSize); // qMax is necessary for in/out parameters of stored procedures
            indicatorBuffer()[row] = (SQLLEN)text.length() * (SQLLEN)sizeof(ushort);
            memcpy(valueBuffer(row), text.utf16(), (size_t)text.length() * sizeof(ushort));
         }
         else
         {
            _valueBufferSize = qMax((SQLLEN)text.length() * (SQLLEN)sizeof(wchar_t), _valueBufferSize); // qMax is necessary for in/out parameters of stored procedures
            indicatorBuffer()[row] = (SQLLEN)text.length() * (SQLLEN)sizeof(wchar_t);
            text.toWCharArray((wchar_t*)valueBuffer(row));
         }
      }
      break;
   case SQL_C_LONG:
      _schema.size = 10;
      _schema.scale = 0;
      _valueBufferSize = (SQLLEN)sizeof(SQLINTEGER);
      indicatorBuffer()[row] = 0;
      *(SQLINTEGER*)valueBuffer(row) = value.toInt(&ok);
      break;
   case SQL_C_ULONG:
      _schema.size = 10;
      _schema.scale = 0;
      _valueBufferSize = (SQLLEN)sizeof(SQLUINTEGER);
      indicatorBuffer()[row] = 0;
      *(SQLUINTEGER*)valueBuffer(row) = value.toUInt(&ok);
      break;
   case SQL_C_DOUBLE:
      _schema.size = 15;
      _schema.scale = 0;
      _valueBufferSize = (SQLLEN)sizeof(SQLDOUBLE);
      indicatorBuffer()[row] = 0;
      *(SQLDOUBLE*)valueBuffer(row) = value.toDouble(&ok);
      break;
   case SQL_C_BIT:
      _schema.size = 1;
      _schema.scale = 0;
      _valueBufferSize = (SQLLEN)sizeof(SQLCHAR);
      indicatorBuffer()[row] = 0;
      *(SQLCHAR*)valueBuffer(row) = value.toBool(&ok) ? 1 : 0;
      break;
   case SQL_C_SBIGINT:
      _schema.size = 19;
      _schema.scale = 0;
      _valueBufferSize = (SQLLEN)sizeof(SQLBIGINT);
      indicatorBuffer()[row] = 0;
      *(SQLBIGINT*)valueBuffer(row) = value.toInt64(&ok);
      break;
   case SQL_C_UBIGINT:
      _schema.size = 20;
      _schema.scale = 0;
      _valueBufferSize = (SQLLEN)sizeof(SQLUBIGINT);
      indicatorBuffer()[row] = 0;
      *(SQLUBIGINT*)valueBuffer(row) = value.toUInt64(&ok);
      break;
   case SQL_C_NUMERIC:
      {
         const auto decimal = value.toDecimal(&ok);

         _schema.size = decimal.length();
         _schema.scale = decimal.fractionalDigits();
         _valueBufferSize = (SQLLEN)sizeof(SQL_NUMERIC_STRUCT);

         Q_ASSERT(!decimal.isNull()); // This is handled by Variant::isValueNull()

         indicatorBuffer()[row] = 0;

         auto number = (SQL_NUMERIC_STRUCT*)valueBuffer(row);

         decimal.toOdbcNumber(number->val, SQL_MAX_NUMERIC_LEN);
         number->sign = decimal.isNegative() ? -1 : 1;
         number->precision = _schema.size;
         number->scale = _schema.scale;
      }
      break;
   case SQL_C_BINARY:
      {
         const auto blob = value.toByteArray(&ok);

         Q_ASSERT(!blob.isNull()); // This is handled by Variant::isValueNull()

         _schema.size = qMax(blob.length(), _schema.size);
         _schema.scale = 0;
         _valueBufferSize = qMax((SQLLEN)blob.length(), _valueBufferSize); // qMax is necessary for in/out parameters of stored procedures
         indicatorBuffer()[row] = (SQLLEN)blob.length();
         memcpy(valueBuffer(row), blob.data(), (size_t)blob.length());
      }
      break;
   case SQL_C_TYPE_DATE:
      {
         const auto date = value.toDateEx(&ok);

         _schema.size = 10;
         _schema.scale = 0;
         _valueBufferSize = (SQLLEN)sizeof(SQL_DATE_STRUCT);

         if (date.maxPrecision() == DateTimePart::Millennium)
         {
            indicatorBuffer()[row] = 0;

            SQL_DATE_STRUCT *dv = (SQL_DATE_STRUCT*)valueBuffer(row);

            dv->year = date.year();
            dv->month = qMax(date.month(), 1);
            dv->day = qMax(date.day(), 1);
         }
         else if (ok)
         {
            qFatal("Invalid date value");

            indicatorBuffer()[row] = SQL_NULL_DATA;
         }
      }
      break;
   case SQL_C_TYPE_TIME:
      {
         const auto time = value.toTimeEx(&ok);

         _schema.size = 8;
         _schema.scale = 0;
         _valueBufferSize = (SQLLEN)sizeof(SQL_TIME_STRUCT);

         if (time.maxPrecision() >= DateTimePart::Hour)
         {
            indicatorBuffer()[row] = 0;

            SQL_TIME_STRUCT *tv = (SQL_TIME_STRUCT*)valueBuffer(row);

            tv->hour = qMax(time.hour(), 0);
            tv->minute = qMax(time.minute(), 0);
            tv->second = qMax(time.second(), 0);
         }
         else if (ok)
         {
            qFatal("Invalid time value");

            indicatorBuffer()[row] = SQL_NULL_DATA;
         }
      }
      break;
   case SQL_C_TYPE_TIMESTAMP:
      {
         const auto dateTime = value.toDateTimeEx(&ok);

         if (dateTime.maxPrecision() == DateTimePart::Millennium)
         {
            _schema.size = 23;
            _schema.scale = 3;
            _valueBufferSize = (SQLLEN)sizeof(SQL_TIMESTAMP_STRUCT);

            indicatorBuffer()[row] = 0;

            auto tsv = (SQL_TIMESTAMP_STRUCT*)valueBuffer(row);

            tsv->year = dateTime.year();
            tsv->month = qMax(dateTime.month(), 1);
            tsv->day = qMax(dateTime.day(), 1);
            tsv->hour = qMax(dateTime.hour(), 0);
            tsv->minute = qMax(dateTime.minute(), 0);
            tsv->second = qMax(dateTime.second(), 0);
            tsv->fraction = qMax(dateTime.millisecond(), 0) * 1000000;
         }
         else if (ok && dateTime.maxPrecision() == DateTimePart::Hour)
         {
            // This is an error because Variant already stores the correct type
            qFatal("QTimeEx value with SQL_C_TYPE_TIMESTAMP type");

            _schema.type = DT_TIME;
            _schema.vendorType = SQL_TYPE_TIME;
            _schema.size = 8;
            _schema.scale = 0;
            _cType = SQL_C_TYPE_TIME;
            _valueBufferSize = (SQLLEN)sizeof(SQL_TIME_STRUCT);

            indicatorBuffer()[row] = 0;

            auto tv = (SQL_TIME_STRUCT*)valueBuffer(row);

            tv->hour = dateTime.hour();
            tv->minute = qMax(dateTime.minute(), 0);
            tv->second = qMax(dateTime.second(), 0);
         }
         else if (ok)
         {
            qFatal("Invalid timestamp value");

            indicatorBuffer()[row] = SQL_NULL_DATA;
         }
      }
      break;
   }

   if (!ok)
      return QDbState(QDbState::Error, "Value cannot be converted to placeholder type without losing information");
   return QDbState();
}

void *QDB2CommandEngine::DB2PhInfo::valueBuffer(int row)
{
   createBuffer();
   return _buffer + (size_t)_rowCount * sizeof(SQLLEN) + (size_t)row * (size_t)_valueBufferSize;
}

const void *QDB2CommandEngine::DB2PhInfo::valueBuffer(int row) const
{
   if (_buffer)
      return _buffer + (size_t)_rowCount * sizeof(SQLLEN) + (size_t)row * (size_t)_valueBufferSize;
   return nullptr;
}

bool QDB2CommandEngine::DB2PhInfo::limitValueBufferSize()
{
   if (_valueBufferSize > _valueBufferSizeLimit)
   {
      _valueBufferSize = _defaultChunkSize;
      return true;
   }

   return false;
}

SQLLEN *QDB2CommandEngine::DB2PhInfo::indicatorBuffer()
{
   createBuffer();
   return (SQLLEN *)_buffer;
}

const SQLLEN *QDB2CommandEngine::DB2PhInfo::indicatorBuffer() const
{
   return (const SQLLEN *)_buffer;
}

SQLSMALLINT QDB2CommandEngine::DB2PhInfo::targetType()
{
   if (_cType == SQL_UNKNOWN_TYPE)
   {
      switch (_schema.vendorType)
      {
      case SQL_CHAR:
      case SQL_VARCHAR:
      case SQL_LONGVARCHAR:
      case SQL_CLOB:
         _cType = SQL_C_CHAR;
         break;
      case SQL_WCHAR:
      case SQL_WVARCHAR:
      case SQL_WLONGVARCHAR:
      case SQL_GRAPHIC:
      case SQL_VARGRAPHIC:
      case SQL_LONGVARGRAPHIC:
      case SQL_DBCLOB:
         _cType = SQL_C_WCHAR;
         break;
      case SQL_CLOB_LOCATOR: // Not supported by the engine!
         _cType = SQL_C_CLOB_LOCATOR;
         break;
      case SQL_DBCLOB_LOCATOR: // Not supported by the engine!
         _cType = SQL_C_DBCLOB_LOCATOR;
         break;
      case SQL_DECIMAL:
      case SQL_NUMERIC:
         _cType = SQL_C_NUMERIC; // SQL_C_CHAR
         break;
      case SQL_BIT:
         _cType = SQL_C_BIT;
         break;
      case SQL_TINYINT:
      case SQL_SMALLINT:
      case SQL_INTEGER:
         _cType = SQL_C_LONG;
         break;
      case SQL_BIGINT:
         _cType = SQL_C_SBIGINT;
         break;
      case SQL_REAL:
      case SQL_FLOAT:
      case SQL_DOUBLE:
         _cType = SQL_C_DOUBLE;
         break;
      case SQL_BINARY:
      case SQL_VARBINARY:
      case SQL_LONGVARBINARY:
      case SQL_BLOB:
         _cType = SQL_C_BINARY;
         break;
      case SQL_BLOB_LOCATOR: // Not supported by the engine!
         _cType = SQL_C_BLOB_LOCATOR;
         break;
      case SQL_TYPE_DATE:
         _cType = SQL_C_TYPE_DATE;
         break;
      case SQL_TYPE_TIME:
         _cType = SQL_C_TYPE_TIME;
         break;
      case SQL_TYPE_TIMESTAMP:
         _cType = SQL_C_TYPE_TIMESTAMP;
         break;
      }
   }

   return _cType;
}

void QDB2CommandEngine::DB2PhInfo::updateValueBufferSize()
{
   if (_buffer)
   {
      free(_buffer);
      _buffer = nullptr;
   }

   switch (targetType())
   {
   case SQL_C_CHAR:
      if (_schema.size > INT_MAX - 1)
      {
         _valueBufferSize = INT_MAX;
      }
      else
      {
         _valueBufferSize = (SQLLEN)_schema.size + 1; // Add space for the terminating 0 character
      }
      break;
   case SQL_C_WCHAR:
      if (_schema.size > INT_MAX / (int)sizeof(SQLWCHAR) - 1)
      {
         _valueBufferSize = INT_MAX;
      }
      else
      {
         _valueBufferSize = ((SQLLEN)_schema.size + 1) * (SQLLEN)sizeof(SQLWCHAR); // Add space for the terminating 0 character
      }
      break;
   case SQL_C_DBCHAR:
      if (_schema.size > INT_MAX / (int)sizeof(SQLDBCHAR) - 1)
      {
         _valueBufferSize = INT_MAX;
      }
      else
      {
         _valueBufferSize = ((SQLLEN)_schema.size + 1) * (SQLLEN)sizeof(SQLDBCHAR); // Add space for the terminating 0 character
      }
      break;
   case SQL_C_LONG:
      _valueBufferSize = (SQLLEN)sizeof(SQLINTEGER);
      break;
   case SQL_C_ULONG:
      _valueBufferSize = (SQLLEN)sizeof(SQLUINTEGER);
      break;
   case SQL_C_DOUBLE:
      _valueBufferSize = (SQLLEN)sizeof(SQLDOUBLE);
      break;
   case SQL_C_BIT:
      _valueBufferSize = (SQLLEN)sizeof(SQLCHAR);
      break;
   case SQL_C_SBIGINT:
      _valueBufferSize = (SQLLEN)sizeof(SQLBIGINT);
      break;
   case SQL_C_UBIGINT:
      _valueBufferSize = (SQLLEN)sizeof(SQLUBIGINT);
      break;
   case SQL_C_NUMERIC:
      _valueBufferSize = (SQLLEN)sizeof(SQL_NUMERIC_STRUCT);
      break;
   case SQL_C_BINARY:
      _valueBufferSize = (SQLLEN)_schema.size;
      break;
   case SQL_C_TYPE_DATE:
      _valueBufferSize = (SQLLEN)sizeof(SQL_DATE_STRUCT);
      break;
   case SQL_C_TYPE_TIME:
      _valueBufferSize = (SQLLEN)sizeof(SQL_TIME_STRUCT);
      break;
   case SQL_C_TYPE_TIMESTAMP:
      _valueBufferSize = (SQLLEN)sizeof(SQL_TIMESTAMP_STRUCT);
      break;
   }
}

void QDB2CommandEngine::DB2PhInfo::createBuffer()
{
   Q_ASSERT(_valueBufferSize < 0x40000000LL); // Protect for unreasonable buffer sizes

   int bufferSize = _rowCount * ((int)sizeof(SQLLEN) + (int)_valueBufferSize);
   void* oldBuffer = _buffer;

   if (bufferSize > _bufferSize)
      _buffer = nullptr;
   else
      bufferSize = _bufferSize; // Thats a precaution because in this case _buffer should never be null

   if (_buffer == nullptr)
   {
      _buffer = (quint8 *)malloc(bufferSize);
      if (oldBuffer)
      {
         if (_rowCount == 1)
            memcpy(_buffer, oldBuffer, _bufferSize);
         free(oldBuffer);
         oldBuffer = nullptr;
      }
      else if (_rowCount == 1)
      {
         *(SQLLEN *)_buffer = _schema.hasDefault ? SQL_DEFAULT_PARAM : SQL_NULL_DATA;
      }
      _bufferSize = bufferSize;
   }
}

QDbCommandEngine::PhInfo* QDB2CommandEngine::createPhInfo(const QDbSchema::Column& schema, QDbConnection::PhType phType)
{
   return new DB2PhInfo(schema, phType, _connection.data());
}

QDbState QDB2CommandEngine::setPlaceholderType(PhInfo* phInfo, QDbDataType type)
{
   if (phInfo->type() != type)
   {
      if (_prepared)
         return QDbState(QDbState::Error, tr("The data type of the placeholder at position %1 cannot be set after execution").arg(phInfo->positions().first()), _statement);

      phInfo->setType(type);
      if (!phInfo->isValid())
         return errorInferringDataType(type, phInfo);
   }

   return QDbState();
}

QDbState QDB2CommandEngine::setPlaceholderValue(PhInfo* phInfo, const Variant& value)
{
   if (phInfo->type() == DT_UNKNOWN)
   {
      // Infer the type from the value
      const auto type = dbTypeFromVariant(value);

      if (type == DT_UNKNOWN)
         return errorInferringDataType(value, phInfo);

      phInfo->setType(type);
      if (!phInfo->isValid())
         return errorInferringDataType(type, phInfo);
   }

   return phInfo->setValue(value);
}

QDbState QDB2CommandEngine::placeholderValue(const PhInfo* phInfo, Variant& value) const
{
   if (!phInfo->isValid())
      return errorInvalidVariable(phInfo);

   value = phInfo->value();

   return QDbState();
}

QDbState QDB2CommandEngine::prepare(const QList<PhInfo*>& phList)
{
   // Prepare the statement to bind the placeholders

   if (_executed)
   {
      // Reset the statement
      SQLFreeStmt(_hstmt, SQL_CLOSE);
      SQLFreeStmt(_hstmt, SQL_UNBIND);
      SQLFreeStmt(_hstmt, SQL_RESET_PARAMS);
      // _hstmt is deleted in the destructor

      _executed = false;
   }

   auto dbState = prepareStatement(phList);

   if (dbState.type() == QDbState::Error)
      return dbState.withSourceText(_statement);

   dbState = checkerr(SQLPrepare(_hstmt, (SQLWCHAR *)_preparedStatement.utf16(), SQL_NTS), _hstmt, SQL_HANDLE_STMT, false);
   if (dbState.type() == QDbState::Error)
      return dbState.withSourceText(_statement);

   _prepared = true;

   return dbState;
}

QDbState QDB2CommandEngine::exec(const QList<PhInfo*>& phList, int rowsToFetch)
{
   QDbState dbState(_statement);

   _rowsFetched = 0;
   qDeleteAll(_resultList);
   _resultList.clear();
   _rowStatusList.clear();
   _rowIndex = 0;
   _hasMore = false;

   if (_executed)
   {
      if (_prepared)
      {
         SQLFreeStmt(_hstmt, SQL_UNBIND);
         SQLFreeStmt(_hstmt, SQL_RESET_PARAMS);
      }
      else
      {
         // Reset the statement
         SQLFreeStmt(_hstmt, SQL_CLOSE);
         SQLFreeStmt(_hstmt, SQL_UNBIND);
         SQLFreeStmt(_hstmt, SQL_RESET_PARAMS);
         // _hstmt is deleted in the destructor
      }

      _executed = false;
   }

   if (!_prepared)
   {
      //if (_functionSchema.isValid())
      //   dbState = prepare(phList);
      //else
         dbState = prepareStatement(phList);

      if (dbState.type() == QDbState::Error)
         return dbState;
   }

   // Bind placeholders
   for (int index = 0; index < phList.count(); ++index)
   {
      auto ph = dynamic_cast<DB2PhInfo*>(phList[index]);

      for (auto&& pos : ph->positions())
      {
         dbState = ph->bindParameter(_hstmt, pos - 1);
         if (dbState.type() == QDbState::Error)
            return dbState;
      }
   }

   // Execute the statement
   SQLRETURN retCode;
   if (!_prepared)
      retCode = SQLExecDirect(_hstmt, (SQLWCHAR *)_preparedStatement.utf16(), _preparedStatement.length());
   else
      retCode = SQLExecute(_hstmt);

   // TODO: Check https://docs.microsoft.com/de-de/sql/odbc/reference/syntax/sqlmoreresults-function?view=sql-server-ver15

   SQLPOINTER token = nullptr;

   _executed = true;

   while (retCode == SQL_NEED_DATA)
   {
      retCode = SQLParamData(_hstmt, &token); // The token is the index of the parameter
      if (retCode == SQL_NEED_DATA)
      {
         auto ph = dynamic_cast<DB2PhInfo*>(phList[(int)(intptr_t)token]);

         ph->indicatorBuffer()[0] -= SQL_LEN_DATA_AT_EXEC_OFFSET;

         // Put in chunks
         SQLPutData(_hstmt, ph->valueBuffer(), ph->indicatorBuffer()[0]);
      }
   }

   // TODO: Start processing the streamed output parameters
   // See https://www.columbia.edu/sec/acis/db2/db2l0/db2l066.htm
   // See https://www.ibm.com/support/producthub/db2/docs/content/SSEPGG_11.5.0/com.ibm.db2.luw.apdv.cli.doc/doc/r0000604.html
   // See IBM\SQLLIB\samples\cli\dtlob.c

   dbState = checkerr(retCode, _hstmt, SQL_HANDLE_STMT, false);
   if (dbState.type() == QDbState::Error)
      return dbState.withSourceText(description());

   //
   // Configure output
   //

   // How many columns are in the result-set
   SQLSMALLINT columnCount = 0;

   dbState = checkerr(SQLNumResultCols(_hstmt, &columnCount), _hstmt, SQL_HANDLE_STMT, false);
   if (dbState.type() == QDbState::Error)
      return dbState;

   if (columnCount)
   {
      _resultList.resize(columnCount);

      bool hasUndersizedBuffer = false;

      // Fetch column schemas
      for (int i = 0; i < columnCount; ++i)
      {
         SQLWCHAR    columnName[64];
         SQLSMALLINT columnNameLength = 0;
         SQLSMALLINT columnType = 0;
         SQLULEN     columnSize = 0;
         SQLSMALLINT columnDigits = 0;
         SQLSMALLINT columnNullable = 0;

         dbState = checkerr(SQLDescribeCol(_hstmt, i + 1, columnName, sizeof(columnName) / sizeof(*columnName), &columnNameLength, &columnType, &columnSize, &columnDigits, &columnNullable), _hstmt, SQL_HANDLE_STMT, false);
         if (dbState.type() == QDbState::Error)
            return dbState;

         Q_ASSERT(columnSize > 0 && columnSize <= INT_MAX);

         QDbSchema::Column columnInfo;

         columnInfo.name = QString::fromUtf16((const char16_t *)columnName);
         columnInfo.type = QDB2Connection::dataTypeFromSqlType(columnType);
         columnInfo.vendorType = columnType;
         columnInfo.size = columnSize;
         columnInfo.scale = columnDigits;
         columnInfo.nullable = (columnNullable == SQL_NULLABLE);

         _resultList[i] = new DB2PhInfo(columnInfo, _connection->phType(), _connection.data());

         if (_resultList[i]->limitValueBufferSize())
            hasUndersizedBuffer = true;
      }

      // Set up the values to return fetch information into

      if (hasUndersizedBuffer)
         _rowStatusList.resize(1); // Use piecewise get instead of fetch to retrieve the result
      else
         _rowStatusList.resize(rowsToFetch);

      dbState = checkerr(SQLSetStmtAttr(_hstmt, SQL_ATTR_ROW_ARRAY_SIZE, (SQLPOINTER)(intptr_t)_rowStatusList.count(), 0), _hstmt, SQL_HANDLE_STMT, false);
      if (dbState.type() == QDbState::Error)
         return dbState;

      dbState = checkerr(SQLSetStmtAttr(_hstmt, SQL_ATTR_ROWS_FETCHED_PTR, &_rowsFetched, 0), _hstmt, SQL_HANDLE_STMT, false);
      if (dbState.type() == QDbState::Error)
         return dbState;
      dbState = checkerr(SQLSetStmtAttr(_hstmt, SQL_ATTR_ROW_STATUS_PTR, _rowStatusList.data(), 0), _hstmt, SQL_HANDLE_STMT, false);
      if (dbState.type() == QDbState::Error)
         return dbState;

      // Bind result buffers

      for (int i = 0; i < columnCount; ++i)
      {
         if (_rowStatusList.count() == 1)
         {
            // Use piecewise get instead of binding buffers
            if (_resultList[i]->targetType() == SQL_C_NUMERIC)
            {
               dbState = _resultList[i]->setNumericDescriptor(_hstmt, i + 1, false, false);
               if (dbState.type() == QDbState::Error)
                  return dbState;
            }
         }
         else
         {
            // Bind the results to buffers
            _resultList[i]->setRowCount(_rowStatusList.count());
            dbState = _resultList[i]->bindResult(_hstmt, i);
            if (dbState.type() == QDbState::Error)
               return dbState;
         }
      }

      _hasMore = true;
   }

   return dbState;
}

void QDB2CommandEngine::clear()
{
   QDbCommandEngine::clear();
   SQLFreeStmt(_hstmt, SQL_CLOSE);
   SQLFreeStmt(_hstmt, SQL_UNBIND);
   SQLFreeStmt(_hstmt, SQL_RESET_PARAMS);
   // _hstmt is deleted in the destructor
   _prepared = false;
   qDeleteAll(_resultList);
   _resultList.clear();
   _rowsFetched = 0;
   _rowStatusList.clear();
   _rowIndex = 0;
   _hasMore = false;
}

QDbState QDB2CommandEngine::prepareStatement(const QList<PhInfo*>& phList)
{
   // Are all placeholders defined?
   for (int i = 0; i < phList.count(); i++)
   {
      if (!phList[i]->isValid())
         return QDbState(QDbState::Error, tr("Placeholder at position %1 is not defined").arg(i + 1), _statement);
   }

   // Complete the statement if its a procedure call
   // This has to be done here because of the optional parameters!

   if (_functionSchema.isValid())
   {
      // Create a proper procedure escape sequence
      // This has to correspond to prepareBindings()

      if (!_preparedStatement.isEmpty())
      {
         // The statement has to be prepared again because the parameter may have changed
         _preparedStatement.clear();
         for (int i = 0; i < phList.count(); i++)
         {
            phList[i]->clearPositions();
         }
      }

      _preparedStatement = "call ";

      int phIndex = 0;

      // First add the return value
      if (_functionSchema.retVal.isValid())
      {
         phList[phIndex]->addPosition(phIndex + 1);
         _preparedStatement.append("? = ");
         phIndex++;
      }

      // Add the function identifier
      _preparedStatement.append(_functionSchema.schema);
      if (!_functionSchema.packageName.isEmpty())
      {
         _preparedStatement.append(".");
         _preparedStatement.append(_functionSchema.packageName);
      }
      _preparedStatement.append(".");
      _preparedStatement.append(_functionSchema.name);

      // Append the rest of the parameters
      if (phIndex < phList.count()) // Don't add parenthesis if there are no arguments at all
      {
         // Append the rest of the parameters
         _preparedStatement.append("(");
         for (; phIndex < phList.count(); ++phIndex)
         {
            phList[phIndex]->addPosition(phIndex + 1);
            _preparedStatement.append("?");
            if (phIndex + 1 < phList.count())
               _preparedStatement.append(", ");
         }
         _preparedStatement.append(")");
      }

      // Create a non SQL ODBC statement
      _preparedStatement.prepend("{");
      _preparedStatement.append("}");
   }
   else if (_preparedStatement.isEmpty())
   {
      // Statement without placeholder
      _preparedStatement = _statement;
   }

   return QDbState();
}
