#ifndef QDB2ENVIRONMENT_H
#define QDB2ENVIRONMENT_H

#include <QDbEnvironment>
#include <sqlcli.h>

QDbState checkerr(SQLRETURN status, SQLHANDLE handle, SQLSMALLINT type, bool throwOnError);

class QDB2Environment : public QDbEnvironment
{
public:
   QDB2Environment();
   ~QDB2Environment() override;

   SQLHENV& env() { return _env; }

public:
   static QDB2Environment* theEnvironment;
   static int environmentRefCount;

   static void attach();
   static void detach();

private:
   SQLHENV _env;
};

#endif // QDB2ENVIRONMENT_H
