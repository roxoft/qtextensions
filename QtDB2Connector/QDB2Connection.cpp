#include "QDB2Connection.h"
#include "QDB2Environment.h"
#include "QDB2CommandEngine.h"
#include <Variant.h>
#include <QRational.h>
#include <QDbConnection.h> // For SQL92_DATETIME_FORMAT

static const int maxNameLen = 255; // May only be needed by the remark, the other values can be 129

static void addToConnectionString(QString& connectionString, const QString& key, const QString& value)
{
   if (value.isEmpty())
      return;

   if (!connectionString.isEmpty())
      connectionString += u';';
   connectionString += key;
   connectionString += u'=';
   connectionString += value;
}

static int vendorTypeFromSqlType(int sqlType, int sqlSubType)
{
   if (sqlType != SQL_DATETIME)
   {
      return sqlType;
   }

   switch (sqlSubType)
   {
   case SQL_CODE_DATE:
      return SQL_TYPE_DATE;
   case SQL_CODE_TIME:
      return SQL_TYPE_TIME;
   case SQL_CODE_TIMESTAMP:
      return SQL_TYPE_TIMESTAMP;
   }

   return 0;
}

class QDB2Connection::Data
{
public:
   Data() = default;
   ~Data()
   {
      if (connected)
         SQLDisconnect(dbc); /* disconnect from driver */
      /* free up allocated handles */
      SQLFreeHandle(SQL_HANDLE_DBC, dbc);
   }

public:
   SQLHDBC dbc = 0;
   bool connected = false;
   QString driverName;
   bool transaction = false;
};

QString QDB2ConnectionParameters::toConnectionString() const
{
   QString connectString;

   // For connection parameters see https://www.connectionstrings.com/ibm-db2/
   // https://www.ibm.com/support/pages/cli-keywords-db2cliini-are-not-used-when-dbalias-used-connect
   addToConnectionString(connectString, "Driver", driver);
   addToConnectionString(connectString, "DSN", dbAlias);
   addToConnectionString(connectString, "Database", database);
   addToConnectionString(connectString, "Hostname", host);
   addToConnectionString(connectString, "Port", port);
   addToConnectionString(connectString, "Protocol", protocol);
   addToConnectionString(connectString, "Uid", userName);
   addToConnectionString(connectString, "Pwd", password);
   addToConnectionString(connectString, "CurrentSchema", schema);

   return connectString;
}

QDB2Connection::QDB2Connection(const QString& connectionString) : _connectString(connectionString), _data(new Data)
{
   QDB2Environment::attach();

   _dbState = QDB2Environment::theEnvironment->dbState();

   /* Allocate a connection handle */
   if (_dbState.type() != QDbState::Error)
      _dbState = checkerr(SQLAllocHandle(SQL_HANDLE_DBC, QDB2Environment::theEnvironment->env(), &_data->dbc), QDB2Environment::theEnvironment->env(), SQL_HANDLE_ENV, false);

   // Connect to the DSN
   SQLWCHAR outstr[1024];
   SQLSMALLINT outstrlen;

   if (_dbState.type() != QDbState::Error)
      _dbState = checkerr(SQLDriverConnect(_data->dbc, NULL, (SQLWCHAR *)_connectString.utf16(), SQL_NTS, outstr, sizeof(outstr) / sizeof(*outstr), &outstrlen, SQL_DRIVER_NOPROMPT), _data->dbc, SQL_HANDLE_DBC, false);

   if (_dbState.type() != QDbState::Error)
   {
      _data->connected = true;
      _connectString = QString::fromUtf16((const char16_t*)outstr);
   }
}

QDB2Connection::~QDB2Connection()
{
   delete _data;

   QDB2Environment::detach();
}

bool QDB2Connection::isValid() const
{
   return _data->connected;
}

QString QDB2Connection::connectionString() const
{
   return _connectString;
}

QString QDB2Connection::dataSourceName() const
{
   wchar_t     dsn[SQL_MAX_DSN_LENGTH + 1];
   SQLSMALLINT lengthIndicator;

   _dbState = checkerr(SQLGetInfo(_data->dbc, SQL_DATA_SOURCE_NAME, (SQLPOINTER)dsn, sizeof(dsn) / sizeof(*dsn), &lengthIndicator), _data->dbc, SQL_HANDLE_DBC, false);
   if (_dbState.type() != QDbState::Error)
      return QString::fromUtf16((const char16_t *)dsn);
   return QString();
}

QString QDB2Connection::driverName() const
{
   if (_data->driverName.isEmpty())
   {
      wchar_t     dsn[SQL_MAX_DSN_LENGTH + 1];
      SQLSMALLINT lengthIndicator;

      _dbState = checkerr(SQLGetInfo(_data->dbc, SQL_DRIVER_NAME, (SQLPOINTER)dsn, sizeof(dsn) / sizeof(*dsn), &lengthIndicator), _data->dbc, SQL_HANDLE_DBC, false);
      if (_dbState.type() != QDbState::Error)
         _data->driverName = QString::fromUtf16((const char16_t *)dsn);
   }

   return _data->driverName;
}

SqlStyle QDB2Connection::sqlStyle() const
{
   return SqlStyle::DB2;
}

QDbConnection::PhType QDB2Connection::phType() const
{
   return QDbConnection::PHT_UnnamedOnly;
}

QDbEnvironment *QDB2Connection::environment()
{
   return QDB2Environment::theEnvironment;
}

bool QDB2Connection::beginTransaction()
{
   if (!_data->transaction)
   {
      _dbState = checkerr(SQLSetConnectAttr(_data->dbc, SQL_ATTR_AUTOCOMMIT, (SQLPOINTER) SQL_AUTOCOMMIT_OFF, (SQLINTEGER) 0), _data->dbc, SQL_HANDLE_DBC, false);
      _data->transaction = true;
      return true;
   }
   return false;
}

bool QDB2Connection::rollbackTransaction()
{
   if (_data->transaction)
   {
      _data->transaction = false;
      _dbState = checkerr(SQLEndTran(SQL_HANDLE_DBC, _data->dbc, SQL_ROLLBACK), _data->dbc, SQL_HANDLE_DBC, false); // May throw an exception
      if (_dbState.type() != QDbState::Error)
         _dbState = checkerr(SQLSetConnectAttr(_data->dbc, SQL_ATTR_AUTOCOMMIT, (SQLPOINTER) SQL_AUTOCOMMIT_ON, (SQLINTEGER) 0), _data->dbc, SQL_HANDLE_DBC, false);
   }
   return _dbState.type() != QDbState::Error;
}

bool QDB2Connection::commitTransaction()
{
   if (_data->transaction)
   {
      _data->transaction = false;
      _dbState = checkerr(SQLSetConnectAttr(_data->dbc, SQL_ATTR_AUTOCOMMIT, (SQLPOINTER) SQL_AUTOCOMMIT_ON, (SQLINTEGER) 0), _data->dbc, SQL_HANDLE_DBC, false);
   }
   return _dbState.type() != QDbState::Error;
}

bool QDB2Connection::abortExecution()
{
   return false;
}

bool QDB2Connection::tableExists(const QString &tableName)
{
   return tableSchema(tableName).isValid();
}

QChar QDB2Connection::phIndicator() const
{
   return u':';
}

QString QDB2Connection::toBooleanLiteral(bool value) const
{
   return value ? "1" : "0";
}

QString QDB2Connection::toDateLiteral(const QDateEx& value) const
{
   return QString("DATE '%1'").arg(value.toString(SQL92_DATE_FORMAT));
}

QString QDB2Connection::toTimeLiteral(const QTimeEx& value) const
{
   return QString("TIME '%1'").arg(value.toString(SQL92_TIME_FORMAT));
}

QString QDB2Connection::toDateTimeLiteral(const QDateTimeEx& value) const
{
   return QString("TIMESTAMP '%1'").arg(value.toString(SQL92_DATETIME_FORMAT));
}

QString QDB2Connection::toStringLiteral(QString value) const
{
   QStringList result;
   auto pos = 0;

   for (auto i = 0; i < value.length(); ++i)
   {
      if (value.at(i) == u'\r')
      {
         if (pos < i)
            result += QDbConnectionData::toStringLiteral(value.mid(pos, i - pos));
         pos = i + 1;
         result += "CHR(13)";
      }
      else if (value.at(i) == u'\n')
      {
         if (pos < i)
            result += QDbConnectionData::toStringLiteral(value.mid(pos, i - pos));
         pos = i + 1;
         result += "CHR(10)";
      }
   }
   if (pos < value.length())
      result += QDbConnectionData::toStringLiteral(value.mid(pos));

   return result.join("||");
}

QString QDB2Connection::toBinaryLiteral(const QByteArray& value) const
{
   return QString("BX'%1'").arg(QString::fromLatin1(value.toHex()));
}

QDbSchema::Database QDB2Connection::databaseSchema()
{
   QDbSchema::Database dbInfo;

   SQLHSTMT stmt;
   SQLRETURN ret; /* DB2 API return status */
   SQLSMALLINT columns; /* number of columns in result-set */

   /* Allocate a statement handle */
   SQLAllocHandle(SQL_HANDLE_STMT, _data->dbc, &stmt);
   /* Retrieve a list of tables */
   SQLTables(stmt, NULL, 0, NULL, 0, NULL, 0, (SQLWCHAR*)L"TABLE", SQL_NTS);
   /* How many columns are there */
   SQLNumResultCols(stmt, &columns);
   /* Loop through the rows in the result-set */
   while (SQL_SUCCEEDED(ret = SQLFetch(stmt)))
   {
      QDbSchema::Table tableInfo;

      SQLUSMALLINT i;

      /* Loop through the columns */
      for (i = 1; i <= columns; i++)
      {
         SQLLEN   indicator;
         wchar_t  buf[512];
         /* retrieve column data as a string */
         ret = SQLGetData(stmt, i, SQL_C_WCHAR, buf, sizeof(buf) / sizeof(*buf), &indicator);
         if (SQL_SUCCEEDED(ret))
         {
            /* Handle null columns */
            if (indicator == SQL_NULL_DATA)
               *buf = L'\0';
            if (i == 2)
               tableInfo.schema = QString::fromUtf16((const char16_t *)buf);
            if (i == 3)
               tableInfo.name = QString::fromUtf16((const char16_t *)buf);
         }
      }

      dbInfo.tables.insert(tableInfo.name, tableInfo);
   }
   // free up allocated handles
   SQLFreeHandle(SQL_HANDLE_STMT, stmt);

   return dbInfo;
}

QDbSchema::Synonym QDB2Connection::synonymSchema(const QString &name)
{
   QDbSchema::Synonym synonymInfo;

   return synonymInfo;
}

QDbSchema::Table QDB2Connection::tableSchema(const QString &name)
{
   QDbSchema::Table tableInfo;

   tableInfo.name = name;
   tableInfo.columns = columns(name, nullptr, nullptr);
   if (!tableInfo.columns.isEmpty())
      tableInfo.schema = tableInfo.columns.first().schema;

   return tableInfo;
}

QDbSchema::Function QDB2Connection::functionSchema(const QString &name)
{
   QDbSchema::Function funcInfo;

   SQLHSTMT stmt = NULL;

   try
   {
      SQLCHAR     szCatalog[maxNameLen];
      SQLCHAR     szSchema[maxNameLen];
      SQLCHAR     szProcedureName[maxNameLen];
      SQLCHAR     szRemark[maxNameLen];
      SQLSMALLINT procedureType;

      SQLLEN cbCatalog;
      SQLLEN cbSchema;
      SQLLEN cbProcedureName;
      SQLLEN cbRemark;
      SQLLEN cbProcedureType;

      /* Allocate a statement handle */
      checkerr(SQLAllocHandle(SQL_HANDLE_STMT, _data->dbc, &stmt), _data->dbc, SQL_HANDLE_DBC, true);

      // Retrieve a list of procedures
      checkerr(SQLProcedures(stmt, NULL, 0, NULL, 0, (SQLWCHAR*)name.utf16(), name.length()), stmt, SQL_HANDLE_STMT, true);

      checkerr(SQLBindCol(stmt, 1, SQL_C_CHAR, szCatalog, maxNameLen, &cbCatalog), stmt, SQL_HANDLE_STMT, true);
      checkerr(SQLBindCol(stmt, 2, SQL_C_CHAR, szSchema, maxNameLen, &cbSchema), stmt, SQL_HANDLE_STMT, true);
      checkerr(SQLBindCol(stmt, 3, SQL_C_CHAR, szProcedureName, maxNameLen,&cbProcedureName), stmt, SQL_HANDLE_STMT, true);
      checkerr(SQLBindCol(stmt, 7, SQL_C_CHAR, szRemark, maxNameLen,&cbRemark), stmt, SQL_HANDLE_STMT, true);
      checkerr(SQLBindCol(stmt, 8, SQL_C_SSHORT, &procedureType, 0,&cbProcedureType), stmt, SQL_HANDLE_STMT, true);

      /* Fetch the data */
      SQLRETURN ret;

      if (!SQL_SUCCEEDED(ret = SQLFetch(stmt)))
      {
         if (ret == SQL_NO_DATA)
            throw QDbState(); // Procedure not found
         throw checkerr(ret, stmt, SQL_HANDLE_STMT, false);
      }

      if (cbSchema != SQL_NULL_DATA)
         funcInfo.schema = QString::fromLocal8Bit((const char *)szSchema);
      if (cbProcedureName != SQL_NULL_DATA)
         funcInfo.name = QString::fromLocal8Bit((const char *)szProcedureName);

      // free up allocated handles
      SQLFreeHandle(SQL_HANDLE_STMT, stmt);
      stmt = NULL;

      funcInfo.arguments = columns(funcInfo.name, &funcInfo.retVal, &funcInfo.resultSet);
   }
   catch (const QDbState &dbState)
   {
      // free up allocated handles
      if (stmt)
         SQLFreeHandle(SQL_HANDLE_STMT, stmt);
      _dbState = dbState; // May throw an exception
   }

   return funcInfo;
}

QDbSchema::Package QDB2Connection::packageSchema(const QString &name)
{
   QDbSchema::Package packageInfo;

   return packageInfo;
}

QDbDataType QDB2Connection::dataTypeFromSqlType(int sqlType)
{
   switch (sqlType)
   {
   case SQL_CHAR:
   case SQL_WCHAR:
   case SQL_VARCHAR:
   case SQL_WVARCHAR:
   case SQL_GRAPHIC:
   case SQL_VARGRAPHIC:
      return DT_STRING;
   case SQL_LONGVARCHAR:
   case SQL_WLONGVARCHAR:
   case SQL_LONGVARGRAPHIC:
   case SQL_CLOB:
   case SQL_DBCLOB:
   case SQL_CLOB_LOCATOR: // Not supported by the engine!
   case SQL_DBCLOB_LOCATOR: // Not supported by the engine!
      return DT_LARGE_STRING;
   case SQL_DECIMAL:
   case SQL_NUMERIC:
      return DT_DECIMAL;
   case SQL_BIT:
      return DT_BOOL;
   case SQL_TINYINT:
   case SQL_SMALLINT:
   case SQL_INTEGER:
      return DT_INT;
   case SQL_BIGINT:
      return DT_LONG;
   case SQL_REAL:
   case SQL_FLOAT:
   case SQL_DOUBLE:
      return DT_DOUBLE;
   case SQL_BINARY:
   case SQL_VARBINARY:
      return DT_BINARY;
   case SQL_LONGVARBINARY:
   case SQL_BLOB:
   case SQL_BLOB_LOCATOR: // Not supported by the engine!
      return DT_LARGE_BINARY;
   case SQL_TYPE_DATE:
      return DT_DATE;
   case SQL_TYPE_TIME:
      return DT_TIME;
   case SQL_TYPE_TIMESTAMP:
      return DT_DATETIME;
   }

   return DT_UNKNOWN;
}

QDbCommandEngine *QDB2Connection::newEngine()
{
   /* Allocate a statement handle */
   SQLHSTMT stmt = 0;

   _dbState = checkerr(SQLAllocHandle(SQL_HANDLE_STMT, _data->dbc, &stmt), _data->dbc, SQL_HANDLE_DBC, false);

   return new QDB2CommandEngine(this, stmt);
}

QList<QDbSchema::Column> QDB2Connection::columns(const QString &name, QDbSchema::Column *returnColumn, QList<QDbSchema::Column> *resultSet)
{
   QList<QDbSchema::Column> columnList;

   SQLHSTMT stmt = NULL;

   try
   {
      SQLCHAR szSchema[maxNameLen];
      SQLCHAR szCatalog[maxNameLen];
      SQLCHAR szColumnName[maxNameLen];
      SQLCHAR szTableName[maxNameLen];
      SQLCHAR szTypeName[maxNameLen];
      SQLCHAR szRemarks[maxNameLen];
      SQLCHAR szColumnDefault[maxNameLen];
      SQLCHAR szIsNullable[maxNameLen];

      SQLINTEGER ColumnSize;
      SQLINTEGER BufferLength;
      SQLINTEGER CharOctetLength;
      SQLINTEGER OrdinalPosition;

      SQLSMALLINT ColumnType;
      SQLSMALLINT DataType;
      SQLSMALLINT DecimalDigits;
      SQLSMALLINT NumPrecRadix;
      SQLSMALLINT Nullable;
      SQLSMALLINT SQLDataType;
      SQLSMALLINT DatetimeSubtypeCode;

      // Declare buffers for bytes available to return
      SQLLEN cbCatalog;
      SQLLEN cbSchema;
      SQLLEN cbTableName;
      SQLLEN cbColumnName;
      SQLLEN cbColumnType;
      SQLLEN cbDataType;
      SQLLEN cbTypeName;
      SQLLEN cbColumnSize;
      SQLLEN cbBufferLength;
      SQLLEN cbDecimalDigits;
      SQLLEN cbNumPrecRadix;
      SQLLEN cbNullable;
      SQLLEN cbRemarks;
      SQLLEN cbColumnDefault;
      SQLLEN cbSQLDataType;
      SQLLEN cbDatetimeSubtypeCode;
      SQLLEN cbCharOctetLength;
      SQLLEN cbOrdinalPosition;
      SQLLEN cbIsNullable;

      /* Allocate a statement handle */
      checkerr(SQLAllocHandle(SQL_HANDLE_STMT, _data->dbc, &stmt), _data->dbc, SQL_HANDLE_DBC, true);

      /* Retrieve a list of columns */
      if (returnColumn)
         checkerr(SQLProcedureColumns(stmt, NULL, 0, NULL, 0, (SQLWCHAR *)name.utf16(), SQL_NTS, NULL, 0), stmt, SQL_HANDLE_STMT, true);
      else
         checkerr(SQLColumns(stmt, NULL, 0, NULL, 0, (SQLWCHAR *)name.utf16(), SQL_NTS, NULL, 0), stmt, SQL_HANDLE_STMT, true);

      int col = 0;

      checkerr(SQLBindCol(stmt, ++col, SQL_C_CHAR, szCatalog, maxNameLen, &cbCatalog), stmt, SQL_HANDLE_STMT, true);
      checkerr(SQLBindCol(stmt, ++col, SQL_C_CHAR, szSchema, maxNameLen, &cbSchema), stmt, SQL_HANDLE_STMT, true);
      checkerr(SQLBindCol(stmt, ++col, SQL_C_CHAR, szTableName, maxNameLen,&cbTableName), stmt, SQL_HANDLE_STMT, true);
      checkerr(SQLBindCol(stmt, ++col, SQL_C_CHAR, szColumnName, maxNameLen, &cbColumnName), stmt, SQL_HANDLE_STMT, true);
      if (returnColumn)
         checkerr(SQLBindCol(stmt, ++col, SQL_C_SSHORT, &ColumnType, 0, &cbColumnType), stmt, SQL_HANDLE_STMT, true);
      checkerr(SQLBindCol(stmt, ++col, SQL_C_SSHORT, &DataType, 0, &cbDataType), stmt, SQL_HANDLE_STMT, true);
      checkerr(SQLBindCol(stmt, ++col, SQL_C_CHAR, szTypeName, maxNameLen, &cbTypeName), stmt, SQL_HANDLE_STMT, true);
      checkerr(SQLBindCol(stmt, ++col, SQL_C_LONG, &ColumnSize, 0, &cbColumnSize), stmt, SQL_HANDLE_STMT, true);
      checkerr(SQLBindCol(stmt, ++col, SQL_C_LONG, &BufferLength, 0, &cbBufferLength), stmt, SQL_HANDLE_STMT, true);
      checkerr(SQLBindCol(stmt, ++col, SQL_C_SSHORT, &DecimalDigits, 0, &cbDecimalDigits), stmt, SQL_HANDLE_STMT, true);
      checkerr(SQLBindCol(stmt, ++col, SQL_C_SSHORT, &NumPrecRadix, 0, &cbNumPrecRadix), stmt, SQL_HANDLE_STMT, true);
      checkerr(SQLBindCol(stmt, ++col, SQL_C_SSHORT, &Nullable, 0, &cbNullable), stmt, SQL_HANDLE_STMT, true);
      checkerr(SQLBindCol(stmt, ++col, SQL_C_CHAR, szRemarks, maxNameLen, &cbRemarks), stmt, SQL_HANDLE_STMT, true);
      checkerr(SQLBindCol(stmt, ++col, SQL_C_CHAR, szColumnDefault, maxNameLen, &cbColumnDefault), stmt, SQL_HANDLE_STMT, true);
      checkerr(SQLBindCol(stmt, ++col, SQL_C_SSHORT, &SQLDataType, 0, &cbSQLDataType), stmt, SQL_HANDLE_STMT, true);
      checkerr(SQLBindCol(stmt, ++col, SQL_C_SSHORT, &DatetimeSubtypeCode, 0, &cbDatetimeSubtypeCode), stmt, SQL_HANDLE_STMT, true);
      checkerr(SQLBindCol(stmt, ++col, SQL_C_LONG, &CharOctetLength, 0, &cbCharOctetLength), stmt, SQL_HANDLE_STMT, true);
      checkerr(SQLBindCol(stmt, ++col, SQL_C_LONG, &OrdinalPosition, 0, &cbOrdinalPosition), stmt, SQL_HANDLE_STMT, true);
      checkerr(SQLBindCol(stmt, ++col, SQL_C_CHAR, szIsNullable, maxNameLen, &cbIsNullable), stmt, SQL_HANDLE_STMT, true);

      /* Fetch the data */
      SQLRETURN ret;

      while (SQL_SUCCEEDED(ret = SQLFetch(stmt)))
      {
         QDbSchema::Column columnInfo;
         bool              isReturnColumn = false;
         bool              isResultColumn = false;

         if (cbSchema != SQL_NULL_DATA)
            columnInfo.schema = QString::fromLocal8Bit((const char *)szSchema);
         if (cbColumnName != SQL_NULL_DATA)
            columnInfo.name = QString::fromLocal8Bit((const char *)szColumnName);
         if (returnColumn && cbColumnType != SQL_NULL_DATA)
         {
            switch (ColumnType)
            {
            case SQL_PARAM_INPUT:
               columnInfo.writable = true;
               break;
            case SQL_PARAM_INPUT_OUTPUT:
               columnInfo.readable = true;
               columnInfo.writable = true;
               break;
            case SQL_PARAM_OUTPUT:
               columnInfo.readable = true;
               break;
            case SQL_RETURN_VALUE:
               columnInfo.readable = true;
               isReturnColumn = true;
               break;
            case SQL_RESULT_COL:
               columnInfo.readable = true;
               isResultColumn = true;
               break;
            }
         }
         else
            columnInfo.readable = true;
         if (cbSQLDataType > 0)
            columnInfo.vendorType = vendorTypeFromSqlType(SQLDataType, DatetimeSubtypeCode);
         else if (cbDataType > 0)
            columnInfo.vendorType = vendorTypeFromSqlType(DataType, DatetimeSubtypeCode);
         columnInfo.type = dataTypeFromSqlType(columnInfo.vendorType);
         if (cbColumnSize != SQL_NULL_DATA)
            columnInfo.size = ColumnSize;
         if (cbDecimalDigits != SQL_NULL_DATA)
            columnInfo.scale = DecimalDigits;
         Q_ASSERT(cbNumPrecRadix == SQL_NULL_DATA || NumPrecRadix == 10);
         if (cbNullable != SQL_NULL_DATA)
            columnInfo.nullable = (Nullable == SQL_NULLABLE);
         if (cbColumnDefault != SQL_NULL_DATA)
            columnInfo.hasDefault = true; // (const char *)szColumnDefault

         if (columnInfo.vendorType == 0)
            throw QDbState(QDbState::Error, tr("Unknown SQL data type"), QDbState::Connection);

         if (isResultColumn)
            resultSet->append(columnInfo);
         else if (isReturnColumn)
            *returnColumn = columnInfo;
         else
            columnList.append(columnInfo);
      }

      if (ret != SQL_NO_DATA)
         checkerr(ret, stmt, SQL_HANDLE_STMT, true);

      // free up allocated handles
      SQLFreeHandle(SQL_HANDLE_STMT, stmt);
   }
   catch (const QDbState &dbState)
   {
      // free up allocated handles
      if (stmt)
         SQLFreeHandle(SQL_HANDLE_STMT, stmt);
      _dbState = dbState; // May throw an exception
   }

   return columnList;
}

extern "C" {
   void createDB2ConnectionString(QString& connectString, const QString& database, const QString& host, const QString& userName, const QString& password, const QString& schema)
   {
      QDB2ConnectionParameters parameters;

      parameters.database = database;
      parameters.host = host;
      parameters.userName = userName;
      parameters.password = password;
      parameters.schema = schema;

      connectString = parameters.toConnectionString();
   }

   void createDB2AliasConnectionString(QString& connectString, const QString& dbAlias, const QString& userName, const QString& password, const QString& schema)
   {
      QDB2ConnectionParameters parameters;

      parameters.dbAlias = dbAlias;
      parameters.userName = userName;
      parameters.password = password;
      parameters.schema = schema;

      connectString = parameters.toConnectionString();
   }

   QDbConnectionData* newDB2Connection(const QString& connectionString)
   {
      return new QDB2Connection(connectionString);
   }
}
