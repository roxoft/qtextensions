#ifndef QDB2COMMANDENGINE_H
#define QDB2COMMANDENGINE_H

#include <QDbCommandEngine>
#include "QDB2Connection.h"
#include "QDB2Environment.h"
#include <QVector>

// Implementation of QDbCommandEngine for DB2
class QDB2CommandEngine : public QDbCommandEngine
{
   Q_DISABLE_COPY(QDB2CommandEngine)
   Q_DECLARE_TR_FUNCTIONS(QDB2CommandEngine)

public:
   QDB2CommandEngine(QDB2Connection* connection, SQLHSTMT hstmt);
   virtual ~QDB2CommandEngine();

   QDbState rowCount(int &count) const override;

   bool atEnd() const override;

   QDbState next() override;

   QDbSchema::Table  resultSchema() const override;
   QDbState  columnValue(int index, Variant &value) const override;
   QDbState  columnValue(const QString &name, Variant &value) const override;

protected:
   class DB2PhInfo : public PhInfo
   {
   public:
      DB2PhInfo(const QDbSchema::Column& schema, QDbConnection::PhType phType, QDB2Connection* c);
      ~DB2PhInfo() override { if (_buffer) free(_buffer); }

      // PhInfo interface

      bool isValid() const override { return _schema.vendorType; }

      Variant value() const override { return value(0); }
      QDbState setValue(const Variant& value) override { return setValue(value, 0); }

      void setType(QDbDataType type) override;

      // ODBC specific

      int   rowCount() const { return _rowCount; }
      void  setRowCount(int rowCount) { _rowCount = rowCount; }

      QDbState bindResult(SQLHSTMT hstmt, int index);
      QDbState bindParameter(SQLHSTMT hstmt, int index);
      QDbState setNumericDescriptor(SQLHSTMT hstmt, int pos, bool asParameter, bool withBuffer);
      QDbState getValuePiecewise(SQLHSTMT hstmt, int index);

      Variant  value(int row) const;
      QDbState setValue(const Variant &value, int row);

      void*       valueBuffer(int row = 0);
      const void* valueBuffer(int row = 0) const;
      bool        limitValueBufferSize();

      SQLLEN*        indicatorBuffer();
      const SQLLEN*  indicatorBuffer() const;

      SQLSMALLINT targetType();
      SQLSMALLINT targetType() const { return _cType; }

   private:
      void updateValueBufferSize();
      void createBuffer();

   private:
      QDB2Connection* _connection = nullptr;

      int      _cType = SQL_UNKNOWN_TYPE;
      SQLLEN   _valueBufferSize = 0; // Length of the buffer for 1 item!
      int      _rowCount = 1;
      quint8*  _buffer = nullptr;
      int      _bufferSize = 0;

      const SQLLEN _valueBufferSizeLimit = 0x10000; // Limit the size to 64k
      const SQLLEN _defaultChunkSize = 0x1000;
   };

   QDbConnectionData* connection() override { return _connection.data(); }
   PhInfo* createPhInfo(const QDbSchema::Column& schema, QDbConnection::PhType phType) override;
   QDbState setPlaceholderType(PhInfo* phInfo, QDbDataType type) override;
   QDbState setPlaceholderValue(PhInfo* phInfo, const Variant& value) override;
   QDbState placeholderValue(const PhInfo* phInfo, Variant& value) const override;
   QDbState prepare(const QList<PhInfo*>& phList) override;
   QDbState exec(const QList<PhInfo*>& phList, int rowsToFetch) override; // if rowsToFetch == 1 piecewise get is used to retrieve the values
   void clear() override;

private:
   QDbState prepareStatement(const QList<PhInfo*>& phList);

private:
   bp<QDB2Connection> _connection;

   SQLHSTMT                _hstmt = NULL;
   bool                    _prepared = false;
   bool                    _executed = false;
   QVector<DB2PhInfo*>     _resultList;
   unsigned int            _rowsFetched = 0;
   QVector<SQLUSMALLINT>   _rowStatusList;
   int                     _rowIndex = 0;
   bool                    _hasMore = false;
};

#endif // QDB2COMMANDENGINE_H
