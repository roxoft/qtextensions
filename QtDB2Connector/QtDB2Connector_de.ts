<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE">
<context>
    <name>QDB2CommandEngine</name>
    <message>
        <location filename="QDB2CommandEngine.cpp" line="87"/>
        <source>Row error</source>
        <translation>Datensatzfehler</translation>
    </message>
    <message>
        <location filename="QDB2CommandEngine.cpp" line="109"/>
        <source>Invalid position of row counter</source>
        <translation>Ungültige Position für den Datensatzzähler</translation>
    </message>
    <message>
        <location filename="QDB2CommandEngine.cpp" line="111"/>
        <source>Index %1 not in result set</source>
        <translation>Der Index %1 ist nicht im Ergebnis</translation>
    </message>
    <message>
        <location filename="QDB2CommandEngine.cpp" line="131"/>
        <source>Column name %1 not found in result set</source>
        <translation>Der Spaltenname %1 ist nicht im Ergebnis</translation>
    </message>
    <message>
        <location filename="QDB2CommandEngine.cpp" line="989"/>
        <source>The data type of the placeholder at position %1 cannot be set after execution</source>
        <translation>Datentyp für den Platzhalter an Position %1 darf nach der Ausführung nicht gesetzt werden</translation>
    </message>
    <message>
        <location filename="QDB2CommandEngine.cpp" line="1264"/>
        <source>Placeholder at position %1 is not defined</source>
        <translation>Der Platzhalter an Position %1 ist nicht definiert</translation>
    </message>
</context>
<context>
    <name>QDB2Connection</name>
    <message>
        <location filename="QDB2Connection.cpp" line="601"/>
        <source>Unknown SQL data type</source>
        <translation>Unbekannter SQL Datentyp</translation>
    </message>
</context>
<context>
    <name>TestCases</name>
    <message>
        <location filename="TestCases.cpp" line="858"/>
        <source>Missing DB2 database credentials: Test skipped</source>
        <translation>DB2 Zugangsdaten fehlen: Tests werden ausgelassen</translation>
    </message>
</context>
</TS>
