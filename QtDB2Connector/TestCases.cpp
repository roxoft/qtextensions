#include "TestCases.h"
#include "QDB2Connection.h"
#include <QDbCommand.h>
#include <QDir>
#include <QThread>
#include <QSettingsFile.h>

class ThreadingTest : public QThread
{
public:
   ThreadingTest() = default;
   ~ThreadingTest() override = default;

   void run() override;

   inline void addError(const QString& message)
   {
      errorMessages.append(message);
   }

   inline void errorIfNot(bool predicate, const QString& message)
   {
      if (!predicate)
         errorMessages.append(message);
   }

   inline void errorIf(bool predicate, const QString& message)
   {
      if (predicate)
         errorMessages.append(message);
   }

   template<typename T1, typename T2>
   inline void errorIfNotEqualEx(const T1& left, const T2& right, const QString& message)
   {
      if (!(left == right))
         errorMessages.append(message.arg(toQString(left).left(200)).arg(toQString(right).left(200)));
   }

   template<typename T1, typename T2>
   inline void errorIfNotEqual(const T1& left, const T2& right, const QString& message)
   {
      if (!(left == right))
         errorMessages.append(message.arg(left).arg(right));
   }

public:
   QString dbConnectionString;
   QStringList errorMessages;
};

void ThreadingTest::run()
{
   QDbConnection connection = new QDB2Connection(dbConnectionString);

   T_ERROR_IF(connection.state().type() == QDbState::Error, connection.state().message());

   QDbCommand query(connection);

   query.setStatement("select test1, test2, test3, test4, test5, test6, test7, test8, test9 from cr_cli_test order by test1");

   T_ERROR_IF(!query.exec(), query.state().message());

   auto resultSet = query.result();

   T_IS_TRUE(resultSet.next());

   T_EQUALS_EX(resultSet.valueAt(QString("test1")), 42);
   T_EQUALS_EX(resultSet.valueAt(QString("test2")), QDecimal(9999999999L));
   T_EQUALS_EX(resultSet.valueAt(QString("test3")), QDecimal(100.99));
   T_EQUALS(resultSet.valueAt(QString("test4")).toString().replace("\r", "\\r").replace("\n", "\\n"), "\\Path\\Name\\nLine\\\\n<New> \\ä\\nö\\rü\\tß line\\");
   T_EQUALS(resultSet.valueAt(QString("test5")).toString().replace("\r", "\\r").replace("\n", "\\n"), "\\Path\\Name\\r\\nLine\\\\r\\n<New> \\ä\\nö\\rü\\tß line\\");
   T_EQUALS(resultSet.valueAt(QString("test6")).toString().replace("\r", "\\r").replace("\n", "\\n"), "\\Path\\Name\\nLine\\\\n<New> \\ä\\nö\\rü\\tß line\\");
   T_EQUALS(resultSet.valueAt(QString("test7")).toString().replace("\r", "\\r").replace("\n", "\\n"), "\\Path\\Name\\r\\nLine\\\\r\\n<New> \\ä\\nö\\rü\\tß line\\");
   T_EQUALS_EX(resultSet.valueAt(QString("test8")), QDateTimeEx(2015, 1, 11, 17, 30, 0, 0));
   T_IS_TRUE(resultSet.valueAt(QString("test9")) == QByteArray("Binärdaten"));

   T_ERROR_IF(query.state().type() == QDbState::Error, query.state().message());

   T_IS_TRUE(resultSet.next());

   T_EQUALS_EX(resultSet.valueAt(QString("test1")), 123456);
   T_EQUALS_EX(resultSet.valueAt(QString("test2")), QDecimal(9999999999L));
   T_EQUALS_EX(resultSet.valueAt(QString("test3")), QDecimal(100.99));
   T_IS_TRUE(resultSet.valueAt(QString("test4")) == QString(3000, QLatin1Char('z')));
   T_IS_TRUE(resultSet.valueAt(QString("test5")) == QString("ř").repeated(1500));
   T_IS_TRUE(resultSet.valueAt(QString("test6")) == QString(5000, QLatin1Char('u')));
   T_IS_TRUE(resultSet.valueAt(QString("test7")) == QString(5000, QLatin1Char('n')));
   T_EQUALS_EX(resultSet.valueAt(QString("test8")), QDateTimeEx(2015, 1, 11, 17, 30, 0, 0));
   T_IS_TRUE(resultSet.valueAt(QString("test9")) == QByteArray(5000, 'b'));

   T_ERROR_IF(query.state().type() == QDbState::Error, query.state().message());

   T_IS_TRUE(resultSet.next());

   T_EQUALS_EX(resultSet.valueAt(QString("test1")), 987654);
   T_EQUALS_EX(resultSet.valueAt(QString("test2")), QDecimal(1));
   T_EQUALS_EX(resultSet.valueAt(QString("test3")), QDecimal(12.5));
   T_IS_TRUE(resultSet.valueAt(QString("test4")) == QString(4000, QLatin1Char('a')));
   T_IS_TRUE(resultSet.valueAt(QString("test5")) == QString("ß").repeated(2000));
   T_IS_TRUE(resultSet.valueAt(QString("test6")) == QString(100000, QLatin1Char('f')));
   T_IS_TRUE(resultSet.valueAt(QString("test7")) == QString(100000, QLatin1Char('g')));
   T_EQUALS_EX(resultSet.valueAt(QString("test8")), QDateTimeEx(2018, 7, 12, 5, 15, 0, 0));
   T_IS_TRUE(resultSet.valueAt(QString("test9")) == QByteArray(100000, 'a'));

   T_ERROR_IF(query.state().type() == QDbState::Error, query.state().message());

   T_IS_TRUE(!resultSet.next());
}

void TestCases::stmtParsingTests()
{
   auto connection = createDB2Connection();

   if (!connection.isValid())
      return;

   QDbCommand command(connection);

   command.setStatement("update cr_cli_test set test7 = :ph02, test6 = ?, test4 = ?, test5 = :ph04");
   command.parsePlaceholders();
   T_EQUALS(command.preparedStatement(), "update cr_cli_test set test7 = ?, test6 = ?, test4 = ?, test5 = ?");

   command.setStatement("  update cr_cli_test set test7 = :2, test6 = :ph01, test4 = :ph03, test5 = :");
   command.parsePlaceholders();
   T_EQUALS(command.preparedStatement(), "update cr_cli_test set test7 = ?, test6 = ?, test4 = ?, test5 = :");

   command.setStatement("select gpd_inhalt \"qpd:inhalt\", gpd_bemerkung from ihs_gp_dokumente where gpd_id = :gpd_id   ");
   command.parsePlaceholders();
   T_EQUALS(command.preparedStatement(), "select gpd_inhalt \"qpd:inhalt\", gpd_bemerkung from ihs_gp_dokumente where gpd_id = ?");
}

void TestCases::primaryTests()
{
   const auto connection = createDB2Connection();

   if (!connection.isValid())
      return;

   QDbCommand command(connection);

   // CREATE_TEST_TABLE
   command.setStatement("create table cr_test ( id int not null, name varchar(100) not null, numeric_value numeric(10), char_bool_value char(1), bool_value boolean, date_value date, time_string varchar(8), time_value time, primary key (id) )");
   if (!command.exec())
      addError(command.state().message());

   T_IS_TRUE(!connection.databaseSchema().tables.isEmpty());

   // Fill test table

   command.setStatement("INSERT INTO CR_TEST (ID, NAME, NUMERIC_VALUE, CHAR_BOOL_VALUE, BOOL_VALUE, DATE_VALUE, TIME_STRING, TIME_VALUE) VALUES (:ph00, :ph01, :ph02, :ph03, :ph04, :ph05, :ph06, :ph07)");

   command.setPlaceholderValue("ph00", 1);
   command.setPlaceholderValue("ph01", QString("Müller"));
   command.setPlaceholderValue("ph02", INT_MAX);
   command.setPlaceholderValue("ph03", true);
   command.setPlaceholderValue("ph04", true);
   command.setPlaceholderValue("ph05", QDate(2014, 5, 12));
   command.setPlaceholderValue("ph06", QTime(12, 30, 0));
   command.setPlaceholderValue("ph07", QTime(12, 30, 0));
   T_ERROR_IF(!command.exec(), command.state().message());

   // Reset command
   command.setStatement("INSERT INTO CR_TEST (ID, NAME, NUMERIC_VALUE, CHAR_BOOL_VALUE, BOOL_VALUE, DATE_VALUE, TIME_STRING, TIME_VALUE) VALUES (:ph00, :ph01, :ph02, :ph03, :ph04, :ph05, :ph06, :ph07)");

   command.setPlaceholderValue("ph00", 2);
   command.setPlaceholderValue("ph01", QString("Ralph"));
   command.setPlaceholderValue("ph02", 987654);
   command.setPlaceholderValue("ph03", false);
   command.setPlaceholderValue("ph04", false);
   command.setPlaceholderValue("ph05", QDateEx(2016, 5, 2));
   command.setPlaceholderValue("ph06", QString("12:50:00"));
   command.setPlaceholderValue("ph07", QString("12:50:00"));
   T_ERROR_IF(!command.exec(), command.state().message());

   // Repeat insert
   command.setPlaceholderValue("ph00", 3);
   command.setPlaceholderValue("ph01", QString("Hubert"));
   command.setPlaceholderValue("ph02", 654321);
   command.setPlaceholderValue("ph03", 0);
   command.setPlaceholderValue("ph04", 0);
   command.setPlaceholderValue("ph05", QDateEx(2018, 6, 15));
   command.setPlaceholderValue("ph06", QString("08:12:00"));
   command.setPlaceholderValue("ph07", QString("08:12:00"));
   T_ERROR_IF(!command.exec(), command.state().message());

   command.setPlaceholderValue("ph00", 4);
   command.setPlaceholderValue("ph01", QString("John"));
   command.setPlaceholderValue("ph02", -1);
   command.setPlaceholderValue("ph03", 1);
   command.setPlaceholderValue("ph04", 1);
   command.setPlaceholderValue("ph05", QDateTimeEx(2018, 6, 30));
   command.setPlaceholderValue("ph06", QTimeEx(18, 45, 0));
   command.setPlaceholderValue("ph07", QTimeEx(18, 45, 0));
   T_ERROR_IF(!command.exec(), command.state().message());

   // Insert with literals
   command.setStatement(QString("INSERT INTO CR_TEST (ID, NAME, NUMERIC_VALUE, CHAR_BOOL_VALUE, BOOL_VALUE, DATE_VALUE, TIME_STRING, TIME_VALUE) VALUES (%1, %2, %3, %4, %5, %6, %7, %8)")
      .arg(connection.toLiteral(DT_INT, 6))
      .arg(connection.toLiteral(DT_STRING, "\\Path\\Name\nLine\\\n<New> \\ä\\nö\\rü\\tß line\\"))
      .arg(connection.toLiteral(DT_LONG, 1234567890))
      .arg(connection.toLiteral(DT_STRING, "N"))
      .arg(connection.toLiteral(DT_BOOL, true))
      .arg(connection.toLiteral(DT_DATE, QDateEx(2020, 1, 20)))
      .arg(connection.toLiteral(DT_STRING, "12:55:44"))
      .arg(connection.toLiteral(DT_TIME, QTimeEx(15, 32, 0)))
   );
   T_ERROR_IF(!command.exec(), command.state().message());

   command.setStatement(QString("INSERT INTO CR_TEST (ID, NAME, NUMERIC_VALUE, CHAR_BOOL_VALUE, BOOL_VALUE, DATE_VALUE, TIME_STRING, TIME_VALUE) VALUES (%1, %2, %3, %4, %5, %6, %7, %8)")
      .arg(connection.toLiteral(DT_INT, 7))
      .arg(connection.toLiteral(DT_STRING, "\\Path\\Name\r\nLine\\\r\n<New> \\ä\\nö\\rü\\tß line\\"))
      .arg(connection.toLiteral(DT_LONG, 1234567890))
      .arg(connection.toLiteral(DT_STRING, "N"))
      .arg(connection.toLiteral(DT_BOOL, true))
      .arg(connection.toLiteral(DT_DATE, QDateEx(2020, 1, 20)))
      .arg(connection.toLiteral(DT_STRING, "12:55:44"))
      .arg(connection.toLiteral(DT_TIME, QTimeEx(15, 32, 0)))
   );
   T_ERROR_IF(!command.exec(), command.state().message());

   // Update test table

   command.setStatement("UPDATE CR_TEST SET ID = :ph00, NAME = :ph01, NUMERIC_VALUE = :ph02, DATE_VALUE = :ph03, TIME_STRING = :ph04, TIME_VALUE = :ph05 WHERE ID = :ph06");
   command.setPlaceholderValue("ph00", 5);
   command.setPlaceholderValue("ph01", QString("Horst"));
   command.setPlaceholderValue("ph02", 3);
   command.setPlaceholderValue("ph03", QDate(2016, 6, 2));
   command.setPlaceholderValue("ph04", QString("12:00:15"));
   command.setPlaceholderValue("ph05", (const QDateTimeEx&)QTimeEx(12, 0, 15));
   command.setPlaceholderValue("ph06", 1);
   T_ERROR_IF(!command.exec(), command.state().message());
   T_EQUALS(command.affectedRows(), 1);

   // Read test table

   command.setStatement("SELECT name, numeric_value, char_bool_value, bool_value, date_value, time_string, time_value FROM cr_test ORDER BY id");
   T_ERROR_IF(!command.exec(), command.state().message());
   auto result = command.result();

   T_IS_TRUE(result.next());

   T_EQUALS("Ralph", result["name"].toString());
   T_EQUALS_EX(result["numeric_value"], QDecimal(987654));
   T_EQUALS_EX(result["char_bool_value"].toBool(), false);
   T_EQUALS_EX(result["bool_value"], false);
   T_EQUALS_EX(QDate(2016, 5, 2), result["date_value"].toDateEx(nullptr, QLocale::C));
   T_EQUALS(result["time_string"].toString(), "12:50:00");
   T_EQUALS_EX(result["time_value"].toTimeEx(nullptr, QLocale::C), QTimeEx(12, 50, 0));

   T_ERROR_IF(result.state().type() == QDbState::Error, result.state().message());

   T_IS_TRUE(result.next());

   T_EQUALS("Hubert", result["name"].toString());
   T_EQUALS_EX(result["numeric_value"], QDecimal(654321));
   T_EQUALS_EX(result["char_bool_value"].toBool(), false);
   T_EQUALS_EX(result["bool_value"], false);
   T_EQUALS_EX(QDate(2018, 6, 15), result["date_value"].toDate(nullptr, QLocale::C));
   T_EQUALS(result["time_string"].toString(), "08:12:00");
   T_EQUALS_EX(result["time_value"].toTimeEx(nullptr, QLocale::C), QTimeEx(8, 12, 0));

   T_ERROR_IF(result.state().type() == QDbState::Error, result.state().message());

   T_IS_TRUE(result.next());

   T_EQUALS("John", result["name"].toString());
   T_EQUALS_EX(result["numeric_value"], QDecimal(-1));
   T_EQUALS_EX(result["char_bool_value"].toBool(), true);
   T_EQUALS_EX(result["bool_value"], true);
   T_EQUALS_EX(QDate(2018, 6, 30), result["date_value"].toDate(nullptr, QLocale::C));
   T_EQUALS(result["time_string"].toString(), "18:45:00");
   T_EQUALS_EX(result["time_value"].toTimeEx(nullptr, QLocale::C), QTimeEx(18, 45, 0));

   T_ERROR_IF(result.state().type() == QDbState::Error, result.state().message());

   T_IS_TRUE(result.next());

   T_EQUALS("Horst", result["name"].toString());
   T_EQUALS_EX(result["numeric_value"], QDecimal(3));
   T_EQUALS_EX(result["char_bool_value"].toBool(), true);
   T_EQUALS_EX(result["bool_value"], true);
   T_EQUALS_EX(QDate(2016, 6, 2), result["date_value"].toDate(nullptr, QLocale::C));
   T_EQUALS(result["time_string"].toString(), "12:00:15");
   T_EQUALS_EX(result["time_value"].toTimeEx(nullptr, QLocale::C), QTimeEx(12, 0, 15));

   T_ERROR_IF(result.state().type() == QDbState::Error, result.state().message());

   T_IS_TRUE(result.next());

   T_EQUALS(result["name"].toString().replace("\r", "\\r").replace("\n", "\\n"), "\\Path\\Name\\nLine\\\\n<New> \\ä\\nö\\rü\\tß line\\");
   T_EQUALS_EX(result["numeric_value"], QDecimal(1234567890));
   T_EQUALS(result["char_bool_value"].toString(), "N");
   T_EQUALS_EX(result["bool_value"], true);
   T_EQUALS_EX(QDate(2020, 1, 20), result["date_value"].toDate(nullptr, QLocale::C));
   T_EQUALS(result["time_string"].toString(), "12:55:44");
   T_EQUALS_EX(result["time_value"].toTimeEx(nullptr, QLocale::C), QTimeEx(15, 32, 0));

   T_IS_TRUE(result.next());

   T_EQUALS(result["name"].toString().replace("\r", "\\r").replace("\n", "\\n"), "\\Path\\Name\\r\\nLine\\\\r\\n<New> \\ä\\nö\\rü\\tß line\\");
   T_EQUALS_EX(result["numeric_value"], QDecimal(1234567890));
   T_EQUALS(result["char_bool_value"].toString(), "N");
   T_EQUALS_EX(result["bool_value"], true);
   T_EQUALS_EX(QDate(2020, 1, 20), result["date_value"].toDate(nullptr, QLocale::C));
   T_EQUALS(result["time_string"].toString(), "12:55:44");
   T_EQUALS_EX(result["time_value"].toTimeEx(nullptr, QLocale::C), QTimeEx(15, 32, 0));

   T_ERROR_IF(result.state().type() == QDbState::Error, result.state().message());

   T_IS_TRUE(!result.next());

   // DESTROY_TEST_TABLE
   command.setStatement("drop table cr_test");
   T_ERROR_IF(!command.exec(), command.state().message());
}

void TestCases::lobTests()
{
   const auto connection = createDB2Connection();

   if (!connection.isValid())
      return;

   QDbCommand command(connection);

   // CREATE_TEST_TABLE
   command.setStatement("create table cr_test ( id integer not null, numeric_value decimal(10,2), large_string clob, large_data blob )");
   if (!command.exec())
      addError(command.state().message());

   // Fill test table

   command.setStatement("INSERT INTO CR_TEST (ID, numeric_value, large_string, large_data) VALUES (:ph01, :ph02, :ph03, :ph04)");

   command.setPlaceholderValue("ph01", 1);
   command.setPlaceholderValue("ph02", 7000.22);
   command.setPlaceholderValue("ph03", QString(10000, u'x'));
   command.setPlaceholderValue("ph04", QByteArray(10000, 'x'));
   T_ERROR_IF(!command.exec(), command.state().message());

   // Update test table

   command.setStatement("UPDATE CR_TEST SET ID = :ph01, numeric_value = :ph02, large_string = :ph03, large_data = :ph04 WHERE ID = :ph05");
   command.setPlaceholderValue("ph01", 2);
   command.setPlaceholderValue("ph02", 9876.54);
   command.setPlaceholderValue("ph03", QString(10000, u'u'));
   command.setPlaceholderValue("ph04", QByteArray(10000, 'u'));
   command.setPlaceholderValue("ph05", 1);
   T_ERROR_IF(!command.exec(), command.state().message());
   T_EQUALS(command.affectedRows(), 1);

   // Read test table

   command.setStatement("SELECT id, numeric_value, large_string, large_data FROM cr_test ORDER BY id");
   T_ERROR_IF(!command.exec(), command.state().message());
   auto result = command.result();

   T_IS_TRUE(result.next());

   T_EQUALS(result["id"].toInt(), 2);
   T_EQUALS(result["numeric_value"].toString(QLocale::C), "9876.54");
   T_EQUALS(result["large_string"].toString(), QString(10000, u'u'));
   T_IS_TRUE(result["large_data"].toByteArray() == QByteArray(10000, 'u'));

   T_ERROR_IF(result.state().type() == QDbState::Error, result.state().message());

   T_IS_TRUE(!result.next());

   // DESTROY_TEST_TABLE
   command.setStatement("drop table cr_test");
   T_ERROR_IF(!command.exec(), command.state().message());
}

void TestCases::storedProcedureTests()
{
   auto connection = createDB2Connection();

   if (!connection.isValid())
      return;

   QDbCommand query(connection);

   query.setStatement("create or replace procedure test_ce (IN pi VARCHAR(4000), OUT po varchar(8000)) BEGIN declare tmp varchar(8000); set tmp = pi || pi; set po = tmp; end;");
   if (!query.exec())
      addError(query.state().message());
   else
   {
      // Procedure call
      query.setProcedure("test_ce");

      query.setPlaceholderValue(1, QString(4000, QLatin1Char('x')));
      if (!query.exec())
         addError(query.state().message());
      else
         T_EQUALS(query.placeholderValue(2).toString(), QString(8000, QLatin1Char('x')));

      query.setPlaceholderValue(1, QString());
      query.setPlaceholderValue(2, Variant::null);
      if (!query.exec())
         addError(query.state().message());
      else
         T_EQUALS(query.placeholderValue(2).toString(), QString());

      // Drop procedure
      query.setStatement("drop procedure test_ce (VARCHAR(4000), varchar(8000))");
      T_ERROR_IF(!query.exec(), query.state().message());
   }

   query.setStatement("create or replace procedure test_ce(INOUT pio CLOB) BEGIN declare tmp CLOB; set tmp = pio || pio; set pio = tmp; END;");
   if (!query.exec())
      addError(query.state().message());
   else
   {
      // Procedure call
      query.setProcedure("test_ce");
      query.setPlaceholderValue(1, QString(5000, QLatin1Char('x')));
      if (!query.exec())
         addError(query.state().message());
      else
         T_EQUALS(query.placeholderValue(1).toString(), QString(10000, QLatin1Char('x')));

      // Drop procedure
      query.setStatement("drop procedure test_ce(CLOB)");
      T_ERROR_IF(!query.exec(), query.state().message());
   }

   query.setStatement(QString("create or replace procedure test_ce(INOUT pio NCLOB) begin declare tmp NCLOB; set tmp = pio || pio; set pio = tmp; END;"));
   if (!query.exec())
      addError(query.state().message());
   else
   {
      // Procedure call
      query.setProcedure(QString("test_ce"));
      query.setPlaceholderValue(1, QString(5000, QLatin1Char('x')));
      if (!query.exec())
         addError(query.state().message());
      else
         T_EQUALS(query.placeholderValue(1).toString(), QString(10000, QLatin1Char('x')));

      // Drop procedure
      query.setStatement(QString("drop procedure test_ce(NCLOB)"));
      T_ERROR_IF(!query.exec(), query.state().message());
   }

   query.setStatement(QString("create or replace procedure test_ce(IN pi blob, out po blob) begin declare tmp blob; set tmp = pi; set po = tmp; END;"));
   if (!query.exec())
      addError(query.state().message());
   else
   {
      // Procedure call
      query.setProcedure(QString("test_ce"));
      query.setPlaceholderValue(1, QByteArray(5000, 'x'));
      if (!query.exec())
         addError(query.state().message());
      else
         T_IS_TRUE(query.placeholderValue(2) == QByteArray(5000, 'x'));

      // Drop procedure
      query.setStatement(QString("drop procedure test_ce(blob, blob)"));
      T_ERROR_IF(!query.exec(), query.state().message());
   }

   query.setStatement(QString("create or replace procedure test_ce(in pi clob, out po clob) begin declare tmp clob; set tmp = pi || pi; set po = tmp; END;"));
   if (!query.exec())
      addError(query.state().message());
   else
   {
      // Procedure call
      query.setProcedure(QString("test_ce"));

      query.setPlaceholderValue(1, QString(4000, QLatin1Char('x')));
      if (!query.exec())
         addError(query.state().message());
      else
         T_EQUALS(query.placeholderValue(2).toString(), QString(8000, QLatin1Char('x')));

      query.setPlaceholderValue(1, QString(""));
      query.setPlaceholderValue(2, Variant::null);
      if (!query.exec())
         addError(query.state().message());
      else
         T_EQUALS(query.placeholderValue(2).toString(), QString(""));

      // Drop procedure
      query.setStatement(QString("drop procedure test_ce(clob, clob)"));
      T_ERROR_IF(!query.exec(), query.state().message());
   }

   query.setStatement(QString("create or replace procedure test_ce(in pi numeric(3,1), out po numeric(3,1)) begin declare tmp numeric(3,1); set tmp = pi * 2; set po = tmp; END;"));
   if (!query.exec())
      addError(query.state().message());
   else
   {
      // Procedure call
      query.setProcedure(QString("test_ce"));

      query.setPlaceholderValue(1, QDecimal(42.5));
      if (!query.exec())
         addError(query.state().message());
      else
         T_EQUALS_EX(query.placeholderValue(2).toDecimal(), QDecimal(85));

      query.setPlaceholderValue(1, QDecimal(0));
      query.setPlaceholderValue(2, Variant::null);
      if (!query.exec())
         addError(query.state().message());
      else
         T_EQUALS_EX(query.placeholderValue(2).toDecimal(), QDecimal(0));

      // Drop procedure
      query.setStatement(QString("drop procedure test_ce(numeric(3,1), numeric(3,1))"));
      T_ERROR_IF(!query.exec(), query.state().message());
   }

   query.setStatement(QString("create or replace procedure test_ce (out pi_Result decimal, in pi_In1 date, in pi_In2 date) begin declare tmp decimal(4); set tmp = DAYS(pi_In2) -DAYS(pi_In1); set pi_Result = tmp; END;"));
   if (!query.exec())
      addError(query.state().message());
   else
   {
      // Procedure call
      query.setProcedure(QString("test_ce"));

      query.setPlaceholderValue(2, QDate(2014, 1, 1));
      query.setPlaceholderValue(3, QDate(2014, 12, 31));
      if (!query.exec())
         addError(query.state().message());
      else
         T_EQUALS_EX(query.placeholderValue(1).toDecimal(), QDecimal(364));

      // Drop procedure
      query.setStatement(QString("drop procedure test_ce"));
      T_ERROR_IF(!query.exec(), query.state().message());
   }
}

void TestCases::triggerTests()
{
   auto connection = createDB2Connection();

   if (!connection.isValid())
      return;

   QDbCommand query(connection);

   query.setStatement("create table ce_test_trigger ( id numeric(10) not null, host_name varchar(512) not null )");
   T_ERROR_IF(!query.exec(), query.state().message());

   query.setStatement(R"(CREATE OR REPLACE TRIGGER TRG_INS_ce_test_trigger
BEFORE INSERT ON ce_test_trigger
REFERENCING NEW AS NEW
FOR EACH ROW
BEGIN
   SELECT CURRENT CLIENT_WRKSTNNAME INTO NEW.host_name FROM SYSIBM.SYSDUMMY1;
END;)");
   T_ERROR_IF(!query.exec(), query.state().message());

   query.setStatement("insert into ce_test_trigger (id) values (112)");
   T_ERROR_IF(!query.exec(), query.state().message());

   query.setStatement("select id, host_name from ce_test_trigger");
   T_ERROR_IF(!query.exec(), query.state().message());
   T_IS_TRUE(query.result().next());
   T_EQUALS(query.result().valueAt(0).toInt(), 112);
   T_IS_TRUE(!query.result().valueAt(1).toString().isEmpty());

   query.setStatement("select host_name from FINAL TABLE (insert into ce_test_trigger (id) values (112))");
   T_ERROR_IF(!query.exec(), query.state().message());
   T_IS_TRUE(query.result().next());
   T_IS_TRUE(!query.result().valueAt(0).toString().isEmpty());

   query.setStatement(QString("drop trigger TRG_INS_ce_test_trigger"));
   T_ERROR_IF(!query.exec(), query.state().message());

   query.setStatement(QString("drop table ce_test_trigger"));
   T_ERROR_IF(!query.exec(), query.state().message());
}

void TestCases::threadingTests()
{
   auto connection = createDB2Connection();

   if (!connection.isValid())
      return;

   QDbCommand query(connection);

#if 1 // CREATE_TEST_TABLE
   query.setStatement(QString(
      "create table cr_cli_test ( test1 integer, test2 decimal(10), test3 decimal(30, 10), test4 varchar(4000 byte), test5 vargraphic(2000), test6 clob, test7 dbclob, test8 timestamp, test9 blob )"));
   T_ERROR_IF(!query.exec(), query.state().message());
#endif

#if 1
   query.setStatement(QString("insert into cr_cli_test (test1, test2, test3, test4, test5, test6, test7, test8, test9) values (:ph1, :ph2, :ph3, :ph4, :ph5, :ph6, :ph7, :ph8, :ph9)"));

   query.setPlaceholderValue(QString("ph1"), 123456);
   query.setPlaceholderValue(QString("ph2"), QDecimal(9999999999L));
   query.setPlaceholderValue(QString("ph3"), QDecimal(100.99));
   query.setPlaceholderValue(QString("ph4"), QString(3000, QLatin1Char('z')));
   query.setPlaceholderValue(QString("ph5"), QString("ř").repeated(1500), DT_NSTRING);
   query.setPlaceholderValue(QString("ph6"), QString(5000, QLatin1Char('u')));
   query.setPlaceholderValue(QString("ph7"), QString(5000, QLatin1Char('n')));
   query.setPlaceholderValue(QString("ph8"), QDateTime(QDate(2015, 1, 11), QTime(17, 30)));
   query.setPlaceholderValue(QString("ph9"), QByteArray(5000, 'b'));

   T_ERROR_IF(!query.exec(), query.state().message());

   // Repeat insert

   query.setPlaceholderValue(QString("ph1"), 987654);
   query.setPlaceholderValue(QString("ph2"), QDecimal(1));
   query.setPlaceholderValue(QString("ph3"), QDecimal(12.5));
   query.setPlaceholderValue(QString("ph4"), QString(4000, QLatin1Char('a')));
   query.setPlaceholderValue(QString("ph5"), QString("ß").repeated(2000), DT_NSTRING);
   query.setPlaceholderValue(QString("ph6"), QString(100000, QLatin1Char('f')));
   query.setPlaceholderValue(QString("ph7"), QString(100000, QLatin1Char('g')));
   query.setPlaceholderValue(QString("ph8"), QDateTime(QDate(2018, 7, 12), QTime(5, 15)));
   query.setPlaceholderValue(QString("ph9"), QByteArray(100000, 'a'));

   T_ERROR_IF(!query.exec(), query.state().message());

   // Insert with literals

   query.setStatement(QString("insert into cr_cli_test (test1, test2, test3, test4, test5, test6, test7, test8, test9) values (%1, %2, %3, %4, %5, %6, %7, %8, %9)")
      .arg(connection.toLiteral(DT_INT, 42))
      .arg(connection.toLiteral(DT_DECIMAL, QDecimal(9999999999L)))
      .arg(connection.toLiteral(DT_DECIMAL, QDecimal(100.99)))
      .arg(connection.toLiteral(DT_STRING, "\\Path\\Name\nLine\\\n<New> \\ä\\nö\\rü\\tß line\\"))
      .arg(connection.toLiteral(DT_STRING, "\\Path\\Name\r\nLine\\\r\n<New> \\ä\\nö\\rü\\tß line\\"))
      .arg(connection.toLiteral(DT_STRING, "\\Path\\Name\nLine\\\n<New> \\ä\\nö\\rü\\tß line\\"))
      .arg(connection.toLiteral(DT_STRING, "\\Path\\Name\r\nLine\\\r\n<New> \\ä\\nö\\rü\\tß line\\"))
      .arg(connection.toLiteral(DT_DATETIME, QDateTime(QDate(2015, 1, 11), QTime(17, 30))))
      .arg(connection.toLiteral(DT_BINARY, QByteArray("Binärdaten")))
   );
   T_ERROR_IF(!query.exec(), query.state().message());
#endif

#if 1
   query.setStatement("select test1, test2, test3, test4, test5, test6, test7, test8, test9 from cr_cli_test order by test1");

   T_ERROR_IF(!query.exec(), query.state().message());

   auto resultSet = query.result();

   T_IS_TRUE(resultSet.next());

   T_EQUALS_EX(resultSet.valueAt(QString("test1")), 42);
   T_EQUALS_EX(resultSet.valueAt(QString("test2")), QDecimal(9999999999L));
   T_EQUALS_EX(resultSet.valueAt(QString("test3")), QDecimal(100.99));
   T_EQUALS(resultSet.valueAt(QString("test4")).toString().replace("\r", "\\r").replace("\n", "\\n"), "\\Path\\Name\\nLine\\\\n<New> \\ä\\nö\\rü\\tß line\\");
   T_EQUALS(resultSet.valueAt(QString("test5")).toString().replace("\r", "\\r").replace("\n", "\\n"), "\\Path\\Name\\r\\nLine\\\\r\\n<New> \\ä\\nö\\rü\\tß line\\");
   T_EQUALS(resultSet.valueAt(QString("test6")).toString().replace("\r", "\\r").replace("\n", "\\n"), "\\Path\\Name\\nLine\\\\n<New> \\ä\\nö\\rü\\tß line\\");
   T_EQUALS(resultSet.valueAt(QString("test7")).toString().replace("\r", "\\r").replace("\n", "\\n"), "\\Path\\Name\\r\\nLine\\\\r\\n<New> \\ä\\nö\\rü\\tß line\\");
   T_EQUALS_EX(resultSet.valueAt(QString("test8")), QDateTimeEx(2015, 1, 11, 17, 30, 0, 0));
   T_IS_TRUE(resultSet.valueAt(QString("test9")) == QByteArray("Binärdaten"));

   T_ERROR_IF(query.state().type() == QDbState::Error, query.state().message());

   T_IS_TRUE(resultSet.next());

   T_EQUALS_EX(resultSet.valueAt(QString("test1")), 123456);
   T_EQUALS_EX(resultSet.valueAt(QString("test2")), QDecimal(9999999999L));
   T_EQUALS_EX(resultSet.valueAt(QString("test3")), QDecimal(100.99));
   T_IS_TRUE(resultSet.valueAt(QString("test4")) == QString(3000, QLatin1Char('z')));
   T_IS_TRUE(resultSet.valueAt(QString("test5")) == QString("ř").repeated(1500));
   T_IS_TRUE(resultSet.valueAt(QString("test6")) == QString(5000, QLatin1Char('u')));
   T_IS_TRUE(resultSet.valueAt(QString("test7")) == QString(5000, QLatin1Char('n')));
   T_EQUALS_EX(resultSet.valueAt(QString("test8")), QDateTimeEx(2015, 1, 11, 17, 30, 0, 0));
   T_IS_TRUE(resultSet.valueAt(QString("test9")) == QByteArray(5000, 'b'));

   T_ERROR_IF(query.state().type() == QDbState::Error, query.state().message());

   T_IS_TRUE(resultSet.next());

   T_EQUALS_EX(resultSet.valueAt(QString("test1")), 987654);
   T_EQUALS_EX(resultSet.valueAt(QString("test2")), QDecimal(1));
   T_EQUALS_EX(resultSet.valueAt(QString("test3")), QDecimal(12.5));
   T_IS_TRUE(resultSet.valueAt(QString("test4")) == QString(4000, QLatin1Char('a')));
   T_IS_TRUE(resultSet.valueAt(QString("test5")) == QString("ß").repeated(2000));
   T_IS_TRUE(resultSet.valueAt(QString("test6")) == QString(100000, QLatin1Char('f')));
   T_IS_TRUE(resultSet.valueAt(QString("test7")) == QString(100000, QLatin1Char('g')));
   T_EQUALS_EX(resultSet.valueAt(QString("test8")), QDateTimeEx(2018, 7, 12, 5, 15, 0, 0));
   T_IS_TRUE(resultSet.valueAt(QString("test9")) == QByteArray(100000, 'a'));

   T_ERROR_IF(query.state().type() == QDbState::Error, query.state().message());

   T_IS_TRUE(!resultSet.next());
#endif

#if 1
   QList<ThreadingTest*> testThreads;

   for (auto r = 0; r < 2; ++r)
   {
      for (auto i = 0; i < 5; i++)
      {
         auto testThread = new ThreadingTest;

         testThread->dbConnectionString = connection.connectionString();
         testThreads.append(testThread);
         testThread->start();
      }

      for (auto&& testThread : testThreads)
      {
         testThread->wait();
         for (auto&& errorMsg : testThread->errorMessages)
            addError(errorMsg);
         delete testThread;
      }

      testThreads.clear();
   }
#endif

#if 1
   query.setStatement(QString("update cr_cli_test set test7 = :ph02, test6 = :ph01, test4 = :ph03, test5 = :ph04 where test1 = 123456"));
   query.setPlaceholderValue(QString("ph01"), eutf8("ä").repeated(3000)); // CLOB
   query.setPlaceholderValue(QString("ph02"), eutf8("Ü").repeated(3000), DT_NSTRING); // NCLOB
   query.setPlaceholderValue(QString("ph03"), QTime(12, 30, 0)); // VARCHAR2
   query.setPlaceholderValue(QString("ph04"), eutf8("ý").repeated(2000), DT_NSTRING); // NVARCHAR2

   T_ERROR_IF(!query.exec(), query.state().message());
   T_EQUALS(query.affectedRows(), 1);

   query.setStatement(QString("SELECT test4, test5, test6, test7 FROM cr_cli_test where test1 = 123456"));

   T_ERROR_IF(!query.exec(), query.state().message());

   QDbResultSet result = query.result();

   T_IS_TRUE(result.next());

   auto text = result[QString("test4")].toString();
   T_IS_TRUE(text == "12:30:00");
   T_ERROR_IF(result.state().type() == QDbState::Error, result.state().message());
   text = result[QString("test5")].toString();
   T_IS_TRUE(text == eutf8("ý").repeated(2000));
   T_ERROR_IF(result.state().type() == QDbState::Error, result.state().message());
   text = result[QString("test6")].toString();
   T_IS_TRUE(text == eutf8("ä").repeated(3000));
   T_ERROR_IF(result.state().type() == QDbState::Error, result.state().message());
   text = result[QString("test7")].toString();
   T_IS_TRUE(text == eutf8("Ü").repeated(3000));
   T_ERROR_IF(result.state().type() == QDbState::Error, result.state().message());
#endif

#if 1
   query.setStatement(QString("insert into cr_cli_test (test1, test2, test3, test4, test5, test6, test7, test8, test9) values (:ph1, :ph2, :ph3, :ph4, :ph5, NULL, NULL, :ph8, NULL)"));

   query.setPlaceholderValue(QString("ph1"), 789);
   query.setPlaceholderValue(QString("ph2"), QDecimal(9999999999L));
   query.setPlaceholderValue(QString("ph3"), QDecimal(100.99));
   query.setPlaceholderValue(QString("ph4"), QTimeEx(6, 12, 0));
   query.setPlaceholderValue(QString("ph5"), eutf8("í").repeated(2000), DT_NSTRING);
   query.setPlaceholderValue(QString("ph8"), QDateTime(QDate(2015, 01, 11), QTime(17, 30)));

   T_ERROR_IF(!query.exec(), query.state().message());

   query.setStatement(QString("select test1, test2, test3, test4, test5, test6, test7, test8, test9 from cr_cli_test where test1 = 789"));

   T_ERROR_IF(!query.exec(), query.state().message());

   resultSet = query.result();

   T_IS_TRUE(resultSet.next());

   T_EQUALS_EX(resultSet.valueAt(QString("test1")), 789);
   T_EQUALS_EX(resultSet.valueAt(QString("test2")), QDecimal(9999999999L));
   T_EQUALS_EX(resultSet.valueAt(QString("test3")), QDecimal(100.99));
   T_IS_TRUE(resultSet.valueAt(QString("test4")) == "06:12:00");
   T_IS_TRUE(resultSet.valueAt(QString("test5")) == eutf8("í").repeated(2000));
   T_IS_TRUE(resultSet.valueAt(QString("test6")).isNull());
   T_IS_TRUE(resultSet.valueAt(QString("test7")).isNull());
   T_EQUALS_EX(resultSet.valueAt(QString("test8")), QDateTimeEx(2015, 01, 11, 17, 30, 0, 0));
   T_IS_TRUE(resultSet.valueAt(QString("test9")).isNull());

   T_ERROR_IF(query.state().type() == QDbState::Error, query.state().message());

   T_IS_TRUE(!resultSet.next());
#endif

#if 1 // DESTROY_TEST_TABLE
   query.setStatement("drop table cr_cli_test");
   T_ERROR_IF(!query.exec(), query.state().message());
#endif
}

void TestCases::numericLimitTest()
{
   auto connection = createDB2Connection();

   if (!connection.isValid())
      return;

   QDbCommand command(connection);

   // Create test table

   command.setStatement("create table cr_test ( id int not null, numeric_value numeric(31, 16), primary key (id) )");
   if (!command.exec())
      addError(command.state().message());

   // Fill test table

   command.setStatement("INSERT INTO CR_TEST (ID, NUMERIC_VALUE) VALUES (:ph00, :ph01)");

   command.setPlaceholderValue("ph00", 1);
   command.setPlaceholderValue("ph01", QDecimal(42.5));
   T_ERROR_IF(!command.exec(), command.state().message());

   command.setPlaceholderValue("ph00", 2);
   command.setPlaceholderValue("ph01", QDecimal("123456789012.1234567890123456"));
   T_ERROR_IF(!command.exec(), command.state().message());

   command.setPlaceholderValue("ph00", 3);
   command.setPlaceholderValue("ph01", QDecimal("123456789012345.1234567890123456"));
   T_ERROR_IF(!command.exec(), command.state().message());

   // Read test table

   command.setStatement("SELECT numeric_value FROM cr_test ORDER BY id");
   T_ERROR_IF(!command.exec(), command.state().message());
   auto result = command.result();

   T_IS_TRUE(result.next());
   T_EQUALS_EX(result["numeric_value"].toDecimal(), 42.5);
   T_ERROR_IF(result.state().type() == QDbState::Error, result.state().message());

   T_IS_TRUE(result.next());
   T_EQUALS_EX(result["numeric_value"].toDecimal(), QDecimal("123456789012.1234567890123456"));
   T_ERROR_IF(result.state().type() == QDbState::Error, result.state().message());

   T_IS_TRUE(result.next());
   T_EQUALS_EX(result["numeric_value"].toDecimal(), QDecimal("123456789012345.1234567890123456"));
   T_ERROR_IF(result.state().type() == QDbState::Error, result.state().message());

   // Destroy test table
   command.setStatement("drop table cr_test");
   T_ERROR_IF(!command.exec(), command.state().message());
}

QDbConnection TestCases::createDB2Connection()
{
   QFile credentialsFile(QDir(dataDir()).filePath("DB2Credentials.ini"));
   const QSettingsFile settings(&credentialsFile);

   const auto dbAlias = settings.value("Alias").toString();
   const auto dbHost = settings.value("Host").toString();
   const auto dbName = settings.value("Database").toString();
   const auto dbUser = settings.value("User").toString();
   const auto dbPassword = settings.value("Password").toString();
   const auto dbSchema = settings.value("Schema").toString();

   if (dbAlias.isEmpty() && dbHost.isEmpty())
   {
      addWarning(tr("Missing DB2 database credentials: Test skipped"));
      return QDbConnection();
   }

   QString db2ConnectionString;
   if (dbAlias.isEmpty())
      createDB2ConnectionString(db2ConnectionString, dbName, dbHost, dbUser, dbPassword, dbSchema);
   else
      createDB2AliasConnectionString(db2ConnectionString, dbAlias, dbUser, dbPassword, dbSchema);

   const auto db2Connection = new QDB2Connection(db2ConnectionString);

   if (db2Connection->state().type() == QDbState::Error)
      throw TestException(db2Connection->state().message());

   //db2Connection->setServerCharacterEncoding(QTextCodec::codecForMib(2252));

   return db2Connection;
}

BEGIN_TEST_SUITES
   TEST_SUITE(TestCases);
END_TEST_SUITES
