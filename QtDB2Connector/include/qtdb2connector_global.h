#pragma once

#include <QtCore/qglobal.h>

#ifndef BUILD_STATIC
# if defined(QTDB2CONNECTOR_LIB)
#  define QTDB2CONNECTOR_EXPORT Q_DECL_EXPORT
# else
#  define QTDB2CONNECTOR_EXPORT Q_DECL_IMPORT
# endif
#else
# define QTDB2CONNECTOR_EXPORT
#endif
