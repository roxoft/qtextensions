#ifndef QDB2CONNECTION_H
#define QDB2CONNECTION_H

#include "qtdb2connector_global.h"
#include <QDbConnectionData>
#include <QDbSchema>
#include <QMap>

struct QTDB2CONNECTOR_EXPORT QDB2ConnectionParameters
{
   QString driver = "{IBM DB2 ODBC DRIVER}";
   QString dbAlias;
   QString host;
   QString database;
   QString port;
   QString protocol = "TCPIP";
   QString userName;
   QString password;
   QString schema;

   QString toConnectionString() const;
};

// Implementation for QDbConnection for DB2.
//
// NUMERIC (DECIMAL) restriction:
// The precision for NUMERIC (DECIMAL) values is 31 decimal digits (see https://www.ibm.com/support/knowledgecenter/SSEPGG_11.5.0/com.ibm.db2.luw.sql.ref.doc/doc/r0008469.html).
// The reason is, that DB2 reserves only 16 bytes in the result set when fetching NUMERIC values. See QDB2CommandEngine::DB2PhInfo::bindResult().
// As the size of the numeric structure in a result set is only 16 bytes, it leaves 13 bytes for the mantissa which would result in a precision of 31 decimal digits.
class QTDB2CONNECTOR_EXPORT QDB2Connection : public QDbConnectionData
{
   Q_DISABLE_COPY(QDB2Connection)
   Q_DECLARE_TR_FUNCTIONS(QDB2Connection)

public:
   QDB2Connection(const QString& connectionString);
   ~QDB2Connection() override;

   bool isValid() const override;

   QString  connectionString() const override;
   QString  dataSourceName() const override;
   QString  driverName() const override;
   SqlStyle sqlStyle() const override;
   QDbConnection::PhType phType() const override;

   QDbEnvironment *environment() override;

   bool beginTransaction() override;
   bool rollbackTransaction() override;
   bool commitTransaction() override;

   bool abortExecution() override;

   bool tableExists(const QString &tableName) override;

   QChar phIndicator() const override;

   QString toBooleanLiteral(bool value) const override;
   QString toDateLiteral(const QDateEx &value) const override;
   QString toTimeLiteral(const QTimeEx &value) const override;
   QString toDateTimeLiteral(const QDateTimeEx &value) const override;
   QString toStringLiteral(QString value) const override;
   QString toBinaryLiteral(const QByteArray &value) const override;

   QDbSchema::Database   databaseSchema() override;
   QDbSchema::Synonym    synonymSchema(const QString &name) override;
   QDbSchema::Table      tableSchema(const QString &name) override;
   QDbSchema::Function   functionSchema(const QString &name) override;
   QDbSchema::Package    packageSchema(const QString &name) override;

public:
   static QDbDataType dataTypeFromSqlType(int sqlType);

protected:
   QDbCommandEngine *newEngine() override;

private:
   class Data;

private:
   QList<QDbSchema::Column> columns(const QString &name, QDbSchema::Column *returnColumn, QList<QDbSchema::Column> *resultSet);

private:
   QString _connectString;
   Data* _data;
};

extern "C" {
   QTDB2CONNECTOR_EXPORT void createDB2ConnectionString(QString& connectString, const QString& database, const QString& host, const QString& userName, const QString& password, const QString& schema);
   QTDB2CONNECTOR_EXPORT void createDB2AliasConnectionString(QString& connectString, const QString& dbAlias, const QString& userName, const QString& password, const QString& schema);
   QTDB2CONNECTOR_EXPORT QDbConnectionData* newDB2Connection(const QString& connectionString);
}

#endif // QDB2CONNECTION_H
