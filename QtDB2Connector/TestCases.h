#ifndef TESTCASES_H
#define TESTCASES_H

#include <QLocaleEx.h> // Must be included before TestFramework to be able to resolve toQString(bool)
#include <TestFramework>
#include <QDbConnection.h>

class TestCases : public TestFramework
{
   Q_OBJECT

public:
   TestCases(QObject *parent = 0) : TestFramework(parent) {}
   ~TestCases() override = default;

public slots:
   void stmtParsingTests();
   void primaryTests();
   void lobTests();
   void storedProcedureTests();
   void triggerTests();
   void threadingTests();
   void numericLimitTest();

private:
   void _primaryTest(const QDbConnection& connection);

   QDbConnection createDB2Connection();
};

#endif // TESTCASES_H
