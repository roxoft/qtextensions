HEADERS += $$PWD/include/QDB2Connection.h \
    $$PWD/include/qtdb2connector_global.h \
    $$PWD/QDB2Environment.h \
    $$PWD/QDB2CommandEngine.h \
    $$PWD/TestCases.h
SOURCES += $$PWD/QDB2Connection.cpp \
    $$PWD/QDB2Environment.cpp \
    $$PWD/QDB2CommandEngine.cpp \
    $$PWD/TestCases.cpp
