TestRunner
==========

User interface to execute Unit Tests:

{TestRunner.png}

Uses `TestDialog` from `QtGuiEx`.
