TEMPLATE = app
TARGET = QtTestRunner
unix {
    VERSION = 1.1.0
}
QT += core gui widgets

include(TestRunner.pri)
win32:RC_FILE = TestRunner.rc

contains(QT_ARCH, i386) {
    CONFIG(release, debug|release): INSTALL_LIB_PATH = $$quote($$system_path($$_PRO_FILE_PWD_/../x86/Release))
    else:CONFIG(debug, debug|release): INSTALL_LIB_PATH = $$quote($$system_path($$_PRO_FILE_PWD_/../x86/Debug))
} else {
    CONFIG(release, debug|release): INSTALL_LIB_PATH = $$quote($$system_path($$_PRO_FILE_PWD_/../x64/Release))
    else:CONFIG(debug, debug|release): INSTALL_LIB_PATH = $$quote($$system_path($$_PRO_FILE_PWD_/../x64/Debug))
}

LIBS += -L$${INSTALL_LIB_PATH}

LIBS += -lQtCoreEx

INCLUDEPATH += $$quote($$system_path($$_PRO_FILE_PWD_/../include/QtCoreEx))
DEPENDPATH += $$quote($$system_path($$_PRO_FILE_PWD_/../include/QtCoreEx))

LIBS += -lQtGuiEx

INCLUDEPATH += $$quote($$system_path($$_PRO_FILE_PWD_/../include/QtGuiEx))
DEPENDPATH += $$quote($$system_path($$_PRO_FILE_PWD_/../include/QtGuiEx))

INSTALL_HEADER_PATH = $$quote($$system_path($$_PRO_FILE_PWD_/../include/$${TARGET}))

win32 {
    CONFIG(release, debug|release): TARGET_PATH = $$quote($$system_path($$OUT_PWD/release/$${TARGET}))
    else:CONFIG(debug, debug|release): TARGET_PATH = $$quote($$system_path($$OUT_PWD/debug/$${TARGET}))

    QMAKE_POST_LINK += (if not exist $${INSTALL_LIB_PATH} $(MKDIR) $${INSTALL_LIB_PATH})
    QMAKE_POST_LINK += && $(COPY) $${TARGET_PATH}.exe $${INSTALL_LIB_PATH}
    CONFIG(debug, debug|release): QMAKE_POST_LINK += && $(COPY) $${TARGET_PATH}.pdb $${INSTALL_LIB_PATH}
} else: unix {
    QMAKE_POST_LINK += $(MKDIR) $${INSTALL_LIB_PATH}
    QMAKE_POST_LINK += && $(COPY) -d $$OUT_PWD/$${TARGET} $${INSTALL_LIB_PATH}
}
