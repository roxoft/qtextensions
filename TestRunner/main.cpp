#include <TestDialog.h>
#include <QCommandLineParser>
#include <QApplication>
#include <BlueGradientProxyStyle>
#ifdef Q_OS_WIN
#include <QStyleFactory>
#include <QSettings>
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#endif

int main(int argc, char *argv[])
{
   // Create Qt list with all arguments

   QStringList arguments;

   for (int i = 0; i < argc; i++)
      arguments += QString::fromLocal8Bit(argv[i]);

   // Parse the command line and get the arguments

   QCommandLineParser parser;

   parser.setApplicationDescription("This application can load DLLs and execute their unit tests.");

   parser.addPositionalArgument("module", "Name of the module (.dll or .exe) or test list (.csv) to load.");
   parser.addPositionalArgument("dataDir", "Optional path to a directory with test data.");

   // Help option
   QCommandLineOption helpOption(QStringList() << "h" << "help", "Show this help.");
   parser.addOption(helpOption);

   // Bath option
   QCommandLineOption batchOption("b", "Do not show the test dialog (batch mode).");
   parser.addOption(batchOption);

   // Gui option
   QCommandLineOption noGuiOption("u", "The tests do not have access to the GUI (unattended). This option has no effect in batch mode.");
   parser.addOption(noGuiOption);

   parser.process(arguments);

   // Check for help

   if (parser.isSet(helpOption))
   {
      QCoreApplication a(argc, argv); // Needed for helpText()

      auto helpText = parser.helpText();

      printf("%s\n", helpText.toLocal8Bit().data());

      return 0;
   }

   arguments = parser.positionalArguments();

   // Make sure the argument list has always 2 arguments

   while (arguments.count() < 2)
      arguments.append(QString());

   // Check if the tests should be shown in a GUI

   if (!parser.isSet(batchOption))
   {
      // Show the test dialog
#ifdef Q_OS_WIN
      FreeConsole();
#endif

      QApplication a(argc, argv);

#ifdef Q_OS_WIN
      // Dark style
      QApplication::setStyle(new QExtendedProxyStyle(QExtendedProxyStyle::EntireCellCheckBoxPosition::Full, QStyleFactory::create("WindowsVista")));

      QSettings settings("HKEY_CURRENT_USER\\Software\\Microsoft\\Windows\\CurrentVersion\\Themes\\Personalize", QSettings::NativeFormat);
      if (settings.value("AppsUseLightTheme") == 0)
      {
         QPalette darkPalette;
         QColor darkColor = QColor(45, 45, 45);
         QColor disabledColor = QColor(127, 127, 127);
         darkPalette.setColor(QPalette::Window, darkColor);
         darkPalette.setColor(QPalette::WindowText, Qt::white);
         darkPalette.setColor(QPalette::Base, QColor(18, 18, 18));
         darkPalette.setColor(QPalette::AlternateBase, darkColor);
         darkPalette.setColor(QPalette::ToolTipBase, Qt::white);
         darkPalette.setColor(QPalette::ToolTipText, Qt::white);
         darkPalette.setColor(QPalette::Text, Qt::white);
         darkPalette.setColor(QPalette::Disabled, QPalette::Text, disabledColor);
         darkPalette.setColor(QPalette::Button, darkColor);
         darkPalette.setColor(QPalette::ButtonText, Qt::white);
         darkPalette.setColor(QPalette::Disabled, QPalette::ButtonText, disabledColor);
         darkPalette.setColor(QPalette::BrightText, Qt::red);
         darkPalette.setColor(QPalette::Link, QColor(42, 130, 218));

         darkPalette.setColor(QPalette::Highlight, QColor(42, 130, 218));
         darkPalette.setColor(QPalette::HighlightedText, Qt::black);
         darkPalette.setColor(QPalette::Disabled, QPalette::HighlightedText, disabledColor);

         qApp->setPalette(darkPalette);

         qApp->setStyleSheet("QToolTip { color: #ffffff; background-color: #2a82da; border: 1px solid white; }");
      }
#else
      // Normal style
      QApplication::setStyle(new BlueGradientProxyStyle(QExtendedProxyStyle::EntireCellCheckBoxPosition::Full, QApplication::style()));
#endif

      TestDialog w;

      w.setTestWithGui(!parser.isSet(noGuiOption));

      w.show();

      if (!arguments[0].isEmpty())
         w.loadTestDll(arguments[0]);
      if (!arguments[1].isEmpty())
         w.setTestDataDir(arguments[1]);

      return a.exec();
   }

   // Execute the tests without showing a GUI

#ifdef Q_OS_WIN
   // Prevent displaying a message box when loading a DLL and depending DLLs are missing!
   SetErrorMode(SEM_FAILCRITICALERRORS | SEM_NOOPENFILEERRORBOX);
#endif

   // Create a core application
   QCoreApplication a(argc, argv);

   // Load the tests

   TestManager testManager;
   FunctionStatus functionStatus;

   if (arguments[0].endsWith(".csv", Qt::CaseInsensitive))
   {
      printf("Loading modules from file \"%s\"\n", arguments[0].toLocal8Bit().data());

      testManager.addModulesFromFile(functionStatus, arguments[0]);
   }
   else
   {
      printf("Adding module \"%s\" with data path \"%s\"\n", arguments[0].toLocal8Bit().data(), arguments[1].toLocal8Bit().data());

      testManager.addModule(functionStatus, arguments[0], arguments[1]);
   }

   if (functionStatus.hasError())
   {
      printf("%s\n", functionStatus.completeMessage().toLocal8Bit().data());
      return 1;
   }

   // Execute the tests

   for (auto&& testModule : testManager.testModules())
   {
      auto moduleStatus = functionStatus.subStatus(QApplication::translate("main", "Testing module \"%1\"").arg(testModule->libraryFile()));

      if (testModule->dataDirectory().isEmpty())
         moduleStatus.addWarning(QApplication::translate("main", "No test data."));
      else
         moduleStatus.addInformation(QApplication::translate("main", "Test data in \"%1\".").arg(testModule->dataDirectory()));

      for (auto testSuite : testModule->testSuites())
      {
         auto suiteStatus = moduleStatus.subStatus(QApplication::translate("main", "TestSuite %1").arg(testSuite.name()));

         testSuite.init(suiteStatus);

         for (int iMethod = 0; iMethod < testSuite.methodCount(); iMethod++)
         {
            auto methodStatus = suiteStatus.subStatus(QApplication::translate("main", "%1:").arg(testSuite.methodSignature(iMethod)));

            testSuite.executeMethod(iMethod, methodStatus);

            if (!methodStatus.hasError())
               methodStatus.addInformation("OK");
         }

         testSuite.deinit(suiteStatus);
      }
   }

   // Show results

   printf("%s\n", functionStatus.completeMessage().toLocal8Bit().data());

   return functionStatus.hasError() ? 1 : 0;
}
