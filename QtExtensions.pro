TEMPLATE = subdirs
SUBDIRS += QtGuiEx \
    TestFramework \
    TestRunner \
    QtGuiExDesignerPlugin \
    QtCoreEx \
    QtService \
    QtODBCConnector \
    QtDatabaseAccess \
    QtSQLiteConnector

QtGuiEx.subdir = QtGuiEx
TestFramework.subdir = TestFramework
TestRunner.subdir = TestRunner
QtGuiExDesignerPlugin.subdir = QtGuiExDesignerPlugin
QtCoreEx.subdir = QtCoreEx
QtService.subdir = QtService
#QtOracleConnector.subdir = QtOracleConnector
QtODBCConnector.subdir = QtODBCConnector
QtDatabaseAccess.subdir = QtDatabaseAccess
QtSQLiteConnector.subdir = QtSQLiteConnector

QtGuiEx.depends = QtCoreEx TestFramework
TestRunner.depends = QtGuiEx QtCoreEx TestFramework
QtGuiExDesignerPlugin.depends = QtGuiEx QtCoreEx
QtCoreEx.depends = TestFramework
QtService.depends = QtCoreEx TestFramework
#QtOracleConnector.depends = QtCoreEx
QtODBCConnector.depends = QtCoreEx TestFramework
QtDatabaseAccess.depends = QtCoreEx TestFramework
QtSQLiteConnector.depends = QtCoreEx TestFramework
