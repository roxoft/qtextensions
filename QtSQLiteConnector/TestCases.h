#ifndef TESTCASES_H
#define TESTCASES_H

#include <TestFramework.h>

class TestCases : public TestFramework
{
   Q_OBJECT

public:
   TestCases(QObject *parent = 0);
   virtual ~TestCases();

public slots:
   void basicTests();
};

#endif // TESTCASES_H
