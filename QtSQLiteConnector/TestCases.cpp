#include "TestCases.h"
#include <QDbConnection.h>
#include <QDbCommand.h>
#include "QSQLiteConnection.h"
#include <QFile>
#include <QtCore/QDir>

TestCases::TestCases(QObject *parent)
   : TestFramework(parent)
{
}

TestCases::~TestCases()
{
}

void TestCases::basicTests()
{
   QString testDatabaseFile = QDir::temp().filePath("testdatabase.sqlite");

   {
      auto sqliteConnection = new QSQLiteConnection(testDatabaseFile);
      QDbConnection connection = sqliteConnection;

      if (connection.state().type() == QDbState::Error)
      {
         addError(connection.state().text());
         return;
      }

      QDbCommand query(connection);

      // Create test table

      query.setStatement(eutf8("create table TestTable ( TT_TEXT TEXT, TT_LONG INTEGER, TT_INT INT, TT_BOOL BOOLEAN, TT_NUMERIC DECIMAL(12,6) );"));

      if (!query.exec())
         addError(query.state().text());

      // Check schema

      auto databaseSchema = sqliteConnection->databaseSchema();
      auto tableSchema = connection.tableSchema("TestTable");

      T_EQUALS(tableSchema.columns.count(), 5);
      T_EQUALS(tableSchema.columns[0].vendorType, DT_STRING);
      T_EQUALS(tableSchema.columns[1].vendorType, DT_LONG);
      T_EQUALS(tableSchema.columns[2].vendorType, DT_INT);
      T_EQUALS(tableSchema.columns[3].vendorType, DT_BOOL);
      T_EQUALS(tableSchema.columns[4].vendorType, DT_DECIMAL);
      T_EQUALS(tableSchema.columns[4].size, 12);
      T_EQUALS(tableSchema.columns[4].scale, 6);

      // Insert rows

      query.setStatement("insert into TestTable values (:\"PH 01\", :'ph 02', :PH03, :PH04, :PH05)");

      query.setPlaceholderValue("PH 01", "Testtext");
      query.setPlaceholderValue("ph 02", 123456789123456789LL);
      query.setPlaceholderValue("PH03", 123456789);
      query.setPlaceholderValue("PH04", true);
      query.setPlaceholderValue("PH05", 123456.123456);

      T_ERROR_IF(!query.exec(), query.state().text());

      query.setPlaceholderValue("PH 01", "Anderer Text");
      query.setPlaceholderValue("ph 02", 2LL);
      query.setPlaceholderValue("PH03", 3);
      query.setPlaceholderValue("PH04", false);
      query.setPlaceholderValue("PH05", 4.5);

      T_ERROR_IF(!query.exec(), query.state().text());

      // Insert with literals

      query.setStatement(QString("insert into TestTable values (%1, %2, %3, %4, %5)")
         .arg(connection.toLiteral(DT_STRING, "\\Path\\Name\nLine\\\nNew \\ä\\nö\\rü\\tß line\\"))
         .arg(connection.toLiteral(DT_INT, 1))
         .arg(connection.toLiteral(DT_INT, 4))
         .arg(connection.toLiteral(DT_INT, 0))
         .arg(connection.toLiteral(DT_INT, 7))
      );

      T_ERROR_IF(!query.exec(), query.state().text());

      query.setStatement(QString("insert into TestTable values (%1, %2, %3, %4, %5)")
         .arg(connection.toLiteral(DT_STRING, "\\Path\\Name\r\nLine\\\r\nNew \\ä\\nö\\rü\\tß line\\"))
         .arg(connection.toLiteral(DT_INT, 0))
         .arg(connection.toLiteral(DT_INT, 0))
         .arg(connection.toLiteral(DT_INT, 1))
         .arg(connection.toLiteral(DT_DECIMAL, QDecimal(0)))
      );

      T_ERROR_IF(!query.exec(), query.state().text());

      // Select

      query.setStatement("select TT_TEXT, TT_LONG, TT_INT, TT_BOOL, TT_NUMERIC from TestTable order by TT_LONG");

      T_ERROR_IF(!query.exec(), query.state().text());

      auto result = query.result();
      //auto shema = result.schema();

      T_IS_TRUE(result.next());

      T_EQUALS(result.valueAt("TT_TEXT").toString().replace("\r", "\\r").replace("\n", "\\n"), "\\Path\\Name\\r\\nLine\\\\r\\nNew \\ä\\nö\\rü\\tß line\\");
      T_EQUALS_EX(result.valueAt("TT_LONG"), Variant(0LL));
      T_EQUALS_EX(result.valueAt("TT_INT"), Variant(0LL));
      T_EQUALS_EX(result.valueAt("TT_BOOL"), Variant(1LL));
      T_EQUALS_EX(result.valueAt("TT_NUMERIC"), Variant(0LL));

      T_IS_TRUE(result.next());

      T_EQUALS(result.valueAt("TT_TEXT").toString().replace("\r", "\\r").replace("\n", "\\n"), "\\Path\\Name\\nLine\\\\nNew \\ä\\nö\\rü\\tß line\\");
      T_EQUALS_EX(result.valueAt("TT_LONG"), Variant(1LL));
      T_EQUALS_EX(result.valueAt("TT_INT"), Variant(4LL));
      T_EQUALS_EX(result.valueAt("TT_BOOL"), Variant(0LL));
      T_EQUALS_EX(result.valueAt("TT_NUMERIC"), Variant(7LL));

      T_IS_TRUE(result.next());

      T_EQUALS_EX(result.valueAt("TT_TEXT"), Variant("Anderer Text"));
      T_EQUALS_EX(result.valueAt("TT_LONG"), Variant(2LL));
      T_EQUALS_EX(result.valueAt("TT_INT"), Variant(3LL));
      T_EQUALS_EX(result.valueAt("TT_BOOL"), Variant(0LL));
      T_EQUALS_EX(result.valueAt("TT_NUMERIC"), Variant(4.5));

      T_IS_TRUE(result.next());

      T_EQUALS_EX(result.valueAt("TT_TEXT"), Variant("Testtext"));
      T_EQUALS_EX(result.valueAt("TT_LONG"), Variant(123456789123456789LL));
      T_EQUALS_EX(result.valueAt("TT_INT"), Variant(123456789LL));
      T_EQUALS_EX(result.valueAt("TT_BOOL"), Variant(1LL));
      T_EQUALS_EX(result.valueAt("TT_NUMERIC"), Variant(123456.123456));

      T_IS_TRUE(!result.next());
   }

   T_ERROR_IF(!QFile::remove(testDatabaseFile), QString("The file %1 couldn't be removed").arg(testDatabaseFile));
}

BEGIN_TEST_SUITES
TEST_SUITE(TestCases);
END_TEST_SUITES
