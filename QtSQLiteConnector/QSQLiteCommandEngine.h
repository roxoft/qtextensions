#ifndef QSQLITECOMMANDENGINE_H
#define QSQLITECOMMANDENGINE_H

#include <QDbCommandEngine>
#include <SmartPointer.h>
#include "sqlite3.h"
#include <QVector>

class QSQLiteConnection;

QDbSchema::Column sqliteColumnSchema(const QString& vendorType, const QString& name, bool isNullable, bool hasDefault);

class QSQLiteCommandEngine : public QDbCommandEngine
{
   Q_DISABLE_COPY(QSQLiteCommandEngine)
   Q_DECLARE_TR_FUNCTIONS(QSQLiteCommandEngine)

public:
   QSQLiteCommandEngine(QSQLiteConnection* connection, sqlite3* database);
   virtual ~QSQLiteCommandEngine();

   QDbState rowCount(int &count) const override;

   bool atEnd() const override;

   QDbState next() override;

   QDbSchema::Table  resultSchema() const override;
   QDbState  columnValue(int index, Variant &value) const override;
   QDbState  columnValue(const QString &name, Variant &value) const override;

protected:
   class SQLitePhInfo : public PhInfo
   {
   public:
      SQLitePhInfo(const QDbSchema::Column& schema, QDbConnection::PhType phType) : PhInfo(schema, phType) {}
      virtual ~SQLitePhInfo() {}

      bool isValid() const override { return _schema.type != DT_UNKNOWN; }

      Variant value() const override { return _value; }
      QDbState setValue(const Variant& value) override { _value = value; return QDbState(); }

      Variant _value;
   };

   QDbConnectionData* connection() override;
   PhInfo* createPhInfo(const QDbSchema::Column& schema, QDbConnection::PhType phType) override;
   QDbState setPlaceholderType(PhInfo* phInfo, QDbDataType type) override;
   QDbState setPlaceholderValue(PhInfo* phInfo, const Variant& value) override;
   QDbState placeholderValue(const PhInfo* phInfo, Variant& value) const override;
   QDbState prepare(const QList<PhInfo*>& phList) override;
   QDbState exec(const QList<PhInfo*>& phList, int rowsToFetch) override; // If there are multiple commands in the statement only the first command gets the placeholders set!
   void clear() override;

private:
   QDbState execNextStatement(const ushort*& nextStatement, const QList<PhInfo*>& phList);
   int bindPlaceholder(const SQLitePhInfo* ph, int pos);

private:
   bp<QSQLiteConnection> _connection;

   sqlite3*       _database = nullptr;
   sqlite3_stmt*  _stmt = nullptr;
   bool           _hasMore = false;
   int            _rowsRead = 0;
   QString        _lastStatement;
};

#endif // QSQLITECOMMANDENGINE_H
