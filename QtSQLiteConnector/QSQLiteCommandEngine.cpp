#include "QSQLiteCommandEngine.h"
#include "QSQLiteConnection.h"
#include "sqlite3.h"
#include <SqlParser.h>

QDbSchema::Column sqliteColumnSchema(const QString& vendorType, const QString& name, bool isNullable, bool hasDefault)
{
   QDbSchema::Column columnInfo;

   columnInfo.name = name;

   // The following interpretation of type is according to the SQLite documentation

   if (vendorType.contains("INTEGER", Qt::CaseInsensitive))
      columnInfo.type = DT_LONG;
   else if (vendorType.contains("INT", Qt::CaseInsensitive))
      columnInfo.type = DT_INT;
   else if (vendorType.contains("CHAR", Qt::CaseInsensitive) || vendorType.contains("CLOB", Qt::CaseInsensitive) || vendorType.contains("TEXT", Qt::CaseInsensitive))
      columnInfo.type = DT_STRING;
   else if (vendorType.contains("BLOB", Qt::CaseInsensitive) || vendorType.isEmpty())
      columnInfo.type = DT_BINARY;
   else if (vendorType.contains("REAL", Qt::CaseInsensitive) || vendorType.contains("FLOA", Qt::CaseInsensitive) || vendorType.contains("DOUB", Qt::CaseInsensitive))
      columnInfo.type = DT_DOUBLE;
   else
      columnInfo.type = DT_DECIMAL;

   // Get more specific information by parsing the vendorType

   auto vt = vendorType;
   QTextStream is(&vt, QIODevice::ReadOnly);
   Tokenizer tokenizer(&is, createSqlContext());

   tokenizer.setIgnoreWhiteSpace();

   auto token = tokenizer.next();

   if (token.isIdentifier("BOOL", Qt::CaseInsensitive) || token.isIdentifier("BOOLEAN", Qt::CaseInsensitive))
   {
      columnInfo.vendorType = DT_BOOL;
   }
   else if (token.isIdentifier("INT", Qt::CaseInsensitive))
   {
      columnInfo.vendorType = DT_INT;
   }
   else if (token.isIdentifier("INTEGER", Qt::CaseInsensitive))
   {
      columnInfo.vendorType = DT_LONG;
   }
   else if (token.isIdentifier("TEXT", Qt::CaseInsensitive) || token.isIdentifier("CLOB", Qt::CaseInsensitive))
   {
      columnInfo.vendorType = DT_STRING;
   }
   else if (token.identifier().startsWith("VARCHAR", Qt::CaseInsensitive))
   {
      columnInfo.vendorType = DT_STRING;
   }
   else if (token.isIdentifier("REAL", Qt::CaseInsensitive) || token.isIdentifier("DOUBLE", Qt::CaseInsensitive))
   {
      columnInfo.vendorType = DT_DOUBLE;
   }
   else if (token.isIdentifier("DECIMAL", Qt::CaseInsensitive))
   {
      columnInfo.vendorType = DT_DECIMAL;

      token = tokenizer.next();

      if (token.isSeparator(u'('))
      {
         token = tokenizer.next();
         columnInfo.size = token.value().toInt();
         auto opToken = tokenizer.next().as<SqlOperatorToken>();
         if (opToken && opToken->op().action() == SqlOperator::Action::Comma)
         {
            token = tokenizer.next();
            columnInfo.scale = token.value().toInt();
         }
      }
   }
   else if (token.isIdentifier("DATE", Qt::CaseInsensitive))
   {
      columnInfo.vendorType = DT_DATE;
   }
   else if (token.isIdentifier("TIME", Qt::CaseInsensitive))
   {
      columnInfo.vendorType = DT_TIME;
   }
   else if (token.isIdentifier("DATETIME", Qt::CaseInsensitive))
   {
      columnInfo.vendorType = DT_DATETIME;
   }
   else if (token.isIdentifier("BLOB", Qt::CaseInsensitive))
   {
      columnInfo.vendorType = DT_BINARY;
   }

   columnInfo.nullable = isNullable;

   columnInfo.hasDefault = hasDefault;

   columnInfo.readable = true;

   return columnInfo;
}

QSQLiteCommandEngine::QSQLiteCommandEngine(QSQLiteConnection* connection, sqlite3* database)
   : _connection(connection), _database(database)
{
}

QSQLiteCommandEngine::~QSQLiteCommandEngine()
{
   if (_stmt)
      sqlite3_finalize(_stmt);
}

QDbState QSQLiteCommandEngine::rowCount(int &count) const
{
   QDbState dbState;

   count = sqlite3_changes(_database);

   return dbState;
}

bool QSQLiteCommandEngine::atEnd() const
{
   return !_hasMore;
}

QDbState QSQLiteCommandEngine::next()
{
   QDbState dbState(_statement);

   if (_hasMore)
   {
      if (_rowsRead) // The first row was already fetched
      {
         _hasMore = false;

         int status = sqlite3_step(_stmt);

         if (status == SQLITE_BUSY)
         {
            // The engine was unable to acquire the database locks.
            // If we are inside a transaction and the statement is not a COMMIT the next statement should be a ROLLBACK
            dbState = QDbState(QDbState::Error, sqlite3_errmsg(_database), QDbState::Statement);
         }
         else if (status == SQLITE_DONE)
         {
            // The statement executed successfully.
            // Do not call sqlite3_step again without first calling sqlite3_reset()
         }
         else if (status == SQLITE_ROW)
         {
            // The statement executed successfully and data is available
            _hasMore = true;
         }
         else
         {
            dbState = QDbState(QDbState::Error, sqlite3_errmsg(_database), QDbState::Statement);
         }
      }
   }

   if (_hasMore)
      _rowsRead++;

   return dbState;
}

QDbSchema::Table QSQLiteCommandEngine::resultSchema() const
{
   QDbSchema::Table table;

   table.name = QLatin1String("SELECT_LIST");

   const auto columnCount = sqlite3_column_count(_stmt);

   // Fetch column schemas
   for (int i = 0; i < columnCount; ++i)
   {
      const auto szDeclType = (const ushort*)sqlite3_column_decltype16(_stmt, i);

      if (szDeclType && *szDeclType)
      {
         table.columns.append(sqliteColumnSchema(QString::fromUtf16((const char16_t*)szDeclType), QString::fromUtf16((const char16_t*)sqlite3_column_name16(_stmt, i)), true, false));
      }
      else
      {
         const auto type = sqlite3_column_type(_stmt, i);

         QDbSchema::Column columnInfo;

         columnInfo.name = QString::fromUtf16((const char16_t*)sqlite3_column_name16(_stmt, i));
         switch (type)
         {
         case SQLITE_INTEGER:
            columnInfo.type = DT_LONG;
            break;
         case SQLITE_FLOAT:
            columnInfo.type = DT_DOUBLE;
            break;
         case SQLITE3_TEXT:
            columnInfo.type = DT_STRING;
            break;
         case SQLITE_BLOB:
            columnInfo.type = DT_BINARY;
            break;
         case SQLITE_NULL:
            columnInfo.type = DT_UNKNOWN;
            break;
         }
         // !!! DT_DECIMAL !!!
         columnInfo.vendorType = columnInfo.type;
         columnInfo.size = -1;
         columnInfo.scale = -1;
         columnInfo.nullable = true;
         columnInfo.readable = true;

         table.columns.append(columnInfo);
      }
   }

   return table;
}

QDbState QSQLiteCommandEngine::columnValue(int index, Variant &value) const
{
   if (!_hasMore)
      return QDbState(QDbState::Error, tr("Invalid position of row counter"), QDbState::Statement);

   int columnCount = sqlite3_column_count(_stmt);

   if (index < 0 || index >= columnCount)
      return QDbState(QDbState::Error, tr("Index %1 not in result set").arg(index), QDbState::Statement);

   const auto type = sqlite3_column_type(_stmt, index);

   switch (type)
   {
   case SQLITE_INTEGER:
      value = sqlite3_column_int64(_stmt, index);
      break;
   case SQLITE_FLOAT:
      value = sqlite3_column_double(_stmt, index);
      break;
   case SQLITE3_TEXT:
      value = QString::fromUtf16((const char16_t*)sqlite3_column_text16(_stmt, index));
      break;
   case SQLITE_BLOB:
   {
      const auto blob = (const char*)sqlite3_column_blob(_stmt, index);
      const auto size = sqlite3_column_bytes(_stmt, index); // Call sqlite3_column_bytes after sqlite3_column_blob so that any conversions are finished.

      value = QByteArray(blob, size);
      break;
   }
   case SQLITE_NULL:
      value.clear();
      break;
   }

   return QDbState();
}

QDbState QSQLiteCommandEngine::columnValue(const QString &name, Variant &value) const
{
   const auto columnCount = sqlite3_column_count(_stmt);

   for (int i = 0; i < columnCount; ++i)
   {
      auto columnName = QString::fromUtf16((const char16_t*)sqlite3_column_name16(_stmt, i));

      if (columnName.compare(name, Qt::CaseInsensitive) == 0)
         return columnValue(i, value);
   }

   return QDbState(QDbState::Error, tr("Column name %1 not found in result set").arg(name), QDbState::Statement);
}

QDbConnectionData* QSQLiteCommandEngine::connection()
{
   return _connection.data();
}

QDbCommandEngine::PhInfo* QSQLiteCommandEngine::createPhInfo(const QDbSchema::Column& schema, QDbConnection::PhType phType)
{
   return new SQLitePhInfo(schema, phType);
}

QDbState QSQLiteCommandEngine::setPlaceholderType(PhInfo* phInfo, QDbDataType type)
{
   if (phInfo->type() != type)
   {
      if (_stmt)
         return QDbState(QDbState::Error, tr("The data type of the placeholder at position %1 cannot be set after execution").arg(phInfo->positions().first()), _statement);

      phInfo->setType(type);

      if (!phInfo->isValid())
         return errorInferringDataType(type, phInfo);
   }

   return QDbState();
}

QDbState QSQLiteCommandEngine::setPlaceholderValue(PhInfo* phInfo, const Variant& value)
{
   if (phInfo->type() == DT_UNKNOWN)
   {
      // Infer the type from the value
      const auto type = dbTypeFromVariant(value);

      if (type == DT_UNKNOWN)
         return errorInferringDataType(value, phInfo);

      phInfo->setType(type);
   }

   return phInfo->setValue(value);
}

QDbState QSQLiteCommandEngine::placeholderValue(const PhInfo* phInfo, Variant& value) const
{
   if (phInfo->schema().type == DT_UNKNOWN)
      return errorInvalidVariable(phInfo);

   value = phInfo->value();

   return QDbState();
}

QDbState QSQLiteCommandEngine::prepare(const QList<PhInfo*>& phList)
{
   return QDbState();
}

QDbState QSQLiteCommandEngine::exec(const QList<PhInfo*>& phList, int rowsToFetch)
{
   QDbState dbState(_statement);

   if (_preparedStatement.isEmpty())
   {
      // Statement without placeholder
      _preparedStatement = _statement;
   }

   auto nextStatement = _preparedStatement.utf16();
   auto nextPhList = phList;

   do
   {
      dbState = execNextStatement(nextStatement, nextPhList);
      nextPhList.clear();
   } while (dbState.type() != QDbState::Error && nextStatement != nullptr);

   return dbState;
}

void QSQLiteCommandEngine::clear()
{
   QDbCommandEngine::clear();
   if (_stmt)
      sqlite3_finalize(_stmt);
   _stmt = nullptr;
   _hasMore = false;
   _rowsRead = 0;
}

QDbState QSQLiteCommandEngine::execNextStatement(const ushort*& nextStatement, const QList<PhInfo*>& phList)
{
   _hasMore = false;
   _rowsRead = 0;

   if (_stmt)
   {
      // !! The statement cannot be reused because the data is bound by value and not by buffer
      //if (_lastStatement == QString::fromUtf16(nextStatement))
      //{
      //   nextStatement = nullptr;
      //}
      //else
      {
         if (sqlite3_finalize(_stmt) != SQLITE_OK)
            return QDbState(QDbState::Error, sqlite3_errmsg(_database));
         _stmt = nullptr;
      }
   }

   if (_stmt == nullptr)
   {
      // Are all placeholders defined?
      for (int i = 0; i < phList.count(); i++)
      {
         if (!phList[i]->isValid())
            return QDbState(QDbState::Error, tr("Placeholder at position %1 is not defined").arg(i + 1), _statement);
      }

      // Prepare the statement to bind the placeholders

      const auto currentStmt = nextStatement;

      if (sqlite3_prepare16_v2(_database, nextStatement, -1, &_stmt, (const void**)&nextStatement) != SQLITE_OK)
         return QDbState(QDbState::Error, sqlite3_errmsg(_database), QString::fromUtf16((const char16_t*)currentStmt));

      // Skip white space to the next statement and reset the statement if it was the last

      while (QChar(*nextStatement).isSpace())
         nextStatement++;

      _lastStatement = QString::fromUtf16((const char16_t*)currentStmt, nextStatement - currentStmt);

      if (*nextStatement == 0)
         nextStatement = nullptr;

      // Bind the placeholder

      for (auto index = 0; index < phList.count(); ++index)
      {
         for (auto&& pos : phList[index]->positions())
         {
            int status = bindPlaceholder(dynamic_cast<SQLitePhInfo*>(phList[index]), pos);

            QDbState dbState;

            if (status == -1)
               dbState = QDbState(QDbState::Error, tr("Unknown parameter type"), QDbState::Statement);
            else if (status != SQLITE_OK)
               dbState = QDbState(QDbState::Error, sqlite3_errmsg(_database), QDbState::Statement);

            if (dbState.type() == QDbState::Error)
               return dbState.withSourceText(_statement);
         }
      }
   }

   // Execute the statement

   QDbState dbState;

   int status = sqlite3_step(_stmt);

   if (status == SQLITE_BUSY)
   {
      // The engine was unable to acquire the database locks.
      // If we are inside a transaction and the statement is not a COMMIT the next statement should be a ROLLBACK
      dbState = QDbState(QDbState::Error, sqlite3_errmsg(_database), _lastStatement);
   }
   else if (status == SQLITE_DONE)
   {
      // The statement executed successfully.
      // Do not call sqlite3_step again without first calling sqlite3_reset()
   }
   else if (status == SQLITE_ROW)
   {
      // The statement executed successfully and data is available
      _hasMore = true;
   }
   else
   {
      dbState = QDbState(QDbState::Error, sqlite3_errmsg(_database), description());
   }

   return dbState;
}

int QSQLiteCommandEngine::bindPlaceholder(const SQLitePhInfo* ph, int pos)
{
   int status = SQLITE_OK;

   if (ph->_value.isValueNull())
      status = sqlite3_bind_null(_stmt, pos);
   else
   {
      switch (ph->schema().type)
      {
      case DT_BOOL:
         status = sqlite3_bind_int(_stmt, pos, ph->_value.toBool() ? 1 : 0);
         break;
      case DT_INT:
         status = sqlite3_bind_int(_stmt, pos, ph->_value.toInt());
         break;
      case DT_UINT:
         status = sqlite3_bind_int64(_stmt, pos, ph->_value.toUInt());
         break;
      case DT_LONG:
         status = sqlite3_bind_int64(_stmt, pos, ph->_value.to<long long int>());
         break;
      case DT_ULONG:
         status = sqlite3_bind_int64(_stmt, pos, ph->_value.to<unsigned long long int>()); // Not supported!
         break;
      case DT_DOUBLE:
         status = sqlite3_bind_double(_stmt, pos, ph->_value.toDouble());
         break;
      case DT_DECIMAL:
         status = sqlite3_bind_text16(_stmt, pos, ph->_value.toString(QLocale::C).utf16(), -1, SQLITE_TRANSIENT);
         break;
      case DT_STRING:
      case DT_LARGE_STRING:
      case DT_NSTRING:
      case DT_LARGE_NSTRING:
         status = sqlite3_bind_text16(_stmt, pos, ph->_value.toString(QLocale::C).utf16(), -1, SQLITE_TRANSIENT);
         break;
      case DT_DATE:
      case DT_TIME:
      case DT_DATETIME:
         status = sqlite3_bind_text16(_stmt, pos, ph->_value.toDateTimeEx().toISOString().utf16(), -1, SQLITE_TRANSIENT);
         break;
      case DT_BINARY:
      case DT_LARGE_BINARY:
      {
         const auto byteArray = ph->_value.toByteArray();
         status = sqlite3_bind_blob(_stmt, pos, byteArray.data(), byteArray.length(), SQLITE_TRANSIENT);
         break;
      }
      default:
         status = -1;
         break;
      }
   }

   return status;
}
