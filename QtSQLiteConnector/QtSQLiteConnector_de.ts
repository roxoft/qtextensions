<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE">
<context>
    <name>QSQLiteCommandEngine</name>
    <message>
        <location filename="QSQLiteCommandEngine.cpp" line="230"/>
        <source>Invalid position of row counter</source>
        <translation>Ungültige Position des Datensatzzählers</translation>
    </message>
    <message>
        <location filename="QSQLiteCommandEngine.cpp" line="235"/>
        <source>Index %1 not in result set</source>
        <translation>Der Index %1 ist nicht im Ergebnis</translation>
    </message>
    <message>
        <location filename="QSQLiteCommandEngine.cpp" line="278"/>
        <source>Column name %1 not found in result set</source>
        <translation>Der Spaltenname %1 ist nicht im Ergebnis</translation>
    </message>
    <message>
        <location filename="QSQLiteCommandEngine.cpp" line="296"/>
        <source>The data type of the placeholder at position %1 cannot be set after execution</source>
        <translation>Datentyp für den Platzhalter an Position %1 darf nach der Ausführung nicht gesetzt werden</translation>
    </message>
    <message>
        <location filename="QSQLiteCommandEngine.cpp" line="396"/>
        <source>Placeholder at position %1 is not defined</source>
        <translation>Der Platzhalter an Position %1 ist nicht definiert</translation>
    </message>
    <message>
        <location filename="QSQLiteCommandEngine.cpp" line="427"/>
        <source>Unknown parameter type</source>
        <translation>Unbekannter Parametertyp</translation>
    </message>
</context>
</TS>
