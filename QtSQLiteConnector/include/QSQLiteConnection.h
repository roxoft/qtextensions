#ifndef QSQLITECONNECTION_H
#define QSQLITECONNECTION_H

#include "qtsqliteconnector_global.h"
#include <QDbConnectionData>

class QT5SQLITECONNECTOR_EXPORT QSQLiteConnection : public QDbConnectionData
{
   Q_DISABLE_COPY(QSQLiteConnection)

public:
   QSQLiteConnection(const QString &dataSourceName);
   ~QSQLiteConnection() override;

   bool isValid() const override;

   QString  connectionString() const override;
   QString  dataSourceName() const override;
   QString  driverName() const override;
   SqlStyle sqlStyle() const override;
   QDbConnection::PhType phType() const override;

   QDbEnvironment *environment() override;

   bool beginTransaction() override;
   bool rollbackTransaction() override;
   bool commitTransaction() override;

   bool abortExecution() override;

   bool tableExists(const QString &tableName) override;

   QChar phIndicator() const override;

   QString toDateLiteral(const QDateEx &value) const override;
   QString toTimeLiteral(const QTimeEx &value) const override;
   QString toDateTimeLiteral(const QDateTimeEx &value) const override;
   QString toStringLiteral(QString value) const override;

   QDbSchema::Database   databaseSchema() override;
   QDbSchema::Synonym    synonymSchema(const QString &name) override;
   QDbSchema::Table      tableSchema(const QString &name) override;
   QDbSchema::Function   functionSchema(const QString &name) override;
   QDbSchema::Package    packageSchema(const QString &name) override;

protected:
   QDbCommandEngine *newEngine() override;

private:
   class Data;

private:
   QString  _connectString;
   Data*    _data;
};

extern "C" {
   QT5SQLITECONNECTOR_EXPORT QDbConnectionData *newSQLiteConnection(const QString &dataSourceName);
}

#endif // QSQLITECONNECTION_H
