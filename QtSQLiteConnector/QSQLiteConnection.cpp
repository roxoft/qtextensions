#include "QSQLiteConnection.h"
#include "sqlite3.h"
#include <QRational.h>
#include "QSQLiteCommandEngine.h"

class QSQLiteConnection::Data
{
public:
   Data() : database(nullptr) {}
   ~Data()
   {
      if (database)
         sqlite3_close(database);
   }

public:
   sqlite3* database;
};

static QDbEnvironment theEnvironment;

QSQLiteConnection::QSQLiteConnection(const QString &dataSourceName)
   : _connectString(dataSourceName), _data(new Data)
{
   if (sqlite3_open16(dataSourceName.utf16(), &_data->database) != SQLITE_OK)
   {
      _dbState = QDbState(QDbState::Error, sqlite3_errmsg(_data->database), QDbState::Connection);
      if (_data->database)
         sqlite3_close(_data->database);
      _data->database = nullptr;
   }
}

QSQLiteConnection::~QSQLiteConnection()
{
   delete _data;
}

bool QSQLiteConnection::isValid() const
{
   return _data->database;
}

QString QSQLiteConnection::connectionString() const
{
   return _connectString;
}

QString QSQLiteConnection::dataSourceName() const
{
   return _connectString;
}

QString QSQLiteConnection::driverName() const
{
   return QLatin1String("SQLite");
}

SqlStyle QSQLiteConnection::sqlStyle() const
{
   return SqlStyle::SQLite;
}

QDbConnection::PhType QSQLiteConnection::phType() const
{
   return QDbConnection::PHT_UnnamedOnly;
}

QDbEnvironment *QSQLiteConnection::environment()
{
   return &theEnvironment;
}

bool QSQLiteConnection::beginTransaction()
{
   char *errorMsg = nullptr;

   if (sqlite3_exec(_data->database, "BEGIN TRANSACTION", nullptr, nullptr, &errorMsg) == SQLITE_OK)
      return true;

   if (errorMsg)
   {
      _dbState = QDbState(QDbState::Error, errorMsg, QDbState::Connection);
      sqlite3_free(errorMsg);
      errorMsg = nullptr;
   }
   else
      _dbState = QDbState(QDbState::Error, "Unknown error", QDbState::Connection);


   return false;
}

bool QSQLiteConnection::rollbackTransaction()
{
   char *errorMsg = nullptr;

   if (sqlite3_exec(_data->database, "ROLLBACK TRANSACTION", nullptr, nullptr, &errorMsg) == SQLITE_OK)
      return true;

   if (errorMsg)
   {
      _dbState = QDbState(QDbState::Error, errorMsg, QDbState::Connection);
      sqlite3_free(errorMsg);
      errorMsg = nullptr;
   }
   else
      _dbState = QDbState(QDbState::Error, "Unknown error", QDbState::Connection);


   return false;
}

bool QSQLiteConnection::commitTransaction()
{
   char *errorMsg = nullptr;

   if (sqlite3_exec(_data->database, "COMMIT TRANSACTION", nullptr, nullptr, &errorMsg) == SQLITE_OK)
      return true;

   if (errorMsg)
   {
      _dbState = QDbState(QDbState::Error, errorMsg, QDbState::Connection);
      sqlite3_free(errorMsg);
      errorMsg = nullptr;
   }
   else
      _dbState = QDbState(QDbState::Error, "Unknown error", QDbState::Connection);


   return false;
}

bool QSQLiteConnection::abortExecution()
{
   return false;
}

bool QSQLiteConnection::tableExists(const QString &tableName)
{
   return tableSchema(tableName).isValid();
}

QChar QSQLiteConnection::phIndicator() const
{
   return u':';
}

QString QSQLiteConnection::toDateLiteral(const QDateEx& value) const
{
   return QString("'%1'").arg(value.toISOString());
}

QString QSQLiteConnection::toTimeLiteral(const QTimeEx& value) const
{
   return QString("'%1'").arg(value.toISOString());
}

QString QSQLiteConnection::toDateTimeLiteral(const QDateTimeEx& value) const
{
   return QString("'%1'").arg(value.toISOString());
}

QString QSQLiteConnection::toStringLiteral(QString value) const
{
   return QDbConnectionData::toStringLiteral(value);
}

static int sqliteTableCallback(void* pTableNames, int argc, char** argv, char** azColName)
{
   auto tableNames = (QStringList*)pTableNames;

   Q_ASSERT(argc == 1);

   if (argv[0])
   {
      tableNames->append(QString::fromUtf8(argv[0]));
   }

   return 0;
}

QDbSchema::Database QSQLiteConnection::databaseSchema()
{
   QStringList tableNames;

   char *errorMsg = nullptr;

   if (sqlite3_exec(_data->database, "SELECT name FROM sqlite_master WHERE type='table'", sqliteTableCallback, &tableNames, &errorMsg) != SQLITE_OK)
   {
      if (errorMsg)
      {
         _dbState = QDbState(QDbState::Error, errorMsg, QDbState::Connection);
         sqlite3_free(errorMsg);
         errorMsg = nullptr;
      }
      else
         _dbState = QDbState(QDbState::Error, "Unknown error", QDbState::Connection);
   }

   QDbSchema::Database dbInfo;

   foreach (const QString& tableName, tableNames)
   {
      dbInfo.tables.insert(tableName, tableSchema(tableName));
   }

   return dbInfo;
}

QDbSchema::Synonym QSQLiteConnection::synonymSchema(const QString &name)
{
   QDbSchema::Synonym synonymInfo;

   return synonymInfo;
}

static int sqliteColumnCallback(void* pTableInfo, int argc, char** argv, char** azColName)
{
   auto tableInfo = (QDbSchema::Table*)pTableInfo;

   Q_ASSERT(argc > 4);

   tableInfo->columns.append(sqliteColumnSchema(QString::fromUtf8(argv[2]), QString::fromUtf8(argv[1]), argv[3][0] == '0', argv[4]));

   return 0;
}

QDbSchema::Table QSQLiteConnection::tableSchema(const QString &name)
{
   QDbSchema::Table tableInfo;

   auto command = QString("PRAGMA table_info(%1)").arg(name);

   char *errorMsg = nullptr;

   if (sqlite3_exec(_data->database, command.toUtf8().data(), sqliteColumnCallback, &tableInfo, &errorMsg) != SQLITE_OK)
   {
      if (errorMsg)
      {
         _dbState = QDbState(QDbState::Error, errorMsg, QDbState::Connection);
         sqlite3_free(errorMsg);
         errorMsg = nullptr;
      }
      else
         _dbState = QDbState(QDbState::Error, "Unknown error", QDbState::Connection);
   }
   else if (!tableInfo.columns.isEmpty())
   {
      tableInfo.name = name;
      tableInfo.schema = tableInfo.columns.first().schema;
   }

   return tableInfo;
}

QDbSchema::Function QSQLiteConnection::functionSchema(const QString &name)
{
   QDbSchema::Function funcInfo;

   return funcInfo;
}

QDbSchema::Package QSQLiteConnection::packageSchema(const QString &name)
{
   QDbSchema::Package packageInfo;

   return packageInfo;
}

QDbCommandEngine *QSQLiteConnection::newEngine()
{
   return new QSQLiteCommandEngine(this, _data->database);
}

extern "C" {
   QDbConnectionData* newSQLiteConnection(const QString& dataSourceName)
   {
      return new QSQLiteConnection(dataSourceName);
   }
}
