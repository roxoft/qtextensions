HEADERS += $$PWD/include/QSQLiteConnection.h \
    $$PWD/include/qtsqliteconnector_global.h \
    $$PWD/QSQLiteCommandEngine.h \
    $$PWD/sqlite3.h \
    $$PWD/TestCases.h
SOURCES += $$PWD/QSQLiteCommandEngine.cpp \
    $$PWD/QSQLiteConnection.cpp \
    $$PWD/sqlite3.c \
    $$PWD/TestCases.cpp
