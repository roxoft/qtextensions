#include "TestCases.h"
#include <QDbConnection>
#include <QLibrary>
#include <QDecimal.h>
#include <QFile>
#include <QDir>
#include <QSettingsFile.h>
#include "QtDatabaseAccess"
#include <QDbConnectionData.h>

#define T_SQLITE_EQUALS(connection, command, expected) if (connection.sqlStyle() == SqlStyle::SQLite) { auto actual = getSqlStatement(connection, command); T_EQUALS(actual, QString(expected)); }
#define T_SQLITE_ARG_EQUALS(connection, command, arg1, expected) T_SQLITE_EQUALS(connection, command, QString(expected).arg(arg1))
#define T_ORACLE_EQUALS(connection, command, expected) { auto actual = getSqlStatement(connection.sqlStyle() == SqlStyle::Oracle ? connection : new MockOracleConnection("TestOracle", "F", "T"), command); T_EQUALS(actual, QString(expected)); }
#define T_ORACLE_ARG_EQUALS(connection, command, arg1, expected) T_ORACLE_EQUALS(connection, command, QString(expected).arg(arg1))
#define T_SYBASE_EQUALS(connection, command, expected) if (connection.sqlStyle() == SqlStyle::Sybase) T_EQUALS(getSqlStatement(connection, command), QString(expected));
#define T_DB2_EQUALS(connection, command, expected) if (connection.sqlStyle() == SqlStyle::DB2) { auto actual = getSqlStatement(connection, command); T_EQUALS(actual, QString(expected)); }
#define T_DB2_ARG_EQUALS(connection, command, arg1, expected) T_DB2_EQUALS(connection, command, QString(expected).arg(arg1))
#define T_SQLSERVER_EQUALS(connection, command, expected) { auto actual = getSqlStatement(connection.sqlStyle() == SqlStyle::SqlServer ? connection : new MockSqlServerConnection("TestSqlServer"), command); T_EQUALS(actual, QString(expected)); }
#define T_SQLSERVER_ARG_EQUALS(connection, command, arg1, expected) T_SQLSERVER_EQUALS(connection, command, QString(expected).arg(arg1))

#define Q_DB_TABLE_BEGIN(tblName, clsName) \
   class Tbl##clsName : public QDbTable \
   { \
   public: \
      Tbl##clsName() : QDbTable(#tblName) {} \
      Tbl##clsName(const QDbDataSource& dataSource) : QDbTable(dataSource) {} \
      ~Tbl##clsName() = default;

#define DB_NATIVE_TYPE_Bool bool
#define DB_NATIVE_TYPE_Int qint32
#define DB_NATIVE_TYPE_Decimal QDecimal
#define DB_NATIVE_TYPE_String QString
#define DB_NATIVE_TYPE_Blob QByteArray
#define DB_NATIVE_TYPE_Date QDateEx
#define DB_NATIVE_TYPE_Time QTimeEx
#define DB_NATIVE_TYPE_DateTime QDateTimeEx

#define Q_DB_COLUMN(cName, fName, type) \
   QDbTypedColumn<DB_NATIVE_TYPE_##type> col##fName() \
   { \
      return column(#cName); \
   }

#define Q_DB_TABLE_END };

Q_DB_TABLE_BEGIN(CR_TEST, CrTest)
   Q_DB_COLUMN(ID, Id, Decimal)
   Q_DB_COLUMN(SMALL_INT, SmallInt, Int);
   Q_DB_COLUMN(NAME, Name, String)
   Q_DB_COLUMN(ZAHL, Zahl, Decimal)
   Q_DB_COLUMN(BOOL, Bool, Bool)
   Q_DB_COLUMN(DATUM, Datum, Date)
Q_DB_TABLE_END

class TestElement
{
public:
   TestElement() = default;
   ~TestElement() = default;

   int number() const { return _number; }
   void setNumber(int number) { _number = number; }

   QDecimal decimal() const { return _decimal; }
   void setDecimal(const QDecimal& decimal) { _decimal = decimal; }

   QString name() const { return _name; }
   void setName(const QString& name) { _name = name; }

   QDate date() const { return _date; }
   void setDate(const QDate& date) { _date = date; }

   Variant value() const { return _value; }
   void setValue(const Variant& value) { _value = value; }

public:
   int _num1 = 0;
   int _num2 = 0;
   QString _test;

private:
   int _number = 0;
   QDecimal _decimal;
   QString _name;
   QDate _date;
   Variant _value;
};

static QDbEnvironment theTestEnvironment;

class MockOracleConnection : public QDbConnectionData
{
   Q_DISABLE_COPY(MockOracleConnection)

public:
   MockOracleConnection(const QString& dataSourceName, const QString& falseString = "0", const QString& trueString = "1")
      : _dataSourceName(dataSourceName), _falseString(falseString), _trueString(trueString) {}
   ~MockOracleConnection() override = default;

   bool isValid() const override { return false; }

   QString  connectionString() const override { return _dataSourceName; }
   QString  dataSourceName() const override { return _dataSourceName; }
   QString  driverName() const override { return "ORACLE"; }
   SqlStyle sqlStyle() const override { return SqlStyle::Oracle; }
   QDbConnection::PhType phType() const override { return QDbConnection::PHT_NamedOnly; }

   QDbEnvironment* environment() override { return &theTestEnvironment; }

   bool beginTransaction() override { return true; }
   bool rollbackTransaction() override { return true; }
   bool commitTransaction() override { return true; }

   bool abortExecution() override { return false; }

   bool tableExists(const QString& tableName) override { return true; }

   QChar phIndicator() const override { return u':'; }

   QString toBooleanLiteral(bool value) const override { return QString("'%1'").arg(value ? _trueString : _falseString); }
   QString toDateLiteral(const QDateEx& value) const override { return QString("TO_DATE('%1', 'YYYY-MM-DD')").arg(value.toString(SQL92_DATE_FORMAT)); }
   QString toTimeLiteral(const QTimeEx& value) const override { return QString("'%1'").arg(value.toString(SQL92_TIME_FORMAT)); }
   QString toDateTimeLiteral(const QDateTimeEx& value) const override { return QString("TO_TIMESTAMP('%1', 'YYYY-MM-DD HH24:MI:SS')").arg(value.toString(SQL92_DATETIME_FORMAT)); }
   QString toStringLiteral(QString value) const override { return QDbConnectionData::toStringLiteral(value); }
   QString toBinaryLiteral(const QByteArray& value) const override { return QString("'%1'").arg(QString::fromLatin1(value.toHex())); }

   QDbSchema::Database   databaseSchema() override { throw QNotImplementedException(); }
   QDbSchema::Synonym    synonymSchema(const QString& name) override { throw QNotImplementedException(); }
   QDbSchema::Table      tableSchema(const QString& name) override { if (_tableSchema.name == name) return _tableSchema; return QDbSchema::Table(); }
   void setTableSchema(QDbSchema::Table tableSchema) { _tableSchema = tableSchema; }
   QDbSchema::Function   functionSchema(const QString& name) override { throw QNotImplementedException(); }
   QDbSchema::Package    packageSchema(const QString& name) override { throw QNotImplementedException(); }

protected:
   QDbCommandEngine* newEngine() override { throw QNotImplementedException(); }

private:
   QString _dataSourceName;
   QString _falseString;
   QString _trueString;
   QDbSchema::Table _tableSchema;
};

class MockSqlServerConnection : public QDbConnectionData
{
   Q_DISABLE_COPY(MockSqlServerConnection)

public:
   MockSqlServerConnection(const QString& dataSourceName) : _dataSourceName(dataSourceName) {}
   ~MockSqlServerConnection() override = default;

   bool isValid() const override { return false; }

   QString  connectionString() const override { return _dataSourceName; }
   QString  dataSourceName() const override { return _dataSourceName; }
   QString  driverName() const override { return "ODBC Driver 17 for SQL Server"; }
   SqlStyle sqlStyle() const override { return SqlStyle::SqlServer; }
   QDbConnection::PhType phType() const override { return QDbConnection::PHT_UnnamedOnly; }

   QDbEnvironment* environment() override { return &theTestEnvironment; }

   bool beginTransaction() override { return true; }
   bool rollbackTransaction() override { return true; }
   bool commitTransaction() override { return true; }

   bool abortExecution() override { return false; }

   bool tableExists(const QString& tableName) override { return true; }

   QChar phIndicator() const override { return u':'; }

   QString toBooleanLiteral(bool value) const override { return value ? "1" : "0"; }
   QString toDateLiteral(const QDateEx& value) const override { return QString("{d '%1'}").arg(value.toString(SQL92_DATE_FORMAT)); }
   QString toTimeLiteral(const QTimeEx& value) const override { return QString("{t '%1'}").arg(value.toString(SQL92_TIME_FORMAT)); }
   QString toDateTimeLiteral(const QDateTimeEx& value) const override { return QString("{ts '%1'}").arg(value.toString(SQL92_DATETIME_FORMAT)); }
   QString toStringLiteral(QString value) const override { return QDbConnectionData::toStringLiteral(value.replace("\\\r\n", "\\\\\r\n\r\n").replace("\\\n", "\\\\\n\n")); }
   QString toBinaryLiteral(const QByteArray& value) const override { return QString("'0x%1'").arg(QString::fromLatin1(value.toHex())); }

   QDbSchema::Database   databaseSchema() override { throw QNotImplementedException(); }
   QDbSchema::Synonym    synonymSchema(const QString& name) override { throw QNotImplementedException(); }
   QDbSchema::Table      tableSchema(const QString& name) override { if (_tableSchema.name == name) return _tableSchema; return QDbSchema::Table(); }
   void setTableSchema(QDbSchema::Table tableSchema) { _tableSchema = tableSchema; }
   QDbSchema::Function   functionSchema(const QString& name) override { throw QNotImplementedException(); }
   QDbSchema::Package    packageSchema(const QString& name) override { throw QNotImplementedException(); }

protected:
   QDbCommandEngine* newEngine() override { throw QNotImplementedException(); }

private:
   QString _dataSourceName;
   QDbSchema::Table _tableSchema;
};

static QDbConnection createSQLiteConnection()
{
   const auto newConnection = (QDbConnectionData *(*)(const QString&))QLibrary::resolve("QtSQLiteConnector", "newSQLiteConnection");

   if (!newConnection)
      return QDbConnection();

   return newConnection(":memory:");
}

static QString getSqlStatement(const QDbConnection& connection, const QDbAbstractCommand& command)
{
   return command.createSqlStatements(QDbSqlContext(connection, QDbSqlContext::PhUsage::Required)).first().sqlStatement();
}

void TestCases::init()
{
   TestFramework::init();

   if (dataDir().isEmpty())
   {
      addWarning(tr("Missing data folder"));
      return;
   }

   if (!QFile::exists(QDir(dataDir()).filePath("OracleCredentials.ini")))
   {
      addWarning(tr("Missing Oracle credentials"));
   }

   if (!QFile::exists(QDir(dataDir()).filePath("SqlServerCredentials.ini")))
   {
      addWarning(tr("Missing SqlServer credentials"));
   }

   if (!QFile::exists(QDir(dataDir()).filePath("DB2Credentials.ini")))
   {
      addWarning(tr("Missing DB2 credentials"));
   }
}

void TestCases::deinit()
{
   TestFramework::deinit();
}

void TestCases::primaryTests()
{
   _primaryTests(createSQLiteConnection());
   _primaryTests(createSqlServerConnection());
   _primaryTests(createOracleConnection());
#ifdef Q_PROCESSOR_X86_32
   _primaryTests(createSybaseConnection());
#endif
   _primaryTests(createDb2Connection());
}

void TestCases::postLobTestOracle()
{
   auto connection = createOracleConnection();

   if (!connection.isValid())
   {
      auto mockConnection = new MockOracleConnection("TestOracle");

      QDbSchema::Table tableSchema;
      QDbSchema::Column col;

      tableSchema.name = "CR_TEST";
      col.name = "TEST2";
      col.type = DT_LARGE_STRING;
      tableSchema.columns.append(col);
      col.name = "TEST3";
      col.type = DT_LARGE_BINARY;
      tableSchema.columns.append(col);

      mockConnection->setTableSchema(tableSchema);

      connection = mockConnection;
   }

   QDbCommand query(connection);

#if 1 // CREATE_TEST_TABLE
   if (connection.isValid())
   {
      query.setStatement("create table cr_test ( test1 varchar2(4000 byte), test2 clob, test3 blob, test4 date )");
      T_ERROR_IF(!query.exec(), query.state().message());
   }
#endif

#if 1
   QDbTable table1("cr_test");
   auto inserter = table1.inserter();

   inserter.setUsePostLob(true);

   inserter.set(table1.column("test2"), QString(5000, u'o'));
   inserter.set(table1.column("test3"), QByteArray(5000, 'x'));
   inserter.set(table1.column("test1"), QString(4000, u'a'));
   inserter.set(table1.column("test4"), QDate(2014, 5, 12));

   auto expectedSql = "INSERT INTO CR_TEST (TEST2, TEST3, TEST1, TEST4) VALUES (EMPTY_CLOB(), EMPTY_BLOB(), :PH03, :PH04) RETURNING TEST2, TEST3 INTO :PH01, :PH02";
   QList<QDbParameter> phList;

   T_EQUALS(inserter.createSqlStatements(QDbSqlContext(connection)).first().sqlStatement(), expectedSql);

   if (connection.isValid())
   {
      T_ERROR_IF(!inserter.execute(connection), inserter.lastError());

      auto reader = table1.reader(connection);

      auto colTest1 = reader.select(table1.column("test1"));
      auto colTest2 = reader.select(table1.column("test2"));
      auto colTest3 = reader.select(table1.column("test3"));
      auto colTest4 = reader.select(table1.column("test4"));

      T_ERROR_IF(!reader.readNext(), reader.lastError());
      T_EQUALS(colTest1.value().toString(), QString(4000, u'a'));
      T_IS_TRUE(colTest2.value().toString() == QString(5000, u'o'));
      T_IS_TRUE(colTest3.value().toByteArray() == QByteArray(5000, 'x'));
      T_EQUALS_EX(colTest4.value().toDate(), QDate(2014, 5, 12));
   }
#endif

#if 1
   auto updater = table1.updater();

   updater.setUsePostLob(true);

   updater.set(table1.column("test2"), QString(5000, QChar(0xF6))); // ö
   updater.set(table1.column("test3"), QByteArray(5000, ' '));
   updater.set(table1.column("test4"), QDate(2018, 5, 12));

   updater.where(table1.column("test1") == QString(4000, u'a'));

   if (connection.isValid())
   {
      T_ERROR_IF(!updater.execute(connection), inserter.lastError());

      auto reader = table1.reader(connection);

      auto colTest1 = reader.select(table1.column("test1"));
      auto colTest2 = reader.select(table1.column("test2"));
      auto colTest3 = reader.select(table1.column("test3"));
      auto colTest4 = reader.select(table1.column("test4"));

      T_ERROR_IF(!reader.readNext(), reader.lastError());
      T_EQUALS(colTest1.value().toString(), QString(4000, u'a'));
      T_IS_TRUE(colTest2.value().toString() == QString(5000, QChar(0xF6))); // ö
      T_IS_TRUE(colTest3.value().toByteArray() == QByteArray(5000, ' '));
      T_EQUALS_EX(colTest4.value().toDate(), QDate(2018, 5, 12));
   }
#endif

#if 1 // DESTROY_TEST_TABLE
   if (connection.isValid())
   {
      query.setStatement(QString("drop table cr_test cascade constraints"));
      T_ERROR_IF(!query.exec(), query.state().message());
   }
#endif
}

void TestCases::joinTests()
{
   _joinTests(createSQLiteConnection());
   _joinTests(createSqlServerConnection());
   _joinTests(createOracleConnection());
#ifdef Q_PROCESSOR_X86_32
   //_joinTests(createSybaseConnection());
#endif
   //_joinTests(createDb2Connection());
}

void TestCases::columnJoinTests()
{
   //_columnJoinTests(createSQLiteConnection());
   //_columnJoinTests(createSqlServerConnection());
   _columnJoinTests(createOracleConnection());
#ifdef Q_PROCESSOR_X86_32
   _columnJoinTests(createSybaseConnection());
#endif
   _columnJoinTests(createDb2Connection());
}

void TestCases::emptyStringTests()
{
   _emptyStringTest(createOracleConnection());
}

void TestCases::sqlServerTest()
{
   auto connection = createSqlServerConnection();

   if (!connection.isValid())
      return;

   QDbCommand query(connection);

#if 1 // Create table cr_test
   query.setStatement("create table cr_test ( id int not null identity(1,1), name varchar(100) not null, number decimal(10), bool bit, some_date datetime )");
   T_ERROR_IF(!query.exec(), query.state().message());
#endif

#if 1 // Fill table cr_test

   QDbTable table1("cr_test");
   auto inserter = table1.inserter();

   inserter.set(table1.column("name"), "Jack");
   inserter.set(table1.column("number"), 20);
   inserter.set(table1.column("bool"), true);
   inserter.set(table1.column("some_date"), QDate(2014, 5, 12));

   const auto colId = inserter.get(table1.column("id"), DT_INT);

   T_ERROR_IF(!inserter.execute(connection), inserter.lastError());

   T_EQUALS(inserter.affectedRows(), 1);

   T_EQUALS(colId.value().toInt(), 1);

#endif

#if 1 // Update table cr_test

   auto updater = table1.updater().where(table1.column("name") == "Jack");

   updater.set(table1.column("number"), 42);
   updater.set(table1.column("some_date"), QDate(2018, 5, 12));

   //const auto colNumber = updater.get(table1.column("number"), DT_INT);

   T_ERROR_IF(!updater.execute(connection), updater.lastError());

   T_EQUALS(updater.affectedRows(), 1);

   //T_EQUALS(colNumber.value().toInt(), 42);

#endif

#if 1 // Check content of table cr_test

   auto reader = table1.reader(connection).where(table1.column("id") == 1);

   auto colName = reader.select(table1.column("name"));
   auto colNumber = reader.select(table1.column("number"));
   auto colBool = reader.select(table1.column("bool"));
   auto colSomeDate = reader.select(table1.column("some_date"));

   T_ERROR_IF(!reader.execute(), reader.lastError());

   T_IS_TRUE(reader.readNext());

   T_EQUALS_EX(colName.value(), Variant("Jack"));
   T_EQUALS_EX(colNumber.value(), Variant(QDecimal(42)));
   T_EQUALS_EX(colBool.value(), Variant(true));
   T_EQUALS_EX(colSomeDate.value(), Variant(QDateTimeEx(2018, 5, 12, 0, 0, 0, 0)));

#endif

#if 1 // Destroy cr_test
   query.setStatement(QString("drop table cr_test"));
   T_ERROR_IF(!query.exec(), query.state().message());
#endif
}

void TestCases::storedProcedureTest()
{
   addWarning("Stored procedure tests not implemented.");

   //auto connection = createSQLiteConnection();

   //T_ASSERT(connection.isValid());
}

void TestCases::triggerTestOracle()
{
   auto connection = createOracleConnection();

   if (!connection.isValid())
   {
      connection = new MockOracleConnection("TestOracle");
   }

   QDbTable testTable("cr_test_trigger");

   if (connection.isValid())
   {
      QDbCommand query(connection);

      query.setStatement("create table cr_test_trigger ( id number(10) not null, host_name varchar2(512 byte) not null )");
      T_ERROR_IF(!query.exec(), query.state().message());

      query.setStatement(R"(CREATE OR REPLACE TRIGGER TRG_INS_CR_TEST_TRIGGER
BEFORE INSERT ON CR_TEST_TRIGGER
FOR EACH ROW
BEGIN
   SELECT SYS_CONTEXT('USERENV','HOST') INTO :NEW.host_name FROM dual;
END;)");
      T_ERROR_IF(!query.exec(), query.state().message());
   }

   auto inserter = testTable.inserter();

   inserter.set(testTable.column("id"), 112);

   T_ORACLE_EQUALS(connection, inserter, "INSERT INTO CR_TEST_TRIGGER (ID) VALUES (112)")

   if (connection.isValid())
   {
      T_ERROR_IF(!inserter.execute(connection), inserter.lastError());
   }

   auto reader = testTable.reader(connection);

   auto colId = reader.select(testTable.column("id"));
   auto colHost = reader.select(testTable.column("host_name"));

   T_EQUALS(getSqlStatement(connection, reader), "SELECT T1.ID, T1.HOST_NAME FROM CR_TEST_TRIGGER T1");

   if (connection.isValid())
   {
      T_ERROR_IF(!reader.readNext(), reader.lastError());
      T_EQUALS(colId.value().toInt(), 112);
      T_IS_TRUE(!colHost.value().toString().isEmpty());
   }

   inserter = testTable.inserter();

   inserter.set(testTable.column("id"), 112);

   colHost = inserter.get(testTable.column("host_name"), DT_STRING);

   T_ORACLE_EQUALS(connection, inserter, "INSERT INTO CR_TEST_TRIGGER (ID) VALUES (112) RETURNING HOST_NAME INTO :PH01")

   if (connection.isValid())
   {
      T_ERROR_IF(!inserter.execute(connection), inserter.lastError());
      T_IS_TRUE(!colHost.value().toString().isEmpty());

      QDbCommand query(connection);

      query.setStatement(QString("drop table cr_test_trigger cascade constraints"));
      T_ERROR_IF(!query.exec(), query.state().message());
   }
}

void TestCases::serializationTests()
{
   // Write and read XML
   addWarning("Serialization tests not implemented.");
}

void TestCases::valueProxyTests()
{
   _valueProxyTests(createSQLiteConnection());
}

void TestCases::sqlGroupTests()
{
   _sqlGroupTests(createSQLiteConnection());
   _sqlGroupTests(createSqlServerConnection());
   _sqlGroupTests(createOracleConnection());
#ifdef Q_PROCESSOR_X86_32
   //_sqlGroupTests(createSybaseConnection());
#endif
   //_sqlGroupTests(createDb2Connection());
}

void TestCases::sqlInTests()
{
   _sqlInTests(createSQLiteConnection());
   _sqlInTests(createSqlServerConnection());
   _sqlInTests(createOracleConnection());
#ifdef Q_PROCESSOR_X86_32
   //_sqlInTests(createSybaseConnection());
#endif
   //_sqlInTests(createDb2Connection());
}

void TestCases::limitOffsetTests()
{
   _limitOffsetTests(createSQLiteConnection());
   _limitOffsetTests(createSqlServerConnection());
   _limitOffsetTests(createOracleConnection());
#ifdef Q_PROCESSOR_X86_32
   _limitOffsetTests(createSybaseConnection());
#endif
   _limitOffsetTests(createDb2Connection());
}

void TestCases::_primaryTests(const QDbConnection& connection)
{
   if (!connection.isValid())
      return;

   QDbCommand query(connection);

#if 1 // CREATE_TEST_TABLE
   if (connection.sqlStyle() == SqlStyle::SQLite)
      query.setStatement("create table cr_test ( ID INTEGER PRIMARY KEY AUTOINCREMENT, small_int int not null, name varchar(100) not null, zahl numeric(8), bool boolean, datum date )");
   else if (connection.sqlStyle() == SqlStyle::DB2)
      query.setStatement("create table cr_test ( ID INT NOT NULL GENERATED ALWAYS AS IDENTITY(START WITH 1 INCREMENT BY 1), small_int int not null, name varchar(100) not null, zahl numeric(8), bool boolean, datum date, PRIMARY KEY(id) )");
   else if (connection.sqlStyle() == SqlStyle::SqlServer)
      query.setStatement("create table cr_test ( ID INT NOT NULL IDENTITY(1,1) PRIMARY KEY, small_int smallint not null, name varchar(100) not null, zahl decimal(8), bool bit, datum datetime )");
   else if (connection.sqlStyle() == SqlStyle::Oracle)
      query.setStatement("create table cr_test (ID NUMBER PRIMARY KEY, SMALL_INT INT NOT NULL, NAME VARCHAR2(100) NOT NULL, ZAHL NUMBER(8), BOOL CHAR(1), DATUM DATE)");
   else if (connection.sqlStyle() == SqlStyle::Sybase)
      query.setStatement("create table cr_test ( ID INT NOT NULL GENERATED ALWAYS AS IDENTITY(START WITH 1 INCREMENT BY 1), small_int int not null, name varchar(100) not null, zahl numeric(8), bool boolean, datum date, PRIMARY KEY(id) )");

   if (!query.exec())
      addError(query.state().text());

   if (connection.sqlStyle() == SqlStyle::Oracle)
   {
      query.setStatement("CREATE SEQUENCE cr_test_seq START WITH 1 INCREMENT BY 1 NOCACHE");
      if (!query.exec())
         addError(query.state().text());

      query.setStatement(
         "CREATE OR REPLACE TRIGGER cr_test_bir BEFORE INSERT ON cr_test FOR EACH ROW WHEN (NEW.ID IS NULL) BEGIN SELECT cr_test_seq.NEXTVAL INTO :NEW.ID FROM DUAL; END;");
      if (!query.exec())
         addError(query.state().text());
   }
#endif

#if 1 // Fill test table
   QDbTable testTable("cr_test");

   // Insert with returning id

   auto inserter = testTable.inserter();

   inserter.set(testTable.column("small_int"), 1);
   inserter.set(testTable.column("name"), "Müller");
   inserter.set(testTable.column("zahl"), 123456);
   inserter.set(testTable.column("bool"), true);
   inserter.set(testTable.column("datum"), QDate(2014, 5, 12));

   auto colId = inserter.get(testTable.column("id"), DT_INT);

   T_SQLITE_EQUALS(connection, inserter, "INSERT INTO CR_TEST (SMALL_INT, NAME, ZAHL, BOOL, DATUM) VALUES (1, 'Müller', 123456, TRUE, '2014-05-12'); SELECT last_insert_rowid();")
   T_DB2_EQUALS(connection, inserter, "SELECT ID FROM FINAL TABLE (INSERT INTO CR_TEST (SMALL_INT, NAME, ZAHL, BOOL, DATUM) VALUES (1, 'Müller', 123456, 1, DATE '2014-05-12'))")
   T_SQLSERVER_EQUALS(connection, inserter, "INSERT INTO CR_TEST (SMALL_INT, NAME, ZAHL, BOOL, DATUM) OUTPUT INSERTED.[ID] VALUES (1, 'Müller', 123456, 1, {d '2014-05-12'})")
   T_ORACLE_EQUALS(connection, inserter, "INSERT INTO CR_TEST (SMALL_INT, NAME, ZAHL, BOOL, DATUM) VALUES (1, 'Müller', 123456, 'T', TO_DATE('2014-05-12', 'YYYY-MM-DD')) RETURNING ID INTO :PH01")

   T_ERROR_IF(!inserter.execute(connection), inserter.lastError());
   T_EQUALS(inserter.affectedRows(), 1);
   T_EQUALS(colId.value().toInt(), 1);

   // Repeat insert

   inserter.set(testTable.column("small_int"), 2);
   inserter.set(testTable.column("name"), "Mayer");
   inserter.set(testTable.column("zahl"), Variant::null);
   inserter.set(testTable.column("bool"), false);
   inserter.set(testTable.column("datum"), QDate(2018, 1, 6));

   T_SQLITE_EQUALS(connection, inserter, "INSERT INTO CR_TEST (SMALL_INT, NAME, ZAHL, BOOL, DATUM) VALUES (2, 'Mayer', NULL, FALSE, '2018-01-06'); SELECT last_insert_rowid();")
   T_DB2_EQUALS(connection, inserter, "SELECT ID FROM FINAL TABLE (INSERT INTO CR_TEST (SMALL_INT, NAME, ZAHL, BOOL, DATUM) VALUES (2, 'Mayer', NULL, 0, DATE '2018-01-06'))")
   T_SQLSERVER_EQUALS(connection, inserter, "INSERT INTO CR_TEST (SMALL_INT, NAME, ZAHL, BOOL, DATUM) OUTPUT INSERTED.[ID] VALUES (2, 'Mayer', NULL, 0, {d '2018-01-06'})")
   T_ORACLE_EQUALS(connection, inserter, "INSERT INTO CR_TEST (SMALL_INT, NAME, ZAHL, BOOL, DATUM) VALUES (2, 'Mayer', NULL, 'F', TO_DATE('2018-01-06', 'YYYY-MM-DD')) RETURNING ID INTO :PH01")

   T_ERROR_IF(!inserter.execute(connection), inserter.lastError());
   T_EQUALS(inserter.affectedRows(), 1);
   T_EQUALS(colId.value().toInt(), 2);

   // BulkInsert

   inserter = testTable.inserter();

   inserter.set(testTable.column("small_int"), 42);
   inserter.set(testTable.column("name"), "Max\nMaier");
   inserter.set(testTable.column("zahl"), 456789);
   inserter.set(testTable.column("bool"), 1);
   inserter.set(testTable.column("datum"), QDate(2020, 5, 12));

   inserter.stashRow();

   inserter.set(testTable.column("small_int"), 987);
   inserter.set(testTable.column("name"), "Maike\tMaier");
   inserter.set(testTable.column("zahl"), 321);
   inserter.set(testTable.column("bool"), 0);
   inserter.set(testTable.column("datum"), QDate(2020, 7, 12));

   inserter.stashRow();

   T_SQLITE_EQUALS(connection, inserter, "INSERT INTO CR_TEST (SMALL_INT, NAME, ZAHL, BOOL, DATUM) VALUES (42, 'Max\nMaier', 456789, 1, '2020-05-12'), (987, 'Maike\tMaier', 321, 0, '2020-07-12')")
   T_DB2_EQUALS(connection, inserter, "INSERT INTO CR_TEST (SMALL_INT, NAME, ZAHL, BOOL, DATUM) VALUES (42, 'Max'||CHR(10)||'Maier', 456789, 1, DATE '2020-05-12'), (987, 'Maike\tMaier', 321, 0, DATE '2020-07-12')")
   T_SQLSERVER_EQUALS(connection, inserter, "INSERT INTO CR_TEST (SMALL_INT, NAME, ZAHL, BOOL, DATUM) VALUES (42, 'Max\nMaier', 456789, 1, {d '2020-05-12'}), (987, 'Maike	Maier', 321, 0, {d '2020-07-12'})")
   T_ORACLE_EQUALS(connection, inserter, "INSERT ALL\nINTO CR_TEST (SMALL_INT, NAME, ZAHL, BOOL, DATUM) VALUES (42, 'Max\nMaier', 456789, 1, TO_DATE('2020-05-12', 'YYYY-MM-DD'))\nINTO CR_TEST (SMALL_INT, NAME, ZAHL, BOOL, DATUM) VALUES (987, 'Maike	Maier', 321, 0, TO_DATE('2020-07-12', 'YYYY-MM-DD'))\nSELECT 1 FROM DUAL")

   T_ERROR_IF(!inserter.execute(connection), inserter.lastError());
   T_EQUALS(inserter.affectedRows(), 2);
#endif

#if 1 // Ubdate test table
   auto updater = testTable.updater();

   updater.set(testTable.column("datum"), QDate(2016, 5, 2));
   updater.set(testTable.column("zahl"), testTable.column("small_int") + 20);

   updater.where(testTable.column("id") == 1);

   T_SQLITE_EQUALS(connection, updater, "UPDATE CR_TEST SET DATUM = '2016-05-02', ZAHL = SMALL_INT + 20 WHERE ID = 1")
   T_DB2_EQUALS(connection, updater, "UPDATE CR_TEST SET DATUM = DATE '2016-05-02', ZAHL = SMALL_INT + 20 WHERE ID = 1")
   T_SQLSERVER_EQUALS(connection, updater, "UPDATE CR_TEST SET DATUM = {d '2016-05-02'}, ZAHL = SMALL_INT + 20 WHERE ID = 1")
   T_ORACLE_EQUALS(connection, updater, "UPDATE CR_TEST SET DATUM = TO_DATE('2016-05-02', 'YYYY-MM-DD'), ZAHL = SMALL_INT + 20 WHERE ID = 1")

   T_ERROR_IF(!updater.execute(connection), updater.lastError());
   T_EQUALS(updater.affectedRows(), 1);
#endif

#if 1 // Read test table
   auto reader = testTable.reader(connection).where(testTable.column("id").in(QList<Variant>() << 1 << 2) && testTable.column("name") == "Müller" && testTable.column("datum") == QDate(2016, 5, 2));

   QDbTypedColumn<QString> colName = testTable.column("name");
   QDbTypedColumn<QDate> colDate = testTable.column("datum");

   auto valueName = reader.select(colName);
   auto valueDate = reader.select(colDate);

   T_SQLITE_EQUALS(connection, reader, "SELECT T1.NAME, T1.DATUM FROM CR_TEST T1 WHERE T1.ID IN (1, 2) AND T1.NAME = 'Müller' AND T1.DATUM = '2016-05-02'")
   T_DB2_EQUALS(connection, reader, "SELECT T1.NAME, T1.DATUM FROM CR_TEST T1 WHERE T1.ID IN (1, 2) AND T1.NAME = 'Müller' AND T1.DATUM = DATE '2016-05-02'")
   T_SQLSERVER_EQUALS(connection, reader, "SELECT T1.NAME, T1.DATUM FROM CR_TEST T1 WHERE T1.ID IN (1, 2) AND T1.NAME = 'Müller' AND T1.DATUM = {d '2016-05-02'}")
   T_ORACLE_EQUALS(connection, reader, "SELECT T1.NAME, T1.DATUM FROM CR_TEST T1 WHERE T1.ID IN (1, 2) AND T1.NAME = 'Müller' AND T1.DATUM = TO_DATE('2016-05-02', 'YYYY-MM-DD')")

   T_ERROR_IF(!reader.readNext(), reader.lastError());
   T_EQUALS("Müller", valueName.value());
   T_EQUALS_EX(QDate(2016, 5, 2), valueDate.value());

   T_ERROR_IF(!reader.lastError().isEmpty(), reader.lastError());

   // Terminate the command so that the table can be deleted
   reader = QDbReader();
#endif

#if 1
   // Read constrained

   TblCrTest tblCrTest;
   QString name;
   nullable<int> zahl;
   QDateTimeEx date;
   qint64 id;

   reader = tblCrTest.reader(connection);
   reader.select(tblCrTest.colId()).into(&id);
   reader.select(tblCrTest.colName()).into(&name);
   reader.select(tblCrTest.colZahl()).into(&zahl);
   auto colBool = reader.select(tblCrTest.colBool());
   reader.select(tblCrTest.colDatum()).into(&date);

   reader.where(tblCrTest.colId().between(1, 3) && tblCrTest.colName() == "Mayer" && tblCrTest.colDatum() == QDate(2018, 1, 6) && tblCrTest.colZahl() == Variant::null);
   reader.orderBy(tblCrTest.colDatum());
   reader.orderBy(colBool);

   T_SQLITE_EQUALS(connection, reader, "SELECT T1.ID, T1.NAME, T1.ZAHL, T1.BOOL, T1.DATUM FROM CR_TEST T1 WHERE (T1.ID BETWEEN 1 AND 3) AND T1.NAME = 'Mayer' AND T1.DATUM = '2018-01-06' AND T1.ZAHL IS NULL ORDER BY T1.DATUM, T1.BOOL")
   T_DB2_EQUALS(connection, reader, "SELECT T1.ID, T1.NAME, T1.ZAHL, T1.BOOL, T1.DATUM FROM CR_TEST T1 WHERE (T1.ID BETWEEN 1 AND 3) AND T1.NAME = 'Mayer' AND T1.DATUM = DATE '2018-01-06' AND T1.ZAHL IS NULL ORDER BY T1.DATUM, T1.BOOL")
   T_SQLSERVER_EQUALS(connection, reader, "SELECT T1.ID, T1.NAME, T1.ZAHL, T1.BOOL, T1.DATUM FROM CR_TEST T1 WHERE (T1.ID BETWEEN 1 AND 3) AND T1.NAME = 'Mayer' AND T1.DATUM = {d '2018-01-06'} AND T1.ZAHL IS NULL ORDER BY T1.DATUM, T1.BOOL")
   T_ORACLE_EQUALS(connection, reader, "SELECT T1.ID, T1.NAME, T1.ZAHL, T1.BOOL, T1.DATUM FROM CR_TEST T1 WHERE (T1.ID BETWEEN 1 AND 3) AND T1.NAME = 'Mayer' AND T1.DATUM = TO_DATE('2018-01-06', 'YYYY-MM-DD') AND T1.ZAHL IS NULL ORDER BY T1.DATUM, T1.BOOL")

   T_IS_TRUE(reader.readNext());
   T_EQUALS(2ll, id);
   T_EQUALS("Mayer", name);
   T_IS_TRUE(zahl.isNull());
   if (connection.sqlStyle() == SqlStyle::SQLite)
      T_EQUALS_EX(colBool.boundValue()->value(), Variant(0LL));
   else if (connection.sqlStyle() == SqlStyle::Oracle)
      T_EQUALS_EX(colBool.boundValue()->value(), Variant("F"));
   else
      T_EQUALS_EX(colBool.boundValue()->value(), Variant(false));
   T_EQUALS_EX(QDate(2018, 1, 6), date);

   T_ERROR_IF(reader.hasError(), reader.lastError());

   T_IS_TRUE(!reader.readNext());

   reader.where(tblCrTest.colId().between(1, 3) && tblCrTest.colName() == "Müller" && tblCrTest.colDatum() == QDate(2016, 5, 2) && tblCrTest.colZahl() != Variant::null);
   reader.clearSortOrder();
   reader.orderBy(tblCrTest.colZahl(), QDb::OrderDescending);

   T_SQLITE_EQUALS(connection, reader, "SELECT T1.ID, T1.NAME, T1.ZAHL, T1.BOOL, T1.DATUM FROM CR_TEST T1 WHERE (T1.ID BETWEEN 1 AND 3) AND T1.NAME = 'Müller' AND T1.DATUM = '2016-05-02' AND T1.ZAHL IS NOT NULL ORDER BY T1.ZAHL DESC")
   T_DB2_EQUALS(connection, reader, "SELECT T1.ID, T1.NAME, T1.ZAHL, T1.BOOL, T1.DATUM FROM CR_TEST T1 WHERE (T1.ID BETWEEN 1 AND 3) AND T1.NAME = 'Müller' AND T1.DATUM = DATE '2016-05-02' AND T1.ZAHL IS NOT NULL ORDER BY T1.ZAHL DESC")
   T_SQLSERVER_EQUALS(connection, reader, "SELECT T1.ID, T1.NAME, T1.ZAHL, T1.BOOL, T1.DATUM FROM CR_TEST T1 WHERE (T1.ID BETWEEN 1 AND 3) AND T1.NAME = 'Müller' AND T1.DATUM = {d '2016-05-02'} AND T1.ZAHL IS NOT NULL ORDER BY T1.ZAHL DESC")
   T_ORACLE_EQUALS(connection, reader, "SELECT T1.ID, T1.NAME, T1.ZAHL, T1.BOOL, T1.DATUM FROM CR_TEST T1 WHERE (T1.ID BETWEEN 1 AND 3) AND T1.NAME = 'Müller' AND T1.DATUM = TO_DATE('2016-05-02', 'YYYY-MM-DD') AND T1.ZAHL IS NOT NULL ORDER BY T1.ZAHL DESC")

   T_IS_TRUE(reader.execute());
   T_IS_TRUE(reader.readNext());
   T_EQUALS(1ll, id);
   T_EQUALS("Müller", name);
   T_EQUALS(zahl.value(), 21);
   if (connection.sqlStyle() == SqlStyle::SQLite)
      T_EQUALS_EX(colBool.boundValue()->value(), Variant(1LL));
   else if (connection.sqlStyle() == SqlStyle::Oracle)
      T_EQUALS_EX(colBool.boundValue()->value(), Variant("T"));
   else
      T_EQUALS_EX(colBool.boundValue()->value(), Variant(true));
   T_EQUALS_EX(QDate(2016, 5, 2), date);

   T_ERROR_IF(reader.hasError(), reader.lastError());

   T_IS_TRUE(!reader.readNext());

   // Read all

   reader = tblCrTest.reader(connection);
   reader.select(tblCrTest.colId(), QDb::OrderAscending).into(&id);
   reader.select(tblCrTest.colName()).into(&name);
   reader.select(tblCrTest.colZahl()).into(&zahl);
   colBool = reader.select(tblCrTest.colBool());
   reader.select(tblCrTest.colDatum()).into(&date);

   reader.orderBy(tblCrTest.colSmallInt()); // Order outside select

   T_EQUALS(getSqlStatement(connection, reader), "SELECT T1.ID, T1.NAME, T1.ZAHL, T1.BOOL, T1.DATUM FROM CR_TEST T1 ORDER BY T1.SMALL_INT, T1.ID");

   T_IS_TRUE(reader.readNext());
   T_EQUALS(1ll, id);
   T_EQUALS("Müller", name);
   T_EQUALS(zahl.value(), 21);
   T_IS_TRUE(colBool.value());
   T_EQUALS_EX(QDate(2016, 5, 2), date);

   T_ERROR_IF(!reader.lastError().isEmpty(), reader.lastError());

   T_IS_TRUE(reader.readNext());
   T_EQUALS(2ll, id);
   T_EQUALS("Mayer", name);
   T_IS_TRUE(zahl.isNull());
   T_IS_TRUE(!colBool.value());
   T_EQUALS_EX(QDate(2018, 1, 6), date);

   T_ERROR_IF(!reader.lastError().isEmpty(), reader.lastError());

   T_IS_TRUE(reader.readNext());
   T_EQUALS(3ll, id);
   T_EQUALS("Max\nMaier", name);
   T_EQUALS(zahl.value(), 456789);
   T_IS_TRUE(colBool.value());
   T_EQUALS_EX(QDate(2020, 5, 12), date);

   T_ERROR_IF(!reader.lastError().isEmpty(), reader.lastError());

   T_IS_TRUE(reader.readNext());
   T_EQUALS(4ll, id);
   T_EQUALS("Maike\tMaier", name);
   T_EQUALS(zahl.value(), 321);
   T_IS_TRUE(!colBool.value());
   T_EQUALS_EX(QDate(2020, 7, 12), date);

   T_ERROR_IF(!reader.lastError().isEmpty(), reader.lastError());

   T_IS_TRUE(!reader.readNext());

   // Terminate the command so that the table can be deleted
   reader = QDbReader();
#endif

#if 1 // Bulk update
   QDbValuesTable valuesTable;
   auto valuesIdColumn = valuesTable.column("ID");
   auto valuesZahlColumn = valuesTable.column("ZAHL");

   valuesTable.set(valuesIdColumn, 1);
   valuesTable.set(valuesZahlColumn, 135);

   valuesTable.stashRow();

   valuesTable.set(valuesIdColumn, 2);
   valuesTable.set(valuesZahlColumn, 246);

   updater = testTable.updater().from(valuesTable);

   updater.set(testTable.column("Zahl"), valuesZahlColumn);

   updater.where(testTable.column("id") == valuesIdColumn);

   T_SQLITE_EQUALS(connection, updater, "WITH T1(ID, ZAHL) AS (VALUES (1, 135), (2, 246)) UPDATE CR_TEST SET ZAHL = T1.ZAHL FROM T1 WHERE CR_TEST.ID = T1.ID")
   T_DB2_EQUALS(connection, updater, "WITH T1(ID, ZAHL) AS (VALUES (1, 135), (2, 246)) UPDATE CR_TEST SET CR_TEST.ZAHL = T1.ZAHL FROM T1 WHERE CR_TEST.ID = T1.ID")
   T_SQLSERVER_EQUALS(connection, updater, "WITH T1(ID, ZAHL) AS (SELECT ID, ZAHL FROM (VALUES (1, 135), (2, 246)) AS T1(ID, ZAHL)) UPDATE CR_TEST SET CR_TEST.ZAHL = T1.ZAHL FROM T1 WHERE CR_TEST.ID = T1.ID")
   T_ORACLE_EQUALS(connection, updater, "MERGE INTO CR_TEST USING (SELECT 1 AS ID, 135 AS ZAHL FROM dual UNION ALL\nSELECT 2 AS ID, 246 AS ZAHL FROM dual) T1 ON (CR_TEST.ID = T1.ID) WHEN MATCHED THEN UPDATE SET CR_TEST.ZAHL = T1.ZAHL")

   T_ERROR_IF(!updater.execute(connection), updater.lastError());
   T_EQUALS(updater.affectedRows(), 2);

   reader = tblCrTest.reader(connection);

   auto colZahl = reader.select(tblCrTest.colZahl());

   reader.orderBy(tblCrTest.colId());

   T_EQUALS(getSqlStatement(connection, reader), "SELECT T1.ZAHL FROM CR_TEST T1 ORDER BY T1.ID");

   T_IS_TRUE(reader.execute());

   T_IS_TRUE(reader.readNext());
   T_EQUALS(colZahl.value().toInt(), 135);
   T_ERROR_IF(reader.hasError(), reader.lastError());

   T_IS_TRUE(reader.readNext());
   T_EQUALS(colZahl.value().toInt(), 246);
   T_ERROR_IF(reader.hasError(), reader.lastError());

   // Terminate the command so that the table can be deleted
   reader = QDbReader();
#endif

#if 1 // Merge (update only)
   // Merge is not yet implemented for SQLite
   if (connection.sqlStyle() != SqlStyle::SQLite)
   {
      valuesTable.stashClear();

      valuesTable.set(valuesIdColumn, 1);
      valuesTable.set(valuesZahlColumn, 235);

      valuesTable.stashRow();

      valuesTable.set(valuesIdColumn, 2);
      valuesTable.set(valuesZahlColumn, 346);

      auto merger = testTable.merger().from(valuesTable);

      merger.set(testTable.column("Zahl"), valuesZahlColumn);

      merger.where(testTable.column("id") == valuesIdColumn);

      T_SQLITE_EQUALS(connection, merger, "MERGE INTO CR_TEST USING (VALUES (1, 235), (2, 346)) AS T1(ID, ZAHL) ON (CR_TEST.ID = T1.ID) WHEN MATCHED THEN UPDATE SET ZAHL = T1.ZAHL")
      T_DB2_EQUALS(connection, merger, "MERGE INTO CR_TEST USING (SELECT ID, ZAHL FROM (VALUES (1, 235), (2, 346)) AS T1(ID, ZAHL)) AS T1 ON (CR_TEST.ID = T1.ID) WHEN MATCHED THEN UPDATE SET CR_TEST.ZAHL = T1.ZAHL")
      T_SQLSERVER_EQUALS(connection, merger, "MERGE INTO CR_TEST USING (SELECT ID, ZAHL FROM (VALUES (1, 235), (2, 346)) AS T1(ID, ZAHL)) AS T1 ON (CR_TEST.ID = T1.ID) WHEN MATCHED THEN UPDATE SET CR_TEST.ZAHL = T1.ZAHL;")
      T_ORACLE_EQUALS(connection, merger, "MERGE INTO CR_TEST USING (SELECT 1 AS ID, 235 AS ZAHL FROM dual UNION ALL\nSELECT 2 AS ID, 346 AS ZAHL FROM dual) T1 ON (CR_TEST.ID = T1.ID) WHEN MATCHED THEN UPDATE SET CR_TEST.ZAHL = T1.ZAHL")

      T_ERROR_IF(!merger.execute(connection), merger.lastError());
      T_EQUALS(merger.affectedRows(), 2);

      reader = tblCrTest.reader(connection);

      colZahl = reader.select(tblCrTest.colZahl());

      reader.orderBy(tblCrTest.colId());

      T_EQUALS(getSqlStatement(connection, reader), "SELECT T1.ZAHL FROM CR_TEST T1 ORDER BY T1.ID");

      T_IS_TRUE(reader.execute());

      T_IS_TRUE(reader.readNext());
      T_EQUALS(colZahl.value().toInt(), 235);
      T_ERROR_IF(reader.hasError(), reader.lastError());

      T_IS_TRUE(reader.readNext());
      T_EQUALS(colZahl.value().toInt(), 346);
      T_ERROR_IF(reader.hasError(), reader.lastError());

      // Terminate the command so that the table can be deleted
      reader = QDbReader();
   }
#endif

#if 1 // CREATE REL_TABLE
   query.setStatement("create table rel_test ( id int not null, cr_id int not null )");
   if (!query.exec())
      addError(query.state().text());
#endif

   QDbTable relTable("rel_test");

#if 1 // Fill REL_TABLE
   inserter = relTable.inserter();

   inserter.set(relTable.column("id"), 1);
   inserter.set(relTable.column("cr_id"), 1);

   T_EQUALS(getSqlStatement(connection, inserter), "INSERT INTO REL_TEST (ID, CR_ID) VALUES (1, 1)");
   T_ERROR_IF(!inserter.execute(connection), inserter.lastError());

   inserter.set(relTable.column("id"), 2);
   inserter.set(relTable.column("cr_id"), 2);

   T_EQUALS(getSqlStatement(connection, inserter), "INSERT INTO REL_TEST (ID, CR_ID) VALUES (2, 2)");
   T_ERROR_IF(!inserter.execute(connection), inserter.lastError());
#endif

#if 1 // Subselect
   auto selector = relTable.selector();
   selector.where(relTable.column("id") == testTable.column("id"));

   reader = testTable.reader(connection);
   reader.where(testTable.column("id") == 1);

   auto idCount = reader.select(selector.asColumn(relTable.column("cr_id").count()), QDb::OrderArbitrary, "idcount");

   T_EQUALS(getSqlStatement(connection, reader), "SELECT (SELECT COUNT(T2.CR_ID) AS COL1 FROM REL_TEST T2 WHERE T2.ID = T1.ID) AS \"idcount\" FROM CR_TEST T1 WHERE T1.ID = 1");

   T_IS_TRUE(reader.readNext());
   T_EQUALS(idCount.value(), 1LL);

   T_ERROR_IF(!reader.lastError().isEmpty(), reader.lastError());

   // Terminate the command so that the table can be deleted
   selector = QDbSelector();
   reader = QDbReader();
#endif

#if 1 // Subselect from selector
   selector = testTable.selector().where(testTable.column("name") == "Müller");

   selector.addColumn(testTable.column("id"));

   reader = relTable.reader(connection).where(relTable.column("id") == selector.asColumn());

   colId = reader.select(relTable.column("cr_id"));

   T_EQUALS(getSqlStatement(connection, reader), "SELECT T1.CR_ID FROM REL_TEST T1 WHERE T1.ID = (SELECT T2.ID FROM CR_TEST T2 WHERE T2.NAME = 'Müller')");

   T_IS_TRUE(reader.readNext());
   T_EQUALS(colId.value().toInt(), 1);

   T_ERROR_IF(!reader.lastError().isEmpty(), reader.lastError());

   // Terminate the command so that the table can be deleted
   selector = QDbSelector();
   reader = QDbReader();
#endif

#if 1 // Selector as data source
   const auto currentDate = QDateEx::currentDate();

   selector = tblCrTest.selector().where(tblCrTest.colDatum() < currentDate);

   auto colSelId = selector.addColumn(tblCrTest.colId(), "sel id");

   reader = relTable.innerJoin(selector, relTable.column("id") == colSelId).reader(connection);

   colId = reader.select(relTable.column("cr_id"));
   auto valSelId = reader.select(colSelId, QDb::OrderAscending);

   T_SQLITE_ARG_EQUALS(connection, reader, currentDate.toString("yyyy-MM-dd"), R"(SELECT T1.CR_ID, T3."sel id" FROM REL_TEST T1 INNER JOIN (SELECT T2.ID AS "sel id" FROM CR_TEST T2 WHERE T2.DATUM < '%1') T3 ON T1.ID = T3."sel id" ORDER BY T3."sel id")")
   T_DB2_ARG_EQUALS(connection, reader, currentDate.toString("yyyy-MM-dd"), R"(SELECT T1.CR_ID, T3."sel id" FROM REL_TEST T1 INNER JOIN (SELECT T2.ID AS "sel id" FROM CR_TEST T2 WHERE T2.DATUM < DATE '%1') T3 ON T1.ID = T3."sel id" ORDER BY T3."sel id")")
   T_SQLSERVER_ARG_EQUALS(connection, reader, currentDate.toString("yyyy-MM-dd"), R"(SELECT T1.CR_ID, T3."sel id" FROM REL_TEST T1 INNER JOIN (SELECT T2.ID AS "sel id" FROM CR_TEST T2 WHERE T2.DATUM < {d '%1'}) T3 ON T1.ID = T3."sel id" ORDER BY T3."sel id")")
   T_ORACLE_ARG_EQUALS(connection, reader, currentDate.toString("yyyy-MM-dd"), R"(SELECT T1.CR_ID, T3."sel id" FROM REL_TEST T1 INNER JOIN (SELECT T2.ID AS "sel id" FROM CR_TEST T2 WHERE T2.DATUM < TO_DATE('%1', 'YYYY-MM-DD')) T3 ON T1.ID = T3."sel id" ORDER BY T3."sel id")")

   T_IS_TRUE(reader.readNext());
   T_EQUALS(colId.value().toInt(), 1);
   T_EQUALS_EX(valSelId.value(), QDecimal(1));

   T_ERROR_IF(!reader.lastError().isEmpty(), reader.lastError());

   // Terminate the command so that the table can be deleted
   reader = QDbReader();
   selector = QDbSelector();
#endif

#if 1 // Selector as data source with explicit aliases
   QDbTable relTableWithAlias("rel_test", "test_alias");

   selector = tblCrTest.selector("inline_table").where(tblCrTest.colDatum() < currentDate);

   colSelId = selector.addColumn(tblCrTest.colId(), "sel_id");

   reader = relTableWithAlias.innerJoin(selector, relTableWithAlias.column("id") == colSelId).reader(connection);

   colId = reader.select(relTableWithAlias.column("cr_id"));
   valSelId = reader.select(colSelId);

   T_SQLITE_ARG_EQUALS(connection, reader, currentDate.toString("yyyy-MM-dd"), R"(SELECT "test_alias".CR_ID, "inline_table"."sel_id" FROM REL_TEST "test_alias" INNER JOIN (SELECT T1.ID AS "sel_id" FROM CR_TEST T1 WHERE T1.DATUM < '%1') "inline_table" ON "test_alias".ID = "inline_table"."sel_id")")
   T_DB2_ARG_EQUALS(connection, reader, currentDate.toString("yyyy-MM-dd"), R"(SELECT "test_alias".CR_ID, "inline_table"."sel_id" FROM REL_TEST "test_alias" INNER JOIN (SELECT T1.ID AS "sel_id" FROM CR_TEST T1 WHERE T1.DATUM < DATE '%1') "inline_table" ON "test_alias".ID = "inline_table"."sel_id")")
   T_SQLSERVER_ARG_EQUALS(connection, reader, currentDate.toString("yyyy-MM-dd"), R"(SELECT "test_alias".CR_ID, "inline_table"."sel_id" FROM REL_TEST "test_alias" INNER JOIN (SELECT T1.ID AS "sel_id" FROM CR_TEST T1 WHERE T1.DATUM < {d '%1'}) "inline_table" ON "test_alias".ID = "inline_table"."sel_id")")
   T_ORACLE_ARG_EQUALS(connection, reader, currentDate.toString("yyyy-MM-dd"), R"(SELECT "test_alias".CR_ID, "inline_table"."sel_id" FROM REL_TEST "test_alias" INNER JOIN (SELECT T1.ID AS "sel_id" FROM CR_TEST T1 WHERE T1.DATUM < TO_DATE('%1', 'YYYY-MM-DD')) "inline_table" ON "test_alias".ID = "inline_table"."sel_id")")

   T_IS_TRUE(reader.readNext());
   T_EQUALS(colId.value().toInt(), 1);
   T_EQUALS_EX(valSelId.value(), QDecimal(1));

   T_ERROR_IF(!reader.lastError().isEmpty(), reader.lastError());

   // Terminate the command so that the table can be deleted
   reader = QDbReader();
   selector = QDbSelector();
#endif

#if 1 // Selector as data source
   selector = tblCrTest.selector().where(tblCrTest.colDatum() < currentDate);

   colSelId = selector.addColumn(tblCrTest.colId(), "sel_id");
   const auto colSelName = selector.addColumn(tblCrTest.colName());

   reader = relTable.innerJoin(selector, relTable.column("id") == colSelId).reader(connection);

   colId = reader.select(relTable.column("cr_id"));
   valSelId = reader.select(colSelId);
   valueName = reader.select(colSelName);

   T_SQLITE_ARG_EQUALS(connection, reader, currentDate.toString("yyyy-MM-dd"), R"(SELECT T1.CR_ID, T3."sel_id", T3.NAME FROM REL_TEST T1 INNER JOIN (SELECT T2.ID AS "sel_id", T2.NAME FROM CR_TEST T2 WHERE T2.DATUM < '%1') T3 ON T1.ID = T3."sel_id")")
   T_DB2_ARG_EQUALS(connection, reader, currentDate.toString("yyyy-MM-dd"), R"(SELECT T1.CR_ID, T3."sel_id", T3.NAME FROM REL_TEST T1 INNER JOIN (SELECT T2.ID AS "sel_id", T2.NAME FROM CR_TEST T2 WHERE T2.DATUM < DATE '%1') T3 ON T1.ID = T3."sel_id")")
   T_SQLSERVER_ARG_EQUALS(connection, reader, currentDate.toString("yyyy-MM-dd"), R"(SELECT T1.CR_ID, T3."sel_id", T3.NAME FROM REL_TEST T1 INNER JOIN (SELECT T2.ID AS "sel_id", T2.NAME FROM CR_TEST T2 WHERE T2.DATUM < {d '%1'}) T3 ON T1.ID = T3."sel_id")")
   T_ORACLE_ARG_EQUALS(connection, reader, currentDate.toString("yyyy-MM-dd"), R"(SELECT T1.CR_ID, T3."sel_id", T3.NAME FROM REL_TEST T1 INNER JOIN (SELECT T2.ID AS "sel_id", T2.NAME FROM CR_TEST T2 WHERE T2.DATUM < TO_DATE('%1', 'YYYY-MM-DD')) T3 ON T1.ID = T3."sel_id")")

   T_IS_TRUE(reader.readNext());
   T_EQUALS(colId.value().toInt(), 1);
   T_EQUALS_EX(valSelId.value(), QDecimal(1));
   T_EQUALS(valueName.value(), "Müller");

   T_ERROR_IF(!reader.lastError().isEmpty(), reader.lastError());

   // Terminate the command so that the table can be deleted
   reader = QDbReader();
   selector = QDbSelector();
#endif

#if 1 // Exists
   selector = relTable.selector();
   selector.where(relTable.column("id") == testTable.column("id"));

   reader = testTable.reader(connection);
   reader.where(testTable.column("id") == 1 && selector.exists(relTable.column("cr_id")));

   valueName = reader.select(colName);

   T_EQUALS(getSqlStatement(connection, reader), "SELECT T1.NAME FROM CR_TEST T1 WHERE T1.ID = 1 AND (EXISTS (SELECT T2.CR_ID FROM REL_TEST T2 WHERE T2.ID = T1.ID))");

   T_IS_TRUE(reader.readNext());
   T_EQUALS(valueName.value(), "Müller");

   T_ERROR_IF(!reader.lastError().isEmpty(), reader.lastError());

   // Terminate the command so that the table can be deleted
   selector = QDbSelector();
   reader = QDbReader();
#endif

#if 1 // Query on self
   QDbTable relTable2("rel_test");

   auto joinedTable = relTable.innerJoin(relTable2).on(relTable.column("id") == relTable2.column("cr_id"));
   reader = joinedTable.reader(connection);
   colId = reader.select(relTable.column("id"));
   reader.where(relTable2.column("id") == 1);

   T_EQUALS(getSqlStatement(connection, reader), "SELECT T1.ID FROM REL_TEST T1 INNER JOIN REL_TEST T2 ON T1.ID = T2.CR_ID WHERE T2.ID = 1");

   T_IS_TRUE(reader.readNext());
   T_EQUALS(colId.value().toInt(), 1);

   T_ERROR_IF(!reader.lastError().isEmpty(), reader.lastError());

   // Terminate the command so that the table can be deleted
   reader = QDbReader();
#endif

#if 1 // DESTROY REL_TABLE
   query.setStatement("drop table rel_test");
   if (!query.exec())
      addError(query.state().text());
#endif

#if 1 // DESTROY_TEST_TABLE
   query.setStatement("drop table cr_test");
   if (!query.exec())
      addError(query.state().text());
   if (connection.sqlStyle() == SqlStyle::Oracle)
   {
      query.setStatement("drop SEQUENCE cr_test_seq");
      if (!query.exec())
         addError(query.state().text());
   }
#endif
}

void TestCases::_joinTests(const QDbConnection& connection)
{
   if (!connection.isValid())
      return;

   QDbCommand query(connection);

   bool tablesError = false;

   query.setStatement("CREATE TABLE TEX_FORMAT_SETS ( SYSTEM_ID NUMERIC(8) NOT NULL, USER_ID NUMERIC(10), FST_ID NUMERIC(10) NOT NULL, FST_NAME VARCHAR(100), FST_DESCRIPTION VARCHAR(500), FST_ENABLED CHAR(1), FST_DEFAULT_FOR_ID NUMERIC(10), FST_LAST_CHANGE DATE )");
   if (!query.exec())
   {
      addError(query.state().text());
      tablesError = true;
   }

   query.setStatement("CREATE TABLE TEX_FORMAT_SETS_TEMPLATES ( SYSTEM_ID NUMERIC(8) NOT NULL, USER_ID NUMERIC(10), FSE_SET_ID NUMERIC(10) NOT NULL, FSE_FOR_ID NUMERIC(10) NOT NULL )");
   if (!query.exec())
   {
      addError(query.state().text());
      tablesError = true;
   }

   if (!tablesError)
   {
      QDbTable tblFormatSets("TEX_FORMAT_SETS");
      QDbTable tblFormatSetsTemplates("TEX_FORMAT_SETS_TEMPLATES");

      auto inserter = tblFormatSets.inserter();

      inserter.set(tblFormatSets.column("SYSTEM_ID"), 1000);
      inserter.set(tblFormatSets.column("FST_ID"), 1);

      if (!inserter.execute(connection))
         addError(inserter.lastError());

      inserter = tblFormatSetsTemplates.inserter();

      inserter.set(tblFormatSetsTemplates.column("SYSTEM_ID"), 1000);
      inserter.set(tblFormatSetsTemplates.column("FSE_SET_ID"), 1);
      inserter.set(tblFormatSetsTemplates.column("FSE_FOR_ID"), 10);

      T_EQUALS(getSqlStatement(connection, inserter), "INSERT INTO TEX_FORMAT_SETS_TEMPLATES (SYSTEM_ID, FSE_SET_ID, FSE_FOR_ID) VALUES (1000, 1, 10)");

      if (!inserter.execute(connection))
         addError(inserter.lastError());

      QDbReader reader(tblFormatSets.innerJoin(tblFormatSetsTemplates).on(tblFormatSets.column("FST_ID") == tblFormatSetsTemplates.column("FSE_SET_ID")));

      auto name = reader.get(tblFormatSets.column("FST_NAME"));
      auto forId = reader.get(tblFormatSetsTemplates.column("FSE_FOR_ID"));

      T_EQUALS(getSqlStatement(connection, reader), "SELECT T1.FST_NAME, T2.FSE_FOR_ID FROM TEX_FORMAT_SETS T1 INNER JOIN TEX_FORMAT_SETS_TEMPLATES T2 ON T1.FST_ID = T2.FSE_SET_ID");

      reader.execute(connection);

      while (reader.readNext())
      {
         auto i = name.value().toString();
         auto d = forId.value().toDecimal();
      }

      if (!reader.lastError().isEmpty())
         this->addError(reader.lastError());
   }

   query.setStatement("drop table TEX_FORMAT_SETS");
   if (!query.exec())
      addError(query.state().text());

   query.setStatement("drop table TEX_FORMAT_SETS_TEMPLATES");
   if (!query.exec())
      addError(query.state().text());
}

void TestCases::_columnJoinTests(const QDbConnection& connection)
{
   if (!connection.isValid())
      return;

   QDbCommand query(connection);

#if 1 // Create table cr_test
   if (connection.sqlStyle() == SqlStyle::Sybase)
      query.setStatement("create table cr_test ( id int not null, name varchar(100) not null, some_number numeric(10), bool bit, some_date date, primary key (id) )");
   else if (connection.sqlStyle() == SqlStyle::Oracle)
      query.setStatement("create table cr_test ( id integer not null, name varchar2(100) not null, some_number number(10), bool char(1), some_date date, primary key (id) )");
   else if (connection.sqlStyle() == SqlStyle::DB2)
      query.setStatement("create table cr_test ( id int not null, name varchar(100) not null, some_number numeric(10), bool boolean, some_date date, primary key (id) )");

   T_ERROR_IF(!query.exec(), query.state().message());
#endif

#if 1 // Fill table cr_test
   QDbTable table1("cr_test");
   auto inserter = table1.inserter();

   inserter.set(table1.column("id"), 1);
   inserter.set(table1.column("name"), "Jack");
   inserter.set(table1.column("some_number"), 20);
   inserter.set(table1.column("bool"), true);
   inserter.set(table1.column("some_date"), QDate(2014, 5, 12));

   T_ORACLE_EQUALS(connection, inserter, "INSERT INTO CR_TEST (ID, NAME, SOME_NUMBER, BOOL, SOME_DATE) VALUES (1, 'Jack', 20, 'T', TO_DATE('2014-05-12', 'YYYY-MM-DD'))")

   T_ERROR_IF(!inserter.execute(connection), inserter.lastError());
#endif

   // TODO: Test update

#if 1 // Check table cr_test
   auto reader = table1.reader(connection);

   auto tbl2Col = reader.select(table1.column("name"));
   auto colBool = reader.select(table1.column("bool"));
   reader.where(table1.column("id") == 1);

   auto testSql = getSqlStatement(connection, reader);
   auto expectedSql = "SELECT T1.NAME, T1.BOOL FROM CR_TEST T1 WHERE T1.ID = 1";

   T_EQUALS(testSql, expectedSql);
   T_ERROR_IF(!reader.execute(), reader.lastError());
   T_IS_TRUE(reader.readNext());
   T_EQUALS(tbl2Col.value().toString(), QString("Jack"));
   T_IS_TRUE(colBool.value().toBool());
#endif

   // Create table rel_test
   query.setStatement("create table rel_test ( id int not null, cr_id int not null, rel_date date )");
   T_ERROR_IF(!query.exec(), query.state().text());

   QDbTable table2("rel_test");

   // Fill table rel_test
   inserter = table2.inserter()
      .set(table2.column("id"), 1)
      .set(table2.column("cr_id"), 1)
      .set(table2.column("rel_date"), QDate(2018, 07, 20));

   T_ORACLE_EQUALS(connection, inserter, "INSERT INTO REL_TEST (ID, CR_ID, REL_DATE) VALUES (1, 1, TO_DATE('2018-07-20', 'YYYY-MM-DD'))")
   T_ERROR_IF(!inserter.execute(connection), inserter.state().text());

   inserter = table2.inserter()
      .set(table2.column("id"), 2)
      .set(table2.column("cr_id"), 2)
      .set(table2.column("rel_date"), QDate(2014, 05, 12));

   T_ORACLE_EQUALS(connection, inserter, "INSERT INTO REL_TEST (ID, CR_ID, REL_DATE) VALUES (2, 2, TO_DATE('2014-05-12', 'YYYY-MM-DD'))")
   T_ERROR_IF(!inserter.execute(connection), inserter.state().text());

#if 1 // Read left join
   auto joinTable = table1 * table2;

   reader = joinTable.reader(connection);
   reader.where(table1.column("id") >>= table2.column("id"));

   auto colCrId = reader.select(table2.column("cr_id"));

   testSql = getSqlStatement(connection, reader);
   if (connection.sqlStyle() == SqlStyle::Sybase)
      expectedSql = "SELECT T2.CR_ID FROM CR_TEST T1, REL_TEST T2 WHERE T1.ID *= T2.ID";
   else
      expectedSql = "SELECT T2.CR_ID FROM CR_TEST T1, REL_TEST T2 WHERE T1.ID = T2.ID(+)";

   T_EQUALS(testSql, expectedSql);
   T_ERROR_IF(!reader.execute(), reader.lastError());
   T_IS_TRUE(reader.readNext());
   T_EQUALS(colCrId.value().toInt(), 1);
   T_IS_TRUE(!reader.readNext());
#endif

#if 1 // Read left join with expression
   reader = joinTable.reader(connection);
   reader.where(table1.column("id") + 1 >>= table2.column("id"));

   colCrId = reader.select(table2.column("cr_id"));

   testSql = getSqlStatement(connection, reader);
   if (connection.sqlStyle() == SqlStyle::Sybase)
      expectedSql = "SELECT T2.CR_ID FROM CR_TEST T1, REL_TEST T2 WHERE T1.ID + 1 *= T2.ID";
   else
      expectedSql = "SELECT T2.CR_ID FROM CR_TEST T1, REL_TEST T2 WHERE T1.ID + 1 = T2.ID(+)";

   T_EQUALS(testSql, expectedSql);
   T_ERROR_IF(!reader.execute(), reader.lastError());
   T_IS_TRUE(reader.readNext());
   T_EQUALS(colCrId.value().toInt(), 2);
   T_IS_TRUE(!reader.readNext());
#endif

#if 1 // Read right join
   reader = joinTable.reader(connection);
   reader.where(table1.column("some_date") <<= table2.column("rel_date"));

   colCrId = reader.select(table2.column("cr_id"), QDb::OrderAscending);
   tbl2Col = reader.select(table1.column("name"));

   testSql = getSqlStatement(connection, reader);
   if (connection.sqlStyle() == SqlStyle::Sybase)
      expectedSql = "SELECT T2.CR_ID, T1.NAME FROM CR_TEST T1, REL_TEST T2 WHERE T1.SOME_DATE =* T2.REL_DATE ORDER BY T2.CR_ID";
   else
      expectedSql = "SELECT T2.CR_ID, T1.NAME FROM CR_TEST T1, REL_TEST T2 WHERE T1.SOME_DATE(+) = T2.REL_DATE ORDER BY T2.CR_ID";

   T_EQUALS(testSql, expectedSql);
   T_ERROR_IF(!reader.execute(), reader.lastError());
   T_IS_TRUE(reader.readNext());
   T_EQUALS(colCrId.value().toInt(), 1);
   T_IS_TRUE(tbl2Col.value().isNull());
   T_IS_TRUE(reader.readNext());
   T_EQUALS(colCrId.value().toInt(), 2);
   T_EQUALS(tbl2Col.value().toString(), QString("Jack"));
   T_IS_TRUE(!reader.readNext());
#endif

#if 1 // Read right join with constant
   reader = joinTable.reader(connection);
   reader.where((table1.column("id") <<= table2.column("id")) && (table1.column("some_date") <<= QDateEx(2014, 5, 12)));

   colCrId = reader.select(table2.column("cr_id"), QDb::OrderAscending);
   tbl2Col = reader.select(table1.column("name"));

   testSql = getSqlStatement(connection, reader);
   if (connection.sqlStyle() == SqlStyle::Sybase)
      expectedSql = "SELECT T2.CR_ID, T1.NAME FROM CR_TEST T1, REL_TEST T2 WHERE T1.ID =* T2.ID AND T1.SOME_DATE =* {d '2014-05-12'} ORDER BY T2.CR_ID";
   else if (connection.sqlStyle() == SqlStyle::Oracle)
      expectedSql = "SELECT T2.CR_ID, T1.NAME FROM CR_TEST T1, REL_TEST T2 WHERE T1.ID(+) = T2.ID AND T1.SOME_DATE(+) = TO_DATE('2014-05-12', 'YYYY-MM-DD') ORDER BY T2.CR_ID";
   else if (connection.sqlStyle() == SqlStyle::DB2)
      expectedSql = "SELECT T2.CR_ID, T1.NAME FROM CR_TEST T1, REL_TEST T2 WHERE T1.ID(+) = T2.ID AND T1.SOME_DATE(+) = DATE '2014-05-12' ORDER BY T2.CR_ID";

   T_EQUALS(testSql, expectedSql);
   if (connection.sqlStyle() == SqlStyle::Sybase)
   {
      T_IS_TRUE(!reader.execute());
      T_EQUALS(reader.lastError(), "42000:1:-680:[Sybase][ODBC Driver][Adaptive Server Anywhere]Syntaxfehler oder Zugriffsverletzung: Ungültiger Ausdruck in WHERE-Klausel des Transact-SQL-Outer-Joins");
   }
   else
   {
      T_ERROR_IF(!reader.execute(), reader.state().message());
   }
#endif

#if 1 // Read right join with expression
   reader = joinTable.reader(connection);
   reader.where(table1.column("some_date") <<= table2.column("rel_date") + 1);

   colCrId = reader.select(table2.column("cr_id"), QDb::OrderAscending);
   tbl2Col = reader.select(table1.column("name"));

   testSql = getSqlStatement(connection, reader);
   if (connection.sqlStyle() == SqlStyle::Sybase)
      expectedSql = "SELECT T2.CR_ID, T1.NAME FROM CR_TEST T1, REL_TEST T2 WHERE T1.SOME_DATE =* T2.REL_DATE + 1 ORDER BY T2.CR_ID";
   else
      expectedSql = "SELECT T2.CR_ID, T1.NAME FROM CR_TEST T1, REL_TEST T2 WHERE T1.SOME_DATE(+) = T2.REL_DATE + 1 ORDER BY T2.CR_ID";

   T_EQUALS(testSql, expectedSql);
   T_ERROR_IF(!reader.execute(), reader.lastError());
   T_IS_TRUE(reader.readNext());
   T_EQUALS(colCrId.value().toInt(), 1);
   T_IS_TRUE(tbl2Col.value().isNull());
   T_IS_TRUE(reader.readNext());
   T_EQUALS(colCrId.value().toInt(), 2);
   T_IS_TRUE(tbl2Col.value().isNull());
   T_IS_TRUE(!reader.readNext());
#endif

#if 1 // Destroy rel_test
   query.setStatement("drop table rel_test");
   T_ERROR_IF(!query.exec(), query.state().message());
#endif

#if 1 // Destroy cr_oci_test
   query.setStatement(QString("drop table cr_test"));
   T_ERROR_IF(!query.exec(), query.state().message());
#endif
}

void TestCases::_emptyStringTest(const QDbConnection& connection)
{
   if (!connection.isValid())
      return;

   //connection.setTreatEmptyAsNull(true);

   QDbCommand query(connection);

#if 1 // Create table cr_test
   query.setStatement("create table cr_test ( id numeric(8), text varchar2(100 byte) )");
   T_ERROR_IF(!query.exec(), query.state().message());
#endif

#if 1 // Fill table cr_test
   QDbTable table1("cr_test");
   auto inserter = table1.inserter();

   inserter.set(table1.column("id"), 1);
   inserter.set(table1.column("text"), "");

   T_EQUALS(getSqlStatement(connection, inserter), QString("INSERT INTO CR_TEST (ID, TEXT) VALUES (1, '')"));

   T_ERROR_IF(!inserter.execute(connection), inserter.lastError());
#endif

#if 1
   // Test on empty string is not null

   auto reader = table1.reader(connection);
   auto colId = reader.select(table1.column("id"));

   reader.where(table1.column("text") == "" || table1.column("text") == QString(""));

   T_EQUALS(getSqlStatement(connection, reader), QString("SELECT T1.ID FROM CR_TEST T1 WHERE T1.TEXT = '' OR T1.TEXT = ''"));

   T_IS_TRUE(reader.execute());
   T_IS_TRUE(!reader.readNext());

   // Test on IS NULL

   reader = table1.reader(connection);
   colId = reader.select(table1.column("id"));

   reader.where(table1.column("text") == Variant() && table1.column("text") == QString());

   T_EQUALS(getSqlStatement(connection, reader), QString("SELECT T1.ID FROM CR_TEST T1 WHERE T1.TEXT IS NULL AND T1.TEXT IS NULL"));

   T_IS_TRUE(reader.execute());
   T_IS_TRUE(reader.readNext());
   T_EQUALS(colId.value().toInt(), 1);
#endif

#if 1 // Destroy cr_test
   query.setStatement(QString("drop table cr_test cascade constraints"));
   T_ERROR_IF(!query.exec(), query.state().message());
#endif
}

void TestCases::_valueProxyTests(const QDbConnection& connection)
{
   if (!connection.isValid())
      return;

   QDbCommand query(connection);

#if 1 // CREATE_TEST_TABLE
   query.setStatement("create table cr_test ( id int not null, name varchar(100) not null, zahl numeric(8), datum date, primary key (id) )");
   if (!query.exec())
   {
      addError(query.state().text());
      return;
   }
#endif

#if 1 // Fill test table
   QDbTable testTable("cr_test");

   auto inserter = testTable.inserter();

   inserter.set(testTable.column("id"), 1);
   inserter.set(testTable.column("name"), "Müller");
   inserter.set(testTable.column("zahl"), 123456);
   inserter.set(testTable.column("datum"), QDate(2014, 5, 12));

   if (!inserter.execute(connection))
      addError(inserter.lastError());
#endif

#if 1 // Read test table
   auto reader = testTable.typedReader<TestElement>();

   reader.map(testTable.typedColumn<int>("id"), [](TestElement* element, int id) { element->setNumber(id); });
   reader.map(testTable.typedColumn<QString>("name"), &TestElement::setName);
   reader.map(testTable.column("datum"), &TestElement::setValue);
   reader.map(testTable.typedColumn<int>("id"), testTable.typedColumn<int>("id"), [](TestElement* element, int id, int id2) { element->_num1 = id; element->_num2 = id2; });
   reader.map(testTable.typedColumn<int>("id"), testTable.typedColumn<int>("id"), [this](TestElement* element, int id, int id2) { element->setNumber(id); element->setNumber(id2); element->_test = dataDir(); });

   auto result = reader.readAll(connection);

   T_EQUALS(result.count(), 1);
   if (!result.isEmpty())
   {
      T_EQUALS(result.first().number(), 1);
      T_EQUALS(result.first().name(), "Müller");
      T_EQUALS_EX(result.first().value(), Variant("2014-05-12"));
      T_EQUALS(result.first()._num1, 1);
      T_EQUALS(result.first()._num2, 1);
      T_EQUALS(result.first()._test, dataDir());
   }
#endif

#if 1 // DESTROY_TEST_TABLE
   query.setStatement("drop table cr_test");
   if (!query.exec())
      addError(query.state().text());
#endif
}

void TestCases::_sqlGroupTests(const QDbConnection& connection)
{
   if (!connection.isValid())
      return;

   QDbCommand query(connection);

#if 1 // CREATE_TEST_TABLE
   query.setStatement("create table cr_test ( id int not null, amount numeric(8), pieces numeric(8), datum date, primary key (id) )");
   if (!query.exec())
      addError(query.state().text());
#endif

#if 1 // Fill test table
   QDbTable testTable("cr_test");

   auto inserter = testTable.inserter();

   inserter.set(testTable.column("id"), 1);
   inserter.set(testTable.column("amount"), 1000);
   auto boundValue = inserter.set(testTable.column("pieces"));
   inserter.set(testTable.column("datum"), QDate(2014, 5, 12));

   boundValue->setValue(6);

   if (!inserter.execute(connection))
      addError(inserter.lastError());

   inserter = testTable.inserter();

   inserter.set(testTable.column("id"), 2);
   inserter.set(testTable.column("amount"), 1000);
   inserter.set(testTable.column("pieces"), 4);
   inserter.set(testTable.column("datum"), QDate(2014, 5, 12));

   if (!inserter.execute(connection))
      addError(inserter.lastError());
#endif

#if 1 // Read test table
   auto reader = testTable.reader(connection);

   QDbTypedColumn<QDate> colDate = testTable.column("datum");
   auto colSum1 = testTable.column("amount").sum();
   auto colSum2 = testTable.column("pieces").sum();

   auto fixValue = reader.select(QDbExpression(5));
   auto valueDate = reader.select(colDate);
   auto sum1 = reader.select(colSum1, QDb::OrderArbitrary, "COL1");
   auto sum2 = reader.select(colSum2, QDb::OrderArbitrary, "col5");
   auto rel = reader.select(colSum1 / colSum2, QDb::OrderAscending);

   reader.having(colSum1 == 2000);

   T_EQUALS(getSqlStatement(connection, reader), R"(SELECT 5, T1.DATUM, SUM(T1.AMOUNT) AS "COL1", SUM(T1.PIECES) AS "col5", SUM(T1.AMOUNT) / SUM(T1.PIECES) AS COL6 FROM CR_TEST T1 GROUP BY T1.DATUM HAVING SUM(T1.AMOUNT) = 2000 ORDER BY COL6)");

   if (reader.readNext())
   {
      T_EQUALS_EX(fixValue.value().toInt(), 5);
      T_EQUALS_EX(valueDate.value(), QDate(2014, 5, 12));
      T_EQUALS_EX(sum1.value().toDecimal(), QDecimal(2000));
      T_EQUALS_EX(sum2.value().toDecimal(), QDecimal(10));
      T_EQUALS_EX(rel.value().toInt(), 200);
   }

   if (!reader.lastError().isEmpty())
      this->addError(reader.lastError());

   // Terminate the command so that the table can be deleted
   reader = QDbReader();
#endif

   // TODO: Test groupable column only in having statement:
   // select dst_e_volumen, count(dst_skontro) from ihs_dstamm where dst_muendelsicher = 'T' group by dst_e_volumen, dst_xetra having dst_xetra = 'T';

#if 1 // DESTROY_TEST_TABLE
   query.setStatement("drop table cr_test");
   if (!query.exec())
      addError(query.state().text());
#endif
}

void TestCases::_sqlInTests(const QDbConnection& connection)
{
   if (!connection.isValid())
      return;

   QDbCommand query(connection);

   bool tablesError = false;

   query.setStatement("CREATE TABLE TEX_FORMAT_SETS ( SYSTEM_ID NUMERIC(8) NOT NULL, USER_ID NUMERIC(10), FST_ID NUMERIC(10) NOT NULL, FST_NAME VARCHAR(100), FST_DESCRIPTION VARCHAR(500), FST_ENABLED CHAR(1), FST_DEFAULT_FOR_ID NUMERIC(10), FST_LAST_CHANGE DATE )");
   if (!query.exec())
   {
      addError(query.state().text());
      tablesError = true;
   }

   query.setStatement("CREATE TABLE TEX_FORMAT_SETS_TEMPLATES ( SYSTEM_ID NUMERIC(8) NOT NULL, USER_ID NUMERIC(10), FSE_SET_ID NUMERIC(10) NOT NULL, FSE_FOR_ID NUMERIC(10) NOT NULL )");
   if (!query.exec())
   {
      addError(query.state().text());
      tablesError = true;
   }

   if (!tablesError)
   {
      QDbTable tblFormatSets("TEX_FORMAT_SETS");
      QDbTable tblFormatSetsTemplates("TEX_FORMAT_SETS_TEMPLATES");

      auto inserter = tblFormatSets.inserter();

      inserter.set(tblFormatSets.column("SYSTEM_ID"), 1000);
      inserter.set(tblFormatSets.column("FST_ID"), 1);

      if (!inserter.execute(connection))
         addError(inserter.lastError());

      inserter = tblFormatSetsTemplates.inserter();

      inserter.set(tblFormatSetsTemplates.column("SYSTEM_ID"), 1000);
      inserter.set(tblFormatSetsTemplates.column("FSE_SET_ID"), 1);
      inserter.set(tblFormatSetsTemplates.column("FSE_FOR_ID"), 10);

      if (!inserter.execute(connection))
         addError(inserter.lastError());

      // Test in subselect

      QDbReader reader(tblFormatSets);

      auto name = reader.get(tblFormatSets.column("FST_NAME"));

      reader.where(tblFormatSets.column("FST_ID").in(tblFormatSetsTemplates.selector("inline_table").asColumn(tblFormatSetsTemplates.column("FSE_SET_ID"))));

      T_EQUALS(getSqlStatement(connection, reader), "SELECT T1.FST_NAME FROM TEX_FORMAT_SETS T1 WHERE T1.FST_ID IN (SELECT T2.FSE_SET_ID FROM TEX_FORMAT_SETS_TEMPLATES T2)");

      reader.execute(connection);

      while (reader.readNext())
      {
         auto i = name.value().toString();
      }

      if (!reader.lastError().isEmpty())
         this->addError(reader.lastError());

      // Test in nothing

      reader = tblFormatSets.reader();

      name = reader.get(tblFormatSets.column("FST_NAME"));

      reader.where(tblFormatSets.column("FST_ID").in(QList<Variant>()));

      T_EQUALS(getSqlStatement(connection, reader), "SELECT T1.FST_NAME FROM TEX_FORMAT_SETS T1 WHERE 1 = 2");

      reader.execute(connection);

      T_IS_TRUE(!reader.readNext());

      if (!reader.lastError().isEmpty())
         this->addError(reader.lastError());

      // Test not in nothing

      reader = tblFormatSets.reader();

      name = reader.get(tblFormatSets.column("FST_NAME"));

      reader.where(tblFormatSets.column("FST_ID").notIn(QList<Variant>()));

      T_EQUALS(getSqlStatement(connection, reader), "SELECT T1.FST_NAME FROM TEX_FORMAT_SETS T1 WHERE 1 = 1");

      reader.execute(connection);

      T_IS_TRUE(reader.readNext());

      if (!reader.lastError().isEmpty())
         this->addError(reader.lastError());

      // Test in single value

      reader = tblFormatSets.reader();

      name = reader.get(tblFormatSets.column("FST_NAME"));

      reader.where(tblFormatSets.column("FST_ID").in(QList<Variant>() << 1));

      T_EQUALS(getSqlStatement(connection, reader), "SELECT T1.FST_NAME FROM TEX_FORMAT_SETS T1 WHERE T1.FST_ID = 1");

      reader.execute(connection);

      T_IS_TRUE(reader.readNext());

      if (!reader.lastError().isEmpty())
         this->addError(reader.lastError());
   }

   query.setStatement("drop table TEX_FORMAT_SETS");
   if (!query.exec())
      addError(query.state().text());

   query.setStatement("drop table TEX_FORMAT_SETS_TEMPLATES");
   if (!query.exec())
      addError(query.state().text());
}

void TestCases::_limitOffsetTests(QDbConnection connection)
{
   if (!connection.isValid())
      return;

   QDbCommand query(connection);

   query.setStatement("create table cr_test ( col_name varchar(100) not null, col_num numeric(8) )");

   T_ERROR_IF(!query.exec(), query.state().text());

   auto maxInsertRows = connection.maxRowsPerDmlCall();
   connection.setMaxRowsPerDmlCall(2);

   QDbTable testTable("cr_test");

   auto inserter = testTable.inserter();

   inserter.set(testTable.column("col_name").to("Max Mustermann"));
   inserter.set(testTable.column("col_num").to(1));
   inserter.stashRow();

   inserter.set(testTable.column("col_name").to("Sabine Mustermann"));
   inserter.set(testTable.column("col_num").to(2));
   inserter.stashRow();

   inserter.set(testTable.column("col_name").to("Anna Mustermann"));
   inserter.set(testTable.column("col_num").to(3));
   inserter.stashRow();

   inserter.set(testTable.column("col_name").to("Karl Mustermann"));
   inserter.set(testTable.column("col_num").to(4));

   T_ERROR_IF(!inserter.execute(connection), inserter.lastError());
   if (connection.sqlStyle() != SqlStyle::Sybase)
      T_EQUALS(inserter.affectedRows(), 4);

   connection.setMaxRowsPerDmlCall(maxInsertRows);

   auto reader = testTable.reader();

   if (connection.sqlStyle() == SqlStyle::Sybase)
      reader.limit(3);
   else
      reader.limit(2).offset(1);

   auto colName = reader.select(testTable.column("col_name"));
   auto colNumber = reader.select(testTable.column("col_num"), QDb::OrderAscending);

   T_ERROR_IF(!reader.execute(connection), reader.lastError());

   if (connection.sqlStyle() == SqlStyle::Sybase)
      reader.readNext();

   T_IS_TRUE(reader.readNext());

   T_EQUALS(colName.value().toString(), "Sabine Mustermann");
   T_EQUALS(colNumber.value().toInt(), 2);

   T_IS_TRUE(reader.readNext());

   T_EQUALS(colName.value().toString(), "Anna Mustermann");
   T_EQUALS(colNumber.value().toInt(), 3);

   T_IS_TRUE(!reader.readNext());

   reader = QDbReader();

   query.setStatement("drop table cr_test");
   T_ERROR_IF(!query.exec(), query.state().text());
}

QDbConnection TestCases::createOracleConnection()
{
   QFile credentialsFile(QDir(dataDir()).filePath("OracleCredentials.ini"));
   const QSettingsFile settings(&credentialsFile);

   const auto dbName = settings.value("Name").toString();
   const auto dbAdmin = settings.value("Admin/User").toString();
   const auto dbAdminPassword = settings.value("Admin/Password").toString();
   const auto dbUser = settings.value("User/User").toString();
   const auto dbUserPassword = settings.value("User/Password").toString();

   if (dbName.isEmpty())
   {
      addWarning(tr("Missing Oracle database credentials"));
      return QDbConnection();
   }

   T_IS_TRUE(!dbName.isEmpty());
   T_IS_TRUE(!dbAdmin.isEmpty());
   T_IS_TRUE(!dbAdminPassword.isEmpty());

   const auto newConnection = (QDbConnectionData *(*)(const QString &, const QString &, const QString &))QLibrary::resolve("QtOracleConnector", "newOracleConnection");

   T_ASSERT(newConnection);

   auto connection = newConnection(dbAdmin, dbAdminPassword, dbName);

   connection->_falseString = "F";
   connection->_trueString = "T";

   return connection;
}

QDbConnection TestCases::createSqlServerConnection()
{
   QFile credentialsFile(QDir(dataDir()).filePath("SqlServerCredentials.ini"));
   const QSettingsFile settings(&credentialsFile);

   const auto dbServer = settings.value("Host").toString();
   const auto dbName = settings.value("Database").toString();
   const auto dbUser = settings.value("User").toString();
   const auto dbPassword = settings.value("Password").toString();

   if (dbServer.isEmpty())
   {
      addWarning(tr("Missing SqlServer database credentials"));
      return QDbConnection();
   }

   QLibrary odbcLib("QtODBCConnector.dll");
   T_ASSERT(odbcLib.load());

   auto createSqlServerConnectionString = (void(*)(QString&, const QString&, const QString&, const QString&, const QString&, const QString&))odbcLib.resolve("createSqlServerConnectionString");
   auto createOdbcConnection = (QDbConnectionData* (*)(const QString&))odbcLib.resolve("newODBCConnection");

   T_ASSERT(createSqlServerConnectionString);
   T_ASSERT(createOdbcConnection);

   QString sqlServerConnectionString;
   createSqlServerConnectionString(sqlServerConnectionString, QString(), dbServer, dbUser, dbPassword, dbName);

   QDbConnection sqlServerConnection = createOdbcConnection(sqlServerConnectionString);

   if (sqlServerConnection.state().type() == QDbState::Error)
      throw TestException(sqlServerConnection.state().message());

   return sqlServerConnection;
}

#ifdef Q_PROCESSOR_X86_32
QDbConnection TestCases::createSybaseConnection()
{
   QLibrary odbcLib("QtODBCConnector.dll");
   T_ASSERT(odbcLib.load());

   auto createSybaseConnectionString = (void(*)(QString&, const QString&, const QString&, const QString&))odbcLib.resolve("createSybaseConnectionString");
   auto createOdbcConnection = (QDbConnectionData* (*)(const QString&))odbcLib.resolve("newODBCConnection");

   T_ASSERT(createSybaseConnectionString);
   T_ASSERT(createOdbcConnection);

   QString sybaseConnectionString;
   createSybaseConnectionString(sybaseConnectionString, "dba", "sql", "SybaseTest");

   QDbConnection sybaseConnection = createOdbcConnection(sybaseConnectionString);

   if (sybaseConnection.state().type() == QDbState::Error)
      throw TestException(sybaseConnection.state().message());

   //sybaseConnection.setServerCharacterEncoding(QTextCodec::codecForMib(2252));

   // Taken from the Rogue Wave implementation it may be good to supply small values in the sql command.
   // ASA5: Some expressions (notably the + operator) cause an error if there is a placeholder on the right side!
   sybaseConnection.setMaxLiteralStringLength(1024);

   return sybaseConnection;
}
#endif

QDbConnection TestCases::createDb2Connection()
{
   QFile credentialsFile(QDir(dataDir()).filePath("DB2Credentials.ini"));
   const QSettingsFile settings(&credentialsFile);

   const auto dbAlias = settings.value("Alias").toString();
   const auto dbHost = settings.value("Host").toString();
   const auto dbName = settings.value("Database").toString();
   const auto dbUser = settings.value("User").toString();
   const auto dbPassword = settings.value("Password").toString();
   const auto dbSchema = settings.value("Schema").toString();

   if (dbAlias.isEmpty() && dbHost.isEmpty())
   {
      addWarning(tr("Missing DB2 database credentials"));
      return QDbConnection();
   }

   QLibrary db2Lib("QtDB2Connector.dll");
   T_ASSERT(db2Lib.load());

   auto createDB2Connection = (QDbConnectionData* (*)(const QString&))db2Lib.resolve("newDB2Connection");
   T_ASSERT(createDB2Connection);

   QString db2ConnectionString;
   if (dbAlias.isEmpty())
   {
      auto createDB2ConnectionString = (void(*)(QString&, const QString&, const QString&, const QString&, const QString&, const QString&))db2Lib.resolve("createDB2ConnectionString");
      T_ASSERT(createDB2ConnectionString);

      createDB2ConnectionString(db2ConnectionString, dbName, dbHost, dbUser, dbPassword, dbSchema);
   }
   else
   {
      auto createDB2AliasConnectionString = (void(*)(QString&, const QString&, const QString&, const QString&, const QString&))db2Lib.resolve("createDB2AliasConnectionString");
      T_ASSERT(createDB2AliasConnectionString);

      createDB2AliasConnectionString(db2ConnectionString, dbAlias, dbUser, dbPassword, dbSchema);
   }

   QDbConnection db2Connection = createDB2Connection(db2ConnectionString);

   if (db2Connection.state().type() == QDbState::Error)
      throw TestException(db2Connection.state().message());

   return db2Connection;
}

BEGIN_TEST_SUITES
   TEST_SUITE(TestCases);
END_TEST_SUITES
