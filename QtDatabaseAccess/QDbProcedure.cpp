#include "QDbProcedure.h"

QDbProcedure::QDbProcedure()
{
}

QDbProcedure::QDbProcedure(const QDbProcedure& other) : _command(other._command)
{
}

QDbProcedure::QDbProcedure(const QString& name) : _command(name)
{
}

QDbProcedure::QDbProcedure(const QDbConnection& connection, const QString& name) : _command(connection, name)
{
}

QDbProcedure::~QDbProcedure()
{
}

QDbProcedure& QDbProcedure::operator=(const QDbProcedure& other)
{
   _command = other._command;
   return *this;
}

void QDbProcedure::set(const QString& parName, const Variant& value)
{
   _command.setPlaceholderValue(parName, value);
}

Variant QDbProcedure::get(const QString& parName)
{
   return _command.placeholderValue(parName);
}

Variant QDbProcedure::getReturnValue()
{
   return _command.placeholderValue(0);
}

bool QDbProcedure::execute(const QDbConnection& connection)
{
   return _command.exec();
}

QString QDbProcedure::lastError() const
{
   return _command.state().text();
}
