#pragma once

#include <QSharedData>
#include <Variant.h>
#include "QDbTableData.h"
#include "QDbSqlBuilderContext.h"
#include <SqlParser.h>
#include "QDbCommandData.h"

class QDbXmlReader;
class QDbXmlWriter;

// Abstract base class
class QDbExpressionData : public QSharedData
{
   Q_DISABLE_COPY(QDbExpressionData)

public:
   QDbExpressionData(QDbDataType type = DT_UNKNOWN) {}
   virtual ~QDbExpressionData() = default;

   virtual QDbExpressionData* clone() = 0;

   virtual QString sql(QDbSqlBuilderContext* builderContext, bool useQualifiedNames) const = 0; // absolute: column names are preceded by table names

   virtual bool isAggregate() const = 0;
   virtual bool isGroupable() const = 0;
   virtual bool isComplex() const = 0;

   virtual void toXml(QDbXmlWriter *writer) const = 0;

   static QDbExpressionData* fromXml(QDbXmlReader *reader);

   virtual bool isEqualTo(const QDbExpressionData* data) const = 0;
};

class QDbExprValueData : public QDbExpressionData
{
   Q_DISABLE_COPY(QDbExprValueData)

public:
   QDbExprValueData() = default;
   QDbExprValueData(const Variant& value) : _value(value) {}
   ~QDbExprValueData() override = default;

   QDbExprValueData* clone() override { return new QDbExprValueData(_value); }

   Variant _value;

   QString sql(QDbSqlBuilderContext* builderContext, bool useQualifiedNames) const override;

   bool isAggregate() const override { return false; }
   bool isGroupable() const override { return false; }
   bool isComplex() const override { return false; }

   void toXml(QDbXmlWriter *writer) const override;

   bool isEqualTo(const QDbExpressionData* data) const override;
};

class QDbExprBoundValueData : public QDbExpressionData
{
   Q_DISABLE_COPY(QDbExprBoundValueData)

public:
   QDbExprBoundValueData() = default;
   QDbExprBoundValueData(const bp<QSharedValue>& boundValue) : _boundValue(boundValue) {}
   ~QDbExprBoundValueData() override = default;

   QDbExprBoundValueData* clone() override { return new QDbExprBoundValueData(_boundValue); }

   bp<QSharedValue> _boundValue;

   QString sql(QDbSqlBuilderContext* builderContext, bool useQualifiedNames) const override;

   bool isAggregate() const override { return false; }
   bool isGroupable() const override { return false; }
   bool isComplex() const override { return false; }

   void toXml(QDbXmlWriter *writer) const override;

   bool isEqualTo(const QDbExpressionData* data) const override;
};

class QDbExprTableColumnData : public QDbExpressionData
{
   Q_DISABLE_COPY(QDbExprTableColumnData)

public:
   QDbExprTableColumnData() = default;
   QDbExprTableColumnData(bp<QDbTableData> table, QString name) : _table(table), _name(name) {}
   ~QDbExprTableColumnData() override = default;

   QDbExprTableColumnData* clone() override { return new QDbExprTableColumnData(_table, _name); }

   bp<QDbTableData> _table;
   QString _name;

   QString sql(QDbSqlBuilderContext* builderContext, bool useQualifiedNames) const override;

   bool isAggregate() const override { return false; }
   bool isGroupable() const override { return true; }
   bool isComplex() const override { return false; }

   void toXml(QDbXmlWriter *writer) const override;

   bool isEqualTo(const QDbExpressionData* data) const override;
};

class QDbExprOperationData : public QDbExpressionData
{
   Q_DISABLE_COPY(QDbExprOperationData)

public:
   QDbExprOperationData() = default;
   QDbExprOperationData(const SqlOperator& op);
   QDbExprOperationData(const SqlOperator& op, const QList<sp<QDbExpressionData>>& operands);
   QDbExprOperationData(const SqlOperator& op, const sp<QDbExpressionData>& operand);
   QDbExprOperationData(const sp<QDbExpressionData>& left, const SqlOperator& op, const sp<QDbExpressionData>& right);
   ~QDbExprOperationData() override = default;

   QDbExprOperationData* clone() override { return new QDbExprOperationData(_operator, _operands); }

   QString sql(QDbSqlBuilderContext* builderContext, bool useQualifiedNames) const override;

   bool isAggregate() const override;
   bool isGroupable() const override;
   bool isComplex() const override { return true; }

   void toXml(QDbXmlWriter *writer) const override;

   bool isEqualTo(const QDbExpressionData* data) const override;

   void addOperand(const sp<QDbExpressionData>& expression);

private:
   SqlOperator _operator;
   QList<sp<QDbExpressionData>> _operands;
};

class QDbExprFunctionData : public QDbExpressionData
{
   Q_DISABLE_COPY(QDbExprFunctionData)

public:
   QDbExprFunctionData() = default;
   QDbExprFunctionData(const QString& function, bool isAggregate, const QList<sp<QDbExpressionData>>& arguments);
   ~QDbExprFunctionData() override = default;

   QDbExprFunctionData* clone() override { return new QDbExprFunctionData(_function, _isAggregate, _arguments); }

   QString sql(QDbSqlBuilderContext* builderContext, bool useQualifiedNames) const override;

   bool isAggregate() const override { return _isAggregate; }
   bool isGroupable() const override { return !_isAggregate; }
   bool isComplex() const override { return true; }

   void toXml(QDbXmlWriter *writer) const override;

   bool isEqualTo(const QDbExpressionData* data) const override;

private:
   QString _function; // For example SUM(%1)
   bool _isAggregate = false; // If set, the expression is an aggregate
   QList<sp<QDbExpressionData> > _arguments;
};

class QDbExprQueryData : public QDbExpressionData
{
   Q_DISABLE_COPY(QDbExprQueryData)

public:
   QDbExprQueryData() = default;
   QDbExprQueryData(const bp<QDbCommandData>& command) : _command(command) {}
   ~QDbExprQueryData() override = default;

   QDbExprQueryData* clone() override { return new QDbExprQueryData(_command); }

   bp<QDbCommandData> _command;

   QString sql(QDbSqlBuilderContext* builderContext, bool useQualifiedNames) const override;

   bool isAggregate() const override { return false; }
   bool isGroupable() const override { return true; }
   bool isComplex() const override { return true; }

   void toXml(QDbXmlWriter *writer) const override;

   bool isEqualTo(const QDbExpressionData* data) const override;
};
