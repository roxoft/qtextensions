#include "QDbBoundColumn.h"
#include <QSharedValue.h>

QDbBoundColumn::QDbBoundColumn(const QDbExpression& c, QDbDataType t) : column(c), type(t), boundValue(new QSharedValue)
{
}

QDbBoundColumn::~QDbBoundColumn()
{
}
