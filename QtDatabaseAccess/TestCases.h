#ifndef TESTCASES_H
#define TESTCASES_H

#include <QLocaleEx.h>
#include <TestFramework.h>

class QDbConnection;

class TestCases : public TestFramework
{
   Q_OBJECT

public:
   TestCases(QObject *parent = 0) : TestFramework(parent) {}
   ~TestCases() override = default;

   void init() override;
   void deinit() override;

public slots :
   void primaryTests();
   void postLobTestOracle();
   void joinTests();
   void columnJoinTests();
   void emptyStringTests();
   void sqlServerTest();
   void storedProcedureTest();
   void triggerTestOracle();
   void serializationTests();
   void valueProxyTests();
   void sqlGroupTests();
   void sqlInTests();
   void limitOffsetTests();

private:
   void _primaryTests(const QDbConnection& connection);
   void _joinTests(const QDbConnection& connection);
   void _columnJoinTests(const QDbConnection& connection);
   void _emptyStringTest(const QDbConnection& connection);
   void _valueProxyTests(const QDbConnection& connection);
   void _sqlGroupTests(const QDbConnection& connection);
   void _sqlInTests(const QDbConnection& connection);
   void _limitOffsetTests(QDbConnection connection);

   QDbConnection createOracleConnection();
   QDbConnection createSqlServerConnection();
#ifdef Q_PROCESSOR_X86_32
   QDbConnection createSybaseConnection();
#endif
   QDbConnection createDb2Connection();
};

#endif // TESTCASES_H
