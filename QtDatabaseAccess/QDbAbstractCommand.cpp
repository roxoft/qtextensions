#include "QDbAbstractCommand.h"
#include "QDbCommandData.h"

QDbAbstractCommand::QDbAbstractCommand()
{
}

QDbAbstractCommand::QDbAbstractCommand(const QDbConnection& connection)
   : _connection(connection)
{
}

QDbAbstractCommand::QDbAbstractCommand(const QDbAbstractCommand& other)
   : _connection(other._connection), _state(other._state), _data(other._data)
{
}

QDbAbstractCommand::~QDbAbstractCommand()
{
}

QDbAbstractCommand& QDbAbstractCommand::operator=(const QDbAbstractCommand& other)
{
   _state.resetState();

   _connection = other._connection;
   _state = other._state;
   _data = other._data;
   return *this;
}

bool QDbAbstractCommand::hasError() const
{
   return _state.type() == QDbState::Error;
}

QString QDbAbstractCommand::lastError() const
{
   if (_state.type() == QDbState::Error)
   {
      if (!_state.sourceText().isEmpty())
      {
         return QString("%1\nSource: %2").arg(_state.text()).arg(_state.sourceText());
      }

      return _state.text();
   }

   return QString();
}

const QDbState& QDbAbstractCommand::state() const
{
   return _state;
}
