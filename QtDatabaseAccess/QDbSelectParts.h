#pragma once

#include "QDbBoundColumn.h"
#include "QDbSqlBuilderContext.h"

class QDbSelectParts
{
public:
   QStringList orderList;
   QStringList selectList;
   bool group = false;
   QStringList groupList;

   void setFrom(const QList<bp<QDbBoundColumn>>& bindings, QList<bp<QDbBoundColumn>> orderBy, QDbSqlBuilderContext* builderContext);
};
