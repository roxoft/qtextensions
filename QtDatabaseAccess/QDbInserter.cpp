#include "QDbInserter.h"
#include "QDbCommandData.h"
#include "QDbTable.h"
#include "QDbTableData.h"
#include "QDbSqlBuilderContext.h"
#include "QDbExpressionData.h"
#include <QRational.h>

QDbInserter::QDbInserter()
{
}

QDbInserter::QDbInserter(const QDbConnection& connection) : QDbAbstractReader(connection)
{
}

QDbInserter::QDbInserter(const QDbTable& table, const QDbConnection& connection) : QDbAbstractReader(connection)
{
   _data = new QDbCommandData;
   _data->_commandType = QDbCommandData::Insert;
   _data->_in = table._data;
}

QDbInserter::QDbInserter(const QDbInserter& other) : QDbAbstractReader(other), _affectedRows(other._affectedRows), _valuesTable(other._valuesTable)
{
}

QDbInserter::~QDbInserter()
{
}

QDbInserter& QDbInserter::operator=(const QDbInserter& other)
{
   QDbAbstractReader::operator=(other);
   _affectedRows = other._affectedRows;
   _valuesTable = other._valuesTable;
   return *this;
}

QDbInserter& QDbInserter::set(const QDbAssignment& assignment)
{
   if (_data)
   {
      int columnIndex;
      for (columnIndex = 0; columnIndex < _data->_assignments.count(); ++columnIndex)
      {
         if (_data->_assignments.at(columnIndex).left().isEqualTo(assignment.left()))
            break;
      }

      if (columnIndex == _data->_assignments.count())
      {
         if (_valuesTable.rowCount() > 0)
         {
            auto column = _valuesTable.column(assignment.left().name());

            _valuesTable.set(column, assignment.right());

            _data->_assignments.append(QDbAssignment(assignment.left(), column));
         }
         else
         {
            _data->_assignments.append(assignment);
         }
      }
      else
      {
         if (_valuesTable.rowCount() > 0)
         {
            _valuesTable.set((const QDbColumn&)_data->_assignments.at(columnIndex).right(), assignment.right());
         }
         else
         {
            _data->_assignments[columnIndex] = assignment;
         }
      }

   }

   return *this;
}

QDbInserter& QDbInserter::operator<<(const QDbAssignment& assignment)
{
   return set(assignment);
}

QDbInserter& QDbInserter::set(const QDbColumn& left, const QDbExpression& right) { set(QDbAssignment(left, right)); return *this; }
QDbInserter& QDbInserter::set(const QDbColumn& left, bool value) { set(QDbAssignment(left, value)); return *this; }
QDbInserter& QDbInserter::set(const QDbColumn& left, int value) { set(QDbAssignment(left, value)); return *this; }
QDbInserter& QDbInserter::set(const QDbColumn& left, long long int value) { set(QDbAssignment(left, value)); return *this; }
QDbInserter& QDbInserter::set(const QDbColumn& left, double value) { set(QDbAssignment(left, value)); return *this; }
QDbInserter& QDbInserter::set(const QDbColumn& left, const char *value) { set(QDbAssignment(left, value)); return *this; }
QDbInserter& QDbInserter::set(const QDbColumn& left, const QString& value) { set(QDbAssignment(left, value)); return *this; }
QDbInserter& QDbInserter::set(const QDbColumn& left, const QRational& value) { set(QDbAssignment(left, value)); return *this; }
QDbInserter& QDbInserter::set(const QDbColumn& left, const QDecimal& value) { set(QDbAssignment(left, value)); return *this; }
QDbInserter& QDbInserter::set(const QDbColumn& left, const QDate& value) { set(QDbAssignment(left, value)); return *this; }
QDbInserter& QDbInserter::set(const QDbColumn& left, const QTime& value) { set(QDbAssignment(left, value)); return *this; }
QDbInserter& QDbInserter::set(const QDbColumn& left, const QDateTime& value) { set(QDbAssignment(left, value)); return *this; }
QDbInserter& QDbInserter::set(const QDbColumn& left, const QDateTimeEx& value) { set(QDbAssignment(left, value)); return *this; }
QDbInserter& QDbInserter::set(const QDbColumn& left, const QByteArray& value) { set(QDbAssignment(left, value)); return *this; }
QDbInserter& QDbInserter::set(const QDbColumn& left, const Variant& value) { set(QDbAssignment(left, value)); return *this; }

bp<QSharedValue> QDbInserter::set(QDbColumn column)
{
   const auto sharedValue = bp<QSharedValue>(new QSharedValue);

   set(QDbAssignment(column, QDbExpression(sharedValue)));

   return sharedValue;
}

void QDbInserter::stashRow()
{
   if (!_data)
      return;

   if (_valuesTable.rowCount() <= 0)
   {
      _valuesTable = QDbValuesTable("BulkInsert");

      for (auto&& assignment : _data->_assignments)
      {
         auto column = _valuesTable.column(assignment.left().name());

         _valuesTable.set(column, assignment.right());

         assignment = QDbAssignment(assignment.left(), column);
      }

      _data->_from = _valuesTable._data;
   }

   _valuesTable.stashRow();
}

void QDbInserter::stashClear()
{
   if (_data)
   {
      _data->_from.reset();
      _valuesTable.stashClear();
   }
}

QDbInserter& QDbInserter::from(const QDbDataSource& dataSource)
{
   if (_data)
      _data->_from = dataSource._data;

   return *this;
}

bool QDbInserter::usePostLob() const
{
   return _data && _data->_usePostLob;
}

void QDbInserter::setUsePostLob(bool usePostLob)
{
   if (_data)
      _data->_usePostLob = usePostLob;
}

bool QDbInserter::hasAssignments() const
{
   return _data && !_data->_assignments.isEmpty();
}

int QDbInserter::affectedRows()
{
   return _affectedRows;
}

QList<QDbStatement> QDbInserter::createSqlStatements(const QDbSqlContext& sqlContext) const
{
   if (!_data)
   {
      return {};
   }

   QList<QDbStatement> dbStatements;
   QDbSqlBuilderContext builderContext(sqlContext, false);

   if (builderContext.sqlStyle() == SqlStyle::Oracle)
   {
      auto maxRowsPerDmlCall = builderContext.maxRowsPerDmlCall();
      builderContext.setMaxRowsPerDmlCall(1);

      do
      {
         QList<QString> batchStatements;

         builderContext.resetStatementVariables();

         while (batchStatements.length() < maxRowsPerDmlCall)
         {
            batchStatements.append(_data->createSql(&builderContext));

            if (!builderContext.hasMore())
            {
               break;
            }
         }

         if (batchStatements.length() == 1)
         {
            batchStatements[0].prepend("INSERT ");
         }
         else
         {
            batchStatements.prepend("INSERT ALL");
            batchStatements.append("SELECT 1 FROM DUAL");
         }

         dbStatements.append(QDbStatement(batchStatements.join("\n"), builderContext.phList()));

      } while (builderContext.hasMore());
   }
   else if (builderContext.sqlStyle() == SqlStyle::Sybase)
   {
      auto maxRowsPerDmlCall = builderContext.maxRowsPerDmlCall();
      builderContext.setMaxRowsPerDmlCall(1);

      do
      {
         QStringList insertStatements;

         builderContext.resetStatementVariables();

         while (insertStatements.length() < maxRowsPerDmlCall)
         {
            insertStatements.append("INSERT " + _data->createSql(&builderContext));

            if (!builderContext.hasMore())
            {
               break;
            }
         }

         dbStatements.append(QDbStatement(insertStatements.join("; "), builderContext.phList()));

      } while (builderContext.hasMore());
   }
   else
   {
      do
      {
         builderContext.resetStatementVariables();

         auto sqlStatement = "INSERT " + _data->createSql(&builderContext);

         // Check if returning values are defined for SQLite queries
         if (!_data->_bindings.isEmpty())
         {
            if (builderContext.sqlStyle() == SqlStyle::SQLite)
            {
               // SQLite does only return the last auto-increment value.
               // Attention: last_insert_rowid() works only on the same connection! The connection must remain open.
               //            Therefore both statements are joined into one command:
               sqlStatement += "; SELECT last_insert_rowid();";
            }
         }

         dbStatements.append(QDbStatement(sqlStatement, builderContext.phList()));

      } while (builderContext.hasMore());
   }

   return dbStatements;
}

bool QDbInserter::execute(const QDbConnection& connection)
{
   if (!_data)
   {
      _state = QDbState(QDbState::Error, "Inserter data is not defined");
      return false;
   }

   QDbCommand command(connection);

   auto dbStatements = createSqlStatements(QDbSqlContext(command.connection(), QDbSqlContext::PhUsage::Always));

   _affectedRows = 0;

   for (auto&& dbStatement : dbStatements)
   {
      _state.withSourceText(dbStatement.sqlStatement());

      _state = dbStatement.exec(&command, -1);
      if (_state.type() == QDbState::Error)
      {
         return false;
      }

      if (!_data->_bindings.isEmpty())
      {
         _state = processResult(command.connection().sqlStyle(), command.result(), _data->_bindings);
         if (_state.type() == QDbState::Error)
         {
            return false;
         }
      }

      _affectedRows += qMax(command.affectedRows(), 0);
   }

   return true;
}

QDbState QDbInserter::processResult(SqlStyle sqlStyle, QDbResultSet result, const QList<bp<QDbBoundColumn>>& bindings)
{
   if (sqlStyle == SqlStyle::SqlServer || sqlStyle == SqlStyle::DB2)
   {
      auto index = 0;

      for (auto&& binding : bindings)
      {
         //if (binding->type == DT_UNKNOWN)
         {
            if (index == 0)
            {
               if (!result.next())
               {
                  return QDbState(QDbState::Error, "The insert statement has no result");
               }
            }

            binding->boundValue->setValue(result.valueAt(index++));
         }
      }
   }
   else if (sqlStyle == SqlStyle::SQLite)
   {
      // Den zuletzt verwendeten Auto-Increment-Wert abfragen.
      if (!result.next())
      {
         return QDbState(QDbState::Error, "The insert statement has no result");
      }

      const auto rowId = result.valueAt(0).toInt64();

      if (rowId == 0LL)
      {
         return QDbState(QDbState::Error, "The returned row ID is 0");
      }

      bindings.at(0)->boundValue->setValue(rowId);

      if (bindings.count() > 1)
      {
         // TODO: Select using rowId
      }
   }

   return {};
}
