#include "QDbDeleter.h"
#include "QDbCommandData.h"
#include "QDbSqlBuilderContext.h"
#include "QDbExpressionData.h"

QDbDeleter::QDbDeleter()
{
}

QDbDeleter::QDbDeleter(const QDbDeleter& other) : QDbAbstractCommand(other)
{
}

QDbDeleter::QDbDeleter(const QDbDataSource& table)
{
   _data = new QDbCommandData;
   _data->_commandType = QDbCommandData::Delete;
   _data->_from = table._data;
}

QDbDeleter::~QDbDeleter()
{
}

QDbDeleter& QDbDeleter::operator=(const QDbDeleter& other)
{
   QDbAbstractCommand::operator=(other);
   return *this;
}

QDbDeleter& QDbDeleter::where(const QDbExpression& constraint)
{
   if (_data)
   {
      _data->_where = constraint._data;
   }
   return *this;
}

int QDbDeleter::affectedRows()
{
   return _affectedRows;
}

QList<QDbStatement> QDbDeleter::createSqlStatements(const QDbSqlContext& sqlContext) const
{
   if (!_data)
   {
      return {};
   }

   QDbSqlBuilderContext builderContext(sqlContext, false);
   return QList<QDbStatement>() << QDbStatement(_data->createSql(&builderContext), builderContext.phList());
}

bool QDbDeleter::execute(const QDbConnection& connection)
{
   if (!_data)
   {
      return {};
   }

   QDbCommand command(connection);

   QDbSqlContext sqlContext(command.connection(), QDbSqlContext::PhUsage::Optimized);
   QDbSqlBuilderContext builderContext(sqlContext, false);

   QDbStatement statement(_data->createSql(&builderContext), builderContext.phList());

   _state = statement.exec(&command, -1);

   if (_state.type() == QDbState::Error)
   {
      return false;
   }

   _affectedRows = command.affectedRows();

   return true;
}
