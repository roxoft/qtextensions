#include "QDbSelector.h"
#include "QDbTableData.h"
#include "QDbBoundColumn.h"
#include "QDbExpression.h"
#include "QDbColumn.h"
#include "QDbCommandData.h"
#include "QDbExpressionData.h"
#include <QStringBuilder>

QDbSelector::QDbSelector()
{
}

QDbSelector::QDbSelector(const QDbSelector& other) : QDbDataSource((const QDbDataSource&)other)
{
}

QDbSelector::QDbSelector(const QDbDataSource& table, const QString& alias)
{
   bp<QDbCommandData> command = new QDbCommandData;
   command->_commandType = QDbCommandData::Select;
   command->_from = table._data;

   _data = new QDbInlineTableData(command, alias);
}

QDbSelector::~QDbSelector()
{
}

QDbSelector& QDbSelector::operator=(const QDbSelector& other)
{
   QDbDataSource::operator=((const QDbDataSource&)other);
   return *this;
}

QDbColumn QDbSelector::addColumn(const QDbExpression& column, const QString& alias, QDbDataType type, QDb::SortOrder sortOrder)
{
   if (!commandData())
      return QDbColumn();

   auto boundColumn = bp_new<QDbBoundColumn>(column, type);

   if (!alias.isEmpty())
      boundColumn->alias = "\"" % alias % "\"";
   boundColumn->sortOrder = sortOrder;

   commandData()->_bindings.append(boundColumn);

   return QDbColumn(*this, boundColumn->alias.isEmpty() ? column.tableColumnName() : boundColumn->alias);
}

QDbSelector& QDbSelector::where(const QDbExpression& constraint)
{
   if (commandData())
      commandData()->_where = constraint._data;
   return *this;
}

QDbSelector& QDbSelector::distinct(bool isDistinct)
{
   if (commandData())
      commandData()->_distinct = isDistinct;
   return *this;
}

QDbSelector& QDbSelector::having(const QDbExpression& constraint)
{
   if (commandData())
      commandData()->_having = constraint._data;
   return *this;
}

QDbExpression QDbSelector::asColumn() const
{
   return QDbExpression(*this);
}

QDbExpression QDbSelector::asColumn(const QDbExpression& column, QDbDataType type)
{
   if (!commandData())
      return QDbExpression();

   const auto boundColumn = bp_new<QDbBoundColumn>(column, type);

   commandData()->_bindings.clear();
   commandData()->_bindings.append(boundColumn);

   return QDbExpression(*this);
}

QDbExpression QDbSelector::exists(const QDbExpression& column)
{
   if (!commandData())
      return QDbExpression();

   const auto boundColumn = bp_new<QDbBoundColumn>(column, DT_UNKNOWN);

   commandData()->_bindings.clear();
   commandData()->_bindings.append(boundColumn);

   QDbExpression expr(SqlOperator::Action::SQL_EXISTS);

   expr.addOperand(QDbExpression(*this));

   return expr;
}

QDbCommandData* QDbSelector::commandData()
{
   const auto inlineTable = _data.as<QDbInlineTableData>();

   if (inlineTable)
      return inlineTable->_command;

   return nullptr;
}

const QDbCommandData* QDbSelector::commandData() const
{
   const auto inlineTable = _data.as<QDbInlineTableData>();

   if (inlineTable)
      return inlineTable->_command;

   return nullptr;
}
