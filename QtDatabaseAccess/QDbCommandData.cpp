#include "QDbCommandData.h"
#include "QDbParameter.h"
#include "QDbTableData.h"
#include "QDbExpressionData.h"
#include "QDbSelectParts.h"
#include "QDbAssignment.h"
#include "QDbAssignmentParts.h"
#include <QtListExtensions.h>
#include <QStringBuilder>

static const char* Commands[] =
{
   "SELECT",
   "INTO",
   "UPDATE",
   "MERGE",
   "DELETE"
};

QDbCommandData::QDbCommandData()
{
}

QDbCommandData::~QDbCommandData()
{
}

// Special cases for multible rows:
//
// Update multiple rows:
// - General: Update .. Set .. From .. Where;
// - Works for Oracle since version 23ai, SQL Server, SQLite since version 3.33.
// - See
//   https://stackoverflow.com/questions/6629088/bulk-record-update-with-sql
//   https://stackoverflow.com/questions/2334712/how-do-i-update-from-a-select-in-sql-server
//   https://stackoverflow.com/questions/7030699/update-a-table-with-data-from-another-table
//   https://learn.microsoft.com/en-us/sql/t-sql/queries/update-transact-sql?view=sql-server-ver16
//   https://database.guide/values-clause-in-sql-server/
// - Alternatives:
//   - Update Set Case Where; See https://stackoverflow.com/questions/15501779/sqlite-bulk-update-statement
//   - Create Temporary Table ..; Update Set Select Where Exists;
//     See
//     https://stackoverflow.com/questions/19270259/update-with-join-in-sqlite
//     https://www.techonthenet.com/oracle/update.php
//
// Merge multiple rows:
// - SQLite: Create Temporary Table ..; Upsert; See https://stackoverflow.com/questions/418898/upsert-not-insert-or-replace
// - Oracle: Create Temporary Table ..; Merge Into;
// - SQLServer: Merge Into Using Values;
//
// Creating a temporary table:
// - See:
//   https://stackoverflow.com/questions/418898/upsert-not-insert-or-replace
//   https://www.sqlite.org/lang_select.html
//   https://stackoverflow.com/questions/43913457/how-do-i-name-columns-in-a-values-clause
//   https://stackoverflow.com/questions/10353969/how-can-i-select-from-list-of-values-in-oracle
//   https://docs.oracle.com/javadb/10.8.3.0/ref/rrefsqlj11277.html
//   https://oracle-base.com/articles/23/table-values-constructor-23
//
QString QDbCommandData::createSql(QDbSqlBuilderContext* builderContext) const
{
   // The WITH clause may work for ORACLE since 12c but the UPDATE ... FROM clause needs at least ORACLE 23ai.
   // For ORACLE updates with multiple rows is therefor the MERGE statement used.
   if (_commandType == Update && builderContext->sqlStyle() == SqlStyle::Oracle && _from.is<QDbValuesTableData>())
   {
      return createMergeCommand(builderContext);
   }

   if (_commandType == Merge)
   {
      return createMergeCommand(builderContext);
   }

   QString sql;
   QList<QDbParameter> phMap;

   // Gather command parts

   auto valueTable = _from.as<QDbValuesTableData>();

   // _from must be converted to SQL before the select parts are evaluated because it generates the table aliases
   QString from;
   if (_from.isValid() && (_commandType != Insert || !valueTable)) // Insert into with values has no FROM clause
   {
      from = _from->sql(builderContext);
   }

   QDbSelectParts selectParts;
   if (_commandType == Select)
   {
      selectParts.setFrom(_bindings, _orderBy, builderContext);
   }
   if (_commandType == Insert && _from.isValid())
   {
      auto bindings = toList<bp<QDbBoundColumn>>(_assignments, [](const QDbAssignment& assignment){ return bp<QDbBoundColumn>(new QDbBoundColumn(assignment.left(), QDbDataType::DT_UNKNOWN)); });
      selectParts.setFrom(bindings, _orderBy, builderContext);
   }

   QDbAssignmentParts assignmentParts;
   if (_commandType == Insert || _commandType == Update)
   {
      assignmentParts.setFrom(_assignments, _bindings, builderContext, _commandType == Update && _from.isValid(), _usePostLob && !_from.isValid());
   }

   QString havingClause;
   if (_having)
   {
      havingClause = _having->sql(builderContext, true);

      if (_having->isGroupable())
         selectParts.groupList.append(havingClause);
   }

   // Construct statement

   if (_commandType == Update && valueTable)
   {
      sql += "WITH ";
      sql += builderContext->getTableAlias(valueTable);
      sql += "(";
      sql += valueTable->assignemntColumns().join(", ");
      sql += ") AS (";
      sql += from;
      sql += ") ";
      from = builderContext->getTableAlias(valueTable);
   }
   sql += Commands[_commandType];
   if (_in)
   {
      sql += " ";
      sql += _in->name();
   }
   if (!assignmentParts.strColumns.isEmpty())
   {
      if (_commandType == Insert)
      {
         sql += " (";
         sql += assignmentParts.strColumns.join(", ");
         sql += ")";
      }
      else if (_commandType == Update)
      {
         sql += " SET ";

         QStringList assignments;

         for (int i = 0; i < assignmentParts.strColumns.count(); ++i)
            assignments.append(assignmentParts.strColumns.at(i) % " = " % assignmentParts.strValues.at(i));

         sql += assignments.join(", ");
      }
   }
   if (!assignmentParts.returningColumns.isEmpty())
   {
      if (builderContext->sqlStyle() == SqlStyle::SqlServer)
      {
         sql += " OUTPUT";
         for (auto&& returningColumn : assignmentParts.returningColumns)
         {
            sql += " INSERTED.[" + returningColumn + "]";
         }

         // Inserting into parameters is not supperted by ODBC
         //if (!assignmentParts.returningPhList.isEmpty())
         //{
         //   result += " INTO ";
         //   result += assignmentParts.returningPhList.join(", ");
         //}
      }
   }
   if (_commandType == Insert)
   {
      if (_from)
      {
         if (valueTable)
         {
            sql += " VALUES " + valueTable->sqlValues(builderContext, toList<sp<QDbExpressionData>>(_assignments, [](const QDbAssignment& assignment){ return assignment.right()._data; }));
            return sql;
         }

         sql += " SELECT ";
      }
      else
      {
         sql += " VALUES (" + assignmentParts.strValues.join(", ") + ")";
      }
   }
   if (_distinct)
      sql += " DISTINCT";
   if (builderContext->sqlStyle() == SqlStyle::Sybase)
   {
      if (_limit > 0 || _offset > 0)
      {
         sql += " TOP ";
         sql += _limit > 0 ? QString::number(_limit) : "ALL";
      }
      if (_offset > 0)
      {
         sql += " START AT ";
         sql += QString::number(_offset);
      }
   }
   if (!selectParts.selectList.isEmpty())
   {
      sql += " ";
      sql += selectParts.selectList.join(", ");
   }
   if (_from)
   {
      sql += " FROM ";
      sql += from;
   }
   if (_where)
   {
      const auto whereClause = _where->sql(builderContext, _commandType != Delete && _from != nullptr);

      sql += " WHERE ";
      sql += whereClause;
   }
   if (selectParts.group && !selectParts.groupList.isEmpty())
   {
      selectParts.groupList.removeDuplicates();

      sql += " GROUP BY ";
      sql += selectParts.groupList.join(", ");
   }
   if (!havingClause.isEmpty())
   {
      sql += " HAVING ";
      sql += havingClause;
   }
   if (!selectParts.orderList.isEmpty())
   {
      sql += " ORDER BY ";
      sql += selectParts.orderList.join(", ");

      if (builderContext->sqlStyle() == SqlStyle::SQLite || builderContext->sqlStyle() == SqlStyle::DB2)
      {
         if (_limit > 0)
         {
            sql += " LIMIT ";
            sql += QString::number(_limit);
            if (_offset > 0)
            {
               sql += " OFFSET ";
               sql += QString::number(_offset);
            }
         }
      }
      else if (builderContext->sqlStyle() == SqlStyle::SqlServer || builderContext->sqlStyle() == SqlStyle::Oracle)
      {
         if (_offset > 0 || _limit > 0)
         {
            sql += " OFFSET ";
            sql += QString::number(_offset);
            sql += "ROWS";
         }
         if (_limit > 0)
         {
            sql += " FETCH NEXT ";
            sql += QString::number(_limit);
            sql += " ROWS ONLY";
         }
      }
   }
   if (!assignmentParts.returningColumns.isEmpty())
   {
      if (builderContext->sqlStyle() == SqlStyle::Oracle)
      {
         // Bei Oracle geschieht dies mit der RETURNING ... INTO clause.
         sql += " RETURNING ";
         sql += assignmentParts.returningColumns.join(", ");
         sql += " INTO ";
         sql += assignmentParts.returningPhList.join(", ");
      }
      else if (builderContext->sqlStyle() == SqlStyle::DB2)
      {
         sql = QString("SELECT %1 FROM FINAL TABLE (%2)").arg(assignmentParts.returningColumns.join(", ")).arg(sql);
      }
   }

   return sql;
}

QString QDbCommandData::createMergeCommand(QDbSqlBuilderContext* builderContext) const
{
   QString sql;
   QList<QDbParameter> phMap;

   const auto from = _from->sql(builderContext); // _from must be converted to SQL before the select parts are evaluated because it generates the table aliases

   QDbAssignmentParts assignmentParts;
   assignmentParts.setFrom(_assignments, _bindings, builderContext, true, false);

   auto valueTable = _from.as<QDbValuesTableData>();

   sql = "MERGE";
   sql += " INTO ";
   sql += _in->sql(builderContext);
   sql += " USING ";
   if (valueTable)
   {
      sql += "(";
      sql += from;
      sql += ") ";
      if (builderContext->sqlStyle() != SqlStyle::Oracle)
         sql += "AS ";
      sql += builderContext->getTableAlias(valueTable);
      if (builderContext->sqlStyle() == SqlStyle::SQLite)
      {
         sql += "(";
         sql += valueTable->assignemntColumns().join(", ");
         sql += ")";
      }
   }
   else
   {
      sql += from;
   }
   sql += " ON (";
   sql += _where->sql(builderContext, true);
   sql += ") WHEN MATCHED THEN UPDATE SET ";

   QStringList assignments;

   for (int i = 0; i < assignmentParts.strColumns.count(); ++i)
      assignments.append(assignmentParts.strColumns.at(i) % " = " % assignmentParts.strValues.at(i));

   sql += assignments.join(", ");

   // TODO: Insert if key assignments are defined

   if (builderContext->sqlStyle() == SqlStyle::SqlServer)
   {
      sql += ";"; // For MS SQL this is mandatory. See https://learn.microsoft.com/en-us/sql/t-sql/statements/merge-transact-sql?view=sql-server-ver16
   }

   return sql;
}
