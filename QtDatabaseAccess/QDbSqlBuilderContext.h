#pragma once
#include <QList>
#include <QHash>
#include "QDbSqlContext.h"
#include "QDbParameter.h"

class QDbTableData;
class QDbExprTableColumnData;

class QDbSqlBuilderContext
{
public:
   QDbSqlBuilderContext(const QDbSqlContext& context, bool useTableAlias)
      : _connection(context.connection()),
        _phUsage(context.phUsage()),
        _maxRowsPerDmlCall(qMax(context.connection().maxRowsPerDmlCall(), 1)),
        _useTableAlias(useTableAlias)
   {
   }

   SqlStyle sqlStyle() const { return _connection.sqlStyle(); }

   int maxRowsPerDmlCall() const { return _maxRowsPerDmlCall; }
   void setMaxRowsPerDmlCall(int maxRowsPerDmlCall) { _maxRowsPerDmlCall = maxRowsPerDmlCall; }

   const QList<QDbParameter>& phList() const { return _phList; }

   QString getParameter(const Variant& value);

   QString getParameter(const bp<QSharedValue> boundValue, QDbDataType dataType = DT_UNKNOWN, QDbTransferMode transferMode = TM_Default);

   QString tryChangePlaceholderToPostLob(const QString& sqlParameter, const sp<QDbExprTableColumnData>& column);

   QString getOrCreateTableAlias(const QDbTableData* tableData, const QString& customAlias);

   QString getTableAlias(const QDbTableData* tableData) const;

   int currentRow() const { return _currentRow; }
   void incrementCurrentRow() { ++_currentRow; }

   bool hasMore() const { return _hasMore; }
   void setHasMore(bool hasMore = true) { _hasMore = hasMore; }

   void resetStatementVariables() { _phList.clear(); _tableAliasList.clear(); _hasMore = false; }

private:
   QDbConnection _connection;
   QDbSqlContext::PhUsage _phUsage = QDbSqlContext::PhUsage::Optimized;
   int _maxRowsPerDmlCall = 1;
   bool _useTableAlias = false;
   int _currentRow = 0;

   QList<QDbParameter> _phList;
   QHash<const QDbTableData*, QString> _tableAliasList; // Only for tables without alias
   bool _hasMore = false;
};
