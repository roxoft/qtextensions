#include "QDbReader.h"
#include "QDbBoundColumn.h"
#include "QDbTableData.h"
#include "QDbCommandData.h"
#include "QDbSqlBuilderContext.h"
#include "QDbExpressionData.h"

QDbReader::QDbReader()
{
}

QDbReader::QDbReader(const QDbConnection& connection) : QDbAbstractReader(connection)
{
}

QDbReader::QDbReader(const QDbDataSource& table, const QDbConnection& connection)
   : QDbAbstractReader(connection)
{
   _data = new QDbCommandData;
   _data->_commandType = QDbCommandData::Select;
   _data->_from = table._data;
}

QDbReader::QDbReader(const QDbDataSource& table, int rowsToFetch, const QDbConnection& connection)
   : QDbAbstractReader(connection)
{
   _data = new QDbCommandData;
   _data->_commandType = QDbCommandData::Select;
   _data->_from = table._data;
   _rowsToFetch = rowsToFetch;
}

QDbReader::QDbReader(const QDbReader& other) : QDbAbstractReader(other), _rowsToFetch(other._rowsToFetch), _result(other._result)
{
}

QDbReader::~QDbReader()
{
}

QDbReader& QDbReader::operator=(const QDbReader& other)
{
   QDbAbstractReader::operator=(other);

   _rowsToFetch = other._rowsToFetch;
   _result = other._result;

   return *this;
}

bool QDbReader::readNext()
{
   if (!_data)
      return false;

   if (!_result.isValid())
      execute();

   if (!_result.next())
      return false;

   for (int i = 0; i < _data->_bindings.count(); ++i)
      _data->_bindings[i]->boundValue->setValue(_result.valueAt(i));

   return true;
}

QList<QDbStatement> QDbReader::createSqlStatements(const QDbSqlContext& sqlContext) const
{
   if (!_data)
   {
      return {};
   }

   QDbSqlBuilderContext builderContext(sqlContext, true);
   return QList<QDbStatement>() << QDbStatement(_data->createSql(&builderContext), builderContext.phList());
}

bool QDbReader::execute(const QDbConnection& connection)
{
   if (!_data)
   {
      return {};
   }

   QDbCommand command(connection);

   QDbSqlContext sqlContext(command.connection(), QDbSqlContext::PhUsage::Optimized);
   QDbSqlBuilderContext builderContext(sqlContext, true);

   QDbStatement statement(_data->createSql(&builderContext), builderContext.phList());

   _state = statement.exec(&command, _rowsToFetch);

   if (_state.type() == QDbState::Error)
   {
      return false;
   }

   _result = command.result();

   return true;
}

bool QDbReader::execute(int rowsToFetch, const QDbConnection &connection)
{
   _rowsToFetch = rowsToFetch;
   return execute(connection);
}
