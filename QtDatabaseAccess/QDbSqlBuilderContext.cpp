#include "QDbSqlBuilderContext.h"
#include "QDbTableData.h"
#include "QDbExpressionData.h"

QString QDbSqlBuilderContext::getParameter(const Variant &value)
{
   const auto dbType = dbTypeFromVariant(value);

   if (dbType != DT_UNKNOWN && (_phUsage == QDbSqlContext::PhUsage::Always || (_phUsage == QDbSqlContext::PhUsage::Optimized && _connection.usePlaceholder(dbType, value))))
   {
      QDbParameter parameter;

      parameter.value = value;
      parameter.name = QString("PH%1").arg(_phList.count() + 1, 2, 10, QLatin1Char('0'));

      _phList.append(parameter);

      return _connection.phIndicator() + parameter.name;
   }

   return _connection.toLiteral(dbType, value);
}

QString QDbSqlBuilderContext::getParameter(const bp<QSharedValue> boundValue, QDbDataType dataType, QDbTransferMode transferMode)
{
   QDbParameter parameter;

   parameter.boundValue = boundValue;
   parameter.type = dataType;
   parameter.transferMode = transferMode;
   parameter.name = QString("PH%1").arg(_phList.count() + 1, 2, 10, QLatin1Char('0'));

   _phList.append(parameter);

   return _connection.phIndicator() + parameter.name;
}

QString QDbSqlBuilderContext::tryChangePlaceholderToPostLob(const QString &sqlParameter, const sp<QDbExprTableColumnData>& column)
{
   if (!sqlParameter.startsWith(_connection.phIndicator()))
   {
      return {};
   }

   auto parameterName = sqlParameter.mid(1);

   for (int i = _phList.length(); i--; )
   {
      if (_phList[i].name == parameterName)
      {
         const auto columnSchema = column->_table.as<QDbPlainTableData>()->tableSchema(_connection).column(column->_name);

         if (columnSchema.type == DT_LARGE_STRING ||
             columnSchema.type == DT_LARGE_NSTRING ||
             columnSchema.type == DT_LARGE_BINARY)
         {
            _phList[i].isPostLob = true;

            return columnSchema.type == DT_LARGE_BINARY ? "EMPTY_BLOB()" : "EMPTY_CLOB()";
         }

         break;
      }
   }

   return {};
}

QString QDbSqlBuilderContext::getOrCreateTableAlias(const QDbTableData *tableData, const QString &customAlias)
{
   if (tableData == nullptr || !(_useTableAlias || tableData->name().isEmpty()))
   {
      return {};
   }

   if (!customAlias.isEmpty())
   {
      return customAlias;
   }

   auto& tableAlias = _tableAliasList[tableData];

   if (tableAlias.isEmpty())
   {
      tableAlias = QString("T%1").arg(_tableAliasList.count());
   }

   return tableAlias;
}

QString QDbSqlBuilderContext::getTableAlias(const QDbTableData *tableData) const
{
   if (tableData == nullptr)
   {
      return {};
   }

   return _tableAliasList.value(tableData, tableData->alias());
}
