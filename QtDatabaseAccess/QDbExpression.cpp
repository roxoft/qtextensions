#include "QDbExpression.h"
#include "QDbExpressionData.h"
#include "QDbSelector.h"

QDbExpression::QDbExpression()
{
}

QDbExpression::QDbExpression(const Variant& value) : _data(new QDbExprValueData(value))
{
}

QDbExpression::QDbExpression(const bp<QSharedValue>& boundValue) : _data(new QDbExprBoundValueData(boundValue.data()))
{
}

QDbExpression::QDbExpression(const QString& sqlTemplate, bool isAggregate, const QList<QDbExpression>& arguments)
{
   QList<sp<QDbExpressionData>> expressionDataList;

   for (auto&& argument : arguments)
   {
      expressionDataList.append(argument._data);
   }

   _data = new QDbExprFunctionData(sqlTemplate, isAggregate, expressionDataList);
}

QDbExpression::QDbExpression(const QDbExpression& leftOperand, SqlOperator::Action op, const QDbExpression& rightOperand) : _data(new QDbExprOperationData(leftOperand._data, SqlOperator(op), rightOperand._data))
{
}

QDbExpression::QDbExpression(SqlOperator::Action op) : _data(new QDbExprOperationData(SqlOperator(op)))
{
}

QDbExpression::QDbExpression(const QDbSelector &selector) : _data(new QDbExprQueryData(const_cast<QDbCommandData*>(selector.commandData())))
{
}

QDbExpression::QDbExpression(const QDbExpression &other) : _data(other._data)
{
}

QDbExpression::~QDbExpression()
{
}

QDbExpression &QDbExpression::operator=(const QDbExpression &other)
{
   _data = other._data;
   return *this;
}

bool QDbExpression::isEqualTo(const QDbExpression& other) const
{
   if (_data)
      return _data->isEqualTo(other._data);
   return _data == other._data;
}

QString QDbExpression::tableColumnName() const
{
   auto tableColumnData = _data.as<QDbExprTableColumnData>();

   if (tableColumnData)
      return tableColumnData->_name;

   return QString();
}

QDbExpression QDbExpression::operator==(const QDbExpression& rhs) const
{
   return QDbExpression(*this, SqlOperator::Action::Equal, rhs);
}

QDbExpression QDbExpression::operator==(const Variant& value) const
{
   return QDbExpression(*this, SqlOperator::Action::Equal, value);
}

QDbExpression QDbExpression::operator!=(const QDbExpression& rhs) const
{
   return QDbExpression(*this, SqlOperator::Action::NotEqual, rhs);
}

QDbExpression QDbExpression::operator!=(const Variant& value) const
{
   return QDbExpression(*this, SqlOperator::Action::NotEqual, value);
}

QDbExpression QDbExpression::isNull() const
{
   return QDbExpression(*this, SqlOperator::Action::Equal, Variant::null);
}

QDbExpression QDbExpression::isNotNull() const
{
   return QDbExpression(*this, SqlOperator::Action::NotEqual, Variant::null);
}

QDbExpression QDbExpression::operator<(const QDbExpression& rhs) const
{
   return QDbExpression(*this, SqlOperator::Action::Less, rhs);
}

QDbExpression QDbExpression::operator<(const Variant& value) const
{
   return QDbExpression(*this, SqlOperator::Action::Less, value);
}

QDbExpression QDbExpression::operator<=(const QDbExpression& rhs) const
{
   return QDbExpression(*this, SqlOperator::Action::LessOrEqual, rhs);
}

QDbExpression QDbExpression::operator<=(const Variant& value) const
{
   return QDbExpression(*this, SqlOperator::Action::LessOrEqual, value);
}

QDbExpression QDbExpression::operator>(const QDbExpression& rhs) const
{
   return QDbExpression(*this, SqlOperator::Action::Greater, rhs);
}

QDbExpression QDbExpression::operator>(const Variant& value) const
{
   return QDbExpression(*this, SqlOperator::Action::Greater, value);
}

QDbExpression QDbExpression::operator>=(const QDbExpression& rhs) const
{
   return QDbExpression(*this, SqlOperator::Action::GreaterOrEqual, rhs);
}

QDbExpression QDbExpression::operator>=(const Variant& value) const
{
   return QDbExpression(*this, SqlOperator::Action::GreaterOrEqual, value);
}

QDbExpression QDbExpression::like(const QDbExpression& rhs) const
{
   return QDbExpression(*this, SqlOperator::Action::SQL_LIKE, rhs);
}

QDbExpression QDbExpression::like(const Variant& value) const
{
   return QDbExpression(*this, SqlOperator::Action::SQL_LIKE, value);
}

QDbExpression QDbExpression::unlike(const QDbExpression& rhs) const
{
   return QDbExpression(*this, SqlOperator::Action::SQL_NOT_LIKE, rhs);
}

QDbExpression QDbExpression::unlike(const Variant& value) const
{
   return QDbExpression(*this, SqlOperator::Action::SQL_NOT_LIKE, value);
}

QDbExpression QDbExpression::startsWith(const QString& text) const
{
   return QDbExpression(*this, SqlOperator::Action::SQL_LIKE, Variant(QDbConnection::escapeForLike(text) + "%"));
}

QDbExpression QDbExpression::startsNotWith(const QString& text) const
{
   return QDbExpression(*this, SqlOperator::Action::SQL_NOT_LIKE, Variant(QDbConnection::escapeForLike(text) + "%"));
}

QDbExpression QDbExpression::endsWith(const QString& text) const
{
   return QDbExpression(*this, SqlOperator::Action::SQL_LIKE, Variant("%" + QDbConnection::escapeForLike(text)));
}

QDbExpression QDbExpression::endsNotWith(const QString& text) const
{
   return QDbExpression(*this, SqlOperator::Action::SQL_NOT_LIKE, Variant("%" + QDbConnection::escapeForLike(text)));
}

QDbExpression QDbExpression::contains(const QString& text) const
{
   return QDbExpression(*this, SqlOperator::Action::SQL_LIKE, Variant("%" + QDbConnection::escapeForLike(text) + "%"));
}

QDbExpression QDbExpression::containsNot(const QString& text) const
{
   return QDbExpression(*this, SqlOperator::Action::SQL_NOT_LIKE, Variant("%" + QDbConnection::escapeForLike(text) + "%"));
}

QDbExpression QDbExpression::between(const QDbExpression &lowerBound, const QDbExpression &upperBound) const
{
   auto expr = QDbExpression(lowerBound, SqlOperator::Action::SQL_AND, upperBound);
   return QDbExpression(*this, SqlOperator::Action::SQL_BETWEEN, expr);
}

QDbExpression QDbExpression::between(const Variant& lowerBound, const Variant& upperBound) const
{
   auto expr = QDbExpression(lowerBound, SqlOperator::Action::SQL_AND, upperBound);
   return QDbExpression(*this, SqlOperator::Action::SQL_BETWEEN, expr);
}

QDbExpression QDbExpression::outside(const QDbExpression &lowerBound, const QDbExpression &upperBound) const
{
   auto expr = QDbExpression(lowerBound, SqlOperator::Action::SQL_AND, upperBound);
   return QDbExpression(*this, SqlOperator::Action::SQL_NOT_BETWEEN, expr);
}

QDbExpression QDbExpression::outside(const Variant& lowerBound, const Variant& upperBound) const
{
   auto expr = QDbExpression(lowerBound, SqlOperator::Action::SQL_AND, upperBound);
   return QDbExpression(*this, SqlOperator::Action::SQL_NOT_BETWEEN, expr);
}

QDbExpression QDbExpression::in(const QList<QDbExpression> &columns) const
{
   return colIn(columns, false);
}

QDbExpression QDbExpression::in(const QList<Variant> &values) const
{
   QList<QDbExpression> columnList;

   for (int i = 0; i < values.count(); ++i)
      columnList.append(values.at(i));

   return colIn(columnList, false);
}

QDbExpression QDbExpression::in(const QDbExpression& values) const
{
   return QDbExpression(*this, SqlOperator::Action::SQL_IN, values);
}

QDbExpression QDbExpression::notIn(const QList<QDbExpression> &columns) const
{
   return colIn(columns, true);
}

QDbExpression QDbExpression::notIn(const QList<Variant> &values) const
{
   QList<QDbExpression> columnList;

   for (int i = 0; i < values.count(); ++i)
      columnList.append(values.at(i));

   return colIn(columnList, true);
}

QDbExpression QDbExpression::notIn(const QDbExpression& values) const
{
   return QDbExpression(*this, SqlOperator::Action::SQL_NOT_IN, values);
}

QDbExpression QDbExpression::includes(const QDbExpression &column) const
{
   return QDbExpression(column, SqlOperator::Action::SQL_IN, *this);
}

QDbExpression QDbExpression::excludes(const QDbExpression &column) const
{
   return QDbExpression(column, SqlOperator::Action::SQL_NOT_IN, *this);
}

QDbExpression QDbExpression::operator>>=(const QDbExpression &rhs) const
{
   return QDbExpression(*this, SqlOperator::Action::LeftEqual, rhs);
}

QDbExpression QDbExpression::operator<<=(const QDbExpression &rhs) const
{
   return QDbExpression(*this, SqlOperator::Action::RightEqual, rhs);
}

QDbExpression QDbExpression::operator<<=(const Variant &value) const
{
   return QDbExpression(*this, SqlOperator::Action::RightEqual, value);
}

QDbExpression QDbExpression::operator+(const QDbExpression& rhs) const
{
   return QDbExpression(*this, SqlOperator::Action::Addition, rhs);
}

QDbExpression QDbExpression::operator+(const Variant& value) const
{
   return QDbExpression(*this, SqlOperator::Action::Addition, value);
}

QDbExpression QDbExpression::operator-(const QDbExpression& rhs) const
{
   return QDbExpression(*this, SqlOperator::Action::Subtraction, rhs);
}

QDbExpression QDbExpression::operator-(const Variant& value) const
{
   return QDbExpression(*this, SqlOperator::Action::Subtraction, value);
}

QDbExpression QDbExpression::operator*(const QDbExpression& rhs) const
{
   return QDbExpression(*this, SqlOperator::Action::Multiplication, rhs);
}

QDbExpression QDbExpression::operator*(const Variant& value) const
{
   return QDbExpression(*this, SqlOperator::Action::Multiplication, value);
}

QDbExpression QDbExpression::operator/(const QDbExpression& rhs) const
{
   return QDbExpression(*this, SqlOperator::Action::Division, rhs);
}

QDbExpression QDbExpression::operator/(const Variant& value) const
{
   return QDbExpression(*this, SqlOperator::Action::Division, value);
}

QDbExpression QDbExpression::operator&&(const QDbExpression& rhs) const
{
   if (!isValid())
      return rhs;
   if (!rhs.isValid())
      return *this;

   return QDbExpression(*this, SqlOperator::Action::SQL_AND, rhs);
}

QDbExpression QDbExpression::operator||(const QDbExpression& rhs) const
{
   if (!isValid())
      return rhs;
   if (!rhs.isValid())
      return *this;

   return QDbExpression(*this, SqlOperator::Action::SQL_OR, rhs);
}

QDbExpression QDbExpression::operator!() const
{
   if (!isValid())
      return *this;

   QDbExpression result(SqlOperator::Action::SQL_NOT);

   result.addOperand(*this);

   return result;
}

QDbExpression& QDbExpression::operator&=(const QDbExpression& rhs)
{
   return *this = *this && rhs;
}

QDbExpression& QDbExpression::operator|=(const QDbExpression& rhs)
{
   return *this = *this || rhs;
}

QDbTypedExpression<QString> QDbExpression::upperCase() const
{
   return QDbExpression("UPPER(%1)", false, QList<QDbExpression>() << *this);
}

QDbExpression QDbExpression::ConcatWith(const QDbExpression& rhs) const
{
   return QDbExpression("CONCAT(%1, %2)", false, QList<QDbExpression>() << *this << rhs);
}

QDbExpression QDbExpression::ConcatWith(const QString& text) const
{
   return QDbExpression("CONCAT(%1, %2)", false, QList<QDbExpression>() << *this << Variant(text));
}

QDbTypedExpression<qint64> QDbExpression::count() const
{
   return QDbExpression("COUNT(%1)", true, QList<QDbExpression>() << *this);
}

QDbExpression QDbExpression::max() const
{
   return QDbExpression("MAX(%1)", true, QList<QDbExpression>() << *this);
}

QDbExpression QDbExpression::min() const
{
   return QDbExpression("MIN(%1)", true, QList<QDbExpression>() << *this);
}

QDbExpression QDbExpression::sum() const
{
   return QDbExpression("SUM(%1)", true, QList<QDbExpression>() << *this);
}

bool QDbExpression::isAggregate() const
{
   return _data && _data->isAggregate();
}

bool QDbExpression::isGroupable() const
{
   return _data && _data->isGroupable();
}

bool QDbExpression::isValue() const
{
   return !_data.is<QDbExprOperationData>();
}

void QDbExpression::addOperand(const QDbExpression& operand)
{
   auto operation = _data.as<QDbExprOperationData>();

   if (operation)
      operation->addOperand(operand._data);
}

void QDbExpression::toXml(QDbXmlWriter* writer) const
{
   if (_data)
      _data->toXml(writer);
}

QDbExpression QDbExpression::colIn(const QList<QDbExpression>& columns, bool negate) const
{
   if (columns.isEmpty())
      return QDbExpression(QDbExpression(1), SqlOperator::Action::Equal, QDbExpression(negate ? 1 : 2));

   if (columns.count() == 1)
      return QDbExpression(*this, negate ? SqlOperator::Action::NotEqual : SqlOperator::Action::Equal, columns.first());

   auto expr = QDbExpression(negate ? SqlOperator::Action::SQL_NOT_IN : SqlOperator::Action::SQL_IN);

   expr.addOperand(QDbExpression(*this));

   auto rightOperand = QDbExpression(SqlOperator::Action::Comma);

   for (auto&& column : columns)
      rightOperand.addOperand(QDbExpression(column));

   expr.addOperand(rightOperand);

   return expr;
}
