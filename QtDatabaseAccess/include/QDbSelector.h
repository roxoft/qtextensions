#ifndef QDBSELECTOR_H
#define QDBSELECTOR_H

#include "qtdatabaseaccess_global.h"
#include "QDbDataSource.h"
#include "QDbTypedExpression.h"
#include "QDbDefs.h"

class QDbCommandData;

class QTDATABASEACCESS_EXPORT QDbSelector : public QDbDataSource
{
   friend class QDbExpression;

public:
   QDbSelector(); // Cannot use default because of QDbDataSource::_data
   QDbSelector(const QDbSelector& other);
   QDbSelector(const QDbDataSource& table, const QString& alias = QString());
   ~QDbSelector(); // Cannot use default because of QDbDataSource::_data

   QDbSelector& operator=(const QDbSelector& other);

   template<class T>
   QDbTypedColumn<T> addColumn(const QDbTypedColumn<T>& column, const QString& alias = QString(), QDb::SortOrder sortOrder = QDb::OrderArbitrary)
   {
      return QDbTypedColumn<T>(addColumn((const QDbExpression&)column, alias, dbTypeFor(typeid(T)), sortOrder));
   }

   template<class T>
   QDbTypedColumn<T> addColumn(const QDbTypedExpression<T>& column, const QString& alias = QString(), QDb::SortOrder sortOrder = QDb::OrderArbitrary)
   {
      return QDbTypedColumn<T>(addColumn((const QDbExpression&)column, alias, dbTypeFor(typeid(T)), sortOrder));
   }

   // To get a valid column the given column must be a table column or an alias has to be specified.
   QDbColumn addColumn(const QDbExpression& column, const QString& alias = QString(), QDbDataType type = DT_UNKNOWN, QDb::SortOrder sortOrder = QDb::OrderArbitrary);

   QDbSelector& where(const QDbExpression &constraint);

   QDbSelector& distinct(bool isDistinct = true);

   QDbSelector& having(const QDbExpression &constraint);

   // Returns a column that contains the command as select statement with the given column as selected column
   template <class T>
   QDbTypedExpression<T> asColumn(const QDbTypedColumn<T>& column)
   {
      return QDbTypedExpression<T>(asColumn((const QDbExpression&)column));
   }

   // Returns a column that contains the command as select statement with the given column as selected column
   template <class T>
   QDbTypedExpression<T> asColumn(const QDbTypedExpression<T>& column)
   {
      return QDbTypedExpression<T>(asColumn((const QDbExpression&)column));
   }

   // Returns a column that contains the command as select statement. Make sure there is exactly one defined column!
   QDbExpression asColumn() const;

   // Returns a column that contains the command as select statement with the given column as selected column
   QDbExpression asColumn(const QDbExpression& column, QDbDataType type = DT_UNKNOWN);

   QDbExpression exists(const QDbExpression& column); // The column specifies the value to check for existence

private:
   QDbCommandData* commandData();
   const QDbCommandData* commandData() const;
};

typedef QDbSelector QDbInlineTable;

#endif // QDBSELECTOR_H
