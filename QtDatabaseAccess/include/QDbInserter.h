#ifndef QDBINSERTER_H
#define QDBINSERTER_H

#include "qtdatabaseaccess_global.h"
#include "QDbAbstractReader.h"
#include "QDbAssignment.h"
#include "QDbValuesTable.h"
#include <QRational>

class QDbTable;

class QTDATABASEACCESS_EXPORT QDbInserter : public QDbAbstractReader
{
public:
   QDbInserter();
   QDbInserter(const QDbConnection& connection);
   QDbInserter(const QDbTable& table, const QDbConnection& connection = QDbConnection());
   QDbInserter(const QDbInserter& other);
   ~QDbInserter() override;

   QDbInserter& operator=(const QDbInserter& other);

   QDbInserter& set(const QDbAssignment& assignment);
   QDbInserter& operator<<(const QDbAssignment& assignment);

   QDbInserter& set(const QDbColumn& left, const QDbExpression& right);
   QDbInserter& set(const QDbColumn& left, bool value);
   QDbInserter& set(const QDbColumn& left, int value);
   QDbInserter& set(const QDbColumn& left, long long int value);
   QDbInserter& set(const QDbColumn& left, double value);
   QDbInserter& set(const QDbColumn& left, const char *value);
   QDbInserter& set(const QDbColumn& left, const QString& value);
   QDbInserter& set(const QDbColumn& left, const QRational& value);
   QDbInserter& set(const QDbColumn& left, const QDecimal& value);
   QDbInserter& set(const QDbColumn& left, const QDate& value);
   QDbInserter& set(const QDbColumn& left, const QTime& value);
   QDbInserter& set(const QDbColumn& left, const QDateTime& value);
   QDbInserter& set(const QDbColumn& left, const QDateTimeEx& value);
   QDbInserter& set(const QDbColumn& left, const QByteArray& value);
   QDbInserter& set(const QDbColumn& left, const Variant& value);

   // Bind a value to a column so it can be set several times
   bp<QSharedValue> set(QDbColumn column);

   void stashRow();
   void stashClear();

   QDbInserter& from(const QDbDataSource& dataSource);

   QDbInserter& where(const QDbExpression &constraint) { QDbAbstractReader::setWhere(constraint); return *this; }

   QDbInserter& distinct(bool isDistinct = true) { QDbAbstractReader::setDistinct(isDistinct); return *this; }

   QDbInserter& having(const QDbExpression &constraint) { QDbAbstractReader::setHaving(constraint); return *this; }

   QDbInserter& limit(int rows) { QDbAbstractReader::setLimit(rows); return *this; }
   QDbInserter& offset(int rows) { QDbAbstractReader::setOffset(rows); return *this; }

   bool usePostLob() const;
   void setUsePostLob(bool usePostLob);

   bool hasAssignments() const;

   int affectedRows();

public:
   QList<QDbStatement> createSqlStatements(const QDbSqlContext& sqlContext) const override;

   bool execute(const QDbConnection& connection = QDbConnection()) override;

private:
   QDbState processResult(SqlStyle sqlStyle, QDbResultSet result, const QList<bp<QDbBoundColumn>>& bindings);

private:
   int _affectedRows = -1;
   QDbValuesTable _valuesTable;
};

typedef QDbInserter QDbInsertInto;

#endif // QDBINSERTER_H
