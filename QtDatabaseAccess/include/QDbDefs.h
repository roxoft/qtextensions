#ifndef QDBDEFS_H
#define QDBDEFS_H

namespace QDb
{
   enum SortOrder { OrderArbitrary, OrderAscending, OrderDescending };
}

#endif // QDBDEFS_H
