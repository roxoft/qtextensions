#ifndef QDBEXPRESSION_H
#define QDBEXPRESSION_H

#include "qtdatabaseaccess_global.h"
#include <ExpressionBase.h>
#include <Variant.h>
#include <SqlParser.h>

class QDbSqlBuilderContext;
class QDbExpressionData;
class QDbXmlWriter;
class QDbTableData;
class QSharedValue;
class QDbSelector;
template <typename T>
class QDbTypedExpression;

/*
 * This class acts as data element (column) in an sql statement.
 *
 * A QDbExpression can be a plain table column or an expression of any kind.
 *
 * Though this class describes sql data in an sql statement it contains no value except if it is a literal.
 * For output values a `DbColumnBinding` class must be used.
 */
class QTDATABASEACCESS_EXPORT QDbExpression
{
   friend class QDbJoinedTableData;
   friend class QDbAbstractReader;
   friend class QDbReader;
   friend class QDbSelector;
   friend class QDbUpdater;
   friend class QDbMerger;
   friend class QDbDeleter;
   friend class QDbCommandData;
   friend class QDbAssignmentParts;
   friend class QDbValuesTable;
   friend class QDbValuesTableData;
   friend class QDbColumn;
   friend class QDbBoundColumn;
   friend class QDbSelectParts;

public:
   QDbExpression(); // Cannot be inline or default because of sp<QDbExprData>
   QDbExpression(const Variant& value);
   explicit QDbExpression(const bp<QSharedValue>& boundValue);
   QDbExpression(const QString& sqlTemplate, bool isAggregate, const QList<QDbExpression>& arguments = QList<QDbExpression>());
   QDbExpression(const QDbExpression& leftOperand, SqlOperator::Action op, const QDbExpression& rightOperand);
   QDbExpression(SqlOperator::Action op);
   QDbExpression(const QDbSelector& selector);
   QDbExpression(const QDbExpression& other); // Cannot be inline or default because of sp<QDbExprData>
   ~QDbExpression(); // Cannot be inline or default because of sp<QDbExpressionData>

   QDbExpression& operator=(const QDbExpression& other); // Cannot be inline or default because of sp<QDbExprData>

   bool isValid() const { return _data.isValid(); }
   bool isEqualTo(const QDbExpression& other) const;

   QString tableColumnName() const;

   //
   // Constraints
   //

   QDbExpression operator==(const QDbExpression& rhs) const;
   QDbExpression operator==(const Variant& value) const;

   QDbExpression operator!=(const QDbExpression& rhs) const;
   QDbExpression operator!=(const Variant& value) const;

   QDbExpression isNull() const;
   QDbExpression isNotNull() const;

   QDbExpression operator<(const QDbExpression& rhs) const;
   QDbExpression operator<(const Variant& value) const;

   QDbExpression operator<=(const QDbExpression& rhs) const;
   QDbExpression operator<=(const Variant& value) const;

   QDbExpression operator>(const QDbExpression& rhs) const;
   QDbExpression operator>(const Variant& value) const;

   QDbExpression operator>=(const QDbExpression& rhs) const;
   QDbExpression operator>=(const Variant& value) const;

   QDbExpression like(const QDbExpression& rhs) const;
   QDbExpression like(const Variant& value) const;

   QDbExpression unlike(const QDbExpression& rhs) const;
   QDbExpression unlike(const Variant& value) const;

   QDbExpression startsWith(const QString& text) const;
   QDbExpression startsNotWith(const QString& text) const;
   QDbExpression endsWith(const QString& text) const;
   QDbExpression endsNotWith(const QString& text) const;
   QDbExpression contains(const QString& text) const;
   QDbExpression containsNot(const QString& text) const;

   QDbExpression between(const QDbExpression& lowerBound, const QDbExpression& upperBound) const;
   QDbExpression between(const Variant& lowerBound, const Variant& upperBound) const;
   QDbExpression outside(const QDbExpression& lowerBound, const QDbExpression& upperBound) const;
   QDbExpression outside(const Variant& lowerBound, const Variant& upperBound) const;

   template <typename T>
   QDbExpression in(const QList<T>& values) const
   {
      QList<QDbExpression> columnList;

      for (int i = 0; i < values.count(); ++i)
         columnList.append(Variant(values.at(i)));

      return colIn(columnList, false);
   }
   QDbExpression in(const QList<QDbExpression>& columns) const;
   QDbExpression in(const QList<Variant>& values) const;
   QDbExpression in(const QDbExpression& values) const; // values should be a subselect

   QDbExpression notIn(const QList<QDbExpression>& columns) const;
   QDbExpression notIn(const QList<Variant>& values) const;
   QDbExpression notIn(const QDbExpression& values) const; // values should be a subselect

   QDbExpression includes(const QDbExpression& column) const; // column in this
   QDbExpression excludes(const QDbExpression& column) const; // column not in this

   // Left outer join -- DEPRECATED! Use QDbDataSource::leftJoin instead --

   QDbExpression operator>>=(const QDbExpression& rhs) const;

   // Right outer join -- DEPRECATED! Use QDbDataSource::rightJoin instead --
   // Remark: Sybase doesn't support outer joins with constants

   QDbExpression operator<<=(const QDbExpression& rhs) const;
   QDbExpression operator<<=(const Variant& value) const;

   //
   // Operations
   //

   QDbExpression operator+(const QDbExpression& rhs) const;
   QDbExpression operator+(const Variant& value) const;
   QDbExpression operator-(const QDbExpression& rhs) const;
   QDbExpression operator-(const Variant& value) const;
   QDbExpression operator*(const QDbExpression& rhs) const;
   QDbExpression operator*(const Variant& value) const;
   QDbExpression operator/(const QDbExpression& rhs) const;
   QDbExpression operator/(const Variant& value) const;

   QDbExpression operator&&(const QDbExpression& rhs) const;
   QDbExpression operator||(const QDbExpression& rhs) const;

   QDbExpression operator!() const;

   QDbExpression& operator&=(const QDbExpression& rhs);
   QDbExpression& operator|=(const QDbExpression& rhs);

   QDbTypedExpression<QString> upperCase() const;

   QDbExpression ConcatWith(const QDbExpression& rhs) const;
   QDbExpression ConcatWith(const QString& text) const;

   template <typename T>
   static QDbTypedExpression<T> caseWhen(const QDbExpression& constraint, const QDbExpression& truePart, const QDbExpression& falsePart = QDbExpression())
   {
      if (!falsePart.isValid())
         return QDbExpression("CASE WHEN %1 THEN %2 END", false, QList<QDbExpression>() << constraint << truePart);
      return QDbExpression("CASE WHEN {0} THEN {1} ELSE {2} END", false, QList<QDbExpression>() << constraint << truePart << falsePart);
   }

   //
   // Aggregations
   //

   QDbTypedExpression<qint64> count() const;
   QDbExpression max() const;
   QDbExpression min() const;
   QDbExpression sum() const;

   bool isAggregate() const;
   bool isGroupable() const;

   //
   // Expression interface
   //

   bool isValue() const; // Returns true if the expression is not an operator tree (a value, query or expression)

   //void addOperand(const QDbExpression& operand) { addOperand(QDbExpression(operand)); }
   void addOperand(const QDbExpression& operand);

   void toXml(QDbXmlWriter *writer) const;

private:
   QDbExpression colIn(const QList<QDbExpression>& columns, bool negate) const;

private:
   sp<QDbExpressionData> _data;
};

#endif // QDBEXPRESSION_H
