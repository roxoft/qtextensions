#ifndef QDBASSIGNMENT_H
#define QDBASSIGNMENT_H

#include "qtdatabaseaccess_global.h"
#include "QDbExpression.h"
#include "QDbColumn.h"

class QTDATABASEACCESS_EXPORT QDbAssignment
{
public:
   QDbAssignment();
   QDbAssignment(const QDbColumn& left, const Variant& right);
   QDbAssignment(const QDbColumn& left, const QDbExpression& right);
   ~QDbAssignment();

   const QDbColumn& left() const { return _left; }
   const QDbExpression& right() const { return _right; }

private:
   QDbColumn      _left;
   QDbExpression  _right;
};

#endif // QDBASSIGNMENT_H
