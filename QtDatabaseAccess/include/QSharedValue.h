#pragma once
#include <Variant.h>
#include <QSharedData>

class QSharedValue : public QSharedData
{
public:
   QSharedValue() {}
   virtual ~QSharedValue() {}

   virtual const Variant& value() const { return _value; }
   virtual void setValue(const Variant& value) { _value = value; }

protected:
   Variant _value;
};
