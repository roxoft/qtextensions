#pragma once
#include <Variant.h>
#include <QDbSchema.h>
#include "QSharedValue.h"
#include <SmartPointer.h>

class QDbParameter
{
public:
   QDbParameter() = default;
   ~QDbParameter() = default;

   QString name;

   Variant value;
   bp<QSharedValue> boundValue;

   QDbDataType type = DT_UNKNOWN;
   QDbTransferMode transferMode = TM_Default;

   bool isPostLob = false;

   const Variant& getValue() const { return boundValue ? boundValue->value() : value; }
   void setValue(const Variant& v) { if (boundValue) boundValue->setValue(v); else value = v; }
};
