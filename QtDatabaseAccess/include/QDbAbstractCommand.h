#ifndef QDBABSTRACTCOMMAND_H
#define QDBABSTRACTCOMMAND_H

#include "qtdatabaseaccess_global.h"
#include <QList>
#include <QDbConnection.h>
#include "QDbStatement.h"
#include "QDbSqlContext.h"
#include <SmartPointer.h>

class QDbCommandData;

class QTDATABASEACCESS_EXPORT QDbAbstractCommand
{
protected:
   QDbAbstractCommand();
   QDbAbstractCommand(const QDbConnection& connection);
   QDbAbstractCommand(const QDbAbstractCommand& other);
   virtual ~QDbAbstractCommand();

   QDbAbstractCommand& operator=(const QDbAbstractCommand& other);

public:
   virtual QList<QDbStatement> createSqlStatements(const QDbSqlContext& sqlContext) const = 0;

   virtual bool execute(const QDbConnection& connection = QDbConnection()) = 0;

   bool hasError() const;
   QString lastError() const;
   const QDbState& state() const;

protected:
   QDbConnection _connection;
   QDbState _state;
   bp<QDbCommandData> _data;
};

#endif // QDBABSTRACTCOMMAND_H
