#include "qtdatabaseaccess_global.h"
#include "QDbDataSource.h"
#include <QRational.h>
#include "QDbColumn.h"

#pragma once

class QTDATABASEACCESS_EXPORT QDbValuesTable : public QDbDataSource
{
public:
    QDbValuesTable(); // Cannot use default because of QDbDataSource::_data
    QDbValuesTable(const QString& name);
    QDbValuesTable(const QDbValuesTable& other);
    ~QDbValuesTable(); // Cannot use default because of QDbDataSource::_data

    QDbColumn column(const QString& name);

    QDbValuesTable& operator=(const QDbValuesTable& other);

    QDbValuesTable& set(const QDbColumn& column, const QDbExpression& value);

    QDbValuesTable& set(const QDbColumn& column, bool value);
    QDbValuesTable& set(const QDbColumn& column, int value);
    QDbValuesTable& set(const QDbColumn& column, long long int value);
    QDbValuesTable& set(const QDbColumn& column, double value);
    QDbValuesTable& set(const QDbColumn& column, const char *value);
    QDbValuesTable& set(const QDbColumn& column, const QString& value);
    QDbValuesTable& set(const QDbColumn& column, const QRational& value);
    QDbValuesTable& set(const QDbColumn& column, const QDecimal& value);
    QDbValuesTable& set(const QDbColumn& column, const QDate& value);
    QDbValuesTable& set(const QDbColumn& column, const QTime& value);
    QDbValuesTable& set(const QDbColumn& column, const QDateTime& value);
    QDbValuesTable& set(const QDbColumn& column, const QDateTimeEx& value);
    QDbValuesTable& set(const QDbColumn& column, const QByteArray& value);
    QDbValuesTable& set(const QDbColumn& column, const Variant& value);

    void stashRow();
    int rowCount() const;
    void stashClear();
};
