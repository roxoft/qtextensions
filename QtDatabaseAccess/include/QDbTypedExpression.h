#ifndef QDBTYPEDEXPRESSION_H
#define QDBTYPEDEXPRESSION_H

#include "QDbColumn.h"

// This class just supplies a type to an expression.
// It is used to get binding columns of the correct type.
template <typename T>
class QDbTypedExpression : public QDbExpression
{
public:
   QDbTypedExpression() = default;
   QDbTypedExpression(const QDbExpression& expression) : QDbExpression(expression) {}
   ~QDbTypedExpression() = default;
};

// This class just supplies a type to a column.
// It is used to get binding columns of the correct type.
template <typename T>
class QDbTypedColumn : public QDbColumn
{
public:
   QDbTypedColumn() = default;
   QDbTypedColumn(const QDbColumn& column) : QDbColumn(column) {}
   ~QDbTypedColumn() = default;
};

#endif // QDBTYPEDEXPRESSION_H
