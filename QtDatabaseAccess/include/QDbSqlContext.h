#pragma once
#include <QDbConnection.h>

class QDbSqlContext
{
public:
   enum class PhUsage { Optimized, Required, Always };

public:
   QDbSqlContext(const QDbConnection& connection, PhUsage phUsage = PhUsage::Optimized)
      : _connection(connection), _phUsage(phUsage)
   {
   }

   const QDbConnection& connection() const { return _connection; }

   PhUsage phUsage() const { return _phUsage; }

private:
   QDbConnection _connection;
   PhUsage _phUsage = PhUsage::Optimized;
};
