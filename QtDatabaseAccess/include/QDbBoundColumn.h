#ifndef QDBBOUNDCOLUMN_H
#define QDBBOUNDCOLUMN_H

#include "qtdatabaseaccess_global.h"
#include <QDbSchema.h>
#include "QDbDefs.h"
#include "QDbExpression.h"
#include <SmartPointer.h>
#include "QSharedValue.h"

// This class represents one column of a select result and where to store the resulting value
class QTDATABASEACCESS_EXPORT QDbBoundColumn : public QSharedData
{
public:
   QDbBoundColumn(const QDbExpression& c, QDbDataType t);
   ~QDbBoundColumn();

   QDbExpression column;
   QDbDataType type = DT_UNKNOWN;
   QString alias;
   QDb::SortOrder sortOrder = QDb::OrderArbitrary;
   bp<QSharedValue> boundValue;
};

#endif // QDBBOUNDCOLUMN_H
