#ifndef QDBTABLE_H
#define QDBTABLE_H

#include "qtdatabaseaccess_global.h"
#include "QDbDataSource.h"
#include "QDbTypedExpression.h"
#include <QDbSchema.h>

class QDbDeleter;
class QDbUpdater;
class QDbMerger;
class QDbInserter;
class QDbPlainTableData;
class QDbConnection;

/*
 * Represents a plain database table (a table name in the database)
 * 
 * It is the starting point for all insert/update/delete operations.
 */
class QTDATABASEACCESS_EXPORT QDbTable : public QDbDataSource
{
   friend class QDbInserterData;
   friend class QDbUpdaterData;
   friend class QDbDeleterData;

public:
   QDbTable(); // Cannot use default because of QDbDataSource::_data
   QDbTable(QString tableName, QString alias = QString());
   QDbTable(const QDbTable& other);
   QDbTable(const QDbDataSource& dataSource);
   ~QDbTable(); // Cannot use default because of QDbDataSource::_data

   QDbTable& operator=(const QDbTable& other);

   QString name() const;

   QDbInserter inserter() const;
   QDbUpdater updater() const;
   QDbMerger merger() const;
   QDbDeleter deleter() const;

   QDbColumn column(const QString& name) const;

   template <typename T>
   QDbTypedColumn<T> typedColumn(const QString& name) const
   {
      return QDbTypedColumn<T>(column(name));
   }

   QDbSchema::Table schema(const QDbConnection& connection) const;
};

#endif // QDBTABLE_H
