#ifndef QDBJOINEDTABLE_H
#define QDBJOINEDTABLE_H

#include "qtdatabaseaccess_global.h"
#include "QDbDataSource.h"

class QTDATABASEACCESS_EXPORT QDbJoinedTable : public QDbDataSource
{
public:
   enum JoinMode
   {
      InnerJoin,
      LeftJoin,
      RightJoin,
      OuterJoin,
      CrossJoin
   };

public:
   QDbJoinedTable(); // Cannot use default because of QDbDataSource::_data
   QDbJoinedTable(const QDbDataSource& left, const QDbDataSource& right, const QDbExpression& condition = QDbExpression(), JoinMode joinMode = CrossJoin); // If the joinMode is CrossJoin but a condition is given the joinMode is changed to OuterJoin
   QDbJoinedTable(const QDbJoinedTable& other);
   ~QDbJoinedTable(); // Cannot use default because of QDbDataSource::_data

   QDbJoinedTable& operator=(const QDbJoinedTable& other);

   QDbJoinedTable& on(const QDbExpression& condition);

   void setJoinMode(JoinMode joinMode);
};

#endif // QDBJOINEDTABLE_H
