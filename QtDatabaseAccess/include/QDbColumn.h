#ifndef QDBCOLUMN_H
#define QDBCOLUMN_H

#include "qtdatabaseaccess_global.h"
#include "QDbExpression.h"

class QDbDataSource;
class QDbAssignment;

class QTDATABASEACCESS_EXPORT QDbColumn : public QDbExpression
{
public:
   QDbColumn() = default;
   QDbColumn(const QDbDataSource& table, const QString& name);
   QDbColumn(const QDbColumn& other) = default;
   ~QDbColumn() = default;

   QDbColumn& operator=(const QDbColumn& other) = default;

   QDbDataSource table() const;
   QString name() const;
   //QDbSchema::Column schema() const;

   //
   // Assignment
   //

   QDbAssignment to(const QDbExpression& rhs) const;
   QDbAssignment to(const Variant& value) const;
};

#endif // QDBCOLUMN_H
