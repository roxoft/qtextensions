#ifndef QDBTYPEDREADER_H
#define QDBTYPEDREADER_H

#include "QDbReader.h"
#include <QList>
#include <QSharedPointer>

template <class T>
class QDbValueProxy
{
public:
   QDbValueProxy() {}
   virtual ~QDbValueProxy() {}

   void addValue(const bp<QSharedValue>& boundValue)
   {
      _values.append(boundValue);
   }

   virtual void set(T* element) = 0;

protected:
   QList<bp<QSharedValue>> _values;
};

template <class T, typename V>
class QDbValueProxyFunction : public QDbValueProxy<T>
{
public:
   QDbValueProxyFunction(void (*setter)(T*, const V&)) : _setter(setter) {}
   ~QDbValueProxyFunction() override {}

   void set(T* element) override
   {
      bool ok;
      _setter(element, QDbValueProxy<T>::_values.first()->value().template to<V>(&ok, QLocale::C));
      Q_ASSERT(ok);
   }

private:
   void (*_setter)(T*, const V&);
};

template <class T>
class QDbValueProxyFunction<T, Variant> : public QDbValueProxy<T>
{
public:
   QDbValueProxyFunction(void(*setter)(T*, const Variant&)) : _setter(setter) {}
   ~QDbValueProxyFunction() override {}

   void set(T* element) override
   {
      _setter(element, QDbValueProxy<T>::_values.first()->value());
   }

private:
   void(*_setter)(T*, const Variant&);
};

template <class T, typename V>
class QDbValueProxyLambda : public QDbValueProxy<T>
{
public:
   QDbValueProxyLambda(std::function<void(T*, const V&)> setter) : _setter{ std::move(setter) } {}
   ~QDbValueProxyLambda() override {}

   void set(T* element) override
   {
      bool ok;
      _setter(element, QDbValueProxy<T>::_values.first()->value().template to<V>(&ok, QLocale::C));
      Q_ASSERT(ok);
   }

private:
   std::function<void(T*, const V&)> _setter;
};

template <class T>
class QDbValueProxyLambda<T, Variant> : public QDbValueProxy<T>
{
public:
   QDbValueProxyLambda(std::function<void(T*, const Variant&)> setter) : _setter{ std::move(setter) } {}
   ~QDbValueProxyLambda() override {}

   void set(T* element) override
   {
      _setter(element, QDbValueProxy<T>::_values.first()->value());
   }

private:
   std::function<void(T*, const Variant&)> _setter;
};

template <class T, typename V1, typename V2>
class QDbValueProxyFunction2 : public QDbValueProxy<T>
{
public:
   QDbValueProxyFunction2(void(*setter)(T*, const V1&, const V2&)) : _setter(setter) {}
   ~QDbValueProxyFunction2() override {}

   void set(T* element) override
   {
      bool ok;
      _setter(element, QDbValueProxy<T>::_values.first()->value().template to<V1>(&ok, QLocale::C), QDbValueProxy<T>::_values.first()->value().template to<V2>(&ok, QLocale::C));
      Q_ASSERT(ok);
   }

private:
   void(*_setter)(T*, const V1&, const V2&);
};

template <class T, typename V1, typename V2>
class QDbValueProxyLambda2 : public QDbValueProxy<T>
{
public:
   QDbValueProxyLambda2(std::function<void(T*, const V1&, const V2&)> setter) : _setter{ std::move(setter) } {}
   ~QDbValueProxyLambda2() override {}

   void set(T* element) override
   {
      bool ok;
      _setter(element, QDbValueProxy<T>::_values.first()->value().template to<V1>(&ok, QLocale::C), QDbValueProxy<T>::_values.first()->value().template to<V2>(&ok, QLocale::C));
      Q_ASSERT(ok);
   }

private:
   std::function<void(T*, const V1&, const V2&)> _setter;
};

template <class T, typename V>
class QDbValueProxyMemberFunction : public QDbValueProxy<T>
{
public:
   QDbValueProxyMemberFunction(void (T::*setter)(const V&)) : _setter(setter) {}
   ~QDbValueProxyMemberFunction() override {}

   void set(T* element) override
   {
      bool ok;
      (element->*_setter)(QDbValueProxy<T>::_values.first()->value().template to<V>(&ok, QLocale::C));
      Q_ASSERT(ok);
   }

private:
   void (T::*_setter)(const V&);
};

template <class T>
class QDbValueProxyMemberFunction<T, Variant> : public QDbValueProxy<T>
{
public:
   QDbValueProxyMemberFunction(void (T::*setter)(const Variant&)) : _setter(setter) {}
   ~QDbValueProxyMemberFunction() override {}

   void set(T* element) override
   {
      (element->*_setter)(QDbValueProxy<T>::_values.first()->value());
   }

private:
   void (T::*_setter)(const Variant&);
};

template <class T, typename V>
class QDbValueProxyMember : public QDbValueProxy<T>
{
public:
   QDbValueProxyMember(V T::* member) : _member(member) {}
   ~QDbValueProxyMember() override {}

   void set(T* element) override
   {
      bool ok;
      element->*_member = QDbValueProxy<T>::_values.first()->value().template to<V>(&ok, QLocale::C);
      Q_ASSERT(ok);
   }

private:
   V T::* _member;
};

template <class T>
class QDbTypedReader : public QDbReader
{
public:
   QDbTypedReader() {}
   QDbTypedReader(const QDbTypedReader<T>& other) : QDbReader(other), _proxies(other._proxies) {}
   QDbTypedReader(const QDbDataSource &table, const QDbConnection& connection = QDbConnection()) : QDbReader(table, connection) {}
   ~QDbTypedReader() {}

   QDbTypedReader<T>& operator=(const QDbTypedReader<T>& other)
   {
      QDbReader::operator=(other);
      _proxies = other._proxies;
      return *this;
   }

   void setValues(T* object)
   {
      for (auto&& proxy : _proxies)
      {
         proxy->set(object);
      }
   }

   bool readNextInto(T* object)
   {
      if (!readNext())
         return false;

      setValues(object);
      return true;
   }

   QList<T> readAll(const QDbConnection& connection = QDbConnection());

   template <typename V>
   QDbTypedReader<T>& map(QDbTypedColumn<V> column, V T::*member)
   {
      auto proxy = new QDbValueProxyMember<T, V>(member);

      proxy->addValue(get((const QDbExpression&)column).boundValue());

      _proxies.append(QSharedPointer<QDbValueProxy<T>>(proxy));

      return *this;
   }

   template <typename V>
   QDbTypedReader<T>& map(QDbTypedExpression<V> column, V T::*member)
   {
      auto proxy = new QDbValueProxyMember<T, V>(member);

      proxy->addValue(get((const QDbExpression&)column).boundValue());

      _proxies.append(QSharedPointer<QDbValueProxy<T>>(proxy));

      return *this;
   }

   template <typename V>
   QDbTypedReader<T>& map(QDbTypedColumn<V> column, void(*setter)(T*, const V&))
   {
      auto proxy = new QDbValueProxyFunction<T, V>(setter);

      proxy->addValue(get((const QDbExpression&)column).boundValue());

      _proxies.append(QSharedPointer<QDbValueProxy<T>>(proxy));

      return *this;
   }

   template <typename V>
   QDbTypedReader<T>& map(QDbTypedExpression<V> column, void(*setter)(T*, const V&))
   {
      auto proxy = new QDbValueProxyFunction<T, V>(setter);

      proxy->addValue(get((const QDbExpression&)column).boundValue());

      _proxies.append(QSharedPointer<QDbValueProxy<T>>(proxy));

      return *this;
   }

   QDbTypedReader<T>& map(QDbExpression column, void(*setter)(T*, const Variant&))
   {
      auto proxy = new QDbValueProxyFunction<T, Variant>(setter);

      proxy->addValue(get(column).boundValue());

      _proxies.append(QSharedPointer<QDbValueProxy<T>>(proxy));

      return *this;
   }

   template <typename V1, typename V2>
   QDbTypedReader<T>& map(QDbTypedColumn<V1> column1, QDbTypedColumn<V2> column2, void(*setter)(T*, const V1&, const V2&))
   {
      auto proxy = new QDbValueProxyFunction2<T, V1, V2>(setter);

      proxy->addValue(get((const QDbExpression&)column1).boundValue());
      proxy->addValue(get((const QDbExpression&)column2).boundValue());

      _proxies.append(QSharedPointer<QDbValueProxy<T>>(proxy));

      return *this;
   }

   template <typename V1, typename V2>
   QDbTypedReader<T>& map(QDbTypedExpression<V1> column1, QDbTypedExpression<V2> column2, void(*setter)(T*, const V1&, const V2&))
   {
      auto proxy = new QDbValueProxyFunction2<T, V1, V2>(setter);

      proxy->addValue(get((const QDbExpression&)column1).boundValue());
      proxy->addValue(get((const QDbExpression&)column2).boundValue());

      _proxies.append(QSharedPointer<QDbValueProxy<T>>(proxy));

      return *this;
   }

   template <typename V>
   QDbTypedReader<T>& map(QDbTypedColumn<V> column, void (T::*setter)(const V&))
   {
      auto proxy = new QDbValueProxyMemberFunction<T, V>(setter);

      proxy->addValue(get((const QDbExpression&)column).boundValue());

      _proxies.append(QSharedPointer<QDbValueProxy<T>>(proxy));

      return *this;
   }

   template <typename V>
   QDbTypedReader<T>& map(QDbTypedExpression<V> column, void (T::*setter)(const V&))
   {
      auto proxy = new QDbValueProxyMemberFunction<T, V>(setter);

      proxy->addValue(get((const QDbExpression&)column).boundValue());

      _proxies.append(QSharedPointer<QDbValueProxy<T>>(proxy));

      return *this;
   }

   QDbTypedReader<T>& map(QDbExpression column, void (T::*setter)(const Variant&))
   {
      auto proxy = new QDbValueProxyMemberFunction<T, Variant>(setter);

      proxy->addValue(get(column).boundValue());

      _proxies.append(QSharedPointer<QDbValueProxy<T>>(proxy));

      return *this;
   }

   // For lambda expressions

   template <typename V, typename L>
   QDbTypedReader<T>& map(QDbTypedColumn<V> column, L setter)
   {
      auto proxy = new QDbValueProxyLambda<T, V>(setter);

      proxy->addValue(get((const QDbExpression&)column).boundValue());

      _proxies.append(QSharedPointer<QDbValueProxy<T>>(proxy));

      return *this;
   }

   template <typename V, typename L>
   QDbTypedReader<T>& map(QDbTypedExpression<V> column, L setter)
   {
      auto proxy = new QDbValueProxyLambda<T, V>(setter);

      proxy->addValue(get((const QDbExpression&)column).boundValue());

      _proxies.append(QSharedPointer<QDbValueProxy<T>>(proxy));

      return *this;
   }

   template <typename L>
   QDbTypedReader<T>& map(QDbExpression column, L setter)
   {
      auto proxy = new QDbValueProxyLambda<T, Variant>(setter);

      proxy->addValue(get(column).boundValue());

      _proxies.append(QSharedPointer<QDbValueProxy<T>>(proxy));

      return *this;
   }

   template <typename V1, typename V2, typename L>
   QDbTypedReader<T>& map(QDbTypedColumn<V1> column1, QDbTypedColumn<V2> column2, L setter)
   {
      auto proxy = new QDbValueProxyLambda2<T, V1, V2>(setter);

      proxy->addValue(get((const QDbExpression&)column1).boundValue());
      proxy->addValue(get((const QDbExpression&)column2).boundValue());

      _proxies.append(QSharedPointer<QDbValueProxy<T>>(proxy));

      return *this;
   }

   template <typename V1, typename V2, typename L>
   QDbTypedReader<T>& map(QDbTypedExpression<V1> column1, QDbTypedExpression<V2> column2, L setter)
   {
      auto proxy = new QDbValueProxyLambda2<T,V1, V2>(setter);

      proxy->addValue(get((const QDbExpression&)column1).boundValue());
      proxy->addValue(get((const QDbExpression&)column2).boundValue());

      _proxies.append(QSharedPointer<QDbValueProxy<T>>(proxy));

      return *this;
   }

private:
   QList<QSharedPointer<QDbValueProxy<T>>> _proxies;
};

template <class T>
QList<T> QDbTypedReader<T>::readAll(const QDbConnection& connection)
{
   QList<T> result;

   if (!execute(connection))
      return result;

   while (readNext())
   {
      T element;

      setValues(&element);

      result.append(element);
   }

   return result;
}

#endif // QDBTYPEDREADER_H
