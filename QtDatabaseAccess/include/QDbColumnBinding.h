#ifndef QDBCOLUMNBINDING_H
#define QDBCOLUMNBINDING_H

#include "QSharedValue.h"
#include "QDbBoundColumn.h"
#include <Variant.h>
#include <nullable.h>

template <typename T>
class QDbTypedValueBinding : public QSharedValue
{
public:
   QDbTypedValueBinding(T* boundValue) : _boundValue(boundValue) {}
   ~QDbTypedValueBinding() override = default;

   void setValue(const Variant& value) override { _value = value; bool ok; *_boundValue = _value.to<T>(&ok, QLocale::C); }

private:
   T* _boundValue;
};

template <>
class QDbTypedValueBinding<Variant> : public QSharedValue
{
public:
   QDbTypedValueBinding(Variant* boundValue) : _boundValue(boundValue) {}
   ~QDbTypedValueBinding() override = default;

   void setValue(const Variant& value) override { _value = value; *_boundValue = _value; }

private:
   Variant* _boundValue;
};

template <typename U>
class QDbTypedValueBinding<nullable<U>> : public QSharedValue
{
public:
   QDbTypedValueBinding(nullable<U>* boundValue) : _boundValue(boundValue) {}
   ~QDbTypedValueBinding() override = default;

   void setValue(const Variant& value) override
   {
      _value = value;
      if (value.isNull())
      {
         *_boundValue = nullable<U>();
      }
      else
      {
         bool ok;
         *_boundValue = _value.to<U>(&ok, QLocale::C);
      }
   }

private:
   nullable<U>* _boundValue;
};

class QDbColumnBindingBase
{
   friend class QDbAbstractReader;
public:
   QDbColumnBindingBase() = default;
   QDbColumnBindingBase(const bp<QDbBoundColumn>& boundColumn) : _column(boundColumn) {}
   ~QDbColumnBindingBase() = default;

   bool isBound() const { return !_column.isNull(); }

   bool isNull() const { return _column.isNull() || _column->boundValue->value().isValueNull(); }

   template<typename B>
   void into(B* value) { if (_column) _column->boundValue = new QDbTypedValueBinding<B>(value); }

   void orderAscending() { _column->sortOrder = QDb::OrderAscending; }
   void orderDescending() { _column->sortOrder = QDb::OrderDescending; }
   void orderArbitrary() { _column->sortOrder = QDb::OrderArbitrary; }

   const bp<QSharedValue>& boundValue() const { Q_ASSERT(_column); return _column->boundValue; }
   const bp<QDbBoundColumn>& boundColumn() const { return _column; }

protected:
   bp<QDbBoundColumn> _column;
};

// A bound column with a type.
// The type is used to access the value type safe.
template <typename T>
class QDbColumnBinding : public QDbColumnBindingBase
{
public:
   QDbColumnBinding() = default;
   QDbColumnBinding(const bp<QDbBoundColumn>& boundColumn) : QDbColumnBindingBase(boundColumn) {}
   ~QDbColumnBinding() = default;

   T value() const { if (_column.isNull()) return T(); bool ok; return _column->boundValue->value().to<T>(&ok, QLocale::C); }
};

template <>
class QDbColumnBinding<Variant> : public QDbColumnBindingBase
{
public:
   QDbColumnBinding() = default;
   QDbColumnBinding(const bp<QDbBoundColumn>& boundColumn) : QDbColumnBindingBase(boundColumn) {}
   ~QDbColumnBinding() = default;

   Variant value() const { if (_column.isNull()) return Variant::null; return _column->boundValue->value(); }
};

#endif // QDBCOLUMNBINDING_H
