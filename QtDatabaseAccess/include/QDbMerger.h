#pragma once

#include "qtdatabaseaccess_global.h"
#include "QDbAbstractReader.h"
#include "QDbAssignment.h"
#include <QRational>

class QDbTable;

class QTDATABASEACCESS_EXPORT QDbMerger : public QDbAbstractReader
{
public:
   QDbMerger();
   QDbMerger(const QDbConnection& connection);
   QDbMerger(const QDbTable& table, const QDbConnection& connection = QDbConnection());
   QDbMerger(const QDbMerger& other);
   ~QDbMerger() override;

   QDbMerger& operator=(const QDbMerger& other);

   QDbMerger& set(const QDbAssignment& assignment);
   QDbMerger& operator<<(const QDbAssignment& assignment);

   QDbMerger& set(const QDbColumn& left, const QDbExpression& right);
   QDbMerger& set(const QDbColumn& left, bool value);
   QDbMerger& set(const QDbColumn& left, int value);
   QDbMerger& set(const QDbColumn& left, long long int value);
   QDbMerger& set(const QDbColumn& left, double value);
   QDbMerger& set(const QDbColumn& left, const char *value);
   QDbMerger& set(const QDbColumn& left, const QString& value);
   QDbMerger& set(const QDbColumn& left, const QRational& value);
   QDbMerger& set(const QDbColumn& left, const QDecimal& value);
   QDbMerger& set(const QDbColumn& left, const QDate& value);
   QDbMerger& set(const QDbColumn& left, const QTime& value);
   QDbMerger& set(const QDbColumn& left, const QDateTime& value);
   QDbMerger& set(const QDbColumn& left, const QDateTimeEx& value);
   QDbMerger& set(const QDbColumn& left, const QByteArray& value);
   QDbMerger& set(const QDbColumn& left, const Variant& value);

   // Bind a value to a column so it can be set several times
   bp<QSharedValue> set(QDbColumn column);

   QDbMerger& from(const QDbDataSource& dataSource);

   QDbMerger& where(const QDbExpression &constraint) { QDbAbstractReader::setWhere(constraint); return *this; }

   QDbMerger& distinct(bool isDistinct = true) { QDbAbstractReader::setDistinct(isDistinct); return *this; }

   QDbMerger& having(const QDbExpression &constraint) { QDbAbstractReader::setHaving(constraint); return *this; }

   QDbMerger& limit(int rows) { QDbAbstractReader::setLimit(rows); return *this; }
   QDbMerger& offset(int rows) { QDbAbstractReader::setOffset(rows); return *this; }

   bool hasAssignments() const;

   int affectedRows();

public:
   QList<QDbStatement> createSqlStatements(const QDbSqlContext& sqlContext) const override;

   bool execute(const QDbConnection& connection = QDbConnection()) override;

private:
   int _affectedRows = -1;
};

typedef QDbMerger QDbMergeInto;
