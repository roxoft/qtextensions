#ifndef QDBREADER_H
#define QDBREADER_H

#include "qtdatabaseaccess_global.h"
#include "QDbAbstractReader.h"

class QDbDataSource;
class QDbExpression;

class QTDATABASEACCESS_EXPORT QDbReader : public QDbAbstractReader
{
public:
   QDbReader();
   QDbReader(const QDbConnection& connection);
   QDbReader(const QDbDataSource &table, const QDbConnection& connection = QDbConnection());
   QDbReader(const QDbDataSource &table, int rowsToFetch, const QDbConnection& connection = QDbConnection());
   QDbReader(const QDbReader& other);
   ~QDbReader() override;

   QDbReader& operator=(const QDbReader& other);

   QDbReader& where(const QDbExpression &constraint) { QDbAbstractReader::setWhere(constraint); return *this; }

   QDbReader& distinct(bool isDistinct = true) { QDbAbstractReader::setDistinct(isDistinct); return *this; }

   QDbReader& having(const QDbExpression &constraint) { QDbAbstractReader::setHaving(constraint); return *this; }

   QDbReader& limit(int rows) { QDbAbstractReader::setLimit(rows); return *this; }
   QDbReader& offset(int rows) { QDbAbstractReader::setOffset(rows); return *this; }

   QDbReader& orderBy(QDbColumnBindingBase column, QDb::SortOrder sortOrder = QDb::OrderAscending) { QDbAbstractReader::addOrderBy(column, sortOrder); return *this; }
   QDbReader& orderBy(const QDbColumn& column, QDb::SortOrder sortOrder = QDb::OrderAscending) { QDbAbstractReader::addOrderBy(column, sortOrder); return *this; }

   bool readNext();

   template<typename T>
   T readValue(QDbTypedColumn<T> column);

public:
   QList<QDbStatement> createSqlStatements(const QDbSqlContext& sqlContext) const override;

   bool execute(const QDbConnection& connection = QDbConnection()) override;
   bool execute(int rowsToFetch, const QDbConnection& connection = QDbConnection());

private:
   int _rowsToFetch = -1;
   QDbResultSet _result;
};

typedef QDbReader QDbSelectFrom;

template<typename T>
T QDbReader::readValue(QDbTypedColumn<T> column)
{
    auto boundColumn = get(column);

    if (!_result.isValid())
    {
        if (!execute())
        {
            return {};
        }
    }

    if (!readNext())
    {
        return {};
    }

    return boundColumn.value();
}

#endif // QDBREADER_H
