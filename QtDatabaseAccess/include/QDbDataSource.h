#ifndef QDbDataSource_H
#define QDbDataSource_H

#include "qtdatabaseaccess_global.h"
#include "QDbExpression.h"
#include "QDbTypedReader.h"

class QDbJoinedTable;
class QDbTableData;
class QDbReader;
class QDbSelector;

/*
 * Base class for all data sources (including tables) in the database.
 * 
 * QDbDataSource has polymorphic behaviour without being virtual. An object from a derived class can be assigned to QDbDataSource without loss of information.
 * Each instance of QDbDataSource is identified by an unique internal id which is generated in its constructor and is independent of the underlying data source.
 * Therefore two QDbTable objects with the same table name are still two different data sources. Use the same table object to refer to the same table (do not rely on the table names).
 * 
 * Data sources are:
 *  - QDbTable: Plain database table with named columns.
 *  - QDbJoinedTable: Two or more tables joined together.
 *  - QDbSelector: Data source based on a select statement.
 */
class QTDATABASEACCESS_EXPORT QDbDataSource
{
   friend class QDbJoinedTableData;
   friend class QDbExpression;
   friend class QDbColumn;
   friend class QDbReader;
   friend class QDbSelector;
   friend class QDbInserter;
   friend class QDbUpdater;
   friend class QDbMerger;
   friend class QDbDeleter;

public:
   QDbDataSource(); // Cannot use default because of _data
   QDbDataSource(const QDbDataSource& other);
   ~QDbDataSource(); // Cannot use default because of _data

   QDbDataSource& operator=(const QDbDataSource& other);

   bool operator==(const QDbDataSource& other) const;
   bool operator!=(const QDbDataSource& other) const { return !operator==(other); }
   bool isValid() const { return _data; }

   // Any tables can be joined
   QDbJoinedTable innerJoin(const QDbDataSource& table, const QDbExpression& condition = QDbExpression()) const;
   QDbJoinedTable leftJoin(const QDbDataSource& table, const QDbExpression& condition = QDbExpression()) const;
   QDbJoinedTable rightJoin(const QDbDataSource& table, const QDbExpression& condition = QDbExpression()) const;
   QDbJoinedTable outerJoin(const QDbDataSource& table, const QDbExpression& condition = QDbExpression()) const;
   QDbJoinedTable crossJoin(const QDbDataSource& table) const;

   QDbJoinedTable operator*(const QDbDataSource& table) const; // Same as crossJoin

   QDbReader reader(const QDbConnection& connection = QDbConnection()) const;
   template <class T>
   QDbTypedReader<T> typedReader(const QDbConnection& connection = QDbConnection()) const;

   // Creates a new data source (sub select) based on this data source
   QDbSelector selector(const QString& alias = QString()) const;

protected:
   bp<QDbTableData> _data;
};

template <class T>
QDbTypedReader<T> QDbDataSource::typedReader(const QDbConnection& connection) const
{
   return QDbTypedReader<T>(*this, connection);
}

#endif // QDbDataSource_H
