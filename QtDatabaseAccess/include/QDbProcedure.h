#ifndef QDBPROCEDURE_H
#define QDBPROCEDURE_H

#include "qtdatabaseaccess_global.h"
#include <Variant.h>
#include <QDbCommand.h>

class QTDATABASEACCESS_EXPORT QDbProcedure
{
public:
   QDbProcedure();
   QDbProcedure(const QDbProcedure& other);
   QDbProcedure(const QString& name);
   QDbProcedure(const QDbConnection& connection, const QString& name);
   ~QDbProcedure();

   QDbProcedure& operator=(const QDbProcedure& other);

   void set(const QString& parName, const Variant& value);
   Variant get(const QString& parName);
   Variant getReturnValue(); // Only valid if the procedure is a function with a return value

   bool execute(const QDbConnection& connection = QDbConnection());

   QString lastError() const;

private:
   QDbStoredProc _command;
};

#endif // QDBPROCEDURE_H
