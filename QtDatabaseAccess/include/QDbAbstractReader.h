#ifndef QDBABSTRACTREADER_H
#define QDBABSTRACTREADER_H

#include "qtdatabaseaccess_global.h"
#include "QDbAbstractCommand.h"
#include "QDbDefs.h"
#include "QDbTypedExpression.h"
#include "QDbColumnBinding.h"
#include "QDbExpression.h"

class QSharedValue;
class QDbColumn;

class QTDATABASEACCESS_EXPORT QDbAbstractReader : public QDbAbstractCommand
{
protected:
   QDbAbstractReader();
   QDbAbstractReader(const QDbConnection& connection);
   QDbAbstractReader(const QDbAbstractReader& other);
   ~QDbAbstractReader() override;

   QDbAbstractReader& operator=(const QDbAbstractReader& other);

public:

   // Selecting columns

   // Select a column which content should be returned. Alias for select().
   template <class T>
   QDbColumnBinding<T> get(QDbTypedColumn<T> column, QDb::SortOrder sortOrder = QDb::OrderArbitrary, const QString& alias = QString())
   {
      return addBinding(column, dbTypeFor(typeid(T)), sortOrder, alias);
   }

   // Select a typed expression which result should be returned. Alias for select().
   template <class T>
   QDbColumnBinding<T> get(QDbTypedExpression<T> column, QDb::SortOrder sortOrder = QDb::OrderArbitrary, const QString& alias = QString())
   {
      return addBinding(column, dbTypeFor(typeid(T)), sortOrder, alias);
   }

   // Select an expression which result should be returned (can also be a QDbColumn).
   QDbColumnBinding<Variant> get(QDbExpression expression, QDb::SortOrder sortOrder = QDb::OrderArbitrary, const QString& alias = QString());

   // Select an expression which result should be returned (can also be a QDbColumn).
   // The type can be used to explicitly specify a type for columns that are used in returning clauses (for example in inserts).
   QDbColumnBinding<Variant> get(QDbExpression expression, QDbDataType type, QDb::SortOrder sortOrder = QDb::OrderArbitrary, const QString& alias = QString());

   // See get()
   template <class T>
   QDbColumnBinding<T> select(QDbTypedColumn<T> column, QDb::SortOrder sortOrder = QDb::OrderArbitrary, const QString& alias = QString())
   {
      return get<T>(column, sortOrder, alias);
   }

   // See get()
   template <class T>
   QDbColumnBinding<T> select(QDbTypedExpression<T> column, QDb::SortOrder sortOrder = QDb::OrderArbitrary, const QString& alias = QString())
   {
      return get<T>(column, sortOrder, alias);
   }

   // See get()
   QDbColumnBinding<Variant> select(QDbExpression expression, QDb::SortOrder sortOrder = QDb::OrderArbitrary, const QString& alias = QString())
   {
      return get(expression, sortOrder, alias);
   }

   // Experimental functions

   int outputColumnCount() const;
   int indexOf(const QDbExpression& column) const;

   void setSortOrder(int index, QDb::SortOrder sortOrder);
   void clearSortOrder();

protected:
   // Constraints

   void setWhere(const QDbExpression &constraint);

   void setDistinct(bool isDistinct = true);

   void setHaving(const QDbExpression &constraint);

   void setLimit(int rows);
   void setOffset(int rows);

   // Ordering

   void addOrderBy(QDbColumnBindingBase column, QDb::SortOrder sortOrder = QDb::OrderAscending);
   void addOrderBy(const QDbColumn& column, QDb::SortOrder sortOrder = QDb::OrderAscending);

   bp<QDbBoundColumn> addBinding(const QDbExpression& expression, QDbDataType type, QDb::SortOrder sortOrder, const QString& alias);
};

#endif // QDBABSTRACTREADER_H
