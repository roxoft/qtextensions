#pragma once
#include "qtdatabaseaccess_global.h"
#include <QString>
#include <QList>
#include <QDbCommand.h>
#include "QDbParameter.h"

class QTDATABASEACCESS_EXPORT QDbStatement
{
public:
   QDbStatement()
   {
   }

   QDbStatement(const QString& statement, const QList<QDbParameter>& phList)
   {
      _sqlStatement = statement;
      _phList = phList;
   }

public:
   const QString& sqlStatement() const { return _sqlStatement; }
   void setSqlStatement(const QString& sqlStatement) { _sqlStatement = sqlStatement; }

   const QList<QDbParameter>& phList() const { return _phList; }
   void setPhList(const QList<QDbParameter>& phList) { _phList = phList; }

public:
   QDbState exec(const QDbConnection& connection, int rowsToFetch = -1);
   QDbState exec(QDbCommand* command, int rowsToFetch = -1);

private:
   QString _sqlStatement;
   QList<QDbParameter> _phList;
};
