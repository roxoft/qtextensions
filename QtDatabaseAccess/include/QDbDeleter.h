#ifndef QDBDELETER_H
#define QDBDELETER_H

#include "qtdatabaseaccess_global.h"
#include "QDbAbstractCommand.h"
#include "QDbDataSource.h"
#include "QDbExpression.h"

class QTDATABASEACCESS_EXPORT QDbDeleter : public QDbAbstractCommand
{
public:
   QDbDeleter();
   QDbDeleter(const QDbDeleter& other);
   QDbDeleter(const QDbDataSource& table);
   virtual ~QDbDeleter();

   QDbDeleter& operator=(const QDbDeleter& other);

   QDbDeleter& where(const QDbExpression &constraint);

   int affectedRows();

public:
   QList<QDbStatement> createSqlStatements(const QDbSqlContext& sqlContext) const override;

   bool execute(const QDbConnection& connection = QDbConnection()) override;

private:
   int _affectedRows = -1;
};

typedef QDbDeleter QDbDeleteFrom;

#endif // QDBDELETER_H
