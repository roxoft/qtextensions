#ifndef QDBUPDATER_H
#define QDBUPDATER_H

#include "qtdatabaseaccess_global.h"
#include "QDbAbstractReader.h"
#include "QDbAssignment.h"
#include <QRational>

class QDbTable;

class QTDATABASEACCESS_EXPORT QDbUpdater : public QDbAbstractReader
{
public:
   QDbUpdater();
   QDbUpdater(const QDbConnection& connection);
   QDbUpdater(const QDbTable& table, const QDbConnection& connection = QDbConnection());
   QDbUpdater(const QDbUpdater& other);
   ~QDbUpdater() override;

   QDbUpdater& operator=(const QDbUpdater& other);

   QDbUpdater& set(const QDbAssignment& assignment);
   QDbUpdater& operator<<(const QDbAssignment& assignment);

   QDbUpdater& set(const QDbColumn& left, const QDbExpression& right);
   QDbUpdater& set(const QDbColumn& left, bool value);
   QDbUpdater& set(const QDbColumn& left, int value);
   QDbUpdater& set(const QDbColumn& left, long long int value);
   QDbUpdater& set(const QDbColumn& left, double value);
   QDbUpdater& set(const QDbColumn& left, const char *value);
   QDbUpdater& set(const QDbColumn& left, const QString& value);
   QDbUpdater& set(const QDbColumn& left, const QRational& value);
   QDbUpdater& set(const QDbColumn& left, const QDecimal& value);
   QDbUpdater& set(const QDbColumn& left, const QDate& value);
   QDbUpdater& set(const QDbColumn& left, const QTime& value);
   QDbUpdater& set(const QDbColumn& left, const QDateTime& value);
   QDbUpdater& set(const QDbColumn& left, const QDateTimeEx& value);
   QDbUpdater& set(const QDbColumn& left, const QByteArray& value);
   QDbUpdater& set(const QDbColumn& left, const Variant& value);

   // Bind a value to a column so it can be set several times
   bp<QSharedValue> set(QDbColumn column);

   QDbUpdater& from(const QDbDataSource& dataSource);

   QDbUpdater& where(const QDbExpression &constraint) { QDbAbstractReader::setWhere(constraint); return *this; }

   QDbUpdater& distinct(bool isDistinct = true) { QDbAbstractReader::setDistinct(isDistinct); return *this; }

   QDbUpdater& having(const QDbExpression &constraint) { QDbAbstractReader::setHaving(constraint); return *this; }

   QDbUpdater& limit(int rows) { QDbAbstractReader::setLimit(rows); return *this; }
   QDbUpdater& offset(int rows) { QDbAbstractReader::setOffset(rows); return *this; }

   bool usePostLob() const;
   void setUsePostLob(bool usePostLob);

   bool hasAssignments() const;

   int affectedRows();

public:
   QList<QDbStatement> createSqlStatements(const QDbSqlContext& sqlContext) const override;

   bool execute(const QDbConnection& connection = QDbConnection()) override;

private:
   int _affectedRows = -1;
};

typedef QDbUpdater QDbUpdateIn;

#endif // QDBUPDATER_H
