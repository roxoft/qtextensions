#include "QDbXmlReader.h"
#include "QDbCommandData.h"
#include "QDbTableData.h"
#include "QDbExpressionData.h"
#include <basic_exceptions>

QDbXmlReader::QDbXmlReader()
   : QXmlStreamReader()
{
}

QDbXmlReader::~QDbXmlReader()
{
}

QString QDbXmlReader::nextElementTag()
{
   while (!atEnd())
   {
      readNext();

      if (isStartElement())
         return name();

      if (isEndElement())
         return QString::fromLatin1("~") + name();
   }

   return QString();
}

QString QDbXmlReader::name() const
{
   return QXmlStreamReader::name().toString();
}

QDbCommandData* QDbXmlReader::readQuery()
{
   QDbCommandData* query = nullptr;

   auto attr = attributes();

   QString version = attr.value("version").toString();

   if (version == "10")
   {
      if (attr.hasAttribute("id"))
         query = _queryList.at(attr.value("id").toString().toInt());
      else
      {
         //query = QDbCommandData::fromXml(this);
         _queryList.append(query);
      }
   }
   else if (version == "3")
   {
      throw QNotImplementedException("Query XML version not supported");
      //if (attr.hasAttribute(QLatin1String("id")))
      //   statement = _statementList.at(attr.value(QLatin1String("id")).toString().toInt());
      //else
      //{
      //   statement = new QSqlStatementPrivate;
      //   statement->fromXmlV3(this);
      //   _statementList.append(statement);
      //}
   }
   else if (version == "2")
   {
      throw QNotImplementedException("Query XML version not supported");
      //statement = new QSqlStatementPrivate;
      //statement->fromXmlV2(*_reader);
   }
   else
   {
      throw QNotImplementedException("Query XML version not supported");
      //statement = new QSqlStatementPrivate;
      //statement->fromXmlV1(*_reader);
   }

   return query;
}

QDbTableData* QDbXmlReader::readTable()
{
   auto attr = attributes();

   if (attr.hasAttribute("id"))
      return _tableList.at(attr.value("id").toString().toInt());

   QDbTableData* table = QDbTableData::fromXml(this);

   _tableList.append(table);

   return table;
}

QDbExpressionData* QDbXmlReader::readColumn()
{
   auto attr = attributes();

   if (attr.hasAttribute("id"))
      return _columnList.at(attr.value("id").toString().toInt());

   QDbExpressionData* column = QDbExpressionData::fromXml(this);

   _columnList.append(column);

   return column;
}
