#ifndef QDBXMLREADER_H
#define QDBXMLREADER_H

#include <QXmlStreamReader>

class QDbExpressionData;
class QDbTableData;
class QDbCommandData;

class QDbXmlReader : public QXmlStreamReader
{
public:
   QDbXmlReader();
   ~QDbXmlReader();

   QString  nextElementTag();
   QString  name() const;

   QDbCommandData* readQuery();
   QDbTableData* readTable();
   QDbExpressionData* readColumn();

private:
   QList<QDbCommandData*> _queryList;
   QList<QDbTableData*> _tableList;
   QList<QDbExpressionData*> _columnList;
};

#endif // QDBXMLREADER_H
