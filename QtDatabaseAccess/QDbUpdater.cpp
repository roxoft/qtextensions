#include "QDbUpdater.h"
#include "QDbTable.h"
#include "QDbCommandData.h"
#include "QDbSqlBuilderContext.h"
#include "QDbTableData.h"
#include "QDbExpressionData.h"

QDbUpdater::QDbUpdater()
{
}

QDbUpdater::QDbUpdater(const QDbConnection& connection) : QDbAbstractReader(connection)
{
}

QDbUpdater::QDbUpdater(const QDbTable& table, const QDbConnection& connection) : QDbAbstractReader(connection)
{
   _data = new QDbCommandData;
   _data->_commandType = QDbCommandData::Update;
   _data->_in = table._data;
}

QDbUpdater::QDbUpdater(const QDbUpdater& other) : QDbAbstractReader(other), _affectedRows(other._affectedRows)
{
}

QDbUpdater::~QDbUpdater()
{
}

QDbUpdater& QDbUpdater::operator=(const QDbUpdater& other)
{
   QDbAbstractReader::operator=(other);
   _affectedRows = other._affectedRows;
    return *this;
}

QDbUpdater& QDbUpdater::set(const QDbAssignment& assignment)
{
   if (_data)
   {
      int columnIndex;
      for (columnIndex = 0; columnIndex < _data->_assignments.count(); ++columnIndex)
      {
         if (_data->_assignments.at(columnIndex).left().isEqualTo(assignment.left()))
            break;
      }

      if (columnIndex == _data->_assignments.count())
      {
         _data->_assignments.append(assignment);
      }
      else
      {
         _data->_assignments[columnIndex] = assignment;
      }
   }

   return *this;
}

QDbUpdater& QDbUpdater::operator<<(const QDbAssignment& assignment)
{
   return set(assignment);
}

QDbUpdater& QDbUpdater::set(const QDbColumn& left, const QDbExpression& right) { set(QDbAssignment(left, right)); return *this; }
QDbUpdater& QDbUpdater::set(const QDbColumn& left, bool value) { set(QDbAssignment(left, value)); return *this; }
QDbUpdater& QDbUpdater::set(const QDbColumn& left, int value) { set(QDbAssignment(left, value)); return *this; }
QDbUpdater& QDbUpdater::set(const QDbColumn& left, long long int value) { set(QDbAssignment(left, value)); return *this; }
QDbUpdater& QDbUpdater::set(const QDbColumn& left, double value) { set(QDbAssignment(left, value)); return *this; }
QDbUpdater& QDbUpdater::set(const QDbColumn& left, const char *value) { set(QDbAssignment(left, value)); return *this; }
QDbUpdater& QDbUpdater::set(const QDbColumn& left, const QString& value) { set(QDbAssignment(left, value)); return *this; }
QDbUpdater& QDbUpdater::set(const QDbColumn& left, const QRational& value) { set(QDbAssignment(left, value)); return *this; }
QDbUpdater& QDbUpdater::set(const QDbColumn& left, const QDecimal& value) { set(QDbAssignment(left, value)); return *this; }
QDbUpdater& QDbUpdater::set(const QDbColumn& left, const QDate& value) { set(QDbAssignment(left, value)); return *this; }
QDbUpdater& QDbUpdater::set(const QDbColumn& left, const QTime& value) { set(QDbAssignment(left, value)); return *this; }
QDbUpdater& QDbUpdater::set(const QDbColumn& left, const QDateTime& value) { set(QDbAssignment(left, value)); return *this; }
QDbUpdater& QDbUpdater::set(const QDbColumn& left, const QDateTimeEx& value) { set(QDbAssignment(left, value)); return *this; }
QDbUpdater& QDbUpdater::set(const QDbColumn& left, const QByteArray& value) { set(QDbAssignment(left, value)); return *this; }
QDbUpdater& QDbUpdater::set(const QDbColumn& left, const Variant& value) { set(QDbAssignment(left, value)); return *this; }

bp<QSharedValue> QDbUpdater::set(QDbColumn column)
{
   const auto sharedValue = bp<QSharedValue>(new QSharedValue);

   set(QDbAssignment(column, QDbExpression(sharedValue)));

   return sharedValue;
}

QDbUpdater& QDbUpdater::from(const QDbDataSource& dataSource)
{
   if (_data)
      _data->_from = dataSource._data;

   return *this;
}

bool QDbUpdater::usePostLob() const
{
   return _data && _data->_usePostLob;
}

void QDbUpdater::setUsePostLob(bool usePostLob)
{
   if (_data)
      _data->_usePostLob = usePostLob;
}

bool QDbUpdater::hasAssignments() const
{
   return _data && !_data->_assignments.isEmpty();
}

int QDbUpdater::affectedRows()
{
   return _affectedRows;
}

QList<QDbStatement> QDbUpdater::createSqlStatements(const QDbSqlContext& sqlContext) const
{
   if (!_data)
   {
      return {};
   }

   QList<QDbStatement> dbStatements;

   QDbSqlBuilderContext builderContext(sqlContext, false);

   if (builderContext.sqlStyle() == SqlStyle::Sybase)
   {
      auto maxRowsPerDmlCall = builderContext.maxRowsPerDmlCall();
      builderContext.setMaxRowsPerDmlCall(1);

      do
      {
         QStringList updateStatements;

         builderContext.resetStatementVariables();

         while (updateStatements.length() < maxRowsPerDmlCall)
         {
            updateStatements.append(_data->createSql(&builderContext));

            if (!builderContext.hasMore())
            {
               break;
            }
         }

         dbStatements.append(QDbStatement(updateStatements.join("; "), builderContext.phList()));

      } while (builderContext.hasMore());
   }
   else
   {
      do
      {
         builderContext.resetStatementVariables();

         auto sqlStatement = _data->createSql(&builderContext);

         dbStatements.append(QDbStatement(sqlStatement, builderContext.phList()));

      } while (builderContext.hasMore());
   }

   return dbStatements;
}

bool QDbUpdater::execute(const QDbConnection& connection)
{
   if (!_data)
   {
      _state = QDbState(QDbState::Error, "Updater data is not defined");
      return {};
   }

   QDbCommand command(connection);

   auto dbStatements = createSqlStatements(QDbSqlContext(command.connection(), QDbSqlContext::PhUsage::Optimized));

   _affectedRows = 0;

   for (auto&& dbStatement : dbStatements)
   {
      _state.withSourceText(dbStatement.sqlStatement());

      _state = dbStatement.exec(&command, -1);
      if (_state.type() == QDbState::Error)
      {
         return false;
      }

      _affectedRows += qMax(command.affectedRows(), 0);
   }

   return true;
}
