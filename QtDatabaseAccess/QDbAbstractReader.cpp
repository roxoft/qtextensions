#include "QDbAbstractReader.h"
#include "QDbBoundColumn.h"
#include "QDbCommandData.h"
#include "QDbExpressionData.h"
#include <QStringBuilder>

QDbAbstractReader::QDbAbstractReader()
{
}

QDbAbstractReader::QDbAbstractReader(const QDbConnection& connection) : QDbAbstractCommand(connection)
{
}

QDbAbstractReader::QDbAbstractReader(const QDbAbstractReader& other) : QDbAbstractCommand(other)
{
}

QDbAbstractReader::~QDbAbstractReader()
{
}

QDbAbstractReader& QDbAbstractReader::operator=(const QDbAbstractReader& other)
{
   QDbAbstractCommand::operator=(other);
   return *this;
}

QDbColumnBinding<Variant> QDbAbstractReader::get(QDbExpression expression, QDb::SortOrder sortOrder, const QString& alias)
{
   return addBinding(expression, DT_UNKNOWN, sortOrder, alias);
}

QDbColumnBinding<Variant> QDbAbstractReader::get(QDbExpression expression, QDbDataType type, QDb::SortOrder sortOrder, const QString& alias)
{
   return addBinding(expression, type, sortOrder, alias);
}

int QDbAbstractReader::outputColumnCount() const
{
   if (!_data)
      return -1;

   return _data->_bindings.count();
}

int QDbAbstractReader::indexOf(const QDbExpression& column) const
{
   if (!_data)
      return -1;

   for (auto index = 0; index < _data->_bindings.count(); ++index)
   {
      if (_data->_bindings.at(index)->column.isEqualTo(column))
         return index;
   }

   return -1;
}

void QDbAbstractReader::setSortOrder(int index, QDb::SortOrder sortOrder)
{
   if (_data && index >= 0 && index < _data->_bindings.count())
      _data->_bindings.at(index)->sortOrder = sortOrder;
}

void QDbAbstractReader::clearSortOrder()
{
   if (!_data)
      return;

   // Clear the sort order in the bindings
   for (auto&& binding : _data->_bindings)
   {
      binding->sortOrder = QDb::OrderArbitrary;
   }

   _data->_orderBy.clear();
}

void QDbAbstractReader::setWhere(const QDbExpression& constraint)
{
   if (_data)
      _data->_where = constraint._data;
}

void QDbAbstractReader::setDistinct(bool isDistinct)
{
   if (_data)
      _data->_distinct = isDistinct;
}

void QDbAbstractReader::setHaving(const QDbExpression& constraint)
{
   if (_data)
      _data->_having = constraint._data;
}

void QDbAbstractReader::setLimit(int rows)
{
   if (_data)
      _data->_limit = rows;
}

void QDbAbstractReader::setOffset(int rows)
{
   if (_data)
      _data->_offset = rows;
}

void QDbAbstractReader::addOrderBy(QDbColumnBindingBase column, QDb::SortOrder sortOrder)
{
   if (!_data)
      return;

   auto boundColumn = column.boundColumn();

   boundColumn->sortOrder = sortOrder;

   _data->_orderBy.append(boundColumn);
}

void QDbAbstractReader::addOrderBy(const QDbColumn& column, QDb::SortOrder sortOrder)
{
   if (!_data)
      return;

   // Search for an equivalent column in the bindings to add to the order list

   for (auto&& binding : _data->_bindings)
   {
      if (binding->column.isEqualTo(column))
      {
         binding->sortOrder = sortOrder;
         _data->_orderBy.append(binding);
         return;
      }
   }

   // Add a new column to the order list

   auto orderColumn = new QDbBoundColumn(column, DT_UNKNOWN);

   orderColumn->sortOrder = sortOrder;

   _data->_orderBy.append(orderColumn);
}

bp<QDbBoundColumn> QDbAbstractReader::addBinding(const QDbExpression& expression, QDbDataType type, QDb::SortOrder sortOrder, const QString& alias)
{
   if (!_data)
      return bp<QDbBoundColumn>();

   auto boundColumn =  bp_new<QDbBoundColumn>(expression, type);

   if (!alias.isEmpty())
      boundColumn->alias = "\"" % alias % "\"";
   boundColumn->sortOrder = sortOrder;

   _data->_bindings.append(boundColumn);

   return boundColumn;
}
