#pragma once

#include "QDbBoundColumn.h"
#include "QDbSqlBuilderContext.h"
#include "QDbAssignment.h"

class QDbExprTableColumnData;

class QDbAssignmentParts
{
public:
   QStringList strColumns;
   QStringList strValues;
   QStringList returningColumns;
   QStringList returningPhList;

   void setFrom(const QList<QDbAssignment>& assignments, const QList<bp<QDbBoundColumn>>& bindings, QDbSqlBuilderContext* builderContext, bool useQualifiedNames, bool usePostLob);
};
