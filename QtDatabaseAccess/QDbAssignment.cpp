#include "QDbAssignment.h"

QDbAssignment::QDbAssignment()
{
}

QDbAssignment::QDbAssignment(const QDbColumn& left, const Variant& right)
   : _left(left), _right(right)
{
}

QDbAssignment::QDbAssignment(const QDbColumn& left, const QDbExpression& right)
   : _left(left), _right(right)
{
}

QDbAssignment::~QDbAssignment()
{
}
