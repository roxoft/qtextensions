QtDatabaseAccess
================

The QtDatabaseAccess library provides a framework to create SQL statements without writing any SQL code.

QtDatabaseAccess uses C++ syntax to describe sql commands.

Usage
-----

1. Include `<QtDatabaseAccess>` for all necessary classes.

2. Define a data source. All data source classes are derived from `QDbDataSource`.
   The simplest data source is an instance of `QDbTable` which represents a plain database table.
   Tables can be joined to create new data sources.
   Sub-queries can be created through the `selector()` member function.

3. Create a command from the data source through one of the following functions:
   - `reader()`
   - `typedReader()`
   - `inserter()`
   - `updater()`
   - `deleter()`

   For `reader()` and `typedReader()` a connection can be passed as parameter. This enables calling `readNext()` without previously calling execute.
   For the other commands specify a connection when calling `execute()`.
   It is good practise to explicitely specify a connection.

4. Provide the necessary information to execute the command (where-clause, returning columns, setting values, etc.).
   For readers all result columns have to be defined through either the `select()` or the `map()` function. `get()` is an alias for `select()`.

5. Executen the command by calling `execute()` and check for errors.
   `execute()` is implicitely called when calling `readNext()` and the command was not yet executed. In this case check for errors after reading all records with `hasError()`.

6. For readers get the results by calling `readNext()`.
   For inserters, updaters and deleters check the affected rows with `affectedRows()`.
   It is good practise to check for errors after reading records with `readNext()`.

Reader example:
---------------

    #include <QtDatabaseAccess>

    QDbTable testTable("TBL_TEST");

    auto reader = testTable.reader(connection).where(testTable.column("id") == 1 && testTable.column("some_name") == "Müller" && testTable.column("some_date") == QDateEx(2016, 5, 2));

    auto valueName = reader.select(testTable.typedColumn<QString>("some_name"));
    auto valueDate = reader.select(testTable.typedColumn<QDateEx>("some_date"));

    while (reader.readNext())
    {
        auto name = valueName.value(); // This is already a QString
        auto date = valueDate.value(); // This is already a QDateEx
    }

    if (reader.hasError())
    {
        signalError(reader.lastError()); // `signalError()` is a pseudo function for any error handling functionality (for example `QMessageBox::critical()`).
    }

Reader example: Storing values directly into variables:
-------------------------------------------------------

    #include <QtDatabaseAccess>

    QDateEx date;
    QString name;

    QDbTable testTable("TBL_TEST");

    auto reader = testTable.reader(connection).where(testTable.column("id") == 1 && testTable.column("some_name") == "Müller" && testTable.column("some_date") == QDate(2016, 5, 2));

    reader.select(testTable.column("some_name")).into(&name);
    reader.select(testTable.column("some_date")).into(&date);

    // IMPORTANT: The variables `date` and `name` must not be destroyed befor the reader operation ends!

    while (reader.readNext())
    {
        // Process the content of the name and date variables.
    }

    if (reader.hasError())
    {
        signalError(reader.lastError());
    }

Reader example: Reading all records into objects:
-------------------------------------------------

    class TestElement
    {
    public:
      TestElement() : _number(0) {}
      ~TestElement() {}

      int number() const { return _number; }
      void setNumber(int number) { _number = number; }

      QDecimal decimal() const { return _decimal; }
      void setDecimal(QDecimal decimal) { _decimal = decimal; }

      QString name() const { return _name; }
      void setName(QString name) { _name = name; }

      QDate date() const { return _date; }
      void setDate(QDate date) { _date = date; }

      Variant value() const { return _value; }
      void setValue(Variant value) { _value = value; }

    private:
      int _number;
      QDecimal _decimal;
      QString _name;
      QDate _date;
      Variant _value;
    };

    #include <QtDatabaseAccess>

    QDbTable testTable("TBL_TEST");

    auto reader = testTable.typedReader<TestElement>();

    reader.map(testTable.typedColumn<int>("id"), [](TestElement* element, int id){ element->setNumber(id); });
    reader.map(testTable.typedColumn<QString>("some_name"), &TestElement::setName);
    reader.map(testTable.column("some_date"), &TestElement::setValue);
    reader.map(testTable.typedColumn<QDateEx>("some_date"), testTable.typedColumn<QDecmimal>("some_number"), [](TestElement* element, const QDateEx& date, const QDecimal& decimal){ element->setDate(date); element->setDecimal(decimal); });

    while (reader.readNext())
    {
       TestElement testElement;
       reader.setValues(&testElement);
    }

    /* Alternative:
    auto result = reader.readAll(connection);

    for (auto&& element : result)
    {
      ...
    }
    */

Example of predefining a table class
------------------------------------

    #include <QDbTable.h>
    #include <QDbTypedExpression.h>
    #include <QIcon>

    class TblMyTable : public QDbTable
    {
    public:
       TblTypes() : QDbTable("MY_TABLE") {}
       ~TblTypes() {}

       QDbTypedColumn<qint64> colId() { return column("ID"); }
       QDbTypedColumn<QString> colName() { return column("NAME"); }
       QDbTypedColumn<qint32> colVersion() { return column("VERSION"); }
       QDbTypedColumn<QIcon> colIcon() { return column("ICON"); }
       QDbTypedColumn<QString> colDescription() { return column("DESC"); }
    };

Example of using a predefined table class
-----------------------------------------

    #include <TblMyTable.h>

    TblMyTable tblMyTable;
    auto reader = tblMyTable.reader();

    auto colId = reader.select(tblMyTable.colId());
    auto colName = reader.select(tblMyTable.colName(), QDb::OrderAscending);
    auto colVersion = reader.select(tblMyTable.colVersion());
    auto colIcon = reader.select(tblMyTable.colIcon());
    auto colDescription = reader.select(tblMyTable.colDescription());

    reader.where(tblMyTable.colId() == id);

    while (reader.readNext())
    {
      _id = colId.value();
      _name = colName.value();
      _version = colVersion.value();
      _icon = colIcon.value();
      _description = colDescription.value();
    }

    if (reader.hasError())
    {
        signalError(reader.lastError());
    }
