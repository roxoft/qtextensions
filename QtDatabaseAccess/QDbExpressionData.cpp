#include "QDbExpressionData.h"
#include "QDbCommandData.h"
#include "QDbXmlReader.h"
#include "QDbXmlWriter.h"
#include <QStringBuilder>
#include <QtListExtensions.h>

// XML helper
static QXmlStreamReader::TokenType readNextElementTag(QXmlStreamReader* reader)
{
   QXmlStreamReader::TokenType tokenType = QXmlStreamReader::Invalid;

   if (reader == nullptr)
      return tokenType;

   while (!reader->atEnd())
   {
      reader->readNext();

      tokenType = reader->tokenType();

      if (tokenType == QXmlStreamReader::StartElement || tokenType == QXmlStreamReader::EndElement)
         break;
   }

   return tokenType;
}

// Sql helper
static int colValueSize(const QDbExpressionData* expression)
{
   int size = 0;

   const auto valueExpression = dynamic_cast<const QDbExprValueData*>(expression);

   if (valueExpression)
   {
      if (valueExpression->_value.type() == typeid(QString) || valueExpression->_value.type() == typeid(QText))
         size = valueExpression->_value.toString().size() * (int)sizeof(ushort);
      else if (valueExpression->_value.type() == typeid(QByteArray))
         size = valueExpression->_value.toByteArray().size();
   }

   return size;
}

// Creates the appropriate column from XML data
QDbExpressionData* QDbExpressionData::fromXml(QDbXmlReader* reader)
{
   QString              endTag = QString::fromLatin1("~") + reader->name();
   QXmlStreamAttributes columnAttributes = reader->attributes();
   QString              columnType = columnAttributes.value(QLatin1String("type")).toString();
   QString              alias = columnAttributes.value(QLatin1String("alias")).toString();
   QDbExpressionData    *column = nullptr;

   Q_ASSERT(!columnType.isEmpty());

   if (columnType == "Value")
   {
      auto valueColumn = new QDbExprValueData;
      valueColumn->_value.deserialize(reader->readElementText().toUtf8());
      column = valueColumn;
   }
   else if (columnType == "TableColumn")
   {
      auto tableColumn = new QDbExprTableColumnData;

      tableColumn->_name = columnAttributes.value("name").toString();
      if (reader->nextElementTag() == "DbTable")
         tableColumn->_table = reader->readTable();

      column = tableColumn;
   }
   else if (columnType == "Expression")
   {
      QString expression;
      auto isAggregate = columnAttributes.hasAttribute("isAggregate");
      QList<sp<QDbExpressionData>> columnList;

      QString element;
      while (!(element = reader->nextElementTag()).isEmpty())
      {
         if (element == endTag)
            break;

         if (element == "Expression")
         {
            expression = reader->readElementText();
         }
         else if (element == "DbColumn")
         {
            columnList.append(sp<QDbExpressionData>(reader->readColumn()));
         }
      }

      column = new QDbExprFunctionData(expression, isAggregate, columnList);
   }
   else if (columnType == "Subselect")
   {
      auto subselect = new QDbExprQueryData;

      if (reader->nextElementTag() == "DbQuery")
         subselect->_command = reader->readQuery();

      column = subselect;
   }

   Q_ASSERT(column);

   return column;
}

//
// QDbExprValueData implementation
//

QString QDbExprValueData::sql(QDbSqlBuilderContext* builderContext, bool useQualifiedNames) const
{
   return builderContext->getParameter(_value);
}

void QDbExprValueData::toXml(QDbXmlWriter* writer) const
{
   writer->writeAttribute("type", "Value");
   writer->writeCharacters(QString::fromUtf8(_value.serialized().data()));
}

bool QDbExprValueData::isEqualTo(const QDbExpressionData* data) const
{
   if (data == this)
      return true;

   const QDbExprValueData* valueColumn = dynamic_cast<const QDbExprValueData*>(data);

   return valueColumn && valueColumn->_value == _value;
}

//
// QDbExprBoundValueData implementation
//

QString QDbExprBoundValueData::sql(QDbSqlBuilderContext* builderContext, bool) const
{
   return builderContext->getParameter(_boundValue);
}

void QDbExprBoundValueData::toXml(QDbXmlWriter* writer) const
{
   // There is no way to serialize the binding so it is serialized as value
   writer->writeAttribute("type", "Value");
   writer->writeCharacters(QString::fromUtf8(_boundValue->value().serialized().data()));
}

bool QDbExprBoundValueData::isEqualTo(const QDbExpressionData* data) const
{
   if (data == this)
      return true;

   const QDbExprBoundValueData* valueColumn = dynamic_cast<const QDbExprBoundValueData*>(data);

   return valueColumn && valueColumn->_boundValue == _boundValue;
}

//
// QDbExprTableColumnData implementation
//

QString QDbExprTableColumnData::sql(QDbSqlBuilderContext* builderContext, bool useQualifiedNames) const
{
   QString tableName;

   if (useQualifiedNames)
      tableName = builderContext->getTableAlias(_table);

   if (tableName.isEmpty())
      return _name;

   return QString("%1.%2").arg(tableName).arg(_name);
}

void QDbExprTableColumnData::toXml(QDbXmlWriter* writer) const
{
   writer->writeAttribute("type", "TableColumn");
   writer->writeAttribute("name", _name);
   if (_table)
      writer->writeTable(_table.data());
}

bool QDbExprTableColumnData::isEqualTo(const QDbExpressionData* data) const
{
   if (data == this)
      return true;

   const auto col = dynamic_cast<const QDbExprTableColumnData*>(data);

   return col && col->_table == _table && col->_name == _name;
}

//
// QDbExprOperationData implementation
//

QDbExprOperationData::QDbExprOperationData(const SqlOperator &op)
   : _operator(op)
{
}

QDbExprOperationData::QDbExprOperationData(const SqlOperator& op, const QList<sp<QDbExpressionData>>& operands)
   : _operator(op), _operands(operands)
{
   Q_ASSERT(_operator.isValid());
}

QDbExprOperationData::QDbExprOperationData(const SqlOperator &op, const sp<QDbExpressionData> &operand)
   : _operator(op)
{
   _operands.append(operand);
}

QDbExprOperationData::QDbExprOperationData(const sp<QDbExpressionData> &left, const SqlOperator &op, const sp<QDbExpressionData> &right)
   : _operator(op)
{
   _operands.append(left);
   _operands.append(right);
}

QString QDbExprOperationData::sql(QDbSqlBuilderContext* builderContext, bool useQualifiedNames) const
{
   auto op = _operator.toString();
   QStringList operands = toList<QString>(_operands, [builderContext, useQualifiedNames](const sp<QDbExpressionData>& expr) { return expr->sql(builderContext, useQualifiedNames); });

   // The first column has to be a table or an expression

   if (operands.count() == 2 && operands.at(1) == QLatin1String("NULL"))
   {
      if (_operator.action() == SqlOperator::Action::Equal)
         op = QLatin1String("IS");
      else if (_operator.action() == SqlOperator::Action::NotEqual)
         op = QLatin1String("IS NOT");
   }

   if (builderContext->sqlStyle() == SqlStyle::Oracle || builderContext->sqlStyle() == SqlStyle::DB2)
   {
      if (_operator.action() == SqlOperator::Action::LeftEqual)
      {
         op = QLatin1String("=");
         operands[1] += QLatin1String("(+)"); // The (+) must be directly attached to the column name so it is added without encasing the operand in brackets
      }
      else if (_operator.action() == SqlOperator::Action::RightEqual)
      {
         op = QLatin1String("=");
         operands[0] += QLatin1String("(+)"); // The (+) must be directly attached to the column name so it is added without encasing the operand in brackets
      }
   }

   QString result;

   for (auto operandIndex = 0; operandIndex < _operands.count(); ++operandIndex)
   {
      if (_operator.type() == SqlOperator::Type::Prefix || operandIndex > 0)
      {
         if (operandIndex > 0 && op != ",")
            result += QLatin1String(" ");
         result += op % QLatin1String(" ");
      }

      const auto operand = _operands[operandIndex].as<QDbExprOperationData>();
      if (_operator.needsBrackets(operand ? operand->_operator : SqlOperator(), operandIndex))
         result += QLatin1String("(") % operands.at(operandIndex) % QLatin1String(")");
      else
         result += operands.at(operandIndex);
   }

   return result;
}

bool QDbExprOperationData::isAggregate() const
{
   for (auto&& operand : _operands)
   {
      if (operand->isAggregate())
         return true;
   }
   return false;
}

bool QDbExprOperationData::isGroupable() const
{
   for (auto&& operand : _operands)
   {
      if (operand->isGroupable())
         return true;
   }
   return false;
}

void QDbExprOperationData::toXml(QDbXmlWriter *writer) const
{
   writer->writeAttribute("operator", _operator.toString());

   for (auto&& operand : _operands)
   {
      writer->writeStartElement("Operand");
      operand->toXml(writer);
      writer->writeEndElement(); // Operand
   }
}

bool QDbExprOperationData::isEqualTo(const QDbExpressionData* data) const
{
   if (data == this)
      return true;

   const QDbExprOperationData* operationData = dynamic_cast<const QDbExprOperationData*>(data);

   if (operationData == nullptr || !(_operator == operationData->_operator) || _operands.count() != operationData->_operands.count())
      return false;

   for (auto i = 0; i < _operands.count(); ++i)
   {
      if (!_operands.at(i)->isEqualTo(operationData->_operands.at(i)))
         return false;
   }

   return true;
}

void QDbExprOperationData::addOperand(const sp<QDbExpressionData> &expression)
{
   auto operandOperation = expression.as<QDbExprOperationData>();

   if (operandOperation && _operator.isAssociative() && _operator.isLeftToRight() && operandOperation->_operator == _operator)
      _operands.append(operandOperation->_operands); // Merge the operation if both are binary operations!
   else
      _operands.append(expression);
}

//
// QDbExprFunctionData implementation
//

QDbExprFunctionData::QDbExprFunctionData(const QString& function, bool isAggregate, const QList<sp<QDbExpressionData>>& arguments)
   : _function(function), _isAggregate(isAggregate), _arguments(arguments)
{
   for (auto&& argument : arguments)
   {
      if (argument->isAggregate())
         _isAggregate = true;
   }
}

QString QDbExprFunctionData::sql(QDbSqlBuilderContext* builderContext, bool useQualifiedNames) const
{
   QString  result(_function);

   for (auto&& argument : _arguments)
   {
      result = result.arg(argument->sql(builderContext, useQualifiedNames));
   }

   return result;
}

void QDbExprFunctionData::toXml(QDbXmlWriter* writer) const
{
   writer->writeAttribute("type", "Expression");
   if (_isAggregate)
      writer->writeAttribute("isAggregate", "true");
   writer->writeStartElement("Expression");
   writer->writeCharacters(_function);
   writer->writeEndElement();
   for (auto&& argument : _arguments)
   {
      writer->writeColumn(argument);
   }
}

bool QDbExprFunctionData::isEqualTo(const QDbExpressionData* data) const
{
   if (data == this)
      return true;

   const QDbExprFunctionData *exprRef = dynamic_cast<const QDbExprFunctionData*>(data);

   if (!exprRef || exprRef->_function != _function)
      return false;

   if (exprRef->_arguments.count() != _arguments.count())
      return false;

   for (int i = 0; i < _arguments.count(); ++i)
   {
      if (!exprRef->_arguments.at(i)->isEqualTo(_arguments.at(i)))
         return false;
   }

   return true;
}

//
// QDbExprQueryData implementation
//

QString QDbExprQueryData::sql(QDbSqlBuilderContext* builderContext, bool useQualifiedNames) const
{
   if (_command)
      return "(" % _command->createSql(builderContext) % ")";
   return QString();
}

void QDbExprQueryData::toXml(QDbXmlWriter* writer) const
{
   writer->writeAttribute("type", "Subselect");
   if (_command)
      writer->writeQuery(_command.data());
}

bool QDbExprQueryData::isEqualTo(const QDbExpressionData* data) const
{
   if (data == this)
      return true;

   const QDbExprQueryData *subQuery = dynamic_cast<const QDbExprQueryData*>(data);

   return subQuery && subQuery->_command == _command; // TODO: Is pointer comparison sufficient?
}
