#include "QDbMerger.h"
#include "QDbTable.h"
#include "QDbCommandData.h"
#include "QDbSqlBuilderContext.h"
#include "QDbTableData.h"
#include "QDbExpressionData.h"

QDbMerger::QDbMerger()
{
}

QDbMerger::QDbMerger(const QDbConnection& connection) : QDbAbstractReader(connection)
{
}

QDbMerger::QDbMerger(const QDbTable& table, const QDbConnection& connection) : QDbAbstractReader(connection)
{
   _data = new QDbCommandData;
   _data->_commandType = QDbCommandData::Merge;
   _data->_in = table._data;
}

QDbMerger::QDbMerger(const QDbMerger& other) : QDbAbstractReader(other), _affectedRows(other._affectedRows)
{
}

QDbMerger::~QDbMerger()
{
}

QDbMerger& QDbMerger::operator=(const QDbMerger& other)
{
   QDbAbstractReader::operator=(other);
   _affectedRows = other._affectedRows;
    return *this;
}

QDbMerger& QDbMerger::set(const QDbAssignment& assignment)
{
   if (_data)
   {
      int columnIndex;
      for (columnIndex = 0; columnIndex < _data->_assignments.count(); ++columnIndex)
      {
         if (_data->_assignments.at(columnIndex).left().isEqualTo(assignment.left()))
            break;
      }

      if (columnIndex == _data->_assignments.count())
      {
         _data->_assignments.append(assignment);
      }
      else
      {
         _data->_assignments[columnIndex] = assignment;
      }
   }

   return *this;
}

QDbMerger& QDbMerger::operator<<(const QDbAssignment& assignment)
{
   return set(assignment);
}

QDbMerger& QDbMerger::set(const QDbColumn& left, const QDbExpression& right) { set(QDbAssignment(left, right)); return *this; }
QDbMerger& QDbMerger::set(const QDbColumn& left, bool value) { set(QDbAssignment(left, value)); return *this; }
QDbMerger& QDbMerger::set(const QDbColumn& left, int value) { set(QDbAssignment(left, value)); return *this; }
QDbMerger& QDbMerger::set(const QDbColumn& left, long long int value) { set(QDbAssignment(left, value)); return *this; }
QDbMerger& QDbMerger::set(const QDbColumn& left, double value) { set(QDbAssignment(left, value)); return *this; }
QDbMerger& QDbMerger::set(const QDbColumn& left, const char *value) { set(QDbAssignment(left, value)); return *this; }
QDbMerger& QDbMerger::set(const QDbColumn& left, const QString& value) { set(QDbAssignment(left, value)); return *this; }
QDbMerger& QDbMerger::set(const QDbColumn& left, const QRational& value) { set(QDbAssignment(left, value)); return *this; }
QDbMerger& QDbMerger::set(const QDbColumn& left, const QDecimal& value) { set(QDbAssignment(left, value)); return *this; }
QDbMerger& QDbMerger::set(const QDbColumn& left, const QDate& value) { set(QDbAssignment(left, value)); return *this; }
QDbMerger& QDbMerger::set(const QDbColumn& left, const QTime& value) { set(QDbAssignment(left, value)); return *this; }
QDbMerger& QDbMerger::set(const QDbColumn& left, const QDateTime& value) { set(QDbAssignment(left, value)); return *this; }
QDbMerger& QDbMerger::set(const QDbColumn& left, const QDateTimeEx& value) { set(QDbAssignment(left, value)); return *this; }
QDbMerger& QDbMerger::set(const QDbColumn& left, const QByteArray& value) { set(QDbAssignment(left, value)); return *this; }
QDbMerger& QDbMerger::set(const QDbColumn& left, const Variant& value) { set(QDbAssignment(left, value)); return *this; }

bp<QSharedValue> QDbMerger::set(QDbColumn column)
{
   const auto sharedValue = bp<QSharedValue>(new QSharedValue);

   set(QDbAssignment(column, QDbExpression(sharedValue)));

   return sharedValue;
}

QDbMerger& QDbMerger::from(const QDbDataSource& dataSource)
{
   if (_data)
      _data->_from = dataSource._data;

   return *this;
}

bool QDbMerger::hasAssignments() const
{
   return _data && !_data->_assignments.isEmpty();
}

int QDbMerger::affectedRows()
{
   return _affectedRows;
}

QList<QDbStatement> QDbMerger::createSqlStatements(const QDbSqlContext& sqlContext) const
{
   if (!_data)
   {
      return {};
   }

   QList<QDbStatement> dbStatements;

   QDbSqlBuilderContext builderContext(sqlContext, false);

   if (builderContext.sqlStyle() == SqlStyle::Sybase)
   {
      auto maxRowsPerDmlCall = builderContext.maxRowsPerDmlCall();
      builderContext.setMaxRowsPerDmlCall(1);

      do
      {
         QStringList mergeStatements;

         builderContext.resetStatementVariables();

         while (mergeStatements.length() < maxRowsPerDmlCall)
         {
            mergeStatements.append(_data->createSql(&builderContext));

            if (!builderContext.hasMore())
            {
               break;
            }
         }

         dbStatements.append(QDbStatement(mergeStatements.join("; "), builderContext.phList()));

      } while (builderContext.hasMore());
   }
   else
   {
      do
      {
         builderContext.resetStatementVariables();

         auto sqlStatement = _data->createSql(&builderContext);

         dbStatements.append(QDbStatement(sqlStatement, builderContext.phList()));

      } while (builderContext.hasMore());
   }

   return dbStatements;
}

bool QDbMerger::execute(const QDbConnection& connection)
{
   if (!_data)
   {
      _state = QDbState(QDbState::Error, "Merger data is not defined");
      return {};
   }

   QDbCommand command(connection);

   auto dbStatements = createSqlStatements(QDbSqlContext(command.connection(), QDbSqlContext::PhUsage::Optimized));

   _affectedRows = 0;

   for (auto&& dbStatement : dbStatements)
   {
      _state.withSourceText(dbStatement.sqlStatement());

      _state = dbStatement.exec(&command, -1);
      if (_state.type() == QDbState::Error)
      {
         return false;
      }

      _affectedRows += qMax(command.affectedRows(), 0);
   }

   return true;
}
