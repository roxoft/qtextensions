#ifndef QDBXMLWRITER_H
#define QDBXMLWRITER_H

#include <QXmlStreamWriter>
#include <QMap>

class QDbCommandData;
class QDbTableData;
class QDbExpressionData;

// Helper class to serialize a sql commands
class QDbXmlWriter : public QXmlStreamWriter
{
public:
   QDbXmlWriter();
   ~QDbXmlWriter();

   void writeQuery(const QDbCommandData* query, const char *name = "DbQuery");
   void writeTable(const QDbTableData* table, const char *name = "DbTable");
   void writeColumn(const QDbExpressionData* column, const char *name = "DbColumn");

private:
   QMap<const QDbCommandData*, int> _queryMap;
   QMap<const QDbTableData*, int> _tableMap;
   QMap<const QDbExpressionData*, int> _columnMap;
};

#endif // QDBXMLWRITER_H
