#include "QDbTableData.h"
#include <QStringBuilder>
#include "QDbSqlBuilderContext.h"
#include "QDbExpressionData.h"
#include "QDbAssignment.h"
#include "QDbCommandData.h"
#include <QtListExtensions.h>

#include "QDbXmlReader.h"
#include "QDbXmlWriter.h"

QDbTableData* QDbTableData::fromXml(QDbXmlReader* reader)
{
   QXmlStreamAttributes tableAttributes = reader->attributes();
   QString              type = tableAttributes.value("type").toString();
   QDbTableData*        tableData = nullptr;

   if (type == "Table")
   {
      tableData = new QDbPlainTableData(tableAttributes.value("name").toString(), tableAttributes.value("alias").toString());
   }
   //else if (type == QLatin1String("View"))
   //{
   //   QSqlStmtPtr view;

   //   if (reader->nextElementTag() == QLatin1String("SqlStatement"))
   //      view = reader->readStatement();

   //   if (view)
   //   {
   //      ViewRef *viewRef = new ViewRef;

   //      viewRef->_view = view;

   //      tableData = viewRef;
   //   }
   //}
   //else if (type == QLatin1String("Join"))
   //{
   //   TableJoin *tableJoin = new TableJoin;

   //   QString endTag = QString::fromLatin1("~") + reader->name();
   //   QString mode = tableAttributes.value(QLatin1String("mode")).toString();

   //   if (mode == QLatin1String("inner"))
   //      tableJoin->_mode = TableJoin::innerJoin;
   //   else if (mode == QLatin1String("left"))
   //      tableJoin->_mode = TableJoin::leftJoin;
   //   else if (mode == QLatin1String("right"))
   //      tableJoin->_mode = TableJoin::rightJoin;

   //   QString element;

   //   while (!(element = reader->nextElementTag()).isEmpty())
   //   {
   //      if (element == endTag)
   //         break;

   //      if (element == QLatin1String("LeftTable"))
   //         tableJoin->_leftTable = reader->readTable();
   //      else if (element == QLatin1String("RightTable"))
   //         tableJoin->_rightTable = reader->readTable();
   //      else if (element == QLatin1String("Condition"))
   //         tableJoin->_condition.fromXmlV3(reader);
   //   }

   //   tableData = tableJoin;
   //}

   return tableData;
}

QDbPlainTableData::QDbPlainTableData(const QString& name, const QString& alias): _name(name), _alias(alias)
{
   if (!_alias.isEmpty())
      _alias = "\"" % _alias % "\"";
}

QString QDbPlainTableData::name() const
{
   return _name;
}

QString QDbPlainTableData::alias() const
{
   if (_alias.isEmpty())
      return _name;
   return _alias;
}

QString QDbPlainTableData::sql(QDbSqlBuilderContext* builderContext) const
{
   auto alias = builderContext->getOrCreateTableAlias(this, _alias);

   if (alias.isEmpty())
      return _name;

   return _name % " " % alias;
}

void QDbPlainTableData::toXml(QDbXmlWriter* writer) const
{
   writer->writeAttribute("type", "Table");
   if (!_alias.isEmpty())
      writer->writeAttribute("alias", _alias);
   writer->writeAttribute("name", _name);
}

const QDbSchema::Table& QDbPlainTableData::tableSchema(const QDbConnection& connection) const
{
   if (!_tableSchema.isValid())
      _tableSchema = connection.tableSchema(_name);

   return _tableSchema;
}

QString QDbJoinedTableData::name() const
{
   return QString();
}

QString QDbJoinedTableData::alias() const
{
   return QString();
}

QString QDbJoinedTableData::sql(QDbSqlBuilderContext* builderContext) const
{
   auto joinStatement = _leftTable._data->sql(builderContext);

   switch (_joinMode)
   {
   case QDbJoinedTable::InnerJoin:
      joinStatement += QLatin1String(" INNER JOIN ");
      break;
   case QDbJoinedTable::LeftJoin:
      joinStatement += QLatin1String(" LEFT OUTER JOIN ");
      break;
   case QDbJoinedTable::RightJoin:
      joinStatement += QLatin1String(" RIGHT OUTER JOIN ");
      break;
   case QDbJoinedTable::OuterJoin:
      joinStatement += QLatin1String(" FULL OUTER JOIN ");
      break;
   case QDbJoinedTable::CrossJoin:
      joinStatement += QLatin1String(", ");
      break;
   default: break;
   }

   joinStatement += _rightTable._data->sql(builderContext);

   if (_condition.isValid())
   {
      joinStatement += QLatin1String(" ON ");
      joinStatement += _condition._data->sql(builderContext, true);
   }

   return joinStatement;
}

void QDbJoinedTableData::toXml(QDbXmlWriter* writer) const
{
   writer->writeAttribute("type", QLatin1String("Join"));
   switch (_joinMode)
   {
   case QDbJoinedTable::InnerJoin:
      writer->writeAttribute("mode", QLatin1String("inner"));
      break;
   case QDbJoinedTable::LeftJoin:
      writer->writeAttribute("mode", QLatin1String("left"));
      break;
   case QDbJoinedTable::RightJoin:
      writer->writeAttribute("mode", QLatin1String("right"));
      break;
   case QDbJoinedTable::OuterJoin:
      writer->writeAttribute("mode", QLatin1String("outer"));
      break;
   default: break;
   }

   writer->writeTable(_leftTable._data.data(), "LeftTable");
   writer->writeTable(_rightTable._data.data(), "RightTable");
   writer->writeStartElement("Condition");
   _condition.toXml(writer);
   writer->writeEndElement(); // Condition
}

QDbInlineTableData::QDbInlineTableData(const bp<QDbCommandData>& command, const QString& alias): _command(command), _alias(alias)
{
   if (!_alias.isEmpty())
      _alias = "\"" % _alias % "\"";
}

QString QDbInlineTableData::name() const
{
   return _alias;
}

QString QDbInlineTableData::alias() const
{
   return _alias;
}

QString QDbInlineTableData::sql(QDbSqlBuilderContext* builderContext) const
{
   QString sqlCommand;

   if (_command)
   {
      sqlCommand = "(" + _command->createSql(builderContext) + ")";

      auto alias = builderContext->getOrCreateTableAlias(this, _alias);

      if (!alias.isEmpty())
      {
         sqlCommand += " " % alias;
      }
   }

   return sqlCommand;
}

void QDbInlineTableData::toXml(QDbXmlWriter* writer) const
{
   writer->writeAttribute("type", QLatin1String("View"));
   if (!_alias.isEmpty())
      writer->writeAttribute("alias", _alias);
   if (_command)
      writer->writeQuery(_command.data());
}

QDbValuesTableData::QDbValuesTableData(const QString &name) : _alias(name)
{
}

QDbValuesTableData::~QDbValuesTableData()
{
}

QString QDbValuesTableData::name() const
{
    return _alias;
}

QString QDbValuesTableData::alias() const
{
   return _alias;
}

QString QDbValuesTableData::sql(QDbSqlBuilderContext* builderContext) const
{
   auto alias = builderContext->getOrCreateTableAlias(this, _alias);

   QString sql;
   if (builderContext->sqlStyle() == SqlStyle::SqlServer)
   {
      sql = "SELECT ";
      sql += assignemntColumns().join(", ");
      sql += " FROM ";
      sql += "(VALUES ";
      sql += sqlValues(builderContext);
      sql += ") AS ";
      sql += alias;
      sql += "(";
      sql += assignemntColumns().join(", ");
      sql += ")";
   }
   else if (builderContext->sqlStyle() == SqlStyle::Oracle)
   {
      sql = sqlValuesAsSelectStatement(builderContext);
   }
   else
   {
      sql = "VALUES ";
      sql += sqlValues(builderContext);
   }

   return sql;
}

void QDbValuesTableData::toXml(QDbXmlWriter *writer) const
{
}

void QDbValuesTableData::setValue(const QDbColumn& column, const sp<QDbExpressionData>& value)
{
   auto tableColumn = column._data.as<QDbExprTableColumnData>();

   if (tableColumn && tableColumn->_table.data() == dynamic_cast<QDbTableData*>(this))
   {
      setValue(tableColumn->_name, value);
   }
}

void QDbValuesTableData::setValue(const QString& column, const sp<QDbExpressionData>& value)
{
    if (_assignmentRow == _assignmentValuesList.count())
        _assignmentValuesList.append(QList<sp<QDbExpressionData>>());

    auto columnIndex = _assignmentColumns.indexOf(column);
    if (columnIndex == -1)
    {
       columnIndex = _assignmentColumns.length();
       _assignmentColumns.append(column);
    }

    while (columnIndex >= _assignmentValuesList.at(_assignmentRow).count())
    {
        _assignmentValuesList[_assignmentRow].append(nullptr);
    }

    _assignmentValuesList[_assignmentRow][columnIndex] = value;
}

void QDbValuesTableData::stashRow()
{
   if (_assignmentRow < _assignmentValuesList.count())
      _assignmentRow++;
}

void QDbValuesTableData::stashClear()
{
   _assignmentRow = 0;
}

QString QDbValuesTableData::sqlValues(QDbSqlBuilderContext* builderContext, const QList<sp<QDbExpressionData>>& columns) const
{
   QList<QStringList> strValuesList;

   auto end = qMin(_assignmentValuesList.length(), builderContext->currentRow() + builderContext->maxRowsPerDmlCall());

   while (builderContext->currentRow() < end)
   {
      auto values = _assignmentValuesList.at(builderContext->currentRow());
      QStringList strValues;

      if (columns.isEmpty())
      {
         for (auto i = 0; i < _assignmentColumns.count(); ++i)
         {
            // Some databases do not support dots in assignment names
            auto value = values.value(i)->sql(builderContext, false);

            Q_ASSERT(!value.isEmpty());

            strValues.append(value);
         }
      }
      else
      {
         for (auto&& column : columns)
         {
            QString value;
            auto tableColumn = column.as<QDbExprTableColumnData>();

            if (tableColumn && tableColumn->_table.data() == dynamic_cast<const QDbTableData*>(this))
            {
               auto columnIndex = _assignmentColumns.indexOf(tableColumn->_name);
               if (columnIndex != -1)
               {
                  value = values.value(columnIndex)->sql(builderContext, false);
               }
            }
            else
            {
               value = column->sql(builderContext, false);
            }

            Q_ASSERT(!value.isEmpty());

            strValues.append(value);
         }
      }

      strValuesList.append(strValues);

      builderContext->incrementCurrentRow();
   }

   builderContext->setHasMore(builderContext->currentRow() < _assignmentValuesList.length());

   return toStringList(strValuesList, [](const QStringList& strValues) { return "(" + strValues.join(", ") + ")"; }).join(", ");
}

QString QDbValuesTableData::sqlValuesAsSelectStatement(QDbSqlBuilderContext *builderContext) const
{
   auto end = qMin(_assignmentValuesList.length(), builderContext->currentRow() + builderContext->maxRowsPerDmlCall());

   QStringList selectLines;
   while (builderContext->currentRow() < end)
   {
      QStringList selectList;
      for (auto i = 0; i < _assignmentColumns.count(); ++i)
      {
         selectList += _assignmentValuesList.at(builderContext->currentRow()).at(i)->sql(builderContext, false) + " AS " + _assignmentColumns.at(i);
      }

      QString selectLine;

      selectLine = "SELECT ";
      selectLine += selectList.join(", ");
      if (builderContext->sqlStyle() == SqlStyle::Oracle)
      {
         selectLine += " FROM dual";
      }

      selectLines.append(selectLine);

      builderContext->incrementCurrentRow();
   }

   builderContext->setHasMore(builderContext->currentRow() < _assignmentValuesList.length());

   return selectLines.join(" UNION ALL\n");
}
