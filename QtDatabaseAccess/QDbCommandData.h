#pragma once
#include <QSharedData>
#include <SmartPointer.h>
#include <QList>

class QDbTableData;
class QDbExpressionData;
class QDbBoundColumn;
class QDbAssignment;
class QDbExprTableColumnData;
class QDbSqlBuilderContext;

class QDbCommandData : public QSharedData
{
   Q_DISABLE_COPY(QDbCommandData)

public:
   QDbCommandData();
   ~QDbCommandData();

   QString createSql(QDbSqlBuilderContext* builderContext) const;

public:
   enum CommandType { Select, Insert, Update, Merge, Delete };

public:
   CommandType _commandType = Select;
   bp<QDbTableData> _from; // Table data for select and delete. For select it may be a joined table, a subselect, a view etc.
   sp<QDbExpressionData> _where;
   bool _distinct = false;
   sp<QDbExpressionData> _having;
   int _offset = 0;
   int _limit = 0;
   QList<bp<QDbBoundColumn>> _bindings; // Select or returning list
   QList<bp<QDbBoundColumn>> _orderBy; // Order sequence
   bp<QDbTableData> _in; // Table data for update and insert.
   QList<QDbAssignment> _assignments;
   bool _usePostLob = false;

private:
   QString createMergeCommand(QDbSqlBuilderContext* builderContext) const;
};
