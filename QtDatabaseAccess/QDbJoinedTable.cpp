#include "QDbJoinedTable.h"
#include "QDbTableData.h"
#include "QDbExpressionData.h"

QDbJoinedTable::QDbJoinedTable()
{
}

QDbJoinedTable::QDbJoinedTable(const QDbDataSource& left, const QDbDataSource& right, const QDbExpression& condition, JoinMode joinMode)
{
   _data = new QDbJoinedTableData(left, right, joinMode == CrossJoin && condition.isValid() ? OuterJoin : joinMode, condition);
}

QDbJoinedTable::QDbJoinedTable(const QDbJoinedTable& other) : QDbDataSource(other)
{
}

QDbJoinedTable::~QDbJoinedTable()
{
}

QDbJoinedTable& QDbJoinedTable::operator=(const QDbJoinedTable& other)
{
   QDbDataSource::operator=(other);
   return *this;
}

QDbJoinedTable& QDbJoinedTable::on(const QDbExpression& condition)
{
   auto data = _data.as<QDbJoinedTableData>();

   if (data)
      data->setCondition(condition);
   return *this;
}

void QDbJoinedTable::setJoinMode(JoinMode joinMode)
{
   auto data = _data.as<QDbJoinedTableData>();

   if (data)
      data->setJoinMode(joinMode);
}
