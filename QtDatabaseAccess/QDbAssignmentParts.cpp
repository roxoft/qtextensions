#include "QDbAssignmentParts.h"
#include <QDbConnection.h>
#include "QDbExpressionData.h"

void QDbAssignmentParts::setFrom(const QList<QDbAssignment>& assignments, const QList<bp<QDbBoundColumn>>& bindings, QDbSqlBuilderContext* builderContext, bool useQualifiedNames, bool usePostLob)
{
   strColumns.clear();
   strValues.clear();
   returningColumns.clear();
   returningPhList.clear();

   // Fill the returningColumnList and the returningPhList.
   // The bindings must be appended to the returning list before the lob columns are appended
   if (!bindings.isEmpty())
   {
      if (builderContext->sqlStyle() == SqlStyle::Oracle)
      {
         for (auto&& boundColumn : bindings)
         {
            // Do not use placeholder in the select list
            // Oracle does not allow dots in the returning names
            returningColumns.append(boundColumn->column._data->sql(builderContext, false));
            returningPhList.append(builderContext->getParameter(boundColumn->boundValue, boundColumn->type, TM_Output));
         }
      }
      else if (builderContext->sqlStyle() == SqlStyle::SqlServer)
      {
         for (auto&& boundColumn : bindings)
         {
            // Currently only INSERTED output values are supported

            // Do not use placeholder in the select list
            returningColumns.append(boundColumn->column._data->sql(builderContext, false));

            // Inserting into parameters is not supperted by ODBC
            //if (phMap && boundColumn->type != DT_UNKNOWN)
            //{
            //   returningPhList.append(builderContext->getParameter(boundColumn->boundValue, boundColumn->type, TM_Output));
            //}
         }
      }
      else if (builderContext->sqlStyle() == SqlStyle::DB2)
      {
         for (auto&& boundColumn : bindings)
         {
            // Do not use placeholder in the select list
            returningColumns.append(boundColumn->column._data->sql(builderContext, false));
         }
      }
   }

   // Fill strColumns, strValuesList and add LOBs to the returning lists if they should be send after execution.

   for (auto&& assignment : assignments)
   {
      auto sqlLeft = assignment.left()._data->sql(builderContext, builderContext->sqlStyle() == SqlStyle::SQLite ? false : useQualifiedNames);
      auto sqlRight = assignment.right()._data->sql(builderContext, useQualifiedNames); // Only use qualified names in statements having a FROM clause because some databases do not support dots in assignment names

      strColumns.append(sqlLeft);

      Q_ASSERT(!sqlRight.isEmpty());

      // If the database is ORACLE and the column is a blob or a clob it must appear at the end of the statement!!!
      if (usePostLob && builderContext->sqlStyle() == SqlStyle::Oracle)
      {
         auto replacementValue = builderContext->tryChangePlaceholderToPostLob(sqlRight, assignment.left()._data);

         if (!replacementValue.isEmpty())
         {
            returningColumns.append(sqlLeft);
            returningPhList.append(sqlRight);
            sqlRight = replacementValue;
         }
      }

      strValues.append(sqlRight);
   }
}
