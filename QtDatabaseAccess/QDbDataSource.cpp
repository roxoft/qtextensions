#include "QDbDataSource.h"
#include "QDbJoinedTable.h"
#include "QDbReader.h"
#include "QDbSelector.h"
#include "QDbTableData.h"
#include "QDbExpressionData.h"

QDbDataSource::QDbDataSource()
{
}

QDbDataSource::QDbDataSource(const QDbDataSource& other) : _data(other._data)
{
}

QDbDataSource::~QDbDataSource()
{
}

QDbDataSource& QDbDataSource::operator=(const QDbDataSource& other)
{
   _data = other._data;
   return *this;
}

bool QDbDataSource::operator==(const QDbDataSource& other) const
{
   return _data == other._data;
}

QDbJoinedTable QDbDataSource::innerJoin(const QDbDataSource& table, const QDbExpression& condition) const
{
   return QDbJoinedTable(*this, table, condition, QDbJoinedTable::InnerJoin);
}

QDbJoinedTable QDbDataSource::leftJoin(const QDbDataSource& table, const QDbExpression& condition) const
{
   return QDbJoinedTable(*this, table, condition, QDbJoinedTable::LeftJoin);
}

QDbJoinedTable QDbDataSource::rightJoin(const QDbDataSource& table, const QDbExpression& condition) const
{
   return QDbJoinedTable(*this, table, condition, QDbJoinedTable::RightJoin);
}

QDbJoinedTable QDbDataSource::outerJoin(const QDbDataSource& table, const QDbExpression& condition) const
{
   return QDbJoinedTable(*this, table, condition, QDbJoinedTable::OuterJoin);
}

QDbJoinedTable QDbDataSource::crossJoin(const QDbDataSource& table) const
{
   return QDbJoinedTable(*this, table);
}

QDbJoinedTable QDbDataSource::operator*(const QDbDataSource& table) const
{
   return crossJoin(table);
}

QDbReader QDbDataSource::reader(const QDbConnection& connection) const
{
   return QDbReader(*this, connection);
}

QDbSelector QDbDataSource::selector(const QString& alias) const
{
   return QDbSelector(*this, alias);
}
