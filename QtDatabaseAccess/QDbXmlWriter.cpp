#include "QDbXmlWriter.h"
#include "QDbTableData.h"
#include "QDbExpressionData.h"

QDbXmlWriter::QDbXmlWriter()
{
}

QDbXmlWriter::~QDbXmlWriter()
{
}

void QDbXmlWriter::writeQuery(const QDbCommandData* query, const char* name)
{
   int id = _queryMap.value(query, -1);

   writeStartElement(name);
   writeAttribute("version", "10");
   if (id >= 0)
   {
      writeAttribute("id", QString::number(id));
   }
   else
   {
      //query->toXml(this);
      _queryMap.insert(query, _queryMap.count());
   }
   writeEndElement();
}

void QDbXmlWriter::writeTable(const QDbTableData* table, const char* name)
{
   int id = _tableMap.value(table, -1);

   writeStartElement(name);
   if (id >= 0)
   {
      writeAttribute("id", QString::number(id));
   }
   else
   {
      table->toXml(this);
      _tableMap.insert(table, _tableMap.count());
   }
   writeEndElement();
}

void QDbXmlWriter::writeColumn(const QDbExpressionData* column, const char* name)
{
   int id = _columnMap.value(column, -1);

   writeStartElement(name);
   if (id >= 0)
   {
      writeAttribute("id", QString::number(id));
   }
   else
   {
      column->toXml(this);
      _columnMap.insert(column, _columnMap.count());
   }
   writeEndElement();
}
