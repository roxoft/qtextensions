<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE">
<context>
    <name>TestCases</name>
    <message>
        <location filename="TestCases.cpp" line="215"/>
        <source>Missing data folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="TestCases.cpp" line="221"/>
        <source>Missing Oracle credentials</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="TestCases.cpp" line="226"/>
        <source>Missing SqlServer credentials</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="TestCases.cpp" line="231"/>
        <source>Missing DB2 credentials</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="TestCases.cpp" line="1854"/>
        <source>Missing Oracle database credentials</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="TestCases.cpp" line="1886"/>
        <source>Missing SqlServer database credentials</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="TestCases.cpp" line="1954"/>
        <source>Missing DB2 database credentials</source>
        <oldsource>Missing DB2 database credentials: Test skipped</oldsource>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
