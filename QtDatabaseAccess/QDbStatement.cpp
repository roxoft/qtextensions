#include "QDbStatement.h"

QDbState QDbStatement::exec(const QDbConnection &connection, int rowsToFetch)
{
   QDbCommand command(connection);
   return exec(&command, rowsToFetch);
}

QDbState QDbStatement::exec(QDbCommand *command, int rowsToFetch)
{
   if (command == nullptr)
   {
      return QDbState(QDbState::Error, "command is null.");
   }

   if (command->state().type() == QDbState::Error)
   {
      return command->state();
   }

   if (command->statement() != _sqlStatement)
   {
      command->setStatement(_sqlStatement);
   }

   bool hasPostLob = false;

   for (const auto& ph : _phList)
   {
      if (ph.isPostLob)
      {
         command->setPlaceholderType(ph.name, ph.getValue().type() == typeid(QString) || ph.getValue().type() == typeid(QText) ? DT_LARGE_STRING : DT_LARGE_BINARY);
         hasPostLob = true;
      }
      else if (ph.transferMode == TM_Output)
      {
         command->setPlaceholderType(ph.name, ph.type);
      }
      else
      {
         command->setPlaceholderValue(ph.name, ph.getValue(), ph.type, ph.transferMode);
      }

      if (command->state().type() == QDbState::Error)
      {
         return command->state();
      }
   }

   QSqlTransaction transaction;

   if (hasPostLob)
      transaction.begin(command->connection());

   if (!command->exec(rowsToFetch))
   {
      return command->state();
   }

   if (hasPostLob)
   {
      for (auto&& ph : _phList)
      {
         if (ph.isPostLob)
            command->setPlaceholderValue(ph.name, ph.getValue());
      }

      if (command->state().type() == QDbState::Error)
      {
         return command->state();
      }

      transaction.commit();
   }

   // Fetch returning values
   for (auto&& ph : _phList)
   {
      if (!ph.isPostLob && (ph.transferMode & TM_Output))
         ph.setValue(command->placeholderValue(ph.name));
   }

   return command->state();
}
