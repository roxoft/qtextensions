#include "QDbValuesTable.h"
#include "QDbExpressionData.h"
#include "QDbTableData.h"

QDbValuesTable::QDbValuesTable()
{
   _data = new QDbValuesTableData;
}

QDbValuesTable::QDbValuesTable(const QString &name)
{
   _data = new QDbValuesTableData(name.trimmed().toUpper());
}

QDbValuesTable::QDbValuesTable(const QDbValuesTable& other) : QDbDataSource(other)
{
}

QDbValuesTable::~QDbValuesTable()
{
}

QDbColumn QDbValuesTable::column(const QString& name)
{
   return QDbColumn(*this, name);
}

QDbValuesTable& QDbValuesTable::operator=(const QDbValuesTable& other)
{
   QDbDataSource::operator=(other);
   return *this;
}

QDbValuesTable& QDbValuesTable::set(const QDbColumn& column, const QDbExpression& value)
{
   const auto data = _data.as<QDbValuesTableData>();

   if (data)
      data->setValue(column, value._data);

   return *this;
}

QDbValuesTable& QDbValuesTable::set(const QDbColumn& column, bool value) { return set(column, Variant(value)); }
QDbValuesTable& QDbValuesTable::set(const QDbColumn& column, int value) { return set(column, Variant(value)); }
QDbValuesTable& QDbValuesTable::set(const QDbColumn& column, long long int value) { return set(column, Variant(value)); }
QDbValuesTable& QDbValuesTable::set(const QDbColumn& column, double value) { return set(column, Variant(value)); }
QDbValuesTable& QDbValuesTable::set(const QDbColumn& column, const char *value) { return set(column, Variant(value)); }
QDbValuesTable& QDbValuesTable::set(const QDbColumn& column, const QString& value) { return set(column, Variant(value)); }
QDbValuesTable& QDbValuesTable::set(const QDbColumn& column, const QRational& value) { return set(column, Variant(value)); }
QDbValuesTable& QDbValuesTable::set(const QDbColumn& column, const QDecimal& value) { return set(column, Variant(value)); }
QDbValuesTable& QDbValuesTable::set(const QDbColumn& column, const QDate& value) { return set(column, Variant(value)); }
QDbValuesTable& QDbValuesTable::set(const QDbColumn& column, const QTime& value) { return set(column, Variant(value)); }
QDbValuesTable& QDbValuesTable::set(const QDbColumn& column, const QDateTime& value) { return set(column, Variant(value)); }
QDbValuesTable& QDbValuesTable::set(const QDbColumn& column, const QDateTimeEx& value) { return set(column, Variant(value)); }
QDbValuesTable& QDbValuesTable::set(const QDbColumn& column, const QByteArray& value) { return set(column, Variant(value)); }
QDbValuesTable& QDbValuesTable::set(const QDbColumn& column, const Variant& value)
{
   const auto data = _data.as<QDbValuesTableData>();

   if (data)
      data->setValue(column, new QDbExprValueData(value));

   return *this;
}

void QDbValuesTable::stashRow()
{
   const auto data = _data.as<QDbValuesTableData>();

   if (data)
      data->stashRow();
}

int QDbValuesTable::rowCount() const
{
   const auto data = _data.as<QDbValuesTableData>();

   if (data)
      return data->rowCount();

   return -1;
}

void QDbValuesTable::stashClear()
{
   const auto data = _data.as<QDbValuesTableData>();

   if (data)
      data->stashClear();
}
