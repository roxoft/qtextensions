#include "QDbSelectParts.h"
#include <QStringBuilder>
#include "QDbExpressionData.h"
#include "QDbSqlBuilderContext.h"

void QDbSelectParts::setFrom(const QList<bp<QDbBoundColumn>>& bindings, QList<bp<QDbBoundColumn>> orderBy, QDbSqlBuilderContext* builderContext)
{
   orderList.clear();
   selectList.clear();
   group = false;
   groupList.clear();

   // Find the auto alias offset

   int aliasNumber = 0;

   for (const auto& boundColumn : bindings)
   {
      auto alias = boundColumn->alias;

      if (alias.startsWith("\"") && alias.endsWith("\""))
         alias = alias.mid(1, alias.length() - 2);

      if (alias.startsWith("COL", Qt::CaseInsensitive))
      {
         bool ok;
         const auto number = alias.mid(3).toInt(&ok);
         if (ok && number > aliasNumber)
         {
            aliasNumber = number;
         }
      }
   }

   // Process the columns

   for (auto i = 0; i < orderBy.count(); ++i)
      orderList.append(QString());

   for (const auto& boundColumn : bindings)
   {
      auto colRef = boundColumn->column._data->sql(builderContext, true); // Do not use placeholder in the select list

      auto alias = boundColumn->alias;

      // Generate an auto-alias for complex columns
      if (alias.isEmpty() && boundColumn->column._data->isComplex())
      {
         alias = "COL" + QString::number(++aliasNumber);
      }

      // groupable columns

      if (boundColumn->column.isGroupable())
         groupList.append(colRef);

      // is grouping necessary?

      if (boundColumn->column.isAggregate())
         group = true;

      // orderList

      if (boundColumn->sortOrder != QDb::OrderArbitrary)
      {
         auto orderIndex = orderBy.indexOf(boundColumn);

         if (orderIndex == -1)
         {
            orderIndex = orderList.count();
            orderList.append(QString());
         }

         QString orderClause;

         if (!alias.isEmpty())
            orderClause = alias;
         else
            orderClause = colRef;

         if (boundColumn->sortOrder == QDb::OrderDescending)
            orderClause += " DESC";

         orderList[orderIndex] = orderClause;
      }

      // selectList

      if (!alias.isEmpty())
         colRef = colRef % " AS " % alias;

      selectList.append(colRef);
   }

   for (auto i = 0; i < orderBy.count(); ++i)
   {
      if (orderList.at(i).isEmpty() && orderBy.at(i)->sortOrder != QDb::OrderArbitrary)
      {
         orderList[i] = orderBy.at(i)->column._data->sql(builderContext, true); // Do not use placeholder in the order list

         if (orderBy.at(i)->sortOrder == QDb::OrderDescending)
            orderList[i] += " DESC";
      }
   }
}
