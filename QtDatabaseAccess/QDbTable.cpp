#include "QDbTable.h"
#include "QDbTableData.h"
#include "QDbInserter.h"
#include "QDbUpdater.h"
#include "QDbMerger.h"
#include "QDbDeleter.h"
#include "QDbExpressionData.h"

QDbTable::QDbTable()
{
}

QDbTable::QDbTable(QString tableName, QString alias)
{
   _data = new QDbPlainTableData(tableName.trimmed().toUpper(), alias);
}

QDbTable::QDbTable(const QDbTable& other) : QDbDataSource(other)
{
}

QDbTable::QDbTable(const QDbDataSource& dataSource) : QDbDataSource(dataSource)
{
}

QDbTable::~QDbTable()
{
}

QDbTable& QDbTable::operator=(const QDbTable& other)
{
   QDbDataSource::operator=(other);
   return *this;
}

QString QDbTable::name() const
{
   if (_data)
      return _data->name();
   return QString();
}

QDbInserter QDbTable::inserter() const
{
   return QDbInserter(*this);
}

QDbUpdater QDbTable::updater() const
{
   return QDbUpdater(*this);
}

QDbMerger QDbTable::merger() const
{
   return QDbMerger(*this);
}

QDbDeleter QDbTable::deleter() const
{
   return QDbDeleter(*this);
}

QDbColumn QDbTable::column(const QString& name) const
{
   const auto dbName = name.trimmed().toUpper();

   if (!dbName.isEmpty())
      return QDbColumn(*this, dbName);

   return QDbColumn();
}

QDbSchema::Table QDbTable::schema(const QDbConnection& connection) const
{
   const auto data = _data.as<QDbPlainTableData>();

   if (!data)
      return QDbSchema::Table();

   return data->tableSchema(connection);
}
