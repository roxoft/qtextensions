#include "QDbColumn.h"
#include "QDbExpressionData.h"
#include "QDbAssignment.h"

QDbColumn::QDbColumn(const QDbDataSource &table, const QString &name)
{
   _data = new QDbExprTableColumnData(table._data, name);
}

QDbDataSource QDbColumn::table() const
{
   const auto data = _data.as<QDbExprTableColumnData>();

   QDbDataSource result;

   if (data)
      result._data = data->_table;

   return result;
}

QString QDbColumn::name() const
{
   const auto data = _data.as<QDbExprTableColumnData>();

   if (data)
      return data->_name;

   return QString();
}

//QDbSchema::Column QDbColumn::schema() const
//{
//   QDbSchema::Column columnSchema;
//   auto data = _data.as<QDbExprTableColumnData>();
//
//   if (data)
//   {
//      columnSchema.name = data->_name;
//      columnSchema.type = data->_type;
//      columnSchema.size = data->_size;
//      columnSchema.scale = data->_scale;
//   }
//
//   return columnSchema;
//}

QDbAssignment QDbColumn::to(const QDbExpression& rhs) const
{
   return QDbAssignment(*this, rhs);
}

QDbAssignment QDbColumn::to(const Variant& value) const
{
   return QDbAssignment(*this, value);
}
