#ifndef QDBTABLEDATA_H
#define QDBTABLEDATA_H

#include <QSharedData>
#include "QDbExpression.h"
#include "QDbJoinedTable.h" // JoinMode

class QDbExprTableColumnData;
class QDbXmlReader;
class QDbXmlWriter;

class QDbTableData : public QSharedData
{
   Q_DISABLE_COPY(QDbTableData)

public:
   QDbTableData() = default;
   virtual ~QDbTableData() = default;

   virtual QString name() const = 0;
   virtual QString alias() const = 0;

   virtual QString sql(QDbSqlBuilderContext* builderContext) const = 0;

   virtual void toXml(QDbXmlWriter *writer) const = 0;

   static QDbTableData* fromXml(QDbXmlReader *reader);
};

class QDbPlainTableData : public QDbTableData
{
   Q_DISABLE_COPY(QDbPlainTableData)

public:
   QDbPlainTableData(const QString& name, const QString& alias = QString());
   ~QDbPlainTableData() override = default;

   QString name() const override;
   QString alias() const override;
   QString sql(QDbSqlBuilderContext* builderContext) const override;

   void toXml(QDbXmlWriter *writer) const override;

   const QDbSchema::Table& tableSchema(const QDbConnection& connection) const;

private:
   QString _name;
   QString _alias;
   mutable QDbSchema::Table _tableSchema; // Cache
};

class QDbJoinedTableData : public QDbTableData
{
   Q_DISABLE_COPY(QDbJoinedTableData)

public:
   QDbJoinedTableData(const QDbDataSource& leftTable, const QDbDataSource& rightTable, QDbJoinedTable::JoinMode joinMode, const QDbExpression &condition)
      : _leftTable(leftTable), _rightTable(rightTable), _joinMode(joinMode), _condition(condition) {}
   ~QDbJoinedTableData() override = default;

   QString name() const override;
   QString alias() const override;
   QString sql(QDbSqlBuilderContext* builderContext) const override;

   void toXml(QDbXmlWriter *writer) const override;

   void setJoinMode(QDbJoinedTable::JoinMode joinMode) { _joinMode = joinMode; }
   void setCondition(const QDbExpression& condition) { _condition = condition; }

private:
   QDbDataSource _leftTable;
   QDbDataSource _rightTable;
   QDbJoinedTable::JoinMode _joinMode;
   QDbExpression _condition;
};

class QDbInlineTableData : public QDbTableData
{
   friend class QDbSelector;

   Q_DISABLE_COPY(QDbInlineTableData)

public:
   QDbInlineTableData(const bp<QDbCommandData>& command, const QString& alias);
   ~QDbInlineTableData() override = default;

   QString name() const override;
   QString alias() const override;
   QString sql(QDbSqlBuilderContext* builderContext) const override;

   void toXml(QDbXmlWriter *writer) const override;

private:
   bp<QDbCommandData> _command;
   QString _alias;
};

class QDbValuesTableData : public QDbTableData
{
    Q_DISABLE_COPY(QDbValuesTableData)

public:
    QDbValuesTableData(const QString& name = QString());
    ~QDbValuesTableData() override;

    QString name() const override;
    QString alias() const override;
    QString sql(QDbSqlBuilderContext* builderContext) const override;

    void toXml(QDbXmlWriter *writer) const override;

    void setValue(const QDbColumn& column, const sp<QDbExpressionData>& value);
    void setValue(const QString& column, const sp<QDbExpressionData>& value);

    void stashRow();
    int rowCount() const { return _assignmentValuesList.length(); }
    void stashClear();

    const QStringList& assignemntColumns() const { return _assignmentColumns; }

    QString sqlValues(QDbSqlBuilderContext* builderContext, const QList<sp<QDbExpressionData>>& columns = QList<sp<QDbExpressionData>>()) const;
    QString sqlValuesAsSelectStatement(QDbSqlBuilderContext* builderContext) const;

private:
    QString _alias;
    QStringList _assignmentColumns;
    QList<QList<sp<QDbExpressionData>>> _assignmentValuesList;
    int _assignmentRow = 0;
};

#endif // QDBTABLEDATA_H
