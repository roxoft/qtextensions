QtExtensions
============

QtExtensions is a collection of libraries with general extensions to the Qt libraries.

QtExtensions contains the following dynamic libraries:
 - [QtCoreEx](QtCoreEx.html): Basic (non GUI) extensions for all other libraries containing data types, models etc.
 - [QtGuiEx](QtGuiEx.html) (based on QtCoreEx): Extensions to the GUI of Qt (views, delegates etc.).
 - QtGuiExDesignerPlugin (based on QtGuiEx): Encapsulates the views in QtQuiEx for the Qt Designer.
 - QtService (based on QtCoreEx): Framework for creating services (Windows Service or Linux daemons).
 - [QtDatabaseAccess](QtDatabaseAccess.html) (based on QtCoreEx): Framework for creating SQL statements without writing SQL. Replaces the QSqlStatement framework in QtCoreEx.
 - QtODBCConnector (based on QtCoreEx): Driver for ODBC databases.
 - QtOracleConnector (based on QtCoreEx): Driver for Oracle databases (currently only for Windows). Requires an Oracle client installation.
 - QtSQLiteConnector (based on QtCoreEx): Driver for SQLite databases. No additional driver installation necessary.
 - QtDB2Connector (based on QtCoreEx): Driver for DB/2 databases (currently only for Windows). Requires a DB/2 client installation.
 
 Test framework:
 - [TestFramework](TestFrameworkOverview.html): Framework for creating tests (unit tests). This is a static library.
 - [TestRunner](TestRunner.html): Executable (with or without GUI) to run tests based on the TestFramework.

Highlights
----------

### QtCoreEx:

|---------------------------------------------------------|---------------------------------------------------------------------------------------------------------|
| [QDecimal](QDecimal.html#QDecimal)                      | Storing and calculation with decimal precision. Also useful for very large integers.                    |
|---------------------------------------------------------|---------------------------------------------------------------------------------------------------------|
| [QDateTimeEx](QDateTimeEx.html#QDateTimeEx)             | Storing date and time values with preserving their precision (for example minutes or days)               |
|---------------------------------------------------------|---------------------------------------------------------------------------------------------------------|
| [Variant](Variant.html#Variant)                         | Replaces QVariant. Easier to add custom types without registration and better type conversion.|
|---------------------------------------------------------|---------------------------------------------------------------------------------------------------------|
| [HtStandardModel](HtStandardModel.html#HtStandardModel) | Replaces QStandardItemModel with a hierarchical representation of data.                                 |
|---------------------------------------------------------|---------------------------------------------------------------------------------------------------------|
| [FunctionStatus](FunctionStatus.html#FunctionStatus)    | Report unhandled execution states (errors, warnings, ...) to the caller (without interrupting execution).|
|---------------------------------------------------------|---------------------------------------------------------------------------------------------------------|
| [QLocaleEx](QLocaleEx.html#QLocaleEx)                   | Better locale configuration (especially for dates).                                                     |
|---------------------------------------------------------|---------------------------------------------------------------------------------------------------------|
| [QSettingsEx](QSettingsEx.html#QSettingsEx)             | Hierarchical processing of settings. Can be derived to handle, for example, settings in databases. |
|---------------------------------------------------------|---------------------------------------------------------------------------------------------------------|
| [QDbConnection](QDbConnection.html#QDbConnection)       | Unified interface for communication with SQL databases.                                                 |
|---------------------------------------------------------|---------------------------------------------------------------------------------------------------------|

### QtGuiEx:

|---------------------------------------------------------|---------------------------------------------------------------------------------------------------------|
| [QSelector](QSelector.html#QSelector) in combination    | Replaces QComboBox. Pop-up is resizable and able to display sortable tables.                            |
| with [QComboBoxEx](QComboBoxEx.html#QComboBoxEx)        |                                                                                                         |
|---------------------------------------------------------|---------------------------------------------------------------------------------------------------------|
| [QWaitOverrideCursor](QWaitOverrideCursor.html#QWaitOverrideCursor) | Safely override the global cursor.                                                          |
|---------------------------------------------------------|---------------------------------------------------------------------------------------------------------|
| [QTreeViewEx](QTreeViewEx.html#QTreeViewEx)             | Reimplemented drag&drop functionality. Resizeable columns with invisible header. Hidden text in tool-tip. |
|---------------------------------------------------------|---------------------------------------------------------------------------------------------------------|
| [QTableViewEx](QTableViewEx.html#QTableViewEx)          | ResizeColumnsToContents reimplemented. ResizeRowsToContents added. Hidden text in tool-tip.             |
|---------------------------------------------------------|---------------------------------------------------------------------------------------------------------|
| [QIncrementalSearchWidget](QIncrementalSearchWidget.html#QIncrementalSearchWidget) | Search tool bar for QTableViewEx and QTreeViewEx.                            |
|---------------------------------------------------------|---------------------------------------------------------------------------------------------------------|
| [QDateEditEx](QDateEditEx.html#QDateEditEx)             | Easier date editing.                                                                                    |
|---------------------------------------------------------|---------------------------------------------------------------------------------------------------------|
| [QNumberEdit](QNumberEdit.html#QNumberEdit)             | For [QDecimal]s(QDecimal.html#QDecimal).                                                                |
|---------------------------------------------------------|---------------------------------------------------------------------------------------------------------|

### QtDatabaseAccess:

Generate SQL statements without writing SQL. C++ syntax and type checking for SQL expressions. Easy binding to variables and classes.

### QtOracleConnector:

Oracle database backend for QDbConnection.
Reliable and fast access to oracle databasese with connection pooling. The simple Qt interface hides complicated OCI calls and can be used with all kinds of oracle data types including lobs. Autodetection of unicode databases.

### QtDB2Connector:

DB/2 database backend for QDbConnection.
Reliable and fast access to DB/2 databasese.
