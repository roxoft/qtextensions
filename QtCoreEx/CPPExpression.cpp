#include "stdafx.h"
#include "CPPExpression.h"
#include "CPPTokenizer.h"
#include <typeindex>
#include <QValue>

// List based on CppAction in CPPLanguageConstants.h
static const char* actionNames[] = {
   "", //Invalid
   "++", //PostIncr
   "--", //PostDecr
   "()", //Function
   "[]", //Subscript
   "?[]", //NullConditionalSubscript
   ".", //Member
   "?.", //NullConditionalMember
   "{}", //Block
   "++", //PreIncr
   "--", //PreDecr
   "+", //Plus
   "-", //Minus
   "!", //LogNot
   "~", //BitNot
   "*", //Multiplication
   "/", //Division
   "%", //Modulus
   "+", //Addition
   "-", //Subtraction
   "<", //LessThan
   ">", //GreaterThan
   "<=", //LessOrEqual
   ">=", //GreaterOrEqual
   "==", //Equality
   "!=", //Inequality
   "&", //BitAnd
   "^", //BitXor
   "|", //BitOr
   "&&", //LogAnd
   "||", //LogOr
   "?:", //Conditional
   ":", //Colon
   "??", //NullCoalescing
   "#", //Repeating
   "#?", //Filter
   "=", //Assignment
   ":=", //Declaration
   ",", //Comma
   ";" //SemiColon
};

// List based on CppAction in CPPLanguageConstants.h
static QMap<std::type_index, Variant (*)(const Variant&, const Variant&, const QLocaleEx&)> binary_value_operation_list[] = {
   { }, // Invalid
   { }, // PostIncr
   { }, // PostDecr
   { }, // Function
   { }, // Subscript
   { }, // NullConditionalSubscript
   { }, // Member
   { }, // NullConditionalMember
   { }, // Block
   { }, // PreIncr
   { }, // PreDecr
   { }, // Plus
   { }, // Minus
   { }, // LogNot
   { }, // BitNot
   { // Multiplication
      { typeid(QDecimal), [](const Variant& left, const Variant& right, const QLocaleEx& locale) -> Variant { return left.toDecimal(nullptr, locale) * right.toDecimal(nullptr, locale); } },
      { typeid(double), [](const Variant& left, const Variant& right, const QLocaleEx& locale) -> Variant { return left.toDouble(nullptr, locale) * right.toDouble(nullptr, locale); } },
      { typeid(QValue), [](const Variant& left, const Variant& right, const QLocaleEx& locale) -> Variant { return QValue(left.toDecimal(nullptr, locale) * right.toDecimal(nullptr, locale), left.to<QValue>(nullptr, locale).unit() + right.to<QValue>(nullptr, locale).unit()); } },
      { typeid(long long), [](const Variant& left, const Variant& right, const QLocaleEx& locale) -> Variant { return left.toInt64(nullptr, locale) * right.toInt64(nullptr, locale); } },
      { typeid(unsigned long long), [](const Variant& left, const Variant& right, const QLocaleEx& locale) -> Variant { return left.toUInt64(nullptr, locale) * right.toUInt64(nullptr, locale); } },
      { typeid(int), [](const Variant& left, const Variant& right, const QLocaleEx& locale) -> Variant { return left.toInt(nullptr, locale) * right.toInt(nullptr, locale); } },
      { typeid(unsigned int), [](const Variant& left, const Variant& right, const QLocaleEx& locale) -> Variant { return left.toUInt(nullptr, locale) * right.toUInt(nullptr, locale); } }
   },
   { // Division
      { typeid(QDecimal), [](const Variant& left, const Variant& right, const QLocaleEx& locale) -> Variant { return left.toDecimal(nullptr, locale) / right.toDecimal(nullptr, locale); } },
      { typeid(double), [](const Variant& left, const Variant& right, const QLocaleEx& locale) -> Variant { return left.toDouble(nullptr, locale) / right.toDouble(nullptr, locale); } },
      { typeid(QValue), [](const Variant& left, const Variant& right, const QLocaleEx& locale) -> Variant { return QValue(left.toDecimal(nullptr, locale) / right.toDecimal(nullptr, locale), left.to<QValue>(nullptr, locale).unit() + "/" + right.to<QValue>(nullptr, locale).unit()); } },
      { typeid(long long), [](const Variant& left, const Variant& right, const QLocaleEx& locale) -> Variant { return left.toInt64(nullptr, locale) / right.toInt64(nullptr, locale); } },
      { typeid(unsigned long long), [](const Variant& left, const Variant& right, const QLocaleEx& locale) -> Variant { return left.toUInt64(nullptr, locale) / right.toUInt64(nullptr, locale); } },
      { typeid(int), [](const Variant& left, const Variant& right, const QLocaleEx& locale) -> Variant { return left.toInt(nullptr, locale) / right.toInt(nullptr, locale); } },
      { typeid(unsigned int), [](const Variant& left, const Variant& right, const QLocaleEx& locale) -> Variant { return left.toUInt(nullptr, locale) / right.toUInt(nullptr, locale); } }
   },
   { // Modulus
      { typeid(QDecimal), [](const Variant& left, const Variant& right, const QLocaleEx& locale) -> Variant { return left.toDecimal(nullptr, locale) % right.toDecimal(nullptr, locale); } },
      { typeid(long long), [](const Variant& left, const Variant& right, const QLocaleEx& locale) -> Variant { return left.toInt64(nullptr, locale) % right.toInt64(nullptr, locale); } },
      { typeid(unsigned long long), [](const Variant& left, const Variant& right, const QLocaleEx& locale) -> Variant { return left.toUInt64(nullptr, locale) % right.toUInt64(nullptr, locale); } },
      { typeid(int), [](const Variant& left, const Variant& right, const QLocaleEx& locale) -> Variant { return left.toInt(nullptr, locale) % right.toInt(nullptr, locale); } },
      { typeid(unsigned int), [](const Variant& left, const Variant& right, const QLocaleEx& locale) -> Variant { return left.toUInt(nullptr, locale) % right.toUInt(nullptr, locale); } }
   },
   { // Addition
      { typeid(QText), [](const Variant& left, const Variant& right, const QLocaleEx& locale) -> Variant { return left.toText(locale) + right.toText(locale); } },
      { typeid(QString), [](const Variant& left, const Variant& right, const QLocaleEx& locale) -> Variant { return left.toString(locale) + right.toString(locale); } },
      { typeid(QDecimal), [](const Variant& left, const Variant& right, const QLocaleEx& locale) -> Variant { return left.toDecimal(nullptr, locale) + right.toDecimal(nullptr, locale); } },
      { typeid(double), [](const Variant& left, const Variant& right, const QLocaleEx& locale) -> Variant { return left.toDouble(nullptr, locale) + right.toDouble(nullptr, locale); } },
      { typeid(QValue), [](const Variant& left, const Variant& right, const QLocaleEx& locale) -> Variant { return QValue(left.toDecimal(nullptr, locale) + right.toDecimal(nullptr, locale), left.to<QValue>(nullptr, locale).unit()); } },
      { typeid(long long), [](const Variant& left, const Variant& right, const QLocaleEx& locale) -> Variant { return left.toInt64(nullptr, locale) + right.toInt64(nullptr, locale); } },
      { typeid(unsigned long long), [](const Variant& left, const Variant& right, const QLocaleEx& locale) -> Variant { return left.toUInt64(nullptr, locale) + right.toUInt64(nullptr, locale); } },
      { typeid(int), [](const Variant& left, const Variant& right, const QLocaleEx& locale) -> Variant { return left.toInt(nullptr, locale) + right.toInt(nullptr, locale); } },
      { typeid(unsigned int), [](const Variant& left, const Variant& right, const QLocaleEx& locale) -> Variant { return left.toUInt(nullptr, locale) + right.toUInt(nullptr, locale); } }
   },
   { // Subtraction
      { typeid(QDecimal), [](const Variant& left, const Variant& right, const QLocaleEx& locale) -> Variant { return left.toDecimal(nullptr, locale) - right.toDecimal(nullptr, locale); } },
      { typeid(double), [](const Variant& left, const Variant& right, const QLocaleEx& locale) -> Variant { return left.toDouble(nullptr, locale) - right.toDouble(nullptr, locale); } },
      { typeid(QValue), [](const Variant& left, const Variant& right, const QLocaleEx& locale) -> Variant { return QValue(left.toDecimal(nullptr, locale) - right.toDecimal(nullptr, locale), left.to<QValue>(nullptr, locale).unit()); } },
      { typeid(long long), [](const Variant& left, const Variant& right, const QLocaleEx& locale) -> Variant { return left.toInt64(nullptr, locale) - right.toInt64(nullptr, locale); } },
      { typeid(unsigned long long), [](const Variant& left, const Variant& right, const QLocaleEx& locale) -> Variant { return left.toUInt64(nullptr, locale) - right.toUInt64(nullptr, locale); } },
      { typeid(int), [](const Variant& left, const Variant& right, const QLocaleEx& locale) -> Variant { return left.toInt(nullptr, locale) - right.toInt(nullptr, locale); } },
      { typeid(unsigned int), [](const Variant& left, const Variant& right, const QLocaleEx& locale) -> Variant { return left.toUInt(nullptr, locale) - right.toUInt(nullptr, locale); } }
   },
   { // LessThan
      { typeid(bool), [](const Variant& left, const Variant& right, const QLocaleEx& locale) -> Variant { return left.toBool(nullptr, locale) < right.toBool(nullptr, locale); } },
      { typeid(QText), [](const Variant& left, const Variant& right, const QLocaleEx& locale) -> Variant { return left.toText(locale) < right.toText(locale); } },
      { typeid(QString), [](const Variant& left, const Variant& right, const QLocaleEx& locale) -> Variant { return left.toString(locale) < right.toString(locale); } },
      { typeid(QDecimal), [](const Variant& left, const Variant& right, const QLocaleEx& locale) -> Variant { return left.toDecimal(nullptr, locale) < right.toDecimal(nullptr, locale); } },
      { typeid(double), [](const Variant& left, const Variant& right, const QLocaleEx& locale) -> Variant { return left.toDouble(nullptr, locale) < right.toDouble(nullptr, locale); } },
      { typeid(QValue), [](const Variant& left, const Variant& right, const QLocaleEx& locale) -> Variant { return left.to<QValue>(nullptr, locale) < right.to<QValue>(nullptr, locale); } },
      { typeid(long long), [](const Variant& left, const Variant& right, const QLocaleEx& locale) -> Variant { return left.toInt64(nullptr, locale) < right.toInt64(nullptr, locale); } },
      { typeid(unsigned long long), [](const Variant& left, const Variant& right, const QLocaleEx& locale) -> Variant { return left.toUInt64(nullptr, locale) < right.toUInt64(nullptr, locale); } },
      { typeid(int), [](const Variant& left, const Variant& right, const QLocaleEx& locale) -> Variant { return left.toInt(nullptr, locale) < right.toInt(nullptr, locale); } },
      { typeid(unsigned int), [](const Variant& left, const Variant& right, const QLocaleEx& locale) -> Variant { return left.toUInt(nullptr, locale) < right.toUInt(nullptr, locale); } }
   },
   { // GreaterThan
      { typeid(bool), [](const Variant& left, const Variant& right, const QLocaleEx& locale) -> Variant { return left.toBool(nullptr, locale) > right.toBool(nullptr, locale); } },
      { typeid(QText), [](const Variant& left, const Variant& right, const QLocaleEx& locale) -> Variant { return left.toText(locale) > right.toText(locale); } },
      { typeid(QString), [](const Variant& left, const Variant& right, const QLocaleEx& locale) -> Variant { return left.toString(locale) > right.toString(locale); } },
      { typeid(QDecimal), [](const Variant& left, const Variant& right, const QLocaleEx& locale) -> Variant { return left.toDecimal(nullptr, locale) > right.toDecimal(nullptr, locale); } },
      { typeid(double), [](const Variant& left, const Variant& right, const QLocaleEx& locale) -> Variant { return left.toDouble(nullptr, locale) > right.toDouble(nullptr, locale); } },
      { typeid(QValue), [](const Variant& left, const Variant& right, const QLocaleEx& locale) -> Variant { return left.to<QValue>(nullptr, locale) > right.to<QValue>(nullptr, locale); } },
      { typeid(long long), [](const Variant& left, const Variant& right, const QLocaleEx& locale) -> Variant { return left.toInt64(nullptr, locale) > right.toInt64(nullptr, locale); } },
      { typeid(unsigned long long), [](const Variant& left, const Variant& right, const QLocaleEx& locale) -> Variant { return left.toUInt64(nullptr, locale) > right.toUInt64(nullptr, locale); } },
      { typeid(int), [](const Variant& left, const Variant& right, const QLocaleEx& locale) -> Variant { return left.toInt(nullptr, locale) > right.toInt(nullptr, locale); } },
      { typeid(unsigned int), [](const Variant& left, const Variant& right, const QLocaleEx& locale) -> Variant { return left.toUInt(nullptr, locale) > right.toUInt(nullptr, locale); } }
   },
   { // LessOrEqual
      { typeid(bool), [](const Variant& left, const Variant& right, const QLocaleEx& locale) -> Variant { return left.toBool(nullptr, locale) <= right.toBool(nullptr, locale); } },
      { typeid(QText), [](const Variant& left, const Variant& right, const QLocaleEx& locale) -> Variant { return left.toText(locale) <= right.toText(locale); } },
      { typeid(QString), [](const Variant& left, const Variant& right, const QLocaleEx& locale) -> Variant { return left.toString(locale) <= right.toString(locale); } },
      { typeid(QDecimal), [](const Variant& left, const Variant& right, const QLocaleEx& locale) -> Variant { return left.toDecimal(nullptr, locale) <= right.toDecimal(nullptr, locale); } },
      { typeid(double), [](const Variant& left, const Variant& right, const QLocaleEx& locale) -> Variant { return left.toDouble(nullptr, locale) <= right.toDouble(nullptr, locale); } },
      { typeid(QValue), [](const Variant& left, const Variant& right, const QLocaleEx& locale) -> Variant { return left.to<QValue>(nullptr, locale) <= right.to<QValue>(nullptr, locale); } },
      { typeid(long long), [](const Variant& left, const Variant& right, const QLocaleEx& locale) -> Variant { return left.toInt64(nullptr, locale) <= right.toInt64(nullptr, locale); } },
      { typeid(unsigned long long), [](const Variant& left, const Variant& right, const QLocaleEx& locale) -> Variant { return left.toUInt64(nullptr, locale) <= right.toUInt64(nullptr, locale); } },
      { typeid(int), [](const Variant& left, const Variant& right, const QLocaleEx& locale) -> Variant { return left.toInt(nullptr, locale) <= right.toInt(nullptr, locale); } },
      { typeid(unsigned int), [](const Variant& left, const Variant& right, const QLocaleEx& locale) -> Variant { return left.toUInt(nullptr, locale) <= right.toUInt(nullptr, locale); } }
   },
   { // GreaterOrEqual
      { typeid(bool), [](const Variant& left, const Variant& right, const QLocaleEx& locale) -> Variant { return left.toBool(nullptr, locale) >= right.toBool(nullptr, locale); } },
      { typeid(QText), [](const Variant& left, const Variant& right, const QLocaleEx& locale) -> Variant { return left.toText(locale) >= right.toText(locale); } },
      { typeid(QString), [](const Variant& left, const Variant& right, const QLocaleEx& locale) -> Variant { return left.toString(locale) >= right.toString(locale); } },
      { typeid(QDecimal), [](const Variant& left, const Variant& right, const QLocaleEx& locale) -> Variant { return left.toDecimal(nullptr, locale) >= right.toDecimal(nullptr, locale); } },
      { typeid(double), [](const Variant& left, const Variant& right, const QLocaleEx& locale) -> Variant { return left.toDouble(nullptr, locale) >= right.toDouble(nullptr, locale); } },
      { typeid(QValue), [](const Variant& left, const Variant& right, const QLocaleEx& locale) -> Variant { return left.to<QValue>(nullptr, locale) >= right.to<QValue>(nullptr, locale); } },
      { typeid(long long), [](const Variant& left, const Variant& right, const QLocaleEx& locale) -> Variant { return left.toInt64(nullptr, locale) >= right.toInt64(nullptr, locale); } },
      { typeid(unsigned long long), [](const Variant& left, const Variant& right, const QLocaleEx& locale) -> Variant { return left.toUInt64(nullptr, locale) >= right.toUInt64(nullptr, locale); } },
      { typeid(int), [](const Variant& left, const Variant& right, const QLocaleEx& locale) -> Variant { return left.toInt(nullptr, locale) >= right.toInt(nullptr, locale); } },
      { typeid(unsigned int), [](const Variant& left, const Variant& right, const QLocaleEx& locale) -> Variant { return left.toUInt(nullptr, locale) >= right.toUInt(nullptr, locale); } }
   },
   { // Equality
      { typeid(bool), [](const Variant& left, const Variant& right, const QLocaleEx& locale) -> Variant { return left.toBool(nullptr, locale) == right.toBool(nullptr, locale); } },
      { typeid(QText), [](const Variant& left, const Variant& right, const QLocaleEx& locale) -> Variant { return left.toText(locale) == right.toText(locale); } },
      { typeid(QString), [](const Variant& left, const Variant& right, const QLocaleEx& locale) -> Variant { return left.toString(locale) == right.toString(locale); } },
      { typeid(QDecimal), [](const Variant& left, const Variant& right, const QLocaleEx& locale) -> Variant { return left.toDecimal(nullptr, locale) == right.toDecimal(nullptr, locale); } },
      { typeid(double), [](const Variant& left, const Variant& right, const QLocaleEx& locale) -> Variant { return left.toDouble(nullptr, locale) == right.toDouble(nullptr, locale); } },
      { typeid(QValue), [](const Variant& left, const Variant& right, const QLocaleEx& locale) -> Variant { return left.to<QValue>(nullptr, locale) == right.to<QValue>(nullptr, locale); } },
      { typeid(long long), [](const Variant& left, const Variant& right, const QLocaleEx& locale) -> Variant { return left.toInt64(nullptr, locale) == right.toInt64(nullptr, locale); } },
      { typeid(unsigned long long), [](const Variant& left, const Variant& right, const QLocaleEx& locale) -> Variant { return left.toUInt64(nullptr, locale) == right.toUInt64(nullptr, locale); } },
      { typeid(int), [](const Variant& left, const Variant& right, const QLocaleEx& locale) -> Variant { return left.toInt(nullptr, locale) == right.toInt(nullptr, locale); } },
      { typeid(unsigned int), [](const Variant& left, const Variant& right, const QLocaleEx& locale) -> Variant { return left.toUInt(nullptr, locale) == right.toUInt(nullptr, locale); } }
   },
   { // Inequality
      { typeid(bool), [](const Variant& left, const Variant& right, const QLocaleEx& locale) -> Variant { return left.toBool(nullptr, locale) != right.toBool(nullptr, locale); } },
      { typeid(QText), [](const Variant& left, const Variant& right, const QLocaleEx& locale) -> Variant { return left.toText(locale) != right.toText(locale); } },
      { typeid(QString), [](const Variant& left, const Variant& right, const QLocaleEx& locale) -> Variant { return left.toString(locale) != right.toString(locale); } },
      { typeid(QDecimal), [](const Variant& left, const Variant& right, const QLocaleEx& locale) -> Variant { return left.toDecimal(nullptr, locale) != right.toDecimal(nullptr, locale); } },
      { typeid(double), [](const Variant& left, const Variant& right, const QLocaleEx& locale) -> Variant { return left.toDouble(nullptr, locale) != right.toDouble(nullptr, locale); } },
      { typeid(QValue), [](const Variant& left, const Variant& right, const QLocaleEx& locale) -> Variant { return left.to<QValue>(nullptr, locale) != right.to<QValue>(nullptr, locale); } },
      { typeid(long long), [](const Variant& left, const Variant& right, const QLocaleEx& locale) -> Variant { return left.toInt64(nullptr, locale) != right.toInt64(nullptr, locale); } },
      { typeid(unsigned long long), [](const Variant& left, const Variant& right, const QLocaleEx& locale) -> Variant { return left.toUInt64(nullptr, locale) != right.toUInt64(nullptr, locale); } },
      { typeid(int), [](const Variant& left, const Variant& right, const QLocaleEx& locale) -> Variant { return left.toInt(nullptr, locale) != right.toInt(nullptr, locale); } },
      { typeid(unsigned int), [](const Variant& left, const Variant& right, const QLocaleEx& locale) -> Variant { return left.toUInt(nullptr, locale) != right.toUInt(nullptr, locale); } }
   },
   { }, // BitAnd
   { }, // BitXor
   { }, // BitOr
   { }, // LogAnd
   { }, // LogOr
   { }, // Conditional
   { }, // Colon
   { }, // NullCoalescing
   { }, // Repeating
   { }, // Filter
   { }, // Assignment
   { }, // Declaration
   { }, // Comma
   { }  // SemiColon
};

// Matrix of binary compatible types
// The first key is the left side of the operation and the second key the right side.
// The value of the second map is the type for the operation.
static const QMap<std::type_index, QMap<std::type_index, std::type_index>> compatibilityMatrix = {
   { typeid(int), {
      { typeid(int), typeid(int) },
      { typeid(long long), typeid(long long) },
      { typeid(double), typeid(double) },
      { typeid(QDecimal), typeid(QDecimal) }
   }},
   { typeid(unsigned int), {
      { typeid(unsigned int), typeid(unsigned int) },
      { typeid(unsigned long long), typeid(unsigned long long) },
      { typeid(double), typeid(double) },
      { typeid(QDecimal), typeid(QDecimal) }
   }},
   { typeid(long long), {
      { typeid(int), typeid(long long) },
      { typeid(long long), typeid(long long) },
      { typeid(double), typeid(double) },
      { typeid(QDecimal), typeid(QDecimal) }
   }},
   { typeid(unsigned long long), {
      { typeid(unsigned int), typeid(unsigned long long) },
      { typeid(unsigned long long), typeid(unsigned long long) },
      { typeid(double), typeid(double) },
      { typeid(QDecimal), typeid(QDecimal) }
   }},
   { typeid(double), {
      { typeid(int), typeid(double) },
      { typeid(unsigned int), typeid(double) },
      { typeid(long long), typeid(double) },
      { typeid(unsigned long long), typeid(double) },
      { typeid(double), typeid(double) },
      { typeid(QDecimal), typeid(QDecimal) }
   }},
   { typeid(QDecimal), {
      { typeid(int), typeid(QDecimal) },
      { typeid(unsigned int), typeid(QDecimal) },
      { typeid(long long), typeid(QDecimal) },
      { typeid(unsigned long long), typeid(QDecimal) },
      { typeid(double), typeid(QDecimal) },
      { typeid(QDecimal), typeid(QDecimal) },
      { typeid(QValue), typeid(QValue) }
   }},
   { typeid(QValue), {
      { typeid(QDecimal), typeid(QValue) },
      { typeid(QValue), typeid(QValue) }
   }}
};

static Variant applyBinaryByType(CppAction action, const Variant& leftValue, const Variant& rightValue, const QLocaleEx& locale)
{
   std::type_index compatibleType(typeid(void));

   if (leftValue.type() == rightValue.type())
   {
      compatibleType = leftValue.type();
   }
   else
   {
      compatibleType = compatibilityMatrix.value(leftValue.type()).value(rightValue.type(), typeid(void));
   }

   if (compatibleType == std::type_index(typeid(void)))
   {
      throw QBasicException(CppOperator::tr("Binary operation %1 not allowed for the types %2 and %3")
         .arg(CppOperator(AbstractExpression::Operator::Type::Binary, action).toString())
         .arg(plain_type_name(leftValue.type()))
         .arg(plain_type_name(rightValue.type()))
      );
   }

   const auto& operations = binary_value_operation_list[(int)action];
   const auto opFunction = operations.value(compatibleType);

   if (!opFunction)
      throw QBasicException(CppOperator::tr("Binary operation %1 for the type %2 not defined")
         .arg(CppOperator(AbstractExpression::Operator::Type::Binary, action).toString())
         .arg(plain_type_name(leftValue.type()))
      );

   return opFunction(leftValue, rightValue, locale);
}

// Apply unary operation action to value.
// No locale necessary because value is not converted.
static Variant applyUnaryByType(CppAction action, const Variant& value)
{
   // The actions PostIncr, PostDecr, PreIncr and PreDecr are already handled separately!

   const std::type_info& type = value.type();
   Variant result;

   if (type == typeid(bool))
   {
      switch (action)
      {
      case CppAction::Plus:
         break; // Invalid operation
      case CppAction::Minus:
         break; // Invalid operation
      case CppAction::LogNot:
         result = !value.toBool();
         break;
      case CppAction::BitNot:
         break; // Invalid operation
      default:;
      }
   }
   else if (type == typeid(QDecimal))
   {
      switch (action)
      {
      case CppAction::PostIncr:
         result = value.toDecimal() + 1;
         break;
      case CppAction::PostDecr:
         result = value.toDecimal() - 1;
         break;
      case CppAction::PreIncr:
         result = value.toDecimal() + 1;
         break;
      case CppAction::PreDecr:
         result = value.toDecimal() - 1;
         break;
      case CppAction::Plus:
         result = value.toDecimal();
         break;
      case CppAction::Minus:
         result = -value.toDecimal();
         break;
      case CppAction::LogNot:
         result = value.toDecimal().isZero();
         break;
      case CppAction::BitNot:
         break; // Invalid operation
      default:;
      }
   }
   else if (type == typeid(double))
   {
      switch (action)
      {
      case CppAction::PostIncr:
         result = value.toDouble() + 1.;
         break;
      case CppAction::PostDecr:
         result = value.toDouble() - 1.;
         break;
      case CppAction::PreIncr:
         result = value.toDouble() + 1.;
         break;
      case CppAction::PreDecr:
         result = value.toDouble() - 1.;
         break;
      case CppAction::Plus:
         result = value.toDouble();
         break;
      case CppAction::Minus:
         result = -value.toDouble();
         break;
      case CppAction::LogNot:
         result = value.toDouble() == .0;
         break;
      case CppAction::BitNot:
         break; // Invalid operation
      default:;
      }
   }
   else if (type == typeid(QValue))
   {
      switch (action)
      {
      case CppAction::PostIncr:
         result = QValue(value.toDecimal() + 1, value.to<QValue>().unit());
         break;
      case CppAction::PostDecr:
         result = QValue(value.toDecimal() - 1, value.to<QValue>().unit());
         break;
      case CppAction::PreIncr:
         result = QValue(value.toDecimal() + 1, value.to<QValue>().unit());
         break;
      case CppAction::PreDecr:
         result = QValue(value.toDecimal() - 1, value.to<QValue>().unit());
         break;
      case CppAction::Plus:
         result = value.to<QValue>();
         break;
      case CppAction::Minus:
         result = QValue(-value.toDecimal(), value.to<QValue>().unit());
         break;
      case CppAction::LogNot:
         result = value.toDecimal().isZero();
         break;
      case CppAction::BitNot:
         break; // Invalid operation
      default:;
      }
   }
   else if (type == typeid(long long))
   {
      switch (action)
      {
      case CppAction::PostIncr:
         result = value.toInt64() + 1ll;
         break;
      case CppAction::PostDecr:
         result = value.toInt64() - 1ll;
         break;
      case CppAction::PreIncr:
         result = value.toInt64() + 1ll;
         break;
      case CppAction::PreDecr:
         result = value.toInt64() - 1ll;
         break;
      case CppAction::Plus:
         result = value.toInt64();
         break;
      case CppAction::Minus:
         result = -value.toInt64();
         break;
      case CppAction::LogNot:
         result = value.toInt64() == 0LL;
         break;
      case CppAction::BitNot:
         result = ~value.toInt64();
         break;
      default:;
      }
   }
   else if (type == typeid(unsigned long long))
   {
      switch (action)
      {
      case CppAction::PostIncr:
         result = value.toUInt64() + 1ull;
         break;
      case CppAction::PostDecr:
         result = value.toUInt64() - 1ull;
         break;
      case CppAction::PreIncr:
         result = value.toUInt64() + 1ull;
         break;
      case CppAction::PreDecr:
         result = value.toUInt64() - 1ull;
         break;
      case CppAction::Plus:
         result = value.toUInt64();
         break;
      case CppAction::Minus:
         break; // Not allowed
      case CppAction::LogNot:
         result = value.toUInt64() == 0ULL;
         break;
      case CppAction::BitNot:
         result = ~value.toUInt64();
         break;
      default:;
      }
   }
   else if (type == typeid(int))
   {
      switch (action)
      {
      case CppAction::PostIncr:
         result = value.toInt() + 1;
         break;
      case CppAction::PostDecr:
         result = value.toInt() - 1;
         break;
      case CppAction::PreIncr:
         result = value.toInt() + 1;
         break;
      case CppAction::PreDecr:
         result = value.toInt() - 1;
         break;
      case CppAction::Plus:
         result = value.toInt();
         break;
      case CppAction::Minus:
         result = -value.toInt();
         break;
      case CppAction::LogNot:
         result = value.toInt() == 0;
         break;
      case CppAction::BitNot:
         result = ~value.toInt();
         break;
      default:;
      }
   }
   else if (type == typeid(unsigned int))
   {
      switch (action)
      {
      case CppAction::PostIncr:
         result = value.toUInt() + 1u;
         break;
      case CppAction::PostDecr:
         result = value.toUInt() - 1u;
         break;
      case CppAction::PreIncr:
         result = value.toUInt() + 1u;
         break;
      case CppAction::PreDecr:
         result = value.toUInt() - 1u;
         break;
      case CppAction::Plus:
         result = value.toUInt();
         break;
      case CppAction::Minus:
         break; // Not allowed
      case CppAction::LogNot:
         result = value.toUInt() == 0u;
         break;
      case CppAction::BitNot:
         result = ~value.toUInt();
         break;
      default:;
      }
   }
   else
   {
      throw QBasicException(CppOperator::tr("Unary operation %1 for the type %2 not defined")
         .arg(CppOperator(AbstractExpression::Operator::Type::Binary, action).toString())
         .arg(plain_type_name(value.type()))
      );
   }

   if (result.isNull())
   {
      throw QBasicException(CppOperator::tr("Unary operation %1 not allowed for the type %2")
         .arg(CppOperator(AbstractExpression::Operator::Type::Binary, action).toString())
         .arg(plain_type_name(value.type()))
      );
   }

   return result;
}

static VarPtr doBinaryOperation(CppAction action, const QList<ExpressionPtr>& operands, const IdentifierResolver& identifierResolver)
{
   Q_ASSERT(operands.count() > 1);

   auto leftItem = operands[0]->resolve(identifierResolver).first;

   if (leftItem->basicType() == IVariable::BasicType::Collection)
   {
      for (auto i = 1; i < operands.count(); ++i)
      {
         const auto rightItem = operands[i]->resolve(identifierResolver).first;

         if (action == CppAction::Addition)
         {
            auto itemList = leftItem->items();
            auto itemNames = leftItem->itemNames();

            if (rightItem->basicType() == IVariable::BasicType::Collection)
            {
               if (itemNames.count() == itemList.count())
               {
                  itemNames << rightItem->itemNames();
               }
               itemList << rightItem->items();
            }
            else
            {
               itemList << rightItem;
            }

            leftItem = identifierResolver.newCollection();
            leftItem->setItems(itemNames, itemList);
         }
         else if (action == CppAction::BitAnd)
         {
            // TODO: Merge names

            QList<VarPtr> itemList;
            const auto itemCount = qMin(leftItem->itemCount(), rightItem->itemCount());

            itemList.reserve(itemCount);

            for (auto j = 0; j < itemCount; ++j)
            {
               auto pair = identifierResolver.newCollection();

               pair->setItem(0, leftItem->getItem(j));
               pair->setItem(1, rightItem->getItem(j));

               itemList.append(pair);
            }

            leftItem = identifierResolver.newCollection();
            leftItem->setItems(QStringList(), itemList);
         }
         else if (action == CppAction::BitOr)
         {
            // TODO: Merge names

            QList<VarPtr> itemList;
            const auto itemCount = qMax(leftItem->itemCount(), rightItem->itemCount());

            itemList.reserve(itemCount);

            for (auto j = 0; j < itemCount; ++j)
            {
               auto pair = identifierResolver.newCollection();

               if (j < leftItem->itemCount())
                  pair->setItem(pair->itemCount(), leftItem->getItem(j));
               if (j < rightItem->itemCount())
                  pair->setItem(pair->itemCount(), rightItem->getItem(j));

               itemList.append(pair);
            }

            leftItem = identifierResolver.newCollection();
            leftItem->setItems(QStringList(), itemList);
         }
      }

      return leftItem;
   }

   auto leftValue = leftItem->value();

   for (auto i = 1; i < operands.count(); ++i)
   {
      // Check for logical operators so that only necessary values are resolved
      if (action == CppAction::LogAnd)
      {
         if (leftValue.toBool())
            leftValue = operands[i]->resolve(identifierResolver).first->value().toBool();
      }
      else if (action == CppAction::LogOr)
      {
         if (!leftValue.toBool())
            leftValue = operands[i]->resolve(identifierResolver).first->value().toBool();
      }
      else
      {
         // Resolve other binary operators

         auto rightValue = operands[i]->resolve(identifierResolver).first->value();

         leftValue = applyBinaryByType(action, leftValue, rightValue, identifierResolver.locale());
      }
   }

   return identifierResolver.newValue(leftValue);
}

void CppOperator::fixTypeNotPrefix()
{
   switch (_action)
   {
   case CppAction::Plus:
      _type = Type::Binary;
      _action = CppAction::Addition;
      break;
   case CppAction::Minus:
      _type = Type::Binary;
      _action = CppAction::Subtraction;
      break;
   case CppAction::PreIncr:
      _type = Type::Postfix;
      _action = CppAction::PostIncr;
      break;
   case CppAction::PreDecr:
      _type = Type::Postfix;
      _action = CppAction::PostDecr;
      break;
   }
}

bool CppOperator::canBePostfix()
{
   return _action == CppAction::Function || _action == CppAction::SemiColon;
}

bool CppOperator::isLeftToRight() const
{
   return _action > CppAction::Invalid && _action < CppAction::Block
      || _action > CppAction::BitNot && _action < CppAction::Conditional
      || _action > CppAction::Declaration;
}

int CppOperator::priority() const
{
   int result = 0;

   switch (_action)
   {
   case CppAction::Invalid:
      result = -1;
      break;
      // Left-to-right
   case CppAction::PostIncr:
   case CppAction::PostDecr:
   case CppAction::Function:
   case CppAction::Subscript:
   case CppAction::NullConditionalSubscript:
   case CppAction::Member:
   case CppAction::NullConditionalMember:
      result++;
      // Right-to-left
   case CppAction::Block:
   case CppAction::PreIncr:
   case CppAction::PreDecr:
   case CppAction::Plus:
   case CppAction::Minus:
   case CppAction::LogNot:
   case CppAction::BitNot:
      result++;
      // Left-to-right
   case CppAction::Multiplication:
   case CppAction::Division:
   case CppAction::Modulus:
      result++;
   case CppAction::Addition:
   case CppAction::Subtraction:
      result++;
   case CppAction::LessThan:
   case CppAction::GreaterThan:
   case CppAction::LessOrEqual:
   case CppAction::GreaterOrEqual:
      result++;
   case CppAction::Equality:
   case CppAction::Inequality:
      result++;
   case CppAction::BitAnd:
      result++;
   case CppAction::BitXor:
      result++;
   case CppAction::BitOr:
      result++;
   case CppAction::LogAnd:
      result++;
   case CppAction::LogOr:
      result++;
      // Right-to-left
   case CppAction::Conditional:
   case CppAction::Colon:
   case CppAction::NullCoalescing:
   case CppAction::Repeating:
   case CppAction::Filter:
   case CppAction::Assignment:
   case CppAction::Declaration:
      result++;
      // Left-to-right
   case CppAction::Comma:
      result++;
   case CppAction::SemiColon:
      break;
   }

   return result;
}

bool CppOperator::isAssociative() const
{
   // Actions are never associative because they can have side effects!
   // The +operator for example returns a string if one of its operands is a string. Therefore ("Text " + 1 + 1) is not the same as ("Text " + (1 + 1))!
   return false;
}

QString CppOperator::toString() const
{
   return actionNames[(int)_action];
}

QPair<VarPtr, VarPtr> CppOperator::resolve(const IdentifierResolver& identifierResolver, const QList<ExpressionPtr>& operands) const
{
   static const QString msgInvalidNumberOfOperands = tr("Invalid number of Operands for %1 operator: %2");

   if (!identifierResolver.isValid())
      throw QBasicException(tr("Invalid identifier resolver"));

   QPair<VarPtr, VarPtr> var; // Result

   switch (_action)
   {
   case CppAction::Invalid:
      break;
   case CppAction::PostIncr:
   case CppAction::PostDecr:
      {
         if (operands.count() != 1)
            throw QBasicException(msgInvalidNumberOfOperands.arg("unary post " + toString()).arg(operands.count()));
         auto lhs = operands[0]->resolve(identifierResolver).first;
         var.first = identifierResolver.newValue(lhs->value());
         lhs->setValue(applyUnaryByType(_action, lhs->value()));
      }
      break;
   case CppAction::Function:
      {
         if (operands.isEmpty())
            throw QBasicException(msgInvalidNumberOfOperands.arg("()").arg(operands.count()));

         const auto fVar = operands[0]->resolve(identifierResolver);
         ExpressionPtr funcItem;

         if (fVar.second)
            funcItem = fVar.second->function();
         else if (fVar.first)
            funcItem = fVar.first->function();

         if (!funcItem)
            throw QBasicException(tr("The operand is not a function"));

         QStringList names;
         QList<VarPtr> items;

         // If its a member function set the item on the left side as first parameter
         if (fVar.second && fVar.first)
         {
            names.append("Subject");
            items.append(fVar.first);
         }

         if (operands.count() > 1)
         {
            auto resPars = operands[1]->resolve(identifierResolver).first;

            if (resPars->basicType() != IVariable::BasicType::Collection)
            {
               names.append("Par0");
               items.append(resPars);
            }
            else if (resPars->getName(0).isEmpty())
            {
               for (auto i = 0; i < resPars->itemCount(); ++i)
               {
                  names.append("Par" + QString::number(i));
                  items.append(resPars->getItem(i));
               }
            }
            else if (items.isEmpty())
            {
               items = resPars->items();
               names = resPars->itemNames();
            }
            else
            {
               items.append(resPars->items());
               names.append(resPars->itemNames());
            }
         }

         auto parList = identifierResolver.newCollection();

         parList->setItems(names, items);

         var = funcItem->resolve(IdentifierResolver(parList, identifierResolver.newCollection(), identifierResolver.functionMap(), identifierResolver.locale()));
      }
      break;
   case CppAction::Subscript:
      {
         if (operands.count() != 2)
            throw QBasicException(msgInvalidNumberOfOperands.arg("[]").arg(operands.count()));
         auto index = operands[1]->resolve(identifierResolver).first->value().toInt();
         auto list = operands[0]->resolve(identifierResolver).first;
         if (index < 0 || index >= list->itemCount())
            throw QBasicException(tr("Index %1 out of range").arg(index));
         var.first = list->getItem(index);
      }
      break;
   case CppAction::NullConditionalSubscript:
      {
         if (operands.count() != 2)
            throw QBasicException(msgInvalidNumberOfOperands.arg("?[]").arg(operands.count()));
         IdentifierResolver conditionalResolver(identifierResolver);
         conditionalResolver.setValue(VarPtr()); // Allow null
         auto left = operands[0]->resolve(conditionalResolver).first;
         if (left)
            var.first = left->getItem(operands[1]->resolve(identifierResolver).first->value().toInt());
      }
      break;
   case CppAction::Member:
   case CppAction::NullConditionalMember:
      if (operands.count() < 2)
         throw QBasicException(msgInvalidNumberOfOperands.arg(".").arg(operands.count()));
      for (auto i = 0; i < operands.count(); ++i)
      {
         if (var.second)
         {
            // Switch to function resolving
            var.second = operands.at(i)->resolve(IdentifierResolver(var.second, VarPtr(), ResolverFunctionMap(), identifierResolver.locale())).first;
         }
         else
         {
            if (var.first)
            {
               IdentifierResolver memberResolver(var.first, VarPtr(), identifierResolver.functionMap(), identifierResolver.locale());

               if (identifierResolver.canModify())
               {
                  if (i + 1 < operands.count())
                     memberResolver.setValue(identifierResolver.newCollection());
                  else
                     memberResolver.setValue(identifierResolver.value());
               }

               if (_action == CppAction::NullConditionalMember)
                  memberResolver.setValue(VarPtr()); // Allow null

               var = operands.at(i)->resolve(memberResolver);
            }
            else
            {
               if (_action == CppAction::NullConditionalMember)
               {
                  IdentifierResolver nullResolver(identifierResolver);
                  nullResolver.setValue(VarPtr()); // Allow null
                  var = operands.at(i)->resolve(nullResolver);
               }
               else
               {
                  var = operands.at(i)->resolve(identifierResolver);
               }
            }
         }

         if (var.first.isNull() && var.second.isNull())
            break;
      }
      break;
   case CppAction::Block:
      if (operands.count() != 1)
         throw QBasicException(msgInvalidNumberOfOperands.arg("{}").arg(operands.count()));
      var.first = identifierResolver.newFunction(operands[0]);
      break;
   case CppAction::PreIncr:
   case CppAction::PreDecr:
      if (operands.count() != 1)
         throw QBasicException(msgInvalidNumberOfOperands.arg("unary " + toString()).arg(operands.count()));
      var = operands[0]->resolve(identifierResolver);
      var.first->setValue(applyUnaryByType(_action, var.first->value()));
      break;
   case CppAction::Plus:
   case CppAction::Minus:
   case CppAction::LogNot:
   case CppAction::BitNot:
      if (operands.count() != 1)
         throw QBasicException(msgInvalidNumberOfOperands.arg("unary " + toString()).arg(operands.count()));
      var.first = identifierResolver.newValue(applyUnaryByType(_action, operands[0]->resolve(identifierResolver).first->value()));
      break;
   case CppAction::Multiplication:
   case CppAction::Division:
   case CppAction::Modulus:
   case CppAction::Addition:
   case CppAction::Subtraction:
   case CppAction::LessThan:
   case CppAction::GreaterThan:
   case CppAction::LessOrEqual:
   case CppAction::GreaterOrEqual:
   case CppAction::Equality:
   case CppAction::Inequality:
   case CppAction::BitAnd:
   case CppAction::BitXor:
   case CppAction::BitOr:
   case CppAction::LogAnd:
   case CppAction::LogOr:
      if (operands.count() < 2)
         throw QBasicException(msgInvalidNumberOfOperands.arg(toString()).arg(operands.count()));
      var.first = doBinaryOperation(_action, operands, identifierResolver);
      break;
   case CppAction::Conditional:
      {
         if (operands.count() != 2)
            throw QBasicException(msgInvalidNumberOfOperands.arg("?").arg(operands.count()));
         auto colonOperator = operands[1].as<AbstractOperatorExpression>();
         if (!colonOperator || !(colonOperator->op() == CppOperator(Type::Binary, CppAction::Colon)))
            throw QBasicException(tr("Operator on the right hand side of ? is not :"));
         // The colon operator is accessed directly so that only the true or false part are evaluated
         var = operands[0]->resolve(identifierResolver).first->value().toBool() ? colonOperator->operandAt(0)->resolve(identifierResolver) : colonOperator->operandAt(1)->resolve(identifierResolver);
      }
      break;
   case CppAction::Colon:
      // The Colon operator is evaluated with the Conditional operator
      throw QBasicException(tr("Invalid : operator"));
   case CppAction::NullCoalescing:
      {
         if (operands.count() != 2)
            throw QBasicException(msgInvalidNumberOfOperands.arg("??").arg(operands.count()));
         IdentifierResolver nullResolver(identifierResolver);
         nullResolver.setValue(VarPtr()); // Allow null
         var = operands[0]->resolve(nullResolver);
         if (var.first.isNull() && var.second.isNull())
            var = operands[1]->resolve(identifierResolver);
      }
      break;
   case CppAction::Repeating:
      {
         if (operands.count() != 2)
            throw QBasicException(msgInvalidNumberOfOperands.arg("#").arg(operands.count()));
         var.first = identifierResolver.newCollection();
         auto leftSide = operands[0]->resolve(identifierResolver).first;
         if (leftSide->basicType() == IVariable::BasicType::Collection)
         {
            IdentifierResolver thisResolver(identifierResolver);
            for (auto i = 0; i < leftSide->itemCount(); ++i)
            {
               thisResolver.setThis(leftSide->getItem(i));
               var.first->setItem(var.first->itemCount(), operands[1]->resolve(thisResolver).first);
            }
         }
         else
         {
            auto loopCount = 0; // This is a security measure to avoid endless loops

            while (leftSide->value().toBool())
            {
               if (loopCount > 0xFFFF)
                  throw QBasicException(tr("Iteration limit of 65535 reached (endless loop?)"));

               var.first->setItem(var.first->itemCount(), operands[1]->resolve(identifierResolver).first);
               leftSide = operands[0]->resolve(identifierResolver).first;

               loopCount++;
            }
         }
      }
      break;
   case CppAction::Filter:
      {
         if (operands.count() != 2)
            throw QBasicException(msgInvalidNumberOfOperands.arg("#?").arg(operands.count()));
         var.first = identifierResolver.newCollection();
         auto leftSide = operands[0]->resolve(identifierResolver).first;
         IdentifierResolver thisResolver(identifierResolver);
         for (auto i = 0; i < leftSide->itemCount(); ++i)
         {
            thisResolver.setThis(leftSide->getItem(i));
            if (operands[1]->resolve(thisResolver).first->value().toBool())
               var.first->setItem(var.first->itemCount(), leftSide->getItem(i));
         }
      }
      break;
   case CppAction::Assignment:
      {
         if (operands.count() != 2)
            throw QBasicException(msgInvalidNumberOfOperands.arg("=").arg(operands.count()));
         const auto rhs = operands[1]->resolve(identifierResolver);
         var = operands[0]->resolve(identifierResolver);
         var.first->assignFrom(rhs.first);
      }
      break;
   case CppAction::Declaration:
      {
         if (operands.count() != 2)
            throw QBasicException(msgInvalidNumberOfOperands.arg(":=").arg(operands.count()));
         IdentifierResolver declarationResolver(identifierResolver);
         declarationResolver.setValue(operands[1]->resolve(identifierResolver).first);
         var = operands.at(0)->resolve(declarationResolver);
      }
      break;
   case CppAction::Comma:
      var.first = identifierResolver.newCollection();
      for (int i = 0; i < operands.count(); ++i)
      {
         var.first->setItem(i, operands.at(i)->resolve(identifierResolver).first);
      }
      break;
   case CppAction::SemiColon:
      for (int i = 0; i < operands.count(); ++i)
      {
         var = operands.at(i)->resolve(identifierResolver);
      }
      break;
   }

   return var;
}

void CppOperator::setBinaryValueOperation(CppAction action, const std::type_info& type, Variant(* operation)(const Variant&, const Variant&, const QLocaleEx&))
{
   binary_value_operation_list[(int)action][type] = operation;
}

CppTokenOperator::CppTokenOperator(const bp<CppOperatorToken>& opToken)
   : CppOperator((Type)(int)cppOperators[opToken->opIndex()].type, cppOperators[opToken->opIndex()].action),
   _token(opToken)
{
}

QPair<VarPtr, VarPtr> CppTokenValue::resolve(const IdentifierResolver& identifierResolver) const
{
   if (!identifierResolver.isValid())
      throw QBasicException(tr("Invalid identifier resolver"));

   if (!_token.is<Token::Identifier>())
   {
      return QPair<VarPtr, VarPtr>(identifierResolver.newValue(_token.value()), VarPtr());
   }

   return identifierResolver.resolve(_token.as<Token::Identifier>()->identifier());
}

ExpressionPtr createCppExpressionTree(const QString& expression)
{
   return createCppExpressionTree(Tokenizer(expression, createCppContext()).tokenList());
}

ExpressionPtr createVersatileExpressionTree(const QString& expression)
{
   return createCppExpressionTree(Tokenizer(expression, createVersatileExpressionContext()).tokenList());
}

ExpressionPtr createCppExpressionTree(const QList<Token>& tokenList)
{
   typedef GenericOperatorExpression<CppTokenOperator> CppOperatorExpression;
   typedef GenericValueExpression<CppTokenValue> CppValueExpression;

   // Feed the expression parser

   ExpressionParser parser;

   for (auto&& token : tokenList)
   {
      auto op = token.as<CppOperatorToken>();

      if (op)
      {
         if (strcmp(op->op(), "(") == 0)
         {
            if (!parser.lastRightSideOperand().isNull()) // Can only be a function if the last operand is a function identifier
            {
               // Insert a function operator so that the the right side of the new operator is the expression in parenthesis.
               parser.addOperator(new CppOperatorExpression(CppTokenOperator(op)));
            }

            parser.beginEnclosure();
         }
         else if (strcmp(op->op(), ")") == 0)
         {
            try
            {
               parser.endEnclosure();
            }
            catch (const QBasicException& exception)
            {
               if (typeid(exception) == typeid(QBasicException))
                  throw TokenException(token, exception.message());
               throw;
            }
         }
         else if (strcmp(op->op(), "[") == 0)
         {
            parser.addOperator(new CppOperatorExpression(CppTokenOperator(op)));
            parser.beginEnclosure();
         }
         else if (strcmp(op->op(), "?[") == 0)
         {
            parser.addOperator(new CppOperatorExpression(CppTokenOperator(op)));
            parser.beginEnclosure();
         }
         else if (strcmp(op->op(), "]") == 0)
         {
            try
            {
               parser.endEnclosure();
            }
            catch (const QBasicException& exception)
            {
               if (typeid(exception) == typeid(QBasicException))
                  throw TokenException(token, exception.message());
               throw;
            }
         }
         else if (strcmp(op->op(), "{") == 0)
         {
            parser.addOperator(new CppOperatorExpression(CppTokenOperator(op)));
            parser.beginEnclosure();
         }
         else if (strcmp(op->op(), "}") == 0)
         {
            try
            {
               parser.endEnclosure();
            }
            catch (const QBasicException& exception)
            {
               if (typeid(exception) == typeid(QBasicException))
                  throw TokenException(token, exception.message());
               throw;
            }
         }
         else
         {
            parser.addOperator(new CppOperatorExpression(CppTokenOperator(op)));
         }
      }
      else if (!token.is<Token::WhiteSpace>() && !token.is<Token::Comment>())
      {
         if (!parser.lastRightSideOperand().isNull())
         {
            // We are about to add a value (identifier) after another value or identifier!
            // A function with a right side without parenthesis is not allowed
            throw TokenException(token, ExpressionParser::tr("Two successive identifiers without operator"));
         }

         parser.addValue(new CppValueExpression(token));
      }
   }

   return parser.expressionTree();
}

void toIdentifierList(const ExpressionPtr& expr, QStringList& identifierList)
{
   if (!expr)
      return;

   const auto op = expr.as<AbstractOperatorExpression>();

   if (op)
   {
      for (auto&& operand : op->operands())
      {
         toIdentifierList(operand, identifierList);
      }
   }
   else
   {
      const auto valueExpression = expr.as<AbstractValueExpression>();

      if (valueExpression)
      {
         const auto identifierToken = dynamic_cast<const CppTokenValue&>(valueExpression->value()).token().as<Token::Identifier>();
         if (identifierToken)
            identifierList.append(identifierToken->identifier());
      }
   }
}
