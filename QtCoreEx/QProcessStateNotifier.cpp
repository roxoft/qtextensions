#include "stdafx.h"
#include "QProcessStateNotifier.h"
#include <QtGlobal>
#include <QString>
#if defined(Q_OS_WIN) && defined(QT_BUILD_CORE_LIB)
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#endif

#if QT_VERSION < QT_VERSION_CHECK(5,0,0)
#define Q_DECL_NOEXCEPT
#endif

static void qt_emergency_out(const char *msg, va_list ap) Q_DECL_NOEXCEPT
{
   char emergency_buf[256] = { '\0' };

   emergency_buf[sizeof emergency_buf - 1] = '\0';

   if (msg)
      vsnprintf(emergency_buf, sizeof emergency_buf - 1, msg, ap);

#if defined(Q_OS_WIN) && defined(QT_BUILD_CORE_LIB)
# if defined(Q_OS_WINCE) || defined(Q_OS_WINRT)
   OutputDebugStringA(emergency_buf);
# else
   if (qWinLogToStderr()) {
      fprintf(stderr, "%s\n", emergency_buf);
      fflush(stderr);
   } else {
      OutputDebugStringA(emergency_buf);
   }
# endif
#else
   fprintf(stderr, "%s\n", emergency_buf);
   fflush(stderr);
#endif
}

static void qt_message(QtMsgType msgType, const char *msg, va_list ap, QString &buf)
{
#if !defined(QT_NO_EXCEPTIONS)
   if (std::uncaught_exceptions()) {
      qt_emergency_out(msg, ap);
      return;
   }
#endif
   if (msg) {
      QT_TRY {
#if QT_VERSION < QT_VERSION_CHECK(5,5,0)
         buf = QString().vsprintf(msg, ap);
#else
         buf = QString::vasprintf(msg, ap);
#endif
      } QT_CATCH(const std::bad_alloc &) {
#if !defined(QT_NO_EXCEPTIONS)
         qt_emergency_out(msg, ap);
         // don't rethrow - we use qWarning and friends in destructors.
         return;
#endif
      }
   }
   //buf += QLatin1String(" (") + qt_error_string(code) + QLatin1Char(')');
#if QT_VERSION < QT_VERSION_CHECK(5,0,0)
   qt_message_output(msgType, buf.toLatin1().data());
#else
   QMessageLogContext context;
   qt_message_output(msgType, context, buf);
#endif
}

QProcessStateNotifier::QProcessStateNotifier(int line, const char *file, const char *date, const char *function)
   : _line(line), _file(file), _date(date), _function(function)
{
}

void QProcessStateNotifier::info(const char *msg, ...)
{
   QString message;

   va_list ap;
   va_start(ap, msg); // use variable arg list
   qt_message(QtDebugMsg, msg, ap, message);
   va_end(ap);
}

void QProcessStateNotifier::warning(const char *msg, ...)
{
   QString message;

   va_list ap;
   va_start(ap, msg); // use variable arg list
   qt_message(QtWarningMsg, msg, ap, message);
   va_end(ap);
}

void QProcessStateNotifier::error(const char *msg, ...)
{
   QString message;

   va_list ap;
   va_start(ap, msg); // use variable arg list
   qt_message(QtCriticalMsg, msg, ap, message);
   va_end(ap);

#if defined(Q_CC_MSVC) && defined(QT_DEBUG) && defined(_DEBUG) && defined(_CRT_ERROR)
   QString file = QString::fromUtf8(_file);

   // get the current report mode
   int reportMode = _CrtSetReportMode(_CRT_ERROR, _CRTDBG_MODE_WNDW);

   _CrtSetReportMode(_CRT_ERROR, reportMode);

   int ret = _CrtDbgReportW(_CRT_ERROR, reinterpret_cast<const wchar_t *>(file.utf16()), _line, _CRT_WIDE(QT_VERSION_STR), reinterpret_cast<const wchar_t *>(message.utf16()));
   if ((ret == 0) && (reportMode & _CRTDBG_MODE_WNDW))
      return; // ignore
   else if (ret == 1)
      _CrtDbgBreak();
#else
    Q_UNUSED(message);
#endif
}

void QProcessStateNotifier::fatal(const char *msg, ...)
{
   QString message;

   va_list ap;
   va_start(ap, msg); // use variable arg list
   qt_message(QtCriticalMsg, msg, ap, message);
   va_end(ap);

   abort();
}
