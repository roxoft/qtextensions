#include "stdafx.h"
#include "SerStream.h"
#include <QBuffer>
#include "TemplateConversions.h"
#include "basic_exceptions.h"

static const char* qtGlobalColorNames[] = {
   "0 pixel value (for bitmaps)",
   "1 pixel value (for bitmaps)",
   "Black (#000000)",
   "White (#ffffff)",
   "Dark gray (#808080)",
   "Gray (#a0a0a4)",
   "Light gray (#c0c0c0)",
   "Red (#ff0000)",
   "Green (#00ff00)",
   "Blue (#0000ff)",
   "Cyan (#00ffff)",
   "Magenta (#ff00ff)",
   "Yellow (#ffff00)",
   "Dark red (#800000)",
   "Dark green (#008000)",
   "Dark blue (#000080)",
   "Dark cyan (#008080)",
   "Dark magenta (#800080)",
   "Dark yellow (#808000)",
   "Transparent black"
};

QTCOREEX_EXPORT const char *qGlobalColorName(Qt::GlobalColor color)
{
   return qtGlobalColorNames[(int)color];
}

QTCOREEX_EXPORT Qt::GlobalColor qGlobalColorFromName(const char* name)
{
   if (name)
   {
      for (size_t i = 0; i < sizeof(qtGlobalColorNames) / sizeof(*qtGlobalColorNames); ++i)
      {
         if (strcmp(qtGlobalColorNames[i], name) == 0)
            return (Qt::GlobalColor)i;
      }
   }

   return Qt::color0;
}

//
// class SerInStream
//

class SerInStreamQIODevice : public SerStream::InDevice
{
public:
   SerInStreamQIODevice(QPair<QIODevice*, int>* device) : _device(device) { _device->second++; }
   ~SerInStreamQIODevice() override
   {
      _device->second--;
      if ((_device->second & 0xFFFF) == 0)
      {
         if (_device->second)
            delete _device->first;
         delete _device;
      }
   }

   QIODevice* device() { return _device->first; }

   bool atEnd() const override;
   bool hasData() const override;
   qint64 pos() const override;
   void reset() override;

   bool read(char *buffer, qint64 size, bool peek) override;

private:
   QPair<QIODevice*, int>* _device = nullptr;
};

bool SerInStreamQIODevice::atEnd() const
{
   return _device->first && _device->first->atEnd();
}

bool SerInStreamQIODevice::hasData() const
{
   return _device->first && _device->first->bytesAvailable() > 0LL;
}

qint64 SerInStreamQIODevice::pos() const
{
   return _device->first ? _device->first->pos() : 0LL;
}

void SerInStreamQIODevice::reset()
{
   if (_device->first)
      _device->first->reset();
}

bool SerInStreamQIODevice::read(char *buffer, qint64 size, bool peek)
{
   if (_device->first == nullptr)
   {
      _lastError = "No device";
      return false;
   }

   for (;;)
   {
      qint64 bytesRead;

      if (peek)
         bytesRead = _device->first->peek(buffer, size);
      else
         bytesRead = _device->first->read(buffer, size);

      if (bytesRead < 0LL)
      {
         _lastError = _device->first->errorString();
         break;
      }

      size -= bytesRead;
      buffer += bytesRead;

      if (size == 0LL)
         return true;

      // Not enough or no data read (may be an End of Stream)

      // Check if there can be more data (the default implementation returns false)
      if (!_device->first->waitForReadyRead(_timeout)) // TODO: This function may fail randomly on Windows!
      {
         // This may be a normal end of stream
         if (!_device->first->atEnd())
            _lastError = _device->first->errorString();
         //else if (!peek)
         //   setError(__FUNCTION__, "Not enough data");
         break;
      }
   }

   return false;
}

class SerOutStreamQIODevice : public SerStream::OutDevice
{
public:
   SerOutStreamQIODevice(QPair<QIODevice*, int>* device) : _device(device) { _device->second++; }
   ~SerOutStreamQIODevice() override
   {
      _device->second--;
      if ((_device->second & 0xFFFF) == 0)
      {
         if (_device->second)
            delete _device->first;
         delete _device;
      }
   }

   QIODevice* device() { return _device->first; }

   bool write(const char *buffer, qint64 size) override;

   void flush() override;

private:
   QPair<QIODevice*, int>* _device = nullptr;
};

bool SerOutStreamQIODevice::write(const char *buffer, qint64 size)
{
   if (_device->first == nullptr)
   {
      _lastError = "No device";
      return false;
   }

   for (;;)
   {
      qint64 bytesWritten = _device->first->write(buffer, size);

      if (bytesWritten < 0LL)
      {
         _lastError = _device->first->errorString();
         break;
      }

      size -= bytesWritten;
      buffer += bytesWritten;

      if (size == 0LL)
         return true;

      if (!_device->first->waitForBytesWritten(_timeout)) // TODO: This function may fail randomly on Windows!
      {
         _lastError = _device->first->errorString();
         break;
      }
   }

   return false;
}

void SerOutStreamQIODevice::flush()
{
   if (_device->first && _device->first->bytesToWrite() && !_device->first->waitForBytesWritten(_timeout))
      _lastError = _device->first->errorString();
}

class SerInStreamQString : public SerStream::InDevice
{
public:
   SerInStreamQString(const QString& data) : _data(data) {}
   ~SerInStreamQString() override = default;

   bool atEnd() const override;
   bool hasData() const override;
   qint64 pos() const override;
   void reset() override;

   bool read(char *buffer, qint64 size, bool peek) override;

private:
   QString _data;
   int _pos = 0;
};

bool SerInStreamQString::atEnd() const
{
   return _pos >= _data.length();
}

bool SerInStreamQString::hasData() const
{
   return _pos < _data.length();
}

qint64 SerInStreamQString::pos() const
{
   return _pos;
}

void SerInStreamQString::reset()
{
   _pos = 0;
}

bool SerInStreamQString::read(char *buffer, qint64 size, bool peek)
{
   if (buffer == nullptr)
   {
      _lastError = "Invalid buffer";
      return false;
   }

   auto pos = _pos;

   while (pos < _data.length() && size > 0LL)
   {
      *buffer = _data.at(pos).toLatin1();
      pos++;
      size--;
      buffer++;
   }

   if (!peek)
      _pos = pos;

   return size == 0LL;
}

class SerOutStreamQString : public SerStream::OutDevice
{
public:
   SerOutStreamQString(QString* data, bool own) : _data(data), _own(own) {}
   ~SerOutStreamQString() override { if (_own) delete _data; }

   bool write(const char *buffer, qint64 size) override;

   void flush() override;

private:
   QString* _data = nullptr;
   bool _own = false;
};

bool SerOutStreamQString::write(const char *buffer, qint64 size)
{
   if (_data == nullptr)
      return true;

   if (buffer == nullptr)
   {
      _lastError = "Invalid buffer";
      return false;
   }

   _data->append(QLatin1String(buffer, (int)size));

   return true;
}

void SerOutStreamQString::flush()
{
}

class SerInStreamQByteArray : public SerStream::InDevice
{
public:
   SerInStreamQByteArray(const QByteArray& data) : _data(data) {}
   ~SerInStreamQByteArray() override = default;

   bool atEnd() const override;
   bool hasData() const override;
   qint64 pos() const override;
   void reset() override;

   bool read(char *buffer, qint64 size, bool peek) override;

private:
   QByteArray _data;
   int _pos = 0;
};

bool SerInStreamQByteArray::atEnd() const
{
   return _pos >= _data.length();
}

bool SerInStreamQByteArray::hasData() const
{
   return _pos < _data.length();
}

qint64 SerInStreamQByteArray::pos() const
{
   return _pos;
}

void SerInStreamQByteArray::reset()
{
   _pos = 0;
}

bool SerInStreamQByteArray::read(char *buffer, qint64 size, bool peek)
{
   if (buffer == nullptr)
   {
      _lastError = "Invalid buffer";
      return false;
   }

   auto pos = _pos;

   while (pos < _data.length() && size > 0LL)
   {
      *buffer = _data.at(pos);
      pos++;
      size--;
      buffer++;
   }

   if (!peek)
      _pos = pos;

   return size == 0LL;
}

class SerOutStreamQByteArray : public SerStream::OutDevice
{
public:
   SerOutStreamQByteArray(QByteArray* data, bool own) : _data(data), _own(own) {}
   ~SerOutStreamQByteArray() override { if (_own) delete _data; }

   QByteArray buffer() const { return _data ? *_data : QByteArray(); }

   bool write(const char *buffer, qint64 size) override;

   void flush() override;

private:
   QByteArray* _data = nullptr;
   bool _own = false;
};

bool SerOutStreamQByteArray::write(const char *buffer, qint64 size)
{
   if (_data == nullptr)
      return true;

   if (buffer == nullptr)
   {
      _lastError = "Invalid buffer";
      return false;
   }

   _data->append(buffer, (int)size);

   return true;
}

void SerOutStreamQByteArray::flush()
{
}

class SerInStreamFILE : public SerStream::InDevice
{
public:
   SerInStreamFILE(FILE* data) : _data(data) { _buffer.reserve(1024); }
   ~SerInStreamFILE() override = default;

   bool atEnd() const override;
   bool hasData() const override;
   qint64 pos() const override;
   void reset() override;

   bool read(char *buffer, qint64 size, bool peek) override;

private:
   FILE* _data = nullptr;
   QByteArray _buffer;
   int _readPos = 0;
   int _writePos = 0;
};

bool SerInStreamFILE::atEnd() const
{
   return _readPos == _writePos && feof(_data);
}

bool SerInStreamFILE::hasData() const
{
   return _readPos < _writePos || feof(_data) == 0;
}

qint64 SerInStreamFILE::pos() const
{
   return (int)ftell(_data) - _writePos + _readPos;
}

void SerInStreamFILE::reset()
{
   _readPos = 0;
   _writePos = 0;
   rewind(_data);
}

bool SerInStreamFILE::read(char *buffer, qint64 size, bool peek)
{
   if (!_data)
      return false;

   auto readPos = _readPos;

   while (readPos < _writePos && size)
   {
      *buffer = _buffer.at(readPos);
      buffer++;
      readPos++;
      size--;
   }
   if (!peek)
      _readPos = readPos;

   if (size)
   {
      Q_ASSERT(_readPos == _writePos);

      if (!peek)
      {
         _writePos = 0;
         _readPos = 0;
      }

      const auto bytesRead = fread(buffer, 1, size, _data);

      if (peek)
      {
         _buffer.insert(_writePos, buffer, (int)bytesRead);
         _writePos += (int)bytesRead;
      }
      size -= (qint64)bytesRead;
   }

   if (size == 0LL)
      return true;

   if (atEnd())
   {
      _lastError = "End of file";
   }
   else
   {
      QByteArray msgBuffer(100, 0);
#ifdef Q_OS_WIN
      strerror_s(msgBuffer.data(), msgBuffer.size(), ferror(_data));
#else
      strerror_r(ferror(_data), msgBuffer.data(), msgBuffer.size());
#endif
      _lastError = QString::fromLocal8Bit(msgBuffer);
   }

   return false;
}

class SerOutStreamFILE : public SerStream::OutDevice
{
public:
   SerOutStreamFILE(FILE* data) : _data(data) {}
   ~SerOutStreamFILE() override = default;

   bool write(const char *buffer, qint64 size) override;

   void flush() override;

private:
   FILE* _data = nullptr;
   size_t _bytesToWrite = 0;
};

bool SerOutStreamFILE::write(const char *buffer, qint64 size)
{
   if (!_data)
      return true;

   const auto bytesWritten = fwrite(buffer, 1, size, _data);

   if (bytesWritten == (size_t)size)
   {
      _bytesToWrite += bytesWritten;
      return true;
   }

   QByteArray msgBuffer(100, 0);
#ifdef Q_OS_WIN
   strerror_s(msgBuffer.data(), msgBuffer.size(), errno);
#else
   strerror_r(errno, msgBuffer.data(), msgBuffer.size());
#endif
   _lastError = QString::fromLocal8Bit(msgBuffer);

   return false;
}

void SerOutStreamFILE::flush()
{
   if (_bytesToWrite)
   {
      if (fflush(_data))
      {
         QByteArray msgBuffer(100, 0);
#ifdef Q_OS_WIN
         strerror_s(msgBuffer.data(), msgBuffer.size(), errno);
#else
         strerror_r(errno, msgBuffer.data(), msgBuffer.size());
#endif
         _lastError = QString::fromLocal8Bit(msgBuffer);
      }
      _bytesToWrite = 0;
   }
}

//
// class SerStream
//

SerStream::SerStream() : _version(actualVersion), _outDevice(new SerOutStreamQByteArray(new QByteArray, true))
{
}

SerStream::SerStream(QIODevice *device, bool own) : _version(actualVersion)
{
   if (device)
   {
      const auto d = new QPair<QIODevice*, int>(device, own ? 0x10000 : 0);

      if (device->isReadable())
         _inDevice = new SerInStreamQIODevice(d);
      if (device->isWritable())
         _outDevice = new SerOutStreamQIODevice(d);

      if ((d->second & 0xFFFF) == 0)
         delete d;
   }
}

SerStream::SerStream(const QByteArray& in, QByteArray* out, bool own) : _version(actualVersion)
{
   if (!in.isEmpty())
      _inDevice = new SerInStreamQByteArray(in);
   if (out)
      _outDevice = new SerOutStreamQByteArray(out, own);
}

SerStream::SerStream(QByteArray* out, bool own)
{
   if (out)
      _outDevice = new SerOutStreamQByteArray(out, own);
}

SerStream::SerStream(const QString& in, QString* out, bool own) : _version(actualVersion)
{
   if (!in.isEmpty())
      _inDevice = new SerInStreamQString(in);
   if (out)
      _outDevice = new SerOutStreamQString(out, own);
}

SerStream::SerStream(QString* out, bool own)
{
   if (out)
      _outDevice = new SerOutStreamQString(out, own);
}

SerStream::SerStream(FILE* in, FILE* out) : _version(actualVersion)
{
   if (in)
      _inDevice = new SerInStreamFILE(in);
   if (out)
      _outDevice = new SerOutStreamFILE(out);
}

SerStream::SerStream(InDevice* inDevice, OutDevice* outDevice) : _version(actualVersion), _inDevice(inDevice), _outDevice(outDevice)
{
}

SerStream::SerStream(OutDevice* outDevice) : _version(actualVersion), _outDevice(outDevice)
{
}

SerStream::~SerStream()
{
   delete _inDevice;
   delete _outDevice;
}

int SerStream::timeout() const
{
   if (_inDevice)
      return _inDevice->timeout();
   if (_outDevice)
      return _outDevice->timeout();
   return -1;
}

void SerStream::setTimeout(int msecs)
{
   if (_inDevice)
      _inDevice->setTimeout(msecs);
   if (_outDevice)
      _outDevice->setTimeout(msecs);
}

QIODevice* SerStream::inDevice()
{
   if (auto qdevice = dynamic_cast<SerInStreamQIODevice*>(_inDevice))
      return qdevice->device();
   return nullptr;
}

QIODevice* SerStream::outDevice()
{
   if (auto qdevice = dynamic_cast<SerOutStreamQIODevice*>(_outDevice))
      return qdevice->device();
   return nullptr;
}

void SerStream::setInDevice(QIODevice *device, bool own)
{
   if (device == inDevice())
      return;

   delete _inDevice;

   const auto d = new QPair<QIODevice*, int>(device, own ? 0x10000 : 0);
   _inDevice = new SerInStreamQIODevice(d);
   _errors.clear();
}

void SerStream::setOutDevice(QIODevice* device, bool own)
{
   if (device == outDevice())
      return;

   delete _outDevice;

   const auto d = new QPair<QIODevice*, int>(device, own ? 0x10000 : 0);
   _outDevice = new SerOutStreamQIODevice(d);
   _errors.clear();
}

bool SerStream::atEnd()
{
   if (!_inDevice)
      return true;
   if (!_inDevice->atEnd())
      return false;
   char c;
   peekRaw(&c, 1LL);
   return c == '\0';
}

bool SerStream::canRead() const
{
   return _inDevice && _inDevice->hasData();
}

void SerStream::setError(const char* functionName, const QString& error)
{
   _errors.append(QString::fromLatin1("%1 (at %2): %3").arg(QString::fromLatin1(functionName)).arg(_inDevice ? _inDevice->pos() : 0).arg(error));
   if (_escalate)
      throw QBasicException(functionName);
}

void SerStream::reset()
{
   if (_inDevice)
      _inDevice->reset();
   _errors.clear();
}

void SerStream::writeRaw(const char *buffer, qint64 size)
{
   Q_ASSERT(_version > 0);

   if (size <= 0LL)
      return;

   if (hasError())
      return;

   if (_outDevice == nullptr)
   {
      setError(__FUNCTION__, "No device");
      return;
   }

   if (!_outDevice->write(buffer, size))
      setError(__FUNCTION__, _outDevice->errorString());
}

void SerStream::flush()
{
   Q_ASSERT(_version > 0);

   if (hasError())
      return;

   if (_outDevice == nullptr)
   {
      setError(__FUNCTION__, "No device");
      return;
   }

   _outDevice->flush();
   if (!_outDevice->errorString().isEmpty())
      setError(__FUNCTION__, _outDevice->errorString());
}

bool SerStream::readBool()
{
   bool        result = false;
   const char* expectedChar = nullptr;
   char        c;

   while (true)
   {
      readChar(&c);
      if (expectedChar == nullptr)
      {
         if (c == 't')
            result = true;
         expectedChar = result ? "true" : "false";
      }
      if (c != *expectedChar)
         break;
      expectedChar++;
      if (*expectedChar == '\0')
         break;
   }

   if (expectedChar == nullptr || *expectedChar != '\0')
      setError(__FUNCTION__, "Unexpected character");

   return result;
}

qint32 SerStream::readInt32()
{
   qint32   result = 0;
   int      digitsRead = 0;
   bool     negative = false;
   char     c;

   while (true)
   {
      readChar(&c);
      if (digitsRead == 0 && c == '-')
         negative = true;
      else if (c >= '0' && c <= '9')
      {
         result *= 10;
         result += (int)(c - '0');
         digitsRead++;
      }
      else
         break;
   }

   if (digitsRead == 0 || !_isEntityEnd(c))
      setError(__FUNCTION__, "No digits or end of entity missing");

   if (negative)
      return -result;
   return result;
}

quint32 SerStream::readUInt32()
{
   quint32  result = 0;
   int      digitsRead = 0;
   char     c;

   while (true)
   {
      readChar(&c);
      if (c < '0' || c > '9')
         break;

      result *= 10;
      result += (unsigned int)(unsigned char)(c - '0');
      digitsRead++;
   }

   if (digitsRead == 0 || !_isEntityEnd(c))
      setError(__FUNCTION__, "No digits or end of entity missing");

   return result;
}

qint64 SerStream::readInt64()
{
   qint64   result = 0;
   int      digitsRead = 0;
   bool     negative = false;
   char     c;

   while (true)
   {
      readChar(&c);
      if (digitsRead == 0 && c == '-')
         negative = true;
      else if (c >= '0' && c <= '9')
      {
         result *= 10LL;
         result += (long long int)(c - '0');
         digitsRead++;
      }
      else
         break;
   }

   if (digitsRead == 0 || !_isEntityEnd(c))
      setError(__FUNCTION__, "No digits or end of entity missing");

   if (negative)
      return -result;
   return result;
}

quint64 SerStream::readUInt64()
{
   quint64  result = 0;
   int      digitsRead = 0;
   char     c;

   while (true)
   {
      readChar(&c);
      if (c < '0' || c > '9')
         break;

      result *= 10ULL;
      result += (unsigned long long int)(unsigned char)(c - '0');
      digitsRead++;
   }

   if (digitsRead == 0 || !_isEntityEnd(c))
      setError(__FUNCTION__, "No digits or end of entity missing");

   return result;
}

double SerStream::readDouble()
{
   QByteArray  buffer;
   char        c;

   while (true)
   {
      readChar(&c);
      if (!_isValidChar(c))
         break;
      buffer.append(c);
   }

   const char* data = buffer.data();
   const char* end = data + buffer.length();
   double      result = stringToDouble(&data, '0');

   if (data != end || !_isEntityEnd(c))
      setError(__FUNCTION__, "Invalid double or end of entity missing");

   return result;
}

QString SerStream::readText()
{
   QString result;

   if (_version == 0)
   {
      // Strings in version 0 are not SerStream conform!
      // All characters up to a 0 terminating character are read and interpreted as UTF-8 character sequence.

      QByteArray  buffer;
      char        c;

      while (true)
      {
         readChar(&c);
         if (c == '\0')
            break;
         buffer.append(c);
      }

      result = QString::fromUtf8(buffer);
   }
   else
   {
      while (true)
      {
         int charCode = decodeChar();

         if (charCode < 0)
         {
            // This can be a terminating character, the end of stream, an error or base64 encoded data
            if (charCode != -1)
               setError(__FUNCTION__, "Invalid character sequence or end of entity missing");
            break;
         }

         result.append((ushort)charCode);
      }
   }

   return result;
}

QByteArray SerStream::readData()
{
   QByteArray  result;
   bool        isBase64 = false;
   char        c;

   if (_version == 0)
   {
      isBase64 = true;

      while (true)
      {
         readChar(&c);
         if (!_isValidChar(c))
            break;
         result.append(c);
      }
   }
   else
   {
      int charCode = decodeChar();

      if (charCode == -2)
      {
         isBase64 = true;
         charCode = decodeChar();
      }

      while (charCode >= 0)
      {
         result.append((char)charCode);
         charCode = decodeChar();
      }

      if (charCode != -1)
         setError(__FUNCTION__, "Invalid character sequence or end of entity missing");
   }

   if (isBase64)
      result = QByteArray::fromBase64(result);

   return result;
}

QByteArray SerStream::readRaw(qint64 size)
{
   QByteArray data;

   data.resize((int)size);

   readRaw(data.data(), size);

   return data;
}

// If there is not enough data to fill the buffer the buffer is completed with 0 chars.
// Hitting the end of stream does not set an error.
void SerStream::readRaw(char *buffer, qint64 size, bool peek)
{
   if (size <= 0LL)
      return;

   memset((void*)buffer, 0, (size_t)size);

   if (hasError())
      return;

   if (_inDevice == nullptr)
   {
      setError(__FUNCTION__, "No device");
      return;
   }

   if (_flushOnRead && _outDevice)
      _outDevice->flush();

   if (!_inDevice->read(buffer, size, peek) && !_inDevice->errorString().isEmpty())
      setError(__FUNCTION__, _inDevice->errorString());
}

QByteArray SerStream::peekRaw(qint64 size)
{
   QByteArray data;

   data.resize((int)size);

   peekRaw(data.data(), size);

   return data;
}

void SerStream::peekRaw(char* buffer, qint64 size)
{
   readRaw(buffer, size, true);
}

void SerStream::skipEntityEnd()
{
   if (atEnd())
      return; // Fine

   char c;
   readChar(&c);
   if (!_isEntityEnd(c))
      setError(__FUNCTION__, "Unexpected character");
}

bool SerStream::atRecordEnd()
{
   if (atEnd())
      return true;

   char c;

   peekRaw(&c, 1LL);

   return c == _recEndChar;
}

void SerStream::skipRecordEnd()
{
   if (atEnd())
      return; // Fine

   char c;
   readChar(&c);
   if (c != _recEndChar)
      setError(__FUNCTION__, "Unexpected character");
}

QByteArray SerStream::internalBuffer() const
{
   const auto byteData = dynamic_cast<SerOutStreamQByteArray*>(_outDevice);
   if (byteData)
      return byteData->buffer();
   return QByteArray();
}

void SerStream::insertEncoded(char c)
{
   if (_isValidChar(c))
   {
      if (c == '%')
         writeRaw("%", 1LL);
      writeRaw(&c, 1LL);
   }
   else
   {
      int   charVal = (unsigned char)c;
      char  encoded[] = "%%%%";
      char* pC = (char *)encoded + 2;

      do
      {
         *pC = (char)(charVal & 0xF);
         if (*pC > 9)
            *pC += 'A' - 10;
         else
            *pC += '0';
         pC--;
         charVal >>= 4;
      } while (charVal);
      writeRaw(pC, encoded + 4 - pC);
   }
}

void SerStream::insertEncoded(QChar c)
{
   Q_ASSERT(_version > 0);

   if (_isValidChar(c.unicode()))
   {
      char cOut = (char)c.unicode();

      if (cOut == '%')
         writeRaw("%", 1LL);
      writeRaw(&cOut, 1LL);
   }
   else
   {
      int   charVal = (int)c.unicode();
      char  encoded[] = "%%%%%%";
      char* pC = (char *)encoded + 4;

      do
      {
         *pC = (char)(charVal & 0xF);
         if (*pC > 9)
            *pC += 'A' - 10;
         else
            *pC += '0';
         pC--;
         charVal >>= 4;
      } while (charVal);
      writeRaw(pC, encoded + 6 - pC);
   }
}

void SerStream::insertEncoded(QByteArray data)
{
   // Test the first 16 bytes to see if its text or data
   int separatorCount = 0;

   for (int i = 0; i < data.length() && i < 16; ++i)
   {
      if (!_isValidChar(data.at(i)))
         separatorCount++;
   }

   if (separatorCount > 8)
   {
      data = data.toBase64();
      writeRaw("%!", 2LL);
   }

   foreach (char c, data)
   {
      insertEncoded(c);
   }
}

void SerStream::insertEncoded(const QString& data)
{
   foreach (QChar c, data)
   {
      insertEncoded(c);
   }
}

int SerStream::decodeChar()
{
   char c;

   readChar(&c);

   if (!_isValidChar(c))
      return c == _entityEndChar ? -1 : -3;
   if (c != '%')
      return (unsigned char)c;

   int   charCode = 0;
   int   charRead = 0;

   while (true)
   {
      readChar(&c);
      if (c < '0' || (c > '9' && c < 'A') || c > 'F')
         break;

      if (c > '9')
         c -= 'A' - 10;
      else
         c -= '0';

      charCode <<= 4;
      charCode |= (int)c;

      charRead++;
   }

   if (charRead == 0)
   {
      if (c == '!')
         charCode = -2;
      else if (c == '%')
         charCode = (int)'%';
      else
         charCode = -3;
   }
   else if (c != '%')
      charCode = -3;

   return charCode;
}

void SerStream::readChar(char *c)
{
   readRaw(c, 1LL);
}

QTCOREEX_EXPORT SerStream &operator<<(SerStream& stream, const QDate& date)
{
   if (stream.version() < 2)
      stream << date.toJulianDay();
   else
   {
      stream << date.year();
      stream << date.month();
      stream << date.day();
   }
   return stream;
}

QTCOREEX_EXPORT SerStream &operator>>(SerStream& stream, QDate& date)
{
   if (stream.version() < 2)
      date = QDate::fromJulianDay(stream.readInt32());
   else
   {
      int year, month, day;

      stream >> year >> month >> day;
      date.setDate(year, month, day);
   }

   return stream;
}

QTCOREEX_EXPORT SerStream &operator<<(SerStream& stream, const QTime& time)
{
   if (stream.version() < 2)
      stream << time.toString("hh:mm:ss");
   else
   {
      stream << time.hour();
      stream << time.minute();
      stream << time.second();
      stream << time.msec();
   }

   return stream;
}

QTCOREEX_EXPORT SerStream &operator>>(SerStream& stream, QTime& time)
{
   if (stream.version() < 2)
      time = QTime::fromString(stream.readText(), "hh:mm:ss");
   else
   {
      int hour, minute, second, msec;

      stream >> hour >> minute >> second >> msec;
      time.setHMS(hour, minute, second, msec);
   }

   return stream;
}

QTCOREEX_EXPORT SerStream &operator<<(SerStream& stream, const QDateTime& dateTime)
{
   stream << dateTime.date() << dateTime.time();

   return stream;
}

QTCOREEX_EXPORT SerStream &operator>>(SerStream& stream, QDateTime& dateTime)
{
   QDate date;
   QTime time;

   if (stream.version() == 0)
   {
      // Get raw data from the stream
      QByteArray serData;

      for (;;)
      {
         int charValue = stream.readCharValue();

         if (charValue == -1)
            break;

         if (charValue == ' ')
         {
            date = QDate::fromJulianDay(serData.toInt());
            serData.clear();
         }
         else
            serData.append((char)(unsigned char)charValue);
      }
      time = QTime::fromString(QString::fromLatin1(serData), "hh:mm:ss");
   }
   else
   {
      stream >> date >> time;
   }

   dateTime = QDateTime(date, time);

   return stream;
}
