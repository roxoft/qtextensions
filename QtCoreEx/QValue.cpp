#include "stdafx.h"
#include "QValue.h"
#include "Variant.h"

QString QValue::toString(const QLocaleEx& locale) const
{
   QString result = QDecimal::toString(locale);

   if (!result.isEmpty() && !_unit.isEmpty())
      result += " " + _unit;

   return result;
}

QString QValue::toString(GroupSeparatorMode groupSeparatorMode, const QLocaleEx& locale) const
{
   QString result = QDecimal::toString(groupSeparatorMode, locale);

   if (!result.isEmpty() && !_unit.isEmpty())
      result += " " + _unit;

   return result;
}

bool QValue::fromString(const QString& number, const QString& unit, const QLocaleEx& locale)
{
   if (!QDecimal::fromString(number, locale))
      return false;

   _unit = unit;

   return true;
}

//
// QValue conversions
//

static bool convert(QValue& lhs, const QString& rhs, const QLocaleEx& locale)
{
   bool ok;

   lhs = QDecimal(rhs, locale, &ok);
   return ok;
}

static bool convert(QValue& lhs, const QDecimal& rhs, const QLocaleEx& locale)
{
   lhs = rhs;
   return true;
}

static bool convert(QString& lhs, const QValue& rhs, const QLocaleEx& locale)
{
   lhs = rhs.toString(locale);
   return true;
}

static bool convert(QDecimal& lhs, const QValue& rhs, const QLocaleEx& locale)
{
   lhs = (const QDecimal &)rhs;
   return true;
}

BEGIN_VARIANT_REGISTRATION
   Variant::registerType<QValue>();

   Variant::registerConversion<QValue, QString>();
   Variant::registerConversion<QValue, QDecimal>();
   Variant::registerConversion<QString, QValue>();
   Variant::registerConversion<QDecimal, QValue>();
END_VARIANT_REGISTRATION
