#include "stdafx.h"
#include "QBlockingDevice.h"

QBlockingDevice::QBlockingDevice(QIODevice *asyncDevice, QObject *parent) : QIODevice(parent), _asyncDevice(nullptr), _readTimeout(-1), _writeTimeout(-1)
{
   setDevice(asyncDevice);
}

QBlockingDevice::~QBlockingDevice()
{
   close();
}

void QBlockingDevice::setDevice(QIODevice *asyncDevice)
{
   if (_asyncDevice == asyncDevice)
      return;

   if (_asyncDevice)
      close();
   _asyncDevice = asyncDevice;
   if (_asyncDevice)
   {
      open(QIODevice::ReadWrite | QIODevice::Unbuffered);
      connect(_asyncDevice, SIGNAL(aboutToClose()), this, SLOT(asyncDeviceClosing()));
      connect(_asyncDevice, SIGNAL(destroyed()), this, SLOT(asyncDeviceDestroyed()));
   }
}

qint64 QBlockingDevice::readData(char *data, qint64 maxSize)
{
   qint64 totalBytesRead = 0LL;

   if (_asyncDevice && maxSize)
   {
      for (;;)
      {
         qint64 bytesRead = _asyncDevice->read(data, maxSize);

         if (bytesRead < 0LL)
            return -1LL;

         maxSize -= bytesRead;
         totalBytesRead += bytesRead;
         data += bytesRead;

         if (maxSize == 0LL)
            break;

         _asyncDevice->waitForReadyRead(_readTimeout);
      }
   }

   return totalBytesRead;
}

qint64 QBlockingDevice::writeData(const char *data, qint64 maxSize)
{
   qint64 totalBytesWritten = 0LL;

   if (_asyncDevice && maxSize)
   {
      for (;;)
      {
         qint64 bytesWritten = _asyncDevice->write(data, maxSize);

         if (bytesWritten < 0LL)
            return -1LL;

         maxSize -= bytesWritten;
         totalBytesWritten += bytesWritten;
         data += bytesWritten;

         if (maxSize == 0LL)
            break;

         _asyncDevice->waitForBytesWritten(_writeTimeout);
      }
   }

   return totalBytesWritten;
}

void QBlockingDevice::close()
{
   if (_asyncDevice && _asyncDevice->bytesToWrite())
      _asyncDevice->waitForBytesWritten(_writeTimeout);
   QIODevice::close();
}

void QBlockingDevice::asyncDeviceClosing()
{
   if (_asyncDevice->bytesToWrite())
      _asyncDevice->waitForBytesWritten(_writeTimeout);
}

void QBlockingDevice::asyncDeviceDestroyed()
{
   _asyncDevice = nullptr;
}
