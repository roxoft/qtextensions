#include "stdafx.h"
#include "HtStandardNode.h"
#include "HtModel.h"

HtStandardNode::~HtStandardNode()
{
   qDeleteAll(_childList);
}

HtStandardNode& HtStandardNode::operator=(const HtNode* other)
{
   if (other)
   {
      QVector<HtNode*> childList(other->childCount());

      for (int i = 0; i < other->childCount(); ++i)
      {
         const auto node = new HtStandardNode;

         *node = other->child(i);
         childList[i] = node;
      }

      assignAttributesFrom(other);
      setChildList(childList);
   }

   return *this;
}

Variant HtStandardNode::attribute(int index, int role) const
{
   if (role == Qt::EditRole)
      role = Qt::DisplayRole;

   Variant result;

   if (index >= 0)
   {
      QMap<int, QVector<Variant> >::const_iterator it(_attributeMap.constFind(role));

      if (it != _attributeMap.end() && index < it.value().count())
         result = it.value().at(index);

      if (result.isNull())
         result = attributeDefault(index, role);
   }
   else
   {
      result = attributeDefault(index, role);
   }

   return result;
}

QVector<Variant> HtStandardNode::attributes(int role) const
{
   if (role == Qt::EditRole)
      role = Qt::DisplayRole;

   return _attributeMap.value(role);
}

QMap<int, Variant> HtStandardNode::attributeRoleMap(int index) const
{
   QMap<int, Variant> result;

   if (index >= 0)
   {
      for (auto it = _attributeMap.begin(); it != _attributeMap.end(); ++it)
      {
         auto attributeList = it.value();
         Variant value;

         if (index < attributeList.count())
            value = attributeList.at(index);

         if (value.isNull())
            value = attributeDefault(index, it.key());

         result.insert(it.key(), value);
      }
   }

   return result;
}

void HtStandardNode::setAttribute(int index, const Variant& value, int role)
{
   if (role == Qt::EditRole)
      role = Qt::DisplayRole;

   if (index < 0 || role < 0)
      return;

   if (index >= _attributeCount)
      _attributeCount = index + 1;

   QVector<Variant>& attributeList = _attributeMap[role];

   if (index >= attributeList.count() && !value.isNull()) // It is not necessary to add NULL variants
      attributeList.resize(index + 1);

   if (index < attributeList.count() && value != attributeList.at(index)) // Added NULL variants are not signaled
   {
      attributeList[index] = value;
      if (_model)
         _model->onItemChanged(this, index, index, role);
   }
}

void HtStandardNode::insertAttributes(int index, int count)
{
   if (index < 0 || count < 1)
      return;

   if (index > _attributeCount)
      _attributeCount = index;
   _attributeCount += count;

   for (QMap<int, QVector<Variant> >::iterator it = _attributeMap.begin(); it != _attributeMap.end(); ++it)
   {
      QVector<Variant>& attributeList = it.value();

      if (index < attributeList.count())
         attributeList.insert(index, count, Variant());
   }
}

void HtStandardNode::removeAttributes(int index, int count)
{
   if (index < 0 || count < 1)
      return;

   if (index + count <= _attributeCount)
      _attributeCount -= count;
   else if (index < _attributeCount)
      _attributeCount = index;

   if (_attributeCount == 0)
   {
      _attributeMap.clear();
      return;
   }

   for (QMap<int, QVector<Variant> >::iterator it = _attributeMap.begin(); it != _attributeMap.end(); ++it)
   {
      QVector<Variant>& attributeList = it.value();

      if (index + count <= attributeList.count())
         attributeList.remove(index, count);
      else if (index < attributeList.count())
         attributeList.resize(index);
   }
}

void HtStandardNode::assignAttributesFrom(const HtNode* other)
{
   if (auto standardNode = dynamic_cast<const HtStandardNode*>(other))
   {
      _attributeMap = standardNode->_attributeMap;
      if (_attributeCount < standardNode->_attributeCount)
         _attributeCount = standardNode->_attributeCount;
      _defaultMap = standardNode->_defaultMap;
   }
   else if (other)
   {
      _attributeMap.clear();
      if (_attributeCount < other->attributeCount())
         _attributeCount = other->attributeCount();

      for (int c = 0; c < _attributeCount; ++c)
      {
         auto valueMap = other->attributeRoleMap(c);

         for (auto it = valueMap.begin(); it != valueMap.end(); ++it)
         {
            auto& values = _attributeMap[it.key()];

            if (values.isEmpty())
               values.resize(_attributeCount);

            values[c] = it.value();
         }
      }
   }

   if (_model)
      _model->onItemChanged(this, 0, _attributeCount - 1);
}

Variant HtStandardNode::defaultAttribute(int index, int role) const
{
   if (role == Qt::EditRole)
      role = Qt::DisplayRole;

   Variant result;

   index++; // The first value is the default for all attributes
   if (index >= 0)
   {
      // Find the default attributes for the given role
      const auto it = _defaultMap.constFind(role);

      if (it != _defaultMap.end())
      {
         auto& attributeList = it.value();

         if (index < attributeList.count())
            result = attributeList.at(index);
      }
   }

   return result;
}

void HtStandardNode::setDefaultAttribute(int index, const Variant& value, int role)
{
   if (role == Qt::EditRole)
      role = Qt::DisplayRole;

   index++; // The first value is the default for the all attributes
   if (index < 0 || role < 0)
      return;

   QVector<Variant>& attributeList = _defaultMap[role];

   if (index >= attributeList.count() && !value.isNull()) // Its not necessary to add NULL variants
      attributeList.resize(index + 1);

   if (index < attributeList.count())
      attributeList[index] = value;

   if (_model)
   {
      if (index == 0)
         _model->onItemChanged(this, 0, attributeCount() - 1, role);
      _model->startCollectChanges();
      for (auto node : _childList)
         _model->onItemChanged(node, index == 0 ? 0 : index - 1, index == 0 ? node->attributeCount() - 1 : index - 1, role);
      _model->endCollectChanges();
   }
}

const HtNode *HtStandardNode::child(int index) const
{
   static HtStandardNode defaultNode;

   if (index >= 0 && index < _childList.count() && _childList.at(index) != nullptr)
      return _childList.at(index);

   return &defaultNode;
}

void HtStandardNode::assertSize(int size)
{
   while (_childList.count() < size)
   {
      const auto node = new HtStandardNode(_model, this);

      if (_model)
         _model->onBeginInsertItems(this, _childList.count(), 1);
      _childList.append(node);
      if (_model)
         _model->onEndInsertItems(this, _childList.count() - 1, 1);
   }
}

HtNode* HtStandardNode::child(int index)
{
   if (index < 0)
      return nullptr;

   assertSize(index + 1);

   return _childList[index];
}

HtNode* HtStandardNode::insertChild(int index, HtNode* child)
{
   if (index < 0)
   {
      if (child->parent() == nullptr)
         delete child;
      return nullptr;
   }

   if (child == nullptr || child->parent())
   {
      const auto node = new HtStandardNode;

      *node = child;
      child = node;
   }

   child->setParent(this);
   child->setModel(_model);

   assertSize(index);

   if (_model)
      _model->onBeginInsertItems(this, index, 1);
   _childList.insert(index, child);
   if (_model)
      _model->onEndInsertItems(this, index, 1);

   return child;
}

HtNode* HtStandardNode::insertChild(int index, const HtNode* child, bool /*link*/)
{
   const auto node = new HtStandardNode;

   *node = child;
   return insertChild(index, node);
}

void HtStandardNode::removeChild(int index)
{
   if (index >= 0 && index < _childList.count())
   {
      if (_model)
         _model->onBeginRemoveItems(this, index, 1);
      delete _childList[index];
      _childList.remove(index);
      if (_model)
         _model->onEndRemoveItems(this, index);
   }
}

HtNode* HtStandardNode::takeChild(int index)
{
   HtNode* child = nullptr;

   if (index >= 0 && index < _childList.count())
   {
      child = _childList.at(index);
      if (_model)
         _model->onBeginRemoveItems(this, index, 1);
      _childList.remove(index);
      if (_model)
         _model->onEndRemoveItems(this, index);
      child->setParent(nullptr);
      child->setModel(nullptr);
   }

   return child;
}

void HtStandardNode::removeAllChildNodes()
{
   if (_childList.isEmpty())
      return;

   if (_model)
      _model->onBeginRemoveItems(this, 0, _childList.count());
   qDeleteAll(_childList);
   _childList.clear();
   if (_model)
      _model->onEndRemoveItems(this, 0);
}

bool HtStandardNode::move(int from, int to)
{
   if (from == to)
      return true;

   if (from >= _childList.count() || to >= _childList.count())
      return false;

   if (_model)
   {
      if (!_model->onBeginMoveItems(this, from, 1, to))
         return false;
#if (QT_VERSION == QT_VERSION_CHECK(5, 10, 1))
      _model->onBeginLayoutChange(this); // Bug fix to avoid resizing all columns to standard size
#endif
   }

   _childList.move(from, to);

   if (_model)
      _model->onEndMoveItems(this, from, 1, to);

   return true;
}

QVector<const HtNode*> HtStandardNode::childList() const
{
   QVector<const HtNode*> result(_childList.count());

   for (int i = 0; i < _childList.count(); ++i)
      result[i] = _childList.at(i);

   return result;
}

void HtStandardNode::setChildList(const QVector<HtNode*>& childList)
{
   removeAllChildNodes();

   if (childList.isEmpty())
      return;

   if (_model)
      _model->onBeginInsertItems(this, 0, childList.count());
   _childList = childList;
   foreach (HtNode* node, _childList)
   {
      node->setModel(_model);
      node->setParent(this);
   }
   if (_model)
      _model->onEndInsertItems(this, 0, _childList.count());
}

void HtStandardNode::sort(const HtSortOrder& sortOrder)
{
   if (_childList.count() < 2)
      return;

   if (_model)
      _model->onBeginLayoutChange(this);
   std::stable_sort(_childList.begin(), _childList.end(), sortOrder);
   if (_model)
      _model->onEndLayoutChange(this);
}

void HtStandardNode::setModel(HtModel* model)
{
   if (_model == model)
      return;

   _model = model;
   foreach (HtNode* item, _childList)
      item->setModel(model);
}
