#include "stdafx.h"
#include "HtIterators.h"
#include "HtNode.h"
#include "basic_exceptions.h"

HtCellContainer::RecursiveCellIteratorImpl::RecursiveCellIteratorImpl(const HtNode *container)
   : IteratorCore(container), _node(nullptr), _attIndex(0)
{
   if (container && container->childCount())
      _node = container->child(0);
   updateValue();
}

bool HtCellContainer::RecursiveCellIteratorImpl::isEqualTo(const IteratorCore<const HtNode*, HtCellContainer> &rhs) const
{
   auto rhsIterator = dynamic_cast<const RecursiveCellIteratorImpl &>(rhs);

   return _node == rhsIterator._node && _attIndex == rhsIterator._attIndex;
}

bool HtCellContainer::RecursiveCellIteratorImpl::isLessThan(const IteratorCore& rhs) const
{
   throw QNotImplementedException(QLatin1String("Not implemented"));
}

bool HtCellContainer::RecursiveCellIteratorImpl::isValid() const
{
   return _attIndex >= 0 && _node != _container && _node && _attIndex < _node->attributeCount();
}

void HtCellContainer::RecursiveCellIteratorImpl::moveBackward(int distance)
{
   throw QNotImplementedException(QLatin1String("Not implemented"));
}

int HtCellContainer::RecursiveCellIteratorImpl::distanceFrom(const IteratorCore& other) const
{
   throw QNotImplementedException(QLatin1String("Not implemented"));
}

void HtCellContainer::RecursiveCellIteratorImpl::moveNext()
{
   if (_node == nullptr || _node == _container)
      return;

   _attIndex++;
   if (_attIndex >= _node->attributeCount())
   {
      int parentIndex = 0;

      _attIndex = 0;

      // Move to the next node
      while (_node != _container)
      {
         if (parentIndex < _node->childCount())
         {
            _node = _node->child(parentIndex);
            parentIndex = 0;
            if (_node->attributeCount())
               break;
         }
         else
         {
            parentIndex = _node->parent()->indexOf(_node);
            _node = _node->parent();
            parentIndex++;
         }
      }
   }

   updateValue();
}

void HtCellContainer::RecursiveCellIteratorImpl::updateValue()
{
   if (_node && _attIndex >= 0 && _attIndex < _node->attributeCount())
      _value = _node->attribute(_attIndex);
}
