#include "stdafx.h"
#include "SqlParser.h"
#include "IVariable.h"

struct SqlOpDef
{
   const char* op;
   AbstractExpression::Operator::Type type;
   SqlOperator::Action action;
} sqlOperators[] = {
   // At the same op character position the letters must be together! The longer operators must come first!
   {"+", AbstractExpression::Operator::Type::Binary, SqlOperator::Action::Addition},
   {"-", AbstractExpression::Operator::Type::Binary, SqlOperator::Action::Subtraction},
   {"*=", AbstractExpression::Operator::Type::Binary, SqlOperator::Action::LeftEqual},
   {"*", AbstractExpression::Operator::Type::Binary, SqlOperator::Action::Multiplication},
   {"/", AbstractExpression::Operator::Type::Binary, SqlOperator::Action::Division},
   {"%", AbstractExpression::Operator::Type::Binary, SqlOperator::Action::Modulo},
   {"=*", AbstractExpression::Operator::Type::Binary, SqlOperator::Action::RightEqual},
   {"=", AbstractExpression::Operator::Type::Binary, SqlOperator::Action::Equal},
   {"!=", AbstractExpression::Operator::Type::Binary, SqlOperator::Action::NotEqual},
   {"!<", AbstractExpression::Operator::Type::Binary, SqlOperator::Action::NotLess},
   {"!>", AbstractExpression::Operator::Type::Binary, SqlOperator::Action::NotGreater},
   {"<=", AbstractExpression::Operator::Type::Binary, SqlOperator::Action::LessOrEqual},
   {"<>", AbstractExpression::Operator::Type::Binary, SqlOperator::Action::NotEqual},
   {"<", AbstractExpression::Operator::Type::Binary, SqlOperator::Action::Less},
   {">=", AbstractExpression::Operator::Type::Binary, SqlOperator::Action::GreaterOrEqual},
   {">", AbstractExpression::Operator::Type::Binary, SqlOperator::Action::Greater},
   {".", AbstractExpression::Operator::Type::Binary, SqlOperator::Action::Punctuator},
   {",", AbstractExpression::Operator::Type::Binary, SqlOperator::Action::Comma},
   {"ALL", AbstractExpression::Operator::Type::Prefix, SqlOperator::Action::SQL_ALL},
   {"AND", AbstractExpression::Operator::Type::Binary, SqlOperator::Action::SQL_AND},
   {"ANY", AbstractExpression::Operator::Type::Prefix, SqlOperator::Action::SQL_ANY},
   {"BETWEEN", AbstractExpression::Operator::Type::Binary, SqlOperator::Action::SQL_BETWEEN},
   {"EXISTS", AbstractExpression::Operator::Type::Prefix, SqlOperator::Action::SQL_EXISTS},
   {"IN", AbstractExpression::Operator::Type::Binary, SqlOperator::Action::SQL_IN},
   {"IS", AbstractExpression::Operator::Type::Binary, SqlOperator::Action::SQL_IS},
   {"LIKE", AbstractExpression::Operator::Type::Binary, SqlOperator::Action::SQL_LIKE},
   {"NOT BETWEEN", AbstractExpression::Operator::Type::Binary, SqlOperator::Action::SQL_NOT_BETWEEN},
   {"NOT EXISTS", AbstractExpression::Operator::Type::Prefix, SqlOperator::Action::SQL_NOT_EXISTS},
   {"NOT IN", AbstractExpression::Operator::Type::Binary, SqlOperator::Action::SQL_NOT_IN},
   {"NOT LIKE", AbstractExpression::Operator::Type::Binary, SqlOperator::Action::SQL_NOT_LIKE},
   {"NOT", AbstractExpression::Operator::Type::Prefix, SqlOperator::Action::SQL_NOT},
   {"OR", AbstractExpression::Operator::Type::Binary, SqlOperator::Action::SQL_OR},
   {"UNIQUE", AbstractExpression::Operator::Type::Prefix, SqlOperator::Action::SQL_UNIQUE}
};

static AbstractExpression::Operator::Type operatorTypeFromAction(SqlOperator::Action action)
{
   for (auto&& sqlOp : sqlOperators)
   {
      if (sqlOp.action == action)
         return sqlOp.type;
   }

   return AbstractExpression::Operator::Type::Invalid;
}

Tokenizer::Context* createSqlContext()
{
   auto context = new ProbingContext;

   context->add(new WhiteSpaceContext(context));
   context->add(new CommentContext(QList<QPair<QString, QString>>() << QPair<QString, QString>("--", "\n") << QPair<QString, QString>("/*", "*/ "), context));
   context->add(new StringContext(QLatin1Char('\''), context));
   context->add(new SqlOperatorContext(context));
   context->add(new NumericContext(context));
   context->add(new IdentifierContext(context));
   context->add(new SeparatorContext(context));

   return context;
}

SqlOperator::SqlOperator(Action action) : Operator(operatorTypeFromAction(action)), _action(action)
{
}

SqlOperator::SqlOperator(Action action, Type type) : Operator(type), _action(action)
{
}

// The operator precedence is adopted form SQL where the logical NOT operator does not precede the comparison operators (unlike C++)
int SqlOperator::priority() const
{
   int result = 0;

   switch (_action)
   {
   case Action::Invalid:
      result = -1;
      break;
   case Action::Punctuator:
      result++;
   case Action::Multiplication:
   case Action::Division:
   case Action::Modulo:
      result++;
   case Action::Addition:
   case Action::Subtraction:
      result++;
   // Comparison operators
   case Action::Equal:
   case Action::LeftEqual:
   case Action::RightEqual:
   case Action::NotEqual:
   case Action::Less:
   case Action::Greater:
   case Action::LessOrEqual:
   case Action::GreaterOrEqual:
   case Action::NotLess:
   case Action::NotGreater:
   case Action::SQL_IS:
   case Action::SQL_LIKE:
   case Action::SQL_NOT_LIKE:
   case Action::SQL_IN:
   case Action::SQL_NOT_IN:
   case Action::SQL_ALL:
   case Action::SQL_ANY:
      result++;
   // Logical operators
   case Action::SQL_NOT:
      result++;
   case Action::SQL_AND:
      result++;
   case Action::SQL_BETWEEN:
   case Action::SQL_NOT_BETWEEN:
   case Action::SQL_EXISTS:
   case Action::SQL_NOT_EXISTS:
   case Action::SQL_OR:
   case Action::SQL_UNIQUE:
      result++;
   case Action::Comma:
      break;
   }

   return result;
}

bool SqlOperator::isAssociative() const
{
   return _action == Action::Addition
      || _action == Action::Multiplication
      || _action == Action::SQL_AND
      || _action == Action::SQL_OR;
}

QString SqlOperator::toString() const
{
   for (auto&& sqlOp : sqlOperators)
   {
      if (sqlOp.action == _action)
         return sqlOp.op;
   }
   return QString();
}

SqlOperator SqlOperatorToken::op() const
{
   return SqlOperator(sqlOperators[_opIndex].action, sqlOperators[_opIndex].type);
}

Variant SqlOperatorToken::value() const
{
   return QString(sqlOperators[_opIndex].op);
}

const Tokenizer::Context* SqlOperatorContext::readToken(Token& token, TextStreamReader& data) const
{
   auto opIndex = 0u;
   auto opEnd = sizeof(sqlOperators) / sizeof(*sqlOperators);
   auto opCharIndex = 0;
   auto charIndex = 0;

   while (opIndex < opEnd)
   {
      auto opChar = sqlOperators[opIndex].op[opCharIndex];

      if (!opChar)
      {
         // If the last character of the operator is a letter no letter must follow
         opChar = sqlOperators[opIndex].op[opCharIndex - 1];
         if (opChar < 'A' || opChar > 'Z' || !isIdentifierCharacter(data.peekChar(charIndex)))
         {
            data.skipChar(charIndex);
            token = new SqlOperatorToken(opIndex);
         }
         break;
      }

      if (opChar == data.peekChar(charIndex).toUpper().unicode() || opChar == u' ' && data.peekChar(charIndex).isSpace())
      {
         // Find the end of the same tokens
         for (auto i = opIndex; ++i < opEnd; )
         {
            if (sqlOperators[i].op[opCharIndex] != opChar)
            {
               opEnd = i;
               break;
            }
         }

         charIndex++;
         if (opChar == u' ')
         {
            // Skip all white spaces
            while (data.peekChar(charIndex).isSpace())
               charIndex++;
         }
         opCharIndex++;
      }
      else
      {
         opIndex++;
      }
   }

   return _outerContext;
}

bool SqlOperatorContext::isIdentifierCharacter(const QChar& c) const
{
   return c.isLetterOrNumber() || _identifierSpecialChars.contains(c);
}

ExpressionPtr createSqlExpressionTree(const QString& expression)
{
   return createSqlExpressionTree(Tokenizer(expression, createSqlContext()).tokenList());
}

ExpressionPtr createSqlExpressionTree(const QList<Token>& tokenList)
{
   typedef GenericOperatorExpression<SqlOperator> SqlOperatorExpression;
   typedef GenericValueExpression<SqlTokenValue> SqlValueExpression;

   ExpressionParser parser;

   // Feed the expression parser

   for (auto&& token : tokenList)
   {
      if (token.isSeparator(u'('))
      {
         //if (isFunction())
         //   addOperator(CppExpression(SqlOperator(ExpressionBase::Operator::Type::Binary, SqlOperator::Action::Function)));

         parser.beginEnclosure();
      }
      else if (token.isSeparator(u')'))
      {
         parser.endEnclosure();
      }
      //else if (token.isSeparator(u'{'))
      //{
      //   addOperator(CppExpression(SqlOperator(ExpressionBase::Operator::Type::Prefix, SqlOperator::Action::Block)));
      //   beginEnclosure();
      //}
      //else if (token.isSeparator(u'}'))
      //{
      //   endEnclosure();
      //}
      else if (const auto op = token.as<SqlOperatorToken>())
      {
         parser.addOperator(new SqlOperatorExpression(SqlOperator(op->op())));
      }
      else if (!token.is<Token::WhiteSpace>() && !token.is<Token::Comment>())
      {
         //if (isFunction())
         //   addOperator(SqlExpression(SqlOperator(ExpressionBase::Operator::Type::Binary, SqlOperator::Action::Function)));

         parser.addValue(new SqlValueExpression(token));
      }
   }

   return parser.expressionTree();
}
