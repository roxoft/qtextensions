#pragma once

#include "qtcoreex_global.h"
#include <QStringView>
#include <QMutex>
#if !defined(Q_OS_WIN)
#include <iconv.h>
#endif

class QTCOREEX_EXPORT QTextCodec
{
private:
   QTextCodec();
   ~QTextCodec();

public:
   QByteArray fromUnicode(QStringView str) const;

   QString toUnicode(const char* chars, int size) const;

   QString toUnicode(const QByteArray& a) const;

public:
   static QTextCodec* codecForLocale();

   static QTextCodec* codecForName(const QByteArray& name);

   static QTextCodec* codecForName(const char *name)
   {
      return codecForName(QByteArray(name));
   }

private:
   QByteArray _name;

#if defined(Q_OS_WIN)
   int _codePage = 0;
#else
   iconv_t _toUnicodeDescriptor = nullptr;
   iconv_t _fromUnicodeDescriptor = nullptr;
#endif

   QTextCodec* _nextCodec = nullptr;

private:
   static QMutex mutex;
   static QTextCodec rootTextCodec;
};
