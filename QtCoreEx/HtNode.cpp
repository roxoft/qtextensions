#include "stdafx.h"
#include "HtNode.h"
#include "HtModel.h"
#include "basic_exceptions.h"
#include "CsvParser.h"
#include <QTextStream>

bool HtSortOrder::operator()(const HtNode *left, const HtNode *right) const
{
   foreach (HtAttributeOrder orderInfo, *this)
   {
      if (left->attribute(orderInfo.index(), orderInfo.role()) < right->attribute(orderInfo.index(), orderInfo.role()))
         return !orderInfo.descending();
      if (right->attribute(orderInfo.index(), orderInfo.role()) < left->attribute(orderInfo.index(), orderInfo.role()))
         return orderInfo.descending();
   }

   return false;
}

Variant HtNode::attributeDefault(int index, int role) const
{
   auto result = defaultAttribute(-1, role);

   if (result.isNull())
   {
      if (parent())
         result = parent()->effectiveDefaultAttribute(index, role);
      else if (model())
         result = model()->defaultAttribute(role);
   }

   return result;
}

Variant HtNode::effectiveDefaultAttribute(int index, int role) const
{
   auto result = defaultAttribute(index, role);

   if (result.isNull())
      result = defaultAttribute(-1, role);

   if (result.isNull())
   {
      if (parent())
         result = parent()->effectiveDefaultAttribute(index, role);
      else if (model())
         result = model()->defaultAttribute(role);
   }

   return result;
}

HtChildrenContainer<HtNode*> HtNode::children()
{
   return this;
}

HtChildrenContainer<const HtNode*> HtNode::children() const
{
   return this;
}

HtCellContainer HtNode::cells() const
{
   return this;
}

QVector<HtNode*> HtNode::childList()
{
   auto count = childCount();
   QVector<HtNode*> result(count);

   while (count--)
      result[count] = child(count);

   return result;
}

QVector<const HtNode*> HtNode::childList() const
{
   auto count = childCount();
   QVector<const HtNode*> result(count);

   while (count--)
      result[count] = child(count);

   return result;
}

void HtNode::setChildList(const QVector<HtNode*>& childList)
{
   removeAllChildNodes();
   for (auto i = 0; i < childList.count(); ++i)
      insertChild(i, childList.at(i));
}

bool HtNode::moveTo(int index)
{
   if (!parent())
      return false;

   const auto fromIndex = parent()->indexOf(this);

   if (fromIndex < index)
      index--;

   return parent()->move(fromIndex, index);
}

void HtNode::remove()
{
   if (parent())
      parent()->removeChild(parent()->indexOf(this));
}

int HtNode::findIndex(int attIndex, const Variant &value, int start, Qt::MatchFlags flags, int role) const
{
   if (start < 0)
      return -1;

   bool  wrap = flags & Qt::MatchWrap;
   int   i = start;
   int   count = childCount();

   while (i < count)
   {
      if (child(i)->attribute(attIndex, role).match(value, flags))
         break;
      i++;
      if (wrap)
      {
         if (i == count)
            i = 0;
         if (i == start)
         {
            i = -1;
            break;
         }
      }
   }

   return i < count ? i : -1;
}

HtNode* HtNode::find(int attIndex, const Variant& value, int start, Qt::MatchFlags flags, int role)
{
   if (start < 0)
      return nullptr;

   HtNode*  node = nullptr;
   bool     wrap = flags & Qt::MatchWrap;
   bool     recursive = flags & Qt::MatchRecursive;
   int      i = start;
   int      count = childCount();

   while (i < count)
   {
      node = child(i);

      if (!node->attribute(attIndex, role).match(value, flags))
      {
         if (recursive)
            node = node->find(attIndex, value, 0, flags, role);
         else
            node = nullptr;
      }

      if (node)
         break;

      i++;
      if (wrap)
      {
         if (i == count)
            i = 0;
         if (i == start)
            break;
      }
   }

   return node;
}

#if 0
const HtNode* HtNode::nextItem(bool includeChildren, bool includeParents) const
{
   if (includeChildren)
   {
      // Return first child
      if (childCount() > 0)
         return child(0);
   }

   if (!parent())
      return NULL;

   // Return next sibling
   int index = parent()->indexOf(this);

   index++;
   if (index < parent()->childCount())
      return parent()->child(index);

   // Return the next sibling of a parent
   if (includeParents)
      return parent()->nextItem(false);

   return NULL;
}

const HtNode* HtNode::findForward(int attIndex, const Variant& value, Qt::MatchFlags flags, int role) const
{
   bool recurse = flags & Qt::MatchRecursive;
   bool wrap = flags & Qt::MatchWrap;

   const HtNode* next = this;

   forever
   {
      if (attIndex < next->attributeCount() && next->attribute(attIndex, role).match(value, flags))
         break;

      next = next->nextItem(recurse, recurse);
      if (next == NULL)
      {
         if (!wrap)
            break;

         next = parent();

         if (next == NULL)
            break;

         if (recurse)
         {
            while (next->parent())
               next = next->parent();
         }
         next = next->child(0);
      }
      if (next == this)
      {
         next = NULL;
         break;
      }
   }

   return next;
}

const HtNode* HtNode::findForward(int* attIndex, const Variant& value, Qt::MatchFlags flags, int role) const
{
   if (attIndex == NULL)
      return NULL;

   bool  recurse = flags & Qt::MatchRecursive;
   bool  wrap = flags & Qt::MatchWrap;
   int   origIndex = *attIndex;
   int   endIndex = attributeCount();

   const HtNode* next = this;

   forever
   {
      while (*attIndex < endIndex && !next->attribute(*attIndex, role).match(value, flags))
         (*attIndex)++;
      if (*attIndex < endIndex)
         break;
      if (next == this && *attIndex < attributeCount())
      {
         next = NULL;
         break;
      }
      *attIndex = 0;

      next = next->nextItem(recurse, recurse);
      if (next == NULL)
      {
         next = parent();

         if (!wrap || next == NULL)
            break;

         if (recurse)
         {
            while (next->parent())
               next = next->parent();
         }
         next = next->child(0);
      }
      if (next == this)
         endIndex = origIndex;
      else
         endIndex = next->attributeCount();
   }

   return next;
}
#endif

void HtNode::sort(int attIndex, int role, bool descending)
{
   HtSortOrder sortOrder;

   sortOrder.append(HtAttributeOrder(attIndex, role, descending));
   sort(sortOrder);
}

HtNode *HtNode::binaryFindOrInsert(int attIndex, const Variant &value, int role)
{
   HtNode   *node = nullptr;
   int      end = upperBound(attIndex, value, role);

   if (end > 0)
      node = child(end - 1);

   if (node == nullptr || node->attribute(attIndex, role) < value)
   {
      node = insertChild(end);
      node->setAttribute(attIndex, value, role);
   }

   return node;
}

int HtNode::upperBound(int attIndex, const Variant& value, int role, int lowerBound) const
{
   int begin = lowerBound < 0 ? 0 : lowerBound;
   int end = childCount();

   // Return *end* if the *value* is greater or equal to the last element
   if (!(begin < end && value < child(end - 1)->attribute(attIndex, role)))
   {
      return end;
   }

   end--; // *value* is less than the last element

   // If the *value* is less or equal to the first element find and return the first element greater than *value*
   if (!(begin < end && child(begin)->attribute(attIndex, role) < value))
   {
      while (begin < end)
      {
         if (value < child(begin)->attribute(attIndex, role))
         {
            break;
         }
         begin++;
      }

      return begin;
   }

   begin++; // *value* is greater than the first element

   // Binary find the first element greater than *value*
   while (begin < end)
   {
      int index = (begin + end) / 2;

      if (value < child(index)->attribute(attIndex, role))
         end = index;
      else
         begin = index + 1;
   }

   return end;
}

int HtNode::upperBound(const HtSortOrder& sortOrder, HtNode* item) const
{
   if (item == NULL)
      return -1;

   int begin = 0;
   int end = childCount();

   // Binary find the first element following the *item*
   while (begin < end)
   {
      int index = (begin + end) / 2;

      // Use another index if we accidentally hit the search item
      if (child(index) == item)
      {
         if (index == begin)
            break;
         index--;
      }

      if (sortOrder(item, child(index)))
         end = index;
      else
         begin = index + 1;
   }

   return end;
}

int HtNode::lowerBound(int attIndex, const Variant &value, int role, int upperBound) const
{
   int begin = 0;
   int end = childCount();

   if (upperBound >= 0 && upperBound < end)
   {
      end = upperBound;
   }

   // Return *begin* if the *value* is less or equal to the first element
   if (!(begin < end && child(begin)->attribute(attIndex, role) < value))
   {
      return begin;
   }

   begin++; // The first element is less than *value*

   // If the *value* is greater or equal to the last element find and return the first element greater or equal to *value*
   if (!(begin < end && value < child(end - 1)->attribute(attIndex, role)))
   {
      while (begin < end)
      {
         if (child(end - 1)->attribute(attIndex, role) < value)
         {
            break;
         }
         end--;
      }

      return end;
   }

   end--; // *value* is less than the last element

   // Binary find the first element greater or equal to *value*
   while (begin < end)
   {
      int index = (begin + end) / 2;

      if (child(index)->attribute(attIndex, role) < value)
         begin = index + 1;
      else
         end = index;
   }

   return end;
}

int HtNode::lowerBound(const HtSortOrder &sortOrder, HtNode *item) const
{
   if (item == NULL)
      return -1;

   int begin = 0;
   int end = childCount();

   // Binary find the first element after the previous element of *item*
   while (begin < end)
   {
      int index = (begin + end) / 2;

      // Use another index if we accidentally hit the search item
      if (child(index) == item)
      {
         if (index == begin)
            break;
         index--;
      }

      if (sortOrder(child(index), item))
         begin = index + 1;
      else
         end = index;
   }

   return end;
}

void HtNode::save(QTextStream& os, const QLocaleEx& locale, int fieldCount) const
{
   for (int i = 0; i < (fieldCount == -1 ? attributeCount() : fieldCount); i++)
   {
      if (i > 0)
         os << ";";

      const auto value = attribute(i);

      if (value.type() == typeid(QString) || value.type() == typeid(QText))
         os << "\"" << value.toString().replace("\"", "\"\"") << "\"";
      else
         os << value.toString(locale);
   }
#if QT_VERSION < QT_VERSION_CHECK(5,14,0)
   os << endl;
#else
   os << Qt::endl;
#endif

   for (int i = 0; i < childCount(); i++)
   {
      child(i)->save(os, locale, fieldCount);
   }
}

void HtNode::load(QTextStream& is, const QLocaleEx& locale, bool header)
{
   CsvParser csvParser(&is, u';', u'"');

   auto lineValues = csvParser.readLineValues(locale);

   if (header)
   {
      for (auto i = 0; i < lineValues.length(); ++i)
      {
         setAttribute(i, lineValues.at(i));
      }

      lineValues = csvParser.readLineValues(locale);
   }

   while (!lineValues.isEmpty())
   {
      auto node = appendChild();

      for (auto i = 0; i < lineValues.length(); ++i)
      {
         node->setAttribute(i, lineValues.at(i));
      }

      lineValues = csvParser.readLineValues(locale);
   }
}
