#include "stdafx.h"
#include "CPPTokenizer.h"

ProbingContext* createCppContext()
{
   auto context = new ProbingContext;

   context->add(new CppWhiteSpaceContext(true, context));
   context->add(new CppOperatorContext(context)); // Also reads comments!
   context->add(new CppStringContext(context));
   context->add(new NumericContext(context));
   context->add(new CppIdentifierContext(context));
   context->add(new SeparatorContext(context)); // The separator context must always be the last because it returns all characters!

   return context;
}

ProbingContext* createVersatileExpressionContext()
{
   auto context = new ProbingContext;

   context->add(new WhiteSpaceContext(context));
   context->add(new CppOperatorContext(context)); // Also reads comments!
   context->add(new HexadecimalDataContext(context));
   context->add(new StringContext(u'\'', context));
   context->add(new CppStringContext(context));
   context->add(new NumericContext(context));
   context->add(new CppIdentifierContext(context));
   context->add(new CppExplicitIdentifierContext(context));
   context->add(new SeparatorContext(context)); // The separator context must always be the last because it returns all characters!

   return context;
}

const Tokenizer::Context* CppWhiteSpaceContext::readToken(Token& token, TextStreamReader& data) const
{
   QChar prev;
   auto c = data.peekChar();
   const auto bufferStart = data.bufferPos();

   while (!c.isNull())
   {
      if (c == u'\n')
      {
         if (prev != u'\\')
         {
            if (!_includeNewLine)
               break;
            _isNewLine = true;
         }
      }
      else if (!c.isSpace() && c != u'\\')
         break;

      prev = c;
      data.skipChar();
      c = data.peekChar();
   }

   if (bufferStart < data.bufferPos())
      token = new Token::WhiteSpace(data.processedChars(bufferStart));

   if (_isNewLine && c == u'#' && _includeNewLine)
   {
      _isNewLine = false;

      // Switch to Directive context
      if (_directiveContext == nullptr)
         _directiveContext = new CppDirectiveContext(_outerContext);

      // Always return a valid token
      if (!token.isValid())
         return _directiveContext->readToken(token, data);

      return _directiveContext;
   }

   _isNewLine = false;

   return _outerContext;
}

const Tokenizer::Context* CppStringContext::readToken(Token& token, TextStreamReader& data) const
{
   static const QString escapeChars = QLatin1String("ntvbrfa\\'\"0");
   static const QString replaceChars = QLatin1String("\n\t\v\b\r\f\a\\\'\"\0");

   auto index = 0;
   auto c = data.peekChar(index);
   bool isRawString = false;

   if (c == u'R')
   {
      // Raw string
      isRawString = true;
      c = data.peekChar(++index);
   }
   else if (c == u'L')
   {
      // Unicode string
      c = data.peekChar(++index);
   }
   //else if (c == u'@') // C#
   //   data.skipChar(); // The following string is not c-style!

   // R can't be combined wth "'"
   if ((c != u'\'' || isRawString) && c != u'"')
      return _outerContext;

   data.skipChar(index);

   QString delimiter;
   QString value;

   if (isRawString)
   {
      // Assemble the terminating delimiter

      value += c;

      data.skipChar();
      c = data.peekChar();

      delimiter += u')';

      // In c strings can't pass the end of line
      while (!c.isNull() && c != u'\n' && c != u'(' && c != u'"') // Do not eat a terminating line feed
      {
         delimiter += c;

         data.skipChar();
         c = data.peekChar();
      }

      if (c != u'(')
      {
         token = new Token::Text(value);

         if (c == u'"')
            data.skipChar();

         return _outerContext;
      }

      delimiter += u'"';
   }
   else
   {
      delimiter = c;
   }

   data.skipChar();
   c = data.peekChar();

   // Read the string

   value.clear();

   while (!c.isNull())
   {
      // In c strings can't pass the end of line
      if (!isRawString && c == u'\n')
      {
         break; // Do not eat a terminating line feed
      }

      if (c == delimiter[0])
      {
         auto delimIndex = 1;

         while (delimIndex < delimiter.length() && data.peekChar(delimIndex) == delimiter[delimIndex])
         {
            delimIndex++;
         }

         if (delimIndex == delimiter.length())
         {
            data.skipChar(delimIndex);
            break;
         }
      }

      // Handle escape sequences
      if (c == u'\\')
      {
         data.skipChar();
         c = data.peekChar();

         if (c.isNull())
            break;

         const auto i = escapeChars.indexOf(c);

         if (i != -1)
         {
            c = replaceChars[i];
         }
         else if (c == u'\n')
         {
            c = QChar();
         }
      }

      if (!c.isNull())
         value += c;

      data.skipChar();
      c = data.peekChar();
   }

   token = new Token::Text(value);

   return _outerContext;
}

const Tokenizer::Context* CppOperatorContext::readToken(Token& token, TextStreamReader& data) const
{
   auto opIndex = 0u;
   auto opCharIndex = 0;

   while (true)
   {
      const auto opChar = cppOperators[opIndex].op[opCharIndex];

      if (!opChar)
         break;

      if (opChar == data.peekChar().unicode())
      {
         data.skipChar();
         opCharIndex++;
      }
      else
      {
         opIndex++;
      }
   }

   if (opCharIndex == 0)
      return _outerContext;

   if (strcmp(cppOperators[opIndex].op, "//") == 0)
   {
      const auto bufferStart = data.bufferPos();

      auto c = data.peekChar().unicode();
      while (c)
      {
         if (c == u'\n')
            break;

         data.skipChar();
         c = data.peekChar().unicode();
      }

      token = new Token::Comment(data.processedChars(bufferStart));
   }
   else if (strcmp(cppOperators[opIndex].op, "/*") == 0)
   {
      const auto bufferStart = data.bufferPos();

      auto length = 0;
      ushort prev = 0;

      auto c = data.peekChar().unicode();
      while (c)
      {
         data.skipChar();

         if (prev == u'*' && c == u'/')
         {
            length--;
            break;
         }
         length++;
         prev = c;

         c = data.peekChar().unicode();
      }

      token = new Token::Comment(data.processedChars(bufferStart, length));
   }
   else
   {
      token = new CppOperatorToken(cppOperators[opIndex].op, opIndex);
   }

   return _outerContext;
}

int CppIdentifierToken::keyword() const
{
   if (_keyword == -1)
      _keyword = findKeyword(identifier().toLatin1());
   return _keyword;
}

const Tokenizer::Context* CppIdentifierContext::readToken(Token& token, TextStreamReader& data) const
{
   IdentifierContext::readToken(token, data);

   if (token.isValid())
      token = new CppIdentifierToken(token.identifier());

   return _outerContext;
}

const Tokenizer::Context* CppExplicitIdentifierContext::readToken(Token& token, TextStreamReader& data) const
{
   auto c = data.peekChar();

   if (c.isNull())
      return nullptr;

   if (c != u'`')
      return _outerContext;

   data.skipChar();
   c = data.peekChar();

   const auto bufferStart = data.bufferPos();

   while (!c.isNull() && c != u'`' && c != u'\n') // An identifier can't pass the end of line
   {
      data.skipChar();
      c = data.peekChar();
   }

   token = new CppIdentifierToken(data.processedChars(bufferStart));

   if (c == u'`')
       data.skipChar();

   return _outerContext;
}

const Tokenizer::Context* CppDirectiveContext::readToken(Token& token, TextStreamReader& data) const
{
   auto c = data.peekChar();

   if (c != u'#')
      return nullptr;

   data.skipChar();
   c = data.peekChar();

   // Skip white spaces
   while (c == u' ' || c == u'\t')
   {
      data.skipChar();
      c = data.peekChar();
   }

   // Read directive (the following entity can also be a comment!)
   IdentifierContext::readToken(token, data);

   if (token.isValid())
   {
      auto value = token.identifier();
      auto directive = findDirective(value.toLatin1());

      if (directive != -1)
      {
         token = new CppDirectiveToken((CppDirectives)directive);
      }
   }
   else if (data.peekChar(0) == u'/' && data.peekChar(1) == u'/')
   {
      const auto bufferStart = data.bufferPos();

      data.skipChar();
      data.skipChar();

      c = data.peekChar();

      while (!c.isNull())
      {
         if (c == u'\n')
            break;

         data.skipChar();
         c = data.peekChar();
      }

      token = new Token::Comment(data.processedChars(bufferStart + 2));
   }

   return _outerContext;
}
