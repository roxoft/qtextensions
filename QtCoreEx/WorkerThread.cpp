#include "stdafx.h"
#include "WorkerThread.h"
#include <climits>

bool WorkerThread::waitForIdle(int msec)
{
   bool success = false;

   _mutex.lock();
   if (_idle)
      success = true;
   else if (msec)
      success = _idleCond.wait(&_mutex, (msec < 0) ? ULONG_MAX : (unsigned long)msec);
   _mutex.unlock();

   return success;
}

void WorkerThread::startTask(void (QObject::*task)())
{
   if (!this->isRunning())
   {
      _stop = false;
      if (task)
         _task = task;
      _idle = false;
      this->start();
   }
   else if (!_stop)
   {
      _mutex.lock();
      if (!_idle)
         _idleCond.wait(&_mutex);
      if (task)
         _task = task;
      _idle = false;
      _runningCond.wakeOne();
      _mutex.unlock();
   }
}

void WorkerThread::stop()
{
   if (_stop)
      return;

   _mutex.lock();
   if (!_idle)
      _idleCond.wait(&_mutex);
   _idle = true;
   _stop = true;
   _runningCond.wakeOne();
   _mutex.unlock();

   this->wait();
}

void WorkerThread::run()
{
   while (!_stop)
   {
      doTask();

      _mutex.lock();
      _idle = true;
      emit taskFinished();
      _idleCond.wakeOne();
      _runningCond.wait(&_mutex);
      _mutex.unlock();
   }
}
