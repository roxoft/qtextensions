#include "stdafx.h"
#include "CsvParser.h"
#include "QDecimal.h"

static const QString whiteSpaceChars = QLatin1String(" \t");

class CsvValue : public Token::Impl
{
public:
   CsvValue(const QString& text) : _text(text) {}
   ~CsvValue() override = default;

   const QString& text() const { return _text; }
   QString& text() { return _text; }

   Variant value() const override { return _text; }

private:
   QString _text;
};

static Tokenizer::Context* createCsvContext(QChar fieldDelimiter, QChar textDelimiter)
{
   auto context = new ProbingContext;

   context->add(new WhiteSpaceContext(whiteSpaceChars, context));
   if (!textDelimiter.isNull())
      context->add(new StringContext(textDelimiter, context));
   context->add(new CsvValueContext(fieldDelimiter, context));
   context->add(new SeparatorContext(context));

   return context;
}

const Tokenizer::Context* CsvValueContext::readToken(Token& token, TextStreamReader& data) const
{
   const auto bufferStart = data.bufferPos();
   auto bufferEnd = bufferStart;

   auto c = data.peekChar();

   while (!c.isNull())
   {
      if (c == _delimiter || c == u'\n')
         break;

      // c is never white space because white spaces are skipped before calling readToken() and at the end of the loop.
      data.skipChar();
      if (!whiteSpaceChars.contains(c))
         bufferEnd = data.bufferPos();
      c = data.peekChar();
   }

   if (data.bufferPos() > bufferStart)
      token = new CsvValue(data.processedChars(bufferStart, bufferEnd - bufferStart));

   return _outerContext;
}

CsvParser::CsvParser(QTextStream* is, QChar fieldDelimiter, QChar textDelimiter)
   : _tokenizer(is, createCsvContext(fieldDelimiter, textDelimiter))
{
   _tokenizer.setIgnoreWhiteSpace();
   _tokenizer.setIgnoreComments();
}

QStringList CsvParser::readRecord(QString* parsedText)
{
   QStringList record;

   if (parsedText)
      parsedText->clear();

   _tokenizer.setKeepParsedText(parsedText);

   auto token = _tokenizer.next();

   if (!token.isValid())
      return record;

   record.append(QString());

   while (token.isValid())
   {
      if (parsedText)
         parsedText->append(token.parsedText());

      if (const auto separator = token.as<Token::Separator>())
      {
         if (separator->separator() == u'\n')
            break;

         record.append(QString());
      }
      else if (const auto textToken = token.as<Token::Text>())
      {
         record.last() += textToken->text();
      }
      else if (const auto valueToken = token.as<CsvValue>())
      {
         record.last() += valueToken->text();
      }

      token = _tokenizer.next();
   }

   return record;
}

QList<Variant> CsvParser::readLineValues(const QLocaleEx& locale, QString* parsedText)
{
   QList<Variant> lineValues;

   if (parsedText)
      parsedText->clear();

   _tokenizer.setKeepParsedText(parsedText);

   auto token = _tokenizer.next();

   if (!token.isValid())
      return lineValues;

   lineValues.append(Variant());

   while (token.isValid())
   {
      if (parsedText)
         parsedText->append(token.parsedText());

      if (const auto separator = token.as<Token::Separator>())
      {
         if (separator->separator() == u'\n')
            break;

         lineValues.append(Variant());
      }
      else if (const auto textToken = token.as<Token::Text>())
      {
         lineValues.last() = textToken->text();
      }
      else if (const auto valueToken = token.as<CsvValue>())
      {
         const auto text = valueToken->text();
         QDecimal number;

         if (number.fromString(text, locale))
         {
            bool ok;
            lineValues.last() = number.toInt64(&ok);
            if (!ok)
               lineValues.last() = number;
         }
         else
         {
            const auto dateTime = locale.toDateTime(text);

            if (dateTime.isValid())
            {
               lineValues.last() = dateTime;
            }
            else
            {
               const auto date = locale.toDate(text);

               if (date.isValid())
               {
                  lineValues.last() = date;
               }
               else
               {
                  const auto time = locale.toTime(text);

                  if (time.isValid())
                  {
                     lineValues.last() = time;
                  }
                  else
                  {
                     lineValues.last() = text;
                  }
               }
            }
         }
      }

      token = _tokenizer.next();
   }

   return lineValues;
}
