#include "stdafx.h"
#include "QText.h"
#include "QLocaleEx.h"
#if defined(Q_OS_WIN32) || defined(Q_OS_WINCE)
#include <Windows.h>
#else
#include <string>
#include <locale>
#include <stdexcept>
#endif

#if defined(Q_OS_WIN32) || defined(Q_OS_WINCE)
class NlsComparer
{
public:
   NlsComparer(const QString& l, const QString& r)
   {
      left = l.constData();
      llength = l.length();
      right = r.constData();
      rlength = r.length();

#if 1 //_WIN32_WINNT < 0x0600
      /* Use the following code if the program should be running on operating systems prior to Windows Vista (XP for example).
         This is currently a hack!!!
      */
      lcid = 0;

#if QT_VERSION >= QT_VERSION_CHECK(6,6,0)
      if (defaultLocale().language() == QLocale::German && defaultLocale().territory() == QLocale::Germany)
#else
      if (defaultLocale().language() == QLocale::German && defaultLocale().country() == QLocale::Germany)
#endif
         lcid = MAKELCID(MAKELANGID(LANG_GERMAN, SUBLANG_GERMAN), SORT_DEFAULT); // SORT_GERMAN_PHONE_BOOK
      else
         lcid = MAKELCID(MAKELANGID(LANG_ENGLISH, SUBLANG_ENGLISH_US), SORT_DEFAULT);
#else
      /* Use the following code if the program never runs on operating systems prior to Windows Vista
      */
      QString  localeName = defaultLocale().name();

      localeName.replace(2, 1, QLatin1Char('-'));
#endif
   }
   ~NlsComparer() {}

   int compare(Qt::CaseSensitivity cs) const
   {
      int   result = 0;
      DWORD cmpFlags = (cs == Qt::CaseInsensitive) ? NORM_IGNORECASE : 0L;

#if 1 //_WIN32_WINNT < 0x0600
      result = CompareString(lcid, cmpFlags, (wchar_t*)left, llength, (wchar_t*)right, rlength);
#else
      result = CompareStringEx((wchar_t*)localeName.constData(), cmpFlags, (wchar_t*)left, llength, (wchar_t*)right, rlength, NULL, NULL, 0);
#endif

      Q_ASSERT(result > 0);

      return result - 2;
   }

//   bool contains(Qt::CaseSensitivity cs) const
//   {
//      if (rlength == 0)
//         return true;
//
//      if (llength == 0)
//         return false;
//
//#if _WIN32_WINNT < 0x0600
//      /* Use the following code if the program should be running on operating systems prior to Windows Vista (XP for example).
//         This is currently a hack!!!
//      */
//      return wcsstr((wchar_t*)left, (wchar_t*)right);
//#else
//
//      DWORD findFlags = FIND_FROMSTART;
//
//      if (cs == Qt::CaseInsensitive)
//         findFlags |= LINGUISTIC_IGNORECASE;
//
//      int pos = FindNLSString(lcid, findFlags, (wchar_t*)left ,llength, (wchar_t*)right, rlength, NULL);
//
//      return pos >= 0; // If pos == -1 use GetLastError(): ERROR_SUCCESS indicates the string wasn't found, ERROR_INVALID_FLAGS and ERROR_INVALID_PARAMETER indicate an error.
//#endif
//   }

private:
   const QChar *left;
   int         llength;
   const QChar *right;
   int         rlength;
   DWORD       lcid;
};
#endif

int getCorrelationIndicator(const QString& string1, const QString& string2)
{
   auto matchIndicator = 0;

   for (auto i = 0; i < string2.length(); ++i)
   {
      for (auto j = 0; j < string1.length(); ++j)
      {
         auto k = 0;
         auto indicator = 0;

         while (j + k < string1.length() && i + k < string2.length() && string1.at(j + k).toLower() == string2.at(i + k).toLower())
         {
            indicator += 0x10000;
            if (string1.at(j + k) == string2.at(i + k))
               indicator++;
            k++;
         }

         if (indicator && i == 0 && j == 0)
            indicator += 0x8000;

         if (matchIndicator < indicator)
            matchIndicator = indicator;

         if (qMin(string1.length() - j, string2.length() - i) * 0x10000 <= matchIndicator)
            break;
      }

      if (qMin(string1.length(), string2.length() - i) * 0x10000 <= matchIndicator)
         break;
   }

   return matchIndicator;
}

int QText::compare(const QString& str, Qt::CaseSensitivity cs) const
{
   // The localAwareCompare function of QString can't be used because it ignores QLocale settings!!!

   int cmpResult = 0;

#if defined(Q_OS_WIN32) || defined(Q_OS_WINCE)

   NlsComparer comparer(*this, str);

   cmpResult = comparer.compare(cs);

#else

   std::wstring leftString;
   std::wstring rightString;

   if (cs == Qt::CaseInsensitive)
   {
      leftString = this->toLower().toStdWString();
      rightString = str.toLower().toStdWString();
   }
   else
   {
      leftString = this->toStdWString();
      rightString = str.toStdWString();
   }

   QString localeName = defaultLocale().name();

   localeName += QLatin1String(".utf-8");

   try
   {
      std::locale l(localeName.toLatin1().data());

      const std::collate<wchar_t>& fac = std::use_facet<std::collate<wchar_t> >(l);

      cmpResult = fac.compare(leftString.c_str(), leftString.c_str() + leftString.length(), rightString.c_str(), rightString.c_str() + rightString.length());
   }
   catch (std::runtime_error& e)
   {
      qWarning("%s", e.what());
   }

#endif

   return cmpResult;
}

//bool QText::contains(const QText& str, Qt::CaseSensitivity cs) const
//{
//#if defined(Q_OS_WIN32) || defined(Q_OS_WINCE)
//
//   NlsComparer comparer(*this, QStringRef(&str));
//
//   return comparer.contains(cs);
//#else
//   return false; // TODO: Implement functionality
//#endif
//}
