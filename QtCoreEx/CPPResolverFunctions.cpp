#include "stdafx.h"
#include "CPPResolverFunctions.h"
#include "VarTree.h"
#include "CPPExpression.h"

static bool isValidVarName(const QString& name)
{
   if (name.isEmpty())
      return false;

   for (auto&& c : name)
   {
      if (c.isSpace() || c == u'.')
         return false;
   }

   return true;
}

static bool isValidTypeName(const QString& name)
{
   for (auto&& c : name)
   {
      if (c.isSpace())
         return false;
   }

   return true;
}

static VarPtr resolverFunctionItemCount(const IdentifierResolver& ir)
{
   if (ir.varTree()->itemCount() != 1)
      throw QBasicException("Invalid number of arguments");

   return ir.varTree()->newFromValue(ir.varTree()->getItem(0)->itemCount());
}

static VarPtr resolverFunctionTypeName(const IdentifierResolver& ir)
{
   if (ir.varTree()->itemCount() != 1)
      throw QBasicException("Invalid number of arguments");

   return ir.varTree()->newFromValue(ir.varTree()->getItem(0)->typeName());
}

// Adds new names only!
// It is not possible to insert variables at a specific position except the end!
static VarPtr resolverFunctionAddVariable(const IdentifierResolver& ir)
{
   if (ir.varTree()->itemCount() != 3)
      throw QBasicException("Invalid number of arguments");

   auto dict = ir.varTree()->getItem(0);
   const auto name = ir.varTree()->getItem(1)->value().toString(ir.locale());
   auto newItem = ir.varTree()->getItem(2);

   if (name.isEmpty())
      throw QBasicException("Name is empty");

   if (dict->indexOfName(name) != -1)
      throw QBasicException(QString("Name %1 already exists").arg(name));

   if (!isValidVarName(name))
      throw QBasicException(QString("Invalid name %1").arg(name));

   if (newItem)
      newItem = newItem->clone();

   if (newItem)
      dict->setItem(name, newItem);

   return newItem;
}

static VarPtr resolverFunctionAppend(const IdentifierResolver& ir)
{
   if (ir.varTree()->itemCount() != 2)
      throw QBasicException("Invalid number of arguments");

   auto dict = ir.varTree()->getItem(0);
   auto newItem = ir.varTree()->getItem(1);

   if (newItem)
      newItem = newItem->clone();

   if (newItem)
      dict->setItem(dict->itemCount(), newItem);

   return newItem;
}

// Names only!
static VarPtr resolverFunctionDict(const IdentifierResolver& ir)
{
   if (ir.varTree()->itemCount() != 2)
      throw QBasicException("Invalid number of arguments");

   const auto name = ir.varTree()->getItem(1)->value().toString(ir.locale());

   if (name.isEmpty())
      throw QBasicException("Name is empty");

   if (!isValidVarName(name))
      throw QBasicException(QString("Invalid name %1").arg(name));

   return ir.varTree()->getItem(0)->getCollection(name);
}

static VarPtr resolverFunctionSetNames(const IdentifierResolver& ir)
{
   if (ir.varTree()->itemCount() < 2)
      throw QBasicException("Invalid number of arguments");

   const auto dict = ir.varTree()->getItem(0);
   QStringList names;

   for (auto i = 1; i < ir.varTree()->itemCount(); ++i)
   {
      names.append(ir.varTree()->getItem(i)->value().toString(ir.locale()));
      if (!isValidVarName(names.last()))
         throw QBasicException(QString("Invalid name %1").arg(names.last()));
   }

   if (names.count() != dict->itemCount())
      throw QBasicException(QString("The number of names (%1) does not correspond to the number of items (%2)").arg(names.count()).arg(dict->itemCount()));

   if (!names.isEmpty())
      dict->setItems(names, dict->items());

   return dict;
}

static VarPtr resolverFunctionTakeItem(const IdentifierResolver& ir)
{
   if (ir.varTree()->itemCount() != 2)
      throw QBasicException("Invalid number of arguments");

   const auto item = ir.varTree()->getItem(0);
   const auto identifier = ir.varTree()->getItem(1)->value();
   VarPtr removedItem;

   if (identifier.type() == typeid(QString))
   {
      removedItem = item->getItem(identifier.toString(ir.locale()));
      if (!removedItem)
         throw QBasicException(QString("Item %1 not found").arg(identifier.toString(ir.locale())));
      item->removeItem(identifier.toString(ir.locale()));
   }
   else
   {
      removedItem = item->getItem(identifier.toInt());
      if (!removedItem)
         throw QBasicException(QString("Index %1 out of range").arg(identifier.toInt()));
      item->removeItem(identifier.toInt());
   }

   return removedItem;
}

static VarPtr resolverFunctionNew(const IdentifierResolver& ir)
{
   if (ir.varTree()->itemCount() > 1)
      throw QBasicException("Invalid number of arguments");

   QString typeName;

   if (ir.varTree()->itemCount() == 1)
      typeName = ir.varTree()->getItem(0)->value().toString(ir.locale());

   if (!isValidTypeName(typeName))
      throw QBasicException(QString("Invalid type name %1").arg(typeName));

   // Decide if its a value type or a collection type

   Variant value;

   if (typeName == "Bool")
      value = false;
   else if (typeName == "Date")
      value = QDateTimeEx();
   else if (typeName == "Decimal")
      value = QDecimal();
   else if (typeName == "Integer")
      value = 0LL;
   else if (typeName == "String")
      value = QString();
   else if (typeName == "Binary")
      value = QByteArray();

   if (value.isNull())
      return ir.varTree()->newCollection(typeName);
   return ir.varTree()->newFromValue(value, typeName);
}

static VarPtr resolverFunctionToString(const IdentifierResolver& ir)
{
   if (ir.varTree()->itemCount() < 1 || ir.varTree()->itemCount() > 3)
      throw QBasicException("Invalid number of arguments");

   const auto value = ir.varTree()->getItem(0)->value();

   QString localeName;
   if (ir.varTree()->itemCount() > 1)
      localeName = ir.varTree()->getItem(1)->value().toString(ir.locale());

   QString result;

   // toString is type dependent
   const std::type_info& type = value.type();

   if (type == typeid(int) || type == typeid(unsigned int) || type == typeid(long long int) || type == typeid(unsigned long long int) || type == typeid(QDecimal))
   {
      // Numeric values
      const auto number = value.toDecimal();

      QLocaleEx locale;

      if (localeName.isEmpty())
      {
         locale = ir.locale();
      }
      else
      {
         locale = QLocaleEx(localeName);
      }

      QDecimal::GroupSeparatorMode groupSeparatorMode = QDecimal::GS_Auto;

      if (ir.varTree()->itemCount() > 2)
         groupSeparatorMode = ir.varTree()->getItem(2)->value().toBool() ? QDecimal::GS_UseGroupSeparator : QDecimal::GS_OmitGroupSeparator;

      result = number.toString(groupSeparatorMode, locale);
   }
   else if (type == typeid(QDateTime) || type == typeid(QDate) || type == typeid(QTime) || type == typeid(QDateTimeEx) || type == typeid(QDateEx) || type == typeid(QTimeEx))
   {
      // Date values
      const auto dateTime = value.toDateTimeEx();

      QLocaleEx locale;

      if (localeName.isEmpty())
      {
         locale = ir.locale();
      }
      else if (ir.varTree()->itemCount() > 2)
      {
         const auto longFormat = ir.varTree()->getItem(2)->value().toBool();

         locale = QLocaleEx(localeName, longFormat ? DateTimePart::Second : DateTimePart::Minute, longFormat ? QLocale::LongFormat : QLocale::ShortFormat);
      }
      else
      {
         locale = QLocaleEx(localeName);
      }

      result = locale.toString(dateTime);
   }
   else
   {
      // String values
      QLocaleEx locale;

      if (localeName.isEmpty())
      {
         locale = ir.locale();
      }
      else
      {
         locale = QLocaleEx(localeName);
      }

      result = value.toString(locale);
   }

   return ir.varTree()->newFromValue(result);
}

static VarPtr resolverFunctionToday(const IdentifierResolver& ir)
{
   if (ir.varTree()->itemCount() != 0)
      throw QBasicException("Invalid number of arguments");

   return ir.varTree()->newFromValue(QDateTimeEx::currentDate());
}

static VarPtr resolverFunctionNow(const IdentifierResolver& ir)
{
   if (ir.varTree()->itemCount() != 0)
      throw QBasicException("Invalid number of arguments");

   return ir.varTree()->newFromValue(QDateTimeEx::currentDateTime());
}

static VarPtr resolverFunctionCreateSequence(const IdentifierResolver& ir)
{
   if (ir.varTree()->itemCount() != 2 && ir.varTree()->itemCount() != 3)
      throw QBasicException("Invalid number of arguments");

   auto start = ir.varTree()->getItem(0)->value();
   auto count = qMax(0, ir.varTree()->getItem(1)->value().toInt());
   VarPtr step;

   if (ir.varTree()->itemCount() == 3)
      step = ir.varTree()->getItem(2);

   auto result = ir.varTree()->newCollection();

   while (count--)
   {
      result->setItem(result->itemCount(), ir.varTree()->newFromValue(start));
      if (count)
      {
         // Modify start
         if (!step)
         {
            start = start.toInt() + 1;
         }
         else if (step->basicType() == IVariable::BasicType::Value)
         {
            start = start.toInt() + step->value().toInt();
         }
         else if (step->function())
         {
            auto pars = ir.varTree()->newCollection();
            pars->setItem(0, ir.varTree()->newFromValue(start));
            start = step->function()->resolve(pars, ir.functionMap())->value();
         }
         else if (step->itemCount() && step->getItem(0)->function())
         {
            auto pars = ir.varTree()->newCollection();
            pars->setItem(0, ir.varTree()->newFromValue(start));
            for (auto i = 1; i < step->itemCount(); ++i)
               pars->setItem(i, step->getItem(i));
            start = step->getItem(0)->function()->resolve(pars, ir.functionMap())->value();
         }
      }
   }

   return result;
}

static VarPtr resolverFunctionLength(const IdentifierResolver& ir)
{
   if (ir.varTree()->itemCount() != 1)
      throw QBasicException("Invalid number of arguments");

   const auto item = ir.varTree()->getItem(0);

   if (item->basicType() == IVariable::BasicType::Value)
   {
      const auto text = item->value().toString(ir.locale());
      return ir.varTree()->newFromValue(text.length());
   }

   return ir.varTree()->newFromValue(item->itemCount());
}

static VarPtr resolverFunctionMid(const IdentifierResolver& ir)
{
   if (ir.varTree()->itemCount() != 2 && ir.varTree()->itemCount() != 3)
      throw QBasicException("Invalid number of arguments");

   const auto item = ir.varTree()->getItem(0);
   const auto start = ir.varTree()->getItem(1)->value().toInt();
   const auto length = ir.varTree()->itemCount() == 3 ? ir.varTree()->getItem(2)->value().toInt() : -1;

   if (item->basicType() == IVariable::BasicType::Value)
   {
      const auto text = item->value().toString(ir.locale());
      return ir.varTree()->newFromValue(text.mid(start, length));
   }

   auto result = ir.varTree()->newCollection();

   result->setItems(QStringList(), item->items().mid(start, length));

   return result;
}

static VarPtr resolverFunctionStartsWith(const IdentifierResolver& ir)
{
   if (ir.varTree()->itemCount() != 2 && ir.varTree()->itemCount() != 3)
      throw QBasicException("Invalid number of arguments");

   const auto item = ir.varTree()->getItem(0);
   const auto cmp = ir.varTree()->getItem(1);
   const auto isCaseInsensitive = ir.varTree()->itemCount() == 3 ? ir.varTree()->getItem(2)->value().toBool() : false;

   if (item->basicType() == IVariable::BasicType::Collection)
   {
      QList<VarPtr> cmpList;

      if (cmp->basicType() == IVariable::BasicType::Collection)
         cmpList = cmp->items();
      else
         cmpList.append(cmp);

      auto i = 0;
      for (; i < cmpList.count(); ++i)
      {
         const auto left = item->getItem(i);
         const auto right = cmpList.at(i);

         if (!left)
            break;

         if (left->basicType() == IVariable::BasicType::Value)
         {
            if (left->value().type() == typeid(QString))
            {
               if (left->value().toString(ir.locale()).compare(right->value().toString(ir.locale()), isCaseInsensitive ? Qt::CaseInsensitive : Qt::CaseSensitive) != 0)
                  break;
            }
            else if (left->value().type() == typeid(QText))
            {
               if (left->value().toText().compare(right->value().toText(), isCaseInsensitive ? Qt::CaseInsensitive : Qt::CaseSensitive) != 0)
                  break;
            }
            else if (left->value() != right->value())
               break;
         }
         else if (left != right)
            break;
      }

      return ir.varTree()->newFromValue(i == cmpList.count());
   }

   return ir.varTree()->newFromValue(item->value().toString(ir.locale()).startsWith(cmp->value().toString(ir.locale()), isCaseInsensitive ? Qt::CaseInsensitive : Qt::CaseSensitive));
}

static VarPtr resolverFunctionEndsWith(const IdentifierResolver& ir)
{
   if (ir.varTree()->itemCount() != 2 && ir.varTree()->itemCount() != 3)
      throw QBasicException("Invalid number of arguments");

   const auto item = ir.varTree()->getItem(0);
   const auto cmp = ir.varTree()->getItem(1);
   const auto isCaseInsensitive = ir.varTree()->itemCount() == 3 ? ir.varTree()->getItem(2)->value().toBool() : false;

   if (item->basicType() == IVariable::BasicType::Collection)
   {
      QList<VarPtr> cmpList;

      if (cmp->basicType() == IVariable::BasicType::Collection)
         cmpList = cmp->items();
      else
         cmpList.append(cmp);

      auto i = 0;
      for (; i < cmpList.count(); ++i)
      {
         const auto left = item->getItem(item->itemCount() - i - 1);
         const auto right = cmpList.at(cmpList.count() - i - 1);

         if (!left)
            break;

         if (left->basicType() == IVariable::BasicType::Value)
         {
            if (left->value().type() == typeid(QString))
            {
               if (left->value().toString(ir.locale()).compare(right->value().toString(ir.locale()), isCaseInsensitive ? Qt::CaseInsensitive : Qt::CaseSensitive) != 0)
                  break;
            }
            else if (left->value().type() == typeid(QText))
            {
               if (left->value().toText().compare(right->value().toText(), isCaseInsensitive ? Qt::CaseInsensitive : Qt::CaseSensitive) != 0)
                  break;
            }
            else if (left->value() != right->value())
               break;
         }
         else if (left != right)
            break;
      }

      return ir.varTree()->newFromValue(i == cmpList.count());
   }

   return ir.varTree()->newFromValue(item->value().toString(ir.locale()).endsWith(cmp->value().toString(ir.locale()), isCaseInsensitive ? Qt::CaseInsensitive : Qt::CaseSensitive));
}

static VarPtr resolverFunctionContains(const IdentifierResolver& ir)
{
   if (ir.varTree()->itemCount() != 2 && ir.varTree()->itemCount() != 3)
      throw QBasicException("Invalid number of arguments");

   const auto item = ir.varTree()->getItem(0);
   const auto cmp = ir.varTree()->getItem(1);
   const auto isCaseInsensitive = ir.varTree()->itemCount() == 3 ? ir.varTree()->getItem(2)->value().toBool() : false;

   if (item->basicType() == IVariable::BasicType::Collection)
   {
      QList<VarPtr> cmpList;

      if (cmp->basicType() == IVariable::BasicType::Collection)
         cmpList = cmp->items();
      else
         cmpList.append(cmp);

      auto j = 0;
      for (; j <= item->itemCount() - cmpList.count(); ++j)
      {
         auto i = 0;
         for (; i < cmpList.count(); ++i)
         {
            const auto left = item->getItem(j + i);
            const auto right = cmpList.at(i);

            if (!left)
               break;

            if (left->basicType() == IVariable::BasicType::Value)
            {
               if (left->value().type() == typeid(QString))
               {
                  if (left->value().toString(ir.locale()).compare(right->value().toString(ir.locale()), isCaseInsensitive ? Qt::CaseInsensitive : Qt::CaseSensitive) != 0)
                     break;
               }
               else if (left->value().type() == typeid(QText))
               {
                  if (left->value().toText().compare(right->value().toText(), isCaseInsensitive ? Qt::CaseInsensitive : Qt::CaseSensitive) != 0)
                     break;
               }
               else if (left->value() != right->value())
                  break;
            }
            else if (left != right)
               break;
         }

         if (i == cmpList.count())
            break;
      }

      return ir.varTree()->newFromValue(j < item->itemCount());
   }

   return ir.varTree()->newFromValue(item->value().toString(ir.locale()).contains(cmp->value().toString(ir.locale()), isCaseInsensitive ? Qt::CaseInsensitive : Qt::CaseSensitive));
}

static VarPtr resolverFunctionDistinct(const IdentifierResolver& ir)
{
   if (ir.varTree()->itemCount() != 1 && ir.varTree()->itemCount() != 2)
      throw QBasicException("Invalid number of arguments");

   const auto list = ir.varTree()->getItem(0);
   const auto isCaseInsensitive = ir.varTree()->itemCount() == 2 ? ir.varTree()->getItem(1)->value().toBool() : false;
   auto result = ir.varTree()->newCollection();

   for (auto i = 0; i < list->itemCount(); ++i)
   {
      const auto source = list->getItem(i);
      auto j = 0;

      for (; j < result->itemCount(); ++j)
      {
         const auto dest = result->getItem(j);

         if (source->basicType() == IVariable::BasicType::Value)
         {
            if (source->value().type() == typeid(QString))
            {
               if (source->value().toString(ir.locale()).compare(dest->value().toString(ir.locale()), isCaseInsensitive ? Qt::CaseInsensitive : Qt::CaseSensitive) == 0)
                  break;
            }
            else if (source->value().type() == typeid(QText))
            {
               if (source->value().toText().compare(dest->value().toText(), isCaseInsensitive ? Qt::CaseInsensitive : Qt::CaseSensitive) == 0)
                  break;
            }
            else if (source->value() == dest->value())
               break;
         }
         else if (source == dest)
            break;
      }

      if (j == result->itemCount())
      {
         result->setItem(j, source);
      }
   }

   return result;
}

static VarPtr resolverFunctionWhere(const IdentifierResolver& ir)
{
   if (ir.varTree()->itemCount() != 2)
      throw QBasicException("Invalid number of arguments");

   const auto list = ir.varTree()->getItem(0);
   const auto predicate = ir.varTree()->getItem(1)->function();

   if (!predicate)
      throw QBasicException("Parameter has no function");

   auto result = ir.varTree()->newCollection();
   auto parameter = ir.varTree()->newCollection();

   for (auto i = 0; i < list->itemCount(); ++i)
   {
      parameter->setItem(0, list->getItem(i));
      if (predicate->resolve(parameter, ir.functionMap())->value().toBool())
         result->setItem(result->itemCount(), list->getItem(i));
   }

   return result;
}

static VarPtr resolverFunctionReplace(const IdentifierResolver& ir)
{
   if (ir.varTree()->itemCount() < 2 || ir.varTree()->itemCount() > 4)
      throw QBasicException("Invalid number of arguments");

   auto text = ir.varTree()->getItem(0)->value().toString(ir.locale());
   const auto searchString = ir.varTree()->getItem(1)->value().toString(ir.locale());
   const auto replaceString = ir.varTree()->itemCount() > 2 ? ir.varTree()->getItem(2)->value().toString(ir.locale()) : QString();
   const auto isCaseInsensitive = ir.varTree()->itemCount() > 3 ? ir.varTree()->getItem(3)->value().toBool() : false;

   text.replace(searchString, replaceString, isCaseInsensitive ? Qt::CaseInsensitive : Qt::CaseSensitive);

   return ir.varTree()->newFromValue(text);
}

static VarPtr resolverFunctionToUpper(const IdentifierResolver& ir)
{
   if (ir.varTree()->itemCount() != 1)
      throw QBasicException("Invalid number of arguments");

   const auto text = ir.varTree()->getItem(0)->value().toString(ir.locale());

   return ir.varTree()->newFromValue(text.toUpper());
}

static VarPtr resolverFunctionToLower(const IdentifierResolver& ir)
{
   if (ir.varTree()->itemCount() != 1)
      throw QBasicException("Invalid number of arguments");

   const auto text = ir.varTree()->getItem(0)->value().toString(ir.locale());

   return ir.varTree()->newFromValue(text.toLower());
}

static VarPtr resolverFunctionTrim(const IdentifierResolver& ir)
{
   if (ir.varTree()->itemCount() != 1)
      throw QBasicException("Invalid number of arguments");

   const auto text = ir.varTree()->getItem(0)->value().toString(ir.locale());

   return ir.varTree()->newFromValue(text.trimmed());
}

static VarPtr resolverFunctionLeftJustified(const IdentifierResolver& ir)
{
   if (ir.varTree()->itemCount() < 2 || ir.varTree()->itemCount() > 4)
      throw QBasicException("Invalid number of arguments");

   const auto text = ir.varTree()->getItem(0)->value().toString(ir.locale());
   const auto width = ir.varTree()->getItem(1)->value().toInt();
   auto fill = ir.varTree()->itemCount() > 2 ? ir.varTree()->getItem(2)->value().toString(ir.locale()) : QString();
   const auto truncate = ir.varTree()->itemCount() > 3 ? ir.varTree()->getItem(3)->value().toBool() : false;

   if (fill.isEmpty())
      fill = " ";

   return ir.varTree()->newFromValue(text.leftJustified(width, fill[0], truncate));
}

static VarPtr resolverFunctionRightJustified(const IdentifierResolver& ir)
{
   if (ir.varTree()->itemCount() < 2 || ir.varTree()->itemCount() > 4)
      throw QBasicException("Invalid number of arguments");

   const auto text = ir.varTree()->getItem(0)->value().toString(ir.locale());
   const auto width = ir.varTree()->getItem(1)->value().toInt();
   auto fill = ir.varTree()->itemCount() > 2 ? ir.varTree()->getItem(2)->value().toString(ir.locale()) : QString();
   const auto truncate = ir.varTree()->itemCount() > 3 ? ir.varTree()->getItem(3)->value().toBool() : false;

   if (fill.isEmpty())
      fill = " ";

   return ir.varTree()->newFromValue(text.rightJustified(width, fill[0], truncate));
}

static VarPtr resolverFunctionToDecimal(const IdentifierResolver& ir)
{
   if (ir.varTree()->itemCount() != 1 && ir.varTree()->itemCount() != 2)
      throw QBasicException("Invalid number of arguments");

   const auto value = ir.varTree()->getItem(0)->value();

   QString localeName;
   if (ir.varTree()->itemCount() > 1)
      localeName = ir.varTree()->getItem(1)->value().toString(ir.locale());

   QLocaleEx locale;

   if (localeName.isEmpty())
   {
      locale = ir.locale();
   }
   else
   {
      locale = QLocaleEx(localeName);
   }

   auto ok = false;
   const auto result = value.toDecimal(&ok, locale);

   if (!ok)
      throw QBasicException(QString("Unable to convert from %1 to QDecimal").arg(plain_type_name(value.type())));

   return ir.varTree()->newFromValue(result);
}

static VarPtr resolverFunctionToDate(const IdentifierResolver& ir)
{
   if (ir.varTree()->itemCount() < 1 || ir.varTree()->itemCount() > 3)
      throw QBasicException("Invalid number of arguments");

   const auto value = ir.varTree()->getItem(0)->value();

   QString localeName;
   if (ir.varTree()->itemCount() > 1)
      localeName = ir.varTree()->getItem(1)->value().toString(ir.locale());

   QLocaleEx locale;

   if (localeName.isEmpty())
   {
      locale = ir.locale();
   }
   else if (ir.varTree()->itemCount() > 2)
   {
      const auto longFormat = ir.varTree()->getItem(2)->value().toBool();

      locale = QLocaleEx(localeName, longFormat ? DateTimePart::Second : DateTimePart::Minute, longFormat ? QLocale::LongFormat : QLocale::ShortFormat);
   }
   else
   {
      locale = QLocaleEx(localeName);
   }

   auto ok = false;
   const auto result = value.toDateEx(&ok, locale);

   if (!ok)
      throw QBasicException(QString("Unable to convert from %1 to QDateEx").arg(plain_type_name(value.type())));

   return ir.varTree()->newFromValue(result);
}

static VarPtr resolverFunctionToInteger(const IdentifierResolver& ir)
{
   if (ir.varTree()->itemCount() != 1 && ir.varTree()->itemCount() != 2)
      throw QBasicException("Invalid number of arguments");

   const auto value = ir.varTree()->getItem(0)->value();

   QString localeName;
   if (ir.varTree()->itemCount() > 1)
      localeName = ir.varTree()->getItem(1)->value().toString(ir.locale());

   QLocaleEx locale;

   if (localeName.isEmpty())
   {
      locale = ir.locale();
   }
   else
   {
      locale = QLocaleEx(localeName);
   }

   auto ok = false;
   const auto result = value.toInt64(&ok, locale);

   if (!ok)
      throw QBasicException(QString("Unable to convert from %1 to int64").arg(plain_type_name(value.type())));

   return ir.varTree()->newFromValue(result);
}

static VarPtr resolverFunctionRound(const IdentifierResolver& ir)
{
   if (ir.varTree()->itemCount() != 1 && ir.varTree()->itemCount() != 2)
      throw QBasicException("Invalid number of arguments");

   const auto value = ir.varTree()->getItem(0)->value();
   const auto digits = ir.varTree()->itemCount() == 2 ? ir.varTree()->getItem(1)->value().toInt() : 0;

   auto ok = false;
   const auto result = value.toDecimal(&ok, ir.locale()).rounded(digits);

   if (!ok)
      throw QBasicException(QString("Unable to convert from %1 to QDecimal").arg(plain_type_name(value.type())));

   return ir.varTree()->newFromValue(result);
}

static VarPtr resolverFunctionMax(const IdentifierResolver& ir)
{
   if (ir.varTree()->itemCount() != 2)
      throw QBasicException("Invalid number of arguments");

   const auto leftValue = ir.varTree()->getItem(0)->value();
   const auto rightValue = ir.varTree()->getItem(1)->value();

   return ir.varTree()->newFromValue(qMax(leftValue, rightValue));
}

static VarPtr resolverFunctionMin(const IdentifierResolver& ir)
{
   if (ir.varTree()->itemCount() != 2)
      throw QBasicException("Invalid number of arguments");

   const auto leftValue = ir.varTree()->getItem(0)->value();
   const auto rightValue = ir.varTree()->getItem(1)->value();

   return ir.varTree()->newFromValue(qMin(leftValue, rightValue));
}

static VarPtr resolverFunctionIf(const IdentifierResolver& ir)
{
   if (ir.varTree()->itemCount() != 3)
      throw QBasicException("Invalid number of arguments");

   return ir.varTree()->getItem(0)->value().toBool() ? ir.varTree()->getItem(1) : ir.varTree()->getItem(2);
}

ResolverFunctionMap createCppResolverFunctions()
{
   ResolverFunctionMap resolverFunctions;

   addResolverFunction(resolverFunctions, "varCount", resolverFunctionItemCount, ExpressionParser::tr("Integer"), { { "Dictionary|List", "" } },
      ExpressionParser::tr("Returns the number of elements in the collection."));

   addResolverFunction(resolverFunctions, "varType", resolverFunctionTypeName, ExpressionParser::tr("String"), { { "Dictionary|List|Value|Function", "" } },
      ExpressionParser::tr("Returns the type name of the variable."));

   addResolverFunction(resolverFunctions, "getDict", resolverFunctionDict, ExpressionParser::tr("Dictionary"), { { "Dictionary", "" }, { "String", "Name"} },
      ExpressionParser::tr("Returns the dictionary at [Name]. A new dictionary is inserted if [Name] doesn't exist."));

   addResolverFunction(resolverFunctions, "addVar", resolverFunctionAddVariable, ExpressionParser::tr("Variable"), { { "Dictionary", "" }, { "String", "Name" }, { "Variable", "Variable" } },
      ExpressionParser::tr("Adds [Name] with [Variable] to the dictionary and returns [Variable]."));

   addResolverFunction(resolverFunctions, "append", resolverFunctionAppend, ExpressionParser::tr("Variable"), { { "List", "" }, { "Variable", "Variable" } },
      ExpressionParser::tr("Adds [Variable] to the list and returns [Variable]."));

   addResolverFunction(resolverFunctions, "setVarNames", resolverFunctionSetNames, ExpressionParser::tr("Dictionary"), { { "List", "" }, { "List", "Names" } },
      ExpressionParser::tr("Sets the names for the variables in the list to [Names], thus converting the list into a dictionary!"));

   addResolverFunction(resolverFunctions, "takeVar", resolverFunctionTakeItem, ExpressionParser::tr("Variable"), { { "Dictionary|List", "" }, { "String|Integer", "Name|Index"} },
      ExpressionParser::tr("Removes the variable at [Name] or [Index] and returns the removed variable."));

   addResolverFunction(resolverFunctions, "new", resolverFunctionNew, ExpressionParser::tr("Variable"), { { "String", "TypeName" } },
      ExpressionParser::tr("Creates a new element of type [TypeName] and returns it."));

   addResolverFunction(resolverFunctions, "toString", resolverFunctionToString, ExpressionParser::tr("String"), { { "Value", "" }, { "String", "?Locale" },{ "Value", "?Option" } },
      ExpressionParser::tr(R"(Converts a value into a string using [Locale] or the default locale.
For numeric values [Option] controls the insertion of group separators (true always adds group separators, false always omits them and without [Option] group separators are inserted if the number has fractional digits).
For date and time values [Option] specifies if a short format (false) or a long format (true) or the default (no [Option]) should be used.
For other values [Option] has no meaning.)"));

   addResolverFunction(resolverFunctions, "today", resolverFunctionToday, ExpressionParser::tr("Date"), {},
      ExpressionParser::tr("Returns the current date."));

   addResolverFunction(resolverFunctions, "now", resolverFunctionNow, ExpressionParser::tr("DateTime"), {},
      ExpressionParser::tr("Returns the current date and time."));

   addResolverFunction(resolverFunctions, "createSequence", resolverFunctionCreateSequence, ExpressionParser::tr("List"), { { "Value", "Start" }, { "Integer", "Count" }, { "Integer|Function|List", "?Step" } },
      ExpressionParser::tr(R"(Creates a sequence of [Count] values beginning with [Start] modified by [Step].
If [Step] is omitted, the sequence values are incremented by the integer value 1.
If [Step] is a value, the sequence values are incremented by the integer value [Step].
If [Step] is a function, it is called with the current value and must return the next value.
If [Step] is a list the first element must be a function which is called to get the next value. The other list elements are passed as additional parameters.)"));

   addResolverFunction(resolverFunctions, "length", resolverFunctionLength, ExpressionParser::tr("Integer"), { { "Dictionary|List|String", "" } },
      ExpressionParser::tr("Returns the length of the variable."));

   addResolverFunction(resolverFunctions, "mid", resolverFunctionMid, ExpressionParser::tr("List|String"), { { "Dictionary|List|String", "" }, { "Integer", "Start" }, { "Integer", "?Length" } },
      ExpressionParser::tr("Returns a list containing up to [Length] elements beginning at [Start]. If [Length] is omitted the rest of the string is returned."));

   addResolverFunction(resolverFunctions, "startsWith", resolverFunctionStartsWith, ExpressionParser::tr("Boolean"), { { "List|String", "" }, { "List|String", "Compare" }, { "Boolean", "?CaseInsensitive" } },
      ExpressionParser::tr("Returns true if the list or the string starts with [Compare]. By default the comparison is case sensitive."));

   addResolverFunction(resolverFunctions, "endsWith", resolverFunctionEndsWith, ExpressionParser::tr("Boolean"), { { "List|String", "" }, { "List|String", "Compare" }, { "Boolean", "?CaseInsensitive" } },
      ExpressionParser::tr("Returns true if the list or the string ends with [Compare]. By default the comparison is case sensitive."));

   addResolverFunction(resolverFunctions, "contains", resolverFunctionContains, ExpressionParser::tr("Boolean"), { { "List|String", "" }, { "List|String", "Compare" }, { "Boolean", "?CaseInsensitive" } },
      ExpressionParser::tr("Returns true if the list or the string contains [Compare]. By default the comparison is case sensitive."));

   addResolverFunction(resolverFunctions, "distinct", resolverFunctionDistinct, ExpressionParser::tr("List"), { { "List", "" }, { "Boolean", "?CaseInsensitive" } },
      ExpressionParser::tr("Converts a list into a list of unique elements. By default the comparison is case sensitive."));

   addResolverFunction(resolverFunctions, "where", resolverFunctionWhere, ExpressionParser::tr("List"), { { "List", "" }, { "Function", "Predicate" } },
      ExpressionParser::tr("Returns a list containing all elements from [List] where the [Predicate] returns true. The [Predicate] gets each [List] element as argument."));

   addResolverFunction(resolverFunctions, "replace", resolverFunctionReplace, ExpressionParser::tr("String"), { { "String", "" }, { "String", "Before" }, { "String", "?After" }, { "Boolean", "?CaseInsensitive" } },
      ExpressionParser::tr("Returns a string where all occurrences of [Before] are replaced with [After]. If [After] is omitted all occurrences of [Before] are removed. By default the comparison is case sensitive."));

   addResolverFunction(resolverFunctions, "toUpper", resolverFunctionToUpper, ExpressionParser::tr("String"), { { "String", "" } },
      ExpressionParser::tr("Returns a string where all letters are converted to upper case."));

   addResolverFunction(resolverFunctions, "toLower", resolverFunctionToLower, ExpressionParser::tr("String"), { { "String", "" } },
      ExpressionParser::tr("Returns a string where all letters are converted to lower case."));

   addResolverFunction(resolverFunctions, "trim", resolverFunctionTrim, ExpressionParser::tr("String"), { { "String", "" } },
      ExpressionParser::tr("Returns a string where all white spaces are removed from the beginning and the end of the string."));

   addResolverFunction(resolverFunctions, "leftJustified", resolverFunctionLeftJustified, ExpressionParser::tr("String"), { { "String", "" }, { "Integer", "Width" }, { "String", "?Fill" }, { "Boolean", "?Truncate" } },
      ExpressionParser::tr("Returns a string of [Width] length that contains the string padded by the [Fill] character. If the string is longer than [Width] and [Truncate] is false or omitted, the unmodified string is returned. Is [Truncate] true the string is truncated to [Width] length. By default [Fill] is a space."));

   addResolverFunction(resolverFunctions, "rightJustified", resolverFunctionRightJustified, ExpressionParser::tr("String"), { { "String", "" }, { "Integer", "Width" }, { "String", "?Fill" }, { "Boolean", "?Truncate" } },
      ExpressionParser::tr("Returns a string of [Width] length that contains the [Fill] character followed by the string. If the string is longer than [Width] and [Truncate] is false or omitted, the unmodified string is returned. Is [Truncate] true the string is truncated to [Width] length. By default [Fill] is a space."));

   addResolverFunction(resolverFunctions, "toDecimal", resolverFunctionToDecimal, ExpressionParser::tr("Decimal"), { { "String", "" }, { "String", "?Locale" } },
      ExpressionParser::tr("Converts a string into a decimal using [Locale] or the default locale."));

   addResolverFunction(resolverFunctions, "toNumber", resolverFunctionToDecimal, ExpressionParser::tr("Decimal"), { { "String", "" }, { "String", "?Locale" } },
      ExpressionParser::tr("Synonym for toDecimal."));

   addResolverFunction(resolverFunctions, "toDate", resolverFunctionToDate, ExpressionParser::tr("Decimal"), { { "String", "" }, { "String", "?Locale" }, { "Boolean", "?UseLongFormat" } },
      ExpressionParser::tr("Converts a string into a date using [Locale] or the default locale. If [UseLongFormat] is omitted the default format is used."));

   addResolverFunction(resolverFunctions, "toInteger", resolverFunctionToInteger, ExpressionParser::tr("Integer"), { { "String", "" }, { "String", "?Locale" } },
      ExpressionParser::tr("Converts a string into an integer using [Locale] or the default locale."));

   addResolverFunction(resolverFunctions, "round", resolverFunctionRound, ExpressionParser::tr("Decimal"), { { "Decimal", "" }, { "Integer", "?Digits" } },
      ExpressionParser::tr("Returns a decimal rounded to the number of fractional [Digits]. By default [Digits] is 0."));

   addResolverFunction(resolverFunctions, "max", resolverFunctionMax, ExpressionParser::tr("Value"), { { "Value", "First" }, { "Value", "Second" } },
      ExpressionParser::tr("Returns the greater of the two values."));

   addResolverFunction(resolverFunctions, "min", resolverFunctionMin, ExpressionParser::tr("Value"), { { "Value", "First" }, { "Value", "Second" } },
      ExpressionParser::tr("Returns the smaller of the two values."));

   addResolverFunction(resolverFunctions, "if", resolverFunctionIf, ExpressionParser::tr("Variable"), { { "Boolean", "Condition" }, { "Variable", "TruePart" }, { "Variable", "FalsePart" } },
      ExpressionParser::tr("If [Condition] is true the [TruePart] is returned, otherwise the [FalsePart]. All parameters are evaluated before returning one of them! Use the ternary ?: operator to work with side effects."));

   addResolverFunction(resolverFunctions, "cond", resolverFunctionIf, ExpressionParser::tr("Variable"), { { "Boolean", "Condition" }, { "Variable", "TruePart" }, { "Variable", "FalsePart" } },
      ExpressionParser::tr("If [Condition] is true the [TruePart] is returned, otherwise the [FalsePart]. All parameters are evaluated before returning one of them! Use the ternary ?: operator to work with side effects."));

   return resolverFunctions;
}
