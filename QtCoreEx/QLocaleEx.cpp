#include "stdafx.h"
#include "QLocaleEx.h"
#include "Variant.h"
#include <QVariant>
#if QT_VERSION_MAJOR < 6
#include <QTextCodec>
#else
#include <QStringConverter>
#endif
#include "QDateTimeEx.h"
#include "TemplateConversions.h"

static QList<QLocaleEx> _locales;

#if QT_VERSION_MAJOR < 6
static inline QChar toChar(const QChar& c)
{
   return c;
}
#else
static inline QChar toChar(const QString& s)
{
   return s.isEmpty() ? QChar() : s.at(0);
}
#endif

QLocaleEx::QLocaleEx()
{
   if (_locales.isEmpty())
   {
      initFromLocale(LongFormat);
   }
   else
   {
      *this = _locales.first();
   }
}

QLocaleEx::QLocaleEx(const QString &name, DateTimePart timePrecision, FormatType formatType) : QLocale(name), _timePrecision(timePrecision)
{
   initFromLocale(formatType);
}

QLocaleEx::QLocaleEx(Language language, Country country, DateTimePart timePrecision, FormatType formatType) : QLocale(language, country), _timePrecision(timePrecision)
{
   initFromLocale(formatType);
}

QString QLocaleEx::dateTimeFormat(DateTimePart minPart, DateTimePart maxPart, bool editable) const
{
   if (editable)
   {
      for (auto&& f : _editableDateTimeFormats)
      {
         if (f.minPrecision == minPart && f.maxPrecision == maxPart)
         {
            return f.format;
         }
      }
   }

   for (auto&& f : _dateTimeFormats)
   {
      if (f.minPrecision == minPart && f.maxPrecision == maxPart)
      {
         return f.format;
      }
   }

   return QString();
}

bool QLocaleEx::toBool(const QString& string, bool* ok) const
{
   if (ok)
      *ok = true;

   if (string.isEmpty())
      return false;

   if (_trueLiterals.contains(string, Qt::CaseInsensitive))
      return true;

   if (_falseLiterals.contains(string, Qt::CaseInsensitive))
      return false;

   if (ok)
      *ok = false;

   return false;
}

QDateTimeEx QLocaleEx::toDateTimeEx(const QString& string) const
{
   return toDateTime(string);
}

QString QLocaleEx::toString(const QDateTimeEx& dateTime) const
{
   return dateTime.toString(bestDateTimeFormat(dateTime.minPrecision(), dateTime.maxPrecision()).format, *this);
}

QString QLocaleEx::toString(qlonglong i) const
{
   QString result;
   integerToString(i, 10, result, negativeSign(), QChar());
   return result;
}

QString QLocaleEx::toString(qulonglong i) const
{
   QString result;
   integerToString(i, 10, result, negativeSign(), QChar());
   return result;
}

QString QLocaleEx::toString(short i) const
{
   QString result;
   integerToString(i, 10, result, negativeSign(), QChar());
   return result;
}

QString QLocaleEx::toString(ushort i) const
{
   QString result;
   integerToString(i, 10, result, negativeSign(), QChar());
   return result;
}

QString QLocaleEx::toString(int i) const
{
   QString result;
   integerToString(i, 10, result, negativeSign(), QChar());
   return result;
}

QString QLocaleEx::toString(uint i) const
{
   QString result;
   integerToString(i, 10, result, negativeSign(), QChar());
   return result;
}

QString QLocaleEx::toString(double i, int prec) const
{
   QString result;
   doubleToString(i, prec, result, negativeSign(), decimalPoint());
   return result;
}

QString QLocaleEx::toString(float i, int prec) const
{
   QString result;
   doubleToString((double)i, prec, result, negativeSign(), decimalPoint());
   return result;
}

QString QLocaleEx::toString(bool b) const
{
   return b ? _trueLiterals.value(1) : _falseLiterals.value(1);
}

QString QLocaleEx::toString(const QVariant& qVariant) const
{
   QString text;

   if (qVariant.canConvert<Variant>())
   {
      text = qVariant.value<Variant>().toString(*this);
   }
   else
   {
      switch (qVariant.userType())
      {
      case QMetaType::Float:
      case QMetaType::Double:
         text = toString(qVariant.toReal());
         break;
      case QMetaType::Int:
      case QMetaType::LongLong:
         text = toString(qVariant.toLongLong());
         break;
      case QMetaType::UInt:
      case QMetaType::ULongLong:
         text = toString(qVariant.toULongLong());
         break;
      case QMetaType::QDate:
         text = toString(qVariant.toDate());
         break;
      case QMetaType::QTime:
         text = toString(qVariant.toTime());
         break;
      case QMetaType::QDateTime:
         text = toString(qVariant.toDateTime());
         break;
      case QMetaType::Bool:
         text = toString(qVariant.toBool());
         break;
      default:
         text = qVariant.toString();
         break;
      }
   }

   return text;
}

void QLocaleEx::setDateTimeFormat(DateTimePart minPart, DateTimePart maxPart, const QString& format, bool editable)
{
   auto& dateTimeFormats = editable ? _editableDateTimeFormats : _dateTimeFormats;

   for (auto&& f : dateTimeFormats)
   {
      if (f.minPrecision == minPart && f.maxPrecision == maxPart)
      {
         f.format = format;
         return;
      }
   }
   dateTimeFormats.append(DateTimeFormatRange(minPart, maxPart, format));
}

bool QLocaleEx::isCultureLocaleDefined(Language language, Country country)
{
   for (auto&& localeEx : _locales)
   {
#if QT_VERSION >= QT_VERSION_CHECK(6,6,0)
      if (localeEx.language() == language && localeEx.territory() == country)
#else
      if (localeEx.language() == language && localeEx.country() == country)
#endif
      {
         return true;
      }
   }
   return false;
}

const QLocaleEx& QLocaleEx::cultureLocale(Language language, Country country)
{
   for (auto&& localeEx : _locales)
   {
#if QT_VERSION >= QT_VERSION_CHECK(6,6,0)
      if (localeEx.language() == language && localeEx.territory() == country)
#else
      if (localeEx.language() == language && localeEx.country() == country)
#endif
      {
         return localeEx;
      }
   }

   _locales.append(QLocaleEx(language, country));
   if (_locales.size() == 1)
      QLocale::setDefault(_locales.first());

   return _locales.last();
}

void QLocaleEx::setCultureLocale(const QLocaleEx& locale)
{
   for (auto i = 0; i < _locales.count(); ++i)
   {
#if QT_VERSION >= QT_VERSION_CHECK(6,6,0)
      if (_locales.at(i).language() == locale.language() && _locales.at(i).territory() == locale.territory())
#else
      if (_locales.at(i).language() == locale.language() && _locales.at(i).country() == locale.country())
#endif
      {
         _locales[i] = locale;
         if (i == 0)
            QLocale::setDefault(_locales.first());
         return;
      }
   }

   _locales.append(locale);
   if (_locales.size() == 1)
      QLocale::setDefault(_locales.first());
}

void QLocaleEx::setCultureAsDefault(Language language, Country country)
{
   for (auto i = 0; i < _locales.count(); ++i)
   {
#if QT_VERSION >= QT_VERSION_CHECK(6,6,0)
      if (_locales.at(i).language() == language && _locales.at(i).territory() == country)
#else
      if (_locales.at(i).language() == language && _locales.at(i).country() == country)
#endif
      {
         if (i > 0)
         {
            _locales.move(i, 0);
            QLocale::setDefault(_locales.first());
         }
         return;
      }
   }

   _locales.prepend(QLocaleEx(language, country));
   QLocale::setDefault(_locales.first());
}

void QLocaleEx::initFromLocale(FormatType formatType)
{
   // Set date time display formats
   setDateTimeFormat(QLocale::dateTimeFormat(formatType));
   setDateFormat(QLocale::dateFormat(formatType));
   setTimeFormat(QLocale::timeFormat(formatType));

   // Set date time edit formats
   setDateTimeFormat(_timePrecision, DateTimePart::Millennium, QLocale::dateTimeFormat(QLocale::ShortFormat), true);
   setDateTimeFormat(DateTimePart::Day, DateTimePart::Millennium, QLocale::dateFormat(QLocale::ShortFormat), true);
   setDateTimeFormat(_timePrecision, DateTimePart::Hour, QLocale::timeFormat(QLocale::ShortFormat), true);

   // Set numeric formats
   _decimalPoint = toChar(QLocale::decimalPoint());
   if (language() != C) // Group separators should not be allowed in C but QLocale returns ','!
      _groupSeparator = toChar(QLocale::groupSeparator());
   _percent = toChar(QLocale::percent());
   _zeroDigit = toChar(QLocale::zeroDigit());
   _negativeSign = toChar(QLocale::negativeSign());
   _positiveSign = toChar(QLocale::positiveSign());
   _exponential = toChar(QLocale::exponential());

   // Set the true/false literals.
   // There must at least be 2 literals for true and 2 literals for false!
   // The first literal is a short form (1 character) and the second the default long form.
   if (language() == German)
   {
      _trueLiterals << "J" << "Ja" << "Wahr" << "T" << "true" << "Y" << "yes" << "1"; // For compatibility reasons also add the english literals
      _falseLiterals << "N" << "Nein" << "Falsch" << "F" << "false" << "no" << "0"; // For compatibility reasons also add the english literals
   }
   else
   {
      _trueLiterals << "T" << "true" << "Y" << "yes" << "1";
      _falseLiterals << "F" << "false" << "no" << "0";
   }
}

QLocaleEx::DateTimeFormatRange QLocaleEx::bestDateTimeFormat(DateTimePart minPart, DateTimePart maxPart) const
{
   DateTimeFormatRange formatRange;

   for (auto&& f : _dateTimeFormats)
   {
      if (minPart <= f.minPrecision && maxPart >= f.maxPrecision)
      {
         if (formatRange.format.isEmpty() || (f.minPrecision <= formatRange.minPrecision && f.maxPrecision >= formatRange.maxPrecision))
            formatRange = f;
      }
   }

   return formatRange;
}

const QLocaleEx& defaultLocale()
{
   if (_locales.isEmpty())
      _locales.append(QLocaleEx());

   return _locales.first();
}

void setDefaultLocale(const QLocaleEx& locale)
{
   for (auto i = 0; i < _locales.count(); ++i)
   {
#if QT_VERSION >= QT_VERSION_CHECK(6,6,0)
      if (_locales.at(i).language() == locale.language() && _locales.at(i).territory() == locale.territory())
#else
      if (_locales.at(i).language() == locale.language() && _locales.at(i).country() == locale.country())
#endif
      {
         _locales.removeAt(i);
         break;
      }
   }

   QLocale::setDefault(locale);
   _locales.insert(0, locale);
}

const QLocaleEx& toQLocaleEx(const QLocale& locale)
{
#if QT_VERSION >= QT_VERSION_CHECK(6,6,0)
   return QLocaleEx::cultureLocale(locale.language(), locale.territory());
#else
   return QLocaleEx::cultureLocale(locale.language(), locale.country());
#endif
}

QString toQString(const Variant& value)
{
   return value.toString();
}
