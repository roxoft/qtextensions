#include "stdafx.h"
#include "StringTemplate.h"
#include "CPPTokenizer.h"
#include "CPPExpression.h"

class StringTemplateContext : public Tokenizer::Context
{
public:
   StringTemplateContext(const Context* outerContext, bool checkPhEnd) : Context(outerContext), _braceLevel(checkPhEnd ? 1 : 0) {}
   virtual ~StringTemplateContext() = default;

   void setDelimiter(const QChar& startDelimiter, const QChar& endDelimiter) { _startDelimiter = startDelimiter; _endDelimiter = endDelimiter; }

   const Context* readToken(Token& token, TextStreamReader& data) const override;

   bool isPlaceholderOpen() const { return _braceLevel; }

private:
   QChar _startDelimiter = u'{';
   QChar _endDelimiter = u'}';
   mutable int _braceLevel = 0;
   mutable ProbingContext* _cppContext = nullptr;
};

const Tokenizer::Context* StringTemplateContext::readToken(Token& token, TextStreamReader& data) const
{
   if (data.peekChar().isNull())
      return nullptr;

   if (_braceLevel)
   {
      if (_cppContext == nullptr)
         _cppContext = createVersatileExpressionContext();

      _cppContext->readToken(token, data);

      auto opToken = token.as<CppOperatorToken>();

      if (opToken)
      {
         if (*opToken->op() == _endDelimiter)
            _braceLevel--;
         else if (*opToken->op() == _startDelimiter)
            _braceLevel++;
      }
   }

   if (_braceLevel == 0)
   {
      token = Token();

      QList<int> doubleEscapePosList;
      const auto bufferStart = data.bufferPos();
      QChar c;

      while (data.readChar(c))
      {
         if (c == _startDelimiter)
         {
            doubleEscapePosList.append(data.bufferPos() - bufferStart - 1);
            c = data.peekChar();
            if (c != _startDelimiter)
            {
               _braceLevel++;
               break;
            }
            data.skipChar();
         }
      }

      auto content = data.processedChars(bufferStart);

      for (int i = doubleEscapePosList.count(); i--;)
      {
         content.remove(doubleEscapePosList[i], 1);
      }

      token = new StringTemplateTextToken(content);
   }

   return this;
}



StringTemplate::StringTemplate()
{
}

StringTemplate::StringTemplate(const StringTemplate &other) : _tokenList(other._tokenList)
{
}

StringTemplate::StringTemplate(const QString& templateString)
{
   parseTemplate(templateString);
}

StringTemplate::~StringTemplate()
{
}

StringTemplate &StringTemplate::operator=(const StringTemplate &other)
{
   _tokenList = other._tokenList; return *this;
}

StringTemplate& StringTemplate::operator=(const QString& templateString)
{
   parseTemplate(templateString);
   return *this;
}

QString StringTemplate::resolve(const VarPtr& varTree, const ResolverFunctionMap& functionMap) const
{
   if (!varTree)
      return QString();

   QString result;
   QList<Token> expressionTokenList;

   for (auto&& token : _tokenList)
   {
      auto tplText = token.as<StringTemplateTextToken>();

      if (tplText)
      {
         if (!expressionTokenList.isEmpty())
         {
            result += createCppExpressionTree(expressionTokenList)->resolve(varTree, functionMap)->value().toString();

            expressionTokenList.clear();
         }

         result += tplText->text();
      }
      else
      {
         expressionTokenList.append(token);
      }
   }

   return result;
}

void StringTemplate::parseTemplate(const QString& templateString, bool ignoreWhitespaceAndComments, const QChar& startDelimiter, const QChar& endDelimiter)
{
   _tokenList.clear();

   if (templateString.isEmpty())
      return;

   const auto context = new StringTemplateContext(nullptr, false);

   context->setDelimiter(startDelimiter, endDelimiter);

   Tokenizer tokenizer(templateString, context);

   if (ignoreWhitespaceAndComments)
   {
      tokenizer.setIgnoreWhiteSpace();
      tokenizer.setIgnoreComments();
   }

   for (auto token = tokenizer.next(); token.isValid(); token = tokenizer.next())
   {
      _tokenList.append(token);
   }

   _placeholderNotClosed = context->isPlaceholderOpen();
}

QPair<int, int> StringTemplate::expressionRangeAt(int textPos) const
{
   QPair<int, int> result = { textPos, textPos };

   auto iCurrentToken = 0;

   for (; iCurrentToken < _tokenList.count(); ++iCurrentToken)
   {
      if (textPos <= _tokenList.at(iCurrentToken).endPos())
         break;
   }

   if (iCurrentToken == _tokenList.count())
      qFatal("iToken == _tokenList.count()");

   for (auto i = iCurrentToken; i < _tokenList.count(); ++i)
   {
      if (_tokenList.at(i).is<StringTemplateTextToken>())
      {
         if (textPos + 1 < _tokenList.at(i).endPos())
         {
            result.second = _tokenList.at(i).startPos() + 1; // Include the closing brace
            break;
         }
         iCurrentToken++;
      }
   }

   if (textPos >= result.second)
      return result;

   for (auto i = iCurrentToken; i--; )
   {
      if (_tokenList.at(i).is<StringTemplateTextToken>())
      {
         result.first = _tokenList.at(i).endPos() - 1; // Include the opening brace
         break;
      }
   }

   return result;
}
