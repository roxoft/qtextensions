#include "stdafx.h"
#include "TextParser.h"

TextParser::TextParser(TextStreamReader* reader, bool own) : _reader(reader), _own(own)
{
   if (_reader == nullptr)
   {
      _reader = new TextStreamReader;
      _own = true;
   }
}

TextParser::TextParser(const QString& text, bool keepRaw) : _reader(new TextStreamReader(text)), _own(true), _keepRaw(keepRaw)
{
}

TextParser::TextParser(QTextStream* is, bool keepRaw) : _reader(new TextStreamReader(is)), _own(true), _keepRaw(keepRaw)
{
}

TextParser::TextParser(CharacterStreamReader* dataSource, bool keepRaw) : _reader(new TextStreamReader(dataSource)), _own(true), _keepRaw(keepRaw)
{
}

TextParser::~TextParser()
{
   if (_own)
      delete _reader;
}

void TextParser::setText(const QString& text, bool keepRaw)
{
   _reader->setText(text);
   _keepRaw = keepRaw;
}

void TextParser::setStream(QTextStream* is, bool keepRaw)
{
   _reader->setStream(is);
   _keepRaw = keepRaw;
}

void TextParser::setDataSource(CharacterStreamReader* dataSource, bool keepRaw)
{
   _reader->setDataSource(dataSource);
   _keepRaw = keepRaw;
}

// Append _nextChar to _parsedText (optionally) and read the next character from the file into _nextChar. Return the original content of _nextChar.
QChar TextParser::readChar()
{
   QChar c;

   _reader->readChar(c);

   return c;
}

QChar TextParser::peekChar(int index)
{
   return _reader->peekChar(index);
}

QString TextParser::readToCharIn(const QString& delimiters, const QString& whitespaces, bool exclude)
{
   if (!_keepRaw)
      _reader->removeProcessedChars();

   // Loop until end of stream or the character matches (or doesn't match) one of the delimiters
   const auto bufPos = _reader->bufferPos();
   auto index = 0;

   while (true)
   {
      const auto c = _reader->peekChar(index++);

      if (c.isNull() || delimiters.contains(c) != exclude)
         break;

      if (!whitespaces.contains(c))
      {
         _reader->skipChar(index);
         index = 0;
      }
   }

   return _reader->processedChars(bufPos);
}

QString TextParser::readToStringIn(const QList<QString>& delimiters)
{
   if (!_keepRaw)
      _reader->removeProcessedChars();

   const auto bufPos = _reader->bufferPos();

   while (!_reader->atEnd())
   {
      // Try to find a match in the delimiter list
      for (auto&& delimiter : delimiters)
      {
         if (!delimiter.isEmpty())
         {
            auto index = 0;
            while (index < delimiter.length())
            {
               if (delimiter.at(index) != _reader->peekChar(index))
                  break;
               ++index;
            }

            if (index == delimiter.length())
            {
               // Delimiter found
               return _reader->processedChars(bufPos);
            }
         }
      }

      if (!_reader->skipChar())
         break;
   }

   // Delimiter not found or invalid parameters
   return _reader->processedChars(bufPos);
}

bool TextParser::skipChar(int count)
{
   return _reader->skipChar(count);
}
