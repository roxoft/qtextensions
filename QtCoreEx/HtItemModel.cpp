#include "stdafx.h"
#include "HtItemModel.h"

HtMimeData::HtMimeData(const QList<const HtNode*>& nodeList, const HtNode* draggedNode) : _nodeList(nodeList), _draggedNode(draggedNode)
{
}

bool HtMimeData::hasFormat(const QString& mimetype) const
{
   return mimetype == "text/plain";
}

QStringList HtMimeData::formats() const
{
   return QStringList() << "text/plain";
}

#if QT_VERSION_MAJOR < 6
QVariant HtMimeData::retrieveData(const QString& mimetype, QVariant::Type /*preferredType*/) const
#else
QVariant HtMimeData::retrieveData(const QString& mimetype, QMetaType /*preferredType*/) const
#endif
{
   if (mimetype != "text/plain")
      return QVariant();

   QStringList rows;

   for (auto&& node : _nodeList)
   {
      QStringList attributes;

      for (auto i = 0; i < node->attributeCount(); ++i)
      {
         attributes.append(node->attribute(i).toString());
      }

      rows.append(attributes.join(" | "));
   }

   return rows.join("\n");
}

Variant HtItemModel::defaultAttribute(int role) const
{
   if (role == Qt::ItemFlagsRole)
      return (int)defaultItemFlags();
   return HtModel::defaultAttribute(role);
}

void HtItemModel::insertColumn(int index, int count)
{
   onBeginInsertColumns(rootNode(), index, count);
   insertColumnRecursive(rootNode(), index, count);
   onEndInsertColumns(rootNode(), index, count);
}

void HtItemModel::removeColumn(int index, int count)
{
   onBeginRemoveColumns(rootNode(), index, count);
   removeColumnRecursive(rootNode(), index, count);
   onEndRemoveColumns(rootNode(), index);
}

QModelIndex HtItemModel::index(int row, int column, const QModelIndex& parent) const
{
   if (!hasIndex(row, column, parent))
      return QModelIndex();

   const HtNode *parentItem = itemFromIndex(parent);
   const HtNode *childItem = NULL;

   if (parentItem)
      childItem = parentItem->child(row);

   if (childItem)
      return createIndex(row, column, (void*)childItem);
   else
      return QModelIndex();
}

QModelIndex HtItemModel::parent(const QModelIndex & index) const
{
   return indexFromItem(itemFromIndex(index)->parent());
}

bool HtItemModel::hasChildren(const QModelIndex &parent) const
{
   return itemFromIndex(parent)->hasChildren();
}

int HtItemModel::rowCount(const QModelIndex& parent) const
{
   if (parent.column() > 0) // If the parent is valid and the column is not 0 return 0
      return 0;
   return itemFromIndex(parent)->childCount();
}

int HtItemModel::columnCount(const QModelIndex& parent) const
{
   if (parent.column() > 0) // If the parent is valid and the column is not 0 return 0
      return 0;

   int columns = headerCount();

   if (columns == 0 && rootNode()->childCount() > 0)
   {
      columns = rootNode()->child(0)->attributeCount();
   }

   return columns;
}

QVariant HtItemModel::data(const QModelIndex& index, int role) const
{
   auto node = itemFromIndex(index);

   if (!node)
      return QVariant();

   if (role == Qt::AccessibleTextRole)
   {
      auto accessibleText = node->attribute(index.column(), role).toString();

      if (accessibleText.isEmpty())
         accessibleText = node->attribute(index.column()).toString();

      return accessibleText;
   }

   return node->attribute(index.column(), role);
}

QVariant HtItemModel::headerData(int section, Qt::Orientation orientation, int role) const
{
   if (orientation == Qt::Horizontal)
   {
      return rootNode()->attribute(section, role);
   }
   else if (orientation == Qt::Vertical)
   {
      if (role == Qt::DisplayRole || role == Qt::EditRole)
         return section + 1;
   }

   return QVariant();
}

Qt::ItemFlags HtItemModel::flags(const QModelIndex& index) const
{
   if (!index.isValid())
      return (Qt::ItemFlags)rootNode()->flags(0, Qt::ItemFlagsRole);

   return (Qt::ItemFlags)itemFromIndex(index)->flags(index.column(), Qt::ItemFlagsRole);
}

bool HtItemModel::setHeaderData(int section, Qt::Orientation orientation, const QVariant &value, int role)
{
   if (orientation == Qt::Horizontal)
   {
      rootNode()->setAttribute(section, value, role); // The node has to call onItemChanged
      return true;
   }
   return false;
}

bool HtItemModel::setData(const QModelIndex& index, const QVariant& value, int role)
{
   if (!index.isValid())
      return false;

   itemFromIndex(index)->setAttribute(index.column(), value, role); // The node has to call onItemChanged

   return true;
}

bool HtItemModel::insertColumns(int position, int columns, const QModelIndex& parent)
{
   if (parent.isValid())
      return false;

   insertColumn(position, columns);

   return true;
}

bool HtItemModel::removeColumns(int position, int columns, const QModelIndex& parent)
{
   if (parent.isValid())
      return false;

   removeColumn(position, columns);

   return true;
}

bool HtItemModel::insertRows(int position, int rows, const QModelIndex& parent)
{
   HtNode *item = itemFromIndex(parent);

   while (rows--)
   {
      if (item->insertChild(position) == nullptr) // The item calls beginInsertRows / endInsertRows
         return false;
   }

   return true;
}

bool HtItemModel::removeRows(int position, int rows, const QModelIndex& parent)
{
   HtNode *item = itemFromIndex(parent);

   while (rows--)
   {
      item->removeChild(position); // The item calls beginRemoveRows / endRemoveRows
   }

   return true;
}

void HtItemModel::sort(int column, Qt::SortOrder order)
{
   rootNode()->sort(column, sortRole(column), (order == Qt::DescendingOrder));
}

bool HtItemModel::setData(const QModelIndex& topLeft, const QModelIndex& bottomRight, const QVariant& value, int role)
{
   return setData(topLeft.column(), topLeft.row(), bottomRight.column() + 1, bottomRight.row() + 1, value, role);
}

bool HtItemModel::setData(int beginColumn, int beginRow, int endColumn, int endRow, const QVariant& value, int role)
{
   bool success = true;

   startCollectChanges();

   for (int row = beginRow; row < endRow; row++)
   {
      for (int column = beginColumn; column < endColumn; column++)
      {
         auto item = index(row, column);

         if (!setData(item, value, role))
            success = false;
      }
   }

   endCollectChanges();

   return success;
}

void HtItemModel::onBeginInsertItems(HtNode* parent, int index, int count)
{
   beginInsertRows(indexFromItem(parent), index, index + count - 1);
}

void HtItemModel::onEndInsertItems(HtNode* , int , int )
{
   endInsertRows();
}

void HtItemModel::onBeginInsertColumns(HtNode* parent, int index, int count)
{
   beginInsertColumns(indexFromItem(parent), index, index + count - 1);
}

void HtItemModel::onEndInsertColumns(HtNode* , int , int )
{
   endInsertColumns();
}

void HtItemModel::onBeginRemoveItems(HtNode* parent, int index, int count)
{
   beginRemoveRows(indexFromItem(parent), index, index + count - 1);
}

void HtItemModel::onEndRemoveItems(HtNode* , int )
{
   endRemoveRows();
}

void HtItemModel::onBeginRemoveColumns(HtNode* parent, int index, int count)
{
   beginRemoveColumns(indexFromItem(parent), index, index + count - 1);
}

void HtItemModel::onEndRemoveColumns(HtNode* , int )
{
   endRemoveColumns();
}

bool HtItemModel::onBeginMoveItems(HtNode* parent, int from, int count, int to)
{
   QModelIndex parentIndex = indexFromItem(parent);

   return beginMoveRows(parentIndex, from, from + count - 1, parentIndex, from < to ? to + count : to);
}

void HtItemModel::onEndMoveItems(HtNode* , int , int , int )
{
   endMoveRows();
}

void HtItemModel::onBeginSwapItems(HtNode* parent, int index1, int index2)
{
   emit layoutAboutToBeChanged(QList<QPersistentModelIndex>() << indexFromItem(parent), VerticalSortHint);
   _oldIndexList.append(indexFromItem(parent->child(index1)));
   _oldIndexList.append(indexFromItem(parent->child(index2)));
}

void HtItemModel::onEndSwapItems(HtNode* parent, int index1, int index2)
{
   changePersistentIndex(_oldIndexList.at(0), indexFromItem(parent->child(index1)));
   changePersistentIndex(_oldIndexList.at(1), indexFromItem(parent->child(index2)));
   _oldIndexList.clear();
   emit layoutChanged(QList<QPersistentModelIndex>() << indexFromItem(parent), VerticalSortHint);
}

void HtItemModel::onBeginLayoutChange(HtNode* parent)
{
   emit layoutAboutToBeChanged(QList<QPersistentModelIndex>() << indexFromItem(parent), VerticalSortHint);
   for (int i = 0; i < parent->childCount(); ++i)
   {
      HtNode* child = parent->child(i);

      for (int col = 0; col < headerCount(); ++col)
         _oldIndexList.append(indexFromItem(child, col));
   }
}

void HtItemModel::onEndLayoutChange(HtNode* parent)
{
   QModelIndexList newIndexList;
   for (int i = 0; i < parent->childCount(); ++i)
   {
      HtNode* child = parent->child(i);

      for (int col = 0; col < headerCount(); ++col)
         newIndexList.append(indexFromItem(child, col));
   }
   changePersistentIndexList(_oldIndexList, newIndexList);
   _oldIndexList.clear();
   emit layoutChanged(QList<QPersistentModelIndex>() << indexFromItem(parent), VerticalSortHint);
}

void HtItemModel::onBeginResetModel()
{
   beginResetModel();
}

void HtItemModel::onEndResetModel()
{
   endResetModel();
}

void HtItemModel::onItemChanged(HtNode* item, int firstAttribute, int lastAttribute, int role)
{
   if (lastAttribute < firstAttribute)
      lastAttribute = firstAttribute;
   if (firstAttribute < 0)
      return;
   if (role < -1)
      role = -1;

   if (item->parent())
   {
      const auto leftIndex = indexFromItem(item, firstAttribute);
      const auto rightIndex = indexFromItem(item, lastAttribute);

      if (_collectChanges)
      {
         if (!_topLeft.isValid())
            _topLeft = leftIndex;
         else
            _topLeft = index(qMin(_topLeft.row(), leftIndex.row()), qMin(_topLeft.column(), leftIndex.column()), leftIndex.parent());

         if (!_bottomRight.isValid())
            _bottomRight = rightIndex;
         else
            _bottomRight = index(qMax(_bottomRight.row(), rightIndex.row()), qMax(_bottomRight.column(), rightIndex.column()), rightIndex.parent());

         if (!_changedRoles.contains(role))
            _changedRoles.append(role);
      }
      else
      {
         QVector<int> roles;
         if (role >= 0)
            roles.append(role);
         emit dataChanged(leftIndex, rightIndex, roles);
         emit nodeChanged(item, firstAttribute, lastAttribute, role);
      }
   }
   else
   {
      emit headerDataChanged(Qt::Horizontal, firstAttribute, lastAttribute);
      emit nodeChanged(item, firstAttribute, lastAttribute, role);
   }
}

void HtItemModel::startCollectChanges()
{
   _collectChanges++;
}

void HtItemModel::endCollectChanges()
{
   if (_collectChanges > 0)
      _collectChanges--;

   if (_collectChanges == 0 && _topLeft.isValid() && _bottomRight.isValid())
   {
      if (_changedRoles.contains(-1))
         _changedRoles.clear();
      emit dataChanged(_topLeft, _bottomRight, _changedRoles);
      _topLeft = QModelIndex();
      _bottomRight = QModelIndex();
      _changedRoles.clear();
   }
}

const HtNode* HtItemModel::itemFromIndex(const QModelIndex& index) const
{
   if (!index.isValid())
      return rootNode();

   return static_cast<const HtNode*>(index.internalPointer());
}

HtNode* HtItemModel::itemFromIndex(const QModelIndex& index)
{
   if (!index.isValid())
      return rootNode();

   return static_cast<HtNode*>(index.internalPointer());
}

QModelIndex HtItemModel::indexFromItem(const HtNode* item, int attIndex) const
{
   if (item == NULL || item->parent() == NULL)
      return QModelIndex();
   return createIndex(item->parent()->indexOf(item), attIndex, (void*)item);
}

void HtItemModel::insertColumnRecursive(HtNode* item, int index, int count)
{
   item->insertAttributes(index, count);

   for (int i = 0; i < item->childCount(); ++i)
      insertColumnRecursive(item->child(i), index, count);
}

void HtItemModel::removeColumnRecursive(HtNode* item, int index, int count)
{
   item->removeAttributes(index, count);

   for (int i = 0; i < item->childCount(); ++i)
      removeColumnRecursive(item->child(i), index, count);
}
