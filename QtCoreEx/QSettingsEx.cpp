#include "stdafx.h"
#include "QSettingsEx.h"
#include "QtListExtensions.h"

QSettingsEx::QSettingsEx()
{
}

QSettingsEx::QSettingsEx(QSettingsExBase* impl) : bp<QSettingsExBase>(impl)
{
}

QSettingsEx::QSettingsEx(const QSettingsEx& other) : bp<QSettingsExBase>(other)
{
}

QSettingsEx::~QSettingsEx()
{
}

QSettingsEx& QSettingsEx::operator=(const QSettingsEx& other)
{
   bp<QSettingsExBase>::operator=(other);
   return *this;
}

int QSettingsEx::beginGroup(const QString& path)
{
   if (data())
      return data()->beginGroup(path);
   return 0;
}

bool QSettingsEx::endGroup(int depth)
{
   if (data())
      return data()->endGroup(depth);
   return false;
}

void QSettingsEx::endAllGroups()
{
   if (data())
      data()->endAllGroups();
}

Variant QSettingsEx::value(const QString& key, const Variant& defaultValue) const
{
   if (data())
      return data()->value(key, defaultValue);
   return Variant::null;
}

QList<Variant> QSettingsEx::values(const QString& key) const
{
   if (data())
      return data()->values(key);
   return QList<Variant>();
}

QStringList QSettingsEx::stringValues(const QString& key) const
{
   if (data())
      return data()->stringValues(key);
   return QStringList();
}

void QSettingsEx::setValue(const QString& key, const Variant& value)
{
   if (data())
      data()->setValue(key, value);
}

void QSettingsEx::addValue(const QString& key, const Variant& value)
{
   if (data())
      data()->addValue(key, value);
}

QString QSettingsEx::remark(const QString& key) const
{
   if (data())
      return data()->remark(key);
   return QString();
}

void QSettingsEx::setRemark(const QString& key, const QString& remark)
{
   if (data())
      data()->setRemark(key, remark);
}

QStringList QSettingsEx::keys(const QString& path) const
{
   if (data())
      return data()->keys(path);
   return QStringList();
}

QSettingsExBase::QSettingsExBase() : _currentSetting(new Setting(this))
{
}

QSettingsExBase::~QSettingsExBase()
{
   endAllGroups();
   delete _currentSetting;
}

bool QSettingsExBase::isGroup(const QString &path) const
{
   const Setting *s = _currentSetting->const_setting(path);

   if (s)
      return s->isGroup();

   return false;
}

int QSettingsExBase::beginGroup(const QString &path)
{
   QStringList key = normalizedKey(path);

   _currentSetting = _currentSetting->setting(key);
   return key.count();
}

int QSettingsExBase::addGroup(const QString& path)
{
   QStringList key = normalizedKey(path);

   _currentSetting = _currentSetting->addSetting(key);
   return key.count();
}

bool QSettingsExBase::endGroup(int depth)
{
   if (depth < 0)
      return false;

   while (!_currentSetting->isRoot() && depth)
   {
      _currentSetting = _currentSetting->parent();
      depth--;
   }

   return depth == 0;
}

QString QSettingsExBase::absolutePath(const QString &key) const
{
   const auto s = _currentSetting->const_setting(key);

   if (s)
      return s->fullPath();

   return QString();
}

Variant QSettingsExBase::value(const QString& key, const Variant& defaultValue) const
{
   const auto s = _currentSetting->const_setting(key);

   if (s)
      return s->value();

   return defaultValue;
}

QList<Variant> QSettingsExBase::values(const QString& key) const
{
   QList<Variant> result;

   auto settings = _currentSetting->const_settings(key);

   foreach (auto&& setting, settings)
   {
      result.append(setting->value());
   }

   return result;
}

QStringList QSettingsExBase::stringValues(const QString& key) const
{
   QStringList result;

   auto settings = _currentSetting->const_settings(key);

   foreach (auto&& setting, settings)
   {
      result.append(setting->value().toString());
   }

   return result;
}

void QSettingsExBase::setValue(const QString& key, const Variant& value)
{
   const auto s = _currentSetting->setting(key);

   s->setValue(value);
}

void QSettingsExBase::addValue(const QString &key, const Variant &value)
{
   const auto s = _currentSetting->addSetting(key);

   s->setValue(value);
}

QString QSettingsExBase::remark(const QString &key) const
{
   const auto s = _currentSetting->const_setting(key);

   if (s)
      return s->remark();

   return QString();
}

void QSettingsExBase::setRemark(const QString &key, const QString &remark)
{
   auto s = _currentSetting->setting(key);

   if (s)
      s->setRemark(remark);
}

QStringList QSettingsExBase::keys(const QString& path) const
{
   const auto s = _currentSetting->const_setting(path);

   if (s)
      return s->keys();

   return QStringList();
}

void QSettingsExBase::sync()
{
   if (isChanged())
      save();
   else
      load();
}

//
// class QSettingsExBase::Setting
//

void QSettingsExBase::Setting::setIsChanged(bool changed)
{
   _changed = changed;

   if (!changed)
   {
      foreach (Setting *s, _settings)
         s->setIsChanged(changed);
   }
   else if (_parent)
      _parent->setIsChanged(changed);
}

const Variant &QSettingsExBase::Setting::value() const
{
   foreach (const Variant &v, _valueList)
   {
      if (!v.isNull())
         return v;
   }

   return Variant::null;
}

const Variant &QSettingsExBase::Setting::value(int layer) const
{
   if (layer < _valueList.count())
      return _valueList.at(layer);
   return Variant::null;
}

void QSettingsExBase::Setting::setValue(const Variant &value, int layer)
{
   if (layer == 0 && value == this->value())
      return;

   while (_valueList.count() < layer)
      _valueList.append(Variant());

   if (layer < _valueList.count())
      _valueList[layer] = value;
   else
      _valueList.append(value);

   setIsChanged(true);
}

QStringList QSettingsExBase::Setting::keys() const
{
   QStringList result;

   foreach (const Setting *s, _settings)
   {
      result.append(s->_name);
   }

   result.sort();
   static_cast<void>(std::unique(result.begin(), result.end()));

   return result;
}

QSettingsExBase::Setting *QSettingsExBase::Setting::setting(const QString &path, bool *created)
{
   return setting(_base->normalizedKey(path), created);
}

QSettingsExBase::Setting *QSettingsExBase::Setting::setting(const QStringList &path, bool *created)
{
   if (created)
      *created = false;

   if (path.isEmpty())
      return this;

   for (int i = _settings.count(); i--; )
   {
      if (_settings[i]->_name.compare(path.first(), _base->_caseSensitivity) == 0)
         return _settings[i]->setting(path.mid(1), created);
   }

   if (created)
      *created = true;

   auto s = newSetting(_base);
   s->_parent = this;
   s->_name = path.first();

   _settings.append(s);

   setIsChanged(true);

   return s->setting(path.mid(1));
}

const QSettingsExBase::Setting *QSettingsExBase::Setting::const_setting(const QString &path) const
{
   return const_setting(_base->normalizedKey(path));
}

const QSettingsExBase::Setting *QSettingsExBase::Setting::const_setting(const QStringList &path) const
{
   if (path.isEmpty())
      return this;

   for (int i = _settings.count(); i--; )
   {
      if (_settings[i]->_name.compare(path.first(), _base->_caseSensitivity) == 0)
         return _settings[i]->const_setting(path.mid(1));
   }

   return nullptr;
}

QList<QSettingsExBase::Setting *> QSettingsExBase::Setting::settings(const QString &path)
{
   return settings(_base->normalizedKey(path));
}

QList<QSettingsExBase::Setting *> QSettingsExBase::Setting::settings(const QStringList &path)
{
   QList<Setting *> result;

   if (path.isEmpty())
      result.append(this);
   else
   {
      foreach (Setting *s, _settings)
      {
         if (s->_name.compare(path.first(), _base->_caseSensitivity) == 0)
            result += s->settings(path.mid(1));
      }
   }

   return result;
}

QList<const QSettingsExBase::Setting *> QSettingsExBase::Setting::const_settings(const QString &path) const
{
   return const_settings(_base->normalizedKey(path));
}

QList<const QSettingsExBase::Setting *> QSettingsExBase::Setting::const_settings(const QStringList &path) const
{
   QList<const Setting *> result;

   if (path.isEmpty())
      result.append(this);
   else
   {
      foreach (Setting *s, _settings)
      {
         if (s->_name.compare(path.first(), _base->_caseSensitivity) == 0)
            result += s->const_settings(path.mid(1));
      }
   }

   return result;
}

QSettingsExBase::Setting *QSettingsExBase::Setting::addSetting(const QString &path)
{
   return addSetting(_base->normalizedKey(path));
}

QSettingsExBase::Setting *QSettingsExBase::Setting::addSetting(const QStringList &path)
{
   if (path.isEmpty())
      return this;

   Setting *setting = nullptr;

   if (path.count() > 1)
   {
      for (int i = _settings.count(); i--; )
      {
         if (_settings[i]->_name.compare(path.first(), _base->_caseSensitivity) == 0)
         {
            setting = _settings[i];
            break;
         }
      }
   }

   if (setting == nullptr)
   {
      setting = newSetting(_base);
      setting->_parent = this;
      setting->_name = path.first();

      _settings.append(setting);

      setIsChanged(true);
   }

   return setting->addSetting(path.mid(1));
}

void QSettingsExBase::Setting::remove(const QString &path)
{
   remove(_base->normalizedKey(path));
}

void QSettingsExBase::Setting::remove(const QStringList &path)
{
   for (int i = _settings.count(); i--; )
   {
      Setting *s = _settings.at(i);

      // Check if the first path element matches the setting
      if (path.isEmpty() || s->_name.compare(path.first(), _base->_caseSensitivity) == 0)
      {
         QStringList remainingPath;
         if (!path.isEmpty())
            remainingPath = path.mid(1);

         if (remainingPath.isEmpty())
         {
            _settings.removeAt(i);
            delete s;
            setIsChanged(true);
         }
         else
         {
            // Call remove recursively with the rest of the path
            s->remove(remainingPath);
         }
      }
   }
}

void QSettingsExBase::Setting::detach()
{
   if (_parent)
   {
      _parent->_settings.removeOne(this);
      _parent->setIsChanged(true);
      _parent = nullptr;
   }
}

void QSettingsExBase::Setting::clearSettings()
{
   qDeleteAll(_settings);
   _settings.clear();
   setIsChanged(true);
}

void QSettingsExBase::Setting::clear()
{
   _name.clear();
   _remark.clear();
   _valueList.clear();
   qDeleteAll(_settings);
   _settings.clear();
   _changed = false;
}

QSettingsExBase::Setting *QSettingsExBase::Setting::next(Setting *child)
{
   int index = 0;

   if (child)
      index = _settings.indexOf(child) + 1;

   if (index < _settings.count())
      return _settings[index];

   if (_parent == nullptr)
      return nullptr;

   return _parent->next(this);
}

QString QSettingsExBase::Setting::fullPath() const
{
   if (_parent == nullptr || _base->_keySeparator.isNull())
      return QString();

   if (_parent->isRoot())
      return _name;

   return _parent->fullPath() + _base->_keySeparator + _name;
}

QSettingsExBase::QSettingsExBase(Setting *root) : _currentSetting(root)
{
}

QStringList QSettingsExBase::normalizedKey(const QString& key) const
{
   QStringList result;
   QString     element;
   bool        space = false;

   foreach (QChar qChar, key)
   {
      if (qChar == _keySeparator)
      {
         if (!element.isEmpty())
         {
            result.append(element);
            element.clear();
         }
         space = false;
      }
      else if (qChar.isSpace())
      {
         space = true;
      }
      else
      {
         if (space && !element.isEmpty())
            element.append(QLatin1Char(' '));
         space = false;
         element.append(qChar);
      }
   }
   if (!element.isEmpty())
      result.append(element);

   return result;
}
