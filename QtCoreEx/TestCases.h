#ifndef TESTCASES_H
#define TESTCASES_H

#include "QLocaleEx.h"
inline QString toQString(const std::type_info& type) { return QString::fromLatin1(type.name()); }
#include <TestFramework.h>

class MiscTests : public TestFramework
{
   Q_OBJECT

public:
   MiscTests(QObject* parent = nullptr) : TestFramework(parent) {}
   ~MiscTests() override = default;

public slots:
   void functionStatusTests();
   void listExtensionsTests();
   void sortedHtNodesTests();
   void textCodecConversions();
};

class ConversionTest : public TestFramework
{
   Q_OBJECT

public:
   ConversionTest(QObject* parent = 0) : TestFramework(parent) {}
   ~ConversionTest() override = default;

   void init() override;
   void deinit() override;

public slots:
   void doubleToString();
   void numberConversions();
   void nullableConversions();
   void localeEx();
   void rational();
   void decimal();
   void dateTime();
   void dateTimeEx();
   void lists();

private:
   QLocaleEx _prevLocale;
};

class VariantTests : public TestFramework
{
   Q_OBJECT

public:
   VariantTests(QObject *parent = 0) : TestFramework(parent) {}
   ~VariantTests() override = default;

   void init() override;
   void deinit() override;

public slots:
   void conversionTests();
   void serializationTests();

private:
   QLocaleEx _prevLocale;
};

class SettingTests : public TestFramework
{
   Q_OBJECT

public:
   SettingTests(QObject *parent = 0) : TestFramework(parent) {}
   ~SettingTests() override = default;

public slots:
   void loadTests();
   void indentGroupTest();
   void braceGroupTest();
};

class SerStreamTests : public TestFramework
{
   Q_OBJECT

public:
   SerStreamTests(QObject *parent = 0) : TestFramework(parent) {}
   ~SerStreamTests() override = default;

public slots :
   void fileTests();
};

class VarTreeTests : public TestFramework
{
   Q_OBJECT

public:
   VarTreeTests(QObject *parent = 0) : TestFramework(parent) {}
   ~VarTreeTests() override = default;

public slots :
   void dictTest();
   void basicTest();
   void tableTest();
};

class ParserTests : public TestFramework
{
   Q_OBJECT

public:
   ParserTests(QObject *parent = 0) : TestFramework(parent) {}
   ~ParserTests() override = default;

public slots :
   void csvTest();
   void cppTests();
};

class SqlExpressionTests : public TestFramework
{
   Q_OBJECT

public:
   SqlExpressionTests(QObject* parent = 0) : TestFramework(parent) {}
   ~SqlExpressionTests() override = default;

public slots :
   void basic_tests();
};

class CppExpressionTests : public TestFramework
{
   Q_OBJECT

public:
   CppExpressionTests(QObject* parent = 0) : TestFramework(parent) {}
   ~CppExpressionTests() override = default;

public slots :
   void basic_tests();
   void error_tests();
   void function_call();
   void stringTemplate_tests();
};

#endif // TESTCASES_H
