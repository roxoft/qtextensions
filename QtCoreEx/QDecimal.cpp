#include "stdafx.h"
#include "QDecimal.h"
#include "double.h"
#include "Variant.h"
#include <limits>

// From orl.h
#define OCI_NUMBER_SIZE 22
struct OCINumber
{
   unsigned char OCINumberPart[OCI_NUMBER_SIZE];
};

static const quint64 segmentBase = 1000000000ULL;

static const quint32 decBase[] = {
   1, 10, 100, 1000, 10000, 100000, 1000000, 10000000, 100000000, 1000000000
};

bool QDecimal::operator==(const QDecimal& rhs) const
{
   if (_data == nullptr && rhs._data == nullptr)
      return true;
   if (_data == nullptr || rhs._data == nullptr)
      return false;

   if (_data->negative != rhs._data->negative)
      return false;

   int offset = (int)_data->fracSize - (int)rhs._data->fracSize;
   int i;

   if (offset < 0)
      i = qMax(segmentCount() + (-offset + digitsPerBlock - 1) / digitsPerBlock, rhs.segmentCount());
   else
      i = qMax(segmentCount(), rhs.segmentCount() + (offset + digitsPerBlock - 1) / digitsPerBlock);

   // Compare
   while (i--)
   {
      if (offset < 0)
      {
         if (segmentValue(i, offset) != rhs.segmentValue(i))
            return false;
      }
      else
      {
         if (segmentValue(i) != rhs.segmentValue(i, -offset))
            return false;
      }
   }
   return true;
}

bool QDecimal::operator<(const QDecimal& rhs) const
{
   if (rhs._data == nullptr)
      return false;
   if (_data == nullptr)
      return true;

   if (_data->negative && !rhs._data->negative)
      return true;
   if (!_data->negative && rhs._data->negative)
      return false;

   return mantissaLess((int)_data->fracSize - (int)rhs._data->fracSize, rhs);
}

QDecimal &QDecimal::operator++()
{
   if (isNegative())
      decrementAt(fracSize());
   else
      incrementAt(fracSize());
   return *this;
}

QDecimal &QDecimal::operator--()
{
   if (isNegative())
      incrementAt(fracSize());
   else
      decrementAt(fracSize());
   return *this;
}

QDecimal QDecimal::operator-() const
{
   QDecimal result(*this);

   result.negate();

   return result;
}

QDecimal &QDecimal::operator+=(const QDecimal &rhs)
{
   add(rhs, false);
   return *this;
}

QDecimal &QDecimal::operator-=(const QDecimal &rhs)
{
   add(rhs, true);
   return *this;
}

QDecimal &QDecimal::operator+=(int number)
{
   add(number, false);
   return *this;
}

QDecimal &QDecimal::operator-=(int number)
{
   add(number, true);
   return *this;
}

QDecimal &QDecimal::operator *=(const QDecimal &rhs)
{
   QDecimal lhs = *this;

   release();

   if (lhs._data && rhs._data)
   {
      int t = (int)qMax(lhs._data->blocks, rhs._data->blocks) - 1;
      int u = 0;
      int l = 0;

      while (l <= t)
      {
         for (int i = l; i <= u - l; ++i)
         {
            quint64 r = (quint64)lhs.at(i) * (quint64)rhs.at(u - i);

            int j = u;

            while (r)
            {
               r += (quint64)at(j);
               set(j, r % segmentBase);
               r /= segmentBase;
               j++;
            }
         }
         u++;
         if (u > t)
            l++;
      }

      // Make the _data pointer valid
      cow();

      _data->fracSize = lhs._data->fracSize + rhs._data->fracSize;

      if (!isZero() && (lhs._data->negative != rhs._data->negative))
         _data->negative = 1;

      // Strip off the rightmost zeros up to the fractional size of the left hand side
      truncate(~(int)lhs._data->fracSize);
   }

   return *this;
}

QDecimal &QDecimal::operator /=(const QDecimal &rhs)
{
   devideBy(rhs);
   return *this;
}

QDecimal &QDecimal::operator %=(const QDecimal &rhs)
{
   *this = devideBy(rhs);
   return *this;
}

QDecimal &QDecimal::operator *=(qint32 rhs)
{
   if (_data)
   {
      cow(); // make sure we have exclusive data access

      if (rhs < 0)
      {
         _data->negative ^= 1;
         rhs *= -1;
      }

      int      segments = (int)_data->blocks;
      quint64  carry = 0ULL;

      for (int i = 0; i < segments || carry; ++i)
      {
         carry += (quint64)rhs * (quint64)at(i);
         set(i, carry % segmentBase);
         carry /= segmentBase;
      }

      if (_data->negative && isZero())
         _data->negative = 0;
   }

   return *this;
}

QDecimal &QDecimal::operator /=(qint32 rhs)
{
   devideBy(rhs);
   return *this;
}

QDecimal &QDecimal::operator %=(qint32 rhs)
{
   int remainder = devideBy(rhs);

   *this = QDecimal(remainder);

   return *this;
}

QDecimal& QDecimal::operator<<=(int count)
{
   if (count < 0)
      return operator>>=(-count);

   if (count > 0)
   {
      int i = segmentCount() + (count + digitsPerBlock - 1) / digitsPerBlock;

      while (i--)
      {
         quint32 segment = segmentValue(i, -count);

         if (segment || i == 0)
            set(i, segment);
      }
   }

   return *this;
}

QDecimal& QDecimal::operator>>=(int count)
{
   if (count < 0)
      return operator<<=(-count);

   if (count > 0)
   {
      for (int i = 0; i < segmentCount(); ++i)
      {
         set(i, segmentValue(i, count));
      }
   }

   return *this;
}

QDecimal QDecimal::operator<<(int count) const
{
   return QDecimal(*this) <<= count;
}

QDecimal QDecimal::operator>>(int count) const
{
   return QDecimal(*this) >>= count;
}

qint32 QDecimal::devideBy(qint32 divisor)
{
   if (divisor == 0)
   {
      release();
      return 0;
   }

   qint64 remainder = 0LL;

   if (_data && _data->blocks)
   {
      // Work with positive divisor

      auto negate = false;

      if (divisor < 0)
      {
         negate = true;
         divisor *= -1;
      }

      // Do the division

      int i = (int)_data->blocks;

      while (i--)
      {
         remainder *= segmentBase;
         remainder += (qint64)at(i);
         set(i, remainder / (qint64)divisor);
         remainder %= (qint64)divisor;
      }

      // Exclusive access is granted by at least one call to set()

      if (_data->negative)
         remainder *= -1LL;

      if (negate)
         _data->negative ^= 1;
      if (_data->negative && isZero())
         _data->negative = 0;
   }

   return (qint32)remainder;
}

QDecimal QDecimal::devideBy(const QDecimal& divisor)
{
   auto dividend = *this;

   // Clear the result

   release();

   if (dividend._data == nullptr || divisor._data == nullptr)
      return QDecimal();

   // Check if the divisor can be represented by a qint32

   int highSegment = (int)divisor._data->blocks;
   int lowSegment = 0;

   while (highSegment--)
   {
      if (divisor.at(highSegment))
         break;
   }

   while (lowSegment < highSegment)
   {
      if (divisor.at(lowSegment))
         break;
      lowSegment++;
   }

   if (highSegment < 0)
   {
      // The divisor is 0!
      return QDecimal();
   }

   if (highSegment - lowSegment < 2)
   {
      auto d = (qint64)divisor.at(highSegment);

      if (lowSegment < highSegment)
      {
         d *= (qint64)segmentBase;
         d += divisor.at(lowSegment);
      }

      // Fractional digits of the new divisor d (can be negative!)
      auto f = (int)divisor._data->fracSize - lowSegment * digitsPerBlock;

      // Reduce the fractional digits by reducing the divisor
      while ((f > 0 || d > INT_MAX) && (d % 10LL) == 0)
      {
         d /= 10LL;
         f--;
      }

      if (d <= INT_MAX)
      {
         if (divisor._data->negative)
            d *= -1;
         dividend <<= f;
         auto r = dividend.devideBy((qint32)d);
         *this = dividend;

         QDecimal remainder(r);

         remainder <<= (int)divisor._data->fracSize - f;

         remainder._data->fracSize = dividend._data->fracSize + divisor._data->fracSize;

         return remainder;
      }
   }

   // Decimal shift the dividend by at least the fractional digits of the divisor to be able to ignore fracSize and to work furthermore with the mantissas only.
   // This is equal to shifting the decimal point of both the divisor and the dividend to the right until the divisor has no more fractional digits.

   auto divisorScale = (int)divisor._data->fracSize;

   if (dividend._data->fracSize + divisorScale > maxFrac)
      divisorScale = maxFrac - dividend._data->fracSize;

   if (divisorScale)
   {
      dividend <<= divisorScale; // Grants exclusive access
      dividend._data->fracSize += divisorScale;
   }

   // Calculate the smallest decimal offset of the numerator so that the divisor is greater than the numerator

   int offset = 0;

   do
   {
      offset++;
   } while (!dividend.mantissaLess(offset, divisor));

   // Devide the mantissa of the dividend (numerator) by the mantissa of the divisor

   quint32  segment = 0;
   int      leftSegments = (int)dividend._data->blocks; // The number of segments is constant during the operation

   while (offset--)
   {
      // Devide the shifted numerator by the divisor (through counting how often the divisor can be substracted from the numerator)

      int r = 0; // How often divisor is contained in the shifted mantissa (0 to 9)

      while (!dividend.mantissaLess(offset, divisor))
      {
         // Substract the divisor from the shifted numerator

         quint32 carry = 1;

         for (int i = 0; i < leftSegments; ++i)
         {
            carry += (quint32)segmentBase - 1 - divisor.segmentValue(i, -offset) + dividend.segmentValue(i);
            dividend.set(i, carry % (quint32)segmentBase);
            carry /= (quint32)segmentBase;
         }

         // Count the number of substractions

         r++;
      }

      // Add the result of the division to the current segment

      segment *= 10;
      segment += r;

      // Add the segment to the result if it is full

      if ((offset % digitsPerBlock) == 0)
      {
         set(offset / digitsPerBlock, segment);
         segment = 0;
      }
   }

   // Make the _data pointer valid

   cow();

   // Set fractional size and sign of the result

   _data->fracSize = dividend._data->fracSize - divisor._data->fracSize;
   if (!isZero())
      _data->negative = (dividend._data->negative == divisor._data->negative) ? 0 : 1;

   return dividend; // return the remainder
}

bool QDecimal::negate()
{
   if (isZero())
      return false;
   cow(); // Get exclusive access
   _data->negative ^= 1;
   return true;
}

bool QDecimal::isZero() const
{
   if (_data)
   {
      int i = (int)_data->blocks;

      while (i--)
         if (at(i))
            return false;
   }
   return true;
}

int QDecimal::length() const
{
   auto digits = this->digits();
   auto scale = this->fracSize();

   if (digits < scale)
      digits = scale;

   if (digits == scale)
      digits++; // Add the preceding 0 digit

   return digits;
}

int QDecimal::reciprocalMostSignificantDigit() const
{
   if (_data == nullptr)
      return 0;

   auto digits = 0;

   for (int iBlock = (int)_data->blocks; iBlock--; )
   {
      quint32 segment = at(iBlock);
      quint32 magnitude = 1u;

      while (segment > magnitude)
      {
         digits++;
         magnitude *= 10;
      }
      if (segment)
      {
         digits += iBlock * digitsPerBlock;
         if (segment == magnitude)
         {
            while (iBlock--)
            {
               if (at(iBlock))
               {
                  digits++;
                  break;
               }
            }
         }
         break;
      }
   }

   return (int)_data->fracSize - digits;
}

void QDecimal::shiftDecimal(int distance)
{
   int fracDigits = fracSize() - distance;

   if (fracDigits < 0)
   {
      setMinFractionalDigits(distance);
      fracDigits = 0;
   }
   else if (fracDigits > maxFrac)
      fracDigits = maxFrac;

   if (_data)
   {
      cow(); // make sure we have exclusive data access
      _data->fracSize = fracDigits;
   }
}

QDecimal& QDecimal::moveDecimalPoint(int digits)
{
   shiftDecimal(digits);
   return *this;
}

QDecimal QDecimal::movedDecimalPoint(int digits) const
{
   QDecimal result(*this);

   result.shiftDecimal(digits);

   return result;
}

void QDecimal::setMinFractionalDigits(int fracDigits)
{
   if (fracDigits < 0)
      fracDigits = 0;
   else if (fracDigits > maxFrac)
      fracDigits = maxFrac;

   int offset = fracDigits - fracSize();

   if (offset > 0)
   {
      operator<<=(offset);

      // Exclusive access granted by at least one call to set()
      _data->fracSize = fracDigits;
   }
}

QDecimal& QDecimal::round(int precision)
{
   if (precision < 0)
      return *this;

   if (precision > maxFrac)
      precision = maxFrac;

   int offset = fracSize() - precision;

   if (offset > 0)
   {
      // Round mathematical (the value is always positive)
      // TODO: implement other rounding strategies
      if ((at((offset - 1) / digitsPerBlock) / decBase[(offset - 1) % digitsPerBlock]) % 10 >= 5)
         incrementAt(offset);

      // Reduce fractional part
      for (int i = 0; i < segmentCount(); ++i)
      {
         set(i, segmentValue(i, offset));
      }
      // Exclusive access is granted by at least one call to set()
      _data->fracSize = precision;
   }
   else
   {
      setMinFractionalDigits(precision);
   }

   return *this;
}

QDecimal QDecimal::rounded(int precision) const
{
   QDecimal result(*this);

   result.round(precision);

   return result;
}

QDecimal &QDecimal::truncate(int fracDigits)
{
   int clipDigits = 0;

   if (fracDigits < 0)
   {
      // Truncate only 0 digits
      fracDigits = ~fracDigits;

      for (clipDigits = 0; clipDigits < fracSize() - fracDigits; ++clipDigits)
      {
         if (at(clipDigits / digitsPerBlock) % decBase[(clipDigits % digitsPerBlock) + 1])
            break;
      }
      fracDigits = fracSize() - clipDigits;
   }
   else
      clipDigits = fracSize() - fracDigits;

   if (clipDigits > 0)
   {
      operator>>=(clipDigits);
      // Exclusive access is granted by at least one call to set()
      _data->fracSize = fracDigits;
   }

   return *this;
}

QDecimal QDecimal::truncated(int fracDigits) const
{
   QDecimal result(*this);

   result.truncate(fracDigits);

   return result;
}

QString QDecimal::toString(const QLocaleEx& locale) const
{
   return toString((locale.numberOptions() & QLocale::OmitGroupSeparator) ? GS_OmitGroupSeparator : GS_Auto, locale);
}

QString QDecimal::toString(GroupSeparatorMode groupSeparatorMode, const QLocaleEx& locale) const
{
   QString result;

   if (_data == nullptr)
      return result;

   if (!locale.groupSeparator().isPrint())
      groupSeparatorMode = GS_OmitGroupSeparator;
   else if (groupSeparatorMode == GS_Auto)
      groupSeparatorMode = _data->fracSize > 0 ? GS_UseGroupSeparator : GS_OmitGroupSeparator;

   int d = length();
   int i = d;

   if (_data->negative)
      i++; // Sign
   if (_data->fracSize)
      i++; // Decimal point
   if (groupSeparatorMode == GS_UseGroupSeparator)
      i += (d - (int)_data->fracSize - 1) / 3; // Thousand separators

   result.resize(i);

   i = 0;

   if (_data->negative)
      result[i++] = locale.negativeSign();

   quint32 segment = 0;

   if (d % digitsPerBlock)
      segment = at(d / digitsPerBlock);

   while (d--)
   {
      if ((d % digitsPerBlock) == 8)
         segment = at(d / digitsPerBlock);

      quint32 digit = (segment / decBase[d % digitsPerBlock]) % 10;

      if (d + 1 == (int)_data->fracSize)
         result[i++] = locale.decimalPoint();
      result[i++] = QChar::fromLatin1('0' + (char)digit);
      if (groupSeparatorMode == GS_UseGroupSeparator && d > (int)_data->fracSize && (d - (int)_data->fracSize) % 3 == 0)
         result[i++] = locale.groupSeparator();
   }

   return result;
}

bool QDecimal::fromString(const QString& number, const QLocaleEx& locale)
{
   release();

   int      i = 0;
   bool     ok = true;
   quint32  segment = 0;
   int      fracDigits = 0;
   bool     isNegative = false;

   for (int iChar = number.length(); iChar--; )
   {
      QChar qChar = number.at(iChar);

      if (isNegative && qChar != QLatin1Char(' '))
         ok = false;

      if (qChar == locale.decimalPoint())
      {
         if (fracDigits || i == 0 || i > maxFrac)
            ok = false;
         fracDigits = i;
      }
      else if (qChar.isDigit())
      {
         segment += (quint32)qChar.digitValue() * decBase[i % digitsPerBlock];
         i++;
         if ((i % digitsPerBlock) == 0)
         {
            set((i / digitsPerBlock) - 1, segment);
            segment = 0;
         }
      }
      else if (qChar == locale.negativeSign())
         isNegative = true;
      else if ((locale.numberOptions() & QLocale::RejectGroupSeparator) || qChar != locale.groupSeparator())
         ok = false;
   }
   if (i % digitsPerBlock)
      set(i / digitsPerBlock, segment);

   if (_data)
   {
      _data->fracSize = fracDigits;
      _data->negative = isNegative ? 1 : 0;
   }

   return ok;
}

void QDecimal::fromString(const char* szNumber, int len)
{
   release();

   if (szNumber == nullptr || *szNumber == '\0' || len == 0)
      return;

   const char* pChar = szNumber;

   if (len > 0)
      pChar += len;
   else
   {
      while (*pChar != '\0')
         pChar++;
   }

   int      i = 0;
   quint32  segment = 0;
   int      fracDigits = 0;
   bool     isNegative = false;

   while (pChar != szNumber)
   {
      pChar--;
      if (*pChar == '.')
         fracDigits = i;
      else if (*pChar >= '0' && *pChar <= '9')
      {
         segment += (quint32)(*pChar - '0') * decBase[i % digitsPerBlock];
         i++;
         if ((i % digitsPerBlock) == 0)
         {
            set((i / digitsPerBlock) - 1, segment);
            segment = 0;
         }
      }
      else if (*pChar == '-')
         isNegative = true;
   }
   if (i % digitsPerBlock)
      set(i / digitsPerBlock, segment);

   if (_data)
   {
      _data->fracSize = fracDigits;
      _data->negative = isNegative ? 1 : 0;
   }
}

qint64 QDecimal::toInt64(bool* ok) const
{
   auto result = (qint64)internalToUInt64(ok);

   if (ok && result < 0)
      *ok = false;

   if (isNegative())
      return -result;
   return result;
}

void QDecimal::fromInt64(qint64 number)
{
   release();

   int   i = 0;
   bool  isNegative = false;

   if (number < 0)
   {
      isNegative = true;
      number = -number;
   }

   do
   {
      set(i++, number % segmentBase);
      number /= segmentBase;
   } while (number);

   _data->negative = isNegative ? 1 : 0;
}

quint64 QDecimal::toUInt64(bool* ok) const
{
   auto result = internalToUInt64(ok);

   if (ok && *ok)
      *ok = !isNegative();

   return result;
}

void QDecimal::fromUInt64(quint64 number)
{
   release();

   int i = 0;

   do
   {
      set(i++, number % segmentBase);
      number /= segmentBase;
   } while (number);
}

int QDecimal::toInt(bool* ok) const
{
   auto result = toInt64(ok);

   if (ok && *ok && result > std::numeric_limits<int>::max())
      *ok = false;

   return (int)result;
}

void QDecimal::fromInt(int number)
{
   fromInt64(number);
}

unsigned int QDecimal::toUInt(bool* ok) const
{
   auto result = toUInt64(ok);

   if (ok && *ok && result > std::numeric_limits<unsigned int>::max())
      *ok = false;

   return (unsigned int)result;
}

void QDecimal::fromUInt(unsigned int number)
{
   fromUInt64(number);
}

double QDecimal::toDouble(bool* ok) const
{
   double result = 0;

   for (int i = segmentCount(); i--; )
   {
      result *= segmentBase;
      result += at(i);
   }
   for (int i = fracSize(); i--; )
      result /= 10;

   if (ok)
   {
      *ok = (_data != nullptr && (_data->blocks < 2 || at(1) * segmentBase + at(0) < 0x10000000000000)); // One situation is not covered when the most significant bit is above the 52nd but the lower bits are 0 which is rather unlikely.
   }

   if (isNegative())
      return -result;
   return result;
}

void QDecimal::fromDouble(double number)
{
   release();

   // Adjust the mantissa for a decimal exponent
   quint64  mantissa = 0ULL;
   int      exponent = 0;
   bool     sign = false;

   splitDouble(number, mantissa, exponent, sign); // The exponent is binary!
   if (!mantissaIsValid(mantissa))
      return;
   transformExponent(mantissa, exponent, 2, 10, 1ULL << 52);

   int i = 0;

   if (exponent > 0)
   {
      i = exponent / digitsPerBlock;
      set(i++, (quint32)(mantissa % (quint64)decBase[digitsPerBlock - (exponent % digitsPerBlock)]) * decBase[exponent % digitsPerBlock]);
      mantissa /= (quint64)decBase[digitsPerBlock - (exponent % digitsPerBlock)];
   }
   else
   {
      set(i++, (quint32)(mantissa % segmentBase));
      mantissa /= segmentBase;
   }

   while (mantissa)
   {
      set(i++, (quint32)(mantissa % segmentBase));
      mantissa /= segmentBase;
   }

   if (exponent < 0)
      _data->fracSize = -exponent;

   _data->negative = sign ? 1 : 0;
}

void QDecimal::toClsDecimal(quint32 clsDecimal[]) const
{
   unsigned long long int m[] = { 0, 0, 0 };

   for (int i = segmentCount(); i--; )
   {
      unsigned long long int r = (unsigned long long int)at(i);

      for (int p = 0; p < 3; ++p)
      {
         m[p] *= segmentBase;
         m[p] += r;
         r = m[p] >> 32;
         m[p] &= 0xFFFFFFFFULL;
      }
   }

   for (int p = 0; p < 3; ++p)
      clsDecimal[p] = (quint32)m[p];

   clsDecimal[3] = (quint32)fracSize() << 16;
   if (isNegative())
      clsDecimal[3] |= 0x80000000UL;
}

QDecimal &QDecimal::fromClsDecimal(quint32 clsDecimal[4])
{
   release();

   /*
   Die binäre Darstellung einer Decimal-Zahl besteht aus einem 1-Bit-Vorzeichen, einer 96-Bit-Ganzzahl und einem Skalierungsfaktor, der zum Dividieren der Ganzzahl verwendet wird und angibt, welcher Teil ein Dezimalbruch ist. Der Skalierfaktor ist implizit die Zahl 10, potenziert mit einem Exponenten im Bereich von 0 bis 28.

   Der Rückgabewert ist ein Array von 32-Bit-Ganzzahlen mit Vorzeichen, das aus vier Elementen besteht.

   Das erste, zweite und dritte Element des zurückgegebenen Arrays enthält jeweils die unteren, mittleren und oberen 32 Bits der 96-Bit-Ganzzahl.

   Das vierte Element des zurückgegebenen Arrays enthält den Skalierungsfaktor und das Vorzeichen.Es besteht aus folgenden Teilen:

   Die Bits 0 bis 15, das niederwertige Wort, werden nicht genutzt und müssen 0 (null) sein.

   Die Bits 16 bis 23 müssen einen Exponenten zwischen 0 (null) und 28 enthalten, der die Zehnerpotenz angibt, durch die die Ganzzahl dividiert werden soll.

   Die Bits 24 bis 30 werden nicht genutzt und müssen 0 (null) sein.

   Bit 31 enthält das Vorzeichen; 0 heißt positiv und 1 heißt negativ.

   Beachten Sie, dass die Bitdarstellung zwischen +0 und -0 unterscheidet.Diese Werte werden bei allen Operationen als gleiche Werte behandelt.
   */
   unsigned long long int d[3];

   for (int p = 0; p < 3; ++p)
   {
      d[p] = clsDecimal[p];
   }

   int i = 0;
   int h = 2;

   do
   {
      for (int p = h; p--; )
      {
         if (d[p + 1])
         {
            d[p] += (d[p + 1] % segmentBase) << 32;
            d[p + 1] /= segmentBase;
         }
         else if (p + 1 == h)
            h--;
      }
      set(i++, d[0] % segmentBase);
      d[0] /= segmentBase;
   } while (d[0] || h);

   _data->fracSize = (clsDecimal[3] >> 16) & 0xFF;
   _data->negative = clsDecimal[3] >> 31;

   return *this;
}

void QDecimal::toOciNumber(OCINumber *ociNumber) const
{
   if (ociNumber == nullptr)
      return;

   quint64  segment = 0ULL;
   quint64  d = 1ULL;
   int      ociExp = 0;
   int      pos = 2;

   if ((fracSize() + segmentCount()) % 2)
      d = 10ULL;

   for (int i = segmentCount(); i--; )
   {
      segment *= segmentBase;
      segment += segmentValue(i);
      if (i == 0 && d == 1ULL)
      {
         // Make it even
         segment *= 10ULL;
         d *= 10ULL;
      }
      d *= segmentBase;

      while (d >= 100ULL)
      {
         d /= 100ULL;

         int base100 = (int)(segment / d);

         if (ociExp || base100)
         {
            if (pos < OCI_NUMBER_SIZE)
               ociNumber->OCINumberPart[pos++] = (unsigned char)(_data->negative ? 101 - base100 : base100 + 1);
            ociExp++;
         }

         segment -= (quint64)base100 * d;
      }
   }

   if (ociExp)
   {
      while (pos > 2 && ociNumber->OCINumberPart[pos - 1] == (_data->negative ? 101 : 1))
         pos--; // Remove trailing zeros
      if (pos < OCI_NUMBER_SIZE)
      {
         // Append terminator
         if (_data->negative)
            ociNumber->OCINumberPart[pos++] = 102; // The terminator is part of the number
         else
            ociNumber->OCINumberPart[pos] = 0;
      }
      ociExp -= 1 + (fracSize() + 1) / 2;
   }
   else
      ociExp = -65;

   ociNumber->OCINumberPart[0] = pos - 1;
   ociNumber->OCINumberPart[1] = ociExp + 65 + 128;

   if (pos > 2 && _data->negative)
      ociNumber->OCINumberPart[1] = ~(ociNumber->OCINumberPart[1]);
}

QDecimal &QDecimal::fromOciNumber(const OCINumber *ociNumber)
{
   release();

   if (ociNumber == nullptr || ociNumber->OCINumberPart[0] == 0)
      return *this;

   int   pos = ociNumber->OCINumberPart[0] + 1;
   bool  isNegative = (ociNumber->OCINumberPart[1] < 128);
   int   exponent = (char)((isNegative ? ~ociNumber->OCINumberPart[1] : ociNumber->OCINumberPart[1]) - 128) - 64;
   // The exponent is now be between -64 and 63 meaning a decimal exponent of -128 to 126!!!

   quint64  segment = 0ULL;
   quint64  d = 1ULL;
   int      i = 0;

   if (isNegative && pos > 2 && ociNumber->OCINumberPart[pos - 1] == 102)
      pos--;

   if (exponent > pos - 2)
   {
      int excess = exponent - pos + 2;

      excess *= 2;
      i = excess / digitsPerBlock;
      excess %= digitsPerBlock;
      while (excess--)
         d *= 10ULL;
   }

   while (pos-- > 2)
   {
      int base100 = (isNegative ? 101 - ociNumber->OCINumberPart[pos] : ociNumber->OCINumberPart[pos] - 1);

      segment += (quint64)base100 * d;
      exponent--;

      d *= 100ULL;

      if (d >= segmentBase)
      {
         set(i++, (quint32)(segment % segmentBase));
         segment /= segmentBase;
         d /= segmentBase;
      }
   }
   if (segment)
      set(i++, (quint32)segment);

   if (_data)
   {
      if (exponent < 0)
         _data->fracSize = -exponent * 2;
      if (isNegative)
         _data->negative = 1;
   }
   else
      set(0, 0); // The oci number cannot be null

   return *this;
}

bool QDecimal::toOdbcNumber(void *odbcNumber, int maxSize) const
{
   Q_ASSERT((maxSize % 4) == 0);

   if (odbcNumber == nullptr || maxSize <= 0)
      return false;

   memset(odbcNumber, 0, maxSize);

   static_assert(sizeof(quint32) == blockSize, "toOdbcNumber: blockSize is not equal to size of quint32");

   if (_data && _data->blocks)
   {
      int      sigCount = _data->blocks;
      quint32  *source = new quint32[sigCount];
      quint32  *dest = (quint32 *)odbcNumber;
      int      ni = 0;

      memcpy(source, _data + 1, sigCount * blockSize);

      while (sigCount)
      {
         quint64 valPart = 0ULL;

         for (int i = sigCount; i--; )
         {
            valPart *= segmentBase;
            valPart += source[i];
            source[i] = valPart >> 32;
            valPart &= 0xFFFFFFFFULL;
            if (i + 1 == sigCount && source[i] == 0)
               sigCount--;
         }

         if (ni < maxSize / (int)sizeof(quint32))
            dest[ni] = (quint32)valPart;
         else if (valPart)
         {
            // Overflow
            delete[] source;
            return false;
         }
         ni++;
      }

      delete[] source;
   }

   return true;
}

// The odbcNumber is an integer mantissa with the precision of size bytes
QDecimal &QDecimal::fromOdbcNumber(const void *odbcNumber, int size, int scale, bool negative)
{
   release();

   if (odbcNumber == nullptr || size <= 0 || scale < 0)
      return *this;

   auto sigCount = (size + 3) / 4; // Number of significant 32 bit integers
   auto value = new quint32[sigCount]; // Buffer for the odbc number
   auto si = 0;

   // Copy the ODBC number to the buffer and fill up with zeros
   memset(value, 0, sigCount * sizeof(quint32));
   memcpy(value, odbcNumber, size);

   // Convert the ODBC number to segments
   while (sigCount)
   {
      auto segment = 0ULL;

      for (int i = sigCount; i--; )
      {
         segment <<= 32;
         segment |= value[i];
         value[i] = segment / segmentBase;
         segment %= segmentBase;
         if (i + 1 == sigCount && value[i] == 0)
            sigCount--;
      }

      if (si == 0 || segment)
         set(si, (quint32)segment);
      si++;
   }

   delete[] value;

   // Set scale and sign
   if (_data)
   {
      _data->fracSize = scale;
      _data->negative = negative ? 1 : 0;
   }

   return *this;
}

int QDecimal::digits() const
{
   if (_data == nullptr)
      return 0;

   int i = (int)_data->blocks;
   int digits = 0;

   while (i--)
   {
      quint32 segment = at(i);

      while (segment)
      {
         segment /= 10;
         digits++;
      }
      if (digits)
         break;
   }

   return digits + i * digitsPerBlock;
}

void QDecimal::significantDigits(int& digitCount, int& mostSignificantDigit, int& trailingZeros) const
{
   digitCount = 0;
   mostSignificantDigit = 0;
   trailingZeros = 0;

   // Number of trailing zeros

   for (int i = 0; i < (int)_data->blocks; ++i)
   {
      quint32 segment = at(i);

      if (segment)
      {
         while ((segment % 10) == 0)
         {
            trailingZeros++;
            segment /= 10;
         }
         break;
      }
      trailingZeros += digitsPerBlock;
   }

   // Calculate the number of significant digits

   auto divisorSegments = (int)_data->blocks;

   while (divisorSegments--)
   {
      quint32 segment = at(divisorSegments);

      while (segment)
      {
         mostSignificantDigit = segment % 10;
         segment /= 10;
         digitCount++;
      }
      if (digitCount)
         break;
   }

   digitCount += divisorSegments * digitsPerBlock;

   digitCount -= trailingZeros;
}

bool QDecimal::mantissaLess(int offset, const QDecimal &rhs) const
{
   int i;

   if (offset < 0)
      i = qMax(segmentCount() + (-offset + digitsPerBlock - 1) / digitsPerBlock, rhs.segmentCount());
   else
      i = qMax(segmentCount(), rhs.segmentCount() + (offset + digitsPerBlock - 1) / digitsPerBlock);

   while (i--)
   {
      quint32  left;
      quint32  right;

      if (offset < 0)
      {
         left = segmentValue(i, offset);
         right = rhs.segmentValue(i);
      }
      else
      {
         left = segmentValue(i);
         right = rhs.segmentValue(i, -offset);
      }

      if (left < right)
         return true;
      if (right < left)
         return false;
   }

   return false;
}

void QDecimal::incrementAt(int index)
{
   quint32 segment = decBase[index % digitsPerBlock];

   index /= digitsPerBlock;

   while (segment)
   {
      segment += at(index);
      set(index, segment % (quint32)segmentBase);
      segment /= (quint32)segmentBase;
      index++;
   }
}

void QDecimal::decrementAt(int index)
{
   quint32  segment = decBase[index % digitsPerBlock];
   int      end = segmentCount();

   index /= digitsPerBlock;

   do
   {
      if (at(index) < segment)
      {
         set(index, segment - at(index));
         segment = 1;
      }
      else
      {
         set(index, at(index) - segment);
         segment = 0;
      }
      index++;
   } while (segment && index < end);

   if (segment)
      _data->negative ^= 1;
   else if (isNegative() && isZero())
      _data->negative = 0; // A zero value has always positive sign
}

void QDecimal::add(const QDecimal &rhs, bool substract)
{
   if (_data == nullptr)
   {
      *this = rhs;
      if (substract && _data)
      {
         cow(); // Make sure to have exclusive access
         // Do not negate 0!
         if (_data->negative)
            _data->negative = 0;
         else if (!isZero())
            _data->negative = 1;
      }
   }
   else if (rhs._data)
   {
      setMinFractionalDigits(rhs._data->fracSize);

      int offset = _data->fracSize - rhs._data->fracSize;
      int end = qMax((int)_data->blocks, (int)rhs._data->blocks + ((offset + digitsPerBlock - 1) / digitsPerBlock));

      bool negateLeft = _data->negative;
      bool negateRight = substract != rhs._data->negative;

      quint32 carry = 0;

      if (negateLeft)
         carry++;
      if (negateRight)
         carry++;

      for (int i = 0; ; ++i)
      {
         if (negateLeft)
            carry += (quint32)segmentBase - at(i) - 1;
         else
            carry += at(i);
         if (negateRight)
            carry += (quint32)segmentBase - rhs.segmentValue(i, -offset) - 1;
         else
            carry += rhs.segmentValue(i, -offset);

         if (i == end)
         {
            Q_ASSERT(carry == 0 || carry == 1 || carry == (quint32)segmentBase - 1 || carry == (quint32)segmentBase || carry == (quint32)segmentBase * 2 - 2 || carry == (quint32)segmentBase * 2 - 1);
            if ((carry + 1) % (quint32)segmentBase == 0)
            {
               Q_ASSERT((carry == (quint32)segmentBase - 1 && negateLeft != negateRight) || (carry == (quint32)segmentBase * 2 - 1 && negateLeft && negateRight));
               // invert
               carry = 1;
               for (int i = 0; i < end; ++i)
               {
                  carry += (quint32)segmentBase - at(i) - 1;
                  set(i, carry % (quint32)segmentBase);
                  carry /= (quint32)segmentBase;
               }
               if (carry == 1) // We tried to invert 0!
                  _data->negative = 0;
               else
                  _data->negative = 1;
               break;
            }

            if (carry != 1 && carry != (quint32)segmentBase * 2 - 2)
            {
               Q_ASSERT((carry == 0 && !negateLeft && !negateRight) || (carry == (quint32)segmentBase && negateLeft != negateRight));
               _data->negative = 0;
               break;
            }

            end++;
         }

         set(i, carry % (quint32)segmentBase);
         carry /= (quint32)segmentBase;
         Q_ASSERT(carry == 0 || carry == 1);
      }
   }
}

quint64 QDecimal::internalToUInt64(bool *ok) const
{
   auto result = 0ULL;

   for (int i = segmentCount(); i--; )
   {
      if (ok)
      {
         // Check for overflow
         if (result * segmentBase / segmentBase != result)
            *ok = false;
      }
      result *= segmentBase;
      result += segmentValue(i, fracSize());
   }

   if (ok)
   {
      *ok = _data != nullptr; // NULL conversion is invalid

      // Check if all fractional digits are 0

      int i = fracSize() / digitsPerBlock;

      if (at(i) % decBase[fracSize() % digitsPerBlock])
         *ok = false;

      while (i--)
      {
         if (at(i))
         {
            *ok = false;
            break;
         }
      }
   }

   return result;
}

quint32 QDecimal::at(int index) const
{
   if (_data && index >= 0 && index < (int)_data->blocks)
      return ((quint32 *)(_data + 1))[index];
   return 0;
}

void QDecimal::set(int index, quint32 value)
{
   Q_ASSERT(index >= 0);

   if (index >= maxBlocks)
      return;

   cow(index);
   ((quint32 *)(_data + 1))[index] = value;
}

void QDecimal::cow(int index)
{
   if (index >= maxBlocks)
      return;

   if (_data == nullptr || _data->refCount.loadAcquire() > 1 || index >= (int)_data->blocks)
   {
      int newBlocks = index + 1;

      if (_data && (int)_data->blocks > newBlocks)
         newBlocks = (int)_data->blocks;

      Data* newData = new (malloc(sizeof(Data) + newBlocks * blockSize)) Data(newBlocks);

      if (_data)
      {
         newData->negative = _data->negative;
         newData->fracSize = _data->fracSize;
         memcpy(newData + 1, _data + 1, (int)_data->blocks * blockSize);
         memset((char*)(newData + 1) + (int)_data->blocks * blockSize, 0, (newBlocks - (int)_data->blocks) * blockSize);
      }
      else
         memset(newData + 1, 0, newBlocks * blockSize);

      release();
      _data = newData;
      bind();
   }
}

quint32 QDecimal::segmentValue(int index, int offset) const
{
   index += offset / digitsPerBlock;
   offset %= digitsPerBlock;

   quint64 r = 0ULL;

   if (offset < 0)
   {
      r = (quint64)at(index) * segmentBase + (quint64)at(index - 1);
      r /= decBase[digitsPerBlock + offset];
   }
   else if (offset > 0)
   {
      r = (quint64)at(index + 1) * segmentBase + (quint64)at(index);
      r /= decBase[offset];
   }
   else
      r = (quint64)at(index);

   return (quint32)(r % segmentBase);
}

static bool convert(QDecimal& lhs, int rhs, const QLocaleEx& locale)
{
   Q_UNUSED(locale);

   lhs = QDecimal(rhs);
   return true;
}

static bool convert(QDecimal& lhs, unsigned int rhs, const QLocaleEx& locale)
{
   Q_UNUSED(locale);

   lhs = QDecimal(rhs);
   return true;
}

static bool convert(QDecimal& lhs, long long int rhs, const QLocaleEx& locale)
{
   Q_UNUSED(locale);

   lhs = QDecimal(rhs);
   return true;
}

static bool convert(QDecimal& lhs, unsigned long long int rhs, const QLocaleEx& locale)
{
   Q_UNUSED(locale);

   lhs = QDecimal(rhs);
   return true;
}

static bool convert(QDecimal& lhs, double rhs, const QLocaleEx& locale)
{
   Q_UNUSED(locale);

   lhs = QDecimal(rhs);
   return true;
}

static bool convert(QDecimal& lhs, const QString& rhs, const QLocaleEx& locale)
{
   bool ok;

   lhs = QDecimal(rhs, locale, &ok);
   return ok;
}

static bool convert(QDecimal& lhs, const QByteArray& rhs, const QLocaleEx& locale)
{
   Q_UNUSED(locale);

   lhs = QDecimal(rhs.data());
   return true;
}

static bool convert(int& lhs, const QDecimal& rhs, const QLocaleEx& locale)
{
   Q_UNUSED(locale);

   bool ok;

   lhs = rhs.toInt(&ok);
   return ok;
}

static bool convert(unsigned int& lhs, const QDecimal& rhs, const QLocaleEx& locale)
{
   Q_UNUSED(locale);

   bool ok;

   lhs = rhs.toUInt(&ok);
   return ok;
}

static bool convert(long long int& lhs, const QDecimal& rhs, const QLocaleEx& locale)
{
   Q_UNUSED(locale);

   bool ok;

   lhs = rhs.toInt64(&ok);
   return ok;
}

static bool convert(unsigned long long int& lhs, const QDecimal& rhs, const QLocaleEx& locale)
{
   Q_UNUSED(locale);

   bool ok;

   lhs = rhs.toUInt64(&ok);
   return ok;
}

static bool convert(double& lhs, const QDecimal& rhs, const QLocaleEx& locale)
{
   Q_UNUSED(locale);

   bool ok;

   lhs = rhs.toDouble(&ok);
   return ok;
}

static bool convert(QString& lhs, const QDecimal& rhs, const QLocaleEx& locale)
{
   lhs = rhs.toString(locale);
   return true;
}

static bool convert(QByteArray& lhs, const QDecimal& rhs, const QLocaleEx& locale)
{
   lhs = rhs.toString(locale).toLatin1();
   return true;
}

BEGIN_VARIANT_REGISTRATION
   Variant::registerType<QDecimal>();

   Variant::registerConversion<QDecimal, int>();
   Variant::registerConversion<QDecimal, unsigned int>();
   Variant::registerConversion<QDecimal, long long int>();
   Variant::registerConversion<QDecimal, unsigned long long int>();
   Variant::registerConversion<QDecimal, double>();
   Variant::registerConversion<QDecimal, QString>();
   Variant::registerConversion<QDecimal, QText>();
   Variant::registerConversion<QDecimal, QByteArray>();
   Variant::registerConversion<int, QDecimal>();
   Variant::registerConversion<unsigned int, QDecimal>();
   Variant::registerConversion<long long int, QDecimal>();
   Variant::registerConversion<unsigned long long int, QDecimal>();
   Variant::registerConversion<double, QDecimal>();
   Variant::registerConversion<QString, QDecimal>();
   Variant::registerConversion<QByteArray, QDecimal>();
END_VARIANT_REGISTRATION
