#include "stdafx.h"
#include "QRational.h"
#include "TemplateConversions.h"
#include <QList>
//#include <QElapsedTimer>
//#include "PrimeNumbers.h"
#include "Variant.h"

const qint64 max_int64 = 0x7FFFFFFFFFFFFFFFLL;

using namespace std;

//static int getMaxBits(qint64 number)
//{
//   int maxBits = 0;
//
//   while ((1 << maxBits) < number)
//      maxBits++;
//
//   return maxBits;
//}

template <typename T>
inline T absolute(const T& number) { return (number < (T)0) ? -number : number; }

//struct PrimeState
//{
//   qint64 prime;
//   qint64 current;
//
//   PrimeState(qint64 first, qint64 second) : prime(first), current(second) {}
//};
//
//static qint64 primeNumber(int index) // Not thread safe!
//{
//   static QList<PrimeState>   primeList;
//   static qint64              test = 2;
//
//   // Prevent frozen applications
////   if (100000LL < end - test)
////      end = test + 100000LL;
//
//   while (primeList.count() <= index)
//   {
//      QList<PrimeState>::iterator it;
//
//      for (it = primeList.begin(); it != primeList.end(); ++it)
//      {
//         if (it->current < test)
//            it->current += it->prime;
//         if (it->current == test)
//            break;
//      }
//      if (it == primeList.end())
//         primeList.append(PrimeState(test, test));
//      test++;
//   }
//
//   return primeList.at(index).prime;
//}

#if 0
static inline qint64 primeNumber(int index)
{
   if ((size_t)index < sizeof(primeTable) / sizeof(*primeTable))
      return (qint64)primeTable[index];
   return 0LL;
}

// Reduce by prime numbers
static bool reduce(qint64& first, qint64& second, int& index, bool full = false)
{
   bool     reduced = false;
   qint64   minNumber = qMin(absolute(first), absolute(second));

   while (full || !reduced)
   {
      qint64 prim = primeNumber(index);

      if (prim < 1LL || minNumber < prim)
         break;

      while (first % prim == 0 && second % prim == 0)
      {
         first /= prim;
         second /= prim;
         reduced = true;
      }
      index++;
   }

   return reduced;
}
#endif

// Find the greates common divisor (GDC) using the Euclidean algorithm
qint64 greatestCommonDivisor(qint64 numerator, qint64 denominator)
{
   if (numerator < 0LL)
      numerator *= -1LL;
   if (denominator < 0LL)
      denominator *= -1LL;

   while (denominator != 0LL)
   {
      auto t = denominator;
      denominator = numerator % denominator;
      numerator = t;
   }

   return numerator;
}

// Reduce using the GDC
static bool reduce(qint64& first, qint64& second)
{
   if (first == 0LL)
      return false;

   auto gdc = greatestCommonDivisor(first, second);

   if (gdc == 1LL)
      return false;

   first /= gdc;
   second /= gdc;

   return true;
}

static inline bool tryMultiply(qint64& number, qint64 multiplicator)
{
   if (number == 0LL)
      return true;
   if (absolute(multiplicator) > max_int64 / absolute(number))
      return false; // Overflow
   number *= multiplicator;
   return true;
}

static bool tryMultiply(qint64& numerator, qint64& denominator, qint64 multiplicator)
{
   if (!tryMultiply(numerator, multiplicator))
   {
      // Overflow, try to reduce denominator
      if (denominator % multiplicator == 0LL)
      {
         denominator /= multiplicator;
         return true;
      }

      if (multiplicator % denominator == 0LL)
      {
         if (tryMultiply(numerator, multiplicator / denominator))
         {
            denominator = 1LL;
            return true;
         }
      }

      // Try to reduce generally
      do
      {
         if (!::reduce(numerator, denominator) && !::reduce(multiplicator, denominator))
         {
            if (denominator == 1LL || (absolute(numerator) == 1LL && absolute(multiplicator) == 1LL))
            {
               // Overflow
               denominator = 0LL;
               return false;
            }
            // Reduce the precision!
            if (absolute(numerator) > absolute(multiplicator))
               numerator /= 2LL;
            else
               multiplicator /= 2LL;
            denominator /= 2LL;
         }
      } while (!tryMultiply(numerator, multiplicator));
   }

   return true;
}

static bool tryMultiply(qint64& numerator, qint64& denominator, qint64 multNum, qint64 multDen)
{
   if (denominator < 1LL || multDen < 1LL)
      return false;

   auto numeratorReduction = 1LL;
   auto denominatorReduction = 1LL;

   if (!tryMultiply(numerator, multNum))
   {
      // Overflow, try to reduce denominators
      do
      {
         if (  !::reduce(numerator, denominator)
            && !::reduce(multNum, multDen)
            && !::reduce(numerator, multDen)
            && !::reduce(multNum, denominator)
            )
         {
            if ((denominator == 1LL && multDen == 1LL) || (absolute(numerator) == 1LL && absolute(multNum) == 1LL))
            {
               // Overflow
               denominator = 0LL;
               return false;
            }
            if (absolute(numerator) > absolute(multNum))
               numerator /= 2LL;
            else
               multNum /= 2LL;
            numeratorReduction *= 2LL;
         }
      } while (!tryMultiply(numerator, multNum));
   }
   if (!tryMultiply(denominator, multDen))
   {
      // Overflow, try to reduce numerator
      do
      {
         if (  !::reduce(numerator, denominator)
            && !::reduce(numerator, multDen)
            )
         {
            if ((denominator == 1LL && multDen == 1LL) || (absolute(numerator) == 1LL))
            {
               // Overflow
               denominator = 0LL;
               return false;
            }
            if (denominator > multDen)
               denominator /= 2LL;
            else
               multDen /= 2LL;
            denominatorReduction *= 2LL;
         }
      } while (!tryMultiply(denominator, multDen));
   }

   if (numeratorReduction > 1LL || denominatorReduction > 1LL)
   {
      if (numeratorReduction > denominatorReduction)
      {
         numeratorReduction /= denominatorReduction;
         denominatorReduction = 1LL;
      }
      else
      {
         denominatorReduction /= numeratorReduction;
         numeratorReduction = 1LL;
      }
      if (numeratorReduction > 1LL)
         denominator /= numeratorReduction;
      else if (denominatorReduction > 1LL)
         numerator /= denominatorReduction;
   }

   return true;
}

//static bool decInc(QString& strNumber, QChar zeroDigit)
//{
//   int   i = strNumber.length();
//   QChar qChar = zeroDigit;

//   while (qChar == zeroDigit && i--)
//   {
//      qChar = strNumber[i];

//      if (!qChar.isDigit())
//         qChar = zeroDigit;
//      else
//      {
//         if (qChar.digitValue() == 9)
//            qChar = zeroDigit;
//         else
//            qChar.unicode()++;
//         strNumber[i] = qChar;
//      }
//   }

//   return qChar == zeroDigit;
//}

QRational::QRational(const char* szNumber) : _numerator(0LL), _denominator(1LL)
{
   fromDecimalString(QString::fromLatin1(szNumber));
}

QRational::QRational(double number, qint64 base) : _numerator(0LL), _denominator(1LL)
{
   quint64  mantissa = 0ULL;
   int      exponent = 0;
   bool     sign = false;

   splitDouble(number, mantissa, exponent, sign);
   if (!mantissaIsValid(mantissa))
   {
      _denominator = 0LL;
      return;
   }

   transformExponent(mantissa, exponent, 2, (int)base, 1ULL << 52);

   // Take the mantissa as numerator
   _numerator = (qint64)mantissa;

   qint64   limit = max_int64 / base;
   int      cutDigit = 0;

   while (-exponent > limit)
   {
      // The exponent doesn't fit into the denominator
      cutDigit = (int)(_numerator % base);
      _numerator /= base;
      exponent++;
   }

   // Round mathematical (_numerator is always positive)
   // TODO: implement other rounding strategies
   if (cutDigit > ((int)base - 1) / 2)
      _numerator++;

   // Set the denominator
   while (exponent < 0)
   {
      _denominator *= base;
      exponent++;
   }
   // Adjust the numerator
   while (exponent > 0)
   {
      if (_numerator > limit)
      {
         // Overflow
         //_numerator = max_int64;
         _denominator = 0LL;
         break;
      }
      _numerator *= base;
      exponent--;
   }
   // Set the sign
   if (sign)
      _numerator *= -1LL;
}

QRational::QRational(const QString& number, const QLocaleEx& locale) : _numerator(0LL), _denominator(1LL)
{
   fromDecimalString(number, locale.zeroDigit(), locale.decimalPoint(), locale.groupSeparator(), locale.negativeSign());
}

QRational::QRational(QDecimal decimal) : _numerator(0LL), _denominator(1LL)
{
   int fracDigits = decimal.fractionalDigits();

   decimal.shiftDecimal(fracDigits);
   _numerator = decimal.toInt64();
   while (fracDigits--)
   {
      if ((_numerator % 10LL) == 0LL)
         _numerator /= 10LL;
      else
         _denominator *= 10LL;
   }
}

bool QRational::fromDecimalString(const QString& strNumber, QChar zeroDigit, QChar decimalSeparator, QChar thousandsSeparator, QChar negativeSign)
{
   bool  ok = true;
   bool  isNegative = false;
   int   pos = 0;

   if (strNumber.isEmpty())
      return ok;

   if (strNumber[pos] == negativeSign)
   {
      isNegative = true;
      pos++;
   }

   bool  isFraction = false;
   int   precision = 0;

   _numerator = 0LL;
   _denominator = 1LL;

   for (; pos < strNumber.length() && precision < 18; pos++)
   {
      QChar qChar = strNumber[pos];

      if (!isFraction && qChar == decimalSeparator)
         isFraction = true;
      else if (qChar.isDigit())
      {
         _numerator *= 10LL;
         _numerator += (qint64)qChar.digitValue();
         if (_numerator || isFraction)
            precision++;
         if (isFraction)
            _denominator *= 10LL;
      }
      else if (qChar != thousandsSeparator)
         ok = false;
   }

   // Test for insufficient precision
   if (isFraction)
   {
      // Pending zeros after the decimal point are not a problem
      for (; pos < strNumber.length(); pos++)
      {
         if (strNumber[pos] != zeroDigit)
            break;
      }
   }
   if (pos < strNumber.length())
      ok = false;

   if (isNegative)
      _numerator *= -1LL;

   return ok;
}

QString QRational::toDecimalString(int precision, bool useGroupSeparators, const QLocaleEx& locale) const
{
   if (_denominator < 1LL)
      return QString();

   bool noTrailingZeros = false;

   if (precision < 0)
   {
      precision *= -1;
      noTrailingZeros = true;
   }

   QRational decimal(*this);

   decimal.rebase(10LL);

   if (!decimal.isValid())
      return QString();

   int fractionalDigits = 0;

   while (decimal._denominator > 9LL)
   {
      fractionalDigits++;
      decimal._denominator /= 10LL;
   }

   int cutDigit = 0;

   while (precision < fractionalDigits)
   {
      cutDigit = decimal._numerator % 10LL;
      decimal._numerator /= 10LL;
      fractionalDigits--;
   }

   if (cutDigit >= 5)
      decimal._numerator++;

   if (noTrailingZeros)
   {
      if (fractionalDigits < precision)
         precision = fractionalDigits;
   }
   else
   {
      while (fractionalDigits < precision)
      {
         decimal._numerator *= 10LL;
         fractionalDigits++;
      }
   }

   QString result;

   integerToString(
      decimal._numerator,
      10,
      result,
      locale.negativeSign(),
      (useGroupSeparators && locale.groupSeparator().isPrint() ? locale.groupSeparator() : QChar()),
      precision,
      locale.decimalPoint()
      );

   return result;
}

int QRational::toInt(bool* ok) const
{
   if (isValid())
   {
      if (ok)
         *ok = true;
      return (int)(_numerator / _denominator);
   }
   if (ok)
      *ok = false;
   return 0;
}

unsigned int QRational::toUInt(bool* ok) const
{
   if (isValid() && _numerator >= 0)
   {
      if (ok)
         *ok = true;
      return (unsigned int)(_numerator / _denominator);
   }
   if (ok)
      *ok = false;
   return 0;
}

QDecimal QRational::toDecimal(bool* ok) const
{
   if (isValid())
   {
      QRational decimal(*this);

      decimal.rebase(10LL);

      if (decimal.isValid())
      {
         int fracDigits = 0;

         while (decimal._denominator > 9LL)
         {
            decimal._denominator /= 10LL;
            fracDigits++;
         }

         QDecimal result;

         result.fromInt64(decimal._numerator);
         result.shiftDecimal(-fracDigits);

         if (ok)
            *ok = true;

         return result;
      }
   }

   if (ok)
      *ok = false;

   return QDecimal();
}

bool QRational::operator==(const QRational& other) const
{
   return compare(other) == 0;
}

bool QRational::operator<(const QRational& other) const
{
   return compare(other) < 0;
}

int QRational::compare(const QRational& other) const
{
   QRational left(*this);
   auto right = other._numerator;

   // Invalid operands are always less than valid ones
   if (_denominator < 1LL || other._denominator < 1LL)
   {
      left._numerator = _denominator < 1LL ? 0LL : 1LL;
      right = other._denominator < 1LL ? 0LL : 1LL;
   }
   // The denominators are not relevant if one of the operands is 0 or both denominators are equal
   else if (_numerator != 0LL && other._numerator != 0LL && _denominator != other._denominator)
   {
      left *= other._denominator;

      if (!left.isValid())
         return false;

      left.reduce();

      if (!tryMultiply(right, left._denominator))
         return false;
   }

   return left._numerator == right ? 0 : left._numerator < right ? -1 : 1;
}

QRational& QRational::operator*=(qint64 number)
{
   if (!tryMultiply(_numerator, _denominator, number))
      _denominator = 0LL;
   return *this;
}

QRational& QRational::operator/=(qint64 number)
{
   if (!tryMultiply(_denominator, _numerator, number))
      _denominator = 0LL;
   return *this;
}

QRational& QRational::operator+=(const QRational& other)
{
   if (_denominator == other._denominator)
      _numerator += other._numerator;
   else
   {
      qint64  reducedDenominator(_denominator);
      qint64  otherReducedDenominator(other._denominator);

      ::reduce(reducedDenominator, otherReducedDenominator);
      _numerator *= otherReducedDenominator;
      _denominator *= otherReducedDenominator;
      _numerator += other._numerator * reducedDenominator;
   }

   return *this;
}

QRational& QRational::operator-=(const QRational& other)
{
   if (_denominator == other._denominator)
      _numerator -= other._numerator;
   else
   {
      qint64  reducedDenominator(_denominator);
      qint64  otherReducedDenominator(other._denominator);

      ::reduce(reducedDenominator, otherReducedDenominator);
      _numerator *= otherReducedDenominator;
      _denominator *= otherReducedDenominator;
      _numerator -= other._numerator * reducedDenominator;
   }

   return *this;
}

QRational& QRational::operator*=(const QRational& other)
{
   if (!tryMultiply(_numerator, _denominator, other._numerator, other._denominator))
      _denominator = 0LL;
   return *this;
}

QRational& QRational::operator/=(const QRational& other)
{
   if (!tryMultiply(_numerator, _denominator, other._denominator, other._numerator))
      _denominator = 0LL;
   return *this;
}

void QRational::reduce()
{
   ::reduce(_numerator, _denominator);
}

QRational QRational::rebase(qint64 base, int precision)
{
   if (_denominator < 1LL || base <= 0LL)
      return QRational();

   qint64 remainder = _numerator % _denominator;
   qint64 denominator = _denominator;

   _numerator /= _denominator;
   _denominator = 1LL;

   if (precision < 0)
      precision = INT_MAX;

   qint64 limit = max_int64 / base;

   while (remainder && precision--)
   {
      if (absolute(_numerator) > limit || _denominator > limit)
         break; // Precision exhausted
      _numerator *= base;
      _denominator *= base;
      if (!tryMultiply(remainder, denominator, base))
         return QRational(); // Overflow couldn't be prevented
      _numerator += remainder / denominator;
      remainder %= denominator;
   }

   // Mathematical rounding
   // TODO: Implement other rounding strategies
   if (remainder < 0LL)
   {
      // remainder is less than denominator so multiplying it with base is likely to succeed (otherwise we may just round to the wrong side)
      if ((-remainder) * base / denominator > (base - 1LL) / 2LL)
      {
         _numerator -= 1;
         remainder += denominator;
      }
   }
   else
   {
      // remainder is less than denominator so multiplying it with base is likely to succeed (otherwise we may just round to the wrong side)
      if (remainder * base / denominator > (base - 1LL) / 2LL)
      {
         _numerator += 1;
         remainder -= denominator;
      }
   }

   // Reduce after rounding
   while (_denominator >= base && (_numerator % base) == 0)
   {
      _numerator /= base;
      _denominator /= base;
   }

   return QRational(remainder, denominator);
}

QTCOREEX_EXPORT SerStream& operator>>(SerStream& stream, QRational& rational)
{
   if (stream.version() == 0)
   {
      QByteArray  serData = stream.readData();
      int         sepPos = serData.indexOf('/');

      if (sepPos > 0 && sepPos + 1 < serData.length())
         rational = QRational(serData.left(sepPos).toLongLong(), serData.mid(sepPos + 1).toLongLong());
      else
         stream.setError(__FUNCTION__, QLatin1String("Missing '/' separator"));
   }
   else
   {
      long long int numerator, denominator;

      stream >> numerator >> denominator;
      rational = QRational(numerator, denominator);
   }

   return stream;
}

//
// Conversion routines
//

static bool convert(int& lhs, const QRational& rhs, const QLocaleEx& locale)
{
   Q_UNUSED(locale);

   bool ok;

   lhs = rhs.toInt(&ok);
   return ok;
}

static bool convert(QRational& lhs, int rhs, const QLocaleEx& locale)
{
   Q_UNUSED(locale);

   lhs = QRational(rhs);
   return true;
}

static bool convert(unsigned int& lhs, const QRational& rhs, const QLocaleEx& locale)
{
   Q_UNUSED(locale);

   bool ok;

   lhs = rhs.toUInt(&ok);
   return ok;
}

static bool convert(QRational& lhs, unsigned int rhs, const QLocaleEx& locale)
{
   Q_UNUSED(locale);

   lhs = QRational((qint64)(quint64)rhs);
   return true;
}

static bool convert(QDecimal& lhs, const QRational& rhs, const QLocaleEx& locale)
{
   Q_UNUSED(locale);

   bool ok;

   lhs = rhs.toDecimal(&ok);
   return ok;
}

static bool convert(QRational& lhs, const QDecimal& rhs, const QLocaleEx& locale)
{
   Q_UNUSED(locale);

   lhs = QRational(rhs);
   return lhs.isValid();
}

static bool convert(QRational& lhs, const QString& rhs, const QLocaleEx& locale)
{
   lhs = QRational(rhs, locale);
   return lhs.isValid();
}

static bool convert(QString& lhs, const QRational& rhs, const QLocaleEx& locale)
{
   lhs = rhs.toString(locale);
   return true;
}

BEGIN_VARIANT_REGISTRATION
   Variant::registerType<QRational>();

   Variant::registerConversion<int, QRational>();
   Variant::registerConversion<QRational, int>();
   Variant::registerConversion<unsigned int, QRational>();
   Variant::registerConversion<QRational, unsigned int>();
   Variant::registerConversion<QDecimal, QRational>();
   Variant::registerConversion<QRational, QDecimal>();
   Variant::registerConversion<QRational, QString>();
   Variant::registerConversion<QRational, QText>();
   Variant::registerConversion<QString, QRational>();
END_VARIANT_REGISTRATION
