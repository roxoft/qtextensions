#include "stdafx.h"
#include "QSettingsFile.h"
#include <QFile>
#include <QTextStream>
#include <QDir>
#include <QCoreApplication>
#if QT_VERSION_MAJOR < 6
#include <QTextCodec>
#else
#include <QStringConverter>
#endif
#ifdef Q_OS_WIN
   #include <ShlObj.h>
#endif

QSettingsFile::QSettingsFile() : QSettingsExBase(new FileSetting(this))
{
}

QSettingsFile::QSettingsFile(const QString &applicationId, SerializeMode serializeMode)
   : QSettingsExBase(new FileSetting(this)), _serializeMode(serializeMode)
{
   if (open(applicationId))
      load();
}
QSettingsFile::QSettingsFile(const QString &applicationId, QSettingsEx::Scope scope, SerializeMode serializeMode)
   : QSettingsExBase(new FileSetting(this)), _serializeMode(serializeMode)
{
   if (open(applicationId, scope))
      load();
}

QSettingsFile::QSettingsFile(const QString &applicationId, const QString &path, SerializeMode serializeMode)
   : QSettingsExBase(new FileSetting(this)), _serializeMode(serializeMode)
{
   if (addIniFile(applicationId, path))
      load();
}

QSettingsFile::QSettingsFile(QIODevice *file, SerializeMode serializeMode)
   : QSettingsExBase(new FileSetting(this)), _serializeMode(serializeMode)
{
   if (addIniFile(file))
      load();
}

QSettingsFile::~QSettingsFile()
{
   if (isChanged())
      save();

   qDeleteAll(_ownFiles);
}

bool QSettingsFile::open(const QString& applicationId, QSettingsEx::Scope scope)
{
   if (applicationId.isEmpty())
      return false;

   _fileList.clear();

   if (scope & QSettingsEx::ApplicationScope)
   {
      if (!addIniFile(applicationId, qApp->applicationDirPath()))
         return false;
   }

#if defined Q_OS_WIN
   if (scope & (QSettingsEx::UserScope | QSettingsEx::SystemScope))
   {
      QString localPath;
      QString systemPath;

#if 0 // Not running with XP
      if (scope & UserScope)
      {
         PWSTR pszPath = NULL;

         if (S_OK == SHGetKnownFolderPath(FOLDERID_LocalAppData, 0, NULL, &pszPath))
         {
            localPath = QString::fromUtf16((const ushort *)pszPath);
            CoTaskMemFree(pszPath);
            pszPath = NULL;
         }
      }
      if (scope & SystemScope)
      {
         PWSTR pszPath = NULL;

         if (S_OK == SHGetKnownFolderPath(FOLDERID_ProgramData, 0, NULL, &pszPath))
         {
            systemPath = QString::fromUtf16((const ushort *)pszPath);
            CoTaskMemFree(pszPath);
            pszPath = NULL;
         }
      }
#endif
      wchar_t szPath[MAX_PATH];

      if ((scope & QSettingsEx::UserScope) && SUCCEEDED(SHGetFolderPathW(NULL, CSIDL_APPDATA, NULL, SHGFP_TYPE_CURRENT, szPath)))
         localPath = QString::fromUtf16((const char16_t *)szPath);

      if ((scope & QSettingsEx::SystemScope) && S_OK == SHGetFolderPathW(NULL, CSIDL_COMMON_APPDATA, NULL, SHGFP_TYPE_CURRENT, szPath))
         systemPath = QString::fromUtf16((const char16_t *)szPath);

      if (scope & QSettingsEx::UserScope)
      {
         if (localPath.isEmpty())
            localPath = QDir::homePath();
         if (!addIniFile(applicationId, localPath))
            return false;
      }
      if (scope & QSettingsEx::SystemScope)
      {
         if (systemPath.isEmpty())
            systemPath = QDir::rootPath();
         if (!addIniFile(applicationId, systemPath))
            return false;
      }
   }
#else
   if (scope & QSettingsEx::UserScope)
   {
      if (!addIniFile(applicationId, QDir::home().filePath(QLatin1String(".config"))))
         return false;
   }

   if (scope & QSettingsEx::SystemScope)
   {
      if (!addIniFile(applicationId, QLatin1String("/etc/xdg")))
         return false;
   }
#endif

   return true;
}

bool QSettingsFile::isOpen() const
{
   return !_fileList.isEmpty();
}

bool QSettingsFile::addIniFile(const QString &applicationId, const QString &path)
{
   QDir iniDir(path);

   iniDir.mkpath(QFileInfo(applicationId).path());

   QString filePath = iniDir.filePath(applicationId);

   if (!filePath.endsWith(QLatin1String(".ini"), Qt::CaseInsensitive))
      filePath += QLatin1String(".ini");

   return addIniFile(new QFile(filePath), true);
}

bool QSettingsFile::addIniFile(QIODevice *file, bool own)
{
   if (file == nullptr)
      return false;

   _fileList.append(file);
   if (own)
      _ownFiles.append(file);

   return true;
}

void QSettingsFile::setSerializeMode(SerializeMode serializeMode)
{
   _serializeMode = serializeMode;
}

void QSettingsFile::load()
{
   // Reload settings from file
   clear();

   const auto oldSeparator = keySeparator();

   if (!_sectionSeparator.isNull())
      setKeySeparator(_sectionSeparator);

#if QT_VERSION_MAJOR < 6
   QTextCodec* codec = nullptr;
#else
   std::optional<QStringConverter::Encoding> codec;
#endif

   // Start with the highest layer to load
   // The last codec specifies the codec for writing
   for (int i = _fileList.count(); i--; )
      codec = loadFile(i);

#if QT_VERSION_MAJOR < 6
   if (_textCodec == nullptr)
#else
   if (!_textCodec.has_value())
#endif
      _textCodec = codec;

   setKeySeparator(oldSeparator);

   setIsChanged(false);
}

void QSettingsFile::save()
{
   QIODevice *file = nullptr;

   if (!_fileList.isEmpty())
      file = _fileList.first();

   // Overwrite settings file
   if (file && file->open(QIODevice::WriteOnly | QIODevice::Text))
   {
      const auto oldSeparator = keySeparator();

      if (!_sectionSeparator.isNull())
         setKeySeparator(_sectionSeparator);

      QTextStream os(file);

#if QT_VERSION_MAJOR < 6
      if (_textCodec)
      {
         os.setCodec(_textCodec);
         os.setGenerateByteOrderMark(true);
      }
#else
      if (_textCodec.has_value())
      {
         os.setEncoding(_textCodec.value());
         os.setGenerateByteOrderMark(true);
      }
#endif

      Setting *root = currentSetting()->root();

      if (!root->remark().isEmpty())
         os << dynamic_cast<FileSetting *>(root)->fileRemark();

      if (!_groupByIndentation && !_groupByBraces)
         saveFileFlat(os, root);
      else
      {
         foreach (auto&& setting, root->settings())
         {
            saveFileHierarchic(os, setting, 0, _groupByBraces);
         }
      }

      if (!_bottomRemark.isEmpty())
         os << _bottomRemark;

      file->close();

      setKeySeparator(oldSeparator);

      setIsChanged(false);
   }
}

QString QSettingsFile::FileSetting::fileRemark() const
{
   QString result;

   for (int i = 0; i < linesBefore; ++i)
      result += QLatin1String("\n");

   auto remLines = remark().split(QLatin1Char('\n'));

   if (remLines.last().isEmpty())
      remLines.removeLast();

   foreach (const QString &line, remLines)
   {
      if (line.isEmpty())
         result += QLatin1String("#");
      else if (line.at(0) == QLatin1Char('#'))
         result += QString::fromLatin1("##") + line;
      else
         result += QString::fromLatin1("# ") + line;
      result += QLatin1String("\n");
   }

   for (int i = 0; i < linesBetween; ++i)
      result += QLatin1String("\n");

   return result;
}

void QSettingsFile::FileSetting::setFileRemark(QStringList &remLines)
{
   linesBefore = -1;
   linesBetween = 0;

   for (int i = 0; i < remLines.count(); ++i)
   {
      QString& line = remLines[i];

      if (linesBefore == -1)
      {
         if (!line.isEmpty())
            linesBefore = i;
      }
      else if (line.isEmpty())
      {
         linesBetween++;
      }
      else
      {
         linesBetween = 0;
      }

      if (line.startsWith(QLatin1Char('#')))
         line.remove(0, 1);
   }

   if (linesBefore == -1)
   {
      // No remark found
      linesBefore = remLines.count();
   }

   setRemark(QStringList(remLines.mid(linesBefore, remLines.count() - linesBefore - linesBetween)).join(QLatin1String("\n")));
}

static int removeIndentation(QString& line)
{
   int indentation = 0;
   int begin = 0;

   while (begin < line.size())
   {
      if (line.at(begin) == u' ')
         indentation++;
      else if (line.at(begin) == u'\t')
         indentation += 4;
      else
         break;
      begin++;
   }

   line.remove(0, begin);

   return indentation;
}

#if QT_VERSION_MAJOR < 6
QTextCodec* QSettingsFile::loadFile(int layer)
#else
QStringConverter::Encoding QSettingsFile::loadFile(int layer)
#endif
{
   QIODevice *file = _fileList.at(layer);
#if QT_VERSION_MAJOR < 6
   QTextCodec* codec = nullptr;
#else
   auto codec = _textCodec.has_value() ? _textCodec.value() : QStringConverter::Encoding::System;
#endif

   if (file && file->open(QIODevice::ReadOnly | QIODevice::Text))
   {
      QTextStream is(file);

#if QT_VERSION_MAJOR < 6
      if (_textCodec)
         is.setCodec(_textCodec);
#else
      is.setEncoding(codec);
#endif

      QStringList remLines;
      QList<QList<int>> levelIndentation; // The first level are braced blocks

      levelIndentation << QList<int>(); // First brace level

      while (!is.atEnd())
      {
         QString  line = is.readLine();
         int      indentation = removeIndentation(line);
         bool     isHint = false;

         if (line.startsWith(QLatin1String("{")))
         {
            levelIndentation << QList<int>(); // New brace level
            // Nothing is expected after an open brace
         }
         else if (line.startsWith(QLatin1String("}")))
         {
            // End all groups
            while (!levelIndentation.last().isEmpty())
            {
               endGroup();
               levelIndentation.last().pop_back();
            }
            levelIndentation.pop_back();
            // Nothing is expected after a close brace
         }
         else if (line.isEmpty() || line.startsWith(QLatin1Char('#')))
         {
            //if (!line.isEmpty() && remark.endsWith(QLatin1String("\n\n")) && _currentSetting->remark().isEmpty())
            if (line.isEmpty() && !remLines.isEmpty() && currentSetting()->remark().isEmpty())
            {
               dynamic_cast<FileSetting *>(currentSetting())->setFileRemark(remLines);
               remLines.clear();
            }
            remLines += line;
         }
         else if (line.startsWith(QLatin1Char('[')))
         {
            auto end = line.length();

            while (end--)
            {
               if (!line.at(end).isSpace())
               {
                  if (line.at(end) == u']')
                     end--;
                  break;
               }
            }
            end++;

            // End groups
            while (!levelIndentation.last().isEmpty() && (!_groupByIndentation || indentation <= levelIndentation.last().last()))
            {
               endGroup();
               levelIndentation.last().pop_back();
            }

            // Begin new groups
            auto levels = addGroup(line.mid(1, end - 1));

            while (levels--)
               levelIndentation.last() << indentation;

            if (!remLines.isEmpty())
            {
               dynamic_cast<FileSetting *>(currentSetting())->setFileRemark(remLines);
               remLines.clear();
            }
         }
         else
         {
            if (line.startsWith(QLatin1String("#>"))) // Thats a hint that the value is taken from a higher level
            {
               isHint = true;
               line.remove(0, 2);
            }

            // End groups depending on indentation
            while (_groupByIndentation && !levelIndentation.last().isEmpty() && indentation <= levelIndentation.last().last())
            {
               endGroup();
               levelIndentation.last().pop_back();
            }

            QString  key;
            QString  value;
            int      equalPos = line.indexOf(QLatin1Char('='));

            if (equalPos == -1)
            {
               key = line.trimmed();
            }
            else
            {
               value = line.mid(equalPos + 1);
               if (equalPos && line.at(equalPos - 1) == QLatin1Char('+'))
                  equalPos--; // There should already be a setting with that name
               key = line.left(equalPos).trimmed();
            }

            if (!key.isEmpty())
            {
               Variant variant;

               if (_serializeMode == SM_VariantSerialize)
                  variant.deserialize(value.toUtf8());
               else
                  variant = value;

               FileSetting *valueSetting = dynamic_cast<FileSetting *>(currentSetting()->addSetting(key));

               valueSetting->setValue(isHint ? Variant::null : variant, layer);

               if (!remLines.isEmpty())
               {
                  valueSetting->setFileRemark(remLines);
                  remLines.clear();
               }
            }
            else
            {
               // This is an error but the line is at least preserved as comment
               remLines += line;
            }
         }
      }

      // End all groups
      while (!levelIndentation.isEmpty())
      {
         while (!levelIndentation.last().isEmpty())
         {
            endGroup();
            levelIndentation.last().pop_back();
         }
         levelIndentation.pop_back();
      }

#if QT_VERSION_MAJOR < 6
      codec = is.codec();
#else
      codec = is.encoding();
#endif

      file->close();

      _bottomRemark = remLines.join(QLatin1Char('\n'));
   }

   return codec;
}

static void addIndentation(QTextStream& os, int indent)
{
   os << QString(indent / 4, u'\t');
   os << QString(indent % 4, u' ');
}

void QSettingsFile::saveFileHierarchic(QTextStream& os, Setting* settings, int indent, bool useBraces) const
{
   os << dynamic_cast<FileSetting *>(settings)->fileRemark();

   if (!settings->isGroup())
   {
      addIndentation(os, indent);
      if (settings->value(0).isNull())
         os << QString::fromLatin1("#>");
      os << settings->name() << QLatin1Char('=');
      if (_serializeMode == SM_VariantSerialize)
         os << QString::fromUtf8(settings->value().serialized().data());
      else
         os << settings->value().toString();
#if QT_VERSION < QT_VERSION_CHECK(5,14,0)
      os << endl;
#else
      os << Qt::endl;
#endif

      return;
   }

   // Group

   addIndentation(os, indent);
#if QT_VERSION < QT_VERSION_CHECK(5,14,0)
   os << QLatin1Char('[') << settings->name() << QLatin1Char(']') << endl;
#else
   os << QLatin1Char('[') << settings->name() << QLatin1Char(']') << Qt::endl;
#endif
   indent++;

   if (settings->name().startsWith(QLatin1Char('@')))
   {
      if (settings->settings().isEmpty())
         return; // Empty @-sections do not get empty braces
      useBraces = true;
   }

   if (useBraces)
   {
      addIndentation(os, indent);
#if QT_VERSION < QT_VERSION_CHECK(5,14,0)
      os << QLatin1Char('{') << endl;
#else
      os << QLatin1Char('{') << Qt::endl;
#endif
   }

   foreach (auto&& setting, settings->settings())
   {
      saveFileHierarchic(os, setting, indent, useBraces);
   }

   if (useBraces)
   {
      addIndentation(os, indent);
#if QT_VERSION < QT_VERSION_CHECK(5,14,0)
      os << QLatin1Char('}') << endl;
#else
      os << QLatin1Char('}') << Qt::endl;
#endif
   }
}

void QSettingsFile::saveFileFlat(QTextStream& os, Setting* group) const
{
   auto groupHeaderWritten = group->parent() == nullptr; // Initially the root group needs no group header
   QList<QSettingsExBase::Setting*> groups;

   for (auto&& setting : group->settings())
   {
      if (setting->isGroup())
      {
         groups.append(setting);
      }
      else
      {
         if (!groupHeaderWritten)
         {
            QString groupKey = setting->parent()->fullPath();

#if QT_VERSION < QT_VERSION_CHECK(5,14,0)
            os << QLatin1Char('[') << groupKey << QLatin1Char(']') << endl;
#else
            os << QLatin1Char('[') << groupKey << QLatin1Char(']') << Qt::endl;
#endif
            groupHeaderWritten = true;
         }

         auto fileRem = dynamic_cast<FileSetting *>(setting)->fileRemark();

         if (!fileRem.isEmpty())
         {
            os << fileRem;
         }

         if (setting->value(0).isNull())
            os << QString::fromLatin1("#>");
         os << setting->name() << QLatin1Char('=');
         if (_serializeMode == SM_VariantSerialize)
            os << QString::fromUtf8(setting->value().serialized().data());
         else
            os << setting->value().toString();
#if QT_VERSION < QT_VERSION_CHECK(5,14,0)
         os << endl;
#else
         os << Qt::endl;
#endif
      }
   }

   for (auto&& setting : groups)
   {
      auto fileRem = dynamic_cast<FileSetting *>(setting)->fileRemark();

      if (!fileRem.isEmpty())
      {
         os << fileRem;
      }

      saveFileFlat(os, setting);
   }
}
