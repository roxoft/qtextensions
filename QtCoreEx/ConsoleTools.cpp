#include "stdafx.h"
#include "ConsoleTools.h"
#include <QCoreApplication>
#include <stdio.h>
#include <QVector>

QStringList appArguments(const QList<ArgDef>& argDefs, const QStringList& cmdLineArgs, QMultiMap<QString, QString>& serviceArgs)
{
   QStringList errorList;
   QVector<bool> usedDefs(argDefs.count());

   for (auto arg : cmdLineArgs)
   {
      Q_ASSERT(!arg.isEmpty());

      if (arg.at(0) == u'-')
      {
         arg.remove(0, 1);

         QString key;
         QString value;
         const auto sepPos = arg.indexOf(u'=');

         if (sepPos != -1)
         {
            key = arg.left(sepPos).trimmed().toLower();
            value = arg.mid(sepPos + 1);
         }
         else
         {
            key = arg.trimmed().toLower();
         }

         if (key.isEmpty())
            errorList.append(QCoreApplication::translate("ConsoleTools::appArguments", "Missing name in option '%1'").arg(arg));
         else
         {
            for (auto iArg = 0; iArg < argDefs.count(); ++iArg)
            {
               auto& argDef = argDefs.at(iArg);

               if (!argDef._options.isEmpty())
               {
                  for (auto&& exclOpt : argDef._options)
                  {
                     if (exclOpt._optName.toLower() == key)
                     {
                        if (usedDefs.at(iArg) && !argDef._isMulti)
                        {
                           errorList.append(QCoreApplication::translate("ConsoleTools::appArguments", "Option '%1' already specified").arg(arg));
                        }
                        else
                        {
                           serviceArgs.insert(exclOpt._optName, value);
                           usedDefs[iArg] = true;
                        }
                        break;
                     }
                  }

                  if (usedDefs.at(iArg))
                  {
                     if (!value.isEmpty())
                     {
                        errorList.append(QCoreApplication::translate("ConsoleTools::appArguments", "Unexpected value for option '%1'").arg(arg));
                     }
                     key.clear();
                     break;
                  }
               }
               else if (!argDef._optName.isEmpty())
               {
                  if (argDef._optName.toLower() == key)
                  {
                     if (usedDefs.at(iArg) && !argDef._isMulti)
                     {
                        errorList.append(QCoreApplication::translate("ConsoleTools::appArguments", "Option '%1' already specified").arg(arg));
                     }
                     else
                     {
                        serviceArgs.insert(argDef._optName, value);
                        usedDefs[iArg] = true;
                     }
                     if (value.isEmpty() && !argDef._valueName.isEmpty())
                        errorList.append(QCoreApplication::translate("ConsoleTools::appArguments", "Missing value for option '%1'").arg(arg));
                     else if (!value.isEmpty() && argDef._valueName.isEmpty())
                        errorList.append(QCoreApplication::translate("ConsoleTools::appArguments", "Unexpected value for option '%1'").arg(arg));
                     key.clear();
                     break;
                  }
               }
            }

            if (!key.isEmpty())
            {
               errorList.append(QCoreApplication::translate("ConsoleTools::appArguments", "Option '%1' not expected").arg(arg));
            }
         }
      }
      else
      {
         auto valueDefined = false;

         for (auto iArg = 0; iArg < argDefs.count(); ++iArg)
         {
            auto& argDef = argDefs.at(iArg);

            if (argDef._optName.isEmpty() && !argDef._valueName.isEmpty() && (!usedDefs.at(iArg) || argDef._isMulti))
            {
               usedDefs[iArg] = true;
               serviceArgs.insert(argDef._valueName, arg);
               valueDefined = true;
               break;
            }
         }

         if (!valueDefined)
         {
            errorList.append(QCoreApplication::translate("ConsoleTools::appArguments", "Unexpected parameter '%1'").arg(arg));
         }
      }
   }

   for (auto iArg = 0; iArg < argDefs.count(); ++iArg)
   {
      auto& argDef = argDefs.at(iArg);

      if (argDef._isMandatory && !usedDefs.at(iArg))
      {
         if (!argDef._options.isEmpty())
         {
            QStringList options;

            for (auto&& exclOpt : argDef._options)
            {
               options.append(exclOpt._optName);
            }

            errorList.append(QCoreApplication::translate("ConsoleTools::appArguments", "Missing one of the options '%1'").arg(options.join(u'|')));
         }
         else if (!argDef._optName.isEmpty())
         {
            errorList.append(QCoreApplication::translate("ConsoleTools::appArguments", "Missing option '%1'").arg(argDef._optName));
         }
         else
         {
            errorList.append(QCoreApplication::translate("ConsoleTools::appArguments", "Missing argumnet"));
         }
      }
   }

   return errorList;
}

#if defined(Q_OS_WIN)

   #define WIN32_LEAN_AND_MEAN
   #include <Windows.h>

   static BOOL WINAPI HandlerRoutine(__in DWORD dwCtrlType)
   {
      Q_UNUSED(dwCtrlType);
      //switch (dwCtrlType)
      //{
      //case CTRL_C_EVENT:
      //   break;
      //case CTRL_BREAK_EVENT:
      //   break;
      //case CTRL_CLOSE_EVENT:
      //   break;
      //case CTRL_LOGOFF_EVENT:
      //   break;
      //case CTRL_SHUTDOWN_EVENT:
      //   break;
      //default:
      //   ;
      //}

      return exitApplication() ? TRUE : FALSE;
   }

#elif defined(Q_OS_UNIX)

   #include <csignal>

   sighandler_t handle_signal(int signum, sighandler_t signalhandler)
   {
      struct sigaction act, oldact;

      act.sa_handler = signalhandler;
      sigemptyset(&act.sa_mask);
      act.sa_flags = SA_RESTART;

      if (sigaction(signum, &act, &oldact) < 0)
         return SIG_ERR;

      return oldact.sa_handler;
   }

   void terminateSignalHandler(int /*sig*/)
   {
      exitApplication();
   }

#endif

bool installConsoleEventHandler()
{
#if defined(Q_OS_WIN)
   return SetConsoleCtrlHandler(HandlerRoutine, TRUE) != FALSE;
#else
   if (handle_signal(SIGINT, terminateSignalHandler) == SIG_ERR)
      return false;
   if (handle_signal(SIGQUIT, terminateSignalHandler) == SIG_ERR)
      return false;
   if (handle_signal(SIGTERM, terminateSignalHandler) == SIG_ERR)
      return false;
   return true;
#endif
}

void consoleOut(const QString &message, bool error)
{
   // Add a period to the message

   int      msgLen = message.length();
   QString  displayText;
   QChar    lastChar;

   if (msgLen)
      lastChar = message.at(msgLen - 1);

   if (lastChar.isNull() || lastChar == QLatin1Char('.') || lastChar == QLatin1Char('\n') || lastChar == QLatin1Char(':'))
      displayText = message;
   else
      displayText = message + QLatin1Char('.');

   // Write to stdout or stderr

   QByteArray buffer;

#if defined(Q_OS_WIN)
   auto codePage = GetConsoleOutputCP() == 850 ? CP_OEMCP : CP_UTF8;

   auto size = WideCharToMultiByte(codePage, 0, (wchar_t*)displayText.data(), displayText.length(), NULL, 0, NULL, NULL);

   if (size == 0)
   {
       qErrnoWarning(GetLastError(), "WideCharToMultiByte failed to convert the display text.");
       return;
   }

   buffer.resize(size);

   WideCharToMultiByte(codePage, 0, (wchar_t*)displayText.data(), displayText.length(), buffer.data(), buffer.length(), NULL, NULL);
#else
   buffer = displayText.toUtf8();
#endif

   buffer += "\n";
   fwrite(buffer, 1, buffer.length(), error ? stderr : stdout);
}

bool exitApplication()
{
   return QMetaObject::invokeMethod(qApp, "quit", Qt::QueuedConnection);
}
