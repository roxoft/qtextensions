Extended item model
===================

All classes for the extended item model start with HT for Hierarchical Table.

The principle is to have rows of Variants ordered in a hierarchical way.

Each row is a HtNode which can have any numbers of attributes for any number of roles. The attributes can be interpreted as columns for example in tree views.

A HtNode can contain other HtNodes and so on.

To use a hierarchy of HtNodes as model for any QAbstractItemView derived class you can wrap it in an HtItemModel.
