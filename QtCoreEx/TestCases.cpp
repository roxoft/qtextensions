#include "stdafx.h"
#include "TestCases.h"
#include "numeric.h"
#include "QRational.h"
#include "Variant.h"
#include <QList>
#include <QByteArray>
#include "HtStandardNode.h"
#include "QDecimal.h"
#include "QValue.h"
#include "QText.h"
#include <QTextCodec>
#include "QSettingsFile.h"
#include <limits>
#include "CsvParser.h"
#include "VarTree.h"
#include "SqlParser.h"
#include "CPPTokenizer.h"
#include "CPPExpression.h"
#include "CPPResolverFunctions.h"
#include "StringTemplate.h"
#include "QDateTimeEx.h"
#include "FunctionStatus.h"
#include "QtListExtensions.h"

#include "TemplateConversions.h"
#include "nullable.h"

static QString toQString(const QList<Variant>& list)
{
   QStringList result;

   for (auto&& entry : list)
      result << entry.toString();

   return result.join(";");
}

static QString toQString(const QStringList& list)
{
   return list.join(";");
}

static QString toQString(const QList<int>& list)
{
   QStringList result;

   for (auto&& entry : list)
      result << QString::number(entry);

   return result.join(";");
}

static QString toQString(const QRational &rational) { return rational.toFractionString(); }

void MiscTests::functionStatusTests()
{
   FunctionStatus status;

   status.addError("Test");

   T_EQUALS(status.completeMessage(), "Error: Test");

   status = FunctionStatus("Function");

   status.addError("Test");

   T_EQUALS(status.completeMessage(), "Function\n   Error: Test");

   auto subStatus = status.subStatus("Substatus");

   subStatus.addWarning("Another Test");

   T_EQUALS(status.completeMessage(), "Function\n   Error: Test\n\n   Substatus\n      Warning: Another Test");

   FunctionStatus otherStatus;

   otherStatus.addError("Test error");

   status.transferMessagesFrom(otherStatus);

   T_EQUALS(status.completeMessage(), "Function\n   Error: Test\n\n   Substatus\n      Warning: Another Test\n\n   Error: Test error");

   otherStatus = FunctionStatus("External function");

   otherStatus.addInformation("A substatus again");

   status.transferMessagesFrom(otherStatus);

   T_EQUALS(status.completeMessage(), "Function\n   Error: Test\n\n   Substatus\n      Warning: Another Test\n\n   Error: Test error\n\n   External function\n      Information: A substatus again");
}

void MiscTests::listExtensionsTests()
{
   QList<int> list1{ 0, 1, 2, 5 };
   QList<int> list2{ 0, 2, 3, 5 };

   auto zipped = zipOrdered(list1, list2, [](int left, int right) { return left - right; });

   QList<std::tuple<int, int>> expected{ { 0, 0 }, { 1, 0}, { 2, 2 }, { 0, 3 }, { 5, 5 } };

   T_IS_TRUE(zipped == expected);

   class Test1
   {
   public:
      int key = 0;
      QString data;

      bool operator==(const Test1& other) const { return key == other.key && data == other.data; }
   };

   class Test2
   {
   public:
      int key = 0;
      QDecimal data;

      bool operator==(const Test2& other) const { return key == other.key && data == other.data; }
   };

   QList<Test1> test1List{ { 1, "One" }, { 3, "Three" } };
   QList<Test2> test2List{ { 1, 1 }, { 2, 2 } };

   auto zipped2 = zipOrdered(test1List, test2List, [](const Test1& left, const Test2& right) { return left.key - right.key; });

   QList<std::tuple<Test1, Test2>> expected2{ { { 1, "One" }, { 1, 1 } }, { {0, QString()}, { 2, 2 } }, { { 3, "Three" }, { 0, QDecimal() } } };

   T_IS_TRUE(zipped2 == expected2);

   QList<Test1*> test1PList{ new Test1{ 1, "One" }, new Test1{ 3, "Three" } };
   QList<Test2*> test2PList{ new Test2{ 1, 1 }, new Test2{ 2, 2 } };

   auto zipped3 = zipOrdered(test1PList, test2PList, [](const Test1* left, const Test2* right) { return left->key - right->key; });

   QList<std::tuple<Test1*, Test2*>> expected3{ { test1PList[0], test2PList[0] }, { nullptr, test2PList[1] }, { test1PList[1], nullptr } };

   T_IS_TRUE(zipped3 == expected3);
}

void MiscTests::sortedHtNodesTests()
{
   HtStandardNode rootNode;

   for (auto i = 0; i < 10; ++i)
   {
      for (auto j = 0; j < 3; ++j)
      {
         auto node = rootNode.appendChild();
         node->setAttribute(0, i + 1);
      }
   }

   T_EQUALS(0, rootNode.lowerBound(0, 0));
   T_EQUALS(0, rootNode.upperBound(0, 0));
   T_EQUALS(0, rootNode.lowerBound(0, 1));
   T_EQUALS(3, rootNode.upperBound(0, 1));

   T_EQUALS(30, rootNode.lowerBound(0, 11));
   T_EQUALS(30, rootNode.upperBound(0, 11));
   T_EQUALS(27, rootNode.lowerBound(0, 10));
   T_EQUALS(30, rootNode.upperBound(0, 10));

   T_EQUALS(12, rootNode.lowerBound(0, 5));
   T_EQUALS(15, rootNode.upperBound(0, 5));
   T_EQUALS(12, rootNode.lowerBound(0, 5, Qt::DisplayRole, 15));
   T_EQUALS(15, rootNode.upperBound(0, 5, Qt::DisplayRole, 12));
   T_EQUALS(12, rootNode.lowerBound(0, 5, Qt::DisplayRole, 16));
   T_EQUALS(15, rootNode.upperBound(0, 5, Qt::DisplayRole, 11));
   T_EQUALS(12, rootNode.lowerBound(0, 5, Qt::DisplayRole, 14));
   T_EQUALS(15, rootNode.upperBound(0, 5, Qt::DisplayRole, 13));

   auto node = rootNode.appendChild();
   node->setAttribute(0, 3);

   T_EQUALS(6, rootNode.lowerBound(node));
   T_EQUALS(9, rootNode.upperBound(node));
}

void MiscTests::textCodecConversions()
{
   auto textCodec = QTextCodec::codecForName("ISO-8859-1");
   T_ASSERT(textCodec);

   auto latin1Text = textCodec->fromUnicode(QString("Falsches Üben von Xylophonmusik quält jeden größeren Zwerg"));
   T_EQUALS_EX(QString("Falsches Üben von Xylophonmusik quält jeden größeren Zwerg").toLatin1(), latin1Text);

   T_ASSERT(QTextCodec::codecForName("ibm850"));
   T_ASSERT(QTextCodec::codecForLocale());
   textCodec = QTextCodec::codecForName("iso-8859-1");
   T_ASSERT(textCodec);

   auto text = textCodec->toUnicode(latin1Text);
   T_EQUALS(QString("Falsches Üben von Xylophonmusik quält jeden größeren Zwerg"), text);
}

void ConversionTest::init()
{
   QLocaleEx qtLocale(QLocale::German);
   qtLocale.setDateFormat("dd.MM.yyyy");
   qtLocale.setTimeFormat("hh:mm:ss");
   qtLocale.setDateTimeFormat("dd.MM.yyyy hh:mm:ss");
   qtLocale.setDateTimeFormat(DateTimePart::Minute, DateTimePart::Hour, "hh:mm");
   qtLocale.setDateTimeFormat(DateTimePart::Minute, DateTimePart::Millennium, "dd.MM.yyyy hh:mm");

   _prevLocale = defaultLocale();

   setDefaultLocale(qtLocale);
}

static QString toQString(const QSize& size) { return QString::number(size.height()); }

static QString toQString(const QByteArray& ansiString) { return QString(ansiString); }

void ConversionTest::deinit()
{
   setDefaultLocale(_prevLocale);
}

void ConversionTest::doubleToString()
{
   T_EQUALS(QString::fromStdString(std::toString(0.)), QString("0"));
   T_EQUALS(QString::fromStdString(std::toString(1.)), QString("1"));
   T_EQUALS(QString::fromStdString(std::toString(1., 2)), QString("1.00"));
   T_EQUALS(QString::fromStdString(std::toString(12345.678, -2, '-', ',', '.')), QString("12.345,68"));
   T_EQUALS(QString::fromStdString(std::toString(.3, -16, '-', ',', '.')), QString("0,3"));
   T_EQUALS(QString::fromStdString(std::toString(.03, -16, '-', ',', '.')), QString("0,03"));
   T_EQUALS(QString::fromStdString(std::toString(123E-300, -16, '-', ',', '.')), QString("1,23E-298"));
   T_EQUALS(QString::fromStdString(std::toString(123E-16, -16, '-', ',', '.')), QString("0,0000000000000123"));
   T_EQUALS(QString::fromStdString(std::toString(123E16, -16, '-', ',', '.')), QString("1,23E18"));
   T_EQUALS(QString::fromStdString(std::toString(123E13, -16, '-', ',', '.')), QString("1.230.000.000.000.000"));
   T_EQUALS(QString::fromStdString(std::toString(-0.123456789, -16, '-', ',', '.')), QString("-0,123456789"));
   T_EQUALS(QString::fromStdString(std::toString(std::numeric_limits<double>::quiet_NaN())), QString("NaN"));
   T_EQUALS(QString::fromStdString(std::toString(std::numeric_limits<double>::infinity())), QString("INFINITY"));
   T_EQUALS(QString::fromStdString(std::toString(-std::numeric_limits<double>::infinity())), QString("-INFINITY"));
   T_EQUALS(QString::fromStdString(std::toString(3E-308, -16, '-', ',', '.')), QString("3E-308")); // Subnormal number test
   // Precision tests
   T_EQUALS(QString::fromStdString(std::toString(8.45, -16, '-', ',', '.')), QString("8,45"));
   T_EQUALS(QString::fromStdString(std::toString(1234567890.1234567, -16, '-', ',', '.')), QString("1.234.567.890,123457"));
   T_EQUALS(QString::fromStdString(std::toString(1834567890.1234567, -16, '-', ',', '.')), QString("1.834.567.890,123457"));
   T_EQUALS(QString::fromStdString(std::toString(3034567890.1234560, -16, '-', ',', '.')), QString("3.034.567.890,123456"));
   T_EQUALS(QString::fromStdString(std::toString(3034567890.1234561, -16, '-', ',', '.')), QString("3.034.567.890,123456"));
   T_EQUALS(QString::fromStdString(std::toString(3034567890.1234562, -16, '-', ',', '.')), QString("3.034.567.890,123456"));
   T_EQUALS(QString::fromStdString(std::toString(3034567890.1234567, -16, '-', ',', '.')), QString("3.034.567.890,123456"));
   T_EQUALS(QString::fromStdString(std::toString(3034567890.1234568, -16, '-', ',', '.')), QString("3.034.567.890,123457"));
   T_EQUALS(QString::fromStdString(std::toString(3034567890.1234569, -16, '-', ',', '.')), QString("3.034.567.890,123457"));
   T_EQUALS(QString::fromStdString(std::toString(9934567890.123450, -16, '-', ',', '.')), QString("9.934.567.890,12345"));
   T_EQUALS(QString::fromStdString(std::toString(9934567890.123451, -16, '-', ',', '.')), QString("9.934.567.890,12345"));
   T_EQUALS(QString::fromStdString(std::toString(9934567890.123458, -16, '-', ',', '.')), QString("9.934.567.890,12346"));
   T_EQUALS(QString::fromStdString(std::toString(9934567890.123459, -16, '-', ',', '.')), QString("9.934.567.890,12346"));
}

void ConversionTest::numberConversions()
{
   T_EQUALS(QString::fromStdString(std::toString(0, 10, '-', '.')), QString::number(0));
   T_EQUALS(QString::fromStdString(std::toString(0u, 10, '-', '.')), QString::number(0u));
   T_EQUALS(QString::fromStdString(std::toString(1, 10, '-', '.')), QString::number(1));
   T_EQUALS(QString::fromStdString(std::toString(-1, 10, '-', '.')), QString::number(-1));
   T_EQUALS(QString::fromStdString(std::toString(1u, 10, '-', '.')), QString::number(1u));
   T_EQUALS(QString::fromStdString(std::toString(1000000, 10, '-', '.')), QString("1.000.000"));

   T_EQUALS(QString::fromStdString(std::toString(INT_MAX, 10, '-', '\0')), QString::number(INT_MAX));
   T_EQUALS(QString::fromStdString(std::toString(INT_MIN, 10, '-', '\0')), QString::number(INT_MIN));
   T_EQUALS(QString::fromStdString(std::toString(UINT_MAX, 10, '-', '\0')), QString::number(UINT_MAX));
   T_EQUALS(QString::fromStdString(std::toString(INT64_MAX, 10, '-', '\0')), QString::number(INT64_MAX));
   T_EQUALS(QString::fromStdString(std::toString(INT64_MIN, 10, '-', '\0')), QString::number(INT64_MIN));
   T_EQUALS(QString::fromStdString(std::toString(UINT64_MAX, 10, '-', '\0')), QString::number(UINT64_MAX));

   QString result;

   T_IS_TRUE(integerToString(0, 10, result, QChar(u'-'), QChar()));
   T_EQUALS(result, QString::number(0));
   T_IS_TRUE(integerToString(0u, 10, result, QChar(u'-'), QChar()));
   T_EQUALS(result, QString::number(0u));
   T_IS_TRUE(integerToString(1, 10, result, QChar(u'-'), QChar()));
   T_EQUALS(result, QString::number(1));
   T_IS_TRUE(integerToString(-1, 10, result, QChar(u'-'), QChar()));
   T_EQUALS(result, QString::number(-1));
   T_IS_TRUE(integerToString(1u, 10, result, QChar(u'-'), QChar()));
   T_EQUALS(result, QString::number(1u));
   T_IS_TRUE(integerToString(1000000, 10, result, QChar(u'-'), QChar(u'.')));
   T_EQUALS(result, QString("1.000.000"));

   T_IS_TRUE(integerToString(INT_MAX, 10, result, QChar(u'-'), QChar()));
   T_EQUALS(result, QString::number(INT_MAX));
   T_IS_TRUE(integerToString(INT_MIN, 10, result, QChar(u'-'), QChar()));
   T_EQUALS(result, QString::number(INT_MIN));
   T_IS_TRUE(integerToString(UINT_MAX, 10, result, QChar(u'-'), QChar()));
   T_EQUALS(result, QString::number(UINT_MAX));
   T_IS_TRUE(integerToString(INT64_MAX, 10, result, QChar(u'-'), QChar()));
   T_EQUALS(result, QString::number(INT64_MAX));
   T_IS_TRUE(integerToString(INT64_MIN, 10, result, QChar(u'-'), QChar()));
   T_EQUALS(result, QString::number(INT64_MIN));
   T_IS_TRUE(integerToString(UINT64_MAX, 10, result, QChar(u'-'), QChar()));
   T_EQUALS(result, QString::number(UINT64_MAX));
}

void ConversionTest::nullableConversions()
{
   nullable<bool> nullableBool1;
   nullable<bool> nullableBool2;

   T_IS_TRUE(nullableBool1.isNull());
   T_IS_TRUE(nullableBool2.isNull());
   T_IS_TRUE(nullableBool1 == nullableBool2);

   nullableBool2 = false;

   T_IS_TRUE(nullableBool2.hasValue());
   T_IS_TRUE(nullableBool1 < nullableBool2);

   nullableBool1 = nullableBool2;

   T_IS_TRUE(nullableBool1.hasValue());
   T_IS_TRUE(nullableBool1 == nullableBool2);
}

void ConversionTest::localeEx()
{
   QString textL;
   QString textR;

   textL = defaultLocale().toString(12345.678);
   textR = QString("12345,678");
   T_EQUALS(textL, textR);
   textL = defaultLocale().toString(.3);
   textR = QString("0,3");
   T_EQUALS(textL, textR);
   textL = defaultLocale().toString(.03);
   textR = QString("0,03");
   T_EQUALS(textL, textR);
   textL = defaultLocale().toString(123E-300);
   textR = QString("1,23E-298");
   T_EQUALS(textL, textR);
   textL = defaultLocale().toString(123E-16);
   textR = QString("0,0000000000000123");
   T_EQUALS(textL, textR);
   textL = defaultLocale().toString(123E16);
   textR = QString("1,23E18");
   T_EQUALS(textL, textR);
   textL = defaultLocale().toString(123E13);
   textR = QString("1230000000000000");
   T_EQUALS(textL, textR);
   textL = defaultLocale().toString(-0.123456789);
   textR = QString("-0,123456789");
   T_EQUALS(textL, textR);

   QText text1 = QString("Bär");
   QText text2 = QString("Bar");
   QText text3 = QString("Baer");
   QText text4 = QString("bar");
   QText text5 = QString("Ball");
   QText text6 = QString("Bast");
   QText text7 = QString("Bb");
   QText text8 = QString("BÄR");

   T_ASSERT(text1 != text2);
   T_ASSERT(text1 != text3);
   T_ASSERT(text1 > text2);
   T_ASSERT(text5 < text1);
   T_ASSERT(text2 > text3);
   T_ASSERT(text1 > text3);
   T_ASSERT(text1 < text6);
   T_ASSERT(text1 < text7);
   T_ASSERT(text2 == text4);
   T_ASSERT(text1 == text8);
}

void ConversionTest::rational()
{
   // Testing GDC
   T_EQUALS(greatestCommonDivisor(20LL, 5LL), 5LL);
   T_EQUALS(greatestCommonDivisor(5LL, 20LL), 5LL);
   T_EQUALS(greatestCommonDivisor(0LL, 5LL), 5LL);
   T_EQUALS(greatestCommonDivisor(-20LL, 5LL), 5LL);
   T_EQUALS(greatestCommonDivisor(7LL, 3LL), 1LL);
   T_EQUALS(greatestCommonDivisor(3LL, 7LL), 1LL);
   T_EQUALS(greatestCommonDivisor(21LL, 14LL), 7LL);
   T_EQUALS(greatestCommonDivisor(14LL, 21LL), 7LL);
   T_EQUALS(greatestCommonDivisor(-21LL, 14LL), 7LL);
   T_EQUALS(greatestCommonDivisor(14LL, -21LL), 7LL);
   T_EQUALS(greatestCommonDivisor(-21LL, -14LL), 7LL);

   // Test string conversion

   QString textL;
   QString textR;

   textL = QRational(0.0).toDecimalString(-18, false);
   textR = QString("0");
   T_EQUALS(textL, textR);
   textL = QRational(1.).toDecimalString(-2, false);
   textR = QString("1");
   T_EQUALS(textL, textR);
   textL = QRational(1.).toDecimalString(2, false);
   textR = QString("1,00");
   T_EQUALS(textL, textR);
   textL = QRational(0.3).toDecimalString(-2, false);
   textR = QString("0,3");
   T_EQUALS(textL, textR);
   textL = QRational(0.03).toDecimalString(3, false);
   textR = QString("0,030");
   T_EQUALS(textL, textR);
   textL = QRational(12345.678).toDecimalString(2, false);
   textR = QString("12345,68");
   T_EQUALS(textL, textR);
   textL = QRational(0.445).toDecimalString(-2, false);
   textR = QString("0,45");
   T_EQUALS(textL, textR);
   textL = QRational(8.45).toDecimalString(-16, false);
   textR = QString("8,45");
   T_EQUALS(textL, textR);
   textL = QRational(123E-16).toDecimalString(-16, false);
   textR = QString("0,0000000000000123");
   T_EQUALS(textL, textR);
   textL = QRational(123E13).toDecimalString(-18, true);
   textR = QString("1.230.000.000.000.000");
   T_EQUALS(textL, textR);
   textL = QRational(-0.123456789).toDecimalString(-16, false);
   textR = QString("-0,123456789");
   T_EQUALS(textL, textR);

   textL = (QRational(1LL, 2LL) / QRational(2, 1)).toDecimalString(-18, false);
   textR = QString("0,25");
   T_EQUALS(textL, textR);
   textL = (QRational(-0x0000FFFFFFFFFFFFFFLL, 0xFA889021BCLL) * QRational(0x0000ABCDEF01234567LL, 0xFFFA82945B2LL)).toDecimalString(-16, false);
   textR = QString("-184096230,6219883298"); // -72057594037927935.0 / 1076032971196.0 * (48358647417488743.0 / 17590712354226.0);
   T_EQUALS(textL, textR);

   // Test comparison

   T_ASSERT(QRational(1, 0) == QRational(1, 0)); // Invalid QRationals
   T_ASSERT(QRational(1, 0) < QRational(-1, 1));
   T_ASSERT(QRational(-1, 1) > QRational(1, 0));
   T_ASSERT(QRational(-2, 1) < QRational(1, 1));
   T_ASSERT(QRational(2, 1) > QRational(1, 1));
   T_ASSERT(QRational(0, 1) < QRational(1, 1000));
   T_ASSERT(QRational(0, 1) > QRational(-1, 1000));
   T_ASSERT(QRational(1, 1000) > QRational(0, 10));
   T_ASSERT(QRational(-1, 1000) < QRational(0, 10));
   T_ASSERT(QRational(25000.3279) != QRational(25000.3278));
   T_ASSERT(QRational(25000.3278) == QRational(25000.3278));
   T_ASSERT(QRational(2, 3) == QRational(4, 6));
   T_ASSERT(QRational(4, 6) == QRational(2, 3));
   T_ASSERT(QRational(0x10000000000LL, 0x10000000000LL) == QRational(0x100000000000LL, 0x100000000000LL));
   T_ASSERT(QRational(0x100000000000LL, 0x100000000000LL) == QRational(0x10000000000LL, 0x10000000000LL));
   T_ASSERT(QRational(0x20000000000LL, 0x20000000000LL) == QRational(0x30000000000LL, 0x30000000000LL));

   // Test mathematical operations

   QRational testValue;
   QRational remainder;

   testValue = QRational(QDecimal("3.0000000"));
   T_ASSERT(testValue.numerator() == 3LL && testValue.denominator() == 1LL);
   T_EQUALS(testValue.toDecimal().toString(), QString::fromLatin1("3"));
   T_EQUALS(testValue.toDecimal().round(7).toString(), QString::fromLatin1("3,0000000"));

   testValue = QRational(QDecimal("3.3000000"));
   T_ASSERT(testValue.numerator() == 33LL && testValue.denominator() == 10LL);
   T_EQUALS(testValue.toDecimal().toString(), QString::fromLatin1("3,3"));
   T_EQUALS(testValue.toDecimal().round(7).toString(), QString::fromLatin1("3,3000000"));

   testValue /= 2;
   T_EQUALS(testValue.toDecimal().toString(), QString::fromLatin1("1,65"));

   testValue = QRational(3000000LL, 6100000LL);
   testValue *= 50834LL;
   remainder = testValue.rebase(10, 4);
   T_EQUALS_EX(remainder, QRational(-1900000, 6100000));
   T_EQUALS_EX(testValue, QRational(25000.3279));

   testValue = QRational(-3000000LL, 6100000LL);
   testValue *= 50834LL;
   remainder = testValue.rebase(10, 4);
   T_EQUALS_EX(remainder, QRational(1900000, 6100000));
   T_EQUALS_EX(testValue, QRational(-25000.3279));
}

void ConversionTest::decimal()
{
   // From doubles

   T_EQUALS(QDecimal(0.3).toString(), QString("0,3"));
   T_EQUALS(QDecimal(3e-20).toString(), QString("0,00000000000000000003"));
   T_EQUALS(QDecimal(3e50).toString(), QString("300000000000000000000000000000000000000000000000000"));
   T_EQUALS(QDecimal(1000000000000000.0).toString(), QString("1000000000000000"));
   T_EQUALS(QDecimal(9999999999999999.0).toString(), QString("10000000000000000"));
   T_EQUALS(QDecimal(5555555555555555.0).toString(), QString("5555555555555560"));
   T_EQUALS(QDecimal(4444444444444444.0).toString(), QString("4444444444444444"));

   // Limits

   T_EQUALS(QDecimal("-123456789123456789.123456789").toString(QLocale::C), QString("-123456789123456789.123456789"));

   // Conversion testing

   bool success = false;

   // Test empty string failure

   T_ASSERT(QDecimal(QString(), &success).isNull());
   if (!success)
      T_SIGNAL_ERROR(QString("QDecimal(QString()) failed"));

   // toInt

   T_EQUALS(QDecimal(12345).toInt(&success), 12345);
   if (!success)
      T_SIGNAL_ERROR(QString("QDecimal(12345).toInt() failed"));
   T_EQUALS(QDecimal(-12345).toInt(&success), -12345);
   if (!success)
      T_SIGNAL_ERROR(QString("QDecimal(-12345).toInt() failed"));

   T_EQUALS(QDecimal("12345.00000000000000000000").toInt(&success), 12345);
   if (!success)
      T_SIGNAL_ERROR(QString("QDecimal(12345.00000000000000000000).toInt() failed"));
   T_EQUALS(QDecimal("-12345.00000000000000000000").toInt(&success), -12345);
   if (!success)
      T_SIGNAL_ERROR(QString("QDecimal(-12345.00000000000000000000).toInt() failed"));

   T_EQUALS(QDecimal(12345.678).toInt(&success), (int)12345.678);
   if (success)
      T_SIGNAL_ERROR(QString("QDecimal(12345.678).toInt() didn't signal a failure"));
   T_EQUALS(QDecimal(-12345.678).toInt(&success), (int)-12345.678);
   if (success)
      T_SIGNAL_ERROR(QString("QDecimal(-12345.678).toInt() didn't signal a failure"));

   T_EQUALS(QDecimal("12345.01000000000000000000").toInt(&success), 12345);
   if (success)
      T_SIGNAL_ERROR(QString("QDecimal(12345.01000000000000000000).toInt() failed"));
   T_EQUALS(QDecimal("-12345.10000000000000000000").toInt(&success), -12345);
   if (success)
      T_SIGNAL_ERROR(QString("QDecimal(-12345.10000000000000000000).toInt() failed"));

   T_EQUALS(QDecimal("12345.00000000000000000001").toInt(&success), 12345);
   if (success)
      T_SIGNAL_ERROR(QString("QDecimal(12345.00000000000000000001).toInt() failed"));
   T_EQUALS(QDecimal("-12345.00000000000000000010").toInt(&success), -12345);
   if (success)
      T_SIGNAL_ERROR(QString("QDecimal(-12345.00000000000000000010).toInt() failed"));

   T_EQUALS(QDecimal(1234567890).toInt64(&success), 1234567890LL);
   if (!success)
      T_SIGNAL_ERROR(QString("QDecimal(1234567890).toInt64() failed"));
   T_EQUALS(QDecimal(-1234567890).toInt64(&success), -1234567890LL);
   if (!success)
      T_SIGNAL_ERROR(QString("QDecimal(-1234567890).toInt64() failed"));

   T_EQUALS(QDecimal(1234567890.0000000000).toInt64(&success), 1234567890LL);
   if (!success)
      T_SIGNAL_ERROR(QString("QDecimal(1234567890.0000000000).toInt64() failed"));
   T_EQUALS(QDecimal(-1234567890.0000000000).toInt64(&success), -1234567890LL);
   if (!success)
      T_SIGNAL_ERROR(QString("QDecimal(-1234567890.0000000000).toInt64() failed"));

   T_EQUALS(QDecimal(0xFFFFFFFFFFFFFFFFULL).toInt64(&success), -1);
   if (success)
      T_SIGNAL_ERROR(QString("QDecimal(0xFFFFFFFFFFFFFFFFULL).toInt64() did not fail"));
   T_EQUALS((QDecimal(0xFFFFFFFFFFFFFFFFULL) + 1).toUInt64(&success), 0ULL);
   if (!success)
      T_SIGNAL_ERROR(QString("(QQDecimal(0xFFFFFFFFFFFFFFFFULL) + 1).toUInt64() did not fail"));

   // toDouble

   T_EQUALS(QDecimal("12345.678").toDouble(&success), 12345.678);
   if (!success)
      T_SIGNAL_ERROR(QString("QDecimal(\"12345.678\").toDouble() failed"));
   T_EQUALS(QDecimal("-12345.678").toDouble(&success), -12345.678);
   if (!success)
      T_SIGNAL_ERROR(QString("QDecimal(\"-12345.678\").toDouble() failed"));
   T_EQUALS(QDecimal(0xFFFFFFFFFFFFFULL).toDouble(&success), 0xFFFFFFFFFFFFFULL);
   if (!success)
      T_SIGNAL_ERROR(QString("QDecimal(0xFFFFFFFFFFFFFULL).toDouble() failed"));
   QDecimal(0x10000000000000ULL).toDouble(&success); // The result is something like 4.5036e+15
   if (success)
      T_SIGNAL_ERROR(QString("QDecimal(0x10000000000000ULL).toDouble() did not fail"));
   T_EQUALS(QDecimal("450000000.0000000").toDouble(&success), 450000000.0);
   if (!success)
      T_SIGNAL_ERROR(QString("QDecimal(\"450000000.0000000\").toDouble() failed"));
   T_EQUALS(QDecimal("460000000.0000000").toDouble(&success), 4.6e+08);
   if (success)
      T_SIGNAL_ERROR(QString("QDecimal(\"460000000.0000000\").toDouble() did not fail"));

   // Rounding

   T_EQUALS_EX(QDecimal(99.95).round(1), QDecimal(100));

   T_EQUALS(QDecimal(100).length(), 3);

   T_EQUALS(QDecimal(99.1).round(2).toString(), QString("99,10"));
   T_EQUALS_EX(QDecimal(-1.5).round(0), QDecimal(-2));
   T_EQUALS(QDecimal(1234.56).toString(QLocale::C), QString("1234.56"));

   // Unary operators

   T_EQUALS_EX(++QDecimal(99), QDecimal(100));
   T_EQUALS_EX(++QDecimal(-1), QDecimal(0));
   T_EQUALS_EX(++QDecimal(-100), QDecimal(-99));
   T_EQUALS_EX(++QDecimal(-0.123), QDecimal(0.877));

   T_EQUALS_EX(--QDecimal(101), QDecimal(100));
   T_EQUALS_EX(--QDecimal(1), QDecimal(0));
   T_EQUALS_EX(--QDecimal(0), QDecimal(-1));
   T_EQUALS_EX(--QDecimal(-99), QDecimal(-100));
   T_EQUALS_EX(--QDecimal(0.123), QDecimal(-0.877));

   // From locale strings
   T_EQUALS_EX(QDecimal(QString("-1,45")), QDecimal(-1.45));

   // .NET conversions

   unsigned int clsDecimal1[] = { 1, 0, 0, 0};
   unsigned int clsDecimal2[] = { 0x107A4000, 0x00005AF3, 0x00000000, 0x00000000 };
   unsigned int clsDecimal3[] = { 0x10000000, 0x3E250261, 0x204FCE5E, 0x00000000 };
   unsigned int clsDecimal4[] = { 0x10000000, 0x3E250261, 0x204FCE5E, 0x000E0000 };
   unsigned int clsDecimal5[] = { 0x10000000, 0x3E250261, 0x204FCE5E, 0x001C0000 };
   unsigned int clsDecimal6[] = { 0x075BCD15, 0x00000000, 0x00000000, 0x00000000 };
   unsigned int clsDecimal7[] = { 0x075BCD15, 0x00000000, 0x00000000, 0x00090000 };
   unsigned int clsDecimal8[] = { 0x075BCD15, 0x00000000, 0x00000000, 0x00120000 };
   unsigned int clsDecimal9[] = { 0x075BCD15, 0x00000000, 0x00000000, 0x001B0000 };
   unsigned int clsDecimal10[] = { 0xFFFFFFFF, 0x00000000, 0x00000000, 0x00000000 };
   unsigned int clsDecimal11[] = { 0xFFFFFFFF, 0xFFFFFFFF, 0x00000000, 0x00000000 };
   unsigned int clsDecimal12[] = { 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0x00000000 };
   unsigned int clsDecimal13[] = { 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0x80000000 };
   unsigned int clsDecimal14[] = { 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0x801C0000 };

   T_EQUALS_EX(QDecimal().fromClsDecimal(clsDecimal1), QDecimal(1LL));
   T_EQUALS_EX(QDecimal().fromClsDecimal(clsDecimal2), QDecimal("100000000000000"));
   T_EQUALS_EX(QDecimal().fromClsDecimal(clsDecimal3), QDecimal("10000000000000000000000000000"));
   T_EQUALS_EX(QDecimal().fromClsDecimal(clsDecimal4), QDecimal("100000000000000.00000000000000"));
   T_EQUALS_EX(QDecimal().fromClsDecimal(clsDecimal5), QDecimal("1.0000000000000000000000000000"));
   T_EQUALS_EX(QDecimal().fromClsDecimal(clsDecimal6), QDecimal("123456789"));
   T_EQUALS_EX(QDecimal().fromClsDecimal(clsDecimal7), QDecimal("0.123456789"));
   T_EQUALS_EX(QDecimal().fromClsDecimal(clsDecimal8), QDecimal("0.000000000123456789"));
   T_EQUALS_EX(QDecimal().fromClsDecimal(clsDecimal9), QDecimal("0.000000000000000000123456789"));
   T_EQUALS_EX(QDecimal().fromClsDecimal(clsDecimal10), QDecimal("4294967295"));
   T_EQUALS_EX(QDecimal().fromClsDecimal(clsDecimal11), QDecimal("18446744073709551615"));
   T_EQUALS_EX(QDecimal().fromClsDecimal(clsDecimal12), QDecimal("79228162514264337593543950335"));
   T_EQUALS_EX(QDecimal().fromClsDecimal(clsDecimal13), QDecimal("-79228162514264337593543950335"));
   T_EQUALS_EX(QDecimal().fromClsDecimal(clsDecimal14), QDecimal("-7.9228162514264337593543950335"));

   unsigned int clsDecimal[4];

   QDecimal(1).toClsDecimal(clsDecimal);
   T_ASSERT(memcmp(clsDecimal, clsDecimal1, sizeof(clsDecimal)) == 0);
   QDecimal("100000000000000").toClsDecimal(clsDecimal);
   T_ASSERT(memcmp(clsDecimal, clsDecimal2, sizeof(clsDecimal)) == 0);
   QDecimal("10000000000000000000000000000").toClsDecimal(clsDecimal);
   T_ASSERT(memcmp(clsDecimal, clsDecimal3, sizeof(clsDecimal)) == 0);
   QDecimal("100000000000000.00000000000000").toClsDecimal(clsDecimal);
   T_ASSERT(memcmp(clsDecimal, clsDecimal4, sizeof(clsDecimal)) == 0);
   QDecimal("1.0000000000000000000000000000").toClsDecimal(clsDecimal);
   T_ASSERT(memcmp(clsDecimal, clsDecimal5, sizeof(clsDecimal)) == 0);
   QDecimal("123456789").toClsDecimal(clsDecimal);
   T_ASSERT(memcmp(clsDecimal, clsDecimal6, sizeof(clsDecimal)) == 0);
   QDecimal("0.123456789").toClsDecimal(clsDecimal);
   T_ASSERT(memcmp(clsDecimal, clsDecimal7, sizeof(clsDecimal)) == 0);
   QDecimal("0.000000000123456789").toClsDecimal(clsDecimal);
   T_ASSERT(memcmp(clsDecimal, clsDecimal8, sizeof(clsDecimal)) == 0);
   QDecimal("0.000000000000000000123456789").toClsDecimal(clsDecimal);
   T_ASSERT(memcmp(clsDecimal, clsDecimal9, sizeof(clsDecimal)) == 0);
   QDecimal("4294967295").toClsDecimal(clsDecimal);
   T_ASSERT(memcmp(clsDecimal, clsDecimal10, sizeof(clsDecimal)) == 0);
   QDecimal("18446744073709551615").toClsDecimal(clsDecimal);
   T_ASSERT(memcmp(clsDecimal, clsDecimal11, sizeof(clsDecimal)) == 0);
   QDecimal("79228162514264337593543950335").toClsDecimal(clsDecimal);
   T_ASSERT(memcmp(clsDecimal, clsDecimal12, sizeof(clsDecimal)) == 0);
   QDecimal("-79228162514264337593543950335").toClsDecimal(clsDecimal);
   T_ASSERT(memcmp(clsDecimal, clsDecimal13, sizeof(clsDecimal)) == 0);
   QDecimal("-7.9228162514264337593543950335").toClsDecimal(clsDecimal);
   T_ASSERT(memcmp(clsDecimal, clsDecimal14, sizeof(clsDecimal)) == 0);

   // ODBC conversions

   quint32 odbcNumber[4] = { 2808348672u, 232830643u, 0u, 0u }; // 100 with 16 fractional digits

   T_EQUALS_EX(QDecimal().fromOdbcNumber(odbcNumber, 16, 16, false), QDecimal(100));
   T_IS_TRUE(QDecimal("100.0000000000000000").toOdbcNumber(odbcNumber, 16));
   T_EQUALS(odbcNumber[0], 2808348672u);
   T_EQUALS(odbcNumber[1], 232830643u);
   T_EQUALS(odbcNumber[2], 0u);
   T_EQUALS(odbcNumber[3], 0u);

   odbcNumber[0] = 3164893642u;
   odbcNumber[1] = 232910017u;

   T_EQUALS_EX(QDecimal().fromOdbcNumber(odbcNumber, 16, 16, false), QDecimal("100.0340909090697674"));

   QDecimal("123456789012.1234567890123456").toOdbcNumber(odbcNumber, 16);

   T_EQUALS_EX(QDecimal().fromOdbcNumber(odbcNumber, 16, 16, false), QDecimal("123456789012.1234567890123456"));

   QDecimal("1234567890123456.1234567890123456").toOdbcNumber(odbcNumber, 16);

   T_EQUALS_EX(QDecimal().fromOdbcNumber(odbcNumber, 16, 16, false), QDecimal("1234567890123456.1234567890123456"));

   QDecimal("123456789012345.1234567890123456").toOdbcNumber(odbcNumber, 16);

   quint8 db2Number[13];

   memcpy(db2Number, odbcNumber, 13);
   T_EQUALS_EX(QDecimal().fromOdbcNumber(db2Number, 13, 16, false), QDecimal("123456789012345.1234567890123456"));

   // QRational conversions

   T_EQUALS_EX(QRational(123456, 1000).toDecimal(), QDecimal("123.456"));
   T_EQUALS_EX(QRational(123456, 876).toDecimal(), QDecimal("140.9315068493150685"));
   T_EQUALS_EX(QRational(123456, 9).toDecimal(), QDecimal("13717.33333333333333"));

   // Decimal operations basic tests

   T_ASSERT(QDecimal() == QDecimal());
   T_ASSERT(QDecimal(0) != QDecimal());
   T_ASSERT(QDecimal() < QDecimal(0));
   T_ASSERT(QDecimal(0) != QDecimal("0.000123"));

   T_ASSERT(QDecimal() < QDecimal(1));
   T_ASSERT(QDecimal() < QDecimal(-1));
   T_ASSERT(QDecimal(0) < QDecimal(1));
   T_ASSERT(QDecimal(0) > QDecimal(-1));
   T_ASSERT(QDecimal(0) < QDecimal("0.000123"));

   T_EQUALS_EX(QDecimal(12) + QDecimal(18), QDecimal(30));
   T_EQUALS_EX(QDecimal(12) - QDecimal(7), QDecimal(5));
   T_EQUALS_EX(QDecimal(-12) + QDecimal(18), QDecimal(6));
   T_EQUALS_EX(QDecimal(12) + QDecimal(-18), QDecimal(-6));
   T_EQUALS_EX(QDecimal(-12) - QDecimal(18), QDecimal(-30));
   T_EQUALS_EX(QDecimal(-12) - QDecimal(-18), QDecimal(6));
   T_EQUALS_EX(QDecimal() - QDecimal(200), QDecimal(-200));
   T_EQUALS_EX(QDecimal() - QDecimal(-200), QDecimal(200));

   T_EQUALS_EX(QDecimal(1.5) + QDecimal(1.55), QDecimal(3.05));
   T_EQUALS_EX(QDecimal(1.5) + QDecimal(-1.55), QDecimal(-0.05));

   T_EQUALS_EX(QDecimal(0) - QDecimal(0), QDecimal(0));
   T_EQUALS_EX(QDecimal() - QDecimal(0), QDecimal(0));

   T_EQUALS_EX(QDecimal(-1) * 0, QDecimal(0));
   T_EQUALS_EX(QDecimal(0) * -5, QDecimal(0));
   T_EQUALS_EX(QDecimal(12) * 6, QDecimal(72));
   T_EQUALS_EX(QDecimal(12) * -6, QDecimal(-72));
   T_EQUALS_EX(QDecimal(-12) * -6, QDecimal(72));
   T_EQUALS_EX(QDecimal(-5) / 6, QDecimal(0));
   T_EQUALS_EX(QDecimal(12) / 6, QDecimal(2));
   T_EQUALS(QDecimal(12) % -5, 12 % -5);

   T_EQUALS_EX(QDecimal().truncate(2), QDecimal());
   T_EQUALS_EX(QDecimal().truncate(-1), QDecimal());
   T_EQUALS_EX(QDecimal(100).truncate(2), QDecimal(100));
   T_EQUALS_EX(QDecimal(100).truncate(0), QDecimal(100));
   T_EQUALS_EX(QDecimal("100.22").truncate(0), QDecimal(100));
   T_EQUALS_EX(QDecimal("100.22").truncate(4), QDecimal("100.22"));
   T_EQUALS_EX(QDecimal("100.127").truncate(2), QDecimal("100.12"));
   T_EQUALS_EX(QDecimal("100.127").truncate(-1), QDecimal("100.127"));
   T_EQUALS_EX(QDecimal("100.120").truncate(-1), QDecimal("100.12"));
   T_EQUALS_EX(QDecimal("100.000").truncate(-1), QDecimal("100"));
   T_EQUALS_EX(QDecimal("-100.22").truncate(0), QDecimal((int)-100.22));

   // devideBy()

   QDecimal number;
   QDecimal remainder;

   number = 7;
   T_EQUALS(number.devideBy(-3), 7 % -3);
   T_EQUALS_EX(number, QDecimal(7 / -3));

   number = -5;
   T_EQUALS(number.devideBy(3), -5 % 3);
   T_EQUALS_EX(number, QDecimal(-5 / 3));

   number = -5;
   T_EQUALS(number.devideBy(-3), -5 % -3);
   T_EQUALS_EX(number, QDecimal(-5 / -3));

   number = 5;
   T_ASSERT(number.devideBy(QDecimal()).isNull());
   T_ASSERT(number.isNull());

   number = 5;
   T_ASSERT(number.devideBy(QDecimal(0)).isNull());
   T_ASSERT(number.isNull());

   number = QDecimal();
   T_ASSERT(number.devideBy(QDecimal(-123)).isNull());
   T_ASSERT(number.isNull());

   number = 50834LL * 3000000LL;
   T_EQUALS(number.devideBy(6100000), 2000000);
   T_EQUALS_EX(number, QDecimal(25000));

   number = QDecimal(508340000LL, 4) * QDecimal(300000000LL, 2);
   T_EQUALS(number.fractionalDigits(), 4);
   remainder = number.devideBy(QDecimal(610000000, 2));
   T_EQUALS_EX(remainder, QDecimal(420));
   T_EQUALS(remainder.fractionalDigits(), 6);
   T_EQUALS_EX(number, QDecimal(25000.3278));
   T_EQUALS(number.fractionalDigits(), 4);

   number = QDecimal(508340000LL, 4) * QDecimal(300000000LL, 2);
   T_EQUALS(number.fractionalDigits(), 4);
   remainder = number.devideBy(QDecimal(61000000000LL, 2));
   T_EQUALS_EX(remainder, QDecimal(48000));
   T_EQUALS(remainder.fractionalDigits(), 6);
   T_EQUALS_EX(number, QDecimal(250.0032));
   T_EQUALS(number.fractionalDigits(), 4);

   number = QDecimal(508340000LL, 4) * QDecimal(300000000LL, 2);
   T_EQUALS(number.fractionalDigits(), 4);
   remainder = number.devideBy(QDecimal(61000000010LL, 2));
   T_EQUALS_EX(remainder, QDecimal(47974999680LL, 6));
   T_EQUALS(remainder.fractionalDigits(), 6);
   T_EQUALS_EX(number, QDecimal(250.0032));
   T_EQUALS(number.fractionalDigits(), 4);
   T_EQUALS_EX(number * QDecimal(61000000010LL, 2) + remainder, QDecimal(508340000LL, 4) * QDecimal(300000000LL, 2));

   // Multiplication

   T_EQUALS_EX(QDecimal("123") * QDecimal("456"), QDecimal("56088"));
   T_EQUALS_EX(QDecimal("123.45") * QDecimal("-456"), QDecimal("-56293.2"));
   T_EQUALS_EX(QDecimal("123.45") * QDecimal("2.5"), QDecimal("308.625"));
   T_EQUALS_EX(QDecimal("123.45") * QDecimal("100.00"), QDecimal("12345.00"));
   T_EQUALS_EX(QDecimal("123") * QDecimal("100.00"), QDecimal("12300"));
   T_ASSERT((QDecimal() * QDecimal("456")).isNull());
   T_ASSERT(!(QDecimal("0") * QDecimal("456")).isNull());
   T_EQUALS_EX(QDecimal("0") * QDecimal("-456"), QDecimal("0"));
   T_EQUALS_EX(QDecimal("-1") * QDecimal("-1"), QDecimal("1"));
   T_EQUALS_EX(QDecimal("1000000000000") * QDecimal("1000000000000"), QDecimal("1000000000000000000000000"));

   // Division

   T_EQUALS((QDecimal(8) / QDecimal(4)).toString(QLocale::C), "2");
   T_EQUALS((QDecimal(0) / QDecimal(-123.45)).toString(QLocale::C), "0");
   T_EQUALS((QDecimal(8.8) / QDecimal(11)).toString(QLocale::C), "0.8");
   T_EQUALS((QDecimal(-10) / QDecimal(3)).toString(QLocale::C), "-3");
   T_EQUALS((QDecimal(123) / QDecimal(-1.5)).toString(QLocale::C), "-82");
   T_EQUALS((QDecimal(654.55) / QDecimal(5.888)).toString(QLocale::C), "111.16");
   T_EQUALS((QDecimal("123456789012") / QDecimal("123456789012")).toString(QLocale::C), "1");
   T_EQUALS((QDecimal("100000000000") / QDecimal("1000")).toString(QLocale::C), "100000000");
   T_EQUALS((QDecimal(10) / QDecimal(0.03)).toString(QLocale::C), "333");
   T_EQUALS((QDecimal(10) / QDecimal(0.01)).toString(QLocale::C), "1000");
   T_EQUALS((QDecimal(10) / QDecimal(100)).toString(QLocale::C), "0");
   T_EQUALS((QDecimal(10) / QDecimal("100.00")).toString(QLocale::C), "0");
   T_EQUALS((QDecimal("10000000000") / QDecimal("1000000000")).toString(QLocale::C), "10");
   T_EQUALS((QDecimal("10000000000") / QDecimal("2000000000")).toString(QLocale::C), "5");
   T_EQUALS((QDecimal("10000000000") / QDecimal("-2000000000")).toString(QLocale::C), "-5");
   T_EQUALS((QDecimal("10000000000") / QDecimal("2000000000.000")).toString(QLocale::C), "5");
   T_EQUALS((QDecimal("10000000000") / QDecimal("1234000000")).toString(QLocale::C), "8");
   T_EQUALS((QDecimal("10000000000") / QDecimal("2500000000")).toString(QLocale::C), "4");
   T_EQUALS((QDecimal("10000000000") / QDecimal("-2500000000")).toString(QLocale::C), "-4");
   T_EQUALS((QDecimal("10000000000") / QDecimal("2500000000.000")).toString(QLocale::C), "4");
   T_EQUALS((QDecimal("1000000000000") / QDecimal("123400000000")).toString(QLocale::C), "8");
   T_EQUALS((QDecimal("1000000000000") / QDecimal("250000000000")).toString(QLocale::C), "4");
   T_EQUALS((QDecimal("1000000000000") / QDecimal("-250000000000")).toString(QLocale::C), "-4");
   T_EQUALS((QDecimal("1000000000000") / QDecimal("250000000000.000")).toString(QLocale::C), "4");
   T_EQUALS((QDecimal(100.1) / QDecimal(100)).toString(QLocale::C), "1.0");
   T_EQUALS((QDecimal(100.1) / QDecimal(1000)).toString(QLocale::C), "0.1");
   T_EQUALS((QDecimal(100.1) / QDecimal(10000)).toString(QLocale::C), "0.0");
   T_EQUALS((QDecimal(10000000000.1) / QDecimal(10000000000.1)).toString(QLocale::C), "1.0");
   T_EQUALS((QDecimal(10000000000.1) / QDecimal(100000000000.1)).toString(QLocale::C), "0.1");
   T_EQUALS((QDecimal(10000000000.1) / QDecimal(1000000000000.1)).toString(QLocale::C), "0.0");

   // Modulo

   T_EQUALS_EX(QDecimal(12) % QDecimal(5), QDecimal(12 % 5));
   T_EQUALS_EX(QDecimal(12) % QDecimal(-5), QDecimal(12 % -5));
   T_EQUALS_EX(QDecimal(12) %= QDecimal(5), QDecimal(12 % 5));
   T_EQUALS_EX(QDecimal("12.0") % QDecimal("5"), QDecimal(0));

   // Large numbers to string

   number.fromString("002300101023502601121710");
   T_EQUALS(number % 97, 1);

   number.fromString("700100800530886804131487");
   T_EQUALS(number % 97, 1);

   number.fromString("210501700012345678131468");
   T_EQUALS(number % 97, 1);

   // Precision tests

   number = 12.5;
   number.setMinFractionalDigits(3);
   number = number * QDecimal(1000000) / QDecimal(125.5);
   T_EQUALS_EX(number.rounded(2), QDecimal(12.5 * 1000000 / 125.5).rounded(2));

   number = QDecimal(3697562) * QDecimal(125632);
   T_EQUALS_EX(number / 100, QDecimal(4645321091LL));
   number.shiftDecimal(-2);
   T_EQUALS_EX(number, QDecimal(4645321091.84));
}

void ConversionTest::dateTime()
{
   T_IS_TRUE(defaultLocale().toDate(QString("01.01.2011")).isValid());

   T_IS_TRUE(defaultLocale().toDateTime(QString("01.01.2011 18:20:00")).isValid());

   T_IS_TRUE(defaultLocale().toTime(QString("06:02:00")).isValid());
   T_IS_TRUE(defaultLocale().toTime(QString("18:20:00")).isValid());

   QLocaleEx shortLocale(QLocale::German, QLocale::Germany, DateTimePart::Minute, QLocale::ShortFormat);

   T_ASSERT(shortLocale.toDateTime(QString("01.01.11 18:20")).isValid());

   T_ASSERT(shortLocale.toTime(QString("06:02")).isValid());
   T_ASSERT(shortLocale.toTime(QString("18:20")).isValid());

   T_IS_TRUE(Variant("01.01.2011 18:20:00").toDateTime().isValid());
}

void ConversionTest::dateTimeEx()
{
   QDateTimeEx dateTime;

   T_EQUALS(*(unsigned long long*)&dateTime, 0ULL);

   dateTime = QDateTimeEx(2017, 12, 24, 12, 30, 0, 0, 60, false);
   T_EQUALS(*(unsigned long long*)&dateTime, 0x6128cc35f040103dULL);

   T_EQUALS(QDateTimeEx(QDateTime(QDate(2017, 12, 6), QTime(12, 30, 0))).toString(QLocaleEx(QLocale::German)), "Mittwoch, 6. Dezember 2017 12:30:00 Z+01:00");
   T_EQUALS(QDateTimeEx(2017, 12, 24, 12, 30, 0, 0).toString(), "24.12.2017 12:30:00");
   T_EQUALS(QDateTimeEx(2017, 12, 24, 12, 30).toString(), "24.12.2017 12:30");
   T_EQUALS(QDateTimeEx(9999, 12, 31, 0, 0).toString(), "31.12.9999 00:00");
   T_EQUALS(QDateTimeEx(9999, 12, 31, 0, 0).toISOString(), "9999-12-31T00:00");
   T_EQUALS(QDateTimeEx(-2222, 12, 31, 0, 0).toString(), "31.12.-2222 00:00");
   T_EQUALS(QDateTimeEx(-2222, 12, 31, 0, 0).toISOString(), "-2222-12-31T00:00");
   T_EQUALS(QDateTimeEx(11300, 12, 31, 0, 0).toString(), "31.12.11300 00:00");
   T_EQUALS(QDateTimeEx(11300, 12, 31, 0, 0).toISOString(), "11300-12-31T00:00");

   T_EQUALS_EX(QDateTimeEx(QDateTime(QDate(2017, 6, 6), QTime(12, 30, 0))).toQDateTime(), QDateTime(QDate(2017, 6, 6), QTime(12, 30, 0)));

   T_IS_TRUE(QDateTimeEx(2017, 2, 23) < QDateTimeEx(2017, 2, 24));
   T_IS_TRUE(QDateTimeEx(2017, 2, 23) <= QDateTimeEx(2017, 2, 24));
   T_IS_TRUE(QDateTimeEx(2017, 2, 24) >= QDateTimeEx(2017, 2, 24));

   T_EQUALS_EX(QDateTimeEx(2017, 2, 23).addDays(10), QDateTimeEx(QDate(2017, 2, 23).addDays(10)));
   T_EQUALS_EX(QDateTimeEx(2016, 2, 23).addDays(10), QDateTimeEx(QDate(2016, 2, 23).addDays(10)));
   T_EQUALS_EX(QDateTimeEx(2000, 2, 23).addDays(10), QDateTimeEx(QDate(2000, 2, 23).addDays(10)));
   T_EQUALS_EX(QDateTimeEx(2017, 3, 5).addDays(-10), QDateTimeEx(QDate(2017, 3, 5).addDays(-10)));
   T_EQUALS_EX(QDateTimeEx(2016, 3, 5).addDays(-10), QDateTimeEx(QDate(2016, 3, 5).addDays(-10)));
   T_EQUALS_EX(QDateTimeEx(2000, 3, 5).addDays(-10), QDateTimeEx(QDate(2000, 3, 5).addDays(-10)));
   T_EQUALS_EX(QDateTimeEx(2017, 12, 24).addDays(10), QDateTimeEx(QDate(2017, 12, 24).addDays(10)));
   T_EQUALS_EX(QDateTimeEx(2018, 1, 3).addDays(-10), QDateTimeEx(QDate(2018, 1, 3).addDays(-10)));
   T_EQUALS_EX(QDateTimeEx(2017, 1, 3).addDays(365), QDateTimeEx(QDate(2017, 1, 3).addDays(365)));
   T_EQUALS_EX(QDateTimeEx(2017, 1, 3).addDays(-365), QDateTimeEx(QDate(2017, 1, 3).addDays(-365)));

   T_EQUALS_EX(QDateTimeEx(2017, 1, 3).addMonths(2), QDateTimeEx(2017, 3, 3));
   T_EQUALS_EX(QDateTimeEx(2017, 1, 3).addMonths(12), QDateTimeEx(2018, 1, 3));
   T_EQUALS_EX(QDateTimeEx(2017, 1, 3).addMonths(-2), QDateTimeEx(2016, 11, 3));
   T_EQUALS_EX(QDateTimeEx(2017, 1, 3).addMonths(-12), QDateTimeEx(2016, 1, 3));
   T_EQUALS_EX(QDateTimeEx(2017, 1, 31).addMonths(1), QDateTimeEx(2017, 2, 28));
   T_EQUALS_EX(QDateTimeEx(2016, 1, 31).addMonths(1), QDateTimeEx(2016, 2, 29));

   T_EQUALS_EX(QDateTimeEx(2017, 2, 23, 12, 0, 0, 0).toUTC(), QDateTimeEx(QDateTime(QDate(2017, 2, 23), QTime(12, 0)).toUTC()));
   T_EQUALS_EX(QDateTimeEx(2017, 2, 23, 12, 30, 0, 0).toUTC(), QDateTimeEx(QDateTime(QDate(2017, 2, 23), QTime(12, 30)).toUTC()));
   T_EQUALS_EX(QDateTimeEx(2017, 6, 24, 12, 0, 0, 0).toUTC(), QDateTimeEx(QDateTime(QDate(2017, 6, 24), QTime(12, 0)).toUTC()));
   T_EQUALS_EX(QDateTimeEx(2017, 6, 24, 12, 30, 0, 0).toUTC(), QDateTimeEx(QDateTime(QDate(2017, 6, 24), QTime(12, 30)).toUTC()));

   T_EQUALS_EX(QDateTimeEx(2017, 2, 23, 12, 0, 0, 0).toLocalTime(), QDateTimeEx(QDateTime(QDate(2017, 2, 23), QTime(12, 0))));
   T_EQUALS_EX(QDateTimeEx(2017, 6, 24, 12, 0, 0, 0).toLocalTime(), QDateTimeEx(QDateTime(QDate(2017, 6, 24), QTime(12, 0))));
   T_EQUALS_EX(QDateTimeEx(2017, 12, 24, 12, 30, 0, 0, 60, false).toLocalTime(), QDateTimeEx(QDateTime(QDate(2017, 12, 24), QTime(12, 30))));
   T_EQUALS_EX(QDateTimeEx(2017, 6, 24, 12, 30, 0, 0, 120, true).toLocalTime(), QDateTimeEx(QDateTime(QDate(2017, 6, 24), QTime(12, 30))));

   T_EQUALS(QDateTimeEx(2017, 12, 24, 12, 30, 25, 700, -30, false).toISOString(), "2017-12-24T12:30:25.700-00:30");
   T_EQUALS(QDateTimeEx(2017, 12, 24, 12, 30, 25, 700, 30, false).toISOString(), "2017-12-24T12:30:25.700+00:30");
   T_EQUALS(QDateTimeEx(2017, 12, 24, 12, 30, 25, 700, -90, false).toISOString(), "2017-12-24T12:30:25.700-01:30");
   T_EQUALS(QDateTimeEx(2017, 12, 24, 12, 30, 25, 700, -720, false).toISOString(), "2017-12-24T12:30:25.700-12:00");
   T_EQUALS(QDateTimeEx(2017, 12, 24, 12, 30, 25, 700, 840, false).toISOString(), "2017-12-24T12:30:25.700+14:00");

   T_EQUALS(QDateTimeEx(2017, 6, 24, 12, 30, 0, 0, 120, true).toISOString(), QString("2017-06-24T12:30:00.000+02:00S"));
   T_EQUALS_EX(QDateTimeEx::fromISOString("2017-06-24T12:30:00,000+02:00S"), QDateTimeEx(2017, 6, 24, 12, 30, 0, 0, 120, true));
   T_EQUALS(QDateTimeEx(-2017, 6, 24, 12, 30, 0, 0, 120, true).toISOString(), QString("-2017-06-24T12:30:00.000+02:00S"));
   T_EQUALS_EX(QDateTimeEx::fromISOString("-2017-06-24T12:30:00,000+02:00S"), QDateTimeEx(-2017, 6, 24, 12, 30, 0, 0, 120, true));

   T_EQUALS_EX(QDateTimeEx(2017, 6, 6, 12, 30, 30, 500, 120, true).toDate(), QDateTimeEx(2017, 6, 6));
   T_EQUALS_EX(QDateTimeEx(2017, 6, 6, 12, 30, 30, 500, 120, true).toTime(), QDateTimeEx(0, 0, 0, 12, 30, 30, 500));

   Variant variant(dateTime);

   auto serialized = variant.serialized();
   T_IS_TRUE(variant.deserialize(serialized));
   T_EQUALS_EX(variant.to<QDateTimeEx>(), dateTime);

   Variant var2(QDateTimeEx(9999, 12, 31));
   serialized = var2.serialized();
   T_IS_TRUE(var2.deserialize(serialized));
   T_EQUALS_EX(var2.to<QDateTimeEx>(), QDateTimeEx(9999, 12, 31));

   QVariant qVariant(variant);

   T_EQUALS_EX(qVariant.toDate(), QDate(2017, 12, 24));
   T_EQUALS_EX(qVariant.toTime(), QTime(12, 30, 0));
   T_EQUALS_EX(qVariant.toDateTime(), QDateTime(QDate(2017, 12, 24), QTime(12, 30, 0)));

   T_EQUALS_EX(Variant("12:30:00").toTimeEx(nullptr, QLocale::C), QTimeEx(12, 30, 0));
   T_EQUALS_EX(Variant("2018-12-01").toDateEx(nullptr, QLocale::C), QDateEx(2018, 12, 1));
   T_EQUALS_EX(Variant("2018-12-01T12:30:00").toDateTimeEx(nullptr, QLocale::C), QDateTimeEx(2018, 12, 1, 12, 30, 0));
}

void ConversionTest::lists()
{
   T_EQUALS_EX(toStringList(QList<int>() << 42 << 3), QStringList() << "42" << "3");
   T_EQUALS_EX(toStringList(QList<int>() << 42 << 3, [](int i) { return QString::number(i, 16); }), QStringList() << "2a" << "3");
   T_EQUALS_EX(toList<int>(QList<int>() << 42 << 3, [](int i) { return i + 2; }), QList<int>() << 44 << 5);

   const QList<int> list{1, 4, 5, 3, 2, 4};

   T_EQUALS_EX(std::sorted(list), QList<int>() << 1 << 2 << 3 << 4 << 4 << 5);
   T_EQUALS_EX(std::sorted_by(list, [](int l, int r) { return l > r; }), QList<int>() << 5 << 4 << 4 << 3 << 2 << 1);
   T_EQUALS_EX(std::stable_sorted(list), QList<int>() << 1 << 2 << 3 << 4 << 4 << 5);
   T_EQUALS_EX(std::stable_sorted_by(list, [](int l, int r) { return l > r; }), QList<int>() << 5 << 4 << 4 << 3 << 2 << 1);
   T_EQUALS_EX(std::copy_if(list, [](int i) { return i == 4; }), QList<int>() << 4 << 4);

   const auto sorted = std::sorted(list);
   T_EQUALS(std::lower_bound_index(sorted, 4, [](int i) { return i; }), 3);
   T_EQUALS(std::upper_bound_index(sorted, 4, [](int i) { return i; }), 5);
}

void VariantTests::init()
{
   QLocaleEx qtLocale(QLocale::German);
   qtLocale.setDateFormat("dd.MM.yyyy");
   qtLocale.setTimeFormat("hh:mm:ss");
   qtLocale.setDateTimeFormat("dd.MM.yyyy hh:mm:ss");

   _prevLocale = defaultLocale();

   setDefaultLocale(qtLocale);
}

void VariantTests::deinit()
{
   setDefaultLocale(_prevLocale);
}

void VariantTests::conversionTests()
{
   bool ok;

   Variant vNull;
   int* pInt = vNull.to<int*>(&ok);
   T_IS_TRUE(ok);
   T_IS_TRUE(pInt == nullptr);

   T_EQUALS_EX(Variant(42.0).toDouble(), 42.0);

   T_IS_TRUE(Variant(QVariant(true)).toQVariant().toBool());
   T_IS_TRUE(Variant(QVariant(1)).toQVariant().toInt() == 1);
   T_IS_TRUE(Variant(QVariant(1u)).toQVariant().toUInt() == 1u);
   T_IS_TRUE(Variant(QVariant(1LL)).toQVariant().toLongLong() == 1LL);
   T_IS_TRUE(Variant(QVariant(1ULL)).toQVariant().toULongLong() == 1ULL);
   T_IS_TRUE(Variant(QVariant(1.0)).toQVariant().toDouble() == 1.0);
   T_IS_TRUE(Variant(QVariant(QString("Hallo"))).toQVariant().toString() == "Hallo");
   T_IS_TRUE(Variant(QVariant(QText("Hallo"))).toQVariant().toString() == "Hallo");

   T_EQUALS_EX(Variant(QVariant(Variant(QDecimal(1.2)))).toDecimal(), QDecimal(1.2));

   T_EQUALS(Variant(true).toString(&ok), QString("Ja"));
   T_IS_TRUE(ok);

   T_EQUALS(Variant(false).toString(&ok), QString("Nein"));
   T_IS_TRUE(ok);

   T_IS_TRUE(Variant("Ja").toBool(&ok));
   T_IS_TRUE(ok);

   T_IS_TRUE(Variant("J").toBool(&ok));
   T_IS_TRUE(ok);

   T_IS_TRUE(Variant("t").toBool(&ok));
   T_IS_TRUE(ok);

   T_IS_TRUE(Variant("yes").toBool(&ok));
   T_IS_TRUE(ok);

   T_IS_TRUE(Variant("Y").toBool(&ok));
   T_IS_TRUE(ok);

   T_IS_TRUE(Variant("1").toBool(&ok));
   T_IS_TRUE(ok);

   T_IS_TRUE(!Variant("Nein").toBool(&ok));
   T_IS_TRUE(ok);

   T_IS_TRUE(!Variant("n").toBool(&ok));
   T_IS_TRUE(ok);

   T_IS_TRUE(!Variant("F").toBool(&ok));
   T_IS_TRUE(ok);

   T_IS_TRUE(!Variant("NO").toBool(&ok));
   T_IS_TRUE(ok);

   T_IS_TRUE(!Variant("0").toBool(&ok));
   T_IS_TRUE(ok);

   Variant vText(QText(QString("123456,789")));

   T_EQUALS_EX(vText.toDecimal(&ok), QDecimal(123456.789));
   T_IS_TRUE(ok);

   T_EQUALS_EX(vText.to<QRational>(&ok), QRational(123456789, 1000));
   T_IS_TRUE(ok);

   T_EQUALS(Variant(QDateTime(QDate(2012, 12, 24), QTime(18, 00))).toString(), QString::fromLatin1("24.12.2012 18:00:00"));
#if QT_VERSION < QT_VERSION_CHECK(5,14,0)
   T_EQUALS_EX(Variant(QDateEx(2019, 9, 1)).toDateTime(), QDateTime(QDate(2019, 9, 1)));
#else
   T_EQUALS_EX(Variant(QDateEx(2019, 9, 1)).toDateTime(), QDateTime(QDate(2019, 9, 1).startOfDay()));
#endif
}

void VariantTests::serializationTests()
{
   T_EQUALS(plain_type_name(typeid(int)), "int32");
   T_EQUALS(plain_type_name(typeid(long long)), "int64");
   T_EQUALS(plain_type_name(typeid(unsigned int)), "unsigned int32");
   T_EQUALS(plain_type_name(typeid(unsigned long long)), "unsigned int64");
   T_EQUALS(plain_type_name(typeid(short)), "int16");
   T_EQUALS(plain_type_name(typeid(unsigned short)), "unsigned int16");
   T_EQUALS(plain_type_name(typeid(char)), "int8");
   T_EQUALS(plain_type_name(typeid(unsigned char)), "unsigned int8");
   T_EQUALS(plain_type_name(typeid(float)), "real32");
   T_EQUALS(plain_type_name(typeid(double)), "real64");
   T_EQUALS(plain_type_name(typeid(Qt::GlobalColor)), "Qt::GlobalColor");
   T_EQUALS(plain_type_name(typeid(QString)), "QString");
   T_EQUALS(plain_type_name(typeid(CppOpDef)), "CppOpDef");

   QList<QByteArray> serList;

   //
   // Serialize
   //

   serList.append(Variant(true).serialized());
   serList.append(Variant(-25).serialized());
   serList.append(Variant(-12345.987).serialized());
   serList.append(Variant(QDecimal(-123.456)).serialized());
   serList.append(Variant(QRational(-1234,1000)).serialized());
   serList.append(Variant(QValue(QDecimal(24.5), "EUR")).serialized());
   serList.append(Variant(QString("Falsches Üben von Xylophonmusik quält jeden größeren Zwerg. Test %-Zeichen.")).serialized());
   serList.append(Variant(QByteArray("Falsches Üben von Xylophonmusik quält jeden größeren Zwerg. Test %-Zeichen.")).serialized());
   serList.append(Variant(QTime(9, 20, 0, 333)).serialized());
   serList.append(Variant(QDateTime(QDate(2012, 2, 29), QTime(12, 30, 0))).serialized());
   serList.append(Variant(QSize(120, 80)).serialized());
   serList.append(Variant(Qt::red).serialized());
   serList.append(Variant(Qt::AlignCenter).serialized());

   Variant value;

   //
   // Deserialize
   //

   T_ASSERT(value.deserialize(serList.last()));
   T_EQUALS_EX(value.type(), typeid(Qt::AlignmentFlag));
   T_EQUALS((int)value.to<Qt::AlignmentFlag>(), (int)Qt::AlignCenter);
   serList.pop_back();

   T_ASSERT(value.deserialize(serList.last()));
   T_EQUALS_EX(value.type(), typeid(Qt::GlobalColor));
   T_EQUALS(value.to<Qt::GlobalColor>(), Qt::red);
   serList.pop_back();

   T_ASSERT(value.deserialize(serList.last()));
   T_EQUALS_EX(value.type(), typeid(QSize));
   T_EQUALS_EX(value.to<QSize>(), QSize(120, 80));
   serList.pop_back();

   T_ASSERT(value.deserialize(serList.last()));
   T_EQUALS_EX(value.type(), typeid(QDateTime));
   T_EQUALS_EX(value.to<QDateTime>(), QDateTime(QDate(2012, 2, 29), QTime(12, 30, 0)));
   serList.pop_back();

   T_ASSERT(value.deserialize(serList.last()));
   T_EQUALS_EX(value.type(), typeid(QTime));
   T_EQUALS_EX(value.to<QTime>(), QTime(9, 20, 0, 333));
   serList.pop_back();

   T_ASSERT(value.deserialize(serList.last()));
   T_EQUALS_EX(value.type(), typeid(QByteArray));
   T_EQUALS_EX(value.to<QByteArray>(), QByteArray("Falsches Üben von Xylophonmusik quält jeden größeren Zwerg. Test %-Zeichen."));
   serList.pop_back();

   T_ASSERT(value.deserialize(serList.last()));
   T_EQUALS_EX(value.type(), typeid(QString));
   T_EQUALS(value.to<QString>(), QString("Falsches Üben von Xylophonmusik quält jeden größeren Zwerg. Test %-Zeichen."));
   serList.pop_back();

   T_ASSERT(value.deserialize(serList.last()));
   T_EQUALS_EX(value.type(), typeid(QValue));
   T_EQUALS_EX(value.to<QValue>(), QValue(QDecimal(24.5), "EUR"));
   serList.pop_back();

   T_ASSERT(value.deserialize(serList.last()));
   T_EQUALS_EX(value.type(), typeid(QRational));
   T_EQUALS_EX(value.to<QRational>(), QRational(-1234,1000));
   serList.pop_back();

   T_ASSERT(value.deserialize(serList.last()));
   T_EQUALS_EX(value.type(), typeid(QDecimal));
   T_EQUALS_EX(value.to<QDecimal>(), QDecimal(-123.456));
   serList.pop_back();

   T_ASSERT(value.deserialize(serList.last()));
   T_EQUALS_EX(value.type(), typeid(double));
   T_EQUALS(value.to<double>(), -12345.987);
   serList.pop_back();

   T_ASSERT(value.deserialize(serList.last()));
   T_EQUALS_EX(value.type(), typeid(int));
   T_EQUALS(value.to<int>(), -25);
   serList.pop_back();

   T_ASSERT(value.deserialize(serList.last()));
   T_EQUALS_EX(value.type(), typeid(bool));
   T_EQUALS(value.to<bool>(), true);
   serList.pop_back();

   T_IS_TRUE(value.deserialize("2~0int~2~"));
   T_EQUALS_EX(value.type(), typeid(int));
   T_EQUALS(value.to<int>(), 2);

   T_IS_TRUE(value.deserialize("2~0QString~data~"));
   T_EQUALS_EX(value.type(), typeid(QString));
   T_EQUALS(value.to<QString>(), "data");

   //
   // Test old data
   //

   QByteArray testData = QByteArray("Falsches Üben von Xylophonmusik quält jeden größeren Zwerg. Test %-Zeichen.").toBase64();

   T_ASSERT(value.deserialize(QByteArray("0.?AVVQByteArray@@:") + testData));
   T_EQUALS_EX(value.type(), typeid(QByteArray));
   T_EQUALS_EX(value.to<QByteArray>(), QByteArray::fromBase64(testData));

   T_ASSERT(value.deserialize(QByteArray("0.?AVVQString@@:1,2,3,4,5,6,7,8")));
   T_EQUALS_EX(value.type(), typeid(QString));
   T_EQUALS(value.to<QString>(), QString("1,2,3,4,5,6,7,8"));

   T_ASSERT(value.deserialize(QByteArray("0.?AVVQString@@:Falsches Üben von Xylophonmusik quält jeden größeren Zwerg.")));
   T_EQUALS_EX(value.type(), typeid(QString));
   T_EQUALS(value.to<QString>(), QString("Falsches Üben von Xylophonmusik quält jeden größeren Zwerg."));

#ifdef Q_OS_WIN
   T_ASSERT(value.deserialize(QByteArray("1~.?AVQString@@~M%FC%ller~")));
#else
   T_ASSERT(value.deserialize(QByteArray("1~7QString~M%FC%ller~")));
#endif
   T_EQUALS_EX(value.type(), typeid(QString));
   T_EQUALS(value.to<QString>(), QString("Müller"));
}

void SettingTests::loadTests()
{
   QSettingsFile settings(eutf8("QtExtensionsTests"));

   QBuffer buffer;

   buffer.setData("test1=5\ntest2=Hallo\r\n[Section1]\nAttr1=Section content\nList = Entry1\nList= Entry2\r\n"
      "[Section1/Section2]\nAttr1=Subsection content\n[Section1/Section2/Section3]\nAttr5=Section3 content\n"
      "[]\n  Section1/Attr2 = Attribute for Section 1  \n");

   QSettingsFile settingsFile(&buffer);

   T_EQUALS(settingsFile.value(eutf8("test1")).toString(), eutf8("5"));
   T_EQUALS(settingsFile.value(eutf8("test2")).toString(), eutf8("Hallo"));
   T_EQUALS(settingsFile.value(eutf8("Section1/Attr1")).toString(), eutf8("Section content"));
   T_EQUALS(settingsFile.value(eutf8("Section1/Attr2")).toString(), eutf8(" Attribute for Section 1  "));
   T_EQUALS_EX(settingsFile.values(eutf8("Section1/List")), QList<Variant>() << eutf8(" Entry1") << eutf8(" Entry2"));
   T_EQUALS(settingsFile.stringValues(eutf8("Section1/List")).join(eutf8(";")), eutf8(" Entry1; Entry2"));
   T_EQUALS(settingsFile.value(eutf8("Section1/Section2/Attr1")).toString(), eutf8("Subsection content"));
   T_EQUALS(settingsFile.value(eutf8("Section1/Section2/Section3/Attr5")).toString(), eutf8("Section3 content"));

   T_EQUALS(settingsFile.beginGroup(eutf8("Section1/Section2")), 2);

   T_EQUALS(settingsFile.value(eutf8("Section3/Attr5")).toString(), eutf8("Section3 content"));

   T_IS_TRUE(settingsFile.endGroup());
}

void SettingTests::indentGroupTest()
{
   QBuffer buffer;

   buffer.setData(
      "test1=5\n"
      "test2=Hallo\n"
      "[Section1]\n"
      " Attr1=Section content\n"
      " List = Entry1\n"
      " [Section2]\n"
      "  Attr1=Subsection content\n"
      " List= Entry2\n"
      " [Section3]\n"
      "  Attr5=Section3 content\n"
      " Attr2 = Attribute for Section 1  \n");

   QSettingsFile settingsFile;

   settingsFile.addIniFile(&buffer);
   settingsFile.groupByIndentation();
   settingsFile.load();

   T_EQUALS(settingsFile.value(eutf8("test1")).toString(), eutf8("5"));
   T_EQUALS(settingsFile.value(eutf8("test2")).toString(), eutf8("Hallo"));
   T_EQUALS(settingsFile.value(eutf8("Section1/Attr1")).toString(), eutf8("Section content"));
   T_EQUALS(settingsFile.value(eutf8("Section1/Attr2")).toString(), eutf8(" Attribute for Section 1  "));
   T_EQUALS_EX(settingsFile.values(eutf8("Section1/List")), QList<Variant>() << eutf8(" Entry1") << eutf8(" Entry2"));
   T_EQUALS(settingsFile.stringValues(eutf8("Section1/List")).join(eutf8(";")), eutf8(" Entry1; Entry2"));
   T_EQUALS(settingsFile.value(eutf8("Section1/Section2/Attr1")).toString(), eutf8("Subsection content"));
   T_EQUALS(settingsFile.value(eutf8("Section1/Section3/Attr5")).toString(), eutf8("Section3 content"));
}

void SettingTests::braceGroupTest()
{
   QBuffer buffer;

   buffer.setData(
      "test1=5\n"
      "test2=Hallo\n"
      "[Section1]\n"
      "{\n"
      "Attr1=Section content\n"
      " List = Entry1\n"
      "List= Entry2\n"
      " [Section2]\n"
      " {\n"
      " Attr1=Subsection content\n"
      " [Section3]\n"
      "  Attr5=Section3 content\n"
      " }\n"
      "}\n"
      "Attr2 = Attribute for Section 1  \n");

   QSettingsFile settingsFile;

   settingsFile.addIniFile(&buffer);
   settingsFile.groupByBraces();
   settingsFile.load();

   T_EQUALS(settingsFile.value(eutf8("test1")).toString(), eutf8("5"));
   T_EQUALS(settingsFile.value(eutf8("test2")).toString(), eutf8("Hallo"));
   T_EQUALS(settingsFile.value(eutf8("Section1/Attr1")).toString(), eutf8("Section content"));
   T_EQUALS(settingsFile.value(eutf8("Section1/Attr2")).toString(), eutf8(" Attribute for Section 1  "));
   T_EQUALS_EX(settingsFile.values(eutf8("Section1/List")), QList<Variant>() << eutf8(" Entry1") << eutf8(" Entry2"));
   T_EQUALS(settingsFile.stringValues(eutf8("Section1/List")).join(eutf8(";")), eutf8(" Entry1; Entry2"));
   T_EQUALS(settingsFile.value(eutf8("Section1/Section2/Attr1")).toString(), eutf8("Subsection content"));
   T_EQUALS(settingsFile.value(eutf8("Section1/Section2/Section3/Attr5")).toString(), eutf8("Section3 content"));
}

void SerStreamTests::fileTests()
{
   if (dataDir().isEmpty())
   {
      addWarning("No data folder --> tests skipped");
      return;
   }

   auto fileName = QDir(dataDir()).filePath("SerStreamTestFile.dat");

   FILE* file;
#ifdef Q_OS_WIN
   T_ASSERT(fopen_s(&file, fileName.toLocal8Bit().data(), "wt") == 0);
#else
   file = fopen(fileName.toLocal8Bit().data(), "wt");
#endif
   T_ASSERT(file);

   SerStream out(nullptr, file);

   out << "ByteArray" << QString("Text") << 42;
   out.endRecord();
   out << "2.nd line" << 42.5;

   T_IS_TRUE(fclose(file) == 0);

   T_ERROR_IF(out.hasError(), out.errors().join("\n"));

#ifdef Q_OS_WIN
   T_ASSERT(fopen_s(&file, fileName.toLocal8Bit().data(), "rt") == 0);
#else
   file = fopen(fileName.toLocal8Bit().data(), "rt");
#endif
   T_ASSERT(file);

   QByteArray testByteArray;
   QString testText;
   int testInt;
   double testDouble;

   SerStream in(file, nullptr);

   in >> testByteArray >> testText >> testInt;
   if (in.atRecordEnd())
      in.skipRecordEnd();
   in >> testText >> testDouble;

   T_IS_TRUE(fclose(file) == 0);

   T_IS_TRUE(remove(fileName.toLocal8Bit().data()) == 0);

   T_ERROR_IF(in.hasError(), in.errors().join("\n"));
   T_EQUALS(QString(testByteArray), QString("ByteArray"));
   T_EQUALS(testText, "2.nd line");
   T_EQUALS_EX(testInt, 42);
   T_EQUALS_EX(testDouble, 42.5);
}

// This test was designed to test the self written binary tree dictionary
void VarTreeTests::dictTest()
{
   VarTree varTree;

   varTree.set("Var1", "data", typeid(QString));
   varTree.set("Var5", "data", typeid(QString));
   varTree.set("Var2", "data", typeid(QString));
   varTree.set("Var9", "data", typeid(QString));
   varTree.set("Var6", "data", typeid(QString));
   varTree.set("Var7", "data", typeid(QString));

   T_IS_TRUE(varTree.contains("Var5"));

   varTree.remove(QString(), "Var5");

   T_IS_TRUE(varTree.contains("Var1"));
   T_IS_TRUE(varTree.contains("Var2"));
   T_IS_TRUE(varTree.contains("Var6"));
   T_IS_TRUE(varTree.contains("Var7"));
   T_IS_TRUE(varTree.contains("Var9"));

   T_IS_TRUE(!varTree.contains("Var5"));
}

// This test was designed to test the self written binary tree dictionary
void VarTreeTests::basicTest()
{
   VarTree varTree;

   varTree.set("Var1", "data", typeid(QString));
   varTree.set("Level0.Var2", "data", typeid(QString));
   varTree.set("Level0.Var 3.νρτ", "data", typeid(QString));

   T_EQUALS_EX(varTree.value("Var1"), "data");
   T_EQUALS_EX(varTree.value("Level0.Var2"), "data");
   T_EQUALS_EX(varTree.value("1.Var2"), "data");
   T_EQUALS_EX(varTree.value("Level0[0]"), "data");
   T_EQUALS_EX(varTree.value("Level0.0"), "data");
   T_EQUALS_EX(varTree.value("Level0[1][0]"), "data");

   varTree.add("Var2.List", 1);
   varTree.add("Var2.List", 2);
   varTree.add("Var2.List", 3);

   const auto serialized = varTree.root()->serializeToAscii();

   T_EQUALS(QString(serialized), R"(
Var1:String=2~1QString~data~
Level0:
  Var2:String=2~1QString~data~
  Var%203:
    %%03BD%%03C1%%03C4:String=2~1QString~data~
Var2:
  List:
    :Integer=2~1int32~1~
    :Integer=2~1int32~2~
    :Integer=2~1int32~3~)");

   varTree = VarTree();
   T_ASSERT_EQUAL(varTree.root()->deserializeFromAscii(serialized), serialized.length());

   T_EQUALS_EX(varTree.value("Var1"), "data");
   T_EQUALS_EX(varTree.value("Level0.Var2"), "data");
   T_EQUALS_EX(varTree.value("1.Var2"), "data");
   T_EQUALS_EX(varTree.value("Level0[0]"), "data");
   T_EQUALS_EX(varTree.value("Level0.0"), "data");
   T_EQUALS_EX(varTree.value("Level0.Var 3.νρτ"), "data");

   T_EQUALS_EX(varTree.value("Var2.List[0]"), 1);
   T_EQUALS_EX(varTree.value("Var2.List[1]"), 2);
   T_EQUALS_EX(varTree.value("Var2.List[2]"), 3);
}

void VarTreeTests::tableTest()
{
   VarTree data;
   VarTree table = data.newDictionaryAt("Tabelle 1", "MyTableType");

   data.set("Variable1", "Wert1", "MyTextType");
   data.set("Üebrschrift1", "Überschrift 1", "MyTextType");
   data.set("Überschrift 2", "Überschrift 2", "MyTextType");
   data.set("Variable2", "Wert 2", "MyTextType");
   data.set("Variable3", 42, "MyNumericType");

   table.addColumn("Spalte1", "MyTextType");
   table.addColumn("Spalte2", "MyHtmlType");
   table.addColumn("Spalte 3", "MyNumericType");

   table.setRowCount(2);

   table.setCellValue(0, "Spalte1", "Zelle 11");
   table.setCellValue(0, "Spalte 3", 3000.9);
   table.setCellValue(1, "Spalte1", "Zelle 21");
   table.setCellValue(1, "Spalte 3", 17);
}

void ParserTests::csvTest()
{
   QString testInput = eutf8("  Text \t; \"String \" \n\n");
   QTextStream is(&testInput);
   CsvParser parser(&is, QLatin1Char(';'), QChar());

   auto record = parser.readRecord();

   T_ASSERT(record.count() == 2);
   T_EQUALS(record[0], eutf8("Text"));
   T_EQUALS(record[1], eutf8("\"String \""));

   record = parser.readRecord();

   T_EQUALS(record.count(), 1);

   record = parser.readRecord();

   T_EQUALS(record.count(), 0);
}

void ParserTests::cppTests()
{
   QString testCode = "#define MACRO1 Inhalt\n"
      "#   define FMACRO(PAR1) Content // Comment  \n"
      "int a = 5;\n";

   auto tokenList = Tokenizer(testCode, createCppContext()).tokenList();

   T_ASSERT_EQUAL(tokenList.count(), 26);

   auto index = 0;

   T_ASSERT(tokenList[index++].is<CppDirectiveToken>());
   T_ASSERT(tokenList[index++].is<Token::WhiteSpace>());
   T_ASSERT(tokenList[index++].is<Token::Identifier>());
   T_ASSERT(tokenList[index++].is<Token::WhiteSpace>());
   T_ASSERT(tokenList[index++].is<Token::Identifier>());
   T_ASSERT(tokenList[index++].is<Token::WhiteSpace>());
   T_ASSERT(tokenList[index++].is<CppDirectiveToken>());
   T_ASSERT(tokenList[index++].is<Token::WhiteSpace>());
   T_ASSERT(tokenList[index++].is<Token::Identifier>());
   T_ASSERT(tokenList[index++].is<CppOperatorToken>());
   T_ASSERT(tokenList[index++].is<Token::Identifier>());
   T_ASSERT(tokenList[index++].is<CppOperatorToken>());
   T_ASSERT(tokenList[index++].is<Token::WhiteSpace>());
   T_ASSERT(tokenList[index++].is<Token::Identifier>());
   T_ASSERT(tokenList[index++].is<Token::WhiteSpace>());
   T_ASSERT(tokenList[index++].is<Token::Comment>());
   T_ASSERT(tokenList[index++].is<Token::WhiteSpace>());
   T_ASSERT(tokenList[index].is<CppIdentifierToken>());
   T_EQUALS(tokenList[index++].as<CppIdentifierToken>()->keyword(), KW_int);
   T_ASSERT(tokenList[index++].is<Token::WhiteSpace>());
   T_ASSERT(tokenList[index++].is<Token::Identifier>());
   T_ASSERT(tokenList[index++].is<Token::WhiteSpace>());
   T_ASSERT(tokenList[index++].is<CppOperatorToken>());
   T_ASSERT(tokenList[index++].is<Token::WhiteSpace>());
   T_ASSERT(tokenList[index++].is<Token::Integer>());
   T_ASSERT(tokenList[index++].is<CppOperatorToken>());
   T_ASSERT(tokenList[index++].is<Token::WhiteSpace>());

   QByteArray testData;

   for (auto i = 0; i < 256; ++i)
      testData.append(i);

   const auto base256 = toBase256(testData);
   testData = fromBase256(base256);

   T_ASSERT_EQUAL(testData.length(), 256);

   for (auto i = 0; i < 256; ++i)
      T_ASSERT_EQUAL((unsigned char)testData.at(i), i);
}

void SqlExpressionTests::basic_tests()
{
   auto exprTree = createSqlExpressionTree("col1 between 5 and 7");
   auto opExpr = exprTree.as<AbstractOperatorExpression>();

   T_ASSERT(opExpr);
   T_ASSERT_EQUAL(opExpr->op().toString(), "BETWEEN");
   T_ASSERT_EQUAL(opExpr->operandsCount(), 2);
   T_ASSERT(opExpr->operandAt(0).is<AbstractValueExpression>());
   T_ASSERT(opExpr->operandAt(1).is<AbstractOperatorExpression>());
   T_ASSERT_EQUAL(opExpr->operandAt(1).as<AbstractOperatorExpression>()->op().toString(), "AND");
   T_ASSERT_EQUAL(opExpr->operandAt(1).as<AbstractOperatorExpression>()->operandsCount(), 2);
   T_ASSERT(opExpr->operandAt(1).as<AbstractOperatorExpression>()->operandAt(0).is<AbstractValueExpression>());
   T_ASSERT(opExpr->operandAt(1).as<AbstractOperatorExpression>()->operandAt(1).is<AbstractValueExpression>());

   exprTree = createSqlExpressionTree("col1 not between 5 and 7 and col2 != 'OK'");
   opExpr = exprTree.as<AbstractOperatorExpression>();

   T_ASSERT(opExpr);
   T_ASSERT_EQUAL(opExpr->op().toString(), "NOT BETWEEN");
   T_ASSERT_EQUAL(opExpr->operandsCount(), 2);
   T_ASSERT(opExpr->operandAt(0).is<AbstractValueExpression>());
   T_ASSERT(opExpr->operandAt(1).is<AbstractOperatorExpression>());
   T_ASSERT_EQUAL(opExpr->operandAt(1).as<AbstractOperatorExpression>()->op().toString(), "AND");
   T_ASSERT_EQUAL(opExpr->operandAt(1).as<AbstractOperatorExpression>()->operandsCount(), 3);
   T_ASSERT(opExpr->operandAt(1).as<AbstractOperatorExpression>()->operandAt(0).is<AbstractValueExpression>());
   T_ASSERT(opExpr->operandAt(1).as<AbstractOperatorExpression>()->operandAt(1).is<AbstractValueExpression>());
   T_ASSERT(opExpr->operandAt(1).as<AbstractOperatorExpression>()->operandAt(1).is<AbstractValueExpression>());

   exprTree = createSqlExpressionTree("(col1 between 5 and 7) and col2 != 'OK'").as<AbstractOperatorExpression>();
   opExpr = exprTree.as<AbstractOperatorExpression>();

   T_ASSERT(opExpr);
   T_ASSERT_EQUAL(opExpr->op().toString(), "AND");
   T_ASSERT_EQUAL(opExpr->operandsCount(), 2);
   T_ASSERT(!opExpr->operandAt(0).is<AbstractValueExpression>());
   T_ASSERT(opExpr->operandAt(1).is<AbstractOperatorExpression>());
   T_ASSERT_EQUAL(opExpr->operandAt(1).as<AbstractOperatorExpression>()->op().toString(), "!=");

   opExpr = opExpr->operands().at(0).as<AbstractOperatorExpression>();

   T_ASSERT_EQUAL(opExpr->op().toString(), "BETWEEN");
   T_ASSERT_EQUAL(opExpr->operandsCount(), 2);
   T_ASSERT(opExpr->operandAt(0).is<AbstractValueExpression>());
   T_ASSERT(opExpr->operandAt(1).is<AbstractOperatorExpression>());
   T_ASSERT_EQUAL(opExpr->operandAt(1).as<AbstractOperatorExpression>()->op().toString(), "AND");
   T_ASSERT_EQUAL(opExpr->operandAt(1).as<AbstractOperatorExpression>()->operandsCount(), 2);
   T_ASSERT(opExpr->operandAt(1).as<AbstractOperatorExpression>()->operandAt(0).is<AbstractValueExpression>());
   T_ASSERT(opExpr->operandAt(1).as<AbstractOperatorExpression>()->operandAt(1).is<AbstractValueExpression>());
}

void CppExpressionTests::basic_tests()
{
   ExpressionPtr exprTree;
   VarTree varTree;
   const auto basicFunctions = createCppResolverFunctions();

   // Test empty expression

   try
   {
      createVersatileExpressionTree(";");
      T_SIGNAL_ERROR(tr("Expression \";\" must throw an exception"));
   }
   catch (const QBasicException& exception)
   {
      T_EQUALS(exception.message(), ExpressionParser::tr("Postfix operator has no operand on the left side"));
   }

   try
   {
      exprTree = createVersatileExpressionTree("0xAf;");
      T_EQUALS(exprTree->resolve(varTree.root())->value().toInt(), 175);
   }
   catch (const QBasicException& exception)
   {
      T_SIGNAL_ERROR(exception.message());
   }

   try
   {
      exprTree = createVersatileExpressionTree("0x\"000F10A0FF02\"");
      T_EQUALS(QString(exprTree->resolve(varTree.root())->value().toByteArray().toHex()), QString("000f10a0ff02"));
   }
   catch (const QBasicException& exception)
   {
      T_SIGNAL_ERROR(exception.message());
   }

   try
   {
      QByteArray testData;

      for (auto i = 0; i < 256; ++i)
         testData.append(i);

      auto context = new ProbingContext;

      context->add(new Base256DataContext(context));

      exprTree = createCppExpressionTree(Tokenizer(QString("0b\"%1\"").arg(toBase256(testData)), context).tokenList());
      T_IS_TRUE(exprTree->resolve(varTree.root())->value().toByteArray() == testData);
   }
   catch (const QBasicException& exception)
   {
      T_SIGNAL_ERROR(exception.message());
   }

   // Test condition operator

   exprTree = createVersatileExpressionTree("source.value1 + source.value2 > 5 ? source.object.id : source.other.id");

   varTree.set("source.value1", 1, typeid(int));
   varTree.set("source.value2", 5, typeid(int));
   varTree.set("source.object.id", 42, typeid(int));
   varTree.set("source.other.id", 7, typeid(int));

   T_EQUALS(exprTree->resolve(varTree.root())->value().toInt(), 42);

   // Test logical operators

   varTree.set("source.text1", "Erster Text", typeid(QString));
   varTree.set("source.text2", "Zweiter Text", typeid(QString));

   try
   {
      exprTree = createVersatileExpressionTree("source.value1 > 5 && source.value2 > 5 ? source.object.id : source.text1 + \", \" + source.text2");

      const auto result = exprTree->resolve(varTree.root());
      T_ASSERT(result);
      T_ASSERT_EQUAL(result->itemCount(), 0);
      T_EQUALS(result->value().toString(), "Erster Text, Zweiter Text");
   }
   catch (const QBasicException& exception)
   {
      T_SIGNAL_ERROR(exception.message());
   }

   // Test side effects

   try
   {
      exprTree = createVersatileExpressionTree("\"Text \" + source.value1 + 1");

      const auto result = exprTree->resolve(varTree.root());
      T_SIGNAL_ERROR(tr("Operator + with QString and int must throw an exception"));
   }
   catch (const QBasicException& exception)
   {
      auto expected = CppOperator::tr("Binary operation %1 not allowed for the types %2 and %3").arg("+").arg("QString").arg("int32");
      T_EQUALS(exception.message(), expected);
   }

   try
   {
      exprTree = createVersatileExpressionTree("(1 + 2) + 3");

      const auto result = exprTree->resolve(varTree.root());
      T_ASSERT(result);
      T_ASSERT_EQUAL(result->itemCount(), 0);
      T_EQUALS(result->value().toInt(), 6);
   }
   catch (const QBasicException& exception)
   {
      T_SIGNAL_ERROR(exception.message());
   }

   try
   {
      exprTree = createCppExpressionTree("R\"(Text\n)\" + (source.value2 - -source.value1).toString()");

      const auto result = exprTree->resolve(varTree.root(), basicFunctions);
      T_ASSERT(result);
      T_ASSERT_EQUAL(result->itemCount(), 0);
      T_EQUALS(result->value().toString(), "Text\n6");
   }
   catch (const QBasicException& exception)
   {
      T_SIGNAL_ERROR(exception.message());
   }

   try
   {
      exprTree = createVersatileExpressionTree("source.value1++ + 2");

      const auto result = exprTree->resolve(varTree.root());
      T_ASSERT(result);
      T_ASSERT_EQUAL(result->itemCount(), 0);
      T_EQUALS(result->value().toInt(), 3);
      T_EQUALS(varTree.value("source.value1").toInt(), 2);
   }
   catch (const QBasicException& exception)
   {
      T_SIGNAL_ERROR(exception.message());
   }

   try
   {
      exprTree = createVersatileExpressionTree("('one', 'two', 'three') + ('four', 'five')");

      const auto result = exprTree->resolve(varTree.root());
      T_ASSERT(result);
      T_ASSERT_EQUAL(result->itemCount(), 5);
      T_EQUALS(result->getItem(0)->value().toString(), "one");
      T_EQUALS(result->getItem(1)->value().toString(), "two");
      T_EQUALS(result->getItem(2)->value().toString(), "three");
      T_EQUALS(result->getItem(3)->value().toString(), "four");
      T_EQUALS(result->getItem(4)->value().toString(), "five");
   }
   catch (const QBasicException& exception)
   {
      T_SIGNAL_ERROR(exception.message());
   }

   try
   {
      exprTree = createVersatileExpressionTree("('one', 'two', 'three') & ('four', 'five')");

      const auto result = exprTree->resolve(varTree.root());
      T_ASSERT(result);
      T_ASSERT_EQUAL(result->itemCount(), 2);
      T_ASSERT_EQUAL(result->getItem(0)->itemCount(), 2);
      T_EQUALS(result->getItem(0)->getItem(0)->value().toString(), "one");
      T_EQUALS(result->getItem(0)->getItem(1)->value().toString(), "four");
      T_ASSERT_EQUAL(result->getItem(1)->itemCount(), 2);
      T_EQUALS(result->getItem(1)->getItem(0)->value().toString(), "two");
      T_EQUALS(result->getItem(1)->getItem(1)->value().toString(), "five");
   }
   catch (const QBasicException& exception)
   {
      T_SIGNAL_ERROR(exception.message());
   }

   try
   {
      exprTree = createVersatileExpressionTree("('one', 'two', 'three') | ('four', 'five')");

      const auto result = exprTree->resolve(varTree.root());
      T_ASSERT(result);
      T_ASSERT_EQUAL(result->itemCount(), 3);
      T_ASSERT_EQUAL(result->getItem(0)->itemCount(), 2);
      T_EQUALS(result->getItem(0)->getItem(0)->value().toString(), "one");
      T_EQUALS(result->getItem(0)->getItem(1)->value().toString(), "four");
      T_ASSERT_EQUAL(result->getItem(1)->itemCount(), 2);
      T_EQUALS(result->getItem(1)->getItem(0)->value().toString(), "two");
      T_EQUALS(result->getItem(1)->getItem(1)->value().toString(), "five");
      T_ASSERT_EQUAL(result->getItem(2)->itemCount(), 1);
      T_EQUALS(result->getItem(2)->getItem(0)->value().toString(), "three");
   }
   catch (const QBasicException& exception)
   {
      T_SIGNAL_ERROR(exception.message());
   }

   // Test repeating operation

   VarPtr sourceList = new VarTree::Variable;

   sourceList->setItem(0, sourceList->newFromValue(10));
   sourceList->setItem(1, sourceList->newFromValue(20));
   sourceList->setItem(2, sourceList->newFromValue(30));
   sourceList->setItem(3, sourceList->newFromValue(40));
   sourceList->setItem(4, sourceList->newFromValue(50));

   varTree.set("source.list", sourceList);

   try
   {
      exprTree = createVersatileExpressionTree(R"(functions := new(); functions.addVar("f1", new("Function")) = {base.setVarNames("pars"); pars.i++}; vars.i := 2; (vars.i < 5) # source.list[functions.f1(vars)])");

      const auto result = exprTree->resolve(varTree.root(), basicFunctions);
      T_ASSERT(result);
      T_ASSERT_EQUAL(result->itemCount(), 3);
      T_EQUALS(result->getItem(0)->value().toInt(), 30);
      T_EQUALS(result->getItem(1)->value().toInt(), 40);
      T_EQUALS(result->getItem(2)->value().toInt(), 50);
   }
   catch (const QBasicException& exception)
   {
      T_SIGNAL_ERROR(exception.message());
   }

   // Test build-in functions

   try
   {
      exprTree = createVersatileExpressionTree(R"(base.addVar("vars", new()))");

      const VarTree testVarTree;

      const auto result = exprTree->resolve(testVarTree.root(), basicFunctions);
      T_ASSERT(result);
      T_EQUALS(result->itemCount(), 0);
      T_EQUALS((int)result->basicType(), (int)IVariable::BasicType::Collection);
      T_IS_TRUE(!testVarTree.variable("vars").isNull());
   }
   catch (const QBasicException& exception)
   {
      T_SIGNAL_ERROR(exception.message());
   }

   try
   {
      exprTree = createVersatileExpressionTree(R"(base.vars.test := true)");

      const VarTree testVarTree;

      const auto result = exprTree->resolve(testVarTree.root(), basicFunctions);
      T_ASSERT(result);
      T_EQUALS_EX(result->value(), Variant(true));
      T_IS_TRUE(!testVarTree.variable("vars").isNull());
      T_EQUALS_EX(testVarTree.value("vars.test"), Variant(true));
   }
   catch (const QBasicException& exception)
   {
      T_SIGNAL_ERROR(exception.message());
   }

   try
   {
      exprTree = createVersatileExpressionTree(R"(addVar("vars", new()))");

      const VarTree testVarTree;

      const auto result = exprTree->resolve(testVarTree.root(), basicFunctions);
      T_ASSERT(result);
      T_EQUALS(result->itemCount(), 0);
      T_EQUALS((int)result->basicType(), (int)IVariable::BasicType::Collection);
      T_EQUALS(testVarTree.root()->itemCount(), 0);
   }
   catch (const QBasicException& exception)
   {
      T_SIGNAL_ERROR(exception.message());
   }

   try
   {
      exprTree = createVersatileExpressionTree(R"(("Val1", "Val2", "Val3").setVarNames("Var1", "Var2", "Var3"))");

      const auto result = exprTree->resolve(VarTree().root(), basicFunctions);
      T_ASSERT(result);
      T_EQUALS(result->itemCount(), 3);
      T_IS_TRUE(result->itemNames() == QStringList() << "Var1" << "Var2" << "Var3");
   }
   catch (const QBasicException& exception)
   {
      T_SIGNAL_ERROR(exception.message());
   }

   try
   {
      exprTree = createVersatileExpressionTree(R"(base.getDict("Test"))");

      const auto result = exprTree->resolve(VarTree().root(), basicFunctions);
      T_ASSERT(result);
      T_EQUALS(result->itemCount(), 0);
      T_EQUALS((int)result->basicType(), (int)IVariable::BasicType::Collection);
   }
   catch (const QBasicException& exception)
   {
      T_SIGNAL_ERROR(exception.message());
   }

   try
   {
      exprTree = createVersatileExpressionTree("today()");

      const auto result = exprTree->resolve(VarTree().root(), basicFunctions);
      T_ASSERT(result);
      T_EQUALS_EX(result->value(), QDateTimeEx::currentDate());
   }
   catch(const QBasicException& exception)
   {
      T_SIGNAL_ERROR(exception.message());
   }

   try
   {
      exprTree = createVersatileExpressionTree("now()");

      const auto result = exprTree->resolve(VarTree().root(), basicFunctions);
      T_ASSERT(result);
      T_IS_TRUE(result->value().type() == typeid(QDateTimeEx));
   }
   catch(const QBasicException& exception)
   {
      T_SIGNAL_ERROR(exception.message());
   }

   try
   {
      exprTree = createVersatileExpressionTree("today().toString(\"en-us\") + \", Buxtehude\"");

      const auto result = exprTree->resolve(VarTree().root(), basicFunctions);
      T_ASSERT(result);
      T_EQUALS(result->value().toString(), QLocaleEx("en-us").toString(QDateTimeEx::currentDate()) + ", Buxtehude");
   }
   catch (const QBasicException& exception)
   {
      T_SIGNAL_ERROR(exception.message());
   }

   try
   {
      exprTree = createVersatileExpressionTree("today().toString(\"en-us\", true)");

      const auto result = exprTree->resolve(VarTree().root(), basicFunctions);
      T_ASSERT(result);
      T_EQUALS(result->value().toString(), QLocaleEx("en-us", DateTimePart::Second, QLocale::LongFormat).toString(QDateTimeEx::currentDate()));
   }
   catch (const QBasicException& exception)
   {
      T_SIGNAL_ERROR(exception.message());
   }

   try
   {
      exprTree = createVersatileExpressionTree("today().toString(\"en-us\", false)");

      const auto result = exprTree->resolve(VarTree().root(), basicFunctions);
      T_ASSERT(result);
      T_EQUALS(result->value().toString(), QLocaleEx("en-us", DateTimePart::Minute, QLocale::ShortFormat).toString(QDateTimeEx::currentDate()));
   }
   catch (const QBasicException& exception)
   {
      T_SIGNAL_ERROR(exception.message());
   }

   try
   {
      exprTree = createVersatileExpressionTree("12.5.toString(\"en-us\")");

      const auto result = exprTree->resolve(VarTree().root(), basicFunctions);
      T_ASSERT(result);
      T_EQUALS(result->value().toString(), "12.5");
   }
   catch (const QBasicException& exception)
   {
      T_SIGNAL_ERROR(exception.message());
   }

   try
   {
      exprTree = createVersatileExpressionTree("1212.5.toString(\"de-de\")");

      const auto result = exprTree->resolve(VarTree().root(), basicFunctions);
      T_ASSERT(result);
      T_EQUALS(result->value().toString(), "1.212,5");
   }
   catch (const QBasicException& exception)
   {
      T_SIGNAL_ERROR(exception.message());
   }

   try
   {
      exprTree = createVersatileExpressionTree("1212.5.toString(\"de-de\", false)");

      const auto result = exprTree->resolve(VarTree().root(), basicFunctions);
      T_ASSERT(result);
      T_EQUALS(result->value().toString(), "1212,5");
   }
   catch (const QBasicException& exception)
   {
      T_SIGNAL_ERROR(exception.message());
   }

   try
   {
      exprTree = createVersatileExpressionTree("1212.toString(\"de-de\")");

      const auto result = exprTree->resolve(VarTree().root(), basicFunctions);
      T_ASSERT(result);
      T_EQUALS(result->value().toString(), "1212");
   }
   catch (const QBasicException& exception)
   {
      T_SIGNAL_ERROR(exception.message());
   }

   try
   {
      exprTree = createVersatileExpressionTree("1212.toString(\"de-de\", true)");

      const auto result = exprTree->resolve(VarTree().root(), basicFunctions);
      T_ASSERT(result);
      T_EQUALS(result->value().toString(), "1.212");
   }
   catch (const QBasicException& exception)
   {
      T_SIGNAL_ERROR(exception.message());
   }

   try
   {
      exprTree = createVersatileExpressionTree("source.list.varCount()");

      const auto result = exprTree->resolve(varTree.root(), basicFunctions);
      T_ASSERT(result);
      T_EQUALS_EX(result->value(), 5);
   }
   catch (const QBasicException& exception)
   {
      T_SIGNAL_ERROR(exception.message());
   }

   try
   {
      exprTree = createVersatileExpressionTree("source.list.append(new(\"String\")), source.list.append(\"Text\")");

      const auto result = exprTree->resolve(varTree.root(), basicFunctions);
      T_ASSERT(result);
      T_ASSERT_EQUAL(result->itemCount(), 2);
      T_EQUALS(varTree.variable("source.list")->itemCount(), 7);
      T_EQUALS(result->getItem(0)->typeName(), "String");
      T_EQUALS_EX(result->getItem(1)->value(), "Text");
      T_EQUALS(varTree.variable("source.list")->getItem(5)->typeName(), "String");
      T_EQUALS((int)varTree.variable("source.list")->getItem(5)->basicType(), (int)IVariable::BasicType::Value);
      T_EQUALS(varTree.variable("source.list")->getItem(5)->value().toString(), "");
      T_EQUALS(varTree.variable("source.list")->getItem(6)->value().toString(), "Text");
   }
   catch (const QBasicException& exception)
   {
      T_SIGNAL_ERROR(exception.message());
   }

   try
   {
      exprTree = createVersatileExpressionTree("source.list.mid(source.list.length() - 2)");

      const auto result = exprTree->resolve(varTree.root(), basicFunctions);
      T_ASSERT(result);
      T_EQUALS(result->itemCount(), 2);
      T_EQUALS(result->getItem(0)->typeName(), "String");
      T_EQUALS(result->getItem(1)->value().toString(), "Text");
   }
   catch (const QBasicException& exception)
   {
      T_SIGNAL_ERROR(exception.message());
   }

   try
   {
      exprTree = createVersatileExpressionTree("source.list.mid(1, 3)");

      const auto result = exprTree->resolve(varTree.root(), basicFunctions);
      T_ASSERT(result);
      T_EQUALS(result->itemCount(), 3);
      T_EQUALS(result->getItem(0)->value().toInt(), 20);
      T_EQUALS(result->getItem(1)->value().toInt(), 30);
      T_EQUALS(result->getItem(2)->value().toInt(), 40);
   }
   catch (const QBasicException& exception)
   {
      T_SIGNAL_ERROR(exception.message());
   }

   try
   {
      exprTree = createVersatileExpressionTree("text := \"Hallo Welt\"; text.mid(text.length() - 2)");

      const auto result = exprTree->resolve(varTree.root(), basicFunctions);
      T_ASSERT(result);
      T_EQUALS(result->value().toString(), "lt");
   }
   catch (const QBasicException& exception)
   {
      T_SIGNAL_ERROR(exception.message());
   }

   try
   {
      exprTree = createVersatileExpressionTree("text := \"Hallo Welt\"; text.mid(1, 3)");

      const auto result = exprTree->resolve(varTree.root(), basicFunctions);
      T_ASSERT(result);
      T_EQUALS(result->value().toString(), "all");
   }
   catch (const QBasicException& exception)
   {
      T_SIGNAL_ERROR(exception.message());
   }

   try
   {
      exprTree = createVersatileExpressionTree("text := \"Hallo Welt\"; text.startsWith(\"Hallo\")");

      const auto result = exprTree->resolve(varTree.root(), basicFunctions);
      T_ASSERT(result);
      T_IS_TRUE(result->value().toBool());
   }
   catch (const QBasicException& exception)
   {
      T_SIGNAL_ERROR(exception.message());
   }

   try
   {
      exprTree = createVersatileExpressionTree("text := \"Hallo Welt\"; text.endsWith(\"Welt\")");

      const auto result = exprTree->resolve(varTree.root(), basicFunctions);
      T_ASSERT(result);
      T_IS_TRUE(result->value().toBool());
   }
   catch (const QBasicException& exception)
   {
      T_SIGNAL_ERROR(exception.message());
   }

   try
   {
      exprTree = createVersatileExpressionTree("text := \"Hallo Welt\"; text.contains(\"lo w\", true)");

      const auto result = exprTree->resolve(varTree.root(), basicFunctions);
      T_ASSERT(result);
      T_IS_TRUE(result->value().toBool());
   }
   catch (const QBasicException& exception)
   {
      T_SIGNAL_ERROR(exception.message());
   }

   try
   {
      exprTree = createVersatileExpressionTree("('One', 'Two', 'Three').startsWith('One')");

      const auto result = exprTree->resolve(varTree.root(), basicFunctions);
      T_ASSERT(result);
      T_IS_TRUE(result->value().toBool());
   }
   catch (const QBasicException& exception)
   {
      T_SIGNAL_ERROR(exception.message());
   }

   try
   {
      exprTree = createVersatileExpressionTree("('One', 'Two', 'Three').endsWith('Three')");

      const auto result = exprTree->resolve(varTree.root(), basicFunctions);
      T_ASSERT(result);
      T_IS_TRUE(result->value().toBool());
   }
   catch (const QBasicException& exception)
   {
      T_SIGNAL_ERROR(exception.message());
   }

   try
   {
      exprTree = createVersatileExpressionTree("('One', 'Two', 'Three').contains('Two')");

      const auto result = exprTree->resolve(varTree.root(), basicFunctions);
      T_ASSERT(result);
      T_IS_TRUE(result->value().toBool());
   }
   catch (const QBasicException& exception)
   {
      T_SIGNAL_ERROR(exception.message());
   }

   try
   {
      exprTree = createVersatileExpressionTree("('One', 'Two', 'Three', 'Two', 'Four', 'One').distinct()");

      const auto result = exprTree->resolve(varTree.root(), basicFunctions);
      T_ASSERT(result);
      T_EQUALS(result->itemCount(), 4);
      T_EQUALS(result->getItem(0)->value().toString(), "One");
      T_EQUALS(result->getItem(1)->value().toString(), "Two");
      T_EQUALS(result->getItem(2)->value().toString(), "Three");
      T_EQUALS(result->getItem(3)->value().toString(), "Four");
   }
   catch (const QBasicException& exception)
   {
      T_SIGNAL_ERROR(exception.message());
   }

   try
   {
      exprTree = createVersatileExpressionTree("('One', 'Two', 'Three', 'Two', 'Four', 'One').where({base[0].contains('w')})");

      const auto result = exprTree->resolve(varTree.root(), basicFunctions);
      T_ASSERT(result);
      T_EQUALS(result->itemCount(), 2);
      T_EQUALS(result->getItem(0)->value().toString(), "Two");
      T_EQUALS(result->getItem(1)->value().toString(), "Two");
   }
   catch (const QBasicException& exception)
   {
      T_SIGNAL_ERROR(exception.message());
   }

   try
   {
      exprTree = createVersatileExpressionTree("source.list.where({base.setVarNames('Number'); Number.varType() == \"Integer\" && Number > 30 })");

      const auto result = exprTree->resolve(varTree.root(), basicFunctions);
      T_ASSERT(result);
      T_EQUALS(result->itemCount(), 2);
      T_EQUALS(result->getItem(0)->value().toInt(), 40);
      T_EQUALS(result->getItem(1)->value().toInt(), 50);
   }
   catch (const QBasicException& exception)
   {
      T_SIGNAL_ERROR(exception.message());
   }

   try
   {
      exprTree = createVersatileExpressionTree("'The house is blue'.replace('blue', 'white')");

      const auto result = exprTree->resolve(varTree.root(), basicFunctions);
      T_ASSERT(result);
      T_EQUALS(result->value().toString(), "The house is white");
   }
   catch (const QBasicException& exception)
   {
      T_SIGNAL_ERROR(exception.message());
   }

   try
   {
      exprTree = createVersatileExpressionTree("'Text'.leftJustified(7, '.')");

      const auto result = exprTree->resolve(varTree.root(), basicFunctions);
      T_ASSERT(result);
      T_EQUALS(result->value().toString(), "Text...");
   }
   catch (const QBasicException& exception)
   {
      T_SIGNAL_ERROR(exception.message());
   }

   try
   {
      exprTree = createVersatileExpressionTree("'Text'.rightJustified(7, '.')");

      const auto result = exprTree->resolve(varTree.root(), basicFunctions);
      T_ASSERT(result);
      T_EQUALS(result->value().toString(), "...Text");
   }
   catch (const QBasicException& exception)
   {
      T_SIGNAL_ERROR(exception.message());
   }

   try
   {
      exprTree = createVersatileExpressionTree("'Text'.toUpper()");

      const auto result = exprTree->resolve(varTree.root(), basicFunctions);
      T_ASSERT(result);
      T_EQUALS(result->value().toString(), "TEXT");
   }
   catch (const QBasicException& exception)
   {
      T_SIGNAL_ERROR(exception.message());
   }

   try
   {
      exprTree = createVersatileExpressionTree("'Text'.toLower()");

      const auto result = exprTree->resolve(varTree.root(), basicFunctions);
      T_ASSERT(result);
      T_EQUALS(result->value().toString(), "text");
   }
   catch (const QBasicException& exception)
   {
      T_SIGNAL_ERROR(exception.message());
   }

   try
   {
      exprTree = createVersatileExpressionTree("'1024.6'.toDecimal('en-us')");

      const auto result = exprTree->resolve(varTree.root(), basicFunctions);
      T_ASSERT(result);
      T_EQUALS_EX(result->value().toDecimal(), QDecimal(1024.6));
   }
   catch (const QBasicException& exception)
   {
      T_SIGNAL_ERROR(exception.message());
   }

   try
   {
      exprTree = createVersatileExpressionTree("'12,4'.toNumber('de-de')");

      const auto result = exprTree->resolve(varTree.root(), basicFunctions);
      T_ASSERT(result);
      T_EQUALS_EX(result->value().toDecimal(), QDecimal(12.4));
   }
   catch (const QBasicException& exception)
   {
      T_SIGNAL_ERROR(exception.message());
   }

   try
   {
      exprTree = createVersatileExpressionTree("'9876543210'.toInteger()");

      const auto result = exprTree->resolve(varTree.root(), basicFunctions);
      T_ASSERT(result);
      T_EQUALS_EX(result->value().toInt64(), 9876543210LL);
   }
   catch (const QBasicException& exception)
   {
      T_SIGNAL_ERROR(exception.message());
   }

   try
   {
      exprTree = createVersatileExpressionTree("'12/31/19'.toDate('en-us', false)");

      const auto result = exprTree->resolve(varTree.root(), basicFunctions);
      T_ASSERT(result);
      T_EQUALS_EX(result->value().toDateEx(), QDateEx(1919, 12, 31));
   }
   catch (const QBasicException& exception)
   {
      T_SIGNAL_ERROR(exception.message());
   }

   try
   {
      exprTree = createVersatileExpressionTree("12.5.round()");

      const auto result = exprTree->resolve(varTree.root(), basicFunctions);
      T_ASSERT(result);
      T_EQUALS_EX(result->value().toDecimal(), QDecimal(13));
   }
   catch (const QBasicException& exception)
   {
      T_SIGNAL_ERROR(exception.message());
   }

   try
   {
      exprTree = createVersatileExpressionTree("max(1, 5)");

      const auto result = exprTree->resolve(varTree.root(), basicFunctions);
      T_ASSERT(result);
      T_EQUALS_EX(result->value().toInt(), 5);
   }
   catch (const QBasicException& exception)
   {
      T_SIGNAL_ERROR(exception.message());
   }

   try
   {
      exprTree = createVersatileExpressionTree("min(1, 5)");

      const auto result = exprTree->resolve(varTree.root(), basicFunctions);
      T_ASSERT(result);
      T_EQUALS_EX(result->value().toInt(), 1);
   }
   catch (const QBasicException& exception)
   {
      T_SIGNAL_ERROR(exception.message());
   }

   try
   {
      exprTree = createVersatileExpressionTree(R"(Test.Number := 5; Test.Text := 'Hallo'; Test.takeVar("Number"), Test.takeVar(0), Test.varCount())");

      const auto result = exprTree->resolve(varTree.root(), basicFunctions);
      T_ASSERT(result);
      T_EQUALS_EX(result->itemCount(), 3);
      T_EQUALS_EX(result->getItem(0)->value().toInt(), 5);
      T_EQUALS(result->getItem(1)->value().toString(), "Hallo");
      T_EQUALS_EX(result->getItem(2)->value().toInt(), 0);
   }
   catch (const QBasicException& exception)
   {
      T_SIGNAL_ERROR(exception.message());
   }

   try
   {
      exprTree = createVersatileExpressionTree("a := 0; if (1 == 1, a = 1, a = 2)");

      const auto result = exprTree->resolve(varTree.root(), basicFunctions);
      T_ASSERT(result);
      T_EQUALS(result->value().toInt(), 2);
   }
   catch (const QBasicException& exception)
   {
      T_SIGNAL_ERROR(exception.message());
   }

   try
   {
      exprTree = createVersatileExpressionTree("source.list # this.varType()");

      const auto result = exprTree->resolve(varTree.root(), basicFunctions);
      T_ASSERT(result);
      T_EQUALS(result->itemCount(), 7);
      T_EQUALS(result->getItem(0)->value().toString(), "Integer");
      T_EQUALS(result->getItem(1)->value().toString(), "Integer");
      T_EQUALS(result->getItem(2)->value().toString(), "Integer");
      T_EQUALS(result->getItem(3)->value().toString(), "Integer");
      T_EQUALS(result->getItem(4)->value().toString(), "Integer");
      T_EQUALS(result->getItem(5)->value().toString(), "String");
      T_EQUALS(result->getItem(6)->value().toString(), "String");
   }
   catch (const QBasicException& exception)
   {
      T_SIGNAL_ERROR(exception.message());
   }

   try
   {
      exprTree = createVersatileExpressionTree("source.list #? this.varType() == \"Integer\" && this > 10 && this < 50");

      const auto result = exprTree->resolve(varTree.root(), basicFunctions);
      T_ASSERT(result);
      T_EQUALS(result->itemCount(), 3);
      T_EQUALS(result->getItem(0)->value().toInt(), 20);
      T_EQUALS(result->getItem(1)->value().toInt(), 30);
      T_EQUALS(result->getItem(2)->value().toInt(), 40);
   }
   catch (const QBasicException& exception)
   {
      T_SIGNAL_ERROR(exception.message());
   }

   try
   {
      exprTree = createVersatileExpressionTree("createSequence(0, 20)");

      const auto result = exprTree->resolve(new VarTree::Variable, basicFunctions);
      T_ASSERT(result);
      T_EQUALS(result->itemCount(), 20);
      T_EQUALS(result->getItem(0)->value().toInt(), 0);
      T_EQUALS(result->getItem(1)->value().toInt(), 1);
      T_EQUALS(result->getItem(19)->value().toInt(), 19);
   }
   catch (const QBasicException& exception)
   {
      T_SIGNAL_ERROR(exception.message());
   }

   try
   {
      exprTree = createVersatileExpressionTree("createSequence(0, 20, 5)");

      const auto result = exprTree->resolve(new VarTree::Variable, basicFunctions);
      T_ASSERT(result);
      T_EQUALS(result->itemCount(), 20);
      T_EQUALS(result->getItem(0)->value().toInt(), 0);
      T_EQUALS(result->getItem(1)->value().toInt(), 5);
      T_EQUALS(result->getItem(19)->value().toInt(), 95);
   }
   catch (const QBasicException& exception)
   {
      T_SIGNAL_ERROR(exception.message());
   }

   try
   {
      exprTree = createVersatileExpressionTree("createSequence(1, 10, {base[0] * 2})");

      const auto result = exprTree->resolve(new VarTree::Variable, basicFunctions);
      T_ASSERT(result);
      T_EQUALS(result->itemCount(), 10);
      T_EQUALS(result->getItem(0)->value().toInt(), 1);
      T_EQUALS(result->getItem(1)->value().toInt(), 2);
      T_EQUALS(result->getItem(9)->value().toInt(), 512);
   }
   catch (const QBasicException& exception)
   {
      T_SIGNAL_ERROR(exception.message());
   }

   try
   {
      exprTree = createVersatileExpressionTree("createSequence(1, 10, ({base[0] + base[1]}, 4))");

      const auto result = exprTree->resolve(new VarTree::Variable, basicFunctions);
      T_ASSERT(result);
      T_EQUALS(result->itemCount(), 10);
      T_EQUALS(result->getItem(0)->value().toInt(), 1);
      T_EQUALS(result->getItem(1)->value().toInt(), 5);
      T_EQUALS(result->getItem(9)->value().toInt(), 37);
   }
   catch (const QBasicException& exception)
   {
      T_SIGNAL_ERROR(exception.message());
   }

   try
   {
      exprTree = createVersatileExpressionTree("invalid?[0] ?? 42");

      const auto result = exprTree->resolve(new VarTree::Variable);
      T_ASSERT(result);
      T_EQUALS(result->value().toInt(), 42);
   }
   catch (const QBasicException& exception)
   {
      T_SIGNAL_ERROR(exception.message());
   }

   try
   {
      exprTree = createVersatileExpressionTree("test?.Member ?? 42, test.Member := 17, test?.Member ?? 32");

      const auto result = exprTree->resolve(new VarTree::Variable);
      T_ASSERT(result);
      T_EQUALS(result->itemCount(), 3);
      T_EQUALS(result->getItem(0)->value().toInt(), 42);
      T_EQUALS(result->getItem(1)->value().toInt(), 17);
      T_EQUALS(result->getItem(2)->value().toInt(), 17);
   }
   catch (const QBasicException& exception)
   {
      T_SIGNAL_ERROR(exception.message());
   }

   //
   // Test Variables with irregular characters
   //

   varTree.set("specialVars.Variable with spaces", "Test");
   varTree.set("specialVars.Hyphen-ated", "Test");
   varTree.set("specialVars.smith & wesson", "Test");
   varTree.set(QStringList() << "specialVars" << "   ", "Test", typeid(QString));
   varTree.set("specialVars.Important!", "Test");
   varTree.set(QStringList() << "specialVars" << "&<>- [[/{}!?''..  ", "Test", typeid(QString));
   varTree.set("specialVars.Linebreak\nis not valid", "Test");
   varTree.set("specialVars.Backtick` does'n work either", "Test");

   try
   {
      exprTree = createVersatileExpressionTree("specialVars.`Variable with spaces`");

      const auto result = exprTree->resolve(varTree.root());
      T_ASSERT(result);
      T_EQUALS(result->value().toString(), "Test");
   }
   catch (const QBasicException& exception)
   {
      T_SIGNAL_ERROR(exception.message());
   }

   try
   {
      exprTree = createVersatileExpressionTree("specialVars.`Hyphen-ated` + specialVars.`smith & wesson` + specialVars.`   ` + specialVars.`Important!` + specialVars.`&<>- [[/{}!?''..  `");

      const auto result = exprTree->resolve(varTree.root());
      T_ASSERT(result);
      T_EQUALS(result->value().toString(), "TestTestTestTestTest");
   }
   catch (const QBasicException& exception)
   {
      T_SIGNAL_ERROR(exception.message());
   }

   try
   {
      exprTree = createVersatileExpressionTree("specialVars.`Linebreak\nis not valid`");

      T_SIGNAL_ERROR(tr("Linebreak in explicit variable did not raise an exception"));
   }
   catch (const TokenException& tokenException)
   {
      T_EQUALS(tokenException.message(), ExpressionParser::tr("Two successive identifiers without operator"));
   }
   catch (const QBasicException& exception)
   {
      T_SIGNAL_ERROR(exception.message());
   }

   try
   {
      exprTree = createVersatileExpressionTree("specialVars.`Backtick`` does'n work either`");

      T_SIGNAL_ERROR(tr("Backticks in explicit variable did not raise an exception"));
   }
   catch (const TokenException& tokenException)
   {
      T_EQUALS(tokenException.message(), ExpressionParser::tr("Two successive identifiers without operator"));
   }
   catch (const QBasicException& exception)
   {
      T_SIGNAL_ERROR(exception.message());
   }
}

void CppExpressionTests::error_tests()
{
   try
   {
      auto exprTree = createVersatileExpressionTree("");
      T_SIGNAL_ERROR(tr("Expression did not fail"));
   }
   catch (const TokenException& exception)
   {
      T_SIGNAL_ERROR(exception.message());
   }
   catch (const ExpressionException& exception)
   {
      T_SIGNAL_ERROR(exception.message());
   }
   catch (const QBasicException& exception)
   {
      T_EQUALS(exception.message(), ExpressionParser::tr("Expression has no root (may be empty)"));
   }

   try
   {
      auto exprTree = createVersatileExpressionTree("()");
      T_SIGNAL_ERROR(tr("Expression did not fail"));
   }
   catch (const TokenException& exception)
   {
      T_SIGNAL_ERROR(exception.message());
   }
   catch (const ExpressionException& exception)
   {
      T_SIGNAL_ERROR(exception.message());
   }
   catch (const QBasicException& exception)
   {
      T_EQUALS(exception.message(), ExpressionParser::tr("Expression has no root (may be empty)"));
   }

   try
   {
      auto exprTree = createVersatileExpressionTree("foo())");
      T_SIGNAL_ERROR(tr("Expression did not fail"));
   }
   catch (const TokenException& exception)
   {
      const auto token = exception.token().as<CppOperatorToken>();
      T_ASSERT(token);
      T_EQUALS(QString(token->op()), QString(")"));
      T_EQUALS(token->startPos(), 5);
      T_EQUALS(token->endPos(), 6);
      T_EQUALS(exception.message(), ExpressionParser::tr("Invalid operator stack (too many closings)"));
   }
   catch (const ExpressionException& exception)
   {
      T_SIGNAL_ERROR(exception.message());
   }
   catch (const QBasicException& exception)
   {
      T_SIGNAL_ERROR(exception.message());
   }

   try
   {
      auto exprTree = createVersatileExpressionTree("foo(5+)");
      T_SIGNAL_ERROR(tr("Expression did not fail"));
   }
   catch (const TokenException& exception)
   {
      T_SIGNAL_ERROR(exception.message());
   }
   catch (const ExpressionException& exception)
   {
      const auto opExpr = exception.expr().as<AbstractOperatorExpression>();
      T_ASSERT(opExpr);
      const auto token = dynamic_cast<const CppTokenOperator&>(opExpr->op()).token().as<CppOperatorToken>();
      T_ASSERT(token);
      T_EQUALS(QString(token->op()), QString("+"));
      T_EQUALS(token->startPos(), 5);
      T_EQUALS(token->endPos(), 6);
      T_EQUALS(exception.message(), ExpressionParser::tr("Binary operator has no operand on the right side"));
   }
   catch (const QBasicException& exception)
   {
      T_SIGNAL_ERROR(exception.message());
   }

   try
   {
      auto exprTree = createVersatileExpressionTree("foo bar");
      T_SIGNAL_ERROR(tr("Expression did not fail"));
   }
   catch (const TokenException& exception)
   {
      const auto token = exception.token().as<CppIdentifierToken>();
      T_ASSERT(token);
      T_EQUALS(token->identifier(), QString("bar"));
      T_EQUALS(token->startPos(), 4);
      T_EQUALS(token->endPos(), 7);
      T_EQUALS(exception.message(), ExpressionParser::tr("Two successive identifiers without operator"));
   }
   catch (const ExpressionException& exception)
   {
      T_SIGNAL_ERROR(exception.message());
   }
   catch (const QBasicException& exception)
   {
      T_SIGNAL_ERROR(exception.message());
   }
}

static VarPtr testFunctionResolver1(const IdentifierResolver& ir)
{
   return ir.varTree()->getItem(0);
}

static VarPtr testFunctionResolver2(const IdentifierResolver& ir)
{
   return ir.varTree()->getItem(1);
}

void CppExpressionTests::function_call()
{
   VarTree varTree;

   varTree.set("functions.f1", ExpressionPtr(new FunctionPointerExpression(&testFunctionResolver2)));
   varTree.set("f1", ExpressionPtr(new FunctionPointerExpression(&testFunctionResolver1)));

   auto exprTree = createVersatileExpressionTree("3 + f1(39)");

   T_EQUALS(exprTree->resolve(varTree.root())->value().toInt(), 42);

   exprTree = createVersatileExpressionTree("3 + functions.f1(42, 39)");

   T_EQUALS(exprTree->resolve(varTree.root())->value().toInt(), 42);

   const auto functionMap = createCppResolverFunctions();

   exprTree = createVersatileExpressionTree(R"(testVar := "Testtext"; testVar.varType() == "String" ? testVar.length() : -1)");

   try
   {
      T_EQUALS(exprTree->resolve(varTree.root(), functionMap)->value().toInt(), 8);
   }
   catch (const QBasicException& exception)
   {
      T_SIGNAL_ERROR(exception.message());
   }

   varTree = VarTree();

   varTree.set("Var1", 5);
   varTree.set("Var2", 2);

   exprTree = createVersatileExpressionTree("f := { Var1 }; f()");

   try
   {
      auto unused = exprTree->resolve(varTree.root(), functionMap);
      T_SIGNAL_ERROR("Function can access variables outside scope");
   }
   catch (const QBasicException&)
   {
      // OK
   }

   exprTree = createVersatileExpressionTree("f := { Var1 }; f(base)");

   try
   {
      T_EQUALS(exprTree->resolve(varTree.root(), functionMap)->value().toInt(), 5);
   }
   catch (const QBasicException& exception)
   {
      T_SIGNAL_ERROR(exception.message());
   }
}

void CppExpressionTests::stringTemplate_tests()
{
   StringTemplate tmpl;
   VarTree varTree;

   varTree.set("Var1", 1);
   varTree.set("Var2", "Zwei");

   tmpl = "String template without placeholders.";
   T_EQUALS(tmpl.resolve(varTree.root()), "String template without placeholders.");

   tmpl = "String template wit a {Var1}.";
   T_EQUALS(tmpl.resolve(varTree.root()), "String template wit a 1.");

   tmpl = "String template with {Var1} two {Var1}{Var2}.";
   T_EQUALS(tmpl.resolve(varTree.root()), "String template with 1 two 1Zwei.");

   tmpl = "{Var1} template with {{braces}} and a variable at {Var2}";
   T_EQUALS(tmpl.resolve(varTree.root()), "1 template with {braces}} and a variable at Zwei");
}

BEGIN_TEST_SUITES
   TEST_SUITE(MiscTests);
   TEST_SUITE(ConversionTest);
   TEST_SUITE(VariantTests);
   TEST_SUITE(SettingTests);
   TEST_SUITE(SerStreamTests);
   TEST_SUITE(VarTreeTests);
   TEST_SUITE(ParserTests);
   TEST_SUITE(SqlExpressionTests);
   TEST_SUITE(CppExpressionTests);
END_TEST_SUITES
