<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE">
<context>
    <name>ConsoleTools::appArguments</name>
    <message>
        <location filename="ConsoleTools.cpp" line="35"/>
        <source>Missing name in option &apos;%1&apos;</source>
        <translation>Optionsname fehlt in &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="ConsoleTools.cpp" line="50"/>
        <location filename="ConsoleTools.cpp" line="77"/>
        <source>Option &apos;%1&apos; already specified</source>
        <translation>Die Option &apos;%1&apos; ist bereits angegeben</translation>
    </message>
    <message>
        <location filename="ConsoleTools.cpp" line="65"/>
        <location filename="ConsoleTools.cpp" line="87"/>
        <source>Unexpected value for option &apos;%1&apos;</source>
        <translation>Unerwarteter Wert für die Option &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="ConsoleTools.cpp" line="85"/>
        <source>Missing value for option &apos;%1&apos;</source>
        <translation>Fehlender Wert für die Option &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="ConsoleTools.cpp" line="96"/>
        <source>Option &apos;%1&apos; not expected</source>
        <translation>Unerwartete Option &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="ConsoleTools.cpp" line="119"/>
        <source>Unexpected parameter &apos;%1&apos;</source>
        <translation>Unerwarterer Parameter &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="ConsoleTools.cpp" line="139"/>
        <source>Missing one of the options &apos;%1&apos;</source>
        <translation>Es muss eine der Optionen &apos;%1&apos; angegeben werden</translation>
    </message>
    <message>
        <location filename="ConsoleTools.cpp" line="143"/>
        <source>Missing option &apos;%1&apos;</source>
        <translation>Option &apos;%1&apos; fehlt</translation>
    </message>
    <message>
        <location filename="ConsoleTools.cpp" line="147"/>
        <source>Missing argumnet</source>
        <translation>Fehlendes Argument</translation>
    </message>
</context>
<context>
    <name>CppExpressionTests</name>
    <message>
        <location filename="TestCases.cpp" line="1648"/>
        <source>Expression &quot;;&quot; must throw an exception</source>
        <translation>Der Ausdruck &quot;;&quot; muss eine Ausnahme auslösen</translation>
    </message>
    <message>
        <location filename="TestCases.cpp" line="1731"/>
        <source>Operator + with QString and int must throw an exception</source>
        <translation>Der Operator + mit QString und int muss eine Ausnahme auslösen</translation>
    </message>
    <message>
        <location filename="TestCases.cpp" line="2651"/>
        <source>Linebreak in explicit variable did not raise an exception</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="TestCases.cpp" line="2666"/>
        <source>Backticks in explicit variable did not raise an exception</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="TestCases.cpp" line="2683"/>
        <location filename="TestCases.cpp" line="2701"/>
        <location filename="TestCases.cpp" line="2719"/>
        <location filename="TestCases.cpp" line="2742"/>
        <location filename="TestCases.cpp" line="2767"/>
        <source>Expression did not fail</source>
        <translation>Ausdruck ergab keinen Fehler</translation>
    </message>
</context>
<context>
    <name>CppOperator</name>
    <message>
        <location filename="CPPExpression.cpp" line="775"/>
        <source>Invalid number of Operands for %1 operator: %2</source>
        <translation>Ungültige Anzahl Operanden für den %1 Operator: %2</translation>
    </message>
    <message>
        <location filename="CPPExpression.cpp" line="778"/>
        <source>Invalid identifier resolver</source>
        <translation>Ungültiger Variablenauflöser</translation>
    </message>
    <message>
        <location filename="CPPExpression.cpp" line="982"/>
        <source>Invalid : operator</source>
        <translation>Der : operator ist ungültig</translation>
    </message>
    <message>
        <location filename="CPPExpression.cpp" line="810"/>
        <source>The operand is not a function</source>
        <translation>Der Operand ist keine Funktion</translation>
    </message>
    <message>
        <location filename="CPPExpression.cpp" line="267"/>
        <location filename="TestCases.cpp" line="1735"/>
        <source>Binary operation %1 not allowed for the types %2 and %3</source>
        <translation>Die binäre Operation %1 ist mit den Typen %2 und %3 nicht erlaubt</translation>
    </message>
    <message>
        <location filename="CPPExpression.cpp" line="278"/>
        <source>Binary operation %1 for the type %2 not defined</source>
        <translation>Die binäre Operation %1 ist für den Typ %2 nicht definiert</translation>
    </message>
    <message>
        <location filename="CPPExpression.cpp" line="525"/>
        <source>Unary operation %1 for the type %2 not defined</source>
        <translation>Der unäre Operator %1 ist für den Typ %2 nicht definiert</translation>
    </message>
    <message>
        <location filename="CPPExpression.cpp" line="533"/>
        <source>Unary operation %1 not allowed for the type %2</source>
        <translation>Der unäre Operator %1 ist für den Typ %2 nicht erlaubt</translation>
    </message>
    <message>
        <location filename="CPPExpression.cpp" line="865"/>
        <source>Index %1 out of range</source>
        <translation>Der Index %1 ist ausserhalb des gültigen Bereichs</translation>
    </message>
    <message>
        <location filename="CPPExpression.cpp" line="975"/>
        <source>Operator on the right hand side of ? is not :</source>
        <translation>Auf der rechten Seite des ?-Operators fehlt der :-Operator</translation>
    </message>
    <message>
        <location filename="CPPExpression.cpp" line="1016"/>
        <source>Iteration limit of 65535 reached (endless loop?)</source>
        <translation>Mehr als 65535 Wiederholungen (Endlosschleife?)</translation>
    </message>
</context>
<context>
    <name>CppTokenValue</name>
    <message>
        <location filename="CPPExpression.cpp" line="1091"/>
        <source>Invalid identifier resolver</source>
        <translation>Ungültiger Variablenauflöser</translation>
    </message>
</context>
<context>
    <name>ExpressionParser</name>
    <message>
        <location filename="ExpressionEvaluator.cpp" line="33"/>
        <source>Binary operator has no operand on the left side</source>
        <translation>Binärer Operator hat keinen Operanden auf der linken Seite</translation>
    </message>
    <message>
        <location filename="ExpressionEvaluator.cpp" line="35"/>
        <location filename="TestCases.cpp" line="2757"/>
        <source>Binary operator has no operand on the right side</source>
        <translation>Binärer Operator hat keinen Operanden auf der rechten Seite</translation>
    </message>
    <message>
        <location filename="ExpressionEvaluator.cpp" line="78"/>
        <location filename="TestCases.cpp" line="1652"/>
        <source>Postfix operator has no operand on the left side</source>
        <translation>Postfix-Operator hat keinen Operanden auf der linken Seite</translation>
    </message>
    <message>
        <location filename="ExpressionEvaluator.cpp" line="80"/>
        <source>Postfix operator has an operand on the right side</source>
        <translation>Postfix-Operator hat einen Operanden auf der rechten Seite</translation>
    </message>
    <message>
        <location filename="ExpressionEvaluator.cpp" line="91"/>
        <source>Prefix operator has no operand on the right side</source>
        <translation>Prefix-Operator hat keinen Operanden auf der rechten Seite</translation>
    </message>
    <message>
        <location filename="ExpressionEvaluator.cpp" line="99"/>
        <source>Invalid operation</source>
        <translation>Ungültige Operation</translation>
    </message>
    <message>
        <location filename="ExpressionEvaluator.cpp" line="112"/>
        <location filename="ExpressionEvaluator.cpp" line="125"/>
        <location filename="ExpressionEvaluator.cpp" line="177"/>
        <source>Invalid operand stack (empty)</source>
        <translation>Ungültiger Operandenstapel (leer)</translation>
    </message>
    <message>
        <location filename="ExpressionEvaluator.cpp" line="121"/>
        <source>Invalid operator</source>
        <translation>Ungültiger Operator</translation>
    </message>
    <message>
        <location filename="ExpressionEvaluator.cpp" line="123"/>
        <source>Invalid operator stack (empty)</source>
        <translation>Ungültiger Operatorstapel (leer)</translation>
    </message>
    <message>
        <location filename="ExpressionEvaluator.cpp" line="160"/>
        <location filename="TestCases.cpp" line="2728"/>
        <source>Invalid operator stack (too many closings)</source>
        <translation>Ungültiger Operatorstapel (zu viele schließende Klammern)</translation>
    </message>
    <message>
        <location filename="ExpressionEvaluator.cpp" line="187"/>
        <source>Expression tree has invalid operator stack</source>
        <translation>Der Ausdrucksbaum hat einen ungültigen Operatorstapel</translation>
    </message>
    <message>
        <location filename="ExpressionEvaluator.cpp" line="189"/>
        <source>Unresolved expression tree operation</source>
        <translation>Nicht aufgelöste Operation im Ausdrucksbaum</translation>
    </message>
    <message>
        <location filename="ExpressionEvaluator.cpp" line="191"/>
        <source>Expression tree has invalid operand stack</source>
        <translation>Ausdrucksbaum hat einen ungültigen Operandenstapel</translation>
    </message>
    <message>
        <location filename="ExpressionEvaluator.cpp" line="193"/>
        <location filename="TestCases.cpp" line="2695"/>
        <location filename="TestCases.cpp" line="2713"/>
        <source>Expression has no root (may be empty)</source>
        <translation>Ausdruck hat keine Wurzel (ist möglicherweise leer)</translation>
    </message>
    <message>
        <location filename="ExpressionEvaluator.cpp" line="214"/>
        <source>Empty operator stack</source>
        <translation>Leerer Operatorstapel</translation>
    </message>
    <message>
        <location filename="CPPResolverFunctions.cpp" line="841"/>
        <location filename="CPPResolverFunctions.cpp" line="884"/>
        <location filename="CPPResolverFunctions.cpp" line="932"/>
        <source>Integer</source>
        <translation>Zahl</translation>
    </message>
    <message>
        <location filename="CPPResolverFunctions.cpp" line="842"/>
        <source>Returns the number of elements in the collection.</source>
        <translation>Gibt die Anzahl der Elemente in der Sammlung zurück.</translation>
    </message>
    <message>
        <location filename="CPPResolverFunctions.cpp" line="844"/>
        <location filename="CPPResolverFunctions.cpp" line="865"/>
        <location filename="CPPResolverFunctions.cpp" line="905"/>
        <location filename="CPPResolverFunctions.cpp" line="908"/>
        <location filename="CPPResolverFunctions.cpp" line="911"/>
        <location filename="CPPResolverFunctions.cpp" line="914"/>
        <location filename="CPPResolverFunctions.cpp" line="917"/>
        <location filename="CPPResolverFunctions.cpp" line="920"/>
        <source>String</source>
        <translation>Text</translation>
    </message>
    <message>
        <location filename="CPPResolverFunctions.cpp" line="845"/>
        <source>Returns the type name of the variable.</source>
        <translation>Gibt den Typnamen der Variablen zurück.</translation>
    </message>
    <message>
        <location filename="CPPResolverFunctions.cpp" line="850"/>
        <location filename="CPPResolverFunctions.cpp" line="853"/>
        <location filename="CPPResolverFunctions.cpp" line="859"/>
        <location filename="CPPResolverFunctions.cpp" line="862"/>
        <location filename="CPPResolverFunctions.cpp" line="944"/>
        <location filename="CPPResolverFunctions.cpp" line="947"/>
        <source>Variable</source>
        <translation>Variable</translation>
    </message>
    <message>
        <location filename="CPPResolverFunctions.cpp" line="874"/>
        <source>DateTime</source>
        <translation>DatumMitUhrzeit</translation>
    </message>
    <message>
        <location filename="CPPResolverFunctions.cpp" line="875"/>
        <source>Returns the current date and time.</source>
        <translation>Gibt das aktuelle Datum mit Uhrzeit zurück.</translation>
    </message>
    <message>
        <location filename="CPPResolverFunctions.cpp" line="885"/>
        <source>Returns the length of the variable.</source>
        <translation>Gibt die Länge der Variablen zurück.</translation>
    </message>
    <message>
        <location filename="CPPResolverFunctions.cpp" line="887"/>
        <source>List|String</source>
        <translation>Liste|String</translation>
    </message>
    <message>
        <location filename="CPPResolverFunctions.cpp" line="847"/>
        <location filename="CPPResolverFunctions.cpp" line="856"/>
        <source>Dictionary</source>
        <translation>Verzeichnis</translation>
    </message>
    <message>
        <location filename="CPPResolverFunctions.cpp" line="848"/>
        <source>Returns the dictionary at [Name]. A new dictionary is inserted if [Name] doesn&apos;t exist.</source>
        <translation>Gibt das Verzeichnis an [Name] zurück. Fügt ein leeres Verzeichnis ein wenn [Name] nicht vorhanden ist.</translation>
    </message>
    <message>
        <location filename="CPPResolverFunctions.cpp" line="851"/>
        <source>Adds [Name] with [Variable] to the dictionary and returns [Variable].</source>
        <translation>Fügt [Name] mit [Variable] in das Verzeichnis ein und gibt [Variable] zurück.</translation>
    </message>
    <message>
        <location filename="CPPResolverFunctions.cpp" line="854"/>
        <source>Adds [Variable] to the list and returns [Variable].</source>
        <translation>Fügt [Variable] zur Liste hinzu und gibt [Variable] zurück.</translation>
    </message>
    <message>
        <location filename="CPPResolverFunctions.cpp" line="857"/>
        <source>Sets the names for the variables in the list to [Names], thus converting the list into a dictionary!</source>
        <translation>Setzt die Namen für die Variablen in der Liste auf [Names] und wandelt damit die Liste in ein Verzeichnis um!</translation>
    </message>
    <message>
        <location filename="CPPResolverFunctions.cpp" line="878"/>
        <source>Creates a sequence of [Count] values beginning with [Start] modified by [Step].
If [Step] is omitted, the sequence values are incremented by the integer value 1.
If [Step] is a value, the sequence values are incremented by the integer value [Step].
If [Step] is a function, it is called with the current value and must return the next value.
If [Step] is a list the first element must be a function which is called to get the next value. The other list elements are passed as additional parameters.</source>
        <translation>Erzeugt eine Reihe von [Count] Elementen die mit [Start] beginnen und durch [Step] verändert werden.
Wird [Step] nicht angegeben, werden die Elemente der Reihe um den Zahlenwert 1 erhöht.
Ist [Step] ein Wert, werden die Elemente der Reihe um den Zahlenwert [Step] erhöht.
Ist [Step] eine Funktion, wird diese mit dem aktuellen Wert aufgerufen und muss den nächsten Wert zurückliefern.
Ist [Step] eine Liste muss das erste Element der Liste eine Funktion sein, die aufgerufen wird um den nächsten Wert zu erhalten. Die anderen Listenelemente werden als zusätzliche Parameter übergeben.</translation>
    </message>
    <message>
        <location filename="CPPResolverFunctions.cpp" line="888"/>
        <source>Returns a list containing up to [Length] elements beginning at [Start]. If [Length] is omitted the rest of the string is returned.</source>
        <translation>Gibt eine Liste mit bis zu [Length] Elementen beginnend ab [Start] zurück. Wenn [Length] nicht angegeben ist, wird der Rest zurückgegeben.</translation>
    </message>
    <message>
        <location filename="CPPResolverFunctions.cpp" line="890"/>
        <location filename="CPPResolverFunctions.cpp" line="893"/>
        <location filename="CPPResolverFunctions.cpp" line="896"/>
        <source>Boolean</source>
        <translation>Boolean</translation>
    </message>
    <message>
        <location filename="CPPResolverFunctions.cpp" line="891"/>
        <source>Returns true if the list or the string starts with [Compare]. By default the comparison is case sensitive.</source>
        <translation>Gibt Wahr zurück wenn die Liste oder der String mit [Compare] beginnt. Groß/Kleinschreibung wird standardmäßig berücksichtigt.</translation>
    </message>
    <message>
        <location filename="CPPResolverFunctions.cpp" line="894"/>
        <source>Returns true if the list or the string ends with [Compare]. By default the comparison is case sensitive.</source>
        <translation>Gibt Wahr zurück wenn die Liste oder der String mit [Compare] endet. Groß/Kleinschreibung wird standardmäßig berücksichtigt.</translation>
    </message>
    <message>
        <location filename="CPPResolverFunctions.cpp" line="897"/>
        <source>Returns true if the list or the string contains [Compare]. By default the comparison is case sensitive.</source>
        <translation>Gibt Wahr zurück wenn die Liste oder der String [Compare] enthält. Groß/Kleinschreibung wird standardmäßig berücksichtigt.</translation>
    </message>
    <message>
        <location filename="CPPResolverFunctions.cpp" line="877"/>
        <location filename="CPPResolverFunctions.cpp" line="899"/>
        <location filename="CPPResolverFunctions.cpp" line="902"/>
        <source>List</source>
        <translation>Liste</translation>
    </message>
    <message>
        <location filename="CPPResolverFunctions.cpp" line="900"/>
        <source>Converts a list into a list of unique elements. By default the comparison is case sensitive.</source>
        <translation>Wandelt eine Liste in eine Liste mit eindeutigen Elementen um. Groß/Kleinschreibung wird standardmäßig berücksichtigt.</translation>
    </message>
    <message>
        <location filename="CPPResolverFunctions.cpp" line="903"/>
        <source>Returns a list containing all elements from [List] where the [Predicate] returns true. The [Predicate] gets each [List] element as argument.</source>
        <translation>Gibt eine Liste mit allen Elementen aus [List] zurück, bei denen [Predicate] true ergibt. Das [Predicate] bekommt jedes [List]-Element als Argument.</translation>
    </message>
    <message>
        <location filename="CPPResolverFunctions.cpp" line="906"/>
        <source>Returns a string where all occurrences of [Before] are replaced with [After]. If [After] is omitted all occurrences of [Before] are removed. By default the comparison is case sensitive.</source>
        <translation>Gibt einen String zurück bei dem alle Vorkommen von [Before] durch [After] ersetzt wurden. Wenn [After] nicht angegeben ist werden alle Vorkommen von [Before] entfernt. Groß/Kleinschreibung wird standardmäßig berücksichtigt.</translation>
    </message>
    <message>
        <location filename="CPPResolverFunctions.cpp" line="909"/>
        <source>Returns a string where all letters are converted to upper case.</source>
        <translation>Gibt einen String in Großbuchstaben umgewandelt zurück.</translation>
    </message>
    <message>
        <location filename="CPPResolverFunctions.cpp" line="912"/>
        <source>Returns a string where all letters are converted to lower case.</source>
        <translation>Gibt einen String in Kleinbuchstaben umgewandelt zurück.</translation>
    </message>
    <message>
        <location filename="CPPResolverFunctions.cpp" line="915"/>
        <source>Returns a string where all white spaces are removed from the beginning and the end of the string.</source>
        <translation>Gibt einen String zurück bei dem alle Leerzeichen vom Anfang und vom Ende entfernt wurden.</translation>
    </message>
    <message>
        <location filename="CPPResolverFunctions.cpp" line="918"/>
        <source>Returns a string of [Width] length that contains the string padded by the [Fill] character. If the string is longer than [Width] and [Truncate] is false or omitted, the unmodified string is returned. Is [Truncate] true the string is truncated to [Width] length. By default [Fill] is a space.</source>
        <translation>Gibt einen String mit [Width] Länge zurück, das mit [Fill]-Zeichen aufgefüllt wurde. Ist der String länger als [Width] und [Truncate] trifft nicht zu oder ist nicht angegeben wird der unveränderte String zurückgegeben. Trifft [Truncate] zu, wird der String auf [Width]-Länge gekürzt. [Fill] ist standardmäßig ein Leerzeichen.</translation>
    </message>
    <message>
        <location filename="CPPResolverFunctions.cpp" line="921"/>
        <source>Returns a string of [Width] length that contains the [Fill] character followed by the string. If the string is longer than [Width] and [Truncate] is false or omitted, the unmodified string is returned. Is [Truncate] true the string is truncated to [Width] length. By default [Fill] is a space.</source>
        <translation>Gibt einen String mit [Width] Länge zurück, das [Fill]-Zeichen enthält die vom String gefolgt werden. Ist der String länger als [Width] und [Truncate] trifft nicht zu oder ist nicht angegeben wird der unveränderte String zurückgegeben. Trifft [Truncate] zu, wird der String auf [Width]-Länge gekürzt. [Fill] ist standardmäßig ein Leerzeichen.</translation>
    </message>
    <message>
        <location filename="CPPResolverFunctions.cpp" line="924"/>
        <source>Converts a string into a decimal using [Locale] or the default locale.</source>
        <translation>Wandelt einen String in eine Dezimalzahl unter Verwendung von [Locale] oder der Standard-Locale um.</translation>
    </message>
    <message>
        <location filename="CPPResolverFunctions.cpp" line="930"/>
        <source>Converts a string into a date using [Locale] or the default locale. If [UseLongFormat] is omitted the default format is used.</source>
        <translation>Wandelt einen String in ein Datum unter Verwendung von [Locale] oder der Standard-Locale um. Ist [UseLongFormat] nicht angegeben wird das Standardformat verwendet.</translation>
    </message>
    <message>
        <location filename="CPPResolverFunctions.cpp" line="933"/>
        <source>Converts a string into an integer using [Locale] or the default locale.</source>
        <translation>Wandelt einen String in eine Ganzzahl unter Verwendung von [Locale] oder der Standard-Locale um.</translation>
    </message>
    <message>
        <location filename="CPPResolverFunctions.cpp" line="923"/>
        <location filename="CPPResolverFunctions.cpp" line="926"/>
        <location filename="CPPResolverFunctions.cpp" line="929"/>
        <location filename="CPPResolverFunctions.cpp" line="935"/>
        <source>Decimal</source>
        <translation>Dezimalzahl</translation>
    </message>
    <message>
        <location filename="CPPResolverFunctions.cpp" line="866"/>
        <source>Converts a value into a string using [Locale] or the default locale.
For numeric values [Option] controls the insertion of group separators (true always adds group separators, false always omits them and without [Option] group separators are inserted if the number has fractional digits).
For date and time values [Option] specifies if a short format (false) or a long format (true) or the default (no [Option]) should be used.
For other values [Option] has no meaning.</source>
        <translation>Wandelt einen Wert unter Verwendung von [Locale] oder der Standard-Locale in einen String um.
Für numerische Werte kontrolliert [Option] das Einfügen von Tausendertrennzeichen (true fügt immer welche ein, false nie und ohne [Option] werden Tausendertrennzeichen nur eingefügt wenn der Wert Nachkommastellen hat).
Für Datums- und Zeitwerte bestimmt [Option] ob das kurze Format (false) oder das lange Format (true) oder der Standard (ohne [Option]) werwendet wird.
Für alle anderen Werte hat [Option] keine Bedeutung.</translation>
    </message>
    <message>
        <location filename="CPPResolverFunctions.cpp" line="927"/>
        <source>Synonym for toDecimal.</source>
        <translation>Synonym für toDecimal.</translation>
    </message>
    <message>
        <location filename="CPPResolverFunctions.cpp" line="936"/>
        <source>Returns a decimal rounded to the number of fractional [Digits]. By default [Digits] is 0.</source>
        <translation>Gibt eine Dezimalzahl zurück, die auf [Digits] Nachkommastellen gerundet wurde. Standardmäßig ist [Digits] 0.</translation>
    </message>
    <message>
        <location filename="CPPResolverFunctions.cpp" line="938"/>
        <location filename="CPPResolverFunctions.cpp" line="941"/>
        <source>Value</source>
        <translation>Wert</translation>
    </message>
    <message>
        <location filename="CPPResolverFunctions.cpp" line="939"/>
        <source>Returns the greater of the two values.</source>
        <translation>Gibt den größeren der beiden Werte zurück.</translation>
    </message>
    <message>
        <location filename="CPPResolverFunctions.cpp" line="942"/>
        <source>Returns the smaller of the two values.</source>
        <translation>Gibt den kleineren der beiden Werte zurück.</translation>
    </message>
    <message>
        <location filename="CPPResolverFunctions.cpp" line="945"/>
        <location filename="CPPResolverFunctions.cpp" line="948"/>
        <source>If [Condition] is true the [TruePart] is returned, otherwise the [FalsePart]. All parameters are evaluated before returning one of them! Use the ternary ?: operator to work with side effects.</source>
        <translation>Wenn [Condition] wahr ist wird [TruePart] zurückgegeben, ansonsten [FalsePart]. Alle Parameter werden ausgewertet bevor einer von ihnen zurückgegeben wird! Bei Ausdrücken mit Seiteneffekten den ternären ?: Operator verwenden.</translation>
    </message>
    <message>
        <location filename="CPPResolverFunctions.cpp" line="860"/>
        <source>Removes the variable at [Name] or [Index] and returns the removed variable.</source>
        <translation>Entfernt die Variable bei [Name] oder [Index] und gibt die entfernte Variable zurück.</translation>
    </message>
    <message>
        <location filename="CPPResolverFunctions.cpp" line="863"/>
        <source>Creates a new element of type [TypeName] and returns it.</source>
        <translation>Erzeugt ein neues Element vom Typ [TypeName] und gibt dieses zurück.</translation>
    </message>
    <message>
        <location filename="CPPResolverFunctions.cpp" line="871"/>
        <source>Date</source>
        <translation>Datum</translation>
    </message>
    <message>
        <location filename="CPPResolverFunctions.cpp" line="872"/>
        <source>Returns the current date.</source>
        <translation>Gibt das aktuelle Datum zurück.</translation>
    </message>
    <message>
        <location filename="CPPExpression.cpp" line="1201"/>
        <location filename="TestCases.cpp" line="2655"/>
        <location filename="TestCases.cpp" line="2670"/>
        <location filename="TestCases.cpp" line="2776"/>
        <source>Two successive identifiers without operator</source>
        <translation>Zwei aufeinanderfolgende Identifier ohne Operator</translation>
    </message>
</context>
<context>
    <name>FunctionStatusBase</name>
    <message>
        <location filename="FunctionStatusBase.cpp" line="132"/>
        <source>Debug</source>
        <translation>Debug</translation>
    </message>
    <message>
        <location filename="FunctionStatusBase.cpp" line="135"/>
        <source>Details</source>
        <translation>Details</translation>
    </message>
    <message>
        <location filename="FunctionStatusBase.cpp" line="138"/>
        <source>Information</source>
        <translation>Information</translation>
    </message>
    <message>
        <location filename="FunctionStatusBase.cpp" line="141"/>
        <source>Warning</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <location filename="FunctionStatusBase.cpp" line="144"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location filename="FunctionStatusBase.cpp" line="147"/>
        <source>Critical</source>
        <translation>Kritisch</translation>
    </message>
</context>
<context>
    <name>IdentifierResolver</name>
    <message>
        <location filename="ExpressionBase.cpp" line="82"/>
        <source>Identifier %1 could not be resolved</source>
        <translation>Der Name %1 konnte nicht aufgelöst werden</translation>
    </message>
</context>
<context>
    <name>QDbCommand</name>
    <message>
        <location filename="QDbCommand.cpp" line="203"/>
        <source>SQL statement is empty</source>
        <translation>SQL-Anweisung fehlt</translation>
    </message>
    <message>
        <location filename="QDbCommand.cpp" line="205"/>
        <location filename="QDbCommand.cpp" line="235"/>
        <location filename="QDbCommand.cpp" line="265"/>
        <source>No database connection</source>
        <translation>Keine gültige Datenbankverbindung</translation>
    </message>
    <message>
        <location filename="QDbCommand.cpp" line="210"/>
        <location filename="QDbCommand.cpp" line="240"/>
        <location filename="QDbCommand.cpp" line="270"/>
        <location filename="QDbCommand.cpp" line="484"/>
        <location filename="QDbCommand.cpp" line="497"/>
        <source>No database engine</source>
        <translation>Kein gültiger Datenbanktreiber</translation>
    </message>
    <message>
        <location filename="QDbCommand.cpp" line="233"/>
        <location filename="QDbCommand.cpp" line="263"/>
        <source>Procedure is invalid</source>
        <translation>Ungültige Funktion</translation>
    </message>
    <message>
        <location filename="QDbCommand.cpp" line="292"/>
        <source>Missing type for the placeholder &apos;%1&apos;</source>
        <translation>Kein Typ für Platzhalter &apos;%1&apos; angegeben</translation>
    </message>
    <message>
        <location filename="QDbCommand.cpp" line="301"/>
        <location filename="QDbCommand.cpp" line="334"/>
        <location filename="QDbCommand.cpp" line="403"/>
        <location filename="QDbCommand.cpp" line="436"/>
        <source>Placeholder &apos;%1&apos; not found</source>
        <translation>Platzhalter &apos;%1&apos; konnte nicht gefunden werden</translation>
    </message>
    <message>
        <location filename="QDbCommand.cpp" line="609"/>
        <source>No query</source>
        <translation>Keine Abfrage definiert</translation>
    </message>
    <message>
        <location filename="QDbCommand.cpp" line="314"/>
        <source>Missing type for the placeholder at position %1</source>
        <translation>Kein Typ für den %1. Platzhalter angegeben</translation>
    </message>
</context>
<context>
    <name>QDbCommandEngine</name>
    <message>
        <location filename="QDbCommandEngine.cpp" line="46"/>
        <source>Stored procedure &apos;%1&apos; not found</source>
        <translation>Die Datenbankprozedur &apos;%1&apos; konnte nicht gefunden werden</translation>
    </message>
    <message>
        <location filename="QDbCommandEngine.cpp" line="289"/>
        <source>&gt;Undefined&lt;</source>
        <translation>&gt;Undefiniert&lt;</translation>
    </message>
    <message>
        <location filename="QDbCommandEngine.cpp" line="291"/>
        <source>&gt;Invalid&lt;</source>
        <translation>&gt;Ungültig&lt;</translation>
    </message>
    <message>
        <location filename="QDbCommandEngine.cpp" line="296"/>
        <source>&gt;NULL&lt;</source>
        <translation>&gt;NULL&lt;</translation>
    </message>
    <message>
        <location filename="QDbCommandEngine.cpp" line="330"/>
        <source>Missing placeholder at position %1</source>
        <translation>Kein Platzhalter an Position %1</translation>
    </message>
    <message>
        <location filename="QDbCommandEngine.cpp" line="335"/>
        <source>Variable invalid for placeholder at position %1</source>
        <translation>Ungültige Variable an Platzhalter %1</translation>
    </message>
    <message>
        <location filename="QDbCommandEngine.cpp" line="340"/>
        <source>Unable to infer the database type form the variant type &apos;%1&apos; for the placeholder at position %2</source>
        <translation>Der Datenbank-Datentyp konnte nicht vom Variant-Datentyp &apos;%1&apos; abgeleitet werden (Platzhalter %2)</translation>
    </message>
    <message>
        <location filename="QDbCommandEngine.cpp" line="345"/>
        <source>Unable to infer the database type from the internal type (%1) for the placeholder at position %2</source>
        <translation>Der Datenbank-Datentyp konnte nicht vom internen Datentyp (%1) für den Platzhalter an Position %2 abgeleitet werden</translation>
    </message>
</context>
<context>
    <name>QDbState</name>
    <message>
        <location filename="QDbState.cpp" line="69"/>
        <source>Connection</source>
        <translation>Verbindung</translation>
    </message>
    <message>
        <location filename="QDbState.cpp" line="72"/>
        <source>Command</source>
        <translation>Befehl</translation>
    </message>
    <message>
        <location filename="QDbState.cpp" line="75"/>
        <source>Transaction</source>
        <translation>Transaktion</translation>
    </message>
</context>
<context>
    <name>TestSuite</name>
    <message>
        <location filename="TestManager.cpp" line="223"/>
        <location filename="TestManager.cpp" line="246"/>
        <location filename="TestManager.cpp" line="270"/>
        <source>Unknow exception!</source>
        <translation>Unbekannte Ausnahme!</translation>
    </message>
    <message>
        <location filename="TestManager.cpp" line="277"/>
        <source>Method not found.</source>
        <translation>Methode nicht gefunden.</translation>
    </message>
</context>
</TS>
