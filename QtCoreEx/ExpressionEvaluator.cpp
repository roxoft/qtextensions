#include "stdafx.h"
#include "ExpressionEvaluator.h"

static void evaluateLastOperation(QList<QPair<ExpressionPtr, bool>>& operandStack, QList<ExpressionPtr>& operatorStack)
{
   // Get the operation
   bp<AbstractOperatorExpression> opExpr = operatorStack.last();

   operatorStack.pop_back();

   const auto postOperand = operandStack.last(); // Operand following the operator

   operandStack.pop_back();
   // The last operand on the stack is now the operand on the left side of the operator

   // Get the operation type

   auto opType = opExpr->op().type();

   // There are special binary operators that do not require a right hand side (for example functions or ';')
   if (postOperand.first.isNull() && opExpr->op().canBePostfix())
   {
      opType = AbstractExpression::Operator::Type::Postfix;
   }

   // Add all operands to the operation

   if (opType == AbstractExpression::Operator::Type::Binary)
   {
      // Binary

      if (operandStack.last().first.isNull())
         throw ExpressionException(opExpr, ExpressionParser::tr("Binary operator has no operand on the left side"));
      if (postOperand.first.isNull())
         throw ExpressionException(opExpr, ExpressionParser::tr("Binary operator has no operand on the right side"));

      // Merge binary operators
      if (opExpr->op().isLeftToRight())
      {
         // Is the left operand an operation?
         auto leftOperation = operandStack.last().first.as<AbstractOperatorExpression>();

         if (leftOperation && (!operandStack.last().second || leftOperation->op().isAssociative()) && leftOperation->op() == opExpr->op())
         {
            opExpr->addOperands(leftOperation->operands());
         }
         else
         {
            opExpr->addOperand(operandStack.last().first);
         }

         opExpr->addOperand(postOperand.first);
      }
      else
      {
         opExpr->addOperand(operandStack.last().first);

         // Is the right operand an operation?
         auto rightOperation = postOperand.first.as<AbstractOperatorExpression>();

         if (rightOperation && (!postOperand.second || rightOperation->op().isAssociative()) && rightOperation->op() == opExpr->op())
         {
            opExpr->addOperands(rightOperation->operands());
         }
         else
         {
            opExpr->addOperand(postOperand.first);
         }
      }

      operandStack.last().first.reset();
   }
   else if (opType == AbstractExpression::Operator::Type::Postfix)
   {
      // Postfix

      if (operandStack.last().first.isNull())
         throw ExpressionException(opExpr, ExpressionParser::tr("Postfix operator has no operand on the left side"));
      if (!postOperand.first.isNull())
         throw ExpressionException(opExpr, ExpressionParser::tr("Postfix operator has an operand on the right side"));

      opExpr->addOperand(operandStack.last().first);

      operandStack.last().first.reset();
   }
   else
   {
      // Prefix

      if (postOperand.first.isNull())
         throw ExpressionException(opExpr, ExpressionParser::tr("Prefix operator has no operand on the right side"));

      opExpr->addOperand(postOperand.first);
   }

   // Add the operation as operand to the operands

   if (!operandStack.last().first.isNull())
      throw ExpressionException(opExpr, ExpressionParser::tr("Invalid operation"));
   operandStack.last() = QPair<ExpressionPtr, bool>(opExpr, false);
}

ExpressionParser::ExpressionParser()
{
   _operatorStackEnclosures.append(QList<ExpressionPtr>());
   _operandStack.append(QPair<ExpressionPtr, bool>());
}

void ExpressionParser::addValue(bp<AbstractValueExpression> expr)
{
   if (_operandStack.isEmpty())
      throw ExpressionException(expr, tr("Invalid operand stack (empty)"));

   Q_ASSERT(_operandStack.last().first.isNull());
   _operandStack.last() = QPair<ExpressionPtr, bool>(expr, false);
}

void ExpressionParser::addOperator(bp<AbstractOperatorExpression> expr)
{
   if (expr->op().type() == AbstractExpression::Operator::Type::Invalid)
      throw ExpressionException(expr, tr("Invalid operator"));
   if (_operatorStackEnclosures.isEmpty())
      throw ExpressionException(expr, tr("Invalid operator stack (empty)"));
   if (_operandStack.isEmpty())
      throw ExpressionException(expr, tr("Invalid operand stack (empty)"));

   /*
    Here the priority of the prefix operator is used even for binary or postfix operators!
    This is possible because evaluateOperators() changes nothing if the priority of the
    last operation (on the stack) is less than the priority of the current (prefix-) operator
    or, if they are equal, the last operation is not left-to-right.
    All operators with the same priority as a prefix operator are right-to-left (by prerequisite).
    If the last operation has a higher priority than our current (prefix-) operator it would have been
    evaluated too if the current operator is postfix or binary.
   */
   evaluateOperators(expr->op().priority());

   // Check if a prefix operator is actually a binary or postfix operator
   if (expr->op().type() == AbstractExpression::Operator::Type::Prefix && !_operandStack.last().first.isNull())
   {
      expr->op().fixTypeNotPrefix();

      // The priority has changed! We must evaluate again.

      evaluateOperators(expr->op().priority());
   }

   _operatorStackEnclosures.last().append(expr);
   _operandStack.append(QPair<ExpressionPtr, bool>());
}

void ExpressionParser::beginEnclosure()
{
   _operatorStackEnclosures.append(QList<ExpressionPtr>());
}

void ExpressionParser::endEnclosure()
{
   if (_operatorStackEnclosures.count() < 2)
      throw QBasicException(tr("Invalid operator stack (too many closings)"));

   while (!_operatorStackEnclosures.last().isEmpty())
   {
      evaluateLastOperation(_operandStack, _operatorStackEnclosures.last());
   }
   _operatorStackEnclosures.pop_back();

   // Mark the last operand as sealed
   Q_ASSERT(!_operandStack.isEmpty());
   if (!_operandStack.isEmpty())
      _operandStack.last().second = true;
}

ExpressionPtr ExpressionParser::lastRightSideOperand() const
{
   if (_operandStack.isEmpty())
      throw QBasicException(tr("Invalid operand stack (empty)"));

   return _operandStack.last().first;
}

ExpressionPtr ExpressionParser::expressionTree()
{
   evaluateOperators(-1); // Evaluate all

   if (_operatorStackEnclosures.count() != 1)
      throw QBasicException(tr("Expression tree has invalid operator stack")); // Should be impossible due to other ExpressionParser checks
   if (!_operatorStackEnclosures.last().isEmpty())
      throw ExpressionException(_operatorStackEnclosures.last().last(), tr("Unresolved expression tree operation"));
   if (_operandStack.count() != 1)
      throw QBasicException(tr("Expression tree has invalid operand stack")); // Should be impossible due to other ExpressionParser checks
   if (_operandStack.last().first.isNull())
      throw QBasicException(tr("Expression has no root (may be empty)"));

   return _operandStack.last().first;
}

void ExpressionParser::clear()
{
   _operatorStackEnclosures.clear();
   _operandStack.clear();

   _operatorStackEnclosures.append(QList<ExpressionPtr>());
   _operandStack.append(QPair<ExpressionPtr, bool>());
}

void ExpressionParser::evaluateOperators(int priority)
{
   // Evaluate previous operators if their priority is higher

   AbstractOperatorExpression* lastOperator = nullptr;

   if (_operatorStackEnclosures.isEmpty())
      throw QBasicException(tr("Empty operator stack"));

   if (!_operatorStackEnclosures.last().isEmpty())
      lastOperator = _operatorStackEnclosures.last().last().as<AbstractOperatorExpression>();

   // Separator ':' must have the same priority as the operator '?'.
   // Separators ',' and ';' must have the lowest possible priority (0)
   // With ':' in a ternary operator evaluate anything up to the conditional operator but do not add the separator
   while (lastOperator &&
      (lastOperator->op().priority() > priority ||
      (lastOperator->op().priority() == priority && lastOperator->op().isLeftToRight())))
   {
      lastOperator = nullptr;
      evaluateLastOperation(_operandStack, _operatorStackEnclosures.last());

      if (!_operatorStackEnclosures.last().isEmpty())
         lastOperator = _operatorStackEnclosures.last().last().as<AbstractOperatorExpression>();
   }
}
