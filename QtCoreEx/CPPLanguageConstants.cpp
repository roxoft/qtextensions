#include "stdafx.h"
#include "CPPLanguageConstants.h"
#include <cstring>

// Operators starting with the same characters must be grouped together ordered descending by operator length!
CppOpDef cppOperators[] = {
   {"##", 'B', CppAction::Invalid},
   {"#@", 'B', CppAction::Invalid},
   {"#?", 'B', CppAction::Filter},
   {"#", 'B', CppAction::Repeating},
   {"<<=", 'B', CppAction::Invalid},
   {"<<", 'B', CppAction::Invalid},
   {"<=", 'B', CppAction::LessOrEqual},
   {"<", 'B', CppAction::LessThan},
   {">>=", 'B', CppAction::Invalid},
   {">>", 'B', CppAction::Invalid},
   {">=", 'B', CppAction::GreaterOrEqual},
   {">", 'B', CppAction::GreaterThan},
   {"::", 'B', CppAction::Invalid},
   {":=", 'B', CppAction::Declaration},
   {":", 'B', CppAction::Colon},
   {"...", 'I', CppAction::Invalid},
   {"..", 'I', CppAction::Invalid},
   {".*", 'B', CppAction::Invalid},
   {".", 'B', CppAction::Member},
   {"->*", 'B', CppAction::Invalid},
   {"->", 'B', CppAction::Invalid},
   {"--", 'U', CppAction::PreDecr},
   {"-=", 'B', CppAction::Invalid},
   {"-", 'U', CppAction::Minus},
   {"++", 'U', CppAction::PreIncr},
   {"+=", 'B', CppAction::Invalid},
   {"+", 'U', CppAction::Plus},
   {"==", 'B', CppAction::Equality},
   {"=", 'B', CppAction::Assignment},
   {"!=", 'B', CppAction::Inequality},
   {"!", 'U', CppAction::LogNot},
   {"&&", 'B', CppAction::LogAnd},
   {"&=", 'B', CppAction::Invalid},
   {"&", 'B', CppAction::BitAnd},
   {"||", 'B', CppAction::LogOr},
   {"|=", 'B', CppAction::Invalid},
   {"|", 'B', CppAction::BitOr},
   {"*=", 'B', CppAction::Invalid},
   {"*", 'B', CppAction::Multiplication},
   {"//", 'U', CppAction::Invalid},
   {"/*", 'U', CppAction::Invalid},
   {"/=", 'B', CppAction::Invalid},
   {"/", 'B', CppAction::Division},
   {"%=", 'B', CppAction::Invalid},
   {"%", 'B', CppAction::Modulus},
   {"^=", 'B', CppAction::Invalid},
   {"^", 'B', CppAction::BitXor},
   {"[", 'B', CppAction::Subscript},
   {"]", 'U', CppAction::Invalid},
   {"(", 'B', CppAction::Function},
   {")", 'U', CppAction::Invalid},
   {"~", 'U', CppAction::BitNot},
   {"??", 'B', CppAction::NullCoalescing},
   {"?.", 'B', CppAction::NullConditionalMember},
   {"?[", 'B', CppAction::NullConditionalSubscript},
   {"?", 'B', CppAction::Conditional},
   {",", 'B', CppAction::Comma},
   {"{", 'U', CppAction::Block},
   {"}", 'U', CppAction::Invalid},
   {";", 'B', CppAction::SemiColon},
   { "", '\0', CppAction::Invalid} // End of list
};

const char* keywords[] = {
   "and",
   "and_eq",
   "alignas",
   "alignof",
   "asm",
   "auto",
   "bitand",
   "bitor",
   "bool",
   "break",
   "case",
   "catch",
   "char",
   "char16_t",
   "char32_t",
   "class",
   "compl",
   "const",
   "constexpr",
   "const_cast",
   "continue",
   "decltype",
   "default",
   "delete",
   "do",
   "double",
   "dynamic_cast",
   "else",
   "enum",
   "explicit",
   "export",
   "extern",
   "false",
   "final", // Identifier with special meaning
   "float",
   "for",
   "friend",
   "goto",
   "if",
   "inline",
   "int",
   "long",
   "mutable",
   "namespace",
   "new",
   "noexcept",
   "not",
   "not_eq",
   "nullptr",
   "operator",
   "or",
   "or_eq",
   "override", // Identifier with special meaning
   "private",
   "protected",
   "public",
   "register",
   "reinterpret_cast",
   "return",
   "short",
   "signed",
   "sizeof",
   "static",
   "static_assert",
   "static_cast",
   "struct",
   "switch",
   "template",
   "this",
   "thread_local",
   "throw",
   "true",
   "try",
   "typedef",
   "typeid",
   "typename",
   "union",
   "unsigned",
   "using",
   "virtual",
   "void",
   "volatile",
   "wchar_t",
   "while",
   "xor",
   "xor_eq"
};

const char* msKeywords[] = {
   "__abstract",
   "__alignof",
   "__asm",
   "__assume",
   "__based",
   "__box",
   "__cdecl",
   "__declspec",
   "__delegate",
   "__event",
   "__except",
   "__fastcall",
   "__finally",
   "__forceinline",
   "__gc",
   "__hook",
   "__identifier",
   "__if_exists",
   "__if_not_exists",
   "__inline",
   "__int16",
   "__int32",
   "__int64",
   "__int8",
   "__interface",
   "__leave",
   "__m128",
   "__m128d",
   "__m128i",
   "__m64",
   "__multiple_inheritance",
   "__nogc",
   "__noop",
   "__pin",
   "__pragma",
   "__property",
   "__ptr32",
   "__ptr64",
   "__raise",
   "__sealed",
   "__single_inheritance",
   "__stdcall",
   "__super",
   "__thiscall",
   "__try",
   "__try_cast",
   "__unaligned",
   "__unhook",
   "__uuidof",
   "__value",
   "__virtual_inheritance",
   "__w64",
   "__wchar_t",
   "abstract",
   "array",
   "delegate",
   "deprecated",
   "dllexport",
   "dllimport",
   "each",
   "event",
   "finally",
   "friend_as",
   "gcnew",
   "generic",
   "in",
   "initonly",
   "interface",
   "interior_ptr",
   "literal",
   "naked",
   "noinline",
   "noreturn",
   "nothrow",
   "novtable",
   "property",
   "ref",
   "safecast",
   "sealed",
   "selectany",
   "thread",
   "uuid",
   "value"
};

const char* qtKeywords[] = {
   "signals",
   "slots"
};

const char* directives[] = {
   "define",
   "elif",
   "else",
   "endif",
   "error",
   "if",
   "ifdef",
   "ifndef",
   "import",
   "include",
   "line",
   "pragma",
   "undef",
   "using"
};

const int declspecExtensions[] = {
   KW_deprecated,
   KW_dllexport,
   KW_dllimport,
   KW_naked,
   KW_noinline,
   KW_noreturn,
   KW_nothrow,
   KW_novtable,
   KW_property,
   KW_selectany,
   KW_thread,
   KW_uuid
};

const int managedExtensions[] = {
   KW___abstract,
   KW___box,
   KW___delegate,
   KW___gc,
   KW___nogc,
   KW___pin,
   KW___property,
   KW___sealed,
   KW___try_cast,
   KW___value,
   KW_array,
   KW_delegate,
   KW_each,
   KW_event,
   KW_finally,
   KW_friend_as,
   KW_gcnew,
   KW_generic,
   KW_in,
   KW_initonly,
   KW_interface,
   KW_interior_ptr,
   KW_literal,
   KW_property,
   KW_ref,
   KW_safecast,
   KW_value
};

const int intrinsicEventHandling[] = {
   KW___hook,
   KW___unhook
};

static const char** binaryFindArray(const char **begin, const char **end, const char *value)
{
   const char** notFound = end;

   while (begin != end)
   {
      const char**   test = begin + (end - begin) / 2;
      int            cmp = strcmp(value, *test);

      if (cmp < 0)
         end = test;
      else if (cmp > 0)
         begin = test + 1;
      else
         return test;
   }

   return notFound;
}

int findKeyword(const char *value)
{
   const char** end = keywords + (sizeof(keywords) / sizeof(*keywords));
   const char** keyword = binaryFindArray(keywords, end, value);

   if (keyword != end)
      return (int)(keyword - keywords);

   end = msKeywords + (sizeof(msKeywords) / sizeof(*msKeywords));
   keyword = binaryFindArray(msKeywords, end, value);
   if (keyword != end)
      return (int)(keyword - msKeywords) + 0x100;

   end = qtKeywords + (sizeof(qtKeywords) / sizeof(*qtKeywords));
   keyword = binaryFindArray(qtKeywords, end, value);
   if (keyword != end)
      return (int)(keyword - qtKeywords) + 0x200;

   return -1;
}

int findDirective(const char *value)
{
   const char** end = directives + (sizeof(directives) / sizeof(*directives));
   const char** directive = binaryFindArray(directives, end, value);

   if (directive == end)
      return -1;
   return (int)(directive - directives);
}
