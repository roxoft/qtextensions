#pragma once

#include "qtcoreex_global.h"
#include "Variant.h"
#include <QVector>
#include "GenericIterators.h"

/*
 * Generic class for iterating through children.
 */
template<typename N>
class HtChildrenContainer
{
public:

   typedef N & reference;
   typedef N const& const_reference;
   typedef N * pointer;
   typedef N const* const_pointer;

protected:

   class QTCOREEX_EXPORT IteratorImpl : public IteratorCore<N, HtChildrenContainer>
   {
   protected:
      IteratorImpl(const IteratorImpl &other) : IteratorCore<N, HtChildrenContainer>(other), _index(other._index), _child(nullptr) {}

   public:
      IteratorImpl(N container, int index = 0) : IteratorCore<N, HtChildrenContainer>(container), _index(index), _child(nullptr) {}
      ~IteratorImpl() override = default;

      IteratorCore<N, HtChildrenContainer> *clone() override { return new IteratorImpl(*this); }

      bool isEqualTo(const IteratorCore<N, HtChildrenContainer> &rhs) const override
      {
         return IteratorCore<N, HtChildrenContainer>::isEqualTo(rhs) && _index == dynamic_cast<const IteratorImpl &>(rhs)._index;
      }

      bool isLessThan(const IteratorCore<N, HtChildrenContainer>& rhs) const override
      {
         return IteratorCore<N, HtChildrenContainer>::isEqualTo(rhs) && _index < dynamic_cast<const IteratorImpl &>(rhs)._index;
      }

      bool isValid() const override { return _index >= 0 && _index < IteratorCore<N, HtChildrenContainer>::_container->childCount(); }

      void moveForward(int distance) override { _index += distance; }
      void moveBackward(int distance) override { _index -= distance; }

      int distanceFrom(const IteratorCore<N, HtChildrenContainer>& other) const override { return _index - dynamic_cast<const IteratorImpl &>(other)._index; }

      reference value() override { _child = IteratorCore<N, HtChildrenContainer>::_container->child(_index); return _child; }
      const_reference value() const override { _child = IteratorCore<N, HtChildrenContainer>::_container->child(_index); return _child; }
      pointer item() override { _child = IteratorCore<N, HtChildrenContainer>::_container->child(_index); return &_child; }
      const_pointer item() const override { _child = IteratorCore<N, HtChildrenContainer>::_container->child(_index); return &_child; }

   private:
      int _index;
      mutable N _child;
   };

public:

   typedef GenericRandomAccessIterator<N, HtChildrenContainer> iterator;
   typedef GenericRandomAccessConstIterator<N, HtChildrenContainer> const_iterator;

public:

   HtChildrenContainer(N node) : _container(node) {}
   HtChildrenContainer(const HtChildrenContainer& other) : _container(other._container) {}
   ~HtChildrenContainer() = default;

   HtChildrenContainer& operator=(const HtChildrenContainer& other) { _container = other._container; return *this; }

   iterator       begin() { return iterator(new IteratorImpl(_container)); }
   const_iterator begin() const { return const_iterator(new IteratorImpl(_container)); }
   iterator       end() { return iterator(new IteratorImpl(_container, _container->childCount())); }
   const_iterator end() const { return const_iterator(new IteratorImpl(_container, _container->childCount())); }

private:

   N _container;
};

class HtNode;

template class QTCOREEX_EXPORT HtChildrenContainer<HtNode*>;
template class QTCOREEX_EXPORT HtChildrenContainer<const HtNode*>;

/*
 * This class can be used to iterate through all cells in a tree of HtNodes.
 * At first the attributes of the current HtNode are iterated left to right.
 * Then the attributes of the child nodes are processed.
 * Then the next sibling is processed.
 */
class HtCellContainer
{
public:
   typedef Variant &reference;
   typedef Variant const_reference;
   typedef Variant *pointer;
   typedef const Variant *const_pointer;

protected:
   class QTCOREEX_EXPORT RecursiveCellIteratorImpl : public IteratorCore<const HtNode*, HtCellContainer>
   {
   protected:
      RecursiveCellIteratorImpl(const RecursiveCellIteratorImpl &other) : IteratorCore<const HtNode*, HtCellContainer>(other), _node(other._node), _attIndex(other._attIndex) {}

   public:
      RecursiveCellIteratorImpl(const HtNode* container);
      virtual ~RecursiveCellIteratorImpl() {}

      IteratorCore<const HtNode*, HtCellContainer> *clone() override { return new RecursiveCellIteratorImpl(*this); }

      bool isEqualTo(const IteratorCore<const HtNode*, HtCellContainer> &rhs) const override;
      bool isLessThan(const IteratorCore<const HtNode*, HtCellContainer>& rhs) const override;

      bool isValid() const override;

      void moveForward(int distance) override { while (distance--) moveNext(); }
      void moveBackward(int distance) override;

      int distanceFrom(const IteratorCore<const HtNode*, HtCellContainer>& other) const override;

      Variant &value() override { return _value; }
      Variant value() const override { return _value; }
      Variant *item() override { return &_value; }
      const Variant *item() const override { return &_value; }

   private:
      void moveNext();
      void updateValue();

   private:
      const HtNode*  _node;
      int            _attIndex;
      Variant        _value;
   };

public:
   typedef GenericForwardIterator<const HtNode*, HtCellContainer> rcell_iterator;
   typedef GenericForwardConstIterator<const HtNode*, HtCellContainer> rcell_const_iterator;

public:
   HtCellContainer(const HtNode* node) : _container(node) {}
   HtCellContainer(const HtCellContainer& other) : _container(other._container) {}
   ~HtCellContainer() {}

   HtCellContainer& operator=(const HtCellContainer& other) { _container = other._container; return *this; }

   rcell_iterator recursiveCellIterator() const { return rcell_iterator(new RecursiveCellIteratorImpl(_container)); }

private:
   const HtNode* _container;
};
