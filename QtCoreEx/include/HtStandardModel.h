#ifndef HTSTANDARDMODEL_H
#define HTSTANDARDMODEL_H

#include "qtcoreex_global.h"
#include "HtItemModel.h"

// Standard model for Qt views (QTreeView, QTableView etc.).
// Drag and drop is enabled by default!
class QTCOREEX_EXPORT HtStandardModel : public HtItemModel
{
   Q_OBJECT
   Q_DISABLE_COPY(HtStandardModel)

public:
   HtStandardModel(QObject* parent = 0) : HtItemModel(parent) {}
   HtStandardModel(const QString& name, QObject* parent = 0) : HtItemModel(parent), _name(name) {}
   virtual ~HtStandardModel();

   static Qt::ItemFlags defaultItemFlags() { return Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsDragEnabled | Qt::ItemIsDropEnabled; }

public:
   // HtModel implementation

   QString   name() const override { return _name; }
   void      setName(const QString& value) override { _name = value; }

   const HtNode   *rootNode() const override;
   HtNode         *rootNode() override;

   Variant defaultAttribute(int role) const override; // The dafault ItemFlagsRole is (Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsDragEnabled | Qt::ItemIsDropEnabled)

   void setRootNode(HtNode *root); // Takes ownership of the item (deletes a previously assigned item)

protected:
   QString  _name;
   HtNode   *_root = nullptr;
};

#endif // HTSTANDARDMODEL_H
