#ifndef QDBCONNECTIONDATA_H
#define QDBCONNECTIONDATA_H

#include "qtcoreex_global.h"
#include "QDbConnection.h"
#include <QSharedData>
#include "QDbEnvironment.h"
#include "QDbState.h"

class QDbCommandEngine;

class QTCOREEX_EXPORT QDbConnectionData : public QSharedData
{
   Q_DISABLE_COPY(QDbConnectionData)

public:
   QDbConnectionData(); // If there is no default database (QDbConnection::defaultConnection()) this will be installed as default database
   virtual ~QDbConnectionData();

   QDbState::ErrhPtr errorHandler() const { return _dbState.errorHandler(); }
   void              setErrorHandler(QDbState::ErrhPtr errorHandler) { _dbState.setErrorHandler(errorHandler); }

   QTextCodec  *serverCharacterEncoding() const { return _serverCharacterEncoding; }
   void        setServerCharacterEncoding(QTextCodec *encoding) { _serverCharacterEncoding = encoding; }

   QTextCodec  *serverNationalCharacterEncoding() const { return _serverNationalCharacterEncoding; }
   void        setServerNationalCharacterEncoding(QTextCodec *encoding) { _serverNationalCharacterEncoding = encoding; }

   bool treatEmptyAsNull() const { return _emptyIsNull; } // By default empty strings and byte arrays are **NOT** treated as NULL
   void setTreatEmptyAsNull(bool emptyIsNull) { _emptyIsNull = emptyIsNull; }

   int _maxLiteralStringLength = 0;
   QString _trueString;
   QString _falseString;
   int _maxRowsPerCall = 300;

   virtual bool isValid() const = 0;

   virtual QString connectionString() const = 0;
   virtual QString dataSourceName() const = 0;
   virtual QString driverName() const = 0;
   virtual QString dbmsName() const { return {}; }
   virtual QString dbmsVersion() const { return {}; }
   virtual SqlStyle sqlStyle() const = 0;
   virtual QDbConnection::PhType phType() const = 0;

   virtual QDbEnvironment *environment() { return nullptr; }

   virtual bool beginTransaction() = 0;
   virtual bool rollbackTransaction() = 0;
   virtual bool commitTransaction() = 0;

   virtual bool abortExecution() = 0; // Aborts execution of a statement on the server

   virtual bool tableExists(const QString &tableName) = 0;

   virtual bool usePlaceholder(QDbDataType dbType, const Variant& value) const;
   virtual QChar phIndicator() const = 0;

   virtual QString toBooleanLiteral(bool value) const;
   virtual QString toNumericLiteral(long long value) const;
   virtual QString toNumericLiteral(double value) const;
   virtual QString toNumericLiteral(const QDecimal& value) const;
   virtual QString toDateLiteral(const QDateEx &value) const;
   virtual QString toTimeLiteral(const QTimeEx &value) const;
   virtual QString toDateTimeLiteral(const QDateTimeEx &value) const;
   virtual QString toStringLiteral(QString value) const;
   virtual QString toBinaryLiteral(const QByteArray &value) const;

   virtual QDbSchema::Database  databaseSchema() = 0;
   virtual QDbSchema::Synonym   synonymSchema(const QString &name) = 0;
   virtual QDbSchema::Table     tableSchema(const QString &name) = 0;
   virtual QDbSchema::Function  functionSchema(const QString &name) = 0;
   virtual QDbSchema::Package   packageSchema(const QString &name) = 0;

   virtual bool schemaCachingEnabled() const { return false; }
   virtual void enableSchemaCaching() {}
   virtual void disableSchemaCaching() {}

   const QDbState& state() const { return _dbState; }

   bool isDebugMode() const { return _debugMode; }
   void setIsDebugMode(bool debugMode = true) { _debugMode = debugMode; }

   virtual QDbCommandEngine *newEngine() = 0;

   // The default connection is non binding! It will be removed when the connection is destroyed.
   static QDbConnectionData* _default;

protected:
   mutable QDbState _dbState;
   QTextCodec* _serverCharacterEncoding = nullptr;
   QTextCodec* _serverNationalCharacterEncoding = nullptr;
   bool _emptyIsNull = false;
   bool _debugMode = false;
   QDbSchema::Database _schemaCache;
};

#endif // QDBCONNECTIONDATA_H
