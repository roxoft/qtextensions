#pragma once

#include "qtcoreex_global.h"
#include <QTextStream>
#include <QVector>

class CharacterStreamReader
{
public:
   virtual ~CharacterStreamReader() = default;
   virtual QChar readChar() = 0;
   virtual int pos() = 0;
};

class QTCOREEX_EXPORT QTextStreamCharacterReader : public CharacterStreamReader
{
public:
   QTextStreamCharacterReader(QTextStream* is) : _is(is) {}
   ~QTextStreamCharacterReader() override = default;

   QChar readChar() override;
   int pos() override;

private:
   QTextStream* _is = nullptr;
   int _pos = 0;
};

class QTCOREEX_EXPORT QStringCharacterReader : public CharacterStreamReader
{
public:
   QStringCharacterReader(const QString& text) : _text(text) {}
   ~QStringCharacterReader() override = default;

   QChar readChar() override;
   int pos() override;

private:
   QString _text;
   int _pos = 0;
};

// The TextStreamReader class holds an additional buffer which is used to peek into the stream.
class QTCOREEX_EXPORT TextStreamReader
{
   Q_DISABLE_COPY(TextStreamReader)

public:
   TextStreamReader() = default;
   TextStreamReader(const QString& text);
   TextStreamReader(CharacterStreamReader* dataSource) : _dataSource(dataSource) {}
   TextStreamReader(QTextStream* is);
   virtual ~TextStreamReader();

   void setText(const QString& text);
   void setDataSource(CharacterStreamReader* dataSource);
   void setStream(QTextStream* is);

   bool atEnd();

   // Peeking

   virtual QChar peekChar(int index = 0);

   // Retrieving

   bool readChar(QChar& c);
   bool skipChar(int count = 1); // Only chars that have been peeked can be skipped!

   // Information about parsed text (not peeked)

   int charCount() const { return _charCount; }
   int lineCount() const { return _lineCount; } // The line number is lineCount() + 1

   // Parse helper

   int bufferPos() const { return _bufferPos; }

   QString processedChars(int bufferPos, int length = -1) const; // Retrieves the processed characters up to the current position

   CharacterStreamReader* dataSource() const { return _dataSource; }

   void removeProcessedChars(); // Removes all read characters.

   int pos() const; // Current position in the underlying data source

protected:
   CharacterStreamReader* _dataSource = nullptr;
   bool _deleteDataSource = false;
   QString _buffer;
   QVector<int> _positions;
   int _bufferPos = 0;
   int _charCount = 0;
   int _lineCount = 0;
};
