#ifndef QDBENVIRONMENT_H
#define QDBENVIRONMENT_H

#include "qtcoreex_global.h"
#include "QDbState.h"

class QDbEnvironment
{
public:
   QDbEnvironment() {}
   virtual ~QDbEnvironment() {}

   const QDbState &dbState() const { return _dbState; }

protected:
   QDbState _dbState;
};

#endif // QDBENVIRONMENT_H
