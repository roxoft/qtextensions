#ifndef NUMERIC_H
#define NUMERIC_H

#include "qtcoreex_global.h"
#include <string>

namespace std
{
   QTCOREEX_EXPORT string   toString(double number, int precision = -16, char negativeSign = '-', char decimalPoint = '.', char thousandsSeparator = '\0');
   QTCOREEX_EXPORT wstring  toWString(double number, int precision = -16, wchar_t negativeSign = u'-', wchar_t decimalPoint = u'.', wchar_t thousandsSeparator = u'\0');

   QTCOREEX_EXPORT string   toString(short number, int base = 10, char negativeSign = '-', char thousandsSeparator = '\0');
   QTCOREEX_EXPORT wstring  toWString(short number, int base = 10, wchar_t negativeSign = u'-', wchar_t thousandsSeparator = u'\0');
   QTCOREEX_EXPORT string   toString(unsigned short number, int base = 10, char negativeSign = '-', char thousandsSeparator = '\0');
   QTCOREEX_EXPORT wstring  toWString(unsigned short number, int base = 10, wchar_t negativeSign = u'-', wchar_t thousandsSeparator = u'\0');
   QTCOREEX_EXPORT string   toString(int number, int base = 10, char negativeSign = '-', char thousandsSeparator = '\0');
   QTCOREEX_EXPORT wstring  toWString(int number, int base = 10, wchar_t negativeSign = u'-', wchar_t thousandsSeparator = u'\0');
   QTCOREEX_EXPORT string   toString(unsigned int number, int base = 10, char negativeSign = '-', char thousandsSeparator = '\0');
   QTCOREEX_EXPORT wstring  toWString(unsigned int number, int base = 10, wchar_t negativeSign = u'-', wchar_t thousandsSeparator = u'\0');
   QTCOREEX_EXPORT string   toString(long number, int base = 10, char negativeSign = '-', char thousandsSeparator = '\0');
   QTCOREEX_EXPORT wstring  toWString(long number, int base = 10, wchar_t negativeSign = u'-', wchar_t thousandsSeparator = u'\0');
   QTCOREEX_EXPORT string   toString(unsigned long number, int base = 10, char negativeSign = '-', char thousandsSeparator = '\0');
   QTCOREEX_EXPORT wstring  toWString(unsigned long number, int base = 10, wchar_t negativeSign = u'-', wchar_t thousandsSeparator = u'\0');
   QTCOREEX_EXPORT string   toString(long long int number, int base = 10, char negativeSign = '-', char thousandsSeparator = '\0');
   QTCOREEX_EXPORT wstring  toWString(long long int number, int base = 10, wchar_t negativeSign = u'-', wchar_t thousandsSeparator = u'\0');
   QTCOREEX_EXPORT string   toString(unsigned long long int number, int base = 10, char negativeSign = '-', char thousandsSeparator = '\0');
   QTCOREEX_EXPORT wstring  toWString(unsigned long long int number, int base = 10, wchar_t negativeSign = u'-', wchar_t thousandsSeparator = u'\0');

   QTCOREEX_EXPORT double toTime(double time, int& days, int& hours, int& minutes, int& seconds); // Returns the fractions of a second
}

#endif // NUMERIC_H
