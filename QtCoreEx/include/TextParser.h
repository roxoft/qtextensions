#ifndef TEXTPARSER_H
#define TEXTPARSER_H

#include "qtcoreex_global.h"
#include "TextStreamReader.h"

/* This is a simple delimiter based parser for text files.
 *
 * The core features are:
 * - Read characters from a stream until the character matches a delimiting character from a given delimiter list.
 * - Read characters from a stream until the read characters match a delimiting string from a given delimiter list.
 * - Return the raw sequence of characters read from the stream (since last reset of raw buffer).
 *
 * Usage:
 * - Create a text parser instance. The stream is not deleted on destruction. Optionally set keepRaw to true to keep the parsed text in its original form.
 * - Use readToCharIn("CharacterSequence") to read all following characters in the file up to one of the specified characters.
 */
class QTCOREEX_EXPORT TextParser
{
   Q_DISABLE_COPY(TextParser)

public:
   TextParser(TextStreamReader* reader = nullptr, bool own = false);
   TextParser(const QString& text, bool keepRaw = false);
   TextParser(QTextStream* is, bool keepRaw = false);
   TextParser(CharacterStreamReader* dataSource, bool keepRaw = false);
   ~TextParser();

   void setText(const QString& text, bool keepRaw = false);
   void setStream(QTextStream* is, bool keepRaw = false);
   void setDataSource(CharacterStreamReader* dataSource, bool keepRaw = false);

   bool  isEndOfStream() { return _reader->atEnd(); } // Test for end of stream

   QChar    readChar(); // Return the next character in the stream (null at end of stream)
   QChar    peekChar(int index = 0); // Check the next character in the stream without retrieving it.
   QString  readToCharIn(const QString &delimiters) { return readToCharIn(delimiters, QString(), false); } // Returns all following characters (starting with nextChar()) up to but not including one of the delimiters
   QString  readToCharIn(const QString &delimiters, const QString &whitespaces) { return readToCharIn(delimiters, whitespaces, false); } // Same as readToCharIn but ignores leading and trailing whitespaces (all characters in delimiters and whitespaces must be mutually exclusive)
   QString  readToCharNotIn(const QString &delimiters) { return readToCharIn(delimiters, QString(), true); } // Returns all following charcters (starting with nextChar()) up to but not including a character not in delimiters
   QString  readToStringIn(const QList<QString> &delimiters); // Returns all following characters up to but not including one of the delimiters (case sensitive!)

   bool skipChar(int count);

   int pos() const { return _reader->pos(); } // Position in the stream
   int bufferPos() const { return _reader->bufferPos(); }
   int lineNumber() const { return _reader->lineCount(); } // Line number
   int charCount() const { return _reader->charCount(); } // Number of characters read from the stream so far (1 after construction if the stream was not empty)

   QString  processedChars(int bufferPos, int length = -1) const { return _reader->processedChars(bufferPos, length); }
   void     removeProcessedChars() { _reader->removeProcessedChars(); }

private:
   /* Read characters from the stream until a delimiting character is encountered and return the read characters as string.
   *
   * Options:
   * - Skip the characters provided in 'whitespaces' at the beginning and at the end of the read operation.
   *   These characters should not be part of the 'delimiters'.
   * - Instead of reading characters until a delimiting character is found, read until a character is encountered which is not included in the delimiter list.
   */
   QString  readToCharIn(const QString &delimiters, const QString &whitespaces, bool exclude);

private:
   TextStreamReader* _reader = nullptr;
   bool _own = false;
   bool _keepRaw = false;
};

#endif // TEXTPARSER_H
