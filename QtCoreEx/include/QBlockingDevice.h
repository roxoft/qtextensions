#ifndef QBLOCKINGDEVICE_H
#define QBLOCKINGDEVICE_H

#include "qtcoreex_global.h"
#include <QIODevice>

class QTCOREEX_EXPORT QBlockingDevice : public QIODevice
{
   Q_OBJECT

public:
   QBlockingDevice(QIODevice *asyncDevice = nullptr, QObject *parent = nullptr);
   virtual ~QBlockingDevice();

   QIODevice   *device() { return _asyncDevice; }
   void        setDevice(QIODevice *asyncDevice);

protected:
   virtual qint64 readData(char *data, qint64 maxSize) override;
   virtual qint64 writeData(const char *data, qint64 maxSize) override;
   virtual void   close() override;
   virtual bool   isSequential() const override { return true; }

private slots:
   void asyncDeviceClosing();
   void asyncDeviceDestroyed();

private:
   QIODevice   *_asyncDevice;
   int         _readTimeout;
   int         _writeTimeout;
};

#endif // QBLOCKINGDEVICE_H
