#ifndef QDATETIMEEX_H
#define QDATETIMEEX_H

#include "qtcoreex_global.h"
#include "QLocaleEx.h"
#include <QTimeZone>

class QTime;
class QDate;
class QDateTime;
class SerStream;

class QDateEx;
class QTimeEx;

/*
 * QDateTimeEx combines the ability of QDate, QTime and QDateTime.
 * 
 * This means that QDateTimeEx can contain a date only, a time only or both a date, a time and a time zone offset!
 * 
 * For each part of a date or time or time zone offset there is an undefined flag (null). An undefined part is assumed to have all possible values.
 * Getting a time zone from a QDateTimeEx with an undefined time zone offset always returns the local time zone.
 * 
 * With the undefined flags the precision of a date or time can be specified. If for example seconds and milliseconds are null the time precision is minutes.
 * 
 * Comparison
 * ----------
 * 
 * The equality (==) or inequality (!=) operators compare exactly. The two QDateTimeEx are binary identical or binary not identical. Different QDateTimeEx may still point to the same time.
 * 
 * The greaterThan (\>) or lesserThan (\<) operators compare logically! If a QDateTimeEx is *not* less than another QDateTimeEx *and not* greater than the other then they have the same time.
 * That does not imply that they are identical (==).
 */
class QTCOREEX_EXPORT QDateTimeEx
{
   friend class QDateEx;
   friend class QTimeEx;

public:
   friend QTCOREEX_EXPORT SerStream& operator<<(SerStream& stream, const QDateTimeEx& value);
   friend QTCOREEX_EXPORT SerStream& operator>>(SerStream& stream, QDateTimeEx& value);

public:
   QDateTimeEx();
   QDateTimeEx(const QDateTimeEx& other);
   QDateTimeEx(const QTime& time, DateTimePart precision = DateTimePart::Millisecond);
   QDateTimeEx(const QDate& date, DateTimePart precision = DateTimePart::Day);
   QDateTimeEx(const QDateTime& dateTime, DateTimePart precision = DateTimePart::Millisecond);
   QDateTimeEx(const QDate& date, const QTime& time, DateTimePart precision = DateTimePart::Millisecond, QTimeZone timeZone = QTimeZone());
   QDateTimeEx(int year, int month, int day, int hour = -1, int minute = -1, int second = -1, int msec = -1, QTimeZone timeZone = QTimeZone());
   QDateTimeEx(int year, int month, int day, int hour, int minute, int second, int msec, int timeZoneOffset, bool dst);
   ~QDateTimeEx();

   QDateTimeEx& operator=(const QDateTimeEx& rhs);

   bool isNull() const { return minPrecision() == DateTimePart::None; }
   bool isValid() const { return minPrecision() != DateTimePart::None; }

   bool operator==(const QDateTimeEx& rhs) const; // Returns true if the times are binary identical (unknown parts and time zones must match too)
   bool operator!=(const QDateTimeEx& rhs) const { return !operator==(rhs); }
   bool equalTo(const QDateTimeEx& rhs) const; // Same as (!(*this < rhs) && !(*this > rhs)) except if there are no common parts where false is returned.
   bool differentTo(const QDateTimeEx& rhs) const; // Same as (*this < rhs) || (*this > rhs)
   bool operator<(const QDateTimeEx& rhs) const;
   bool operator>(const QDateTimeEx& rhs) const { return rhs < *this; }
   bool operator<=(const QDateTimeEx& rhs) const { return !(rhs < *this); }
   bool operator>=(const QDateTimeEx& rhs) const { return !(*this < rhs); }

   QDateTimeEx addMonths(int months) const;
   QDateTimeEx addDays(int days) const;
   QDateTimeEx addHours(int hours) const; // Does not take DST transitions into account!
   QDateTimeEx addMinutes(int minutes) const; // Does not take DST transitions into account!
   QDateTimeEx addSeconds(int seconds) const; // Does not take DST transitions into account!

   operator QTime() const { return toQTime(); }
   operator QDate() const { return toQDate(); }
   operator QDateTime() const { return toQDateTime(); }

   static QDateTimeEx currentDateTime(DateTimePart precision = DateTimePart::Millisecond); // Only time precisions are valid
   static QDateTimeEx currentDateTimeUtc(DateTimePart precision = DateTimePart::Millisecond); // Only time precisions are valid
   static QDateEx currentDate(DateTimePart precision = DateTimePart::Day); // Only date precisions are valid
   static QTimeEx currentTime(DateTimePart precision = DateTimePart::Millisecond); // Only time precisions are valid

   bool hasTimeZone() const { return _tzoffset; }
   bool isUTC() const { return _tzoffset == 1ULL; }
   bool hasMilliseconds() const { return _msec; }
   bool hasSeconds() const { return _second; }
   bool hasMinutes() const { return _minute; }
   bool hasHours() const { return _hour; }
   bool hasDays() const { return _day; }
   bool hasMonths() const { return _month; }
   bool hasYears() const { return _year; }
   bool hasDecades() const { return _decade; }
   bool hasCenturies() const { return _century; }
   bool hasMillenniums() const { return _millennium; }

   DateTimePart minPrecision() const;
   DateTimePart maxPrecision() const;

   int year() const;
   void setYear(int year);
   int month() const { return _month; }
   void setMonth(int month) { _month = month; }
   int day() const { return _day; }
   void setDay(int day) { _day = day; }
   int hour() const { return (int)_hour - 1; }
   void setHour(int hour) { _hour = hour + 1; }
   int minute() const { return (int)_minute - 1; }
   void setMinute(int minute) { _minute = minute + 1; }
   int second() const { return (int)_second - 1; }
   void setSecond(int second) { _second = second + 1; }
   int millisecond() const { return (int)_msec - 1; }
   void setMillisecond(int millisecond) { _msec = millisecond + 1; }
   bool isDst() const { return _dst; }
   void setIsDst(bool isDst) { _dst = isDst ? 1ULL : 0ULL; }
   int timeZoneOffset() const { return _tzoffset ? (_tzsign ? -1 : 1) * ((int)_tzoffset - 1) : 0; }
   void setTimeZoneOffset(int minutes) { if (minutes < 0) { _tzsign = 1; _tzoffset = -minutes + 1; } else { _tzsign = 0; _tzoffset = minutes + 1; } }

   QDateTimeEx toUTC() const;
   QDateTimeEx toLocalTime(QTimeZone timeZone = QTimeZone()) const; // If the timeZone is invalid the system time zone is used

   QDateEx toDate() const;
   QTimeEx toTime() const;

   QString toString(const QLocaleEx& locale = defaultLocale()) const;
   QString toString(const QString& format, const QLocaleEx& locale = defaultLocale()) const;

   QString toISOString() const;
   static QDateTimeEx fromISOString(const QString& s);

   void setDate(const QDate& date, DateTimePart precision = DateTimePart::Day);
   void setDate(int year, int month, int day);
   void setTime(const QTime& time, DateTimePart precision = DateTimePart::Millisecond);
   void setTime(int hour, int minute, int second, int msec);
   void setDateTime(const QDateTime& dateTime, DateTimePart precision = DateTimePart::Millisecond);

   QDate toQDate() const;
   QTime toQTime() const;
   QDateTime toQDateTime() const;

protected:
   QDateTimeEx(unsigned long long dateTime) : _dateTime(dateTime) {}

private:
   void setTimeZone(const QDateTime& dateTime);

private:
   union
   {
      unsigned long long _dateTime;
      struct
      {
         unsigned long long _tzoffset : 10; // (0 - 14*60) + 1; offset + 1
         unsigned long long _tzsign : 1; // 0: +, 1: -; only valid if _tzoffset is valid
         unsigned long long _dst : 1; // 1 = DST in effect; only valid if _tzoffset is valid
         unsigned long long _msec : 10; // 1 - 1000; offset + 1
         unsigned long long _second : 6; // 1 - 60; offset + 1
         unsigned long long _minute : 6; // 1 - 60; offset + 1
         unsigned long long _hour : 5; // 1 - 24; offset + 1
         unsigned long long _day : 5; // 1 - 31
         unsigned long long _month : 4; // 1 - 12
         unsigned long long _year : 4; // 1 - 10; offset + 1
         unsigned long long _decade : 4; // 1 - 10; offset + 1
         unsigned long long _century : 4; // 1 - 10; offset +1
         unsigned long long _millennium : 4; // 1 - 15; offset: +4
      };
   };
};

class QTCOREEX_EXPORT QDateEx : public QDateTimeEx
{
public:
   friend inline SerStream& operator<<(SerStream& stream, const QDateEx& value) { return operator<<(stream, (const QDateTimeEx&) value); }
   friend inline SerStream& operator>>(SerStream& stream, QDateEx& value) { return operator>>(stream, (QDateTimeEx&) value); }

public:
   QDateEx() = default;
   QDateEx(const QDateEx& other) = default;
   QDateEx(const QDateTimeEx& other);
   QDateEx(const QDate& date, DateTimePart precision = DateTimePart::Day) : QDateTimeEx(date, precision) {}
   QDateEx(int year, int month, int day) : QDateTimeEx(year, month, day) {}
   ~QDateEx() = default;

   QDateEx& operator=(const QDateEx& rhs) = default;
   QDateEx& operator=(const QDateTimeEx& rhs);
   QDateEx& operator=(const QDate& rhs);
};

class QTCOREEX_EXPORT QTimeEx : public QDateTimeEx
{
public:
   friend inline SerStream& operator<<(SerStream& stream, const QTimeEx& value) { return operator<<(stream, (const QDateTimeEx&) value); }
   friend inline SerStream& operator>>(SerStream& stream, QTimeEx& value) { return operator>>(stream, (QDateTimeEx&) value); }

public:
   QTimeEx() = default;
   QTimeEx(const QTimeEx& other) = default;
   QTimeEx(const QDateTimeEx& other);
   QTimeEx(const QTime& time, DateTimePart precision = DateTimePart::Millisecond) : QDateTimeEx(time, precision) {}
   QTimeEx(int hour, int minute, int second = -1, int msec = -1) : QDateTimeEx(0, 0, 0, hour, minute, second, msec) {}
   ~QTimeEx() = default;

   QTimeEx& operator=(const QTimeEx& rhs) = default;
   QTimeEx& operator=(const QDateTimeEx& rhs);
   QTimeEx& operator=(const QTime& rhs);
};

#endif // QDATETIMEEX_H
