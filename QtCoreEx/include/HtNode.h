#pragma once

#include "qtcoreex_global.h"
#include "Variant.h"
#include <QVector>
#include <QMap>
#include "HtIterators.h"

/*
 * This class holds the sorting direction for one attribute and one role of an HtNode.
 */
class HtAttributeOrder
{
public:
   HtAttributeOrder(int index = 0, int role = Qt::DisplayRole, bool descending = false) : _index(index), _role(role), _descending(descending) {}
   HtAttributeOrder(const HtAttributeOrder&) = default;
   ~HtAttributeOrder() = default;

   HtAttributeOrder& operator=(const HtAttributeOrder&) = default;

   bool operator==(const HtAttributeOrder &rhs) const { return _index == rhs._index && _role == rhs._role && _descending == rhs._descending; }
   bool operator!=(const HtAttributeOrder &rhs) const { return !operator==(rhs); }

   int index() const { return _index; }
   void setIndex(int index) { _index = index; }

   int role() const { return _role; }
   void setRole(int role) { _role = role; }

   bool descending() const { return _descending; }
   void setDescending(bool descending = true) { _descending = descending; }
   void setAscending(bool ascending = true) { _descending = !ascending; }

private:
   int  _index;
   int  _role;
   bool _descending;
};

/*
 * Collection of sorting orders for attributes and roles.
 */
class QTCOREEX_EXPORT HtSortOrder : public QVector<HtAttributeOrder>
{
public:
   HtSortOrder() = default;
   ~HtSortOrder() = default;

   bool operator()(const HtNode *left, const HtNode *right) const;

   HtSortOrder &operator<<(const HtAttributeOrder &attOrder) { QVector<HtAttributeOrder>::operator<<(attOrder); return *this; }
};

class QTextStream;
class HtModel;

// The class HtNode is the tree node for the Hierarchical Table Model.
// Each node can have any number of attributes for any number of Qt roles.
// Usually the attributes are interpreted as columns.
// Each node can also have any number of child nodes which is used to represent trees.
// You can define default values for attributes that have not been set or are null.
// Default values can be defined for a complete node (all attributes of the node) or for all child attributes at a specific index.
// That a default value at an attribute index is not applied to the attribute in the same node has the reason,
// that a default value should always apply to more than one attribute and instead of defining a default attribute for a single attribute the attribute itself can be set.
class QTCOREEX_EXPORT HtNode
{
   Q_DISABLE_COPY(HtNode)

public:
   HtNode() = default;
   virtual ~HtNode() = default;

   //
   // Attribute handling
   //

   // Number of attributes in the node (usually one plus the highest index used in setAttribute() for any role).
   virtual int attributeCount() const = 0;

   // Returns the attribute at the given index with the given role. It never throws an exception.
   // Returns the effective default value if the attribute is not set or null.
   // If index is -1 the effective default value for this node is returned.
   // Any other negative index returns a null Variant.
   // A negative role always returns a null Variant.
   virtual Variant attribute(int index, int role = Qt::DisplayRole) const = 0;

   // Returns all attributes of the node for the given role.
   virtual QVector<Variant> attributes(int role = Qt::DisplayRole) const = 0;

   // Returns the effective default value for the attribute with the given *index* and *role*.
   // The returned value is either the default value of this node (defaultAttribute(-1, role))
   // or the effective default value of the parent (parent()->effectiveDefaultAttribute(index, role))
   // or the default value of the model (model()->defaultAttribute(role)).
   virtual Variant attributeDefault(int index, int role = Qt::DisplayRole) const;

   // Returns the attributes for all defined roles at *index*.
   // If *index* is negative the returned map is empty.
   virtual QMap<int, Variant> attributeRoleMap(int index) const = 0;

   // Sets the attribute at the given index with the given role to value.
   // Never throws an exception (attributes are created if they don't exist).
   // A sorting order is not preserved. Use sortIn() to reestablish a sorting order.
   // The role must not be negative.
   virtual void setAttribute(int index, const Variant &value, int role = Qt::EditRole) = 0;

   // Insert *count* attributes for all roles at *index*.
   // This means that the values from *index* are moved to the right by count indexes and
   // the attribute count is incremented by *count*.
   virtual void insertAttributes(int index, int count = 1) = 0;

   // Removes *count* attributes from *index* of all roles.
   // Decrements the attribute count by *count*.
   virtual void removeAttributes(int index, int count = 1) = 0;

   // Copies all attributes from all roles from the other node
   virtual void assignAttributesFrom(const HtNode* other) = 0;

   // Searches for a valid default value for the given *index* and *role*.
   // The search starts with the default attribute at index (defaultAttribute(index, role)).
   // If the value is null the attribute default value is returned (attributeDefault(index, role)).
   virtual Variant effectiveDefaultAttribute(int index, int role) const;

   // Returns the default attribute for a given *index* and *role*.
   // If *index* is >= 0, the default value of the attribute at *index* is returned.
   // If *index* is -1, the default value for all attributes of this node is returned.
   virtual Variant defaultAttribute(int index, int role) const = 0;

   // If index is >= 0, *value* is the default value for the given *index* and *role* **for the child nodes only**! dataChanged() is called once for all children but nodeChanged() is **not** called.
   // If index is -1, *value* is the default value for all attributes of this and all child nodes for the given role. Additionally to dataChanged() for the child nodes nodeChanged() is called for all attributes of this node.
   // The role must not be negative.
   virtual void setDefaultAttribute(int index, const Variant &value, int role = Qt::EditRole) = 0;

   // Convenience function to add attributes at the end of the attribute list.

   void appendAttribute(const Variant &value, int role = Qt::EditRole) { int index = attributeCount(); setAttribute(index, value, role); }

   // Convenient functions for flag handling

   int  flags(int index, int role) const { return attribute(index, role).toInt(); }
   void setFlags(int index, int flags, int role) { setAttribute(index, flags, role); }
   void addFlags(int index, int flags, int role) { setAttribute(index, this->flags(index, role) | flags, role); }
   void removeFlags(int index, int flags, int role) { setAttribute(index, this->flags(index, role) & ~flags, role); }

   int  defaultFlags(int index, int role) const { return effectiveDefaultAttribute(index, role).toInt(); }
   void setDefaultFlags(int index, int flags, int role) { setDefaultAttribute(index, flags, role); }
   void addDefaultFlags(int index, int flags, int role) { setDefaultAttribute(index, this->defaultFlags(index, role) | flags, role); }
   void removeDefaultFlags(int index, int flags, int role) { setDefaultAttribute(index, this->defaultFlags(index, role) & ~flags, role); }

   Qt::Alignment alignment(int index) const { return (Qt::Alignment)attribute(index, Qt::TextAlignmentRole).toInt(); }
   void setAlignment(int index, Qt::Alignment alignment) { setAttribute(index, (int)alignment, Qt::TextAlignmentRole); }

   //
   // Children handling
   //

   // Child information members

   virtual HtSortOrder  sortOrder() const { return HtSortOrder() << HtAttributeOrder(); } // Sort the display role of colum 0 ascending
   virtual bool         hasChildren() const { return childCount() > 0; }
   virtual int          childCount() const = 0; // Number of child nodes
   virtual const HtNode *child(int index) const = 0; // Never throws an exception or returns NULL (a default constructed item is returned if index is out of bounds)
   virtual int          indexOf(const HtNode* item) const = 0;

   const HtNode* operator[](int index) const { return child(index); }

   // Child modification members
   // Do not forget to adjust the childs parent!

   virtual HtNode*   child(int index) = 0; // Never throws an exception or returns NULL (the child list is extended to make index valid)
   virtual HtNode*   insertChild(int index, HtNode* child = nullptr) = 0; // If *child* is null a newly created child is inserted and returned. If child has a parent its content is copied to a newly created child.
   virtual HtNode*   insertChild(int index, const HtNode* child, bool link = false) = 0; // A newly created child is inserted and returned. If *child* is not null its contents is copied (or linked) to the new child.
   virtual HtNode*   appendChild(HtNode* child = nullptr) { if (child == nullptr) return this->child(childCount()); return insertChild(childCount(), child); }
   virtual void      removeChild(int index) = 0;
   virtual HtNode*   takeChild(int index) = 0; // Removes the child at index and returns it instead of deleting it (it has to be deleted explicitely!)
   virtual void      removeAllChildNodes() { int count = childCount(); while (count--) removeChild(count); }
   virtual bool      move(int from, int to) = 0; // Same as insertChild(to, takeChild(from));

   HtNode* operator[](int index) { return child(index); }

   // Iterators

   HtChildrenContainer<HtNode*> children();
   HtChildrenContainer<const HtNode*> children() const;
   HtCellContainer cells() const;

   // Collections
   // Use with care as these functions may be expensive

   virtual QVector<HtNode*> childList();
   virtual QVector<const HtNode*> childList() const;
   virtual void setChildList(const QVector<HtNode*>& childList);

   // Parents child list modifying members

   virtual bool moveTo(int index); // index specifies the insert position before the item is removed (if this index < index then index = index - 1)
   virtual void remove();

   // Sequential search for a child

   // Returns the index of the child that matches the value at attIndex or -1. The search is never recursive (Qt::MatchRecursive is **ignored**)!
   int findIndex(int attIndex, const Variant &value, int start = 0, Qt::MatchFlags flags = Qt::MatchFlags(Qt::MatchExactly), int role = Qt::DisplayRole) const;

   // Returns the node found by findIndex() or a nullptr. Qt::MatchRecursive is **supported**!
   HtNode* find(int attIndex, const Variant &value, int start = 0, Qt::MatchFlags flags = Qt::MatchFlags(Qt::MatchExactly), int role = Qt::DisplayRole);
   const HtNode* find(int attIndex, const Variant &value, int start = 0, Qt::MatchFlags flags = Qt::MatchFlags(Qt::MatchExactly), int role = Qt::DisplayRole) const { return const_cast<HtNode*>(this)->find(attIndex, value, start, flags, role); }

#if 0
   // Get node related to the current node (these functions have poor performance)
   const HtNode*  nextItem(bool includeChildren = true, bool includeParents = true) const; // Returns the next item in the hierarchy
   const HtNode*  findForward(int attIndex, const Variant& value, Qt::MatchFlags flags = Qt::MatchFlags(Qt::MatchWrap), int role = Qt::DisplayRole) const; // Returns the next node matching the value at attIndex (column based search); includes children and parents if flags contain Qt::MatchRecursive
   const HtNode*  findForward(int* attIndex, const Variant& value, Qt::MatchFlags flags = Qt::MatchFlags(Qt::MatchWrap), int role = Qt::DisplayRole) const; // Returns the next node with attIndex matching the value (cell based search); includes children and parents if flags contain Qt::MatchRecursive
#endif

   // Sorting

   virtual void sort() { sort(sortOrder()); }
   virtual void sort(const HtSortOrder& sortOrder) = 0;
   virtual void sort(int attIndex, int role = Qt::DisplayRole, bool descending = false);

   // For sorted child lists only!

   virtual bool sortIn() { if (parent()) return moveTo(parent()->upperBound(sortOrder(), this)); return false; }
   virtual bool sortIn(const HtSortOrder &sortOrder) { if (parent()) return moveTo(parent()->upperBound(sortOrder, this)); return false; }

   virtual HtNode *insertChild(const HtSortOrder &sortOrder, HtNode *child) { return insertChild(upperBound(sortOrder, child), child); }
   virtual HtNode *insertChild(HtNode *child) { return insertChild(upperBound(sortOrder(), child), child); }
   virtual HtNode *binaryFindOrInsert(int attIndex, const Variant &value, int role = Qt::DisplayRole);

   int upperBound(int attIndex, const Variant &value, int role = Qt::DisplayRole, int lowerBound = -1) const; // The children have to be sorted ascending by the given attIndex and the role
   int upperBound(const HtSortOrder &sortOrder, HtNode *item) const; // If the item is in the list, it is ignored; if sortOrder is empty, childCount() is returned
   int upperBound(HtNode *item) const { return upperBound(sortOrder(), item); }

   int lowerBound(int attIndex, const Variant &value, int role = Qt::DisplayRole, int upperBound = -1) const;
   int lowerBound(const HtSortOrder &sortOrder, HtNode *item) const;
   int lowerBound(HtNode *item) const { return lowerBound(sortOrder(), item); }

   Q_DECL_DEPRECATED
   int firstIndexAfter(int attIndex, const Variant &value, int role = Qt::DisplayRole) const { return upperBound(attIndex, value, role); }
   Q_DECL_DEPRECATED
   int firstIndexAfter(const HtSortOrder &sortOrder, HtNode *item) const { return upperBound(sortOrder, item); }
   Q_DECL_DEPRECATED
   int firstIndexAfter(HtNode *item) const { return upperBound(sortOrder(), item); }

   // Returns the parent of the node (the root node returns NULL)

   virtual HtNode* parent() const = 0;
   virtual void setParent(HtNode* parent) = 0; // Call setParent() only if you added the node to the parents child list!

   // Access to an optional model

   virtual HtModel* model() const = 0; // Returns a pointer to the model containing the tree
   virtual void setModel(HtModel* model) = 0; // Sets recursively the model for self and all children

   // Saves/loads the table as csv stream (not suitable for trees)

   virtual void save(QTextStream& os, const QLocaleEx& locale = defaultLocale(), int fieldCount = -1) const; // saves the attributes separated by semicolons recursively to the stream
   virtual void load(QTextStream& is, const QLocaleEx& locale = defaultLocale(), bool header = false);

   //
   // Alternate table syntax assuming the current node is a table and its attributes are the column headers (row headers would be the first attribute of each row)
   //

   int columnCount() const { return attributeCount(); }
   int rowCount() const { return childCount(); }

   Variant  headerData(int section, int role = Qt::DisplayRole) const { return attribute(section, role); }
   void     setHeaderData(int section, const Variant& value, int role = Qt::EditRole) { setAttribute(section, value, role); }

   Variant  col(int index, int role = Qt::DisplayRole) const { return attribute(index, role); }
   void     setCol(int index, const Variant &value, int role = Qt::EditRole) { setAttribute(index, value, role); }

   Variant  data(int row, int column, int role = Qt::DisplayRole) const { return child(row)->attribute(column, role); }
   void     setData(int row, int column, const Variant& value, int role = Qt::EditRole) { child(row)->setAttribute(column, value, role); }

   const HtNode   *row(int index) const { return child(index); }
   HtNode         *row(int index) { return child(index); }

   HtNode*  appendRow(HtNode* row = nullptr) { return appendChild(row); }
   HtNode*  insertRow(int index, HtNode* row) { return insertChild(index, row); }
   void     removeRow(int index) { removeChild(index); }
};
