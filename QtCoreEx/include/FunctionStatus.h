#ifndef FUNCTIONSTATUS_H
#define FUNCTIONSTATUS_H

#include "qtcoreex_global.h"
#include "FunctionStatusBase.h"
#include "SmartPointer.h"

/*
 * This class is useful to inform other (outside) authorities about states occuring in program execution (function calls).
 * 
 * The classic way to inform about unusual program states is to return or to throw satus codes or messages.
 * Usually the program or procedure is terminated after issuing the message.
 * In GUI programming message boxes are used to inform the user.
 * This is problematic when the states have to be reported in underlying models.
 * 
 * FunctionStatus can collect all those information and send or display them to an authority.
 * 
 * FunctionStatus itself acts as a smart pointer holding a specific implementation for status handling.
 * Therefore it is possible to pass a FunctionStatus object by value and still act on the same handler.
 * 
 * FunctionStatus is constructed with a default handler implementation which just collects all states and messages.
 * 
 * If you want to act immediately on status messages in your procedure derive your own handler from FunctionStatusBase and supply it in the constructor.
 * Your handler could keep a GUI frontend informed or/and feed a progress bar.
 * 
 * FunctionStatus is desigend to be used on function level and not on class level.
 * The use case is to have a FunctionStatus object during a specific task and to discard it after processing the containing messages.
 * Therefore it is better to supply the FunctionStatus as function parameter instead of a member variable.
 */
class QTCOREEX_EXPORT FunctionStatus
{
public:
   explicit FunctionStatus(const QString& name = QString()); // Constructs a FunctionStatusBase
   FunctionStatus(FunctionStatusBase* status, const QString& name = QString()); // nullptr is invalid!
   FunctionStatus(const bp<FunctionStatusBase>& statusBase, const QStringList& context);
   FunctionStatus(const FunctionStatus& other);

   ~FunctionStatus();

   FunctionStatus& operator=(const FunctionStatus& other);

   bool operator==(const FunctionStatus& rhs) const;

   const QStringList& context() const { return _context; }

   int progress() const { return _statusImpl->progress(); }
   void setProgress(int progress) { _statusImpl->setProgress(progress); }

   void addMessage(QString message, StatusMessage::Severity severity);

   void addInformation(QString message) { addMessage(message, StatusMessage::S_Information); }

   void addWarning(QString message) { addMessage(message, StatusMessage::S_Warning); }

   void addError(QString message) { addMessage(message, StatusMessage::S_Error); }

   FunctionStatus subStatus(const QString& name) const;

   void transferMessagesFrom(FunctionStatus status); // The messages are added in this context!

   void reset() { _statusImpl->reset(); }

   void clear() { reset(); }

   StatusMessage::Severity severity() const { return _statusImpl->severity(); } // Maximum severity of all messages

   bool hasMessages(StatusMessage::Severities severities = StatusMessage::S_All) const { return _statusImpl->hasMessages(severities); }

   bool hasInformation() const { return hasMessages(StatusMessage::S_Information); }

   bool hasWarning() const { return hasMessages(StatusMessage::S_Warning); }

   bool hasError() const { return hasMessages(StatusMessage::S_Error | StatusMessage::S_Critical); }

   int totalMessageCount(StatusMessage::Severities severities = StatusMessage::S_All) const { return _statusImpl->totalMessageCount(severities); }

   int informationCount() const { return totalMessageCount(StatusMessage::S_Information); }

   int warningCount() const { return totalMessageCount(StatusMessage::S_Warning); }

   int errorCount() const { return totalMessageCount(StatusMessage::S_Error | StatusMessage::S_Critical); }

   QString completeMessage(StatusMessage::Severities severities = StatusMessage::S_All) const { return _statusImpl->completeMessage(severities); }

   QStringList informationMessages() const { return _statusImpl->messages(StatusMessage::S_Information); }
   QStringList warningMessages() const { return _statusImpl->messages(StatusMessage::S_Warning); }
   QStringList errorMessages() const { return _statusImpl->messages(StatusMessage::S_Error | StatusMessage::S_Critical); }
   QList<StatusMessage> messages() const { return _statusImpl->messages(); }

   FunctionStatusBase* data() { return _statusImpl.data(); }

private:
   QStringList _context;
   bp<FunctionStatusBase> _statusImpl;
};

#endif // FUNCTIONSTATUS_H
