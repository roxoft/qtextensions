#pragma once

template<typename T>
class nullable
{
public:
   nullable() : _hasValue(false) {}
   nullable(const T& value) : _value(value), _hasValue(true) {}
   ~nullable() {}

   nullable<T>& operator=(const nullable<T>& other)
   {
      _value = other._value;
      _hasValue = other._hasValue;
      return *this;
   }

   bool operator==(const nullable<T>& other) const
   {
      if (_hasValue && other._hasValue)
         return _value == other._value;
      return _hasValue == other._hasValue;
   }

   bool operator<(const nullable<T>& other) const
   {
      if (_hasValue && other._hasValue)
         return _value < other._value;
      return other._hasValue;
   }

   bool isNull() const { return !_hasValue; }
   bool hasValue() const { return _hasValue; }
   const T& value() const { return _value; }

private:
   T     _value;
   bool  _hasValue;
};
