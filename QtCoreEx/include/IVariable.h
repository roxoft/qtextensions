#pragma once

#include "qtcoreex_global.h"
#include <QSharedData>
#include "Variant.h"
#include "SmartPointer.h"

QTCOREEX_EXPORT QByteArray toPercentEncoding(const QString& text);
QTCOREEX_EXPORT QString fromPercentEncoding(const QByteArray& data);

class IVariable;
class AbstractExpression;
class Tokenizer;

typedef bp<IVariable> VarPtr;
typedef bp<AbstractExpression> ExpressionPtr;

// An IVariable can either be a value, a function or a collection of name/IVariable pairs.
class QTCOREEX_EXPORT IVariable : public QSharedData
{
   Q_DISABLE_COPY(IVariable)

public:
   enum class BasicType { Collection, Value, Function };

   static QString getBasicTypeName(BasicType basicType);

public:
   IVariable() = default;
   virtual ~IVariable() = default;

   virtual VarPtr newCollection(const QString& typeName = QString()) const = 0;
   virtual VarPtr newFromValue(const Variant& value, const QString& typeName = QString()) const = 0; // avoid null values
   virtual VarPtr newFromFunction(ExpressionPtr f) const = 0; // f must not be null

   virtual BasicType basicType() const = 0;
   virtual QString typeName() const = 0;

   QString basicTypeName() const { return getBasicTypeName(basicType()); }

   virtual VarPtr clone() const = 0; // clones recursively
   virtual void assignFrom(VarPtr other) = 0; // Clone sub-items before assigning them! assignFrom should not change the basic type!

   //
   // Value functions
   // These functions apply only if the type is not Collection and not a Function
   //

   virtual Variant value() const = 0;
   virtual void setValue(const Variant& value) = 0; // Only for compatible types!

   //
   // Function call
   // These functions apply only if the type is a Function
   //

   virtual ExpressionPtr function() const = 0;
   virtual void setFunction(ExpressionPtr f) = 0;

   //
   // Item functions
   // These functions apply only if type is Collection
   //

   virtual int itemCount() const = 0;
   virtual int indexOfName(const QString& name) const = 0; // Returns -1 if name couldn't be found
   virtual QString getName(int index) const = 0; // Returns an empty name if index doesn't exist
   virtual VarPtr getItem(const QString& name) { return getItem(indexOfName(name)); }
   virtual VarPtr getItem(int index) = 0;
   virtual VarPtr getCollection(const QString& name) = 0; // Creates a default collection if name doesn't exist
   virtual VarPtr getCollection(int index) = 0; // Creates a default collection if index is greater or equal to the number of items
   virtual void setItem(const QString& name, const VarPtr& item) = 0; // If name is empty the item is appended to the collection without name
   virtual void setItem(int index, const VarPtr& item) = 0; // index must be between 0 and itemCount() inclusive
   virtual VarPtr takeItem(const QString& name) { return takeItem(indexOfName(name)); }
   virtual VarPtr takeItem(int index) = 0;
   virtual void removeItem(const QString& name) { return removeItem(indexOfName(name)); }
   virtual void removeItem(int index) = 0;

   virtual QList<VarPtr> items() const = 0;
   virtual QStringList itemNames() const = 0;

   bool isDictionary() const { return basicType() == BasicType::Collection && !getName(0).isEmpty(); }
   bool isList() const { return basicType() == BasicType::Collection && getName(0).isEmpty(); }

   // SetItems() is meant for internal purposes. The items are **not** cloned!
   virtual void setItems(const QStringList& itemNames, const QList<VarPtr>& items) = 0; // The number of itemNames must be less or equal to the number of items

   bool isItemInsertable(const VarPtr& item);

   // Apply a function object to all items in the item list.
   // func() is not called for this element!
   template<typename Function>
   void iterate(Function func)
   {
      const auto itemList = items();
      const auto nameList = itemNames();

      for (int i = 0; i < itemList.count(); ++i)
      {
         QString name;

         if (i < nameList.count())
            name = nameList.at(i);
         else
            name = QString::number(i);

         func(name, itemList.at(i).data());
      }
   }

   // Apply a function object recursively to all items and subitems in the item list.
   // func() is not called for this element!
   template<typename Function>
   void iterateTree(Function func, const QStringList& path = QStringList())
   {
      const auto itemList = items();
      const auto nameList = itemNames();

      for (int i = 0; i < itemList.count(); ++i)
      {
         QString name;

         if (i < nameList.count())
            name = nameList.at(i);
         else
            name = QString::number(i);

         auto childPath = QStringList(path) << name;

         func(childPath, itemList.at(i).data());

         itemList.at(i)->iterateTree(func, childPath);
      }
   }

   // Serializing functions

   static int serializerVersion;

   QByteArray serializeToAscii(int indent = 0) const;
   int deserializeFromAscii(const QByteArray& data, Tokenizer* tokenizer = nullptr, int pos = 0, int versionNumber = -1, int indent = 0);
};

inline QString IVariable::getBasicTypeName(BasicType basicType)
{
   switch (basicType)
   {
   case BasicType::Collection:
      return "Collection";
   case BasicType::Value:
      return "Value";
   case BasicType::Function:
      return "Function";
   }

   return QString();
}
