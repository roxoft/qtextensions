#ifndef QTLISTEXTENSIONS_H
#define QTLISTEXTENSIONS_H

#include "SerStreamLists.h"

// Since Qt 5.7.0 the comparison operators for lists and vecotors are defined in QList.h and QVector.h
#if QT_VERSION < QT_VERSION_CHECK(5,7,0)

#include <QList>
#include <QVector>

template <typename T>
bool operator<(const QList<T> &lhs, const QList<T> &rhs)
{
   int count = qMin(lhs.count(), rhs.count());

   for (int i = 0; i < count; ++i)
   {
      if (lhs.at(i) < rhs.at(i))
         return true;
      if (rhs.at(i) < lhs.at(i))
         return false;
   }

   return lhs.count() < rhs.count();
}

template <typename T>
bool operator<(const QVector<T> &lhs, const QVector<T> &rhs)
{
   int count = qMin(lhs.count(), rhs.count());

   for (int i = 0; i < count; ++i)
   {
      if (lhs.at(i) < rhs.at(i))
         return true;
      if (rhs.at(i) < lhs.at(i))
         return false;
   }

   return lhs.count() < rhs.count();
}

#endif

// Combines the elements of two ordred lists into one list of tuples.
//
// Matching elements (according to cmp()) are paired. Single elements are paired with a default value.
// Example:
//   First list: { 1, 2, 4, 6, 8 }
//   Second list: { 1, 2, 3, 6, 9 }
//   Result: { {1, 1}, {2, 2}, {0, 3}, {4, 0}, {6, 6}, {8, 0}, {0, 9} }
template <class L1, class L2, typename C>
QList<std::tuple<typename L1::value_type, typename L2::value_type>> zipOrdered(const L1& list1, const L2& list2, const C& cmp)
{
   QList<std::tuple<typename L1::value_type, typename L2::value_type>> result;

   auto it1 = list1.begin();
   auto it2 = list2.begin();

   while (it1 != list1.end() || it2 != list2.end())
   {
      if (it1 == list1.end() || (it2 != list2.end() && cmp(*it1, *it2) > 0))
      {
         // There are no more first elements or the second element is smaller than the first element
         result.append(std::tuple<typename L1::value_type, typename L2::value_type>(typename L1::value_type(), *it2));
         ++it2;
      }
      else if (it2 == list2.end() || (it1 != list1.end() && cmp(*it1, *it2) < 0))
      {
         // There are no more second elements or the first element is smaller than the second element
         result.append(std::tuple<typename L1::value_type, typename L2::value_type>(*it1, typename L2::value_type()));
         ++it1;
      }
      else
      {
         // There is a first and a second element and both are equal
         result.append(std::tuple<typename L1::value_type, typename L2::value_type>(*it1, *it2));
         ++it1;
         ++it2;
      }
   }

   return result;
}

template <class L>
QStringList toQStringList(const L& list) { return toStringList(list); }

template <class L>
QStringList toStringList(const L& list)
{
   QStringList dest;

   dest.reserve(list.count());
   for (auto&& element : list)
   {
      dest.append(toQString(element));
   }

   return dest;
}

template <class L, typename C>
QStringList toQStringList(const L& list, C convert) { return toStringList(list, convert); }

template <class L, typename C>
QStringList toStringList(const L& list, C convert)
{
   QStringList result;

   result.reserve(list.count());
   for (auto&& element : list)
   {
      result.append(convert(element));
   }

   return result;
}

template <typename E, class L, typename C>
QList<E> toList(const L& list, C convert)
{
   QList<E> result;

   result.reserve(list.count());
   for (auto&& element : list)
   {
      result.append(convert(element));
   }

   return result;
}

namespace std
{
   template <class L>
   L sorted(const L& list)
   {
      L sorted(list);
      sort(sorted.begin(), sorted.end());
      return sorted;
   }

   template <class L, typename P>
   L sorted_by(const L& list, P predicate)
   {
      L sorted(list);
      sort(sorted.begin(), sorted.end(), predicate);
      return sorted;
   }

   template <class L>
   L stable_sorted(const L& list)
   {
      L sorted(list);
      stable_sort(sorted.begin(), sorted.end());
      return sorted;
   }

   template <class L, typename P>
   L stable_sorted_by(const L& list, P predicate)
   {
      L sorted(list);
      stable_sort(sorted.begin(), sorted.end(), predicate);
      return sorted;
   }

   template <class L, typename U>
   L copy_if(const L& list, U unaryPredicate)
   {
      L result;

      for (auto&& element : list)
      {
         if (unaryPredicate(element))
            result.push_back(element);
      }

      return result;
   }

   template <class L, class V, class C>
   typename L::size_type lower_bound_index(const L& list, const V& value, C toKey)
   {
      typename L::size_type begin = 0;
      typename L::size_type end = list.size();

      while (begin < end)
      {
         if (toKey(list.at((begin + end) / 2)) < value)
            begin = (begin + end) / 2 + 1;
         else
            end = (begin + end) / 2;
      }

      return end;
   }

   template <class L, class V, class C>
   typename L::size_type upper_bound_index(const L& list, const V& value, C toKey)
   {
      typename L::size_type begin = 0;
      typename L::size_type end = list.size();

      while (begin < end)
      {
         if (value < toKey(list.at((begin + end) / 2)))
            end = (begin + end) / 2;
         else
            begin = (begin + end) / 2 + 1;
      }

      return end;
   }
}

#endif // QTLISTEXTENSIONS_H
