#ifndef QDECIMAL_H
#define QDECIMAL_H

#include "qtcoreex_global.h"
#include "QLocaleEx.h"
#include "SerStream.h"
#include <QString>
#include <QAtomicInt>

struct OCINumber;

// QDecimal can store decimal numbers with a precision of 567 digits (< 10^567) with up to 255 fractional digits.
//
// All operations are decimal based. A standard constructed QDecimal (isNull() returns true) behaves like a QDecimal initialized to 0 in all arithmetic operations.
//
// Conversion to string can contain thousand (group) separators.
//
class QTCOREEX_EXPORT QDecimal
{
public:
   enum GroupSeparatorMode { GS_Auto, GS_UseGroupSeparator, GS_OmitGroupSeparator }; // TODO: Check QLocale::NumberOptions

public:

   // Constructors

   QDecimal() : _data(nullptr) {}
   QDecimal(const QDecimal& rhs) : _data(rhs._data) { bind(); }
   QDecimal(int number, int fracDigits = 0) : _data(nullptr) { fromInt(number); setFracSize(fracDigits); }
   QDecimal(unsigned int number, int fracDigits = 0) : _data(nullptr) { fromUInt(number); setFracSize(fracDigits); }
   QDecimal(qint64 number, int fracDigits = 0) : _data(nullptr) { fromInt64(number); setFracSize(fracDigits); }
   QDecimal(quint64 number, int fracDigits = 0) : _data(nullptr) { fromUInt64(number); setFracSize(fracDigits); }
   QDecimal(double number) : _data(nullptr) { fromDouble(number); }
   QDecimal(const QString& number, bool* ok = nullptr) : _data(nullptr) { bool tmpOk = fromString(number); if (ok) *ok = tmpOk; }
   QDecimal(const QString& number, const QLocaleEx& locale, bool* ok = nullptr) : _data(nullptr) { bool tmpOk = fromString(number, locale); if (ok) *ok = tmpOk; }
   explicit QDecimal(const char* szNumber, int len = -1) : _data(nullptr) { fromString(szNumber, len); } // szNumber has to be in C locale

   // Destructor

   ~QDecimal() { release(); }

   // Assignments

   QDecimal& operator=(const QDecimal& rhs) { rhs.bind(); release(); _data = rhs._data; return *this; }
   QDecimal& operator=(const QString& number) { fromString(number); return *this; }
   QDecimal& operator=(int number) { fromInt(number); return *this; }
   QDecimal& operator=(qint64 number) { fromInt64(number); return *this; }
   QDecimal& operator=(quint64 number) { fromUInt64(number); return *this; }
   QDecimal& operator=(double number) { fromDouble(number); return *this; }

   // Comparison
   // The precision of the oparands are adjusted to the higher precision. 

   bool operator==(const QDecimal &rhs) const; // Makes a difference between Null and Zero!
   bool operator!=(const QDecimal &rhs) const { return !operator==(rhs); } // Makes a difference between Null and Zero!
   bool operator<(const QDecimal &rhs) const; // Null is smaller than any other not Null number
   bool operator>(const QDecimal &rhs) const { return rhs < *this; }
   bool operator<=(const QDecimal &rhs) const { return !(rhs < *this); }
   bool operator>=(const QDecimal &rhs) const { return !(*this < rhs); }

   // Increment / Decrement

   QDecimal &operator++();
   QDecimal operator++(int) { QDecimal result(*this); operator++(); return result; }
   QDecimal &operator--();
   QDecimal operator--(int) { QDecimal result(*this); operator--(); return result; }

   // Change sign operator

   QDecimal operator-() const;

   // Arithmetic operations

   QDecimal &operator+=(const QDecimal &rhs);
   QDecimal operator+(const QDecimal &rhs) const { QDecimal result(*this); result += rhs; return result; }
   QDecimal &operator-=(const QDecimal &rhs);
   QDecimal operator-(const QDecimal &rhs) const { QDecimal result(*this); result -= rhs; return result; }
   QDecimal &operator+=(int number);
   QDecimal operator+(int number) const { QDecimal result(*this); result += number; return result; }
   QDecimal &operator-=(int number);
   QDecimal operator-(int number) const { QDecimal result(*this); result -= number; return result; }

   // QDecimal multiplication / division

   // The number of fractional digits after a multiplication is the sum of the fractional digits of its operands
   // except for trailing zeros wich are stripped of the result up to the precision of the multiplicator.
   QDecimal &operator *=(const QDecimal &rhs);
   QDecimal operator *(const QDecimal &rhs) const { QDecimal result(*this); result *= rhs; return result; }
   // The number of fractional digits after a division is truncated to the scale of the dividend.
   QDecimal &operator /=(const QDecimal &rhs);
   QDecimal operator /(const QDecimal &rhs) const { QDecimal result(*this); result /= rhs; return result; }
   // Remainders have the fractional digits of the dividend plus the divisor.
   QDecimal &operator %=(const QDecimal &rhs);
   QDecimal operator %(const QDecimal &rhs) const { QDecimal tmp(*this); return tmp.devideBy(rhs); }

   // Integer multipliciton / division

   QDecimal &operator *=(qint32 rhs);
   QDecimal operator *(qint32 rhs) const { QDecimal result(*this); result *= rhs; return result; }
   QDecimal &operator /=(qint32 rhs);
   QDecimal operator /(qint32 rhs) const { QDecimal result(*this); result /= rhs; return result; }
   QDecimal &operator %=(qint32 rhs);
   qint32   operator %(qint32 rhs) const { QDecimal tmp(*this); return tmp.devideBy(rhs); }

   // Shifting operations.
   // The decimal digits are shifted to the right (positive) or to the lefts (negative) by count digits.
   // New digits are zero and excess digits are dropped.

   QDecimal& operator<<=(int count);
   QDecimal& operator>>=(int count);

   QDecimal operator<<(int count) const;
   QDecimal operator>>(int count) const;

   // Division helper (can be used to reduce calls)
   // Divisions keep their number of fractional digits.

   qint32   devideBy(qint32 divisor); // Devides self by divisor and returns the remainder (if divisor is 0 a division by zero exception is thrown)
   QDecimal devideBy(const QDecimal& divisor); // Devides self by divisor and returns the remainder (if divisor is 0 a null decimal is returned)

   // Sign changing

   bool negate(); // Returns false if this is a zero number

   // Properties (readonly)

   bool  isNull() const { return _data == nullptr; }
   bool  isZero() const; // Returns true if the number is 0 or NULL
   bool  isNegative() const { if (_data) return _data->negative != 0; return false; }
   int   length() const;
   int   integerDigits() const { return length() - fracSize(); }
   int   fractionalDigits() const { return fracSize(); }
   int   reciprocalMostSignificantDigit() const; // Index of the most significant digit after inverting (1/x)

   // Change number of fractional digits

   void shiftDecimal(int distance); // positive: moves the decimal point to the right (multiplication by ten); negative: moves the decimal point to the left (division by ten)
   QDecimal& moveDecimalPoint(int digits); // Same as shiftDecimal()
   QDecimal movedDecimalPoint(int digits) const; // Same as shiftDecimal() on a copy of this

   void setMinFractionalDigits(int fracDigits);
   QDecimal& minScale(int scale) { setMinFractionalDigits(scale); return *this; }
   QDecimal toMinScale(int scale) const { auto result(*this); result.setMinFractionalDigits(scale); return result; }

   QDecimal &round(int precision);
   QDecimal rounded(int precision) const;

   QDecimal &truncate(int fracDigits = -1); // If fracDigits is -1 all zero digits are truncated, -2 leaves at least 1 digit and so on.
   QDecimal truncated(int fracDigits = -1) const; // If fracDigits is -1 all zero digits are truncated, -2 leaves at least 1 digit and so on.

   // Conversion functions

   QString  toString(const QLocaleEx& locale = defaultLocale()) const; // The string contains group seperators if there are fractional digits otherwise not
   QString  toString(GroupSeparatorMode groupSeparatorMode, const QLocaleEx& locale = defaultLocale()) const;
   bool     fromString(const QString& number, const QLocaleEx& locale = defaultLocale()); // Returns true if successfully converted
   void     fromString(const char* szNumber, int len = -1); // szNumber has to be in C locale

   qint64   toInt64(bool* ok = nullptr) const; // Sets ok to false if the fractional part is not zero or this is NULL
   void     fromInt64(qint64 number);

   quint64  toUInt64(bool* ok = nullptr) const; // Sets ok to false if the fractional part is not zero or this is NULL
   void     fromUInt64(quint64 number);

   int      toInt(bool* ok = nullptr) const; // Sets ok to false if the fractional part is not zero, the value is too large or this is NULL
   void     fromInt(int number);

   unsigned int   toUInt(bool* ok = nullptr) const; // Sets ok to false if the fractional part is not zero, the value is too large or this is NULL
   void           fromUInt(unsigned int number);

   double   toDouble(bool* ok = nullptr) const; // Sets ok to false if this is NULL
   void     fromDouble(double number);

   void     toClsDecimal(quint32 clsDecimal[4]) const;
   QDecimal &fromClsDecimal(quint32 clsDecimal[4]);

   void     toOciNumber(OCINumber *ociNumber) const;
   QDecimal &fromOciNumber(const OCINumber *ociNumber);

   bool     toOdbcNumber(void *odbcNumber, int maxSize) const;
   QDecimal &fromOdbcNumber(const void *odbcNumber, int size, int scale, bool negative);

private:
   enum { blockSize = 4, digitsPerBlock = 9, maxBlocks = 63, maxFrac = 255 };

   struct Data
   {
      Data(int b) : fracSize(0), blocks(b), negative(0) {}

      QAtomicInt refCount;

      uint fracSize : 8; // Number of fractional digits (negative decimal exponent)
      uint blocks : 8; // Number of segments in the buffer
      uint unused1 : 8;
      uint unused2 : 6;
      uint unused3 : 1;
      uint negative : 1; // Sign
   };

private:
   void bind() const { if (_data) _data->refCount.ref(); }
   void release() { if (_data && !_data->refCount.deref()) { _data->~Data(); free(_data); } _data = nullptr; }

   int fracSize() const { if (_data) return (int)_data->fracSize; return 0; }
   void setFracSize(int fracSize) { if (_data && fracSize >= 0 && fracSize <= maxFrac) _data->fracSize = (uint)fracSize; } // Does not get exclusive access to the data!!!

   int size() const { if (_data) return qMax((int)_data->blocks * digitsPerBlock, (int)_data->fracSize); return 0; } // Size in digits

   int segmentCount() const { if (_data) return qMax((int)_data->blocks, 1); return 0; }

   int digits() const;

   void significantDigits(int& digitCount, int& mostSignificantDigit, int& trailingZeros) const; // ignores the fractional size; the decimal exponent can be determined by trailingZeros - fracSize()

   bool mantissaLess(int offset, const QDecimal &rhs) const; // Increases the precision of the left operand (negative offset) or right operand (positive offset) by offset digits before comaparing the mantissas.

   void incrementAt(int index); // This is the index of the digit!
   void decrementAt(int index); // This is the index of the digit!

   void add(const QDecimal &rhs, bool substract);

   quint64 internalToUInt64(bool* ok) const;

   quint32 at(int index) const; // Returns the segment at the given index (0 is the least significant segment)
   void set(int index, quint32 value); // Sets the segment at the given index (0 is the least significant segment)

   void cow(int index = 0); // Grants exclusive access and sufficient space to access index

   quint32 segmentValue(int index, int offset = 0) const; // Returns the segment at index after all segments have been shifted by offset digits (a negative offset shifts to the left)

private:
   Data* _data; // The digits of the number are stored after the structure in 32-bit segments with 9 digits starting with the least significant segment
};

Q_DECLARE_TYPEINFO(QDecimal, Q_MOVABLE_TYPE);

inline SerStream& operator<<(SerStream& stream, const QDecimal& decimal) { stream << decimal.toString(QLocale::C).toLatin1(); return stream; }
inline SerStream& operator>>(SerStream& stream, QDecimal& decimal) { decimal.fromString(stream.readData().data()); return stream; }

#endif // QDECIMAL_H
