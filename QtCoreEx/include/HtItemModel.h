#ifndef HTITEMMODEL_H
#define HTITEMMODEL_H

#include "qtcoreex_global.h"
#include "HtModel.h"
#include <QAbstractItemModel>
#include <QMimeData>

namespace Qt
{
   enum HtItemRole
   {
      ItemFlagsRole = 16,
   };
}

class QTCOREEX_EXPORT HtMimeData : public QMimeData
{
public:
   explicit HtMimeData(const QList<const HtNode*>& nodeList, const HtNode* draggedNode);
   ~HtMimeData() override = default;

   bool hasFormat(const QString& mimetype) const override;
   QStringList formats() const override;

   const QList<const HtNode*>& nodeList() const { return _nodeList; }
   const HtNode* draggedNode() const { return _draggedNode; }

protected:
#if QT_VERSION_MAJOR < 6
   QVariant retrieveData(const QString& mimetype, QVariant::Type preferredType) const override;
#else
   QVariant retrieveData(const QString& mimetype, QMetaType preferredType) const override;
#endif

private:
   QList<const HtNode*> _nodeList;
   const HtNode* _draggedNode = nullptr;
};

/*
 * HtItemModel is a combination of an HtModel and an QAbstractItemModel. Its the base class for models that can be used in Qt views (QTreeView, QTableView, QListView etc.).
 * 
 * To enable drops into the root node a drop on the first attribute of the root node must be enabled (coincidentally the first header).
 * 
 * To enable drag and drop for the complete tree set it as the default for the tree:
 *    rootNode()->addDefaultFlags(-1, Qt::ItemIsDragEnabled | Qt::ItemIsDropEnabled, Qt::ItemFlagsRole);
 */
class QTCOREEX_EXPORT HtItemModel : public QAbstractItemModel, public HtModel
{
   Q_OBJECT
   Q_DISABLE_COPY(HtItemModel)

public:
   HtItemModel(QObject *parent = nullptr) : QAbstractItemModel(parent) {}
   ~HtItemModel() override = default;

   static Qt::ItemFlags defaultItemFlags() { return Qt::ItemIsEnabled | Qt::ItemIsSelectable; }

public:
   // HtModelInterface

   Variant defaultAttribute(int role) const override; // The dafault ItemFlagsRole is (Qt::ItemIsEnabled | Qt::ItemIsSelectable)

   void insertColumn(int index, int count = 1) override;
   void removeColumn(int index, int count = 1) override;

public:
   // AbstractItemModel interface

   QModelIndex index(int row, int column, const QModelIndex & parent = QModelIndex()) const override;
   QModelIndex parent(const QModelIndex & index) const override;
   //QModelIndex nextRow(const QModelIndex &start) const override;
   //QModelIndex findRow(const QModelIndex &start, int role, const QVariant &value, int hits = 1, Qt::MatchFlags flags = Qt::MatchFlags(Qt::MatchStartsWith|Qt::MatchWrap)) const override;

   bool hasChildren(const QModelIndex &parent = QModelIndex()) const override;
   int rowCount(const QModelIndex &parent = QModelIndex()) const override;
   int columnCount(const QModelIndex &parent = QModelIndex()) const override;
   QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
   QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
   Qt::ItemFlags flags(const QModelIndex &index) const override;

   bool setHeaderData(int section, Qt::Orientation orientation, const QVariant &value, int role = Qt::EditRole) override;
   bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole) override;
   bool insertColumns(int position, int columns, const QModelIndex &parent = QModelIndex()) override;
   bool removeColumns(int position, int columns, const QModelIndex &parent = QModelIndex()) override;
   bool insertRows(int position, int rows, const QModelIndex &parent = QModelIndex()) override;
   bool removeRows(int position, int rows, const QModelIndex &parent = QModelIndex()) override;

   void sort(int column, Qt::SortOrder order = Qt::AscendingOrder) override;

   int   sortRole(int column = -1) const { return _columnSortRoles.value(column, _columnSortRoles.value(-1, Qt::DisplayRole)); }
   void  setSortRole(int role) { _columnSortRoles[-1] = role; }
   void  setSortRole(int column, int role) { _columnSortRoles[column] = role; }

   bool setData(const QModelIndex &topLeft, const QModelIndex &bottomRight, const QVariant &value, int role = Qt::EditRole);
   bool setData(int beginColumn, int beginRow, int endColumn, int endRow, const QVariant &value, int role = Qt::EditRole);

public: // Callback functions to notify for changes
   void onBeginInsertItems(HtNode* parent, int index, int count) override;
   void onEndInsertItems(HtNode* parent, int index, int count) override;
   void onBeginInsertColumns(HtNode* parent, int index, int count) override;
   void onEndInsertColumns(HtNode* parent, int index, int count) override;
   void onBeginRemoveItems(HtNode* parent, int index, int count) override;
   void onEndRemoveItems(HtNode* parent, int index) override;
   void onBeginRemoveColumns(HtNode* parent, int index, int count) override;
   void onEndRemoveColumns(HtNode* parent, int index) override;
   bool onBeginMoveItems(HtNode* parent, int from, int count, int to) override; // to must be the insert position **after** the items have been removed!
   void onEndMoveItems(HtNode* parent, int from, int count, int to) override; // to must be the insert position **after** the items have been removed!
   void onBeginSwapItems(HtNode* parent, int index1, int index2) override;
   void onEndSwapItems(HtNode* parent, int index1, int index2) override;
   void onBeginLayoutChange(HtNode* parent) override; // Notify about layout changes (reordering) in the child list of parent
   void onEndLayoutChange(HtNode* parent) override; // Must be the same parent as for onBeginLayoutChange
   void onBeginResetModel() override;
   void onEndResetModel() override;
   void onItemChanged(HtNode* item, int firstAttribute, int lastAttribute, int role = -1) override;

public: // Notification control functions

   // Supress all data change notifications.
   // This only works for data changes (not header data)! Inserting or removing rows or columns would currently break the mechanism with unpredictable consequences!
   void startCollectChanges() override;

   // Send one dataChanged() notification for the range of suppressed notifications.
   // nodeChanged() notifications are **not** send!
   void endCollectChanges() override;

public: // Index/node conversions
   const HtNode*  itemFromIndex(const QModelIndex& index) const;
   const HtNode*  nodeFromIndex(const QModelIndex& index) const { return itemFromIndex(index); }
   HtNode*        itemFromIndex(const QModelIndex& index);
   HtNode*        nodeFromIndex(const QModelIndex& index) { return itemFromIndex(index); }
   QModelIndex    indexFromItem(const HtNode* item, int attIndex = 0) const;
   QModelIndex    indexFromNode(const HtNode* item, int attIndex = 0) const { return indexFromItem(item, attIndex); }

signals:
   void nodeChanged(HtNode *node, int firstAttribute, int lastAttribute, int role); // If role is negative consider all roles as modified

private:
   QMap<int, int>    _columnSortRoles; // -1 is default for all columns
   QModelIndexList   _oldIndexList;

   // Collecting notifications
   int               _collectChanges = 0;
   QModelIndex       _topLeft;
   QModelIndex       _bottomRight;
   QVector<int>      _changedRoles;

private:
   static void insertColumnRecursive(HtNode* item, int index, int count);
   static void removeColumnRecursive(HtNode* item, int index, int count);
};

#endif // HTITEMMODEL_H
