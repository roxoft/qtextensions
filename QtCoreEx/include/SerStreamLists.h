#pragma once

#include "SerStream.h"
#include <QList>
#include <QVector>
#include <QStringList>
#include <QMap>

template <typename T>
SerStream& operator<<(SerStream& stream, const QList<T>& list)
{
   stream << list.size();
   foreach(const T & element, list)
      stream << element;

   return stream;
}

template <typename T>
SerStream& operator>>(SerStream& stream, QList<T>& list)
{
   int count = stream.readInt32();

   list.clear();
   list.reserve(count);
   while (count--)
   {
      T element;

      stream >> element;
      list.append(element);
   }

   return stream;
}

inline SerStream& operator<<(SerStream& stream, const QStringList& stringList)
{
   stream << (const QList<QString>&)stringList;

   return stream;
}

inline SerStream& operator>>(SerStream& stream, QStringList& stringList)
{
   stream >> (QList<QString>&)stringList;

   return stream;
}

#if QT_VERSION_MAJOR < 6
template <typename T>
SerStream& operator<<(SerStream& stream, const QVector<T>& list)
{
   stream << list.size();
   foreach(const T & element, list)
      stream << element;

   return stream;
}

template <typename T>
SerStream& operator>>(SerStream& stream, QVector<T>& list)
{
   list.resize(stream.readInt32());
   for (int i = 0; i < list.size(); ++i)
      stream >> list[i];

   return stream;
}
#endif

template <typename K, typename V>
SerStream& operator<<(SerStream& stream, const QPair<K, V>& pair)
{
   stream << pair.first << pair.second;

   return stream;
}

template <typename K, typename V>
SerStream& operator>>(SerStream& stream, QPair<K, V>& pair)
{
   stream >> pair.first >> pair.second;

   return stream;
}

template <typename K, typename V>
SerStream& operator<<(SerStream& stream, const QMap<K, V>& map)
{
   stream << map.size();
   for (typename QMap<K, V>::const_iterator it = map.begin(); it != map.end(); ++it)
   {
      stream << it.key();
      stream << it.value();
   }

   return stream;
}

template <typename K, typename V>
SerStream& operator>>(SerStream& stream, QMap<K, V>& map)
{
   for (int i = stream.readInt32(); i--; )
   {
      K key;
      V value;

      stream >> key;
      stream >> value;

#if QT_VERSION < QT_VERSION_CHECK(5,15,0)
      map.insertMulti(key, value);
#else
      map.insert(key, value);
#endif
   }

   return stream;
}
