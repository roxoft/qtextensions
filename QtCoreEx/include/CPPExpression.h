#ifndef CPPEXPRESSION_H
#define CPPEXPRESSION_H

#include "qtcoreex_global.h"
#include "ExpressionEvaluator.h"
#include "Tokenizer.h"
#include "CPPLanguageConstants.h"

class CppOperatorToken;

class QTCOREEX_EXPORT CppOperator : public AbstractExpression::Operator
{
   Q_DECLARE_TR_FUNCTIONS(CppOperator)

public:
   CppOperator(Type type = Type::Invalid, CppAction action = CppAction::Invalid) : Operator(type), _action(action) {}
   ~CppOperator() override = default;

   bool operator==(const Operator& rhs) const override { return _action == dynamic_cast<const CppOperator&>(rhs)._action; }

   void fixTypeNotPrefix() override;
   bool canBePostfix() override;

   CppAction action() const { return _action; }
   void setCppAction(Type type, CppAction action) { _type = type; _action = action; }

   bool isLeftToRight() const override;

   int priority() const override;

   bool isAssociative() const override;

   QString toString() const override;

   QPair<VarPtr, VarPtr> resolve(const IdentifierResolver& identifierResolver, const QList<ExpressionPtr>& operands) const override;

public:
   static void setBinaryValueOperation(CppAction action, const std::type_info& type, Variant(*operation)(const Variant&, const Variant&, const QLocaleEx&));

private:
   CppAction _action = CppAction::Invalid;
};

// Keeps the token for error handling
class QTCOREEX_EXPORT CppTokenOperator : public CppOperator
{
public:
   CppTokenOperator(const bp<CppOperatorToken>& opToken);
   ~CppTokenOperator() override = default;

   const Token& token() const { return _token; }

private:
   Token _token; // The token is used for error handling
};

class QTCOREEX_EXPORT CppTokenValue : public AbstractExpression::Value
{
   Q_DECLARE_TR_FUNCTIONS(CppTokenValue)

public:
   CppTokenValue() = default;
   CppTokenValue(const Token& token) : _token(token) {}
   ~CppTokenValue() override = default;

   const Token& token() const { return _token; }

   QPair<VarPtr, VarPtr> resolve(const IdentifierResolver& identifierResolver) const override;

private:
   Token _token;
};

QTCOREEX_EXPORT ExpressionPtr createCppExpressionTree(const QString& expression); // using createCppContext
QTCOREEX_EXPORT ExpressionPtr createVersatileExpressionTree(const QString& expression); // using createExpressionContext
QTCOREEX_EXPORT ExpressionPtr createCppExpressionTree(const QList<Token>& tokenList); // Must use CppOperatorTokens

QTCOREEX_EXPORT void toIdentifierList(const ExpressionPtr& expr, QStringList& identifierList);

#endif // CPPEXPRESSION_H
