#ifndef TOKENIZER_H
#define TOKENIZER_H

/*
 Using the tokenizer
 -------------------

 1. Create an instance of the class Tokenizer and initialize it with a TextStream and a primary context.
    The primary context can be created by for example createSqlContext() in SqlParser.h.
 2. Then repeatedly call the Tokenizer member function next() to get all tokens until an invalid token is returned.
 3. To check the type of a token use the is<>() or as<>() functions. The template parameter is the token class of the type to check.
    Additionally there are the convenience functions isIdentifier() and isSeparator().

Example:

    QString data;
    QTextStream is(&data, QIODevice::ReadOnly);
    Tokenizer tokenizer(&is, createSqlContext());
    QList<Token> tokenList;

    for (auto token = tokenizer.next(); token.isValid(); token = tokenizer.next())
    {
       tokenList.append(token);
    }
*/

#include "qtcoreex_global.h"
#include "TextStreamReader.h"
#include <QSharedData>
#include <Variant.h>
#include "SmartPointer.h"

// The token class contains information about one token.
// It usually has the raw text, the value and an indicator if the token was preceded by a white space.
// The type of a token is determined by the class of the internal token instance.
// The token type can be accessed by the is<>() and as<>() functions.
class QTCOREEX_EXPORT Token
{
public:
   // Base class for token implementations.
   // Implementations usually just add a specific value.
   class Impl : public QSharedData
   {
      friend class Tokenizer;

      Q_DISABLE_COPY(Impl)

   public:
      Impl() = default;
      virtual ~Impl() = default;

      virtual Variant value() const = 0;

      int startPos() const { return _startPos; } // Start position of the token in the data source
      int endPos() const { return _endPos; } // End position of the token in the data source

      const QString& parsedText() const { return _parsedText; } // Contains the parsed text if keepParsedText was set in the tokenizer

   private:
      int _startPos = 0; // Set directly by Tokenizer
      int _endPos = 0; // Set directly by Tokenizer
      QString _parsedText; // Set directly by Tokenizer
   };

   typedef bp<Impl> Ptr;

   //
   // Some basic types

   // White space
   class WhiteSpace : public Impl
   {
   public:
      WhiteSpace(const QString& spaceChars) : _spaceChars(spaceChars) {}
      ~WhiteSpace() override = default;

      const QString& spaceChars() const { return _spaceChars; }

      Variant value() const override { return _spaceChars; }

   private:
      QString _spaceChars;
   };

   // Identifier
   class Identifier : public Impl
   {
   public:
      Identifier(const QString& identifier) : _identifier(identifier) {}
      ~Identifier() override = default;

      const QString& identifier() const { return _identifier; }

      Variant value() const override { return _identifier; }

   private:
      QString _identifier;
   };

   // Separator
   class Separator : public Impl
   {
   public:
      Separator(QChar separator) : _separator(separator) {}
      ~Separator() override = default;

      QChar separator() const { return _separator; }

      Variant value() const override { return QString(_separator); }

   private:
      QChar _separator;
   };

   // Integer
   class Integer : public Impl
   {
   public:
      Integer(qint64 integer) : _integer(integer) {}
      ~Integer() override = default;

      qint64 integer() const { return _integer; }

      Variant value() const override { return _integer; }

   private:
      qint64 _integer;
   };

   // Decimal
   class Decimal : public Impl
   {
   public:
      Decimal(const QDecimal& decimal) : _decimal(decimal) {}
      ~Decimal() override = default;

      const QDecimal& decimal() const { return _decimal; }

      Variant value() const override { return _decimal; }

   private:
      QDecimal _decimal;
   };

   // Text
   class Text : public Impl
   {
   public:
      Text(const QString& text) : _text(text) {}
      ~Text() override = default;

      const QString& text() const { return _text; }
      QString& text() { return _text; }

      Variant value() const override { return _text; }

   private:
      QString _text;
   };

   class Binary : public Impl
   {
   public:
      Binary(const QByteArray& data) : _data(data) {}
      ~Binary() override = default;

      const QByteArray& data() const { return _data; }

      Variant value() const override { return _data; }

   private:
      QByteArray _data;
   };

   // Comment
   class Comment : public Impl
   {
   public:
      Comment(const QString& comment) : _comment(comment) {}
      ~Comment() override = default;

      const QString& comment() const { return _comment; }

      Variant value() const override { return _comment; }

   private:
      QString _comment;
   };

public:
   //
   // Token interface
   //

   Token(Impl* impl = nullptr) : _token(impl) {}
   Token(const Token& other) = default;
   ~Token() = default;

   Token& operator=(const Token& other) = default;

   bool isValid() const { return _token; }

   int startPos() const { return _token ? _token->startPos() : 0; }
   int endPos() const { return _token ? _token->endPos() : 0; }

   // Convenience function to check for an identifier
   bool isIdentifier(const QString& identifier, Qt::CaseSensitivity caseSensitivity = Qt::CaseSensitive) const
   {
      auto identifierToken = dynamic_cast<const Identifier*>(_token.data());
      return identifierToken && identifierToken->identifier().compare(identifier, caseSensitivity) == 0;
   }

   // Convenience function to check for a separator
   bool isSeparator(QChar separator) const
   {
      auto separatorToken = dynamic_cast<const Separator*>(_token.data());
      return separatorToken && separatorToken->separator() == separator;
   }

   // Type functions

   template <class C>
   bool is() const { static_assert(std::is_base_of<Impl, C>::value, "C must be derived from Impl"); return dynamic_cast<const C*>(_token.data()); }

   template <class C>
   bp<C> as() const { static_assert(std::is_base_of<Impl, C>::value, "C must be derived from Impl"); return bp<C>(dynamic_cast<C*>(_token.data())); }

   // Value access

   Variant value() const { return _token ? _token->value() : Variant::null; }

   QString identifier() const
   {
      const auto identifierToken = dynamic_cast<const Identifier*>(_token.data());
      return identifierToken ? identifierToken->identifier() : QString();
   }

   QChar separator() const
   {
      const auto separatorToken = dynamic_cast<const Separator*>(_token.data());
      return separatorToken ? separatorToken->separator() : QChar();
   }

   // String that was parsed to obtain the token
   QString parsedText() const { return _token ? _token->parsedText() : QString(); }

   const Ptr& impl() { return _token; }

private:
   Ptr _token;
};

class TokenException : public QBasicException
{
public:
   TokenException(const Token& token, const QString& message) : QBasicException(message), _token(token) {}
   ~TokenException() override = default;

   void raise() const override { throw *this; }
   TokenException *clone() const override { return new TokenException(*this); }

   const Token& token() const { return _token; }

private:
   Token _token;
};

// The Tokenizer splits a string into token
class QTCOREEX_EXPORT Tokenizer
{
   Q_DISABLE_COPY(Tokenizer)

public:
   // Base class for creating tokens.
   class Context
   {
   public:
      Context() = default;
      Context(const Context* outerContext) : _outerContext(outerContext) {}
      virtual ~Context() = default;

      // Tries to read the next token from data. Returns an invalid token if no token could be read and the stream is unaffected.
      // Returns the next context to be used for parsing. Returns nullptr if the parsing should stop (end of stream).
      virtual const Context* readToken(Token&, TextStreamReader& data) const = 0;

   protected:
      const Context* _outerContext = nullptr;
   };

public:
   Tokenizer(const Context* context); // The tokenizer takes ownership of the context.
   Tokenizer(const QString& text, const Context* context); // The tokenizer takes ownership of the context.
   Tokenizer(QTextStream* is, const Context* context); // The tokenizer takes ownership of the context.
   Tokenizer(CharacterStreamReader* dataSource, const Context* context); // The tokenizer takes ownership of the context.
   ~Tokenizer();

   bool keepParsedText() const { return _keepParsedText; }
   void setKeepParsedText(bool keepParsedText = true) { _keepParsedText = keepParsedText; }

   bool ignoreWhiteSpace() const { return _ignoreWhiteSpace; }
   void setIgnoreWhiteSpace(bool ignoreWhiteSpace = true) { _ignoreWhiteSpace = ignoreWhiteSpace; }

   bool ignoreComments() const { return _ignoreComments; }
   void setIgnoreComments(bool ignoreComments = true) { _ignoreComments = ignoreComments; }

   void setText(const QString& text);
   void setStream(QTextStream* is);
   void setDataSource(CharacterStreamReader* dataSource);

   Token next(); // Read the next token from the stream. An invalid token indicates the end of the stream

   int lineNo() const { return _data.lineCount() + 1; }

   QList<Token> tokenList(); // Reads all tokens from the stream until an invalid token is reached.

private:
   TextStreamReader _data;
   const Context* const _context;
   bool _keepParsedText = false;
   bool _ignoreWhiteSpace = false;
   bool _ignoreComments = false;
   const Context* _currentContext = nullptr;
};

//
// Some basic contexts
//

class QTCOREEX_EXPORT WhiteSpaceContext : public Tokenizer::Context
{
public:
   WhiteSpaceContext(const Context* outerContext) : Context(outerContext) {}
   WhiteSpaceContext(const QString& whiteSpaceChars, const Context* outerContext) : Context(outerContext), _whiteSpaceChars(whiteSpaceChars) {}
   ~WhiteSpaceContext() override = default;

   const Context* readToken(Token& token, TextStreamReader& data) const override;

private:
   QString _whiteSpaceChars;
};

class QTCOREEX_EXPORT IdentifierContext : public Tokenizer::Context
{
public:
   IdentifierContext(const Context* outerContext) : Context(outerContext), _allowedSpecialChars(QLatin1String("_")) {}
   IdentifierContext(const QString& allowedSpecialChars, const Context* outerContext) : Context(outerContext), _allowedSpecialChars(allowedSpecialChars) {}
   ~IdentifierContext() override = default;

   const Context* readToken(Token& token, TextStreamReader& data) const override;

private:
   QString _allowedSpecialChars;
};

class QTCOREEX_EXPORT SeparatorContext : public Tokenizer::Context
{
public:
   SeparatorContext(const Context* outerContext) : Context(outerContext) {}
   ~SeparatorContext() override = default;

   const Context* readToken(Token& token, TextStreamReader& data) const override;
};

class QTCOREEX_EXPORT NumericContext : public Tokenizer::Context
{
public:
   NumericContext(const Context* outerContext) : Context(outerContext), _locale(QLocale::C) {}
   NumericContext(const Context* outerContext, const QLocaleEx& locale) : Context(outerContext), _locale(locale) {}
   ~NumericContext() override = default;

   const Context* readToken(Token& token, TextStreamReader& data) const override;

private:
   QLocaleEx _locale;
};

class QTCOREEX_EXPORT HexadecimalDataContext : public Tokenizer::Context
{
public:
   HexadecimalDataContext(const Context* outerContext) : Context(outerContext) {}
   ~HexadecimalDataContext() override = default;

   const Context* readToken(Token& token, TextStreamReader& data) const override;
};

class QTCOREEX_EXPORT Base256DataContext : public Tokenizer::Context
{
public:
   Base256DataContext(const Context* outerContext) : Context(outerContext) {}
   ~Base256DataContext() override = default;

   const Context* readToken(Token& token, TextStreamReader& data) const override;
};

// A doubled delimiter is added to the string as one delimiter and parsing continues.
class QTCOREEX_EXPORT StringContext : public Tokenizer::Context
{
public:
   StringContext(QChar delimiter, const Context* outerContext) : Context(outerContext), _delimiter(delimiter) {}
   ~StringContext() override = default;

   const Context* readToken(Token& token, TextStreamReader& data) const override;

private:
   QChar _delimiter;
};

// Comment delimiters are for example:
//    "//", "\n"
//    "/*", "*/ "
// The space after */ indicates that the end delimiter is skipped
class QTCOREEX_EXPORT CommentContext : public Tokenizer::Context
{
public:
   CommentContext(const QList<QPair<QString, QString>>& commentDelimiters, const Context* outerContext) : Context(outerContext), _commentDelimiters(commentDelimiters) {}
   ~CommentContext() override = default;

   const Context* readToken(Token& token, TextStreamReader& data) const override;

private:
   QList<QPair<QString, QString>> _commentDelimiters;
};

// This context tries a collection of contexts until a valid token is returned.
// This is usually the base of all tokenizers.
class QTCOREEX_EXPORT ProbingContext : public Tokenizer::Context
{
public:
   ProbingContext(const Context* outerContext = nullptr);
   ~ProbingContext() override;

   void add(const Context* context) { _contexts.append(context); }
   void prepend(const Context* context) { _contexts.prepend(context); }

   const Context* readToken(Token& token, TextStreamReader& data) const override;

private:
   QList<const Context*> _contexts;
};

QTCOREEX_EXPORT QString toBase256(const QByteArray& data);
QTCOREEX_EXPORT QByteArray fromBase256(const QString& data);

#endif // TOKENIZER_H
