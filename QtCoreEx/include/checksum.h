#ifndef CHECKSUM_H
#define CHECKSUM_H

#include "qtcoreex_global.h"
#include <QIODevice>

QTCOREEX_EXPORT quint32 crc32(QIODevice* device); // CRC-32 by IEEE802.3 as used by ethernet

class QTCOREEX_EXPORT Crc32
{
public:
   Crc32() : crc32_rev(0xffffffff) {}
   ~Crc32() {}

   void addChar(char c);

   quint32 result() const { return crc32_rev ^ 0xffffffff; }

private:
   quint32 crc32_rev;
};

#endif // CHECKSUM_H
