#ifndef STRINGTEMPLATE_H
#define STRINGTEMPLATE_H

#include "qtcoreex_global.h"
#include "Tokenizer.h"
#include "ExpressionBase.h"

class StringTemplateTextToken : public Token::Text
{
public:
   StringTemplateTextToken(const QString& text) : Text(text) {}
   virtual ~StringTemplateTextToken() = default;
};

// This is a string containing placeholders
class QTCOREEX_EXPORT StringTemplate
{
public:
   StringTemplate();
   StringTemplate(const StringTemplate& other);
   StringTemplate(const QString& templateString);
   ~StringTemplate();

   StringTemplate& operator=(const StringTemplate& other);
   StringTemplate& operator=(const QString& templateString);

   const QList<Token>& tokenList() const // The token list always starts and ends with a StringTemplateTextToken which may be empty
   {
      return _tokenList;
   }

   bool placeholderNotClosed() const { return _placeholderNotClosed; }

   QString resolve(const VarPtr& varTree, const ResolverFunctionMap& functionMap = ResolverFunctionMap()) const;

   void parseTemplate(const QString& templateString, bool ignoreWhitespaceAndComments = true, const QChar& startDelimiter = u'{', const QChar& endDelimiter = u'}');

   QPair<int, int> expressionRangeAt(int textPos) const;

private:
   QList<Token> _tokenList;
   bool _placeholderNotClosed = false;
};
#endif // STRINGTEMPLATE_H
