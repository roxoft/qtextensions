#pragma once

#include <QPointer>
#include <QAtomicInt>

// QObjectPointer manages instances of QObject derived classes.
//
// The managed objects are destroyed when the last QObjectPointer to the object is destroyed and it is not guarded by a parent.
//
// Use qop_new to create instances of guarded QObjects
//
// There is also a qsp_new function returning a QSharedPointer.
//
template <class T>
class QObjectPointer
{
public:
   QObjectPointer() = default;
   explicit QObjectPointer(const QObjectPointer& other) : _pData(other._pData) { bind(); }
   QObjectPointer(QObjectPointer&& other) noexcept : _pData(other._pData) { other._pData = nullptr; }
   QObjectPointer(T* o) { assign(o); }
   ~QObjectPointer() { release(); }

   QObjectPointer& operator=(const QObjectPointer& other) { assign(other._pData); return *this; }
   QObjectPointer& operator=(QObjectPointer&& other) noexcept { qSwap(_pData, other._pData); return *this; }
   QObjectPointer& operator=(T* o) { assign(o); return *this; }

   bool operator==(const QObjectPointer& other) const { return _pData == other._pData; }
   bool operator==(T* o) const { if (_pData) return _pData->guardedPointer == o; return o == nullptr; }

   bool isNull() const { return _pData == nullptr || _pData->guardedPointer.isNull(); }

   void reset() { release(); }

   operator T* () const { return v(); }
   T* data() const { return v(); }

   T* operator->() const { return v(); }
   T& operator*() const { return *v(); }

private:
   struct SharedData
   {
      SharedData(T* object) : guardedPointer(object) {}

      void bind() { atomicInt.ref(); }
      void release() { if (atomicInt.deref()) return; T* p = guardedPointer.data(); guardedPointer = nullptr; if (p && p->QObject::parent() == nullptr) delete p; delete this; }

      QAtomicInt  atomicInt;
      QPointer<T> guardedPointer;
   };

private:
   T* v() const { if (!_pData) return nullptr; return _pData->guardedPointer; }

   void bind() { if (_pData) _pData->bind(); }
   void release() { if (_pData) _pData->release(); _pData = nullptr; }
   void assign(SharedData* o) { if (o) o->bind(); if (_pData) _pData->release(); _pData = o; }
   void assign(T* o) { if (_pData) _pData->release(); _pData = new SharedData(o); _pData->bind(); }

private:
   SharedData* _pData = nullptr;
};

template <class T, typename ... P>
QObjectPointer<T> qop_new(P&& ... p) { return new T(p...); }

template <class T, typename ... P>
QSharedPointer<T> qsp_new(P&& ... p) { return new T(p...); }
