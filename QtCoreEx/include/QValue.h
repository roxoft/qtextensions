#ifndef QVALUE_H
#define QVALUE_H

#include "QDecimal.h"

// QValue combines a magnitude (QDecimal) with a unit (QString).
class QTCOREEX_EXPORT QValue : public QDecimal
{
   friend SerStream& operator<<(SerStream&, const QValue&);
   friend SerStream& operator>>(SerStream&, QValue&);

public:
   QValue() {}
   QValue(const QValue &other) : QDecimal(other), _unit(other._unit) {}
   QValue(const QDecimal &other, const QString &unit) : QDecimal(other), _unit(unit) {}
   ~QValue() {}

   QValue &operator=(const QValue &other)
   {
      QDecimal::operator=(other);
      _unit = other._unit;
      return *this;
   }

   QValue &operator=(const QDecimal &other)
   {
      QDecimal::operator=(other);
      return *this;
   }

   bool operator==(const QValue &rhs) const { return QDecimal::operator==(rhs) && _unit == rhs._unit; }
   bool operator!=(const QValue &rhs) const { return !operator==(rhs); }
   bool operator<(const QValue &rhs) const { if (QDecimal::operator<(rhs)) return true; if (QDecimal::operator>(rhs)) return false; return _unit < rhs._unit; }
   bool operator>(const QValue &rhs) const { return rhs < *this; }
   bool operator<=(const QValue &rhs) const { return !(rhs < *this); }
   bool operator>=(const QValue &rhs) const { return !(*this < rhs); }

   QString unit() const { return _unit; }
   void setUnit(const QString &unit) { _unit = unit; }

   QString  toString(const QLocaleEx& locale = defaultLocale()) const;
   QString  toString(GroupSeparatorMode groupSeparatorMode, const QLocaleEx& locale = defaultLocale()) const;
   bool     fromString(const QString& number, const QString& unit, const QLocaleEx& locale = defaultLocale()); // Returns true if successfully converted

private:
   QString _unit;
};

inline SerStream& operator<<(SerStream& stream, const QValue& value) { stream << (const QDecimal&)value << value._unit; return stream; }
inline SerStream& operator>>(SerStream& stream, QValue& value) { stream >> (QDecimal&)value >> value._unit; return stream; }

#endif // QVALUE_H
