#ifndef CPPTOKENIZER_H
#define CPPTOKENIZER_H

#include "Tokenizer.h"
#include "CPPLanguageConstants.h"

QTCOREEX_EXPORT ProbingContext* createCppContext();
QTCOREEX_EXPORT ProbingContext* createVersatileExpressionContext();

class QTCOREEX_EXPORT CppWhiteSpaceContext : public Tokenizer::Context
{
public:
   CppWhiteSpaceContext(bool includeNewLine, const Context* outerContext) : Context(outerContext), _includeNewLine(includeNewLine) {}
   ~CppWhiteSpaceContext() override { delete _directiveContext; }

   const Context* readToken(Token& token, TextStreamReader& data) const override;

private:
   bool _includeNewLine = false;
   mutable bool _isNewLine = true;
   mutable Context* _directiveContext = nullptr;
};

class QTCOREEX_EXPORT CppStringContext : public Tokenizer::Context
{
public:
   CppStringContext(const Context* outerContext) : Context(outerContext) {}
   ~CppStringContext() override = default;

   const Context* readToken(Token& token, TextStreamReader& data) const override;
};

class QTCOREEX_EXPORT CppOperatorToken : public Token::Impl
{
public:
   CppOperatorToken(const char* op, int opIndex) : _op(op), _opIndex(opIndex) {}
   ~CppOperatorToken() override = default;

   const char* op() const { return _op; }
   int opIndex() const { return _opIndex; }

   Variant value() const override { return QString::fromLatin1(_op); }

private:
   const char* _op;
   int _opIndex;
};

// CppOperatorContext also reads comments!
class QTCOREEX_EXPORT CppOperatorContext : public Tokenizer::Context
{
public:
   CppOperatorContext(const Context* outerContext) : Context(outerContext) {}
   ~CppOperatorContext() override = default;

   const Context* readToken(Token& token, TextStreamReader& data) const override;
};

class QTCOREEX_EXPORT CppIdentifierToken : public Token::Identifier
{
public:
   CppIdentifierToken(const QString& identifier) : Identifier(identifier) {}
   ~CppIdentifierToken() override = default;

   int keyword() const;

private:
   mutable int _keyword = -1;
};

// Recognizes keywords
class QTCOREEX_EXPORT CppIdentifierContext : public IdentifierContext
{
public:
   CppIdentifierContext(const Context* outerContext) : IdentifierContext(QLatin1String("_$"), outerContext) {} // $ is Microsoft specific
   ~CppIdentifierContext() override = default;

   const Context* readToken(Token& token, TextStreamReader& data) const override;
};

class QTCOREEX_EXPORT CppExplicitIdentifierContext : public Tokenizer::Context
{
public:
   CppExplicitIdentifierContext(const Context* outerContext) : Context(outerContext) {}
   ~CppExplicitIdentifierContext() override = default;

   const Context* readToken(Token& token, TextStreamReader& data) const override;
};

class QTCOREEX_EXPORT CppDirectiveToken : public Token::Impl
{
public:
   CppDirectiveToken(CppDirectives directive) : _directive(directive) {}
   ~CppDirectiveToken() override = default;

   CppDirectives directive() const { return _directive; }

   Variant value() const override { return parsedText().trimmed(); }

private:
   CppDirectives _directive = DV_define;
};

class QTCOREEX_EXPORT CppDirectiveContext : public IdentifierContext
{
public:
   CppDirectiveContext(const Context* outerContext) : IdentifierContext(QLatin1String("_$"), outerContext) {} // $ is Microsoft specific
   ~CppDirectiveContext() override = default;

   const Context* readToken(Token& token, TextStreamReader& data) const override;
};

#endif // CPPTOKENIZER_H
