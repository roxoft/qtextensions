#ifndef QDBSCHEMA_H
#define QDBSCHEMA_H

#include "qtcoreex_global.h"
#include <QList>
#include <QMap>

// The data types DT_NSTRING and DT_LARGE_NSTRING are for special SQL UNICODE data types in the so called national character set (NVARCHAR for Oracle or MS SQL).
enum QDbDataType { DT_UNKNOWN, DT_BOOL, DT_INT, DT_UINT, DT_LONG, DT_ULONG, DT_DOUBLE, DT_DECIMAL, DT_STRING, DT_LARGE_STRING, DT_NSTRING, DT_LARGE_NSTRING, DT_DATE, DT_TIME, DT_DATETIME, DT_BINARY, DT_LARGE_BINARY };

enum QDbTransferMode { TM_Default, TM_Input, TM_Output, TM_InputOutput }; // For procedure calls (PL/SQL binds) TM_Default is TM_InputOutput, for all other binds its TM_Input

namespace QDbSchema
{
   class QTCOREEX_EXPORT DbObject
   {
   public:
      DbObject() {}
      ~DbObject() {}

      bool isValid() const { return !name.isEmpty(); }

   public:
      QString  name;
      QString  schema;
   };

   class QTCOREEX_EXPORT Type : public DbObject
   {
   public:
      Type() {}
      ~Type() {}

   public:
      int sqlType = 0;
      int size = 0;
   };

   class QTCOREEX_EXPORT Synonym : public DbObject
   {
   public:
      Synonym() {}
      ~Synonym() {}

   public:
      QString  translatedName;
      QString  translatedSchema;
   };

   class QTCOREEX_EXPORT Column : public DbObject
   {
   public:
      Column() {}
      ~Column() {}

   public:
      QDbDataType type = DT_UNKNOWN;
      int         vendorType = 0;
      int         size = 0; // Number of characters, digits, bytes etc. depending on the type
      int         scale = -1;
      bool        nullable = false;
      bool        readable = false;
      bool        writable = false;
      bool        hasDefault = false;

      QList<Column> recordColumns;
   };

   class QTCOREEX_EXPORT Table : public DbObject
   {
   public:
      Table() {}
      ~Table() {}

      Column column(const QString &name) const { foreach(const Column &column, columns) if (column.name.compare(name, Qt::CaseInsensitive) == 0) return column; return Column(); }

   public:
      QList<Column>  columns;
   };

   class QTCOREEX_EXPORT Function : public DbObject
   {
   public:
      Function() {}
      ~Function() {}

   public:
      QString        packageName;
      QList<Column>  arguments;
      Column         retVal;
      QList<Column>  resultSet;
      int            overloadId = 0;
   };

   class QTCOREEX_EXPORT Package : public DbObject
   {
   public:
      Package() {}
      ~Package() {}

      Function subprogram(const QString &name) const { foreach(const Function &function, subprograms) if (function.name.compare(name, Qt::CaseInsensitive) == 0) return function; return Function(); }

   public:
      QList<Function> subprograms;
   };

   class QTCOREEX_EXPORT Database : public DbObject
   {
   public:
      Database() {}
      ~Database() {}

      Table table(const QString &name) const { return tables.value(name.trimmed().toUpper()); }

      void clear()
      {
         synonyms.clear();
         tables.clear();
         functions.clear();
         packages.clear();
      }

   public:
      QMap<QString, Synonym> synonyms;
      QMap<QString, Table> tables;
      QMap<QString, Function> functions;
      QMap<QString, Package> packages;
   };
}

#endif // QDBSCHEMA_H
