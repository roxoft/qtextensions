#ifndef QDBSTATE_H
#define QDBSTATE_H

#include "qtcoreex_global.h"
#include "SmartPointer.h"
#include <QCoreApplication>

// Represents the state of a database object (connection, statement, transaction, etc.)
// Assigned can only be states with higher Type! resetState() must be called to reset a state to Normal.
class QTCOREEX_EXPORT QDbState
{
   Q_DECLARE_TR_FUNCTIONS(QDbState)

public:
   enum Type { Normal, Information, Warning, Error };
   enum Source { Unknown, Connection, Statement, Transaction };

   class ErrorHandler : public QSharedData
   {
   public:
      ErrorHandler() {}
      virtual ~ErrorHandler() {}

      virtual void onStateChanged(const QDbState &state) = 0;
   };

   typedef bp<ErrorHandler> ErrhPtr;

public:
   QDbState() = default;
   QDbState(const QString& sourceText, Source source = Statement);
   QDbState(Type type, const QString& text, Source source = Unknown, int number = 0);
   QDbState(Type type, const QString& text, const QString& sourceText, Source source = Statement, int number = 0);
   QDbState(const QDbState &other);
   ~QDbState() = default;

   QDbState &operator=(const QDbState &rhs);

   ErrhPtr  errorHandler() const { return _errorHandler; }
   void     setErrorHandler(ErrhPtr errorHandler) { _errorHandler = errorHandler; }

   void resetState();

   QString message(int descSize = 2048) const;

   const QString& text() const { return _text; }
   Type           type() const { return _type; }
   Source         source() const { return _source; }
   const QString& sourceText() const { return _sourceText; }
   int            number() const { return _number; }

   QDbState& withSourceText(const QString& sourceText);

private:
   QString  _text;
   Type     _type = Normal;
   Source   _source = Unknown;
   QString  _sourceText;
   int      _number = 0;
   ErrhPtr  _errorHandler;
};

#endif // QDBSTATE_H
