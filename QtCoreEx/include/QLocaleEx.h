#ifndef QLOCALEEX_H
#define QLOCALEEX_H

#include "qtcoreex_global.h"
#include <QLocale>

#include <QDate>
#include <QTime>
#include <QDateTime>
#include <QStringList>

class QDateTimeEx;
class Variant;
class QVariant;
class QTextCodec;

enum class DateTimePart { None, Timezone, Millisecond, Second, Minute, Hour, Day, Month, Year, Decade, Century, Millennium };

// QLocaleEx extends QLocale. Date and time formats can be configured individually.
class QTCOREEX_EXPORT QLocaleEx : public QLocale
{
public:
   QLocaleEx();
   QLocaleEx(const QString &name, DateTimePart timePrecision = DateTimePart::Second, FormatType formatType = LongFormat);
   QLocaleEx(Language language, Country country = AnyCountry, DateTimePart timePrecision = DateTimePart::Second, FormatType formatType = LongFormat);
   QLocaleEx(const QLocaleEx& other) = default;
   ~QLocaleEx() = default;

   QLocaleEx& operator=(const QLocaleEx& other) = default;

#if 0
   QString dateFormat(int formatIndex = 0) const
   {
      for (auto&& f : _dateTimeFormats)
      {
         if (f.minPrecision >= DateTimePart::Day)
         {
            if (formatIndex == 0)
               return f.format;
            formatIndex--;
         }
      }
      return QLocale::dateFormat();
   }
   QString timeFormat(int formatIndex = 0) const
   {
      for (auto&& f : _dateTimeFormats)
      {
         if (f.maxPrecision <= DateTimePart::Hour)
         {
            if (formatIndex == 0)
               return f.format;
            formatIndex--;
         }
      }
      return QLocale::timeFormat();
   }
   QString dateTimeFormat(int formatIndex = 0) const
   {
      for (auto&& f : _dateTimeFormats)
      {
         if (f.minPrecision <= DateTimePart::Minute && f.maxPrecision >= DateTimePart::Day)
         {
            if (formatIndex == 0)
               return f.format;
            formatIndex--;
         }
      }
      return QLocale::dateTimeFormat();
   }
#endif
   DateTimePart timePrecision() const { return _timePrecision; }

   QString dateTimeFormat(DateTimePart minPart, DateTimePart maxPart, bool editable = false) const;
   QString dateTimeFormat() const { return dateTimeFormat(_timePrecision, DateTimePart::Millennium); }
   QString dateFormat() const { return dateTimeFormat(DateTimePart::Day, DateTimePart::Millennium); }
   QString timeFormat() const { return dateTimeFormat(_timePrecision, DateTimePart::Hour); }

   bool toBool(const QString& string, bool *ok = nullptr) const;

   QDate toDate(const QString &string) const { return QLocale::toDate(string, dateFormat()); }
   QTime toTime(const QString &string) const { return QLocale::toTime(string, timeFormat()); }
   QDateTime toDateTime(const QString &string) const { return QLocale::toDateTime(string, dateTimeFormat()); }
   QDateTimeEx toDateTimeEx(const QString& string) const;

   QString toString(const QDate &date) const { return QLocale::toString(date, dateFormat()); }
   QString toString(const QTime &time) const { return QLocale::toString(time, timeFormat()); }
   QString toString(const QDateTime &dateTime) const { return QLocale::toString(dateTime, dateTimeFormat()); }
   QString toString(const QDateTimeEx &dateTime) const;

   QString toString(qlonglong i) const;
   QString toString(qulonglong i) const;
   QString toString(short i) const;
   QString toString(ushort i) const;
   QString toString(int i) const;
   QString toString(uint i) const;

   // Returns a string equivalent to the number i, formatted according to the printf 'f' format. To use other formats use the method of the base class 'QLocale'.
   QString toString(double i, int prec = -16) const;

   // Returns a string equivalent to the number i, formatted according to the printf 'f' format. To use other formats use the method of the base class 'QLocale'.
   QString toString(float i, int prec = -7) const;

   QString toString(bool b) const;
   QString toString(const QVariant& qVariant) const;

   //void resetDateTimeFormats() { _dateTimeFormats.clear(); }
   void setDateTimeFormat(DateTimePart minPart, DateTimePart maxPart, const QString& format, bool editable = false);
   void setDateFormat(const QString& format) { setDateTimeFormat(DateTimePart::Day, DateTimePart::Millennium, format); }
   void setTimeFormat(const QString& format) { setDateTimeFormat(_timePrecision, DateTimePart::Hour, format); }
   void setDateTimeFormat(const QString& format) { setDateTimeFormat(_timePrecision, DateTimePart::Millennium, format); }

   QChar decimalPoint() const { return _decimalPoint; }
   void  setDecimalPoint(QChar decimalPoint) { _decimalPoint = decimalPoint; }
   QChar groupSeparator() const { return _groupSeparator; }
   void  setGroupSeparator(QChar groupSeparator) { _groupSeparator = groupSeparator; }
   QChar percent() const { return _percent; }
   void  setPercent(QChar percent) { _percent = percent; }
   QChar zeroDigit() const { return _zeroDigit; }
   void  setZeroDigit(QChar zeroDigit) { _zeroDigit = zeroDigit; }
   QChar negativeSign() const { return _negativeSign; }
   void  setNegativeSign(QChar negativeSign) { _negativeSign = negativeSign; }
   QChar positiveSign() const { return _positiveSign; }
   void  setPositiveSign(QChar positiveSign) { _positiveSign = positiveSign; }
   QChar exponential() const { return _exponential; }
   void  setExponential(QChar exponential) { _exponential = exponential; }

public:
   static bool isCultureLocaleDefined(Language language, Country country);
   static const QLocaleEx& cultureLocale(Language language, Country country); // If the culture is not defined, QLocaleEx(language, country) is used
   static void setCultureLocale(const QLocaleEx& locale);
   static void setCultureAsDefault(Language language, Country country);

private:
   class DateTimeFormatRange
   {
   public:
      DateTimeFormatRange() {}
      DateTimeFormatRange(DateTimePart min, DateTimePart max, const QString& f) : minPrecision(min), maxPrecision(max), format(f) {}
      ~DateTimeFormatRange() {}

      DateTimePart minPrecision = DateTimePart::Millisecond;
      DateTimePart maxPrecision = DateTimePart::Millennium;
      QString format;
   };

private:
   void initFromLocale(FormatType formatType);
   DateTimeFormatRange bestDateTimeFormat(DateTimePart minPart, DateTimePart maxPart) const;

private:
   static void setDefault(const QLocale &locale); // Overrides QLocale::setDefault which should not be used

private:
   QList<DateTimeFormatRange> _dateTimeFormats;
   QList<DateTimeFormatRange> _editableDateTimeFormats;
   DateTimePart _timePrecision = DateTimePart::Second;

   QStringList _trueLiterals;
   QStringList _falseLiterals;
   QChar _decimalPoint;
   QChar _groupSeparator;
   QChar _percent;
   QChar _zeroDigit;
   QChar _negativeSign;
   QChar _positiveSign;
   QChar _exponential;
};

QTCOREEX_EXPORT const QLocaleEx& defaultLocale();
QTCOREEX_EXPORT void setDefaultLocale(const QLocaleEx& locale);

QTCOREEX_EXPORT const QLocaleEx& toQLocaleEx(const QLocale& locale); // Adds the locale to the cultures if necessary

template <class T>
QString toQString(const T& value)
{
   return value.toString();
}

QTCOREEX_EXPORT QString toQString(const Variant& value);

inline QString toQString(const char *str, int size = -1) { return QString::fromUtf8(str, size); }
inline QString toQString(const QDate& value) { return defaultLocale().toString(value); }
inline QString toQString(const QTime& value) { return defaultLocale().toString(value); }
inline QString toQString(const QDateTime& value) { return defaultLocale().toString(value); }
inline QString toQString(const qlonglong& value) { return defaultLocale().toString(value); }
inline QString toQString(const qulonglong& value) { return defaultLocale().toString(value); }
inline QString toQString(const short& value) { return defaultLocale().toString(value); }
inline QString toQString(const ushort& value) { return defaultLocale().toString(value); }
inline QString toQString(const int& value) { return defaultLocale().toString(value); }
inline QString toQString(const uint& value) { return defaultLocale().toString(value); }
inline QString toQString(const double& value) { return defaultLocale().toString(value); }
inline QString toQString(const float& value) { return defaultLocale().toString(value); }
inline QString toQString(bool b) { return defaultLocale().toString(b); }
inline QString toQString(const QVariant& value) { return defaultLocale().toString(value); }

inline QString eascii(const char *str, int size = -1) { return QString::fromLatin1(str, size); }
inline QString eascii(const QByteArray &str) { return QString::fromLatin1(str); }
inline QByteArray eascii(const QString &str) { return str.toLatin1(); }

inline QString eutf8(const char *str, int size = -1) { return QString::fromUtf8(str, size); }
inline QString eutf8(const QByteArray &str) { return QString::fromUtf8(str); }
inline QByteArray eutf8(const QString &str) { return str.toUtf8(); }

inline QString elocal(const char *str, int size = -1) { return QString::fromLocal8Bit(str, size); }
inline QString elocal(const QByteArray &str) { return QString::fromLocal8Bit(str); }
inline QByteArray elocal(const QString &str) { return str.toLocal8Bit(); }

#endif // QLOCALEEX_H
