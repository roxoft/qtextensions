#ifndef QDBCOMMANDENGINE_H
#define QDBCOMMANDENGINE_H

#include "qtcoreex_global.h"
#include "QDbConnection.h"
#include "QDbSchema.h"
#include "QDbState.h"
#include "Variant.h"
#include <QCoreApplication>

class QDbConnectionData;

class QTCOREEX_EXPORT QDbCommandEngine : public QSharedData
{
   Q_DISABLE_COPY(QDbCommandEngine)
   Q_DECLARE_TR_FUNCTIONS(QDbCommandEngine)

public:
   QDbCommandEngine();
   virtual ~QDbCommandEngine();

   QDbState       setStatement(const QString& statement);
   QDbState       setStatement(const QDbSchema::Function& function);
   QDbState       setProcedure(const QString& procedure);
   const QString& statement() const { return _statement; }

   void preparePlaceholder();

   const QString& preparedStatement() const{ return _preparedStatement; }

   int placeholderIndex(const QString& name) const; // name must be upper case

   QList<int> placeholderPositions(const QString &name) const; // name must be upper case

   QDbState setPlaceholderType(int index, QDbDataType type);
   QDbState setPlaceholderValue(int index, const Variant& value);
   QDbState setPlaceholderIsSending(int index, bool send);
   QDbState setPlaceholderIsReceiving(int index, bool receive);
   QDbState placeholderValue(int index, Variant& value) const;
   QDbState placeholderIsSending(int index, bool& send) const;
   QDbState placeholderIsReceiving(int index, bool& receive) const;

   QDbState prepare() { return prepare(_phList); }
   QDbState exec(int rowsToFetch) { return exec(_phList, rowsToFetch); }

   virtual QDbState rowCount(int &count) const = 0;

   virtual bool atEnd() const = 0;

   virtual QDbState next() = 0;

   virtual QDbSchema::Table  resultSchema() const = 0;
   virtual QDbState  columnValue(int index, Variant &value) const = 0;
   virtual QDbState  columnValue(const QString &name, Variant &value) const = 0;

   QString description() const;

protected:
   class PhInfo
   {
      Q_DISABLE_COPY(PhInfo)
   public:
      PhInfo(const QDbSchema::Column& schema, QDbConnection::PhType phType) : _schema(schema), _phType(phType) { _schema.name = _schema.name.toUpper(); }
      virtual ~PhInfo() {}

      const QDbSchema::Column& schema() const { return _schema; }

      QDbConnection::PhType phType() const { return _phType; }

      const QString& name() const { return _schema.name; }

      virtual bool isValid() const = 0;

      virtual Variant value() const = 0;
      virtual QDbState setValue(const Variant& value) = 0;

      QDbDataType type() const { return _schema.type; }
      virtual void setType(QDbDataType type) { _schema.type = type; }

      void addPosition(int pos) { _positions.append(pos); }
      const QList<int>& positions() const { return _positions; }
      void clearPositions() { _positions.clear(); }

      // These parameters apply to the server side

      bool canSend() const { return _schema.writable; }
      void setCanSend(bool canSend) { _schema.writable = canSend; }

      bool canReceive() const { return _schema.readable; }
      void setCanReceive(bool canReceive) { _schema.readable = canReceive; }

   protected:
      QDbSchema::Column _schema;
      QList<int> _positions;
      QDbConnection::PhType _phType = QDbConnection::PHT_All;
   };

   virtual QDbConnectionData* connection() = 0;

   virtual PhInfo* createPhInfo(const QDbSchema::Column& schema, QDbConnection::PhType phType) = 0;
   PhInfo* createPhInfo(const QString& name, QDbConnection::PhType phType) { QDbSchema::Column schema; schema.name = name; return createPhInfo(schema, phType); }

   virtual QDbState setPlaceholderType(PhInfo* phInfo, QDbDataType type) = 0;
   virtual QDbState setPlaceholderValue(PhInfo* phInfo, const Variant& value) = 0;
   virtual QDbState placeholderValue(const PhInfo* phInfo, Variant& value) const = 0;

   virtual QDbState prepare(const QList<PhInfo*>& phList) = 0;
   virtual QDbState exec(const QList<PhInfo*>& phList, int rowsToFetch) = 0;

   virtual void clear();

   QDbState errorInvalidIndex(int index) const;
   QDbState errorInvalidVariable(const PhInfo* phInfo) const;
   QDbState errorInferringDataType(const Variant& variant, const PhInfo* phInfo) const;
   QDbState errorInferringDataType(QDbDataType type, const PhInfo* phInfo) const;
   QDbState errorExecuting() const;

protected:
   QString _statement;
   QString _preparedStatement;
   QDbSchema::Function _functionSchema; // If functionSchema is valid the statement is a single identifier that identifies a stored procedure or a function

private:
   void createFunctionPlaceholder();
   PhInfo* addPh(QString name, int position); // name must be upper case

private:
   QList<PhInfo*> _phList;
};

#endif // QDBCOMMANDENGINE_H
