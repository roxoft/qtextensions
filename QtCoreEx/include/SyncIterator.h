#ifndef SYNCITERATOR_H
#define SYNCITERATOR_H

template <class L, class R>
bool syncLessThan(L left, R right)
{
   return left < right;
}

template <class L, class R>
bool syncPtrLessThan(L left, R right)
{
   return *left < *right;
}

template <class L, class R, typename LessThan>
class ConstSyncIterator
{
public:
   ConstSyncIterator(const L& left, const R& right, LessThan lessThan) : _left(left), _right(right), _itLeft(left.begin()), _itRight(right.begin()), _lessThan(lessThan), _state(S_Invalid) { compare(); }
   ~ConstSyncIterator() {}

   bool isValid() const { return _state != S_Invalid; }
   bool isLeftOnly() const { return _state == S_Left; }
   bool isRightOnly() const { return _state == S_Right; }
   bool isBoth() const { return _state == S_Both; }

   void next()
   {
      if (_state & S_Left)
         ++_itLeft;
      if (_state & S_Right)
         ++_itRight;
      compare();
   }

   typename L::const_reference left() const { return *_itLeft; }
   typename R::const_reference right() const { return *_itRight; }

   const typename L::const_iterator &leftIterator() const { return _itLeft; }
   const typename R::const_iterator &rightIterator() const { return _right; }

private:
   enum { S_Invalid, S_Left, S_Right, S_Both };

private:
   void compare()
   {
      _state = S_Invalid;

      if (_itLeft != _left.end() && !(_itRight != _right.end() && _lessThan(*_itRight, *_itLeft)))
         _state |= S_Left;
      if (_itRight != _right.end() && !(_itLeft != _left.end() && _lessThan(*_itLeft, *_itRight)))
         _state |= S_Right;
   }

private:
   const L& _left;
   const R& _right;

   typename L::const_iterator _itLeft;
   typename R::const_iterator _itRight;

   LessThan _lessThan;

   int _state;
};

template <class L, class R, typename LessThan>
class SyncIterator
{
public:
   SyncIterator(L& left, R& right, LessThan lessThan) : _left(left), _right(right), _itLeft(left.begin()), _itRight(right.begin()), _lessThan(lessThan), _state(S_Invalid) { compare(); }
   ~SyncIterator() {}

   bool isValid() const { return _state != S_Invalid; }
   bool isLeftOnly() const { return _state == S_Left; }
   bool isRightOnly() const { return _state == S_Right; }
   bool isBoth() const { return _state == S_Both; }

   void next()
   {
      if (_state & S_Left)
         ++_itLeft;
      if (_state & S_Right)
         ++_itRight;
      compare();
   }
   void removeLeft()
   {
      if (_state & S_Left)
         _itLeft = _left.erase(_itLeft);
      if (_state & S_Right)
         ++_itRight;
      compare();
   }
   void removeRight()
   {
      if (_state & S_Left)
         ++_itLeft;
      if (_state & S_Right)
         _itRight = _right.erase(_itRight);
      compare();
   }
   void remove()
   {
      if (_state & S_Left)
         _itLeft = _left.erase(_itLeft);
      if (_state & S_Right)
         _itRight = _right.erase(_itRight);
      compare();
   }

   typename L::reference left() const { return *_itLeft; }
   typename R::reference right() const { return *_itRight; }

   const typename L::iterator &leftIterator() const { return _itLeft; }
   const typename R::iterator &rightIterator() const { return _itRight; }

private:
   enum { S_Invalid, S_Left, S_Right, S_Both };

private:
   void compare()
   {
      _state = S_Invalid;

      if (_itLeft != _left.end() && !(_itRight != _right.end() && _lessThan(*_itRight,*_itLeft)))
         _state |= S_Left;
      if (_itRight != _right.end() && !(_itLeft != _left.end() && _lessThan(*_itLeft, *_itRight)))
         _state |= S_Right;
   }

private:
   L& _left;
   R& _right;

   typename L::iterator _itLeft;
   typename R::iterator _itRight;

   LessThan _lessThan;

   int _state;
};

template <class L, class R>
SyncIterator<L, R, bool (*)(typename L::const_reference, typename R::const_reference)> syncBegin(L& left, R& right, bool preSort = false)
{
   if (preSort)
   {
      std::sort(left.begin(), left.end());
      std::sort(right.begin(), right.end());
   }
   return SyncIterator<L, R, bool (*)(typename L::const_reference, typename R::const_reference)>(left, right, syncLessThan<typename L::const_reference, typename R::const_reference>);
}

template <class L, class R>
SyncIterator<L, R, bool (*)(typename L::value_type, typename R::value_type)> syncPtrBegin(L& left, R& right, bool preSort = false)
{
   if (preSort)
   {
      std::sort(left.begin(), left.end(), syncPtrLessThan<typename L::value_type, typename L::value_type>);
      std::sort(right.begin(), right.end(), syncPtrLessThan<typename R::value_type, typename R::value_type>);
   }
   return SyncIterator<L, R, bool (*)(typename L::value_type, typename R::value_type)>(left, right, syncPtrLessThan<typename L::value_type, typename R::value_type>);
}

template <class L, class R, typename LessThan>
SyncIterator<L, R, LessThan> syncBegin(L& left, R& right, LessThan lessThan, bool preSort = false)
{
   if (preSort)
   {
      std::sort(left.begin(), left.end(), lessThan);
      std::sort(right.begin(), right.end(), lessThan);
   }
   return SyncIterator<L, R, LessThan>(left, right, lessThan);
}

template <class L, class R>
ConstSyncIterator<L, R, bool (*)(typename L::const_reference, typename R::const_reference)> syncConstBegin(const L& left, const R& right)
{
   return ConstSyncIterator<L, R, bool (*)(typename L::const_reference, typename R::const_reference)>(left, right, syncLessThan<typename L::const_reference, typename R::const_reference>);
}

template <class L, class R>
ConstSyncIterator<L, R, bool (*)(typename L::value_type, typename R::value_type)> syncPtrConstBegin(const L& left, const R& right)
{
   return ConstSyncIterator<L, R, bool (*)(typename L::value_type, typename R::value_type)>(left, right, syncPtrLessThan<typename L::value_type, typename R::value_type>);
}

template <class L, class R, typename LessThan>
ConstSyncIterator<L, R, LessThan> syncConstBegin(const L& left, const R& right, LessThan lessThan)
{
   return ConstSyncIterator<L, R, LessThan>(left, right, lessThan);
}

#endif // SYNCITERATOR_H
