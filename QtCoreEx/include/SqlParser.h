#ifndef SQLPARSER_H
#define SQLPARSER_H

#include "Tokenizer.h"
#include "ExpressionEvaluator.h"

QTCOREEX_EXPORT Tokenizer::Context* createSqlContext();

class QTCOREEX_EXPORT SqlOperator : public AbstractExpression::Operator
{
public:
   enum class Action {
      Invalid,
      Punctuator, Addition, Subtraction, Multiplication, Division, Modulo, Equal, LeftEqual, RightEqual, NotEqual, NotLess, NotGreater, LessOrEqual, Less, GreaterOrEqual, Greater,
      SQL_ALL, SQL_AND, SQL_ANY, SQL_BETWEEN, SQL_NOT_BETWEEN, SQL_EXISTS, SQL_NOT_EXISTS, SQL_IN, SQL_NOT_IN, SQL_LIKE, SQL_NOT_LIKE, SQL_NOT, SQL_OR, SQL_UNIQUE, SQL_IS,
      Comma
   };

public:
   SqlOperator() = default;
   SqlOperator(Action action);
   SqlOperator(Action action, Type type);
   ~SqlOperator() override = default;

   bool operator==(const Operator& rhs) const override { return _action == dynamic_cast<const SqlOperator&>(rhs)._action; }

   void fixTypeNotPrefix() override {}
   bool canBePostfix() override { return false; }

   Action action() const { return _action; }
   void setAction(Action action, Type type) { _action = action; _type = type; }

   bool isLeftToRight() const override { return _type == Operator::Type::Binary; }

   int priority() const override;

   bool isAssociative() const override;

   QString toString() const override;

private:
   Action _action = Action::Invalid;
};

class QTCOREEX_EXPORT SqlTokenValue : public AbstractExpression::Value
{
public:
   SqlTokenValue() = default;
   SqlTokenValue(const Token& token) : _token(token) {}
   ~SqlTokenValue() override = default;

private:
   Token _token;
};

class QTCOREEX_EXPORT SqlOperatorToken : public Token::Impl
{
public:
   SqlOperatorToken(int opIndex) : _opIndex(opIndex) {}
   ~SqlOperatorToken() override = default;

   SqlOperator op() const;

   Variant value() const override;

private:
   int _opIndex;
};

class QTCOREEX_EXPORT SqlOperatorContext : public Tokenizer::Context
{
public:
   SqlOperatorContext(const Context* outerContext) : Context(outerContext), _identifierSpecialChars(QLatin1String("_")) {}
   SqlOperatorContext(const QString& identifierSpecialChars, const Context* outerContext) : Context(outerContext), _identifierSpecialChars(identifierSpecialChars) {}
   ~SqlOperatorContext() override = default;

protected:
   const Context* readToken(Token& token, TextStreamReader& data) const override;

private:
   bool isIdentifierCharacter(const QChar& c) const;

private:
   QString _identifierSpecialChars;
};

QTCOREEX_EXPORT ExpressionPtr createSqlExpressionTree(const QString& expression);
QTCOREEX_EXPORT ExpressionPtr createSqlExpressionTree(const QList<Token>& tokenList); // The Separators are used to identify compounds and SqlOperatorToken to identifty operations.

#endif // SQLPARSER_H
