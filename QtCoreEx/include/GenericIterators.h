#ifndef GENERICITERATORS_H
#define GENERICITERATORS_H

#include <QAtomicInt>

template <class Container, class Types = Container>
class IteratorCore
{
protected:
   IteratorCore(const IteratorCore& other) : _container(other._container) {} // _refCount is not copied because it provides information about the instance

   IteratorCore &operator=(const IteratorCore& other) { _container = other._container;  return *this; }

public:
   IteratorCore(const Container& container) : _container(container) {}
   virtual ~IteratorCore() {}

   virtual IteratorCore *clone() = 0;

   virtual bool isEqualTo(const IteratorCore& rhs) const { return _container == rhs._container; }
   virtual bool isLessThan(const IteratorCore& rhs) const = 0;

   virtual bool isValid() const = 0;

   virtual void moveForward(int distance = 1) = 0;
   virtual void moveBackward(int distance = 1) = 0;

   virtual int distanceFrom(const IteratorCore& other) const = 0;

   virtual typename Types::reference value() = 0;
   virtual typename Types::const_reference value() const = 0;
   virtual typename Types::pointer item() = 0;
   virtual typename Types::const_pointer item() const = 0;

public:
   void bind() { _refCount.ref(); }
   void release() { if (!_refCount.deref()) delete this; }

   bool isShared() const { return atomicLoadAcquire(_refCount) > 1; }

protected:
   Container _container;

private:
   QAtomicInt _refCount;
};

template <class Container, class Types = Container>
class GenericForwardIterator
{
public:
   GenericForwardIterator(IteratorCore<Container, Types>* core = nullptr) : _iterator(core) { bind(); }
   GenericForwardIterator(const GenericForwardIterator &other) : _iterator(other._iterator) { bind(); }
   ~GenericForwardIterator() { release(); }

   GenericForwardIterator &operator=(const GenericForwardIterator &other)
   {
      if (&other != this)
      {
         release();
         _iterator = other._iterator;
         bind();
      }
      return *this;
   }

   bool operator==(const GenericForwardIterator &rhs) const
   {
      return _iterator == rhs._iterator || _iterator && rhs._iterator && _iterator->isEqualTo(*rhs._iterator);
   }
   bool operator!=(const GenericForwardIterator& rhs) const { return !operator==(rhs); }

   bool isValid() const { return _iterator && _iterator->isValid(); }

   GenericForwardIterator& operator++() { claim(); _iterator->moveForward(); return *this; }
   GenericForwardIterator operator++(int) { GenericForwardIterator it(*this); operator++(); return it; }

   typename Types::reference operator*() { return _iterator->value(); }
   typename Types::pointer operator->() { return _iterator->item(); }

private:
   void bind() { if (_iterator) _iterator->bind(); }
   void release() { if (_iterator) _iterator->release(); }
   void claim() { if (_iterator && _iterator->isShared()) { IteratorCore<Container, Types> *it = _iterator->clone(); _iterator->release(); _iterator = it; _iterator->bind(); } }

private:
   IteratorCore<Container, Types> *_iterator;
};

template <class Container, class Types = Container>
class GenericForwardConstIterator
{
public:
   GenericForwardConstIterator(IteratorCore<Container, Types> *core = nullptr) : _iterator(core) { bind(); }
   GenericForwardConstIterator(const GenericForwardConstIterator &other) : _iterator(other._iterator) { bind(); }
   ~GenericForwardConstIterator() { release(); }

   GenericForwardConstIterator &operator=(const GenericForwardConstIterator &other)
   {
      if (&other != this)
      {
         release();
         _iterator = other._iterator;
         bind();
      }
      return *this;
   }

   bool operator==(const GenericForwardConstIterator &rhs) const
   {
      return _iterator == rhs._iterator || _iterator && rhs._iterator && _iterator->isEqualTo(*rhs._iterator);
   }
   bool operator!=(const GenericForwardConstIterator& rhs) const { return !operator==(rhs); }

   bool isValid() const { return _iterator && _iterator->isValid(); }

   GenericForwardConstIterator& operator++() { claim(); _iterator->moveForward(); return *this; }
   GenericForwardConstIterator operator++(int) { GenericForwardConstIterator it(*this); operator++(); return it; }

   typename Types::const_reference operator*() const { return _iterator->value(); }
   typename Types::const_pointer operator->() const { return _iterator->item(); }

private:
   void bind() { if (_iterator) _iterator->bind(); }
   void release() { if (_iterator) _iterator->release(); }
   void claim() { if (_iterator && _iterator->isShared()) { IteratorCore<Container, Types> *it = _iterator->clone(); _iterator->release(); _iterator = it; _iterator->bind(); } }

private:
   IteratorCore<Container, Types> *_iterator;
};

template <class Container, class Types = Container>
class GenericRandomAccessIterator
{
public:
   GenericRandomAccessIterator(IteratorCore<Container, Types> *core = nullptr) : _iterator(core) { bind(); }
   GenericRandomAccessIterator(const GenericRandomAccessIterator &other) : _iterator(other._iterator) { bind(); }
   ~GenericRandomAccessIterator() { release(); }

   GenericRandomAccessIterator &operator=(const GenericRandomAccessIterator &other)
   {
      if (&other != this)
      {
         release();
         _iterator = other._iterator;
         bind();
      }
      return *this;
   }

   bool operator==(const GenericRandomAccessIterator &rhs) const
   {
      return _iterator == rhs._iterator || _iterator && rhs._iterator && _iterator->isEqualTo(*rhs._iterator);
   }
   bool operator!=(const GenericRandomAccessIterator& rhs) const { return !operator==(rhs); }

   bool operator<(const GenericRandomAccessIterator &rhs) const
   {
      return _iterator && rhs._iterator && _iterator->isLessThan(*rhs._iterator);
   }
   bool operator>(const GenericRandomAccessIterator &rhs) const
   {
      return _iterator && rhs._iterator && rhs._iterator->isLessThan(*_iterator);
   }

   bool operator<=(const GenericRandomAccessIterator& rhs) const { return !operator>(rhs); }
   bool operator>=(const GenericRandomAccessIterator& rhs) const { return !operator<(rhs); }

   bool isValid() const { return _iterator && _iterator->isValid(); }

   GenericRandomAccessIterator& operator++() { claim(); _iterator->moveForward(); return *this; }
   GenericRandomAccessIterator operator++(int) { GenericRandomAccessIterator it(*this); operator++(); return it; }

   GenericRandomAccessIterator operator+(int distance) { GenericRandomAccessIterator it(*this); it._iterator->moveForward(distance); return it; }

   int operator-(const GenericRandomAccessIterator &rhs) { if (_iterator && rhs._iterator) return _iterator->distanceFrom(*rhs._iterator); return -1; }

   typename Types::reference operator*() const { return _iterator->value(); }
   typename Types::pointer operator->() const { return _iterator->item(); }

private:
   void bind() { if (_iterator) _iterator->bind(); }
   void release() { if (_iterator) _iterator->release(); }
   void claim() { if (_iterator && _iterator->isShared()) { IteratorCore<Container, Types> *it = _iterator->clone(); _iterator->release(); _iterator = it; _iterator->bind(); } }

private:
   IteratorCore<Container, Types> *_iterator;
};

template <class Container, class Types = Container>
class GenericRandomAccessConstIterator
{
public:
   GenericRandomAccessConstIterator(IteratorCore<Container, Types> *core = nullptr) : _iterator(core) { bind(); }
   GenericRandomAccessConstIterator(const GenericRandomAccessConstIterator &other) : _iterator(other._iterator) { bind(); }
   ~GenericRandomAccessConstIterator() { release(); }

   GenericRandomAccessConstIterator &operator=(const GenericRandomAccessConstIterator &other)
   {
      if (&other != this)
      {
         release();
         _iterator = other._iterator;
         bind();
      }
      return *this;
   }

   bool operator==(const GenericRandomAccessConstIterator &rhs) const
   {
      return _iterator == rhs._iterator || _iterator && rhs._iterator && _iterator->isEqualTo(*rhs._iterator);
   }
   bool operator!=(const GenericRandomAccessConstIterator& rhs) const { return !operator==(rhs); }

   bool operator<(const GenericRandomAccessConstIterator &rhs) const
   {
      return _iterator && rhs._iterator && _iterator->isLessThan(*rhs._iterator);
   }
   bool operator>(const GenericRandomAccessConstIterator &rhs) const
   {
      return _iterator && rhs._iterator && rhs._iterator->isLessThan(*_iterator);
   }

   bool operator<=(const GenericRandomAccessConstIterator& rhs) const { return !operator>(rhs); }
   bool operator>=(const GenericRandomAccessConstIterator& rhs) const { return !operator<(rhs); }

   bool isValid() const { return _iterator && _iterator->isValid(); }

   GenericRandomAccessConstIterator& operator++() { claim(); _iterator->moveForward(); return *this; }
   GenericRandomAccessConstIterator operator++(int) { GenericRandomAccessConstIterator it(*this); operator++(); return it; }

   GenericRandomAccessConstIterator operator+(int distance) { GenericRandomAccessConstIterator it(*this); it._iterator->moveForward(distance); return it; }

   int operator-(const GenericRandomAccessConstIterator &rhs) { if (_iterator && rhs._iterator) return _iterator->distanceFrom(*rhs._iterator); return -1; }

   typename Types::const_reference operator*() const { return _iterator->value(); }
   typename Types::const_pointer operator->() const { return _iterator->item(); }

private:
   void bind() { if (_iterator) _iterator->bind(); }
   void release() { if (_iterator) _iterator->release(); }
   void claim() { if (_iterator && _iterator->isShared()) { IteratorCore<Container, Types> *it = _iterator->clone(); _iterator->release(); _iterator = it; _iterator->bind(); } }

private:
   IteratorCore<Container, Types> *_iterator;
};

#endif // GENERICITERATORS_H
