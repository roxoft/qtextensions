#ifndef QTCOREEX_GLOBAL_H
#define QTCOREEX_GLOBAL_H

#include <QtCore/qglobal.h>

#ifdef QTCOREEX_LIB
# define QTCOREEX_EXPORT Q_DECL_EXPORT
#else
# define QTCOREEX_EXPORT Q_DECL_IMPORT
#endif

#endif // QTCOREEX_GLOBAL_H
