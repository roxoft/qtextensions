#ifndef QDBCONNECTION_H
#define QDBCONNECTION_H

#include "qtcoreex_global.h"
#include "QDbSchema.h"
#include "SmartPointer.h"
#include "Variant.h"
#include "QDbState.h"

class QDbConnectionData;
class QDbEnvironment;
class QDecimal;

enum class SqlStyle { SQL92, Oracle, SqlServer, Sybase, DB2, ODBC, SQLite };

#define SQL92_DATETIME_FORMAT QLatin1String("yyyy-MM-dd hh:mm:ss")
#define SQL92_DATE_FORMAT QLatin1String("yyyy-MM-dd")
#define SQL92_TIME_FORMAT QLatin1String("hh:mm:ss")

QTCOREEX_EXPORT QDbDataType dbTypeFor(const std::type_info& typeInfo);
QTCOREEX_EXPORT QDbDataType dbTypeFromVariant(const Variant& value);

/*
 * Represents a database connection.
 *
 * QDbConnection behaves like a smart pointer. Copies of QDbConnection refer to the same connection object.
 */
class QTCOREEX_EXPORT QDbConnection
{
   friend class QDbCommand;

public:
   enum PhType { PHT_All, PHT_NamedOnly, PHT_UnnamedOnly };

public:
   QDbConnection(QDbConnectionData *connection = nullptr);
   QDbConnection(const QDbConnection &other); // default not possible because of bp<QDbConnectionData>
   ~QDbConnection();

   QDbConnection& operator=(const QDbConnection &other);

   bool operator==(const QDbConnection &other) const;
   bool operator!=(const QDbConnection &other) const { return !operator==(other); }

   // Connection configuration

   QDbState::ErrhPtr errorHandler() const;
   void              setErrorHandler(QDbState::ErrhPtr errorHandler);
   void              setErrorHandler(QDbState::ErrorHandler* errorHandler); // Takes ownership of the handler

   bool treatEmptyAsNull() const; // By default empty strings and byte arrays are **NOT** treated as NULL
   void setTreatEmptyAsNull(bool emptyIsNull);

   int maxLiteralStringLength() const;
   void setMaxLiteralStringLength(int length);

   QString falseString() const;
   QString trueString() const;
   void setBooleanStrings(const QString& falseString, const QString& trueString); // Setting non empty strings overrides the native boolean handling of the database engines.

   int maxRowsPerDmlCall() const; // Maximum number of rows to be inserted or updated in one server call (per INSERT / UPDATE statement).
   void setMaxRowsPerDmlCall(int maxRowsPerDmlCall);

   // Connection state

   bool isValid() const;

   QString connectionString() const;
   QString dataSourceName() const;
   QString driverName() const;
   SqlStyle sqlStyle() const;
   PhType phType() const;

   QDbEnvironment *environment();

   QDbState state() const;

   // Transaction

   bool beginTransaction();
   bool rollbackTransaction();
   bool commitTransaction();

   // Special connection functions

   bool abortExecution(); // Aborts execution of a statement on the server

   bool tableExists(const QString &tableName) const;

   // Functions for generating SQL

   static QString escapeForLike(QString text);

   bool usePlaceholder(QDbDataType dbType, const Variant& value) const;
   QChar phIndicator() const;

   QString toLiteral(QDbDataType dbType, const Variant &value) const; // If the connection is not valid an SQL 92 conform literal is returned

   bool isDebugMode() const;
   void setIsDebugMode(bool debugMode = true);

   // Schema

   QDbSchema::Database  databaseSchema() const;
   QDbSchema::Synonym   synonymSchema(const QString &name) const; // Name must be upper case
   QDbSchema::Table     tableSchema(const QString &name) const; // Name must be upper case
   QDbSchema::Function  functionSchema(const QString &name) const; // Name must be upper case
   QDbSchema::Package   packageSchema(const QString &name) const; // Name must be upper case

   bool schemaCachingEnabled() const;
   void enableSchemaCaching();
   void disableSchemaCaching();

public:
   static QDbConnection defaultConnection(); // The connection created first will become the default connection and is removed if destroyed. The default connection must be bound outside (by a QDbConnection instance).

   static void removeDefaultConnection(); // Removes the default connection. The next connection created will become the default connection.

private:
   bp<QDbConnectionData> _data;
};

class QTCOREEX_EXPORT QDbTransaction
{
public:
   QDbTransaction(bool begin = false);
   QDbTransaction(QDbConnection connection, bool begin = false);
   ~QDbTransaction();

   void begin(QDbConnection connection = QDbConnection());
   void commit();
   void rollback();

private:
   QDbConnection  _connection;
   bool           _active;
};

typedef QDbTransaction QSqlTransaction;

#endif // QDBCONNECTION_H
