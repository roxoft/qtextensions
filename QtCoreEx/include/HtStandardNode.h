#ifndef HTSTANDARDNODE_H
#define HTSTANDARDNODE_H

#include "qtcoreex_global.h"
#include "HtNode.h"
#include <QMap>
#include <QVector>

// The HtStandardNode has no Qt::EditRole. Instead the Qt::Display role is used.
class QTCOREEX_EXPORT HtStandardNode : public HtNode
{
   Q_DISABLE_COPY(HtStandardNode)

public:
   HtStandardNode() : _attributeCount(0), _parent(nullptr), _model(nullptr) {}
   HtStandardNode(HtModel* model, HtNode* parent = nullptr) : _attributeCount(0), _parent(parent), _model(model) {}
   virtual ~HtStandardNode();

   HtStandardNode& operator=(const HtNode* other);

public:
   int attributeCount() const override { return _attributeCount; }
   Variant attribute(int index, int role = Qt::DisplayRole) const override;
   QVector<Variant> attributes(int role = Qt::DisplayRole) const override;
   QMap<int, Variant> attributeRoleMap(int index) const override;

   void  setAttribute(int index, const Variant& value, int role = Qt::EditRole) override;
   void  insertAttributes(int index, int count = 1) override; // Does not notify the model!
   void  removeAttributes(int index, int count = 1) override; // Removes the attributes from all roles. Does not notify the model!
   void  assignAttributesFrom(const HtNode* other) override;

   Variant defaultAttribute(int index, int role) const override;
   void setDefaultAttribute(int index, const Variant& value, int role = Qt::EditRole) override;

   int          childCount() const override { return _childList.count(); }
   const HtNode *child(int index) const override;
   int          indexOf(const HtNode* item) const override { return _childList.indexOf(const_cast<HtNode*>(item)); }

   void      assertSize(int size);

   HtNode*   child(int index) override;
   HtNode*   insertChild(int index, HtNode* child = nullptr) override;
   HtNode*   insertChild(int index, const HtNode* child, bool link = false) override;
   void      removeChild(int index) override;
   HtNode*   takeChild(int index) override;
   void      removeAllChildNodes() override;
   bool      move(int from, int to) override;

   QVector<HtNode*> childList() override { return _childList; }
   QVector<const HtNode*> childList() const override;
   void setChildList(const QVector<HtNode*>& childList) override;

   void sort(const HtSortOrder& sortOrder) override;

   HtNode* parent() const override { return _parent; }
   void setParent(HtNode* parent) override { _parent = parent; }

   HtModel* model() const override { return _model; }
   void setModel(HtModel* model) override;

protected:
   QMap<int, QVector<Variant> >  _attributeMap; // Map of values for each role
   int                           _attributeCount; // Maximum number of attributes over all roles
   QMap<int, QVector<Variant> >  _defaultMap; // Map of default values for each role. The first value in the vector is the default value for all indexes! Therefor the index of the value is the "attribute index" + 1.
   QVector<HtNode*>              _childList;
   HtNode*                       _parent;
   HtModel*                      _model;
};

#endif // HTSTANDARDNODE_H
