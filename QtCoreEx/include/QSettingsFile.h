#ifndef QSETTINGSFILE_H
#define QSETTINGSFILE_H

#include "qtcoreex_global.h"
#include "QSettingsEx.h"

QT_BEGIN_NAMESPACE
   class QIODevice;
   class QTextStream;
QT_END_NAMESPACE

// Store and receive settings from a file.
//
// The class can be used with QSettingsEx to separate the settings from the underlying storage.
//
class QTCOREEX_EXPORT QSettingsFile : public QSettingsExBase
{
   Q_DISABLE_COPY(QSettingsFile)

public:
   enum SerializeMode { SM_ToString, SM_VariantSerialize };

public:
   QSettingsFile(); // Standard constructor

   //
   // The parametrized constructors load the file immediately!
   // To have more control over the settings use the standard constructor and call open and load explicitly
   //

   QSettingsFile(const QString &applicationId, SerializeMode serializeMode = SM_ToString);
   QSettingsFile(const QString &applicationId, QSettingsEx::Scope scope, SerializeMode serializeMode = SM_ToString);
   QSettingsFile(const QString &applicationId, const QString &path, SerializeMode serializeMode = SM_ToString);
   QSettingsFile(QIODevice *file, SerializeMode serializeMode = SM_ToString); // Do not open the file (it is opened by QSettingsFile)

   // Destructor

   ~QSettingsFile() override;

   // Configuration

   QChar sectionSeparator() const
   {
      return _sectionSeparator.isNull() ? keySeparator() : _sectionSeparator;
   }

   void setSectionSeparator(QChar separator = u':') // See https://docs.microsoft.com/de-de/aspnet/core/fundamentals/configuration/?view=aspnetcore-3.0#ini-configuration-provider
   {
      _sectionSeparator = separator;
   }

   // open() uses platform specific file paths and tries to open all specified scopes by calling addIniFile for each found file

   bool open(const QString &applicationId, QSettingsEx::Scope scope = QSettingsEx::DefaultScope) override;
   bool isOpen() const override;

   // addIniFile() can be called several times for different layers (scopes)

   bool addIniFile(const QString &applicationId, const QString &path); // Call load() to obtain the settings
   bool addIniFile(QIODevice *file, bool own = false); // Do not open the file (it is opened by QSettingsFile). Call load() to obtain the settings

   // Additional configuration settings

   void setSerializeMode(SerializeMode serializeMode);

#if QT_VERSION_MAJOR < 6
   void setFileCodec(QTextCodec* codec) { _textCodec = codec; }
#else
   void setFileCodec(std::optional<QStringConverter::Encoding> codec) { _textCodec = codec; }
#endif

   void groupByIndentation(bool enable = true) { _groupByIndentation = enable; }
   void groupByBraces(bool enable = true) { _groupByBraces = enable; }

   // Loading and saving

   void load() override;
   void save() override;

private:
   class FileSetting : public Setting
   {
   public:
      FileSetting(QSettingsExBase* base) : Setting(base), linesBefore(0), linesBetween(0) {}
      virtual ~FileSetting() {}

      QString  fileRemark() const;
      void     setFileRemark(QStringList &remLines);

   protected:
      virtual Setting *newSetting(QSettingsExBase* base) const override { return new FileSetting(base); }

   private:
      int linesBefore; // Empty lines before the remark
      int linesBetween; // Empty lines between remark and key
   };

private:
#if QT_VERSION_MAJOR < 6
   QTextCodec* loadFile(int layer);
#else
   QStringConverter::Encoding loadFile(int layer);
#endif
   void saveFileHierarchic(QTextStream& os, Setting* settings, int indent, bool useBraces) const;
   void saveFileFlat(QTextStream& os, Setting* group) const;

private:
   QList<QIODevice*> _fileList;
   QList<QIODevice*> _ownFiles;
   SerializeMode _serializeMode = SM_ToString;
   bool _groupByIndentation = false;
   bool _groupByBraces = false;
   QString _bottomRemark;
   QChar _sectionSeparator;
#if QT_VERSION_MAJOR < 6
   QTextCodec* _textCodec = nullptr;
#else
   std::optional<QStringConverter::Encoding> _textCodec;
#endif
};

#endif // QSETTINGSFILE_H
