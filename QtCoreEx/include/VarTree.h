#ifndef VARTREE_H
#define VARTREE_H

#include "qtcoreex_global.h"
#include "IVariable.h"
#include "QLookup.h"

/*
 VarTree behaves like a dictionary with sub-dictionaries.
 VarTree has pointer semantics. This means that a copy of VarTree points to the same data. Use clone to create a real copy of VarTree.
 VarTree is based on VarTree::Variable and has therefore a freely configurable type
*/
class QTCOREEX_EXPORT VarTree
{
public:

   // A Variable can be a value, a list, a dictionary or a function.
   // The basic type is derived from the type.
   // Value types are: Bool, Date, Decimal, Integer, String and Binary.
   // The function type is: Function.
   // All other types are lists or dictionaries depending on _items and _itemNames.
   // To change this behaviour derive from Variable.
   class QTCOREEX_EXPORT Variable : public IVariable
   {
   public:
      Variable(const QString& typeName = QString()); // Basic type is Collection!
      Variable(const Variant& value, const QString& typeName); // Basic type is Value!
      Variable(ExpressionPtr f); // Basic type is Function!
      Variable(const Variable& other);
      ~Variable() override;

      Variable& operator=(const Variable& other);

      VarPtr newCollection(const QString& typeName = QString()) const override;
      VarPtr newFromValue(const Variant& value, const QString& typeName = QString()) const override;
      VarPtr newFromFunction(ExpressionPtr f) const override; // f must not be null

      BasicType basicType() const override;
      QString typeName() const override;

      VarPtr clone() const override; // clones recursively
      void assignFrom(VarPtr other) override;

      //
      // Value functions
      //

      Variant value() const override;
      void setValue(const Variant& value) override;

      static QString valueTypeName(const std::type_info& type); // Defaults to String

      //
      // Function call
      //

      ExpressionPtr function() const override;
      void setFunction(ExpressionPtr f) override;

      //
      // Collection functions
      //

      int itemCount() const override { return _items.count(); }
      int indexOfName(const QString& name) const override;
      QString getName(int index) const override;
      VarPtr getItem(int index) override;
      VarPtr getCollection(const QString& name) override;
      VarPtr getCollection(int index) override;
      void setItem(const QString& name, const VarPtr& item) override;
      void setItem(int index, const VarPtr& item) override;
      VarPtr takeItem(const QString& name) override;
      VarPtr takeItem(int index) override;
      void removeItem(const QString& name) override;
      void removeItem(int index) override;

      QList<VarPtr> items() const  override { return _items; }
      QStringList itemNames() const  override { return _itemNames; }
      void setItems(const QStringList& itemNames, const QList<VarPtr>& items) override;

   protected:
      BasicType _basicType = BasicType::Collection;
      QString _type;
      QList<VarPtr> _items;
      QStringList _itemNames; // Names of the items in _items
      mutable QLookup<QString, QStringList> _lookup; // Quickly find an index to a name
      Variant _value;
      ExpressionPtr _f;

   private:
      static QString lookupKey(const QStringList* list, int index)
      {
         return list->at(index);
      }
   };

public:
   // Make sure _data is always valid!

   VarTree();
   VarTree(const VarTree& varTree) = default;
   VarTree(const VarPtr& dictionary);

   ~VarTree() = default;

   VarTree& operator=(const VarTree& other) = default;

   VarPtr root() const { return _data; }

   //
   // Getting values
   //

   bool isEmpty() const
   {
      return _data->itemCount() == 0;
   }

   bool contains(const QString& path) const
   {
      return variable(path);
   }

   Variant value(const QString& path) const
   {
      return value(toPathList(path));
   }

   Variant value(const QStringList& pathList) const;

   QString type(const QString& path) const;

   VarTree at(const QString& path) // The returned tree points to the variable in this tree so that changes in the returned tree also affects this tree!
   {
      return at(path, false);
   }

   VarPtr variable(const QString& path) const;

   VarPtr variable(const QStringList& pathList) const; // The elements may contain indexes

   QList<int> indexHierarchy(const QString& path) const;

   QList<int> indexHierarchy(const QStringList& pathList) const;

   QList<VarPtr> hierarchy(const QString& path) const;

   QList<VarPtr> hierarchy(const QStringList& pathList) const;

   int rowCount() const;

   //
   // Creating elements
   //

   VarPtr newDictionaryAt(const QString& path, const QString& type)
   {
      return newDictionaryAt(toPathList(path), type);
   }

   VarPtr newDictionaryAt(const QStringList& pathList, const QString& type);

   //
   // Setting values
   //

   // Set with 2 parameters

   void set(const QString& path, int value) // There must be a special overload for int because the compiler can't decide if it should be converted to Variant, VarPtr or ExpressionPtr!
   {
      set(path, value, typeid(int));
   }

   void set(const QString& path, const Variant& value) // Sets the variable at path to value
   {
      set(path, value, value.type());
   }

   void set(const QString& path, const VarPtr& var)
   {
      set(toPathList(path), var);
   }

   void set(const QString& path, const ExpressionPtr& expression);

   void set(const QStringList& pathList, const VarPtr& variable);

   // Set with 3 parameters

   void set(const QString& path, const Variant& value, const QString& type)
   {
      set(toPathList(path), value, type);
   }

   void set(const QString& path, const Variant& value, const std::type_info& type)
   {
      set(toPathList(path), value, type);
   }

   void set(const QStringList& pathList, const Variant& value, const QString& type);

   void set(const QStringList& pathList, const Variant& value, const std::type_info& type);

   void set(const QStringList& pathList, int index, const VarPtr& var);

   // Set with 4 parameters

   void set(const QString& path, int index, const Variant& value, const QString& type)
   {
      set(toPathList(path), index, value, type);
   }

   void set(const QString& path, int index, const Variant& value, const std::type_info& type)
   {
      set(toPathList(path), index, value, type);
   }

   void set(const QStringList& pathList, int index, const Variant& value, const QString& type); // With index == -1 the value is added to the variable (possibly creating a list)

   void set(const QStringList& pathList, int index, const Variant& value, const std::type_info& type); // With index == -1 the value is added to the variable (possibly creating a list)

   // Special setters

   void setTree(const QString& path, const VarTree& tree);

   void setCellValue(int row, const QString& columnPath, const Variant& value); // Sets the variable at columnPath with index row to value. The variable must already exist.

   //
   // Adding values
   // If a variable already contains values the new value is appended to the list.
   //

   // add with 2 parameter

   void add(const QString& path, const Variant& value)
   {
      set(toPathList(path), -1, value, value.type());
   }

   void add(const QString& path, const std::type_info& type)
   {
      set(toPathList(path), -1, Variant::null, type);
   }

   // add with 3 parameters

   void add(const QString& path, const Variant& value, const QString& type)
   {
      set(toPathList(path), -1, value, type);
   }

   void add(const QString& path, const Variant& value, const std::type_info& type)
   {
      set(toPathList(path), -1, value, type);
   }

   // Table like functions

   // Adds a column to this VarTree which must represent a table
   void addColumn(const QString& columnPath, const QString& type)
   {
      addColumn(toPathList(columnPath), type);
   }

   // Adds a column to this VarTree which must represent a table
   void addColumn(const QString& columnPath, const std::type_info& type)
   {
      addColumn(toPathList(columnPath), type);
   }

   // Adds a column to this VarTree which must represent a table
   void addColumn(const QStringList& pathList, const QString& type);

   // Adds a column to this VarTree which must represent a table
   void addColumn(const QStringList& pathList, const std::type_info& type);

   // Sets value count of all columns to rowCount.
   // Currently only functional if _data is of type Variable.
   void setRowCount(int rowCount);

   // Removing variables

   Variant take(const QString& path, const QString& name); // returns the value at path+name and removes the variable

   void remove(const QString& path, const QString& name)
   {
      auto dict = getVariable(toPathList(path), false, nullptr);

      if (dict)
         dict->removeItem(name);
   }

   // Iterating functions

   // Apply func to all elements in the root
   template<class Function>
   void iterate(Function func)
   {
      _data->iterate(func);
   }

   // Apply func to all elements in the tree
   template<class Function>
   void iterateTree(Function func)
   {
      _data->iterateTree(func);
   }

#if 0 // XML handling
   /// <summary>
   /// Erzeugt ArtefaktDaten aus einer XML Struktur.
   /// Der Name des übergebenen Elements wird nicht verwendet.
   /// </summary>
   /// <param name="xElement">XElement</param>
   /// <returns>Die aus dem XML generierten ArtifactDaten</returns>
   private static ArtifactData FromXml(XElement xElement)
   {
      if (xElement == null)
      {
         return null;
      }

      // Neues (leeres) Artefakt Datenobjekt erzeugen
      var artifactData = new ArtifactData();

      // Schleife über alle XML-Elemente und rekursiv deren Kindelemente
      foreach (var element in xElement.Elements())
      {
         // Artefakt-Variablenname/-Pfad = Elementname
         var name = element.Name.LocalName;

         // Unterscheidung Gruppe/Einzelelement
         if (element.HasElements)
         {
            // ACHTUNG: Rekursiver Aufruf
            // Neues Artefaktdaten-Objekt mit den Unterelementen erstellen
            var subArtifactData = FromXml(element);

            // Ignoriere, wenn elementDict null ist
            if (subArtifactData != null)
            {
               // Ist der Variablenname/Pfad bereits vorhanden ?
               if (artifactData.Contains(name))
               {
                  // Der Name existiert bereits also machen wir daraus eine Tabelle

                  // Variablenname/Pfad als Tabelle markieren
                  artifactData.MarkAsTable(name);

                  // Zeile zur Tabelle hinzufügen
                  var table = artifactData.GetValue(name, AccessMode.Read).Value as TableDictionary;
                  if (table != null)
                  {
                     var row = table.RowCount();

                     table.AddRow();

                     MergeDict(table, subArtifactData._data.Value as IDictionary<QString, ArtifactValue>, row);
                  }
               }
               else
               {
                  // In den ArtefaktDaten den neuen Variablennamen/Pfad auf elementDict setzen
                  artifactData.Set(name, subArtifactData);
               }
            }
         }
         else
         {
            // Name/Wert-Paar hinzufügen
            // TODO: Attributes werden aktuell nicht ausgewertet
            // Datentyp des Elements aus Schemainfo ermitteln (Default: QString)
            // ACHTUNG: Das Element enthält nur dann ein SchemaInfo, wenn zuvor eine XSD-Validierung mit SchemaInfo-Erzeugung stattgefunden hat.
            var elementValue = GetDataTypeAndValue(element);

            // Name/Wert-Paar hinzufügen
            // ACHTUNG: Der Typ des Elements ist massgeblich für den Typ des Artefakts. Deshalb muss der Typ String 
            // eines jeden XML-Elements in den tatsächlichen Typ (aus der XSD) umgewandelt werden!
            // Der in Add() als zweite Parameter übergeben Typ ist nur relevant  falls das übergene value object null ist!
            artifactData.Add(name, elementValue.Value, elementValue.Type); // Der Wert wird hinzugefügt (nicht überschrieben), d.h. existiert name bereits wird der Wert in eine Liste umgewandelt. // TODO MRU TEX-3659: genau das soll nicht geschehen
         }
      }

      return artifactData;
   }

   /// <summary>
   /// Erzeugt ArtefaktDaten aus einer XML Struktur.
   /// Die XML wird gegen eine XSD validiert.
   /// </summary>
   /// <param name="xml">Das XML</param>
   /// <param name="xsd">Die XSD.</param>
   /// <returns>Die aus dem XML generierten ArtifactDaten</returns>
   public static ArtifactData FromXml(QString xml, QString xsd)
   {
      if (QString.IsNullOrEmpty(xml))
      {
         return null;
      }

      // XSD Validierung
      var xDoc = ValidateXml(xml, xsd);

      // wandle XML in AtrefactData um
      return FromXml(xDoc.Root);
   }

   /// <summary>
   /// Erzeugt ArtefaktDaten aus einer XML Struktur.
   /// Die XML Struktur beinhaltet bereits SchemaInformationen, so dass eine Validierung hier nicht mehr stattfindet.
   /// </summary>
   /// <param name="xDocument">Das XML als XDokument.</param>
   /// <returns>Die aus dem XML generierten ArtifactDaten</returns>
   public static ArtifactData FromXml(XDocument xDocument)
   {
      if (null == xDocument)
      {
         return null;
      }

      // wandle XML in ArtefactData um
      return FromXml(xDocument.Root);
   }

   /// <summary>
   /// Ermittelt den Datentyp und Wert des XElements aus dessen SchemaInfo (=Defaultwert, wenn nillable).
   /// </summary>
   /// <param name="element">Das XElement.</param>
   /// <returns>Den Datentyp des XElements bzw. QString, falls kein SchemaInfo vorhanden ist.</returns>
   private static ArtifactValue GetDataTypeAndValue(XElement element)
   {
      Type dataType = typeof(QString);
      object dataValue = element.Value;

      var xmlSchemaInfo = element.GetSchemaInfo();
      if (null != xmlSchemaInfo)
      {
         // Datentyp ermitteln
         var xmlMemberType = xmlSchemaInfo.MemberType;
         if (xmlMemberType != null)
         {
            dataType = xmlMemberType.Datatype.ValueType;
         }
         else
         {
            var xmlSchemaType = xmlSchemaInfo.SchemaType;
            if (xmlSchemaType != null)
            {
               dataType = xmlSchemaType.Datatype.ValueType;
            }
         }

         // Datenwert ermitteln
         if (xmlSchemaInfo.IsNil)
         {
            dataValue = null;
         }
         else
         {
            var typeConverter = TypeDescriptor.GetConverter(dataType);
            dataValue = typeConverter.ConvertFromInvariantString(element.Value);
         }
      }

      return new ArtifactValue(dataValue, dataType);
   }

   /// <summary>
   /// Validiere das XML gegen die XSD und erzeuge das SchemaInfo.
   /// </summary>
   /// <param name="xml">Die XML-Daten.</param>
   /// <param name="xsd">Die XSD-Daten.</param>
   /// <returns>Ein aus dem XML generiertes XDocument mit Schemainformationen, wenn eine XSD angegeben wurde, sonst ohne.</returns>
   public static XDocument ValidateXml(QString xml, QString xsd)
   {
      // Erzeuge XDocument aus XML.
      var xDoc = XDocument.Parse(xml);

      // Keine XSD definiert?
      if (QString.IsNullOrEmpty(xsd))
      {
         // Dann keine Validierung und kein SchemaInfo möglich.
         return xDoc;
      }

      List<QString> errors = new List<QString>();
      XmlSchemaSet schemas = new XmlSchemaSet();
      schemas.Add(QString.Empty, XmlReader.Create(new StringReader(xsd)));
      xDoc.Validate(schemas, (sender, e) => { errors.Add(e.Message); }, true);
      if (errors.Count > 0)
      {
         throw new Exception(QString.Format(TechnicalMessages.XsdValidationError, errors[0], xml, xsd));
      }

      // XDdocument samt SchemaInfo zurückgeben
      return xDoc;
   }

   static QStringList toPathList(TokenList tokenList)
   {
      var pathList = new List<QString>();

      // Alle Pfadelemente hinzufügen
      var enumerator = tokenList.GetEnumerator();

      while (enumerator.MoveNext() && enumerator.Current.Type == TokenType.Identifier)
      {
         pathList.Add(enumerator.Current.Identifier);

         if (!enumerator.MoveNext())
         {
            // Pfad ist zu ende und ein gültiger Pfad
            return pathList;
         }

         if (enumerator.Current.IsSeparator("["))
         {
            if (!enumerator.MoveNext() || enumerator.Current.Type != TokenType.Integer)
               break;
            var index = Convert.ToInt32(enumerator.Current.Value);
            if (!enumerator.MoveNext() || !enumerator.Current.IsSeparator("]"))
               break;
            pathList[pathList.Count - 1] = pathList[pathList.Count - 1] + "[" + index + "]";
            if (!enumerator.MoveNext())
            {
               // Pfad ist zu ende und ein gültiger Pfad
               return pathList;
            }
         }

         if (!enumerator.Current.IsSeparator("."))
         {
            // Ungültiger Pfad
            break;
         }
      }

      // Wenn wir hier hin kommen ist der Pfad ungültig
      return null;
   }
#endif

#if 0 // Merge
   /// <summary>
   /// Mergen von ArtifactData.
   /// Bereits vorhandene Variablen gleichen Datentyps werden wertmässig überschrieben. (Dies gilt auch für Tabellenzeilen! - Tabellenzeilen werden also nicht angehangen sondern ersetzt!)
   /// Das Hinzufügen einer bereits vorhandenen Variablen unterschiedlichen Datentyps führt zu einer Exception, wenn checkDataType = true ist.
   /// </summary>
   /// <param name="path">Einfügeposition in Ziel-ArtifactData.</param>
   /// <param name="artifactData">Die hinzuzufügenden ArtifactData.</param>
   /// <param name="checkDataType">Gibt an, ob die Variablen-Datentypen auf Eindeutigkeit geprüft werden sollen.</param>
   public void Merge(QString path, ArtifactData artifactData, bool checkDataType)
   {
      Merge(toPathList(path), artifactData, checkDataType);
   }

   /// <summary>
   /// Mergen von ArtifactData.
   /// Bereits vorhandene Variablen gleichen Datentyps werden wertmässig überschrieben. (Dies gilt auch für Tabellenzeilen! - Tabellenzeilen werden also nicht angehangen sondern ersetzt!)
   /// Das Hinzufügen einer bereits vorhandenen Variablen unterschiedlichen Datentyps führt zu einer Exception, wenn checkDataType = true ist.
   /// </summary>
   /// <param name="path">Einfüge-Pfadposition in Ziel-ArtifactData.</param>
   /// <param name="name">Einfüge-Variablenposition in Ziel-ArtifactData.</param>
   /// <param name="artifactData">Die hinzuzufügenden ArtifactData.</param>
   /// <param name="checkDataType">Gibt an, ob die Variablen-Datentypen auf Eindeutigkeit geprüft werden sollen.</param>
   public void Merge(QString path, QString name, ArtifactData artifactData, bool checkDataType)
   {
      path += ".";
      path += name;
      Merge(toPathList(path), artifactData, checkDataType);
   }

   /// <summary>
   /// Mergen von ArtifactData.
   /// Bereits vorhandene Variablen gleichen Datentyps werden wertmässig überschrieben. (Dies gilt auch für Tabellenzeilen! - Tabellenzeilen werden also nicht angehangen sondern ersetzt!)
   /// Das Hinzufügen einer bereits vorhandenen Variablen unterschiedlichen Datentyps führt zu einer Exception, wenn checkDataType = true ist.
   /// </summary>
   /// <param name="pathList">Einfügeposition in Ziel-ArtifactData.</param>
   /// <param name="artifactData">Die hinzuzufügendne ArtifactData.</param>
   /// <param name="checkDataType">Gibt an, ob die Variablen-Datentypen auf Eindeutigkeit geprüft werden sollen.</param>
   protected void Merge(IEnumerable<QString> pathList, ArtifactData artifactData, bool checkDataType)
   {
      if (pathList == null || artifactData == null)
      {
         return;
      }

      // Wir verändern die ArtifactData, deshalb für ToLlData den Cache hier zurücksetzen
      ClearCache();

      // Einfügeposition (=Dictionary) in bestehendem ArtifactData ermitteln
      var dest = GetValue(pathList, AccessMode.Read).Value as IDictionary<QString, ArtifactValue>;
      if (dest != null)
      {
         // mit neuen ArtifactData mergen
         MergeDict(dest, artifactData._data.Value as IDictionary<QString, ArtifactValue>, null, checkDataType);
      }
      else if (pathList.Any())
      {
         Set(pathList.Take(pathList.Count - 1), pathList.Last, artifactData, typeof(ArtifactData));
      }
      else
      {
         _data = artifactData._data;
      }
   }
#endif

private:
   static QStringList toPathList(QString path); // similar to path.Split('.')

   static QList<int> getIndexFromKey(QString& key);

   VarTree at(const QString& path, bool createDictionary = false);

   VarPtr getVariable(const QStringList& pathList, bool createDictionary, QList<int>* indexPath) const;

#if 0 // Merge
   /// <summary>
   /// Merge das Source-Dictionary in das Destination-Dictionary, wenn index null ist.
   /// Hinzufügen des SourceDictionary in das Destination-Dictionary, wenn der index nicht null ist.
   /// </summary>
   /// <param name="destValues">Destination-Dictionary</param>
   /// <param name="sourceValues">Source-Dictionary</param>
   /// <param name="index">Gibt an, an welchem Index eines Wertes im Destination-Dictionary der entsprechende Wert aus dem Source-Dictionary hinzugefügt wird. Siehe setValue-Methode.</param>
   /// <param name="checkDataType">Gibt an, ob der Datentyp gleicher Variablen geprüft werden soll.</param>
   private static void MergeDict(IDictionary<QString, ArtifactValue> destValues, IDictionary<QString, ArtifactValue> sourceValues, int? index = null, bool checkDataType = false)
   {
      foreach (var value in sourceValues)
      {
         if (value.Value.Value is IDictionary<QString, ArtifactValue>)
         {
            // Prüfe, ob die Variable bereits existiert
            if (checkDataType)
            {
               CheckDatatype(destValues, value);
            }

            var destDict = destValues.ValueOrDefault(value.Key).Value as IDictionary<QString, ArtifactValue>;

            if (destDict == null)
            {
               destValues[value.Key] = value.Value;
            }
            else
            {
               MergeDict(destDict, (IDictionary<QString, ArtifactValue>)value.Value.Value, index, checkDataType);
            }
         }
         else
         {
            // Prüfe, ob die Variable bereits existiert
            if (checkDataType)
            {
               CheckDatatype(destValues, value);
            }

            setValue(destValues, value.Key, value.Value.Value, index, value.Value.Type);
         }
      }
   }

   /// <summary>
   /// Prüfe, ob es die Variable im Ziel-Dictionary bereits gibt und falls ja, ob der Datentyp übereinstimmt.
   /// </summary>
   /// <param name="destValues">Ziel-Dictionary.</param>
   /// <param name="value">Die zu prüfende Variable.</param>
   private static void CheckDatatype(IDictionary<QString, ArtifactValue> destValues, KeyValuePair<QString, ArtifactValue> value)
   {
      var dest = destValues.ValueOrDefault(value.Key);
      if (dest.IsValid()
         && dest.Type != value.Value.Type)
      {
         // TODO MRU TEX-3659 Fehler mehr Infos
         // unterschiedliche Datentypen für denselben Variablennamen sind nicht zulässig
         throw new Exception(QString.Format(TechnicalMessages.XmlDatatypeDiffers, value.Key, dest.Type, value.Value.Type));
      }
   }
#endif

private:
   VarPtr _data;
};

#endif // VARTREE_H
