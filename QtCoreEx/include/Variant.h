#ifndef VARIANT_H
#define VARIANT_H

#include "qtcoreex_global.h"
#include "QProcessStateNotifier.h"
#include "SerStream.h"
#include "QLocaleEx.h"
#include "QText.h"
#include "QDecimal.h"
#include "QDateTimeEx.h"
#include <QString>
#include <QByteArray>
#include <QDate>
#include <QTime>
#include <QDateTime>
#include <QVariant>
#include <QMap>
#include <QAtomicInt>
#include <QDataStream>
#include <typeinfo>
#include <typeindex>

#define BEGIN_VARIANT_REGISTRATION static bool variantRegistration() {
#define END_VARIANT_REGISTRATION return true; } static bool variantRegistred = variantRegistration();

#if QT_VERSION >= QT_VERSION_CHECK(5,0,0)
inline int atomicLoadAcquire(const QAtomicInt &atomicInt) { return atomicInt.loadAcquire(); }
#else
inline int atomicLoadAcquire(const QAtomicInt &atomicInt) { return atomicInt; }
#endif

typedef bool (*GenericConversion)(void*, const void*, const QLocaleEx&);

QTCOREEX_EXPORT QByteArray portable_type_name(const std::type_info& typeInfo);
QTCOREEX_EXPORT QString plain_type_name(const std::type_info& typeInfo);

class VariantMeta
{
public:
   VariantMeta() = default;
   virtual ~VariantMeta() = default;

   // Object handling
   virtual void                  destroy() = 0;
   virtual const std::type_info& typeInfo() const = 0;
   virtual int                   metaSize() const = 0;
   virtual VariantMeta*          clone(void* memory = nullptr) const = 0;
   virtual const void*           valuePointer() const = 0;
   virtual void*                 valuePointer() = 0;

   // Comparison
   virtual bool isNull() const = 0;
   virtual bool isEqualTo(const VariantMeta& other) const = 0;
   virtual bool isLessThan(const VariantMeta& other) const = 0;

   // Conversions
   virtual bool hasConversion(const std::type_info& typeInfo) const = 0;
   virtual bool convertToQVariant(QVariant& value) const = 0;
   virtual bool convertInto(void* value, const std::type_info& typeInfo, const QLocaleEx& locale) const = 0;

   // Serialization
   virtual QByteArray portableName() const = 0;

   virtual bool serialize(SerStream* os) const = 0;
   virtual bool deserialize(SerStream* is) = 0;
};

struct MetaTypeInfoData
{
   MetaTypeInfoData() = default;
   ~MetaTypeInfoData() = default;

   VariantMeta* (*construct)(void* memory) = nullptr;
   void (*destroy)(VariantMeta* object) = nullptr;
   int size = 0;
   QByteArray portableName;
#if defined Q_CC_MSVC
   QByteArray raw_name;
#endif

   QMap<std::type_index, GenericConversion> conversionMap;
   GenericConversion qVariantConversion = nullptr;

   QAtomicInt  refCount;
   bool        old = false;
};

class MetaTypeInfo
{
public:
   MetaTypeInfo() = default;
   MetaTypeInfo(const MetaTypeInfo& other) : _d(other._d) { bind(); }
   MetaTypeInfo(MetaTypeInfo&& other) noexcept : _d(other._d) { other._d = nullptr; }
   ~MetaTypeInfo() { release(); }

   MetaTypeInfo& operator=(const MetaTypeInfo& other) { const_cast<MetaTypeInfo&>(other).bind(); release(); _d = other._d; return *this; }

   const MetaTypeInfoData*  constData() const { return _d; }
   MetaTypeInfoData*        data() { if (_d == nullptr) { _d = new MetaTypeInfoData; bind(); } else if (atomicLoadAcquire(_d->refCount) != 1) { _d->old = true; _d = new MetaTypeInfoData(*_d); _d->old = false; bind(); } return _d; }

private:
   void bind() { if (_d) _d->refCount.ref(); }
   void release() { if (_d && !_d->refCount.deref()) delete _d; _d = nullptr; }

private:
   MetaTypeInfoData *_d = nullptr;
};

QTCOREEX_EXPORT MetaTypeInfo getAcceleratedMetaTypeInfo(QAtomicInt& protect, MetaTypeInfo& metaTypeInfo, const std::type_info& typeInfo);

template <typename T>
bool isVariantNull(const T&)
{
   return false;
}

template <typename T>
bool variantEquals(const T& value1, const T& value2)
{
   return value1 == value2;
}

template <typename T>
bool variantLess(const T& lhs, const T& rhs)
{
   return lhs < rhs;
}

template <typename T>
class VariantTypeInfo : public VariantMeta
{
public:
   VariantTypeInfo() : _value() {}
   VariantTypeInfo(const VariantTypeInfo& rhs) : _value(rhs._value) {}
   explicit VariantTypeInfo(const T& value) : _value(value) {}
   ~VariantTypeInfo() override = default;

   VariantTypeInfo& operator=(const VariantTypeInfo& rhs) { _value = rhs._value; return *this; }

   void destroy() override { delete this; }
   const std::type_info& typeInfo() const override { return typeid(T); }
   int metaSize() const override { return sizeof(*this); }
   VariantMeta* clone(void* memory = nullptr) const override { if (memory == nullptr) return new VariantTypeInfo(*this); return new(memory) VariantTypeInfo(*this); }
   const void* valuePointer() const override { return &_value; }
   void* valuePointer() override { return &_value; }
   bool isNull() const override { return isVariantNull(_value); }
   bool isEqualTo(const VariantMeta& other) const override { return variantEquals(_value, static_cast<const VariantTypeInfo&>(other)._value); }
   bool isLessThan(const VariantMeta& other) const override { return variantLess(_value, static_cast<const VariantTypeInfo&>(other)._value); }

   bool hasConversion(const std::type_info& typeInfo) const override
   {
      return getTypeInfo().constData()->conversionMap.contains(typeInfo);
   }

   bool convertToQVariant(QVariant& value) const override
   {
      const auto qVariantConversion = getTypeInfo().constData()->qVariantConversion;
      return qVariantConversion && qVariantConversion(&value, &_value, defaultLocale());
   }

   bool convertInto(void* value, const std::type_info& typeInfo, const QLocaleEx& locale) const override
   {
      const auto converter = getTypeInfo().constData()->conversionMap.value(typeInfo);
      return converter && converter(value, &_value, locale);
   }

   QByteArray portableName() const override { return getTypeInfo().constData()->portableName; }

   bool serialize(SerStream* os) const override { *os << _value; return !os->hasError(); }
   bool deserialize(SerStream* is) override { *is >> _value; return !is->hasError(); }

   const T& value() const { return _value; }

private:
   static MetaTypeInfo getTypeInfo();

private:
   T _value;
};

#ifdef Q_OS_WIN
#pragma managed(push,off)
#endif
template <typename T>
MetaTypeInfo VariantTypeInfo<T>::getTypeInfo()
{
   static QAtomicInt    protect; // Protect the cache
   static MetaTypeInfo  typeInfo; // Cache

   return getAcceleratedMetaTypeInfo(protect, typeInfo, typeid(T));
}
#ifdef Q_OS_WIN
#pragma managed(pop)
#endif

template <>
class QTCOREEX_EXPORT VariantTypeInfo<QVariant> : public VariantMeta
{
public:
   VariantTypeInfo() = default;
   VariantTypeInfo(const VariantTypeInfo& rhs) : _value(rhs._value) {}
   VariantTypeInfo(const QVariant& value) : _value(value) {}
   ~VariantTypeInfo() override = default;

   VariantTypeInfo& operator=(const VariantTypeInfo& rhs) { _value = rhs._value; return *this; }

   void destroy() override { delete this; }
   const std::type_info& typeInfo() const override { return typeid(QVariant); }
   int metaSize() const override { return sizeof(*this); }
   VariantMeta* clone(void* memory = nullptr) const override { if (memory == nullptr) return new VariantTypeInfo(*this); return new(memory) VariantTypeInfo(*this); }
   const void* valuePointer() const override { return &_value; }
   void* valuePointer() override { return &_value; }
   bool isNull() const override { return _value.isNull(); }
   bool isEqualTo(const VariantMeta& other) const override { return _value == static_cast<const VariantTypeInfo&>(other)._value; }
   bool isLessThan(const VariantMeta& other) const override { return _value.toString() < static_cast<const VariantTypeInfo&>(other)._value.toString(); }

   bool hasConversion(const std::type_info& typeInfo) const override
   {
      return getTypeInfo().constData()->conversionMap.contains(typeInfo);
   }

   bool convertToQVariant(QVariant& value) const override
   {
      value = _value; return true;
   }

   bool convertInto(void* value, const std::type_info& typeInfo, const QLocaleEx& locale) const override
   {
      const auto converter = getTypeInfo().constData()->conversionMap.value(typeInfo);
      return converter && converter(value, &_value, locale);
   }

   QByteArray portableName() const override { return getTypeInfo().constData()->portableName; }

   bool serialize(SerStream* os) const override { QByteArray a; QDataStream s(&a, QIODevice::WriteOnly); s.setVersion(QDataStream::Qt_4_7); s << _value; *os << a; return !os->hasError(); }
   bool deserialize(SerStream* is) override { QDataStream s(is->readData()); s.setVersion(QDataStream::Qt_4_7); s >> _value; return !is->hasError(); }

   QVariant value() const { return _value; }

private:
   static MetaTypeInfo getTypeInfo();

private:
   QVariant _value;
};

template <>
inline bool isVariantNull<QString>(const QString& value)
{
   return value.isNull();
}

template <>
inline bool isVariantNull<QByteArray>(const QByteArray& value)
{
   return value.isNull();
}

template <>
inline bool isVariantNull<QDecimal>(const QDecimal& value)
{
   return value.isNull();
}

template <>
inline bool isVariantNull<QDateTime>(const QDateTime& value)
{
   return value.isNull();
}

template <>
inline bool isVariantNull<QDate>(const QDate& value)
{
   return value.isNull();
}

template <>
inline bool isVariantNull<QTime>(const QTime& value)
{
   return value.isNull();
}

template <>
inline bool isVariantNull<QDateTimeEx>(const QDateTimeEx& value)
{
   return value.isNull();
}

template <>
inline bool isVariantNull<QDateEx>(const QDateEx& value)
{
   return value.isNull();
}

template <>
inline bool isVariantNull<QTimeEx>(const QTimeEx& value)
{
   return value.isNull();
}

//
// A Variant can store any data type as long as it is copyable and assignable.
//
// This is a replacement for QVariant with the advantage that it can store custom data types easily.
//
// Furthermore Variants are comparable! They can therefore be used in views with sorted columns.
//
class QTCOREEX_EXPORT Variant
{
public:

   //
   // Constructors
   //

   Variant() = default;
   Variant(const Variant &other) { assign(other); }
   Variant(Variant &&other) noexcept : _vMap(other._vMap), _object(other._object) { other._vMap = nullptr; other._object = nullptr; }
   Variant(const char *value); // Stored as QString
#ifdef _WIN32
   Variant(const wchar_t *value); // Stored as QString
#endif
   Variant(const ushort *value); // Stored as QString
   ~Variant() { clear(); }

   template <typename T>
   Variant(const T& value) { if (sizeof(Variant) < sizeof(VariantTypeInfo<T>)) _object = new VariantTypeInfo<T>(value); else new(this) VariantTypeInfo<T>(value); }

#ifdef _WIN32
   template <>
#endif
#if QT_VERSION_MAJOR < 6
   Variant(const QLatin1String& value) { static_assert(sizeof(Variant) >= sizeof(VariantTypeInfo<QString>), "VariantTypeInfo<QString> is larger than Variant"); new(this) VariantTypeInfo<QString>(QString(value)); }
#else
   Variant(const QLatin1String& value) { static_assert(sizeof(Variant) < sizeof(VariantTypeInfo<QString>), "VariantTypeInfo<QString> is not larger than Variant"); _object = new VariantTypeInfo<QString>(QString(value)); }
#endif

#ifdef _WIN32
   template <>
#endif
   Variant(const float& value) { if (sizeof(Variant) < sizeof(VariantTypeInfo<double>)) _object = new VariantTypeInfo<double>(value); else new(this) VariantTypeInfo<double>(value); }

#ifdef _WIN32
   template <>
#endif
   Variant(const int& value) { static_assert(sizeof(Variant) >= sizeof(VariantTypeInfo<int>), "VariantTypeInfo<int> is larger than Variant"); new(this) VariantTypeInfo<int>(value); }

#ifdef _WIN32
   template <>
#endif
   Variant(const long& value)
   {
      static_assert(sizeof(Variant) >= sizeof(VariantTypeInfo<long>), "VariantTypeInfo<long> is larger than Variant");
      if (sizeof(long) == sizeof(long long int))
         new(this) VariantTypeInfo<long long int>(value);
      else
         new(this) VariantTypeInfo<int>(value);
   }

#ifdef _WIN32
   template <>
#endif
   Variant(const short& value) { static_assert(sizeof(Variant) >= sizeof(VariantTypeInfo<int>), "VariantTypeInfo<int> is larger than Variant"); new(this) VariantTypeInfo<int>(value); }

#ifdef _WIN32
   template <>
#endif
   Variant(const char& value) { static_assert(sizeof(Variant) >= sizeof(VariantTypeInfo<int>), "VariantTypeInfo<int> is larger than Variant"); new(this) VariantTypeInfo<int>(value); }

#ifdef _WIN32
   template <>
#endif
   Variant(const unsigned int& value) { static_assert(sizeof(Variant) >= sizeof(VariantTypeInfo<unsigned int>), "VariantTypeInfo<int> is larger than Variant"); new(this) VariantTypeInfo<unsigned int>(value); }

#ifdef _WIN32
   template <>
#endif
   Variant(const unsigned long& value)
   {
      static_assert(sizeof(Variant) >= sizeof(VariantTypeInfo<unsigned long>), "VariantTypeInfo<unsigned long> is larger than Variant");
      if (sizeof(long) == sizeof(long long int))
         new(this) VariantTypeInfo<unsigned long long int>(value);
      else
         new(this) VariantTypeInfo<unsigned int>(value);
   }

#ifdef _WIN32
   template <>
#endif
   Variant(const unsigned short& value) { static_assert(sizeof(Variant) >= sizeof(VariantTypeInfo<unsigned int>), "VariantTypeInfo<int> is larger than Variant"); new(this) VariantTypeInfo<unsigned int>(value); }

#ifdef _WIN32
   template <>
#endif
   Variant(const unsigned char& value) { static_assert(sizeof(Variant) >= sizeof(VariantTypeInfo<unsigned int>), "VariantTypeInfo<int> is larger than Variant"); new(this) VariantTypeInfo<unsigned int>(value); }

#ifdef _WIN32
   template <>
#endif
   Variant(const QDateTimeEx& value)
   {
      if (value.minPrecision() > DateTimePart::Hour)
      {
#ifdef Q_PROCESSOR_X86_64
         static_assert(sizeof(Variant) >= sizeof(VariantTypeInfo<QDateEx>), "VariantTypeInfo<QDateEx> is larger than Variant");
         new(this) VariantTypeInfo<QDateEx>(value);
#else
         _object = new VariantTypeInfo<QDateEx>(value);
#endif
      }
      else if (value.maxPrecision() < DateTimePart::Day && value.maxPrecision() != DateTimePart::None)
      {
#ifdef Q_PROCESSOR_X86_64
         static_assert(sizeof(Variant) >= sizeof(VariantTypeInfo<QTimeEx>), "VariantTypeInfo<QTimeEx> is larger than Variant");
         new(this) VariantTypeInfo<QTimeEx>(value);
#else
         _object = new VariantTypeInfo<QTimeEx>(value);
#endif
      }
      else
      {
#ifdef Q_PROCESSOR_X86_64
         static_assert(sizeof(Variant) >= sizeof(VariantTypeInfo<QDateTimeEx>), "VariantTypeInfo<QDateTimeEx> is larger than Variant");
         new(this) VariantTypeInfo<QDateTimeEx>(value);
#else
         _object = new VariantTypeInfo<QDateTimeEx>(value);
#endif
      }
   }

#ifdef _WIN32
   template <>
#endif
   Variant(const QVariant& variant);

   //
   // Assignment
   //

   Variant& operator=(const Variant& other) { clear(); assign(other); return *this; }

   void clear(); // Reset

   //
   // Type handling
   //

   static const Variant null;

   bool isNull() const { return _vMap == nullptr && _object == nullptr; }
   bool isValueNull() const { return (_vMap == nullptr && _object == nullptr) || metaObject()->isNull(); }
   const std::type_info& type() const { if (_vMap == nullptr && _object == nullptr) return typeid(void); return metaObject()->typeInfo(); }

   template<typename T>
   bool hasConversionTo() const;

   static bool canConvert(const std::type_info& from, const std::type_info& to);

   //
   // Comparison
   //

   bool        operator==(const Variant& other) const;
   inline bool operator!=(const Variant& other) const { return !operator==(other); }
   bool        operator<(const Variant& other) const;
   inline bool operator>(const Variant& other) const { return other.operator <(*this); }

   bool match(const Variant& other, Qt::MatchFlags flags = {}) const;

   //
   // Conversions
   // GCC needs the specialized to<> functions to be defined outside class scope!
   //

   template <typename T>
   T to(bool* ok = nullptr, const QLocaleEx& locale = defaultLocale()) const; // Raises a debug assertion if the conversion fails

   template <typename T>
   T value(const T& defaultValue, const QLocaleEx& locale = defaultLocale()) const // Does not raise a conversion failure
   {
      bool ok;
      T result = to<T>(&ok, locale);
      if (!ok)
         result = defaultValue;
      return result;
   }

   template <typename T>
   T fromQVariant(bool *ok = nullptr) const;

   // Convenience functions

   QString     toString(bool *ok = nullptr) const { return toString(ok, defaultLocale()); }
   QString     toString(const QLocaleEx& locale) const { return toString(nullptr, locale); }
   QString     toString(bool *ok, const QLocaleEx& locale) const;
   QText       toText(bool *ok = nullptr) const { return toText(ok, defaultLocale()); }
   QText       toText(const QLocaleEx& locale) const { return toText(nullptr, locale); }
   QText       toText(bool *ok, const QLocaleEx& locale) const;
   bool        toBool(bool *ok = nullptr, const QLocaleEx& locale = defaultLocale()) const;
   int         toInt(bool *ok = nullptr, const QLocaleEx& locale = defaultLocale()) const;
   uint        toUInt(bool *ok = nullptr, const QLocaleEx& locale = defaultLocale()) const;
   qint64      toInt64(bool *ok = nullptr, const QLocaleEx& locale = defaultLocale()) const;
   quint64     toUInt64(bool *ok = nullptr, const QLocaleEx& locale = defaultLocale()) const;
   double      toDouble(bool *ok = nullptr, const QLocaleEx& locale = defaultLocale()) const;
   QDecimal    toDecimal(bool *ok = nullptr, const QLocaleEx& locale = defaultLocale()) const;
   QByteArray  toByteArray(bool *ok = nullptr, const QLocaleEx& locale = defaultLocale()) const;
   QDate       toDate(bool *ok = nullptr) const { return toDate(ok, defaultLocale()); }
   QDate       toDate(bool *ok, const QLocaleEx& locale) const;
   QDateEx     toDateEx(bool *ok = nullptr, const QLocaleEx &locale = defaultLocale()) const;
   QTime       toTime(bool *ok = nullptr) const { return toTime(ok, defaultLocale()); }
   QTime       toTime(bool *ok, const QLocaleEx &locale) const;
   QTimeEx     toTimeEx(bool *ok = nullptr, const QLocaleEx &locale = defaultLocale()) const;
   QDateTime   toDateTime(bool *ok = nullptr) const { return toDateTime(ok, defaultLocale()); }
   QDateTime   toDateTime(bool *ok, const QLocaleEx &locale) const;
   QDateTimeEx toDateTimeEx(bool *ok = nullptr, const QLocaleEx &locale = defaultLocale()) const;
   QVariant    toQVariant() const;

   operator QVariant() const { return toQVariant(); }

   //
   // Serialization
   //

   QByteArray  serialized() const; // Serialized representation of the variant
   bool        deserialize(const QByteArray& serVariant); // Restores the variant from the serialized information

   friend QTCOREEX_EXPORT SerStream& operator<<(SerStream &stream, const Variant &value);
   friend QTCOREEX_EXPORT SerStream& operator>>(SerStream &stream, Variant &value);

   //
   // Registration
   //

   template <typename T>
   static bool registerType(const QByteArray& portableName = QByteArray()); // Only needed for serialization

   template <typename L, typename R>
   static bool registerConversion();

   template <typename T>
   static bool registerQVariantConversion();

   template <typename T>
   static bool registerToQVariantConversion();

private:
   template <class M>
   static VariantMeta* constructor(void* memory) { if (memory == nullptr) return new M; return new(memory) M; }

   template <class M>
   static void destructor(VariantMeta* object) { delete object; }

   template <typename L, typename R>
   static bool convWrapper(void* lhs, const void* rhs, const QLocaleEx& locale) { return convert(*(L*)lhs, *(const R*)rhs, locale); }

private:
   void assign(const Variant& other) { if (other._vMap != nullptr) reinterpret_cast<const VariantMeta*>(&other)->clone(this); else if (other._object != nullptr) _object = other._object->clone(); }

   VariantMeta*         metaObject() { return _vMap != nullptr ? reinterpret_cast<VariantMeta*>(this) : _object; }
   const VariantMeta*   metaObject() const { return _vMap != nullptr ? reinterpret_cast<const VariantMeta*>(this) : _object; }

   static void _registerType(const std::type_info& typeId, VariantMeta* (*construct)(void*), void (*destroy)(VariantMeta*), int size, QByteArray portableName);
   static void _registerConversion(const std::type_info& leftType, const std::type_info& rightType, GenericConversion genConv);

   void* _vMap = nullptr;
   VariantMeta* _object = nullptr;
};

template<typename T>
bool Variant::hasConversionTo() const
{
   const auto mo = metaObject();

   return mo && mo->hasConversion(typeid(T));
}

// Specializations of to<>() template function (GCC needs to define these functions outside class scope)

template <>
QTCOREEX_EXPORT long Variant::to<long>(bool* ok, const QLocaleEx& locale) const;

template <>
QTCOREEX_EXPORT short Variant::to<short>(bool* ok, const QLocaleEx& locale) const;

template <>
QTCOREEX_EXPORT char Variant::to<char>(bool* ok, const QLocaleEx& locale) const;

template <>
QTCOREEX_EXPORT unsigned long Variant::to<unsigned long>(bool* ok, const QLocaleEx& locale) const;

template <>
QTCOREEX_EXPORT unsigned short Variant::to<unsigned short>(bool* ok, const QLocaleEx& locale) const;

template <>
QTCOREEX_EXPORT unsigned char Variant::to<unsigned char>(bool* ok, const QLocaleEx& locale) const;

template <>
QTCOREEX_EXPORT QString Variant::to<QString>(bool* ok, const QLocaleEx& locale) const;

template <>
QTCOREEX_EXPORT QText Variant::to<QText>(bool* ok, const QLocaleEx& locale) const;

template <>
QTCOREEX_EXPORT QVariant Variant::to<QVariant>(bool* ok, const QLocaleEx& locale) const;

// Implementation of the conversion template functions

template <typename T>
T Variant::to(bool* ok, const QLocaleEx& locale) const
{
   if (ok)
      *ok = true;

   const auto mo = metaObject();

   if (const auto vType = dynamic_cast<const VariantTypeInfo<T>*>(mo))
      return vType->value();

   VariantTypeInfo<T> other;

   if (mo && !mo->convertInto(other.valuePointer(), typeid(T), locale))
   {
      if (ok)
      {
         *ok = false;
      }
      else
      {
         auto portableName = mo->portableName();
         rpsError("Unable to convert Variant from %s to %s.", portableName.data(), portable_type_name(typeid(T)).data());
      }
   }

   return other.value();
}

template <typename T>
T Variant::fromQVariant(bool *ok) const
{
   if (const auto qvType = dynamic_cast<const VariantTypeInfo<QVariant>*>(metaObject()))
   {
      QVariant qVariant = qvType->value();

      if (qVariant.canConvert<T>())
      {
         if (ok)
            *ok = true;
         return qVariant.value<T>();
      }
   }

   return to<T>(ok);
}

// Standard conversion for QVariant supported types

template <typename T>
bool convertToQVariant(void* lhs, const void* rhs, const QLocaleEx& locale)
{
   Q_UNUSED(locale);

   ((QVariant*)lhs)->setValue(*(const T*)rhs);
   return true;
}

template <typename T>
bool convertFromQVariant(void* lhs, const void* rhs, const QLocaleEx& locale)
{
   Q_UNUSED(locale);

   *(T*)lhs = ((const QVariant*)rhs)->value<T>();
   return ((const QVariant*)rhs)->canConvert<T>();
}

// Implementation of the registering template functions

template <typename T>
bool Variant::registerType(const QByteArray &portableName)
{
   _registerType(typeid(T), &constructor<VariantTypeInfo<T> >, &destructor<VariantTypeInfo<T> >, sizeof(VariantTypeInfo<T>), portableName);
   return true;
}

template <typename L, typename R>
bool Variant::registerConversion()
{
   _registerConversion(typeid(L), typeid(R), &convWrapper<L, R>);
   return true;
}

template <typename T>
bool Variant::registerQVariantConversion()
{
   _registerConversion(typeid(QVariant), typeid(T), &convertToQVariant<T>);
   _registerConversion(typeid(T), typeid(QVariant), &convertFromQVariant<T>);
   return true;
}

template <typename T>
bool Variant::registerToQVariantConversion()
{
   _registerConversion(typeid(QVariant), typeid(T), &convertToQVariant<T>);
   return true;
}

// QVariant registration

Q_DECLARE_METATYPE(Variant)

QTCOREEX_EXPORT QDataStream& operator<<(QDataStream& out, const Variant& myObj);
QTCOREEX_EXPORT QDataStream& operator>>(QDataStream& in, Variant& myObj);

#endif // VARIANT_H
