#pragma once

#include "qtcoreex_global.h"
#include <QString>

// The return value indicates how good the two strings matches.
// The indicator is only valid for strings with less than 0x8000 characters.
// The length of the longest matching substring (case insensitive) of both strings is returned modified by the number of matching case sensitive characters
// and if the match is from the beginning of both strings.
QTCOREEX_EXPORT int getCorrelationIndicator(const QString& string1, const QString& string2);

// QText is for presenting texts to users.
//
// The standard comparison operators compare local aware and case insensitive by default.
//
class QTCOREEX_EXPORT QText : public QString
{
public:
   QText() {}
   ~QText() {}
   QText(const QText& text) : QString(text) {}
   QText(const QString& str) : QString(str) {}
   QText(const QLatin1String& str) : QString(str) {}
#ifndef QT_NO_CAST_FROM_ASCII
   explicit QText(const char* str) : QString(str) {}
#endif

   QText& operator=(const QText& rhs) { QString::operator=(rhs); return *this; }
   QText& operator=(const QString& rhs) { QString::operator=(rhs); return *this; }
   QText& operator=(const QLatin1String& rhs) { QString::operator=(rhs); return *this; }
#ifndef QT_NO_CAST_FROM_ASCII
   QText& operator=(const char* rhs) { QString::operator=(rhs); return *this; }
#endif

   bool operator==(const QText& rhs) const { return compare(rhs) == 0; }
   bool operator!=(const QText& rhs) const { return compare(rhs) != 0; }
   bool operator==(const QString& rhs) const { return compare(rhs) == 0; }
   bool operator!=(const QString& rhs) const { return compare(rhs) != 0; }

   bool operator<(const QText& rhs) const { return compare(rhs) < 0; }
   bool operator<=(const QText& rhs) const { return compare(rhs) <= 0; }
   bool operator>(const QText& rhs) const { return compare(rhs) > 0; }
   bool operator>=(const QText& rhs) const { return compare(rhs) >= 0; }
   bool operator<(const QString& rhs) const { return compare(rhs) < 0; }
   bool operator<=(const QString& rhs) const { return compare(rhs) <= 0; }
   bool operator>(const QString& rhs) const { return compare(rhs) > 0; }
   bool operator>=(const QString& rhs) const { return compare(rhs) >= 0; }

   int compare(const QString& str, Qt::CaseSensitivity cs = Qt::CaseInsensitive) const;

   /*
   ** The following functions do not rely on comparison (greater/less) but on equality.
   ** Therefore the standard functions of QString are sufficient.

   bool contains(const QText& str, Qt::CaseSensitivity cs = Qt::CaseInsensitive) const;
   bool contains(QChar ch, Qt::CaseSensitivity cs = Qt::CaseInsensitive) const;
   bool contains(QRegExp& rx) const;

   bool endsWith(const QText& s, Qt::CaseSensitivity cs = Qt::CaseInsensitive) const;
   bool endsWith(const QLatin1String& s, Qt::CaseSensitivity cs = Qt::CaseInsensitive) const;
   bool endsWith(const QChar& c, Qt::CaseSensitivity cs = Qt::CaseInsensitive) const;

   bool indexOf(const QText& str, int from = 0, Qt::CaseSensitivity cs = Qt::CaseInsensitive) const;
   bool indexOf(const QLatin1String& str, int from = 0, Qt::CaseSensitivity cs = Qt::CaseInsensitive) const;
   bool indexOf(QChar ch, int from = 0, Qt::CaseSensitivity cs = Qt::CaseInsensitive) const;
   bool indexOf(const QRegExp& rx, int from = 0) const;
   bool indexOf(QRegExp& rx, int from = 0) const;

   bool lastIndexOf(const QText& str, int from = -1, Qt::CaseSensitivity cs = Qt::CaseInsensitive) const;
   bool lastIndexOf(const QLatin1String& str, int from = -1, Qt::CaseSensitivity cs = Qt::CaseInsensitive) const;
   bool lastIndexOf(QChar ch, int from = -1, Qt::CaseSensitivity cs = Qt::CaseInsensitive) const;
   bool lastIndexOf(const QRegExp& rx, int from = -1) const;
   bool lastIndexOf(QRegExp& rx, int from = -1) const;

   QText& remove(int position, int n);
   QText& remove(QChar ch, Qt::CaseSensitivity cs = Qt::CaseInsensitive);
   QText& remove(const QText& str, Qt::CaseSensitivity cs = Qt::CaseInsensitive);
   QText& remove(const QRegExp& rx, Qt::CaseSensitivity cs = Qt::CaseInsensitive);

   QText& replace(int position, int n, const QText& after) { QString::replace(position, n, after); return *this; }
   QText& replace(int position, int n, const QChar* unicode, int size) { QString::replace(position, n, unicode, size); return *this; }
   QText& replace(int position, int n, QChar after) { QString::replace(position, n, after); return *this; }
   QText& replace(const QText& before, const QText& after, Qt::CaseSensitivity cs = Qt::CaseInsensitive);
   QText& replace(const QChar* before, int blen, const QChar* after, int alen, Qt::CaseSensitivity cs = Qt::CaseInsensitive);
   QText& replace(QChar ch, const QText& after, Qt::CaseSensitivity cs = Qt::CaseInsensitive);
   QText& replace(QChar before, QChar after, Qt::CaseSensitivity cs = Qt::CaseInsensitive);
   QText& replace(const QLatin1String& before, const QLatin1String& after, Qt::CaseSensitivity cs = Qt::CaseInsensitive);
   QText& replace(const QLatin1String& before, const QText& after, Qt::CaseSensitivity cs = Qt::CaseInsensitive);
   QText& replace(const QText& before, const QLatin1String& after, Qt::CaseSensitivity cs = Qt::CaseInsensitive);
   QText& replace(QChar c, const QLatin1String& after, Qt::CaseSensitivity cs = Qt::CaseInsensitive);
   QText& replace(const QRegExp& rx, const QText& after);

   QStringList split(const QText& sep, SplitBehavior behaviour = KeepEmptyParts, Qt::CaseSensitivity cs = Qt::CaseInsensitive) const;

   bool startsWith(const QText& s, Qt::CaseSensitivity cs = Qt::CaseInsensitive) const;
   bool startsWith(const QLatin1String& s, Qt::CaseSensitivity cs = Qt::CaseInsensitive) const;
   bool startsWith(const QChar& c, Qt::CaseSensitivity cs = Qt::CaseInsensitive) const;
   */
};

Q_DECLARE_TYPEINFO(QText, Q_MOVABLE_TYPE);
