#ifndef BASIC_EXCEPTIONS_H
#define BASIC_EXCEPTIONS_H

#include <QException>
#include <QString>

class QBasicException : public QException
{
public:
   QBasicException(const QString& message) : _message(message) {}
   virtual ~QBasicException() = default;

   void raise() const override { throw *this; }
   QBasicException *clone() const override { return new QBasicException(*this); }

   const QString& message() const { return _message; }

private:
   QString _message;
};

class QNotImplementedException : public QBasicException
{
public:
   QNotImplementedException() : QBasicException("Not implemented") {}
   QNotImplementedException(const QString& message) : QBasicException(message) {}
   ~QNotImplementedException() override = default;

   void raise() const override { throw *this; }
   QNotImplementedException *clone() const override { return new QNotImplementedException(*this); }
};

class QNullPointerException : public QBasicException
{
public:
   QNullPointerException() : QBasicException("Null pointer exception") {}
   QNullPointerException(const QString& message) : QBasicException(message) {}
   ~QNullPointerException() override = default;

   void raise() const override { throw *this; }
   QNullPointerException *clone() const override { return new QNullPointerException(*this); }
};

#endif // BASIC_EXCEPTIONS_H
