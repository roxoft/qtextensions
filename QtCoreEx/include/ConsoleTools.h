#ifndef CONSOLETOOLS_H
#define CONSOLETOOLS_H

#include "qtcoreex_global.h"
#include <QMap>

class ArgDef
{
public:
   ArgDef() = default;
   ~ArgDef() = default;

   QString _optName; // If empty its a value
   QString _valueName; // If empty there is no value expected
   bool _isMandatory = false;
   bool _isMulti = false;
   QString _description; // If description is empty this is a hidden parameter
   QList<ArgDef> _options;
};

QTCOREEX_EXPORT QStringList appArguments(const QList<ArgDef>& argDefs, const QStringList& cmdLineArgs, QMultiMap<QString, QString>& serviceArgs);

#if defined(Q_OS_UNIX)
   typedef void (*sighandler_t)(int);

   QTCOREEX_EXPORT sighandler_t handle_signal(int signum, sighandler_t signalhandler);

   QTCOREEX_EXPORT void terminateSignalHandler(int sig);
#endif

QTCOREEX_EXPORT bool installConsoleEventHandler();

QTCOREEX_EXPORT void consoleOut(const QString &message, bool error = false);

QTCOREEX_EXPORT bool exitApplication(); // Exits the application through the quit() slot in a thread save way

#endif // CONSOLETOOLS_H
