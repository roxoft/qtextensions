#ifndef QRATIONAL_H
#define QRATIONAL_H

#include "qtcoreex_global.h"
#include <QString>
#include "QLocaleEx.h"
#include "QDecimal.h"

// Find the greates common divisor (GDC) using the Euclidean algorithm
QTCOREEX_EXPORT qint64 greatestCommonDivisor(qint64 numerator, qint64 denominator);

// QRational is for storing and calculating rational numbers.
// The advantage is that there are no rounding errors during arithmetic operations as long as the numerator and the denominator can be kept in a 64bit signed integer value.
// If the numerator or the denominator exceeds 64bit an attempt is made to reduce the rational with prime numbers.
//
class QTCOREEX_EXPORT QRational
{
public:
   QRational() : _numerator(0LL), _denominator(1LL) {}
   QRational(const QRational& other) : _numerator(other._numerator), _denominator(other._denominator) {}
   ~QRational() {}

   QRational(qint64 numerator, qint64 denominator = 1LL) : _numerator(numerator), _denominator(denominator) {}
   QRational(int numerator, int denominator = 1) : _numerator((qint64)numerator), _denominator((qint64)denominator) {}
   QRational(const char* szNumber); // The number must be in "C" locale
   explicit QRational(double number, qint64 base = 10LL);
   QRational(const QString& number, const QLocaleEx& locale = defaultLocale());
   QRational(QDecimal decimal);

   QRational& operator=(const QRational& other) { _numerator = other._numerator; _denominator = other._denominator; return *this; }

   bool isValid() const { return _denominator > 0LL; }

   qint64 numerator() const { return _numerator; }
   qint64 denominator() const { return _denominator; }

   QString toString(const QLocaleEx& locale = defaultLocale()) const { return toDecimalString(-18, false, locale); }
   QString toFractionString() const { return QString::fromLatin1("%1/%2").arg(_numerator).arg(_denominator); }

   bool     fromDecimalString(const QString& strNumber, QChar zeroDigit = QLatin1Char('0'), QChar decimalSeparator = QLatin1Char('.'), QChar thousandsSeparator = QLatin1Char(','), QChar negativeSign = QLatin1Char('-')); // A NULL string has no effect. An empty string sets the rational to 0/1.
   QString  toDecimalString(int precision, bool useGroupSeparators, const QLocaleEx& locale = defaultLocale()) const;

   int            toInt(bool* ok = NULL) const;
   unsigned int   toUInt(bool* ok = NULL) const;
   QDecimal       toDecimal(bool* ok = NULL) const;

   bool operator==(const QRational& other) const;
   bool operator!=(const QRational& other) const { return !operator==(other); }
   bool operator<(const QRational& other) const;
   bool operator>(const QRational& other) const { return other < *this; }
   bool operator<=(const QRational& other) const { return !operator>(other); }
   bool operator>=(const QRational& other) const { return !operator<(other); }

   int compare(const QRational& other) const;

   QRational& operator*=(qint64 number);
   QRational& operator/=(qint64 number);

   QRational& operator+=(const QRational& other);
   QRational& operator-=(const QRational& other);
   QRational& operator*=(const QRational& other);
   QRational& operator/=(const QRational& other);

   QRational operator+(const QRational& other) const { return QRational(*this) += other; }
   QRational operator-(const QRational& other) const { return QRational(*this) -= other; }
   QRational operator*(const QRational& other) const { return QRational(*this) *= other; }
   QRational operator/(const QRational& other) const { return QRational(*this) /= other; }

   QRational operator-() const { return QRational(-_numerator, _denominator); }

   void reduce(); // Reduces the nominator and denominator completely using prime numbers

   // Transforms the fraction so that the denominator is a magnitude of base
   // The precision limits the magnitude. A precision of 2 with a base of 10 limits the denominator to 100. -1 uses the available precision.
   // The result is mathematically rounded and completely reduced by base.
   // The remainder of the operaton is returned.
   QRational rebase(qint64 base, int precision = -1);

private:
   qint64 _numerator;
   qint64 _denominator;
};

inline SerStream& operator<<(SerStream& stream, const QRational& rational) { stream << rational.numerator() << rational.denominator(); return stream; }
QTCOREEX_EXPORT SerStream& operator>>(SerStream& stream, QRational& rational);

#endif // QRATIONAL_H
