#ifndef QLOOKUP_H
#define QLOOKUP_H

#include <QVector>

template<typename T, class L>
class QLookup
{
public:
   QLookup(const L* list, T (*keyAt)(const L*, int)) : _list(list), _keyAt(keyAt) {}
   ~QLookup() {}

   void reset() { _lookup.clear(); }

   int indexOf(const T& key)
   {
      return binaryFind(key);
   }

   bool contains(const T& key)
   {
      return binaryFind(key) != -1;
   }

   int findOrAdd(const T& key)
   {
      auto index = binaryFindFirstAfter(key);

      if (index > 0 && _keyAt(_list, _lookup[index - 1]) == key)
         return _lookup[index - 1];

      _lookup.insert(index, _lookup.count());

      return _lookup[index];
   }

   int take(const T& key)
   {
      auto index = binaryFindFirstOrAfter(key);

      if (index == _lookup.count())
         return -1;

      if (_keyAt(_list, _lookup[index]) != key)
         return -1;

      index = _lookup.takeAt(index);

      for (auto&& l : _lookup)
      {
         if (l > index)
            l--;
      }

      return index;
   }

   const QVector<int>& lookup()
   {
      if (_lookup.isEmpty())
      {
         _lookup.resize(_list->count());
         for (int i = _lookup.count(); i--; )
         {
            _lookup[i] = i;
         }

         std::stable_sort(_lookup.begin(), _lookup.end(), *this);
      }
      return _lookup;
   }

   bool operator()(int left, int right) const { return _keyAt(_list, left) < _keyAt(_list, right); } // For sorting

private:
   // Returns the index of the _list!!!
   int binaryFind(const T& key)
   {
      auto index = binaryFindFirstOrAfter(key);

      if (index == _lookup.count())
         return -1;

      index = _lookup[index];

      if (_keyAt(_list, index) != key)
         return -1;

      return index;
   }

   // Returns the index of the _lookup!!!
   int binaryFindFirstOrAfter(const T& key)
   {
      auto begin = 0;
      auto end = lookup().count();

      while (begin < end)
      {
         auto i = begin + (end - begin) / 2;

         if (_keyAt(_list, _lookup[i]) < key)
            begin = i + 1;
         else
            end = i;
      }

      return end;
   }

   // Returns the index of the _lookup!!!
   int binaryFindFirstAfter(const T& key)
   {
      auto begin = 0;
      auto end = lookup().count();

      while (begin < end)
      {
         auto i = begin + (end - begin) / 2;

         if (key < _keyAt(_list, _lookup[i]))
            end = i;
         else
            begin = i + 1;
      }

      return end;
   }

private:
   const L* _list;
   T (*_keyAt)(const L*, int);
   QVector<int> _lookup;
};

#endif // QLOOKUP_H
