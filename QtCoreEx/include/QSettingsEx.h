#ifndef QSETTINGSEX_H
#define QSETTINGSEX_H

#include "qtcoreex_global.h"
#include <QSharedData>
#include "Variant.h"
#include <QList>
#include <QStringList>
#include "SmartPointer.h"

class QSettingsExBase;

// Smart pointer to access settings derived from QSettingsExBase
class QTCOREEX_EXPORT QSettingsEx : public bp<QSettingsExBase>
{
public:
   enum Scope { NoScope = 0, SystemScope = 1, UserScope = 2, ApplicationScope = 4, DefaultScope = SystemScope | UserScope, AllScopes = SystemScope | UserScope | ApplicationScope };

public:
   QSettingsEx();
   QSettingsEx(QSettingsExBase* impl);
   QSettingsEx(const QSettingsEx& other);
   ~QSettingsEx();

   QSettingsEx& operator=(const QSettingsEx& other);

   int   beginGroup(const QString &path); // Switches the current group to path. Returns the number of subgroups contained in path
   bool  endGroup(int depth = 1); // Switches the current group depth groups upwards. Returns true if there were enough levels.
   void  endAllGroups(); // Switches the current group to the root group

   Variant        value(const QString &key, const Variant &defaultValue = Variant()) const;
   QList<Variant> values(const QString &key) const; // Returns an array of values
   QStringList    stringValues(const QString& key) const;
   void           setValue(const QString &key, const Variant &value);
   void           addValue(const QString &key, const Variant &value); // For arrays of values

   QString  remark(const QString &key) const;
   void     setRemark(const QString &key, const QString &remark);

   QStringList keys(const QString &path = QString()) const; // Returns all unique keys in the specified path of the current group (not their subkeys)
};

// Base class for storing key/value pairs in a hierarchical way.
//
// Use QSettingsFile to access ini files.
//
// QSettingsExBase provides a general interface to access the values. See also QSettings.
//
class QTCOREEX_EXPORT QSettingsExBase : public QSharedData
{
   Q_DISABLE_COPY(QSettingsExBase)
   friend class Setting;

public:
   QSettingsExBase();
   virtual ~QSettingsExBase();

   // Open a data source to work with. The Application ID is used to identify the source and is usually "[OrganizationName/]ApplicationName".

   virtual bool open(const QString &applicationId, QSettingsEx::Scope scope = QSettingsEx::DefaultScope) = 0;
   virtual bool isOpen() const = 0;

   // Settings

   bool isCaseSensitive() const { return _caseSensitivity == Qt::CaseSensitive; }
   void setIsCaseSensitive(bool caseSensitive) { _caseSensitivity = caseSensitive ? Qt::CaseSensitive : Qt::CaseInsensitive; }

   QChar keySeparator() const { return _keySeparator; }
   void setKeySeparator(QChar separator) { _keySeparator = separator; }

   // Group handling

   bool isGroup(const QString &path) const; // Returns true if the path contains further keys

   int   beginGroup(const QString &path); // Switches the current group to path. Returns the number of subgroups contained in path
   int   addGroup(const QString& path); // Adds a new group to the current settings. Returns the number of subgroups contained in path
   bool  endGroup(int depth = 1); // Switches the current group depth groups upwards. Returns true if there were enough levels.
   void  endAllGroups() { _currentSetting = _currentSetting->root(); } // Switches the current group to the root group

   QString absolutePath(const QString &key = QString()) const;

   // Key/Value pairs

   Variant        value(const QString &key, const Variant &defaultValue = Variant()) const;
   QList<Variant> values(const QString &key) const; // Returns an array of values
   QStringList    stringValues(const QString& key) const;
   void           setValue(const QString &key, const Variant &value);
   void           addValue(const QString &key, const Variant &value); // For arrays of values

   QString  remark(const QString &key) const;
   void     setRemark(const QString &key, const QString &remark);

   QStringList keys(const QString &path = QString()) const; // Returns all unique keys in the specified path of the current group (not their subkeys)

   void remove(const QString &key) { _currentSetting->remove(key); } // An empty string removes all entries of the current group (if the current group is empty nothing is removed)
   void removeAll() { _currentSetting->remove(QString()); } // Removes all settings from the current group

   // Load and save the key/value pairs

   bool isChanged() const { return _currentSetting->root()->isChanged(); }

   void sync(); // Saves the current state if there were changes otherwise loads the data source again

   virtual void load() = 0;
   virtual void save() = 0;

public:
   //
   // Underlying settings tree
   //

   class QTCOREEX_EXPORT Setting
   {
   public:
      Setting(QSettingsExBase* base) : _base(base) {}
      virtual ~Setting() { qDeleteAll(_settings); }

      bool isEmpty() const { return _name.isEmpty() && _remark.isEmpty() && _settings.isEmpty(); }

      bool isChanged() const { return _changed; }
      void setIsChanged(bool changed);

      Setting* parent() { return _parent; }
      Setting* root() { if (_parent) return _parent->root(); return this; }

      bool isRoot() const { return _parent == nullptr; }
      bool isGroup() const { return _valueList.isEmpty(); }

      const QString& name() const { return _name; }

      const Variant& value() const;
      const Variant& value(int layer) const;
      void           setValue(const Variant &value, int layer = 0);

      const Variant& value(const QString &path, const Variant &defaultValue = Variant()) const { const Setting *s = const_setting(path); if (s) return s->value(); return defaultValue; }

      const QString& remark() const { return _remark; }
      void           setRemark(const QString &remark) { if (remark != _remark) { _remark = remark; setIsChanged(true); } }

      QStringList    keys() const; // List of unique keys

      Setting* setting(const QString &path, bool *created = nullptr);
      Setting* setting(const QStringList &path, bool *created = nullptr); // Returns the first setting found or a newly created one
      const Setting* setting(const QString &path) const { return const_setting(path); }
      const Setting* const_setting(const QString &path) const;
      const Setting* const_setting(const QStringList &path) const; // Returns the first setting found or a nullptr

      QList<Setting *>        settings(const QString &path);
      QList<Setting *>        settings(const QStringList &path);
      QList<const Setting *>  settings(const QString &path) const { return const_settings(path); }
      QList<const Setting *>  const_settings(const QString &path) const;
      QList<const Setting *>  const_settings(const QStringList &path) const;
      Setting*                addSetting(const QString &path);
      Setting*                addSetting(const QStringList &path);

      const QList<Setting *>& settings() const { return _settings; }

      void remove(const QString &path); // All settings matching path are removed (an empty path removes all settings)
      void remove(const QStringList &path);

      void detach(); // Removes itself from the parents list (must be deleted manually!)

      void clearSettings(); // Remove all settings. Sets the change marker.

      void clear(); // Clears everything including the name and the changed marker!

      Setting* next(Setting *child = nullptr); // Returns the next setting regardless of the hierarchy (group or item). child == nullptr starts with the first setting. Returns nullptr after the last setting.

      QString fullPath() const;

   protected:
      virtual Setting* newSetting(QSettingsExBase* base) const { return new Setting(base); }

   private:
      QSettingsExBase*  _base = nullptr; // Pointer to the base instance for configuration settings
      QString           _name; // Name of the setting or group
      QString           _remark; // Remark (if more than one layer (scope) has a remark this is the remark of the lowest layer)
      QList<Variant>    _valueList; // value for each layer (scope)
      QList<Setting *>  _settings; // Child settings (this is then a group)
      bool              _changed = false; // If the setting has changed
      Setting*          _parent = nullptr; // Parent (group) of the setting
   };

   Setting* currentSetting() { return _currentSetting; }
   const Setting* const_currentSetting() const { return _currentSetting; }

   Setting* setting(const QString &path) { return _currentSetting->setting(path); }
   const Setting* const_setting(const QString &path) const { return _currentSetting->const_setting(path); }

protected:
   QSettingsExBase(Setting *root);

   void clear() { _currentSetting = _currentSetting->root(); _currentSetting->clear(); }
   void setIsChanged(bool changed) { _currentSetting->setIsChanged(changed); }

   QStringList normalizedKey(const QString& key) const;

private:
   Setting* _currentSetting = nullptr; // Must never be nullptr
   Qt::CaseSensitivity  _caseSensitivity = Qt::CaseInsensitive; // Case sensitivity of the names
   QChar _keySeparator = u'/';
};

#endif // QSETTINGSEX_H
