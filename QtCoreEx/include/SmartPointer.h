#pragma once

#include "basic_exceptions.h"

/*
 The template class bp can be used to guard an instance of a class. The instance is destroyed when the last bp is destroyed.

 The only requirement is access to a public mutable variable ref of type QAtomicInt.
 This includes all classes derived from QSharedData.

 Be careful to not introduce recursion when using bp inside the guarded classes!

 Use bp_new to create guarded instances.
*/
template <class T>
class bp
{
public:

   // Construction

   bp() = default;
   bp(const bp& other) noexcept : _object(other._object) { bind(); }
   bp(bp&& other) noexcept : _object(other._object) { other._object = nullptr; }
   bp(T* object) noexcept : _object(object) { bind(); }
   template <class C>
   bp(const bp<C>& other) noexcept : _object(dynamic_cast<T*>(other.data())) { bind(); }

   // Destruction

   ~bp() { release(); }

   // Assignment
   bp& operator=(const bp& other) { if (other._object != _object) { if (other._object) other._object->ref.ref(); release(); _object = other._object; } return *this; }
   bp& operator=(bp&& other) noexcept { qSwap(_object, other._object); return *this; }
   bp& operator=(T* ptr) { if (ptr != _object) { if (ptr) ptr->ref.ref(); release(); _object = ptr; } return *this; }
   template <class C>
   bp& operator=(const bp<C>& other) { return operator=(dynamic_cast<T*>(other.data())); }

   // State information

   bool isNull() const { return _object == nullptr; }
   bool isValid() const { return _object != nullptr; }

   template <class C>
   bool is() const { return dynamic_cast<const C*>(_object); }

   // Operations

   operator bool() const { return _object != nullptr; }
   bool operator!() const { return _object == nullptr; }

   bool operator==(const bp& other) const { return _object == other._object; }
   bool operator!=(const bp& other) const { return _object != other._object; }
   bool operator<(const bp& other) const { return _object < other._object; }
   bool operator>(const bp& other) const { return _object > other._object; }

   bool operator==(const T* ptr) const { return _object == ptr; }
   bool operator!=(const T* ptr) const { return _object != ptr; }
   bool operator<(const T* ptr) const { return _object < ptr; }
   bool operator>(const T* ptr) const { return _object > ptr; }

   void reset() { release(); }

   // Pointer access

   operator T* () const { return _object; }

   T* operator->() const { if (!_object) throw QNullPointerException(); return _object; }
   T& operator*() const { if (!_object) throw QNullPointerException(); return *_object; }

   T* data() const { return _object; }
   const T* constData() const { return _object; }

   // Conversions

   template <class C>
   C* as() const { return dynamic_cast<C*>(_object); }

private:
   void bind()
   {
      if (_object)
         _object->ref.ref();
   }

   void release()
   {
      if (_object && !_object->ref.deref())
         delete _object;
      _object = nullptr;
   }

   T* _object = nullptr;
};

template <class T, typename ... P>
bp<T> bp_new(P&& ... p) { return new T(p...); }

/*
 sp works similar to bp except that it creates a copy of a shared instance on write access (COW).

 Use sp_new to create guarded instances of a class.
*/
template <class T>
class sp
{
public:

   // Construction

   sp() = default;
   sp(const sp& other) noexcept : _object(other._object) { bind(); }
   sp(sp&& other) noexcept : _object(other._object) { other._object = nullptr; }
   sp(T* object) noexcept : _object(object) { bind(); }
   template<class C>
   sp(const sp<C>& other) noexcept { other.copyTo(*this); }

   // Destruction

   ~sp() { release(); }

   // Assignment
   sp& operator=(const sp& other) { if (other._object != _object) { if (other._object) other._object->ref.ref(); release(); _object = other._object; } return *this; }
   sp& operator=(sp&& other) noexcept { qSwap(_object, other._object); return *this; }
   sp& operator=(T* ptr) { if (ptr != _object) { if (ptr) ptr->ref.ref(); release(); _object = ptr; } return *this; }
   template <class C>
   sp& operator=(const sp<C>& other) { other.copyTo(*this); return *this; }

   template<class C>
   void copyTo(sp<C>& other) const { other = dynamic_cast<C*>(_object); }

   // State information

   bool isNull() const { return _object == nullptr; }
   bool isValid() const { return _object != nullptr; }

   template <class C>
   bool is() const { return dynamic_cast<const C*>(_object); }

   // Operations

   operator bool() const { return _object != nullptr; }
   bool operator!() const { return _object == nullptr; }

   bool operator==(const sp& other) const { return _object == other._object; }
   bool operator!=(const sp& other) const { return _object != other._object; }
   bool operator<(const sp& other) const { return _object < other._object; }
   bool operator>(const sp& other) const { return _object > other._object; }

   bool operator==(const T* ptr) const { return _object == ptr; }
   bool operator!=(const T* ptr) const { return _object != ptr; }
   bool operator<(const T* ptr) const { return _object < ptr; }
   bool operator>(const T* ptr) const { return _object > ptr; }

   void reset() { release(); }

   // Pointer access

   operator T* () { return v(); }
   operator const T* () const { return _object; }

   T* operator->() { if (!_object) throw QNullPointerException(); return v(); }
   const T* operator->() const { if (!_object) throw QNullPointerException(); return _object; }
   T& operator*() { if (!_object) throw QNullPointerException(); return *v(); }
   const T& operator*() const { if (!_object) throw QNullPointerException(); return *_object; }

   T* data() { return v(); }
   const T* data() const { return _object; }
   const T* constData() const { return _object; }

   // Conversions

   template <class C>
   C* as() { return dynamic_cast<C*>(v()); }

   template <class C>
   const C* as() const { return dynamic_cast<const C*>(_object); }

private:
   T* v()
   {
      if (_object && _object->ref > 1)
      {
         auto newObject = _object->clone();
         _object->ref.deref();
         _object = newObject;
         _object->ref.ref();
      }

      return _object;
   }

   void bind()
   {
      if (_object)
         _object->ref.ref();
   }

   void release()
   {
      if (_object && !_object->ref.deref())
         delete _object;
      _object = nullptr;
   }

   T* _object = nullptr;
};

template <class T, typename ... P>
sp<T> sp_new(P&& ... p) { return new T(p...); }
