#ifndef SERSTREAM_H
#define SERSTREAM_H

#include "qtcoreex_global.h"
#include <QIODevice>
#include "numeric.h"
#include <QDate>
#include <QTime>
#include <QDateTime>
#include <QSize>
#include <QStringList>

QTCOREEX_EXPORT const char *qGlobalColorName(Qt::GlobalColor color);
QTCOREEX_EXPORT Qt::GlobalColor qGlobalColorFromName(const char* name);

/*
 * SerStream serializes and deserializes data to or from a stream.
 * 
 * The stream can be a device, a byte array or a string.
 * 
 * The data stream consist entirely of ASCII characters and can therefore be stored as text.
 *
 * To use Qt containers like QList or QMap with SerStream include QtListExtensions.h.
 */
class QTCOREEX_EXPORT SerStream
{
   Q_DISABLE_COPY(SerStream)

public:
   class InDevice
   {
      Q_DISABLE_COPY(InDevice)

   public:
      InDevice() = default;
      virtual ~InDevice() = default;

      int timeout() const { return _timeout; }
      void setTimeout(int msecs) { _timeout = msecs; }

      virtual bool atEnd() const = 0;
      virtual bool hasData() const = 0;
      virtual qint64 pos() const = 0;
      virtual void reset() = 0;

      virtual bool read(char *buffer, qint64 size, bool peek) = 0;

      const QString& errorString() const { return _lastError; }

   protected:
      int _timeout = -1; // Milliseconds (-1 = infinite)
      QString _lastError;
   };

   class OutDevice
   {
      Q_DISABLE_COPY(OutDevice)

   public:
      OutDevice() = default;
      virtual ~OutDevice() = default;

      int timeout() const { return _timeout; }
      void setTimeout(int msecs) { _timeout = msecs; }

      virtual bool write(const char *buffer, qint64 size) = 0;

      virtual void flush() = 0;

      const QString& errorString() const { return _lastError; }

   protected:
      int _timeout = -1; // Milliseconds (-1 = infinite)
      QString _lastError;
   };

public:
   SerStream(); // Opens an out stream to an internal byte array (retrievable with internalBuffer())
   SerStream(QIODevice* device, bool own = false); // IN/OUT is determined by the open mode
   SerStream(const QByteArray& in, QByteArray* out = nullptr, bool own = false);
   SerStream(QByteArray* out, bool own = false);
   SerStream(const QString& in, QString* out = nullptr, bool own = false);
   SerStream(QString* out, bool own = false);
   SerStream(FILE* in, FILE* out = nullptr);
   SerStream(InDevice* inDevice, OutDevice* outDevice = nullptr); // SerStream takes ownership of inDevice and outDevice
   SerStream(OutDevice* outDevice); // SerStream takes ownership of outDevice
   ~SerStream();

   int timeout() const; // Timeout in milliseconds
   void setTimeout(int msecs); // Set the timeout in milliseconds (-1 is infinite timeout)

   bool escalate() const { return _escalate; }
   void setEscalate(bool escalate) { _escalate = escalate; }

   int version() const { return _version; }
   void setVersion(int version) { _version = version; } // Do not use version 0 for writing to a stream!!!

   QIODevice* inDevice();
   QIODevice* outDevice();
   void setInDevice(QIODevice *device, bool own = false);
   void setOutDevice(QIODevice *device, bool own = false);

   bool atEnd();
   bool isValid() const { return (_inDevice || _outDevice) && !hasError(); }
   bool canRead() const; // Returns if the in stream is ready to read (with bytes available)
   bool hasError() const { return !_errors.isEmpty(); }

   void setError(const char* functionName, const QString& error);

   const QStringList& errors() const { return _errors; }

   void reset();

   SerStream& operator << (bool b) { insertRaw(b ? "true" : "false"); return *this; }
   SerStream& operator << (qint32 i) { insertRaw(std::toString(i)); endEntity(); return *this; }
   SerStream& operator << (quint32 i) { insertRaw(std::toString(i)); endEntity(); return *this; }
   SerStream& operator << (qint64 i) { insertRaw(std::toString(i)); endEntity(); return *this; }
   SerStream& operator << (quint64 i) { insertRaw(std::toString(i)); endEntity(); return *this; }
   SerStream& operator << (double f) { insertRaw(std::toString(f)); endEntity(); return *this; }
   SerStream& operator << (char c) { insertEncoded(c); return *this; }
   SerStream& operator << (QChar c) { insertEncoded(c); return *this; }
   SerStream& operator << (const QString& text) { insertEncoded(text); endEntity(); return *this; }
   SerStream& operator << (const QByteArray& data) { insertEncoded(data); endEntity(); return *this; }
   SerStream& operator << (const char* szText) { insertEncoded(QByteArray(szText)); endEntity(); return *this; }

   void endEntity() { putCharRaw(_entityEndChar); }
   void endRecord() { putCharRaw(_recEndChar); }

   void writeRaw(const char *buffer, qint64 size);

   SerStream& operator >> (bool& b) { b = readBool();  return *this;}
   SerStream& operator >> (qint32& i) { i = readInt32();  return *this;}
   SerStream& operator >> (quint32& i) { i = readUInt32();  return *this;}
   SerStream& operator >> (qint64& i) { i = readInt64();  return *this;}
   SerStream& operator >> (quint64& i) { i = readUInt64();  return *this;}
   SerStream& operator >> (float& f) { f = (float)readDouble();  return *this;}
   SerStream& operator >> (double& f) { f = readDouble();  return *this;}
   SerStream& operator >> (char& c) { int charCode = decodeChar(); if (charCode < 0) c = '\0'; else c = (char)(unsigned char)charCode; return *this; }
   SerStream& operator >> (QChar& c) { int charCode = decodeChar(); if (charCode < 0) c = QChar(); else c = (ushort)charCode; return *this; }
   SerStream& operator >> (QString& text) { text = readText(); return *this;}
   SerStream& operator >> (QByteArray& data) { data = readData(); return *this;}

   void flush();

   bool     readBool();
   qint32   readInt32();
   quint32  readUInt32();
   qint64   readInt64();
   quint64  readUInt64();
   double   readDouble();

   QString     readText();
   QByteArray  readData();
   QByteArray  readRaw(qint64 size);

   void readRaw(char *buffer, qint64 size, bool peek = false);

   QByteArray  peekRaw(qint64 size);
   void        peekRaw(char *buffer, qint64 size);

   bool isFlushOnRead() const { return _flushOnRead; }
   void setFlushOnRead(bool flushOnRead) { _flushOnRead = flushOnRead; }

   void skipEntityEnd();

   bool atRecordEnd();
   void skipRecordEnd();

   int readCharValue() { return decodeChar(); } // A negative value indicates an invalid character sequence (-1: separator (entity or record), -2: base64 encoded, -3: decoding error or end of stream)

   QByteArray internalBuffer() const;

public:
   static const int actualVersion = 2;

private:
   void insertRaw(const std::string& text) { writeRaw(text.c_str(), (qint64)text.length()); }
   void putCharRaw(char c) { writeRaw(&c, 1LL); }
   void insertEncoded(char c);
   void insertEncoded(QChar c);
   void insertEncoded(QByteArray data);
   void insertEncoded(const QString& data);

   int   decodeChar();
   void  readChar(char *c);

   static bool _isValidChar(int c) { return c >= 32 && c <= 125; }

   static const char _entityEndChar = '~'; // ASCII 126
   static const char _recEndChar = '\n';

   bool _isEntityEnd(char c) const { return c == _entityEndChar || _version == 0 && (c == _recEndChar || c == '\0'); }

private:
   int         _version = 0;
   InDevice*   _inDevice = nullptr;
   OutDevice*  _outDevice = nullptr;
   QStringList _errors;
   bool        _escalate = false;
   bool        _flushOnRead = false;
};

template <typename T>
SerStream& operator<<(SerStream &stream, const T& value)
{
   //static_assert(sizeof(T) == sizeof(quint32), "SerStream& operator<<(SerStream&, const T&): Invalid type T");
   Q_ASSERT(std::is_enum<T>::value);

   if (sizeof(T) == sizeof(quint32)) // Make it a runtime issue so that it is not necessary for all types used with Variant to define this stream operator.
   {
      stream << '0';
      stream << *(const quint32 *)(&value);
   }

   return stream;
}

template <typename T>
SerStream& operator>>(SerStream &stream, T& value)
{
   //static_assert(sizeof(T) == sizeof(quint32), "SerStream& operator>>(SerStream&, T&): Invalid type T");
   Q_ASSERT(std::is_enum<T>::value);

   if (sizeof(T) == sizeof(quint32)) // Make it a runtime issue so that it is not necessary for all types used with Variant to define this stream operator.
   {
      char typeVersion;

      stream >> typeVersion;
      Q_ASSERT(typeVersion == '0');
      stream >> *(quint32 *)(&value);
   }

   return stream;
}

inline SerStream &operator<<(SerStream &stream, const unsigned char &value) { stream << (quint32)value; return stream; }
inline SerStream &operator>>(SerStream &stream, unsigned char &value) { value = (unsigned char)stream.readUInt32(); return stream; }

inline SerStream &operator<<(SerStream &stream, const short &value) { stream << (qint32)value; return stream; }
inline SerStream &operator>>(SerStream &stream, short &value) { value = (short)stream.readInt32(); return stream; }

inline SerStream &operator<<(SerStream &stream, const unsigned short &value) { stream << (quint32)value; return stream; }
inline SerStream &operator>>(SerStream &stream, unsigned short &value) { value = (unsigned short)stream.readUInt32(); return stream; }

inline SerStream &operator<<(SerStream &stream, const long &value) { stream << (qint64)value; return stream; }
inline SerStream &operator>>(SerStream &stream, long &value) { value = (long)stream.readInt64(); return stream; }

inline SerStream &operator<<(SerStream &stream, const unsigned long &value) { stream << (quint64)value; return stream; }
inline SerStream &operator>>(SerStream &stream, unsigned long &value) { value = (unsigned long)stream.readUInt64(); return stream; }

QTCOREEX_EXPORT SerStream &operator<<(SerStream& stream, const QDate& date);
QTCOREEX_EXPORT SerStream &operator>>(SerStream& stream, QDate& date);

QTCOREEX_EXPORT SerStream &operator<<(SerStream& stream, const QTime& time);
QTCOREEX_EXPORT SerStream &operator>>(SerStream& stream, QTime& time);

QTCOREEX_EXPORT SerStream &operator<<(SerStream& stream, const QDateTime& dateTime);
QTCOREEX_EXPORT SerStream &operator>>(SerStream& stream, QDateTime& dateTime);

inline SerStream &operator<<(SerStream& stream, Qt::GlobalColor color) { stream << qGlobalColorName(color); return stream; }
inline SerStream &operator>>(SerStream& stream, Qt::GlobalColor& color) { color = qGlobalColorFromName(stream.readData().data()); return stream; }

inline bool operator<(const QSize& lhs, const QSize& rhs) { return lhs.width() * lhs.height() < rhs.width() * rhs.height(); }
inline SerStream &operator<<(SerStream& stream, const QSize& size) { stream << size.width() << size.height(); return stream; }
inline SerStream &operator>>(SerStream& stream, QSize& size) { stream >> size.rwidth() >> size.rheight(); return stream; }

#endif // SERSTREAM_H
