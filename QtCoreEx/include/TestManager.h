#ifndef TESTMANAGER_H
#define TESTMANAGER_H

#include "qtcoreex_global.h"
#include <QString>
#include <QList>
#include "FunctionStatus.h"
#include <QCoreApplication>

class TestFramework;
class HtModel;
class HtNode;
class UnitTestLibrary;
class QWidget;

class QTCOREEX_EXPORT TestSuite
{
   Q_DECLARE_TR_FUNCTIONS(TestSuite)

   friend class UnitTestLibrary;

public:
   TestSuite(TestFramework* testFramework);
   TestSuite(const TestSuite& other);
   ~TestSuite();

   TestSuite& operator=(const TestSuite& other);

   bool isValid() const;

   QString name() const;

   int methodCount() const;
   QString methodSignature(int index) const;

   void setDataDirectory(const QString& path);
   void setParentWindow(QWidget* parent);

   void init(FunctionStatus status);
   void deinit(FunctionStatus status);

   void executeMethod(int index, FunctionStatus status);

private:
   TestFramework* _testFramework;
   const QMetaObject* _metaObject;
};

class QTCOREEX_EXPORT TestModule
{
   Q_DISABLE_COPY(TestModule)
public:
   TestModule();
   ~TestModule();

   QWidget* parentWindow() const { return _parent; }
   void setParentWindow(QWidget* parent);

   const QString& dataDirectory() const { return _dataDir; }
   void setDataDirectory(const QString& path);

   FunctionStatus connect(QString libPath, const QString& dataPath = QString()); // if dataPath is empty the previously set data path is used

   QString libraryFile() const;

   const QList<TestSuite>& testSuites() const { return _testSuites; }

   void clear();

private:
   void setParentWindow();
   void setDataDirectory();

private:
   QWidget* _parent = nullptr;
   QString _dataDir;
   UnitTestLibrary* _library = nullptr;
   QList<TestSuite> _testSuites;
};

class QTCOREEX_EXPORT TestManager
{
   Q_DISABLE_COPY(TestManager)

public:
   static QStringList loadedModules();

public:
   TestManager();
   ~TestManager();

   void setParentWindow(QWidget* parent);

   TestModule* addModule(FunctionStatus status, const QString& modulePath, const QString& dataPath = QString());

   QList<TestModule*> addModulesFromFile(FunctionStatus status, const QString& fileName);

   const QList<TestModule*>& testModules() const { return _testModules; }

private:
   QWidget* _parent = nullptr;
   QList<TestModule*> _testModules;
};
#endif // TESTMANAGER_H
