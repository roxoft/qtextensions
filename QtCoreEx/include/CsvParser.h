#ifndef CSVPARSER_H
#define CSVPARSER_H

#include "Tokenizer.h"

class CsvValueContext : public Tokenizer::Context
{
public:
   CsvValueContext(QChar delimiter, const Context* outerContext) : Context(outerContext), _delimiter(delimiter) {}
   ~CsvValueContext() override = default;

protected:
   const Context* readToken(Token& token, TextStreamReader& data) const override;

private:
   QChar _delimiter;
};

class QTCOREEX_EXPORT CsvParser
{
public:
   CsvParser(QTextStream* is, QChar fieldDelimiter, QChar textDelimiter);
   ~CsvParser() = default;

   QStringList readRecord(QString* parsedText = nullptr);
   QList<Variant> readLineValues(const QLocaleEx& locale, QString* parsedText = nullptr);

private:
   Tokenizer _tokenizer;
};

#endif // CSVPARSER_H
