#ifndef EXPRESSIONBASE_H
#define EXPRESSIONBASE_H

#include "qtcoreex_global.h"
#include "IVariable.h"
#include <QSharedData>
#include <QList>
#include <QCoreApplication>

class ExpressionException : public QBasicException
{
public:
   ExpressionException(const ExpressionPtr& expr, const QString& message) : QBasicException(message), _expr(expr) {}
   ~ExpressionException() override = default;

   void raise() const override { throw *this; }
   ExpressionException *clone() const override { return new ExpressionException(*this); }

   const ExpressionPtr& expr() const { return _expr; }

private:
   ExpressionPtr _expr;
};

class FunctionPointerExpression;
class IdentifierResolver;

typedef QMap<QString, QPair<ExpressionPtr, bool>> ResolverFunctionMap; // Maps function names to function expressions. The boolean indicates it the expression is a member function.

QTCOREEX_EXPORT FunctionPointerExpression* createFunctionExpression(VarPtr(*function)(const IdentifierResolver&), const QString& description, const QString& returnType, const QList<QPair<QString, QString>>& parameters);

QTCOREEX_EXPORT void addResolverFunction(ResolverFunctionMap& functionMap, const QString& name, VarPtr (*function)(const IdentifierResolver&), const QString& returnType,
                                         const QList<QPair<QString, QString>>& parameters, const QString& description);

class QTCOREEX_EXPORT IdentifierResolver
{
   Q_DECLARE_TR_FUNCTIONS(IdentifierResolver)

public:
   IdentifierResolver(const VarPtr& varTree, const VarPtr& localVarTree, ResolverFunctionMap functionMap, const QLocaleEx& locale)
      : _locale(locale), _globalVarTree(varTree), _localVarTree(localVarTree), _functionMap(functionMap) {}
   ~IdentifierResolver() = default;

   const QLocaleEx& locale() const { return _locale; }
   void setLocale(const QLocaleEx& locale) { _locale = locale; }

   bool isValid() const { return _globalVarTree.isValid(); }

   bool canModify() const { return _addIfNotFound; }

   const VarPtr& varTree() const { return _globalVarTree; }
   const ResolverFunctionMap& functionMap() const { return _functionMap; }
   const VarPtr& value() const { return _value; }

   void setValue(const VarPtr& value) { _value = value; _addIfNotFound = true; }

   void setThis(const VarPtr& item) { _this = item; }

   virtual QPair<VarPtr, VarPtr> resolve(const QString& identifier) const;

   VarPtr newCollection() const { return _globalVarTree->newCollection(); }
   VarPtr newValue(const Variant& value) const { return _globalVarTree->newFromValue(value); }
   VarPtr newFunction(const ExpressionPtr& f) const { return _globalVarTree->newFromFunction(f); }

protected:
   QLocaleEx _locale;
   VarPtr _globalVarTree;
   VarPtr _localVarTree;
   ResolverFunctionMap _functionMap;
   VarPtr _value;
   bool _addIfNotFound = false;
   VarPtr _this;
};

class AbstractExpression : public QSharedData
{
public:
   class Value
   {
      friend class AbstractValueExpression;

   public:
      Value() = default;
      virtual ~Value() = default;

   protected:
      // Interface for resolving

      virtual QPair<VarPtr, VarPtr> resolve(const IdentifierResolver& identifierResolver) const { return QPair<VarPtr, VarPtr>(); }

      // Text representation

      virtual QString toString() const { return QString(); }
   };

   /*
    * Prefix operators must haven the second highest priority and only right-to-left operators are allowed in this priority group!
    */
   class QTCOREEX_EXPORT Operator
   {
      friend class AbstractOperatorExpression;

   public:
      enum class Type { Invalid = 'I', Prefix = 'U', Postfix = 'R', Binary = 'B' }; // Prefix operators can change to Postfix or Binary in fixTypeNotPrefix()

   public:
      Operator(Type type = Type::Invalid) : _type(type) {}
      virtual ~Operator() = default;

      virtual bool operator==(const Operator& rhs) const = 0;

   public:
      // Interface for creating an expression tree

      bool isValid() const { return type() != Type::Invalid; }

      Type type() const { return _type; }
      virtual void fixTypeNotPrefix() = 0;
      virtual bool canBePostfix() = 0; // This is for binary operation as ; or functions

      virtual bool needsBrackets(const Operator& op, bool onRightSide) const; // op represents a compound operation

      virtual bool isLeftToRight() const = 0;

      virtual int priority() const = 0;

      virtual bool isAssociative() const = 0;

      // Text representation

      virtual QString toString() const = 0;

   protected:
      // Interface for resolving

      virtual QPair<VarPtr, VarPtr> resolve(const IdentifierResolver& identifierResolver, const QList<ExpressionPtr>& operands) const { return QPair<VarPtr, VarPtr>(); }

   protected:
      Type _type = Type::Invalid;
   };

public:
   AbstractExpression() = default;
   AbstractExpression(const AbstractExpression &other) = default; // This is for cloning the Expression
   virtual ~AbstractExpression() = default;

   AbstractExpression& operator=(const AbstractExpression &other) { return *this; } // Though its not possible to assign the base class the content of an expression can still be assigned

   virtual AbstractExpression* clone() const = 0;

public:
   // Interface for resolving

   // Creates a new VarTree for variables (local variables)
   VarPtr resolve(const VarPtr& varTree, const ResolverFunctionMap& functionMap = ResolverFunctionMap(), const QLocaleEx& locale = defaultLocale()) const
   {
      return resolve(IdentifierResolver(varTree, varTree->newCollection(), functionMap, locale)).first;
   }

   // Has to be derived
   virtual QPair<VarPtr, VarPtr> resolve(const IdentifierResolver& identifierResolver) const
   {
      return QPair<VarPtr, VarPtr>();
   }

   // Text representation

   virtual QString toString() const { return QString(); }
};

class QTCOREEX_EXPORT AbstractOperatorExpression : public AbstractExpression
{
public:
   AbstractOperatorExpression() = default;
   AbstractOperatorExpression(const AbstractOperatorExpression &other) = default; // This is for cloning the Expression
   ~AbstractOperatorExpression() override = default;

   AbstractOperatorExpression& operator=(const AbstractOperatorExpression &other) = default;

public:
   // Interface for creating an expression tree

   virtual const Operator& op() const = 0;
   virtual Operator& op() = 0;
   virtual int operandsCount() const { return _operands.count(); }
   virtual const ExpressionPtr& operandAt(int index) const { return _operands.at(index); }

   virtual void addOperand(const ExpressionPtr& operand) { Q_ASSERT(!operand.isNull()); _operands.append(operand); }
   virtual void addOperands(const QList<ExpressionPtr>& operands) { _operands.append(operands); }

   const QList<ExpressionPtr>& operands() const { return _operands; }

public:
   // Interface for resolving

   QPair<VarPtr, VarPtr> resolve(const IdentifierResolver& identifierResolver) const override;

protected:
   QList<ExpressionPtr> _operands;
};

template <class O>
class GenericOperatorExpression : public AbstractOperatorExpression
{
   static_assert(std::is_base_of<AbstractExpression::Operator, O>::value, "O must be derived from AbstractExpression::Operator");
public:
   GenericOperatorExpression() = default;
   GenericOperatorExpression(const O& op) : _operator(op) {}
   GenericOperatorExpression(const GenericOperatorExpression &other) = default; // This is for cloning the Expression
   ~GenericOperatorExpression() override = default;

   GenericOperatorExpression& operator=(const GenericOperatorExpression &other) = default;

   AbstractExpression* clone() const override
   {
      return new GenericOperatorExpression(*this);
   }

   const Operator& op() const override { return _operator; }
   Operator& op() override { return _operator; }

protected:
   O _operator;
};

class QTCOREEX_EXPORT AbstractValueExpression : public AbstractExpression
{
public:
   AbstractValueExpression() = default;
   AbstractValueExpression(const AbstractValueExpression& other) = default;
   ~AbstractValueExpression() override = default;

   AbstractValueExpression& operator=(const AbstractValueExpression& other) = default;

   virtual const Value& value() const = 0;

public:
   // Interface for resolving

   QPair<VarPtr, VarPtr> resolve(const IdentifierResolver& identifierResolver) const override;
};

template <class V>
class GenericValueExpression : public AbstractValueExpression
{
   static_assert(std::is_base_of<AbstractExpression::Value, V>::value, "V must be derived from AbstractExpression::Value");
public:
   GenericValueExpression() = default;
   GenericValueExpression(const V& value) : _value(value) {}
   GenericValueExpression(const GenericValueExpression& other) = default;
   ~GenericValueExpression() override = default;

   GenericValueExpression& operator=(const GenericValueExpression& other) = default;

   AbstractExpression* clone() const override
   {
      return new GenericValueExpression(*this);
   }

   const Value& value() const override { return _value; }

protected:
   V _value;
};

class QTCOREEX_EXPORT AbstractFunctionExpression : public AbstractExpression
{
public:
   AbstractFunctionExpression() = default;
   AbstractFunctionExpression(const AbstractFunctionExpression& other) = default;
   ~AbstractFunctionExpression() override = default;

   AbstractFunctionExpression& operator=(const AbstractFunctionExpression& other) = default;

   const QString& returnType() const { return _returnType; }
   void setReturnType(const QString& returnType) { _returnType = returnType; }

   // Mandatory parameters always precedes optional parameters
   void addParameter(const QString& parameterName, const QString& parameterType, bool isMandatory);

   const QList<QPair<QString, QString>>& mandatoryParameters() const { return _mandatoryParameters; } // { name, type }
   const QList<QPair<QString, QString>>& optionalParameters() const { return _optionalParameters; } // { name, type }

   const QString& description() const { return _description; }
   void setDescription(const QString& description) { _description = description; }

protected:
   QString _returnType;
   QList<QPair<QString, QString>> _mandatoryParameters;
   QList<QPair<QString, QString>> _optionalParameters;
   QString _description;
};

class QTCOREEX_EXPORT FunctionPointerExpression : public AbstractFunctionExpression
{
public:
   FunctionPointerExpression(VarPtr(*resolveFunction)(const IdentifierResolver&)) : _resolveFunction(resolveFunction) {}
   FunctionPointerExpression(const FunctionPointerExpression& other) = default;
   ~FunctionPointerExpression() override = default;

   FunctionPointerExpression& operator=(const FunctionPointerExpression& other) = default;

   AbstractExpression* clone() const override
   {
      return new FunctionPointerExpression(*this);
   }

public:
   // Interface for resolving

   QPair<VarPtr, VarPtr> resolve(const IdentifierResolver& identifierResolver) const override;

private:
   VarPtr(*_resolveFunction)(const IdentifierResolver&);
};

template <class C>
class MemberFunctionExpression : public AbstractFunctionExpression
{
public:
   MemberFunctionExpression(C* instance, VarPtr(C::*resolveFunction)(const IdentifierResolver&)) : _instance(instance), _resolveFunction(resolveFunction) {}
   MemberFunctionExpression(const MemberFunctionExpression& other) = default;
   ~MemberFunctionExpression() override = default;

   MemberFunctionExpression& operator=(const MemberFunctionExpression& other) = default;

   AbstractExpression* clone() const override
   {
      return new MemberFunctionExpression(*this);
   }

public:
   // Interface for resolving

   QPair<VarPtr, VarPtr> resolve(const IdentifierResolver& identifierResolver) const override;

private:
   C* _instance = nullptr;
   VarPtr(C::*_resolveFunction)(const IdentifierResolver&);
};

template <class C>
QPair<VarPtr, VarPtr> MemberFunctionExpression<C>::resolve(const IdentifierResolver& identifierResolver) const
{
   if (_instance && _resolveFunction)
      return QPair<VarPtr, VarPtr>((_instance->*_resolveFunction)(identifierResolver), VarPtr());
   return QPair<VarPtr, VarPtr>();
}

#endif // EXPRESSIONBASE_H
