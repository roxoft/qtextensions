#ifndef EXPRESSIONEVALUATOR_H
#define EXPRESSIONEVALUATOR_H

#include "qtcoreex_global.h"
#include "ExpressionBase.h"
#include "VarTree.h"
#include <QCoreApplication>

// Creates an operation tree.
//
// Example for creating an experssion tree:
//
//    ExpressionParser expressionParser;
//    typedef ExpressionTemplate<CppOperator, CppTokenValue> CppExpression;
//
//    // Feed the expression parser
//    for (auto&& token : tokenList)
//    {
//       auto op = token.as<CppOperatorToken>();
//
//       if (op)
//       {
//          auto strOp = op->value().toString();
//
//          if (strOp == "(")
//          {
//             expressionParser.beginEnclosure();
//          }
//          else if (strOp == ")")
//          {
//             expressionParser.endEnclosure();
//          }
//          else
//          {
//             expressionParser.addOperator(CppExpression(op->op()));
//          }
//       }
//       else
//       {
//          expressionParser.addValue(CppExpression(token));
//       }
//    }
//
//    // Get the expression tree
//    auto exprTree = expressionParser.expressionTree();
//
class QTCOREEX_EXPORT ExpressionParser
{
   Q_DECLARE_TR_FUNCTIONS(ExpressionParser)

public:
   ExpressionParser();
   ~ExpressionParser() = default;

   void addValue(bp<AbstractValueExpression> expr);
   void addOperator(bp<AbstractOperatorExpression> expr);
   void beginEnclosure();
   void endEnclosure();

   ExpressionPtr lastRightSideOperand() const;

   ExpressionPtr expressionTree();

   void clear();

private:
   void evaluateOperators(int priority);

private:
   QList<QList<ExpressionPtr>> _operatorStackEnclosures;
   QList<QPair<ExpressionPtr, bool>> _operandStack; // The operands are separated into operand before and after an operator, the second parameter indicates if the operation is sealed (cannot be merged)
};

#endif // EXPRESSIONEVALUATOR_H
