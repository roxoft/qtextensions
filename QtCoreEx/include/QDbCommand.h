#ifndef QDBCOMMAND_H
#define QDBCOMMAND_H

#include "qtcoreex_global.h"
#include "QDbConnection.h"
#include <QString>
#include <Variant.h>
#include "QDbState.h"
#include "SmartPointer.h"
#include <QCoreApplication>

class QDbCommandEngine;

class QDbBoundValue : public QSharedData
{
public:
   QDbBoundValue() {}
   virtual ~QDbBoundValue() {}

   virtual const Variant& value() const { return _value; }
   virtual void setValue(const Variant& value) { _value = value; }

protected:
   Variant _value;
};

class SqlParameter
{
public:
   SqlParameter() : type(DT_UNKNOWN), transferMode(TM_Default), isPostLob(false) {}
   ~SqlParameter() {}

   QString name;

   Variant value;
   bp<QDbBoundValue> boundValue;

   QDbDataType type;
   QDbTransferMode transferMode;

   bool isPostLob;

   const Variant& getValue() const { return boundValue ? boundValue->value() : value; }
   void setValue(const Variant& v) { if (boundValue) boundValue->setValue(v); else value = v; }
};

class QTCOREEX_EXPORT QDbResultSet
{
   friend class QDbCommand;

public:
   QDbResultSet();
   QDbResultSet(const QDbResultSet &other);
   ~QDbResultSet();

   QDbResultSet &operator=(const QDbResultSet &rhs);

   bool isValid() const;

   QDbSchema::Table schema() const;

   bool atEnd() const;
   bool next();

   int affectedRows();

   Variant valueAt(int index); // Returns the value at position index (beginning from 0) in the current record
   Variant valueAt(const QString &name); // Returns the value at position indicated by name in the current record

   Variant operator[](int index); // Returns the value at position index (beginning from 0) in the current record
   Variant operator[](const QString &name); // Returns the value at position indicated by name in the current record

   const QDbState& state() const { return _state; }

private:
   bp<QDbCommandEngine> _engine;
   QDbState _state;
};

/* This class is similar to the QSqlQuery class but not derived from it. Therefore it has a significant different interface, though for the same purpose.

The standard usage scenario is:

1. Define the query by instantiating QDbCommand and providing the SQL statement either in the constructor or by calling setStatement().
   The difference to QSqlQuery is that exec() is **not** called implicitly by the constructor if an SQL statement is supplied.

2. If the statement contains placeholder values set their values by calling setPlaceholderValue().
   Placeholder defined in the statement but not set to a value or a type (for output parameters) will cause the execution of the statement to fail.
   Placeholder can either be set by name or by position. If set by name **the leading colon has to be omitted**.
   Setting the value of a named placeholder sets all occurences of this placeholder in the statement to that value.
   The position of a placeholder is the number of unnamed placeholder plus the number of **unique** named placeholders from the left of the statement.
   To make sure the type of a value is correctly interpreted it can be specified explicitely in setPlaceholderValue().

3. Call the member function exec() to execute the statement.

4. If the statement is a SELECT, call result() to get the results. Then you can iterate through the records with next() and obtain the column values with the [] operator.

If exceptions are enabled QDbCommand throws a QDbState object on error.

For stored procedures it is not necessary to specify the type of output parameters (it is *not* necessary to call setPlaceholderType()) as the non optional parameters are always preset from its schema!

Example:

    QDbCommand query("SELECT ACOLUMN FROM ATABLE WHERE BCOLUMN = :placeholder");

    query.setPlaceholderValue("placeholder", 1007);
    if (query.exec())
    {
       QDbResultSet result = query.result();
       while (result.next())
       {
          QString name;

          name = result[0].toString();
       }
    }

    QDbState dbState = query.state();

    if (dbState.type() == QDbState::Error)
    {
       QString errorText = QDbState.text();
    }
*/
class QTCOREEX_EXPORT QDbCommand
{
   Q_DECLARE_TR_FUNCTIONS(QDbCommand)

public:
   QDbCommand();

   QDbCommand(QDbConnection connection);
   QDbCommand(const QString& statement);
   QDbCommand(QDbConnection connection, const QString& statement);
   QDbCommand(const QDbSchema::Function& function);
   QDbCommand(QDbConnection connection, const QDbSchema::Function& function);

   QDbCommand(const QDbCommand &other);
   ~QDbCommand();

   QDbCommand &operator=(const QDbCommand &rhs);

   //
   // Error handling
   //

   const QDbState& state() const { return _state; }

   QDbState::ErrhPtr errorHandler() const { return _state.errorHandler(); }
   void setErrorHandler(QDbState::ErrhPtr errorHandler) { _state.setErrorHandler(errorHandler); }

   bool exceptionsEnabled() const;
   void enableExceptions(); // throws a QDbState on error
   void disableExceptions(); // use state() to check the error state (default)

   //
   // Connection handling
   //

   QDbConnection connection() const;

   //
   // Statement handling
   //

   void setStatement(const QString& statement);
   void setStatement(const QDbConnection& connection, const QString& statement);
   void setStatement(const QDbSchema::Function& function);
   void setStatement(const QDbConnection& connection, const QDbSchema::Function& function);
   void setProcedure(const QString& procedure);
   void setProcedure(const QDbConnection& connection, const QString& procedure);

   QString statement() const;

   //
   // Placeholder handling
   //

   // Parse the statement for placeholders and set the prepared statement.
   // This function is called implicitely when one of the setPlaceholder...() functions is called.
   // The prepared statement may have different placeholders
   void parsePlaceholders();

   // Set the placehoder type for output. The transfer mode is set to output.

   void setPlaceholderType(const QString& placeholder, QDbDataType type);
   void setPlaceholderType(int pos, QDbDataType type);

   // Set the value of the placeholder. If the type is explicitely specified the placeholder is set to this type.
   // BEWARE: The type of the placeholder is only set if the placeholder type is not yet known!
   //         This can be important if a statement is executed more than once:
   //         If, for example, a placeholder value is first set to an integer, all further calls to setPlaceholderValue will convert the values (doubles, decimals) to integer!

   void setPlaceholderValue(const QString& placeholder, const Variant& value, QDbDataType type = DT_UNKNOWN, QDbTransferMode transferMode = TM_Default);
   void setPlaceholderValue(int pos, const Variant& value, QDbDataType type = DT_UNKNOWN, QDbTransferMode transferMode = TM_Default);

   // Returns the position(s) of the placeholders with this name

   QList<int> placeholderPosition(const QString &name) const;

   // Get the value of a placeholder

   Variant placeholderValue(const QString& name);
   Variant placeholderValue(int pos);

   // Get the current transfer mode of the placeholder

   QDbTransferMode placeholderTransferMode(const QString& name);
   QDbTransferMode placeholderTransferMode(int pos);

   // Executing the command

   bool prepare(); // This is useful if commands are repeated

   bool exec() { return exec(-1); }
   bool exec(int rowsToFetch);
   bool exec(QList<SqlParameter>& phList, int rowsToFetch = -1);
   bool exec(const QString& statement, QList<SqlParameter>& phList, int rowsToFetch = -1);
   bool exec(const QString& statement, int rowsToFetch = -1);

   // Results

   QString preparedStatement() const; // After calling exec() this is the executed statement

   int affectedRows(); // Returns the number of rows affected by the last exec() or -1

   QDbResultSet result();

   // Debugging

   QString description() const; // Returns the statement and all the defined placeholders

public:
   static bool tableExists(const QString& tableName);
   static bool tableExists(QDbConnection connection, const QString& tableName);

private:
   QDbConnection _connection;
   bp<QDbCommandEngine> _engine;
   QDbState _state;
};

// Convenience class for stored procedures and functions
class QDbStoredProc : public QDbCommand
{
public:
   QDbStoredProc() {}
   QDbStoredProc(const QString& procedure) { setProcedure(procedure); }
   QDbStoredProc(QDbConnection connection, const QString& procedure) : QDbCommand(connection) { setProcedure(procedure); }
   QDbStoredProc(const QDbStoredProc& other) : QDbCommand(other) {}
   ~QDbStoredProc() {}

   QDbStoredProc& operator=(const QDbStoredProc& other) { QDbCommand::operator=(other); return *this; }
};

#endif // QDBCOMMAND_H
