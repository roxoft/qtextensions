#pragma once

#include "qtcoreex_global.h"
#include "ExpressionBase.h"

QTCOREEX_EXPORT ResolverFunctionMap createCppResolverFunctions();
