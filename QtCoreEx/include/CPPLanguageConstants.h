#ifndef CPPLANGUAGECONSTANTS_H
#define CPPLANGUAGECONSTANTS_H

// The actions are sorted by their priority!
// !!! actionNames in CPPExpression.cpp !!!
// !!! binary_value_operation_list in CPPExpression.cpp !!!
enum class CppAction {
   Invalid,
   PostIncr, PostDecr, Function, Subscript, NullConditionalSubscript, Member, NullConditionalMember, // Left-to-right
   Block, PreIncr, PreDecr, Plus, Minus, LogNot, BitNot, // Right-to-left
   Multiplication, Division, Modulus, Addition, Subtraction, // Left-to-right
   LessThan, GreaterThan, LessOrEqual, GreaterOrEqual, // Left-to-right
   Equality, Inequality, // Left-to-right
   BitAnd, BitXor, BitOr, // Left-to-right
   LogAnd, LogOr, // Left-to-right
   Conditional, Colon, NullCoalescing, Repeating, Filter, Assignment, Declaration, // Right-to-Left
   Comma, // Left-to-right
   SemiColon // Left-to-right
};

struct CppOpDef
{
   const char* op;
   char type;
   CppAction action;
};

// Remark: The pound sign (#) can occur in preprocessing directives but also used as operator
// Remark: // and /* are included as operators to handle comments in CppOperatorContext
extern CppOpDef cppOperators[];

enum CppKeywords {
   KW_and,
   KW_and_eq,
   KW_alignas,
   KW_alignof,
   KW_asm,
   KW_auto,
   KW_bitand,
   KW_bitor,
   KW_bool,
   KW_break,
   KW_case,
   KW_catch,
   KW_char,
   KW_char16_t,
   KW_char32_t,
   KW_class,
   KW_compl,
   KW_const,
   KW_constexpr,
   KW_const_cast,
   KW_continue,
   KW_decltype,
   KW_default,
   KW_delete,
   KW_do,
   KW_double,
   KW_dynamic_cast,
   KW_else,
   KW_enum,
   KW_explicit,
   KW_export,
   KW_extern,
   KW_false,
   KW_final, // Identifier with special meaning
   KW_float,
   KW_for,
   KW_friend,
   KW_goto,
   KW_if,
   KW_inline,
   KW_int,
   KW_long,
   KW_mutable,
   KW_namespace,
   KW_new,
   KW_noexcept,
   KW_not,
   KW_not_eq,
   KW_nullptr,
   KW_operator,
   KW_or,
   KW_or_eq,
   KW_override, // Identifier with special meaning
   KW_private,
   KW_protected,
   KW_public,
   KW_register,
   KW_reinterpret_cast,
   KW_return,
   KW_short,
   KW_signed,
   KW_sizeof,
   KW_static,
   KW_static_assert,
   KW_static_cast,
   KW_struct,
   KW_switch,
   KW_template,
   KW_this,
   KW_thread_local,
   KW_throw,
   KW_true,
   KW_try,
   KW_typedef,
   KW_typeid,
   KW_typename,
   KW_union,
   KW_unsigned,
   KW_using,
   KW_virtual,
   KW_void,
   KW_volatile,
   KW_wchar_t,
   KW_while,
   KW_xor,
   KW_xor_eq
};

enum CppMsKeywords {
   KW___abstract = 0x100,
   KW___alignof,
   KW___asm,
   KW___assume,
   KW___based,
   KW___box,
   KW___cdecl,
   KW___declspec,
   KW___delegate,
   KW___event,
   KW___except,
   KW___fastcall,
   KW___finally,
   KW___forceinline,
   KW___gc,
   KW___hook,
   KW___identifier,
   KW___if_exists,
   KW___if_not_exists,
   KW___inline,
   KW___int16,
   KW___int32,
   KW___int64,
   KW___int8,
   KW___interface,
   KW___leave,
   KW___m128,
   KW___m128d,
   KW___m128i,
   KW___m64,
   KW___multiple_inheritance,
   KW___nogc,
   KW___noop,
   KW___pin,
   KW___pragma,
   KW___property,
   KW___ptr32,
   KW___ptr64,
   KW___raise,
   KW___sealed,
   KW___single_inheritance,
   KW___stdcall,
   KW___super,
   KW___thiscall,
   KW___try,
   KW___try_cast,
   KW___unaligned,
   KW___unhook,
   KW___uuidof,
   KW___value,
   KW___virtual_inheritance,
   KW___w64,
   KW___wchar_t,
   KW_abstract,
   KW_array,
   KW_delegate,
   KW_deprecated,
   KW_dllexport,
   KW_dllimport,
   KW_each,
   KW_event,
   KW_finally,
   KW_friend_as,
   KW_gcnew,
   KW_generic,
   KW_in,
   KW_initonly,
   KW_interface,
   KW_interior_ptr,
   KW_literal,
   KW_naked,
   KW_noinline,
   KW_noreturn,
   KW_nothrow,
   KW_novtable,
   KW_property,
   KW_ref,
   KW_safecast,
   KW_sealed,
   KW_selectany,
   KW_thread,
   KW_uuid,
   KW_value
};

enum CppQtKeywords {
   KW_signals = 0x200,
   KW_slots
};

enum CppDirectives {
   DV_define,
   DV_elif,
   DV_else,
   DV_endif,
   DV_error,
   DV_if,
   DV_ifdef,
   DV_ifndef,
   DV_import,
   DV_include,
   DV_line,
   DV_pragma,
   DV_undef,
   DV_using
};

int findKeyword(const char *value); // Returns -1 if keyword cannot be found

int findDirective(const char *value); // Returns -1 if directive cannot be found

#endif // CPPLANGUAGECONSTANTS_H
