#ifndef HTMODEL_H
#define HTMODEL_H

#include "qtcoreex_global.h"
#include "HtNode.h"
#include "Variant.h"

class QTextStream;

/* Hierarchical table data model using a tree structure.

   HtModel contains a table name and a tree of nodes (HtNode) with the table data.

   The root node of the table model contains the column headers and is usually not part of the table data.

   To use the model in table views or in tree views instantiate a class derived from HtItemModel (HtStandardModel).
 */
class QTCOREEX_EXPORT HtModel
{
   Q_DISABLE_COPY(HtModel)

public:
   HtModel() {}
   virtual ~HtModel() {}

   // Name of the table

   virtual QString   name() const = 0;
   virtual void      setName(const QString&) {}

   // Convenience functions

   int      headerCount() const;
   QString  header(int index) const;
   void     setHeader(int index, const QString& name);

   HtNode         *row(int index) { return rootNode()->child(index); }
   const HtNode   *row(int index) const { return rootNode()->child(index); }

   Variant  cellValue(int row, int col, int role = Qt::DisplayRole) const { return rootNode()->child(row)->attribute(col, role); }
   void     setCellValue(int row, int col, const Variant &value, int role = Qt::EditRole) { rootNode()->child(row)->setAttribute(col, value, role); }

   // Base interface

   virtual const HtNode*   rootNode() const = 0; // The root node of the tree (never returns NULL, a default constructed item may be returned)
   virtual HtNode*         rootNode() = 0; // The root node of the tree which attributes define the column headers (creates a new element if necessary, hence never returning NULL)

   virtual Variant defaultAttribute(int role) const { Q_UNUSED(role); return Variant(); } // This is the model default for all attributes of a given role

   virtual void insertColumn(int index, int count = 1) = 0;
   virtual void removeColumn(int index, int count = 1) = 0;

   // Callback functions to notify for changes (called from HtNode implementations).

   virtual void onBeginInsertItems(HtNode* parent, int index, int count) { Q_UNUSED(parent); Q_UNUSED(index); Q_UNUSED(count); }
   virtual void onEndInsertItems(HtNode* parent, int index, int count) { Q_UNUSED(parent); Q_UNUSED(index); Q_UNUSED(count); }
   virtual void onBeginInsertColumns(HtNode* parent, int index, int count) { Q_UNUSED(parent); Q_UNUSED(index); Q_UNUSED(count); }
   virtual void onEndInsertColumns(HtNode* parent, int index, int count) { Q_UNUSED(parent); Q_UNUSED(index); Q_UNUSED(count); }
   virtual void onBeginRemoveItems(HtNode* parent, int index, int count) { Q_UNUSED(parent); Q_UNUSED(index); Q_UNUSED(count); }
   virtual void onEndRemoveItems(HtNode* parent, int index) { Q_UNUSED(parent); Q_UNUSED(index); }
   virtual void onBeginRemoveColumns(HtNode* parent, int index, int count) { Q_UNUSED(parent); Q_UNUSED(index); Q_UNUSED(count); }
   virtual void onEndRemoveColumns(HtNode* parent, int index) { Q_UNUSED(parent); Q_UNUSED(index); }

   virtual bool onBeginMoveItems(HtNode* parent, int from, int count, int to) { Q_UNUSED(parent); Q_UNUSED(from); Q_UNUSED(count); Q_UNUSED(to); return false; }
   virtual void onEndMoveItems(HtNode* parent, int from, int count, int to) { Q_UNUSED(parent); Q_UNUSED(from); Q_UNUSED(count); Q_UNUSED(to); }
   virtual void onBeginSwapItems(HtNode* parent, int index1, int index2) { Q_UNUSED(parent); Q_UNUSED(index1); Q_UNUSED(index2); }
   virtual void onEndSwapItems(HtNode* parent, int index1, int index2) { Q_UNUSED(parent); Q_UNUSED(index1); Q_UNUSED(index2); }
   virtual void onBeginLayoutChange(HtNode* parent) { Q_UNUSED(parent); } // Notify about layout changes (reordering) in the child list of parent
   virtual void onEndLayoutChange(HtNode* parent) { Q_UNUSED(parent); } // Must be the same parent as for onBeginLayoutChange
   virtual void onBeginResetModel() {} // Resets the header too!
   virtual void onEndResetModel() {} // Resets the header too!

   // If the lastAttribute is less than the firstAttribute, lastAttribute is set to firstAttribute.
   // If *role* is negative all roles should be considered as modified.
   virtual void onItemChanged(HtNode* item, int firstAttribute, int lastAttribute, int role = -1) { Q_UNUSED(item); Q_UNUSED(firstAttribute); Q_UNUSED(lastAttribute); Q_UNUSED(role); }

   // Modify notify behaviour for bulk operations

   virtual void startCollectChanges() {} // Stops notifying data changes but collects them
   virtual void endCollectChanges() {} // Ends collecting change notifications and send a notification with all changes

   // Save the contents of the rootNode to a stream in CSV format

   virtual void save(QTextStream& os, const QLocaleEx& locale = defaultLocale(), int fieldCount = -1) const; // saves the attributes separated by semicolons recursively to the stream
   virtual void load(QTextStream& is, const QLocaleEx& locale = defaultLocale(), bool header = false);
};

#endif // HTMODEL_H
