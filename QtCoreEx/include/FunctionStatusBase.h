#ifndef FUNCTIONSTATUSBASE_H
#define FUNCTIONSTATUSBASE_H

#include "qtcoreex_global.h"
#include <QSharedData>
#include <QStringList>
#include <QCoreApplication>

class QTextStream;

class StatusMessage
{
public:
   // Gives the severity of a message.
   enum Severity
   {
      S_None = 0,
      S_Debug = 1 << 0,
      S_Verbose = 1 << 1,
      S_Information = 1 << 2,
      S_Warning = 1 << 3,
      S_Error = 1 << 4,
      S_Critical = 1 << 5,
      S_All = S_Debug | S_Verbose | S_Information | S_Warning | S_Error | S_Critical
   };

   Q_DECLARE_FLAGS(Severities, Severity)

   StatusMessage(Severity severity, QString message, const QStringList& context)
      : _severity(severity), _message(message), _context(context)
   {
   }

   StatusMessage(const StatusMessage& other) = default;

   StatusMessage(const StatusMessage& message, const QStringList& context)
   {
      _severity = message._severity;
      _message = message._message;
      _context = context + message._context;
   }

   ~StatusMessage() = default;

   StatusMessage& operator=(const StatusMessage& other) = default;

   Severity severity() const { return _severity; }
   QString message() const { return _message; }
   const QStringList& context() const { return _context; }

private:
   Severity _severity;
   QString  _message;
   QStringList _context;
};

Q_DECLARE_OPERATORS_FOR_FLAGS(StatusMessage::Severities)

class QTCOREEX_EXPORT FunctionStatusBase : public QSharedData
{
   Q_DECLARE_TR_FUNCTIONS(FunctionStatusBase)
   Q_DISABLE_COPY(FunctionStatusBase)

public:
   FunctionStatusBase() = default;
   virtual ~FunctionStatusBase() = default;

   int progress() const { return _progress; }
   void setProgress(int progress) { _progress = progress; }

   virtual void add(const StatusMessage& statusMessage);

   virtual void reset();

   virtual StatusMessage::Severity severity() const;

   virtual bool hasMessages(StatusMessage::Severities severities) const;

   virtual int totalMessageCount(StatusMessage::Severities severities) const;

   QString completeMessage(StatusMessage::Severities severities) const;

   virtual QList<StatusMessage> messages() const;

   QStringList messages(StatusMessage::Severities severities) const;

   void transferMessagesFrom(FunctionStatusBase* other, const QStringList& context);

private:
   void generateMessage(StatusMessage::Severities severities, QTextStream& writer) const;

protected:
   int _progress = 0;

private:
   QList<StatusMessage> _statusMessages; // Flat list to preserve the order
};

#endif // FUNCTIONSTATUSBASE_H
