#ifndef WORKERTHREAD_H
#define WORKERTHREAD_H

#include <qtcoreex_global.h>
#include <QThread>
#include <QMutex>
#include <QWaitCondition>

class QTCOREEX_EXPORT WorkerThread : public QThread
{
   Q_OBJECT

public:
   explicit WorkerThread(QObject *parent = nullptr, void (QObject::*task)() = nullptr) : QThread(parent), _task(task), _idle(true), _stop(false) {}
   virtual ~WorkerThread() { stop(); }

   bool waitForIdle(int msec = -1);
   void startTask(void (QObject::*task)() = nullptr);
   void stop();

signals:
   void taskFinished();

protected:
   virtual void doTask() { if (_task) (parent()->*_task)(); }

protected:
   virtual void run() override;

private:
   void           (QObject::*_task)();
   QMutex         _mutex;
   QWaitCondition _runningCond;
   QWaitCondition _idleCond;
   bool           _idle;
   volatile bool  _stop;
};

#endif // WORKERTHREAD_H
