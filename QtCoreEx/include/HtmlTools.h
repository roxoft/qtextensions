#ifndef HTMLTOOLS_H
#define HTMLTOOLS_H

#include "qtcoreex_global.h"
#include <QMap>
#include <QHash>
#include <QString>
#include "TextStreamReader.h"

QTCOREEX_EXPORT bool isTextHtml(QString text);

class HtmlEntities
{
public:
   virtual const QMap<QChar, const char*>& charMap() const = 0;
   virtual const QMap<QByteArray, QChar>& entityMap() const = 0;

   virtual QString encode(const QString& text) const = 0;
   virtual QString decode(const QString& html) const = 0;
};

QTCOREEX_EXPORT const HtmlEntities &htmlEntities();

class QTCOREEX_EXPORT HtmlStreamReader : public TextStreamReader
{
public:
   HtmlStreamReader() { init(); }
   HtmlStreamReader(const QString& text) : TextStreamReader(text) { init(); }
   HtmlStreamReader(CharacterStreamReader* dataSource) : TextStreamReader(dataSource) { init(); }
   HtmlStreamReader(QTextStream* is) : TextStreamReader(is) { init(); }
   ~HtmlStreamReader() override = default;

   QChar peekChar(int index = 0) override;

private:
   // This class is used to build a hierarchical HTML character entity map for incremetal searches.
   struct CEntity
   {
      QMap<QChar, CEntity> entityMap;
      QChar character; // 0 if no character
      int level = 0;
   };

private:
   void init();
   QChar readNextChar();
   QChar translateNextChar();

private:
   CEntity _rootEntity;
};

class QTCOREEX_EXPORT AbstractHtmlEmbeddedResourceManager
{
public:
   AbstractHtmlEmbeddedResourceManager() = default;
   virtual ~AbstractHtmlEmbeddedResourceManager() = default;

   void extractEmbeddedImages(QString& html);
   void embedImages(QString& html);

   static QStringList imageSources(const QString& html);

protected:
   virtual QString addResource(int index, const QByteArray& data, const QString& imgType) = 0;
   virtual QPair<QByteArray, QString> resource(const QString& name) const = 0;
};

class QTCOREEX_EXPORT StandardHtmlEmbeddedResourceManager : public AbstractHtmlEmbeddedResourceManager
{
public:
   StandardHtmlEmbeddedResourceManager() = default;
   ~StandardHtmlEmbeddedResourceManager() override = default;

   const QHash<QString, QByteArray>& imageList() const { return _imageList; }
   void setImageList(const QHash<QString, QByteArray>& imageList) { _imageList = imageList; }

protected:
   QString addResource(int index, const QByteArray& data, const QString& imgType) override;
   QPair<QByteArray, QString> resource(const QString& name) const override;

private:
   QHash<QString, QByteArray> _imageList;
};

#endif // HTMLTOOLS_H
