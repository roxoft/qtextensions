// The main reason for this file is that there should be a reporting function which stops debugging but continues in release mode (so neither qCritical nor qFatal can be used)

#ifndef QPROCESSSTATENOTIFIER_H
#define QPROCESSSTATENOTIFIER_H

#include "qtcoreex_global.h"

#if defined(Q_CC_GNU)
#define __FUNCSIG__ __PRETTY_FUNCTION__
#endif

#define rpsInfo QProcessStateNotifier(__LINE__, __FILE__, __DATE__, __FUNCSIG__).info
#define rpsWarning QProcessStateNotifier(__LINE__, __FILE__, __DATE__, __FUNCSIG__).warning
#define rpsError QProcessStateNotifier(__LINE__, __FILE__, __DATE__, __FUNCSIG__).error
#define rpsFatal QProcessStateNotifier(__LINE__, __FILE__, __DATE__, __FUNCSIG__).fatal

class QTCOREEX_EXPORT QProcessStateNotifier
{
public:
   QProcessStateNotifier(int line, const char *file, const char *date, const char *function);

   void info(const char *msg, ...);
   void warning(const char *msg, ...);
   void error(const char *msg, ...);
   void fatal(const char *msg, ...);

private:
   int         _line;
   const char  *_file;
   const char  *_date;
   const char  *_function;
};

#endif // QPROCESSSTATENOTIFIER_H
