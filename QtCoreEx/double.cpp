#include "stdafx.h"
#include "double.h"

#ifdef __x86__
//
// Structure to split doubles into their components
//
// Excerpt from MSDN:
//
// Double precision values with double type have 8 bytes.
// The format is similar to the float format except that it has an 11-bit excess-1023 exponent and a 52-bit mantissa, plus the implied high-order 1 bit.
// This format gives a range of approximately 1.7E-308 to 1.7E+308 for type double.
//
// Microsoft Specific ->
// The double type contains 64 bits: 1 for sign, 11 for the exponent, and 52 for the mantissa. Its range is +/-1.7E308 with at least 15 digits of precision.
struct doublerep
{
   quint64 mantissa : 52;
   quint64 exponent : 11;
   quint64 sign : 1;
};

void splitDouble(const double& number, quint64& mantissa, int& exponent, bool& sign)
{
   // Split the double value into its components
   const doublerep& split = (const doublerep&)number;

   mantissa = split.mantissa;
   exponent = (int)split.exponent; // Binary exponent
   sign = split.sign != 0;

   // Special states
   if (exponent == 2047)
   {
      if (mantissa == 0)
      {
         // Infinite
         exponent = 0;
      }
      // else not a number (NaN)

      mantissa = (quint64)(-1LL);
      return;
   }

   // Normalize exponent
   if (exponent)
   {
      exponent -= 1023;
      mantissa |= 0x0010000000000000ULL;
   }
   else if (mantissa)
   {
      // subnormal number!
      exponent = -1022;
   }

   // Set the exponent to the right of the mantissa (the value is then calculated by: mantissa * 2 ^ exponent)
   exponent -= 52;

   // Reduce the mantissa by the exponent
   while (exponent < 0 && (mantissa & 1ULL) == 0ULL)
   {
      mantissa >>= 1;
      exponent++;
   }
}
#endif // __x86__

void transformExponent(quint64& mantissa, int& exponent, int fromBase, int toBase, const quint64& precision)
{
   if (mantissa == (quint64)(-1LL) || fromBase == toBase)
      return;

   // Convert the exponent (using all the bits of the mantissa)
   int      destExp = 0; // Exponent in toBase
   quint64  sourceLimit = 0xFFFFFFFFFFFFFFFFULL / (quint64)fromBase; // Upper limit of the mantissa
   quint64  destLimit = 0xFFFFFFFFFFFFFFFFULL / (quint64)toBase; // Upper limit of the mantissa

   while (exponent > 0)
   {
      while (mantissa > sourceLimit)
      {
         // There is not enough space to multiply
         mantissa /= (quint64)toBase;
         destExp++;
      }
      mantissa *= (quint64)fromBase;
      exponent--;
   }
   while (exponent < 0)
   {
      while (mantissa <= destLimit)
      {
         // There is enough sepace to decrement destination exponent
         mantissa *= (quint64)toBase;
         destExp--;
      }
      mantissa /= (quint64)fromBase;
      exponent++;
   }

   // Set the exponent to the result
   exponent = destExp;

   // Renormalize the precision of the mantissa
   int cutDigit = 0;

   while (mantissa >= precision)
   {
      cutDigit = (int)(mantissa % (quint64)toBase);
      mantissa /= (quint64)toBase;
      exponent++;
   }

   // Round mathematical (the mantissa is always positive)
   // TODO: implement other rounding strategies
   if (cutDigit > (toBase - 1) / 2)
      mantissa++;

   // Reduce the mantissa by the exponent
   while (exponent < 0 && (mantissa % (quint64)toBase) == 0ULL)
   {
      mantissa /= (quint64)toBase;
      exponent++;
   }
}
