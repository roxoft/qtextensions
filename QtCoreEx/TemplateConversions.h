#ifndef TEMPLATECONVERSIONS_H
#define TEMPLATECONVERSIONS_H

#include "double.h"

template <typename N>
int countDigits(N number, int base)
{
   int digits = 0;

   do
   {
      digits++;
      number /= (N)base;
   } while (number);

   return digits;
}

template <typename N, class S>
bool integerToString(N number, int base, S& str, typename S::value_type negativeSign = '-', typename S::value_type thousandsSeparator = typename S::value_type(), int fractionalDigits = 0, typename S::value_type decimalPoint = '.')
{
   if (base == 0 || fractionalDigits < 0)
      return false;

   const bool isNegative = number < (N)0;
   int digits = countDigits(number, base);

   if (fractionalDigits >= digits)
      digits = fractionalDigits + 1; // Leading zeros
   if (thousandsSeparator != typename S::value_type())
      digits += (digits - fractionalDigits - 1) / 3; // Thousands separators
   if (isNegative)
      digits++; // Negative sign
   if (fractionalDigits > 0)
      digits++; // Decimal point

   str.resize(digits);

   unsigned magn = 0;

   do
   {
      if (magn == 3 && thousandsSeparator != typename S::value_type())
      {
         digits--;
         str[digits] = thousandsSeparator;
         magn = 0;
      }
      if (fractionalDigits == 0)
         magn++;
      digits--;
      if (isNegative)
         str[digits] = (char)('0' - (number % (N)base));
      else
         str[digits] = (char)('0' + (number % (N)base));
      number /= (N)base;
      if (fractionalDigits > 0)
      {
         fractionalDigits--;
         if (fractionalDigits == 0)
         {
            digits--;
            str[digits] = decimalPoint;
            if (number == 0)
            {
               digits--;
               str[digits] = '0';
            }
         }
      }
   } while (number || fractionalDigits);

   if (isNegative)
   {
      digits--;
      str[digits] = negativeSign;
   }

   Q_ASSERT(digits == 0);

   return true;
}

#ifdef __x86__

/* Converting a double precision value to string representation

   Returns the value converted to a string.

   Parameter:
   - number: The double value to convert
   - precision: Maximum number of digits of the fractional part (after the decimal point). A negative precision indicates to omit trailing zeros.
   - negativeSign: The charcter to use for the negative sign
   - decimalPoint: The charcter to use for the decimal point
   - thousandsSeparator: The charcter to use for the thousand separators

   This conversion is more accurate than sprintf (e.g. with 123E-300) and more secure than sprintf because no buffer size has to be estimated.

   Remark: RWLocaleSnapshot::asString() uses sprintf!
*/
template <class S>
bool doubleToString(const double& number, int precision, S& str, typename S::value_type negativeSign = '-', typename S::value_type decimalPoint = '.', typename S::value_type thousandsSeparator = typename S::value_type())
{
   // Get the mantissa and the decimal exponent of the double
   quint64  mantissa = 0ULL;
   int      exponent = 0;
   bool     sign = false;

   splitDouble(number, mantissa, exponent, sign); // The exponent is binary!

   if (!mantissaIsValid(mantissa))
   {
      str.clear();

      if (sign)
         str += negativeSign;

      if (exponent)
      {
         str += typename S::value_type('N');
         str += typename S::value_type('a');
         str += typename S::value_type('N');
      }
      else
      {
         str += typename S::value_type('I');
         str += typename S::value_type('N');
         str += typename S::value_type('F');
         str += typename S::value_type('I');
         str += typename S::value_type('N');
         str += typename S::value_type('I');
         str += typename S::value_type('T');
         str += typename S::value_type('Y');
      }

      return true;
   }

   transformExponent(mantissa, exponent, 2, 10, 1ULL << 52);

   // If an exponent has to be displayed set the decimal point after the first (non zero) digit (scientific notation)
   int fractionalDigits = 0;

   if (exponent >= -16 && exponent < 0)
   {
      fractionalDigits = -exponent;
      exponent = 0;
   }
   else
   {
      // Count the fractional digits and move the decimal point to the leftmost digit
      // If the exponent was negative it won't get positive
      for (quint64 m = mantissa; exponent && m > 9ULL; m /= 10ULL)
      {
         fractionalDigits++;
         exponent++;
      }
   }

   // Set the number of farctional digits
   bool noTrailingZeros = false;

   if (precision < 0)
   {
      precision *= -1;
      noTrailingZeros = true;
   }

   if (noTrailingZeros)
   {
      if (exponent > 0)
      {
         while ((mantissa % 10ULL) == 0ULL)
         {
            Q_ASSERT(mantissa);
            mantissa /= 10ULL;
            fractionalDigits--;
         }
      }
      if (fractionalDigits < precision)
         precision = fractionalDigits;
   }
   else
   {
      while (fractionalDigits < precision)
      {
         mantissa *= 10ULL;
         fractionalDigits++;
      }
   }

   // Round mathematical
   // TODO: Implement other rounding strategies
   int cutDigit = 0;

   while (precision < fractionalDigits)
   {
      cutDigit = (int)(mantissa % 10ULL);
      mantissa /= 10ULL;
      fractionalDigits--;
   }

   if (cutDigit >= 5) // The mantissa is always positive
      mantissa++;

   if (!integerToString(sign ? -qint64(mantissa) : qint64(mantissa), 10, str, negativeSign, thousandsSeparator, precision, decimalPoint))
      return false;

   // Add exponent
   if (exponent != 0)
   {
      S strExp;
      integerToString(exponent, 10, strExp, negativeSign);

      str += typename S::value_type('E');
      str += strExp;
   }

   return true;
}

#endif // __x86__

template <typename iterator>
double stringToDouble(iterator* it, int zeroDigit = int('0'), int negativeSign = int('-'), int decimalPoint = int('.'), int thousandsSeparator = 0)
{
   double   result = 0;
   bool     negative = false;
   bool     expNegative = false;
   bool     fraction = false;
   bool     exponent = false;
   int      fracDigits = 0;
   int      magnitude = 0;

   if (**it == negativeSign)
   {
      negative = true;
      ++(*it);
   }
   for (;;)
   {
      int c = **it;

      if (c == 0)
         break;

      if (c == negativeSign)
      {
         if (!exponent)
            break;
         if (magnitude)
            break;
         expNegative = true;
      }
      else if (c == decimalPoint)
      {
         if (fraction || exponent)
            break;
         fraction = true;
      }
      else if (c == thousandsSeparator)
      {
         if (fraction || exponent)
            break;
      }
      else if (c == 'E' || c == 'e')
      {
         if (exponent)
            break;
         exponent = true;
      }
      else
      {
         if (c < zeroDigit)
            break;

         c -= zeroDigit;

         if (c > 9)
            break;

         if (exponent)
         {
            magnitude *= 10;
            magnitude += c;
         }
         else
         {
            result *= 10;
            result += c;
            if (fraction)
               fracDigits++;
         }
      }

      ++(*it);
   }

   if (expNegative)
      magnitude *= -1;
   magnitude -= fracDigits;

   while (magnitude > 0)
   {
      result *= 10;
      magnitude--;
   }
   while (magnitude < 0)
   {
      result /= 10;
      magnitude++;
   }

   if (negative)
      return -result;

   return result;
}

template <typename iterator>
double stringToDouble(iterator it, int zeroDigit = int('0'), int negativeSign = int('-'), int decimalPoint = int('.'), int thousandsSeparator = 0)
{
   return stringToDouble(&it, zeroDigit, negativeSign, decimalPoint, thousandsSeparator);
}

#endif // TEMPLATECONVERSIONS_H
