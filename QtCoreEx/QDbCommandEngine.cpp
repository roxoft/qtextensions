#include "stdafx.h"
#include "QDbCommandEngine.h"
#include "QDbConnectionData.h"
#include <QtListExtensions.h>

QDbCommandEngine::QDbCommandEngine()
{
}

QDbCommandEngine::~QDbCommandEngine()
{
   qDeleteAll(_phList);
}

QDbState QDbCommandEngine::setStatement(const QString& statement)
{
   clear();

   _statement = statement;

   return QDbState();
}

QDbState QDbCommandEngine::setStatement(const QDbSchema::Function& function)
{
   clear();

   _functionSchema = function;

   // Create corresponding placeholders
   createFunctionPlaceholder();

   return QDbState();
}

QDbState QDbCommandEngine::setProcedure(const QString& procedure)
{
   clear();

   _statement = procedure;

   _functionSchema = connection()->functionSchema(_statement.trimmed().toUpper());
   if (connection()->state().type() == QDbState::Error)
      return connection()->state();
   if (_functionSchema.name.isEmpty())
      return QDbState(QDbState::Error, tr("Stored procedure '%1' not found").arg(_statement.trimmed()), QDbState::Statement);

   createFunctionPlaceholder();

   return QDbState();
}

void QDbCommandEngine::preparePlaceholder()
{
   if (!_phList.isEmpty() || !_preparedStatement.isEmpty())
      return;

   // Create placeholder list
   bool isNamedPlaceholder = false;
   int phPos = 0;
   QChar quoteChar;
   QChar phChar = connection()->phIndicator();
   auto commentEnd = u"";
   QChar prevChar;
   int begin = 0;
   int phBegin = 0;

   // Skip space
   while (begin < _statement.length() && _statement[begin].isSpace())
      begin++;

   // Parse _statement
   auto pos = begin;

   for (; pos <= _statement.length(); ++pos)
   {
      QChar qChar;

      if (pos < _statement.length())
         qChar = _statement[pos];

      if (*commentEnd)
      {
         if (commentEnd[1])
         {
            if (prevChar == commentEnd[0] && qChar == commentEnd[1])
               commentEnd = u"";
         }
         else
         {
            if (qChar == commentEnd[0])
               commentEnd = u"";
         }
      }
      else if (!quoteChar.isNull())
      {
         if (qChar == quoteChar)
         {
            quoteChar = QChar();
         }
      }
      else
      {
         if (isNamedPlaceholder)
         {
            if ((qChar == u'\'' || qChar == u'"') && (phBegin == pos || prevChar == qChar))
            {
               // This is a quoted placeholder
               quoteChar = qChar;
            }
            else if (phBegin == pos)
            {
               // The first character of the name must be a letter otherwise it is not a placeholder
               if (!qChar.isLetterOrNumber())
                  isNamedPlaceholder = false;
            }
            else if (!qChar.isLetterOrNumber() && qChar.unicode() != u'_' && qChar.unicode() != u'$' && qChar.unicode() != u'#')
            {
               // The current character is not a valid name character or we are at the end of the string

               addPh(_statement.mid(phBegin, pos - phBegin), ++phPos);
               if (connection()->phType() == QDbConnection::PHT_UnnamedOnly)
               {
                  _preparedStatement += _statement.mid(begin, phBegin - 1 - begin); // Copy everything up to the colon
                  begin = pos; // Move the begin to the current position (skip the placeholder)
                  _preparedStatement += "?"; // Replace the placeholder with an unnamed placeholder
               }
               isNamedPlaceholder = false;
            }
         }

         if (!isNamedPlaceholder)
         {
            if (qChar == phChar)
            {
               isNamedPlaceholder = true;
               phBegin = pos + 1;
            }
            else if (qChar == u'?')
            {
               const auto ph = addPh(QString(), ++phPos);
               if (connection()->phType() == QDbConnection::PHT_NamedOnly)
               {
                  _preparedStatement += _statement.mid(begin, pos - begin); // Copy everything up to the question mark
                  begin = pos + 1; // Move the begin to the current position (skip the placeholder)
                  _preparedStatement += phChar; // Add the indicator for the named placeholder
                  _preparedStatement += ph->name(); // Add the placeholder name
               }
            }
            else if (qChar == u'\'')
            {
               quoteChar = u'\'';
            }
            else if (qChar == u'"')
            {
               quoteChar = u'"';
            }
            else if (prevChar == u'-' && qChar == u'-')
            {
               commentEnd = u"\n";
            }
            else if (prevChar == u'/' && qChar == u'/')
            {
               commentEnd = u"\n";
            }
            else if (prevChar == u'/' && qChar == u'*')
            {
               commentEnd = u"*/";
               qChar = QChar(); // Sets prevChar to 0 and starts scanning for the end of the comment after the current asterisk
            }
         }
      }

      prevChar = qChar;
   }

   pos = _statement.length();
   while (pos > 0 && _statement[pos - 1].isSpace())
      pos--;

   if (begin < pos)
      _preparedStatement += _statement.mid(begin, pos - begin); // Copy the rest
}

int QDbCommandEngine::placeholderIndex(const QString& name) const
{
   for (int i = 0; i < _phList.count(); ++i)
   {
      if (_phList[i]->name() == name)
         return i;
   }

   return -1;
}

QList<int> QDbCommandEngine::placeholderPositions(const QString &name) const
{
   for (auto&& ph : _phList)
   {
      if (ph->name() == name)
      {
         return ph->positions();
      }
   }

   return QList<int>();
}

QDbState QDbCommandEngine::setPlaceholderType(int index, QDbDataType type)
{
   if (index < 0 || index >= _phList.count())
      return errorInvalidIndex(index);

   if (type == DT_BOOL && connection() && !connection()->_falseString.isEmpty() && !connection()->_trueString.isEmpty())
      type = DT_STRING;

   return setPlaceholderType(_phList[index], type);
}

QDbState QDbCommandEngine::setPlaceholderValue(int index, const Variant& value)
{
   if (index < 0 || index >= _phList.count())
      return errorInvalidIndex(index);

   // If the value is a boolean and characters should be used for booleans change the type here
   if (value.type() == typeid(bool) && connection() && !connection()->_falseString.isEmpty() && !connection()->_trueString.isEmpty())
      return setPlaceholderValue(_phList[index], value.toBool() ? connection()->_trueString : connection()->_falseString);
   return setPlaceholderValue(_phList[index], value);
}

QDbState QDbCommandEngine::setPlaceholderIsSending(int index, bool send)
{
   if (index < 0 || index >= _phList.count())
      return errorInvalidIndex(index);

   _phList[index]->setCanSend(send);

   return QDbState();
}

QDbState QDbCommandEngine::setPlaceholderIsReceiving(int index, bool receive)
{
   if (index < 0 || index >= _phList.count())
      return errorInvalidIndex(index);

   _phList[index]->setCanReceive(receive);

   return QDbState();
}

QDbState QDbCommandEngine::placeholderValue(int index, Variant& value) const
{
   if (index < 0 || index >= _phList.count())
      return errorInvalidIndex(index);

   return placeholderValue(_phList[index], value);
}

QDbState QDbCommandEngine::placeholderIsSending(int index, bool& send) const
{
   if (index < 0 || index >= _phList.count())
      return errorInvalidIndex(index);

   send = _phList[index]->canSend();

   return QDbState();
}

QDbState QDbCommandEngine::placeholderIsReceiving(int index, bool& receive) const
{
   if (index < 0 || index >= _phList.count())
      return errorInvalidIndex(index);

   receive = _phList[index]->canReceive();

   return QDbState();
}

QString QDbCommandEngine::description() const
{
   QString result = statement();

   result += "\n";
   for (auto&& ph : _phList)
   {
      QString value;

      if (ph->schema().type == DT_UNKNOWN)
         value = tr(">Undefined<");
      else if (!ph->isValid())
         value = tr(">Invalid<");
      else
      {
         auto v = ph->value();
         if (v.isValueNull())
            value = tr(">NULL<");
         else
            value = v.toString();
      }

      if (value.length() > 40)
      {
         value.resize(40);
         value += "...";
      }

      result += "\n";
      if (ph->phType() == QDbConnection::PHT_UnnamedOnly)
         result += toQStringList(ph->positions()).join(", ");
      else
         result += ph->name();
      result += ": ";
      result += value;
   }

   return result;
}

void QDbCommandEngine::clear()
{
   _statement.clear();
   _preparedStatement.clear();
   _functionSchema = QDbSchema::Function();
   qDeleteAll(_phList);
   _phList.clear();
}

QDbState QDbCommandEngine::errorInvalidIndex(int index) const
{
   return QDbState(QDbState::Error, tr("Missing placeholder at position %1").arg(index + 1), _statement);
}

QDbState QDbCommandEngine::errorInvalidVariable(const PhInfo* phInfo) const
{
   return QDbState(QDbState::Error, tr("Variable invalid for placeholder at position %1").arg(phInfo->positions().first()), _statement);
}

QDbState QDbCommandEngine::errorInferringDataType(const Variant& variant, const PhInfo* phInfo) const
{
   return QDbState(QDbState::Error, tr("Unable to infer the database type form the variant type '%1' for the placeholder at position %2").arg(plain_type_name(variant.type())).arg(phInfo->positions().first()), _statement);
}

QDbState QDbCommandEngine::errorInferringDataType(QDbDataType type, const PhInfo* phInfo) const
{
   return QDbState(QDbState::Error, tr("Unable to infer the database type from the internal type (%1) for the placeholder at position %2").arg(type).arg(phInfo->positions().first()), _statement);
}

QDbState QDbCommandEngine::errorExecuting() const
{
   return QDbState(QDbState::Error, QString(), description());
}

void QDbCommandEngine::createFunctionPlaceholder()
{
   Q_ASSERT(_phList.isEmpty());

   if (_functionSchema.retVal.isValid())
   {
      // This is a function with a return value

      // Add the return value to the binding list
      _phList.append(createPhInfo(_functionSchema.retVal, connection()->phType())); // The positions of the placeholder is set when the statement is generated (in exec())
      Q_ASSERT(_phList.last()->isValid());
   }

   foreach(const QDbSchema::Column &argument, _functionSchema.arguments)
   {
      _phList.append(createPhInfo(argument, connection()->phType())); // The positions of the placeholder is set when the statement is generated (in exec())
      Q_ASSERT(_phList.last()->isValid());
   }
}

QDbCommandEngine::PhInfo* QDbCommandEngine::addPh(QString name, int position)
{
   auto type = connection()->phType();

   if (type == QDbConnection::PHT_All)
      type = name.isEmpty() ? QDbConnection::PHT_UnnamedOnly : QDbConnection::PHT_NamedOnly;

   auto i = 0;

   if (name.isEmpty())
   {
      for (auto phPos = 1; phPos < 100; phPos++)
      {
         name = QString("PH%1").arg(phPos, 2, 10, QLatin1Char('0'));
         if (placeholderIndex(name) == -1)
            break;
      }

      i = _phList.count();
   }
   else
   {
      // Check if the name is quoted
      if (name[0] == u'\'')
      {
         name = name.mid(1, name.length() - 2);
         name.replace("''", "'");
      }
      else if (name[0] == u'"')
      {
         name = name.mid(1, name.length() - 2);
         name.replace("\"\"", "\"");
      }
      else
      {
         name = name.toUpper();
      }

      while (i < _phList.count() && _phList[i]->name() != name)
      {
         i++;
      }
   }

   if (i == _phList.count())
      _phList.append(createPhInfo(name, type));

   _phList[i]->addPosition(position);

   return _phList[i];
}
