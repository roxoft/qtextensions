#include "stdafx.h"
#include "QDbState.h"

QDbState::QDbState(const QString& sourceText, Source source)
   : _source(source), _sourceText(sourceText)
{
}

QDbState::QDbState(Type type, const QString& text, Source source, int number)
   : _text(text), _type(type), _source(source), _number(number)
{
}

QDbState::QDbState(Type type, const QString& text, const QString& sourceText, Source source, int number)
   : _text(text), _type(type), _source(source), _sourceText(sourceText), _number(number)
{
}

QDbState::QDbState(const QDbState &other)
   : _text(other._text), _type(other._type), _source(other._source), _sourceText(other._sourceText), _number(other._number), _errorHandler(other._errorHandler)
{
}

QDbState &QDbState::operator=(const QDbState &rhs)
{
   if (rhs._type > _type)
   {
      _text = rhs._text;
      _type = rhs._type;
      if (rhs._source != Unknown)
         _source = rhs._source;
      if (!rhs._sourceText.isEmpty())
         _sourceText = rhs._sourceText;
      _number = rhs._number;

      if (_errorHandler)
         _errorHandler->onStateChanged(*this);
   }
   if (!_errorHandler)
      _errorHandler = rhs._errorHandler;
   return *this;
}

void QDbState::resetState()
{
   _text.clear();
   _type = Normal;
   _source = Unknown;
   _sourceText.clear();
   _number = 0;
}

QString QDbState::message(int descSize) const
{
   auto message = _text.trimmed();

   if (message.isEmpty())
      return message;

   if (!message.endsWith(QLatin1Char('.')))
      message += QLatin1Char('.');

   if (!_sourceText.isEmpty())
   {
      message += QLatin1String(" ");
      switch (_source)
      {
      case Connection:
         message += tr("Connection") + ": ";
         break;
      case Statement:
         message += tr("Command") + ":\n";
         break;
      case Transaction:
         message += tr("Transaction") + ": ";
         break;
      }
      if (descSize > 0 && _sourceText.length() > descSize)
         message += _sourceText.left(descSize) + QLatin1String("...");
      else
      {
         if (_sourceText.length() > 200)
            message += QLatin1String("\n");
         message += _sourceText;
      }
   }

   return message;
}

QDbState& QDbState::withSourceText(const QString& sourceText)
{
   _sourceText = sourceText;
   return *this;
}
