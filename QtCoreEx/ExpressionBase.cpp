#include "stdafx.h"
#include "ExpressionBase.h"

FunctionPointerExpression* createFunctionExpression(VarPtr(*function)(const IdentifierResolver&), const QString& description, const QString& returnType, const QList<QPair<QString, QString>>& parameters)
{
   auto functionExpr = new FunctionPointerExpression(function);

   functionExpr->setDescription(description);
   functionExpr->setReturnType(returnType);

   for (auto i = 0; i < parameters.count(); ++i)
   {
      auto parType = parameters.at(i).first;
      auto parName = parameters.at(i).second;
      auto mandatory = true;

      if (parName.startsWith(u'?'))
      {
         mandatory = false;
         parName.remove(0, 1);
      }

      functionExpr->addParameter(parName, parType, mandatory);
   }

   return functionExpr;
}

void addResolverFunction(ResolverFunctionMap& functionMap, const QString& name, VarPtr (*function)(const IdentifierResolver&), const QString& returnType,
                         const QList<QPair<QString, QString>>& parameters, const QString& description)
{
   functionMap.insert(name , QPair<ExpressionPtr, bool>(createFunctionExpression(function, description, returnType, parameters), !parameters.isEmpty() && parameters.first().second.isEmpty()));
}

QPair<VarPtr, VarPtr> IdentifierResolver::resolve(const QString& identifier) const
{
   QPair<VarPtr, VarPtr> result;

   if (identifier == "true")
   {
      result.first = newValue(true);
   }
   else if (identifier == "false")
   {
      result.first = newValue(false);
   }
   else if (identifier == "base")
   {
      result.first = _globalVarTree;
   }
   else if (identifier == "this")
   {
      result.first = _this;
   }
   else
   {
      if (_localVarTree)
         result.first = _localVarTree->getItem(identifier);

      if (!result.first)
         result.first = _globalVarTree->getItem(identifier);
   }

   if (result.first)
      return result;

   // Check if the identifier is a function name in the function map

   const auto funcInfo = _functionMap.value(identifier);

   if (funcInfo.first)
   {
      result.second = newFunction(funcInfo.first);
      if (funcInfo.second)
         result.first = _localVarTree ? _localVarTree : _globalVarTree; // Member function
   }

   if (result.second)
      return result;

   if (!_addIfNotFound)
      throw QBasicException(tr("Identifier %1 could not be resolved").arg(identifier));

   if (!_value.isNull())
   {
      result.first = _value->clone();

      if (_localVarTree)
         _localVarTree->setItem(identifier, result.first);
      else
         _globalVarTree->setItem(identifier, result.first);
   }
   else
   {
      result.first.reset();
   }

   return result;
}

bool AbstractExpression::Operator::needsBrackets(const Operator& op, bool onRightSide) const
{
   Q_ASSERT(isValid());
   if (!isValid() || !op.isValid())
      return false; // if op is invalid it might be a value which does not need brackets (if _action is invalid this function doesn't make sense)

   if (op.priority() < priority())
      return true;

   if (op.priority() > priority())
      return false;

   return isLeftToRight() == onRightSide;
}

QPair<VarPtr, VarPtr> AbstractOperatorExpression::resolve(const IdentifierResolver& identifierResolver) const
{
   try
   {
      return op().resolve(identifierResolver, _operands);
   }
   catch (const QBasicException& exception)
   {
      if (typeid(exception) == typeid(QBasicException))
         throw ExpressionException(const_cast<AbstractOperatorExpression*>(this), exception.message());
      throw;
   }
}

QPair<VarPtr, VarPtr> AbstractValueExpression::resolve(const IdentifierResolver& identifierResolver) const
{
   try
   {
      return value().resolve(identifierResolver);
   }
   catch (const QBasicException& exception)
   {
      if (typeid(exception) == typeid(QBasicException))
         throw ExpressionException(const_cast<AbstractValueExpression*>(this), exception.message());
      throw;
   }
}

void AbstractFunctionExpression::addParameter(const QString& parameterName, const QString& parameterType, bool isMandatory)
{
   if (isMandatory)
      _mandatoryParameters.append(QPair<QString, QString>(parameterName, parameterType));
   else
      _optionalParameters.append(QPair<QString, QString>(parameterName, parameterType));
}

QPair<VarPtr, VarPtr> FunctionPointerExpression::resolve(const IdentifierResolver& identifierResolver) const
{
   if (_resolveFunction == nullptr)
      return QPair<VarPtr, VarPtr>();
   return QPair<VarPtr, VarPtr>(_resolveFunction(identifierResolver), VarPtr());
}
