#include "stdafx.h"
#include "Tokenizer.h"

static unsigned toBase256(unsigned char byteValue)
{
   if (byteValue < 79)
      return byteValue + 0x30;
   if (byteValue < 174)
      return byteValue - 79 + 0xA1;
   if (byteValue < 191)
      return byteValue - 174 + 0x391;
   return byteValue - 191 + 0x3A3;
}

static int fromBase256(unsigned c)
{
   auto byteValue = -1;

   if (c >= 0x30 && c <= 0x7E)
      byteValue = c - 0x30;
   else if (c >= 0xA1 && c <= 0xFF)
      byteValue = c - 0xA1 + 79;
   else if (c >= 0x391 && c <= 0x3A1)
      byteValue = c - 0x391 + 174;
   else if (c >= 0x3A3 && c <= 0x3E3)
      byteValue = c - 0x3A3 + 191;

   return byteValue;
}

Tokenizer::Tokenizer(const Context* context) : _context(context), _currentContext(context)
{
}

Tokenizer::Tokenizer(const QString& text, const Context* context) : _data(text), _context(context), _currentContext(context)
{
}

Tokenizer::Tokenizer(QTextStream* is, const Context* context) : _data(is), _context(context), _currentContext(context)
{
}

Tokenizer::Tokenizer(CharacterStreamReader* dataSource, const Context* context) : _data(dataSource), _context(context), _currentContext(context)
{
}

Tokenizer::~Tokenizer()
{
   delete _context;
}

void Tokenizer::setText(const QString& text)
{
   _data.setText(text);
   _currentContext = _context;
}

void Tokenizer::setStream(QTextStream* is)
{
   _data.setStream(is);
   _currentContext = _context;
}

void Tokenizer::setDataSource(CharacterStreamReader* dataSource)
{
   _data.setDataSource(dataSource);
   _currentContext = _context;
}

Token Tokenizer::next()
{
   Token token;

   if (!_currentContext || _data.atEnd())
      return token;

   auto dsStartPos = _data.pos();

   _currentContext = _currentContext->readToken(token, _data);

   while ((_ignoreWhiteSpace && token.is<Token::WhiteSpace>()) || (_ignoreComments && token.is<Token::Comment>()))
   {
      // Discard white space or comments
      token = Token();

      if (!_currentContext || _data.atEnd())
         return token;

      dsStartPos = _data.pos();

      _currentContext = _currentContext->readToken(token, _data);
   }

   // If we have a valid context the token should also be valid.
   Q_ASSERT(!_currentContext || token.isValid());

   if (token.isValid())
   {
      token.impl()->_startPos = dsStartPos;
      token.impl()->_endPos = _data.pos();
      if (_keepParsedText)
         token.impl()->_parsedText = _data.processedChars(0);
   }

   _data.removeProcessedChars();

   return token;
}

QList<Token> Tokenizer::tokenList()
{
   QList<Token> tokenList;

   for (auto token = next(); token.isValid(); token = next())
   {
      tokenList.append(token);
   }

   return tokenList;
}

const Tokenizer::Context* WhiteSpaceContext::readToken(Token& token, TextStreamReader& data) const
{
   auto c = data.peekChar();
   const auto bufferStart = data.bufferPos();

   while (!c.isNull() && (_whiteSpaceChars.isEmpty() && c.isSpace() || _whiteSpaceChars.contains(c)))
   {
      data.skipChar();
      c = data.peekChar();
   }

   if (bufferStart < data.bufferPos())
      token = new Token::WhiteSpace(data.processedChars(bufferStart));

   return _outerContext;
}

const Tokenizer::Context* IdentifierContext::readToken(Token& token, TextStreamReader& data) const
{
   auto c = data.peekChar();

   if (c.isNull())
      return nullptr;

   if (!c.isLetter() && !_allowedSpecialChars.contains(c))
      return _outerContext;

   const auto bufferStart = data.bufferPos();

   while (c.isLetterOrNumber() || _allowedSpecialChars.contains(c))
   {
      data.skipChar();
      c = data.peekChar();
   }

   token = new Token::Identifier(data.processedChars(bufferStart));

   return _outerContext;
}

const Tokenizer::Context* SeparatorContext::readToken(Token& token, TextStreamReader& data) const
{
   auto c = data.peekChar();

   if (c.isNull())
      return nullptr; // End of stream

   data.skipChar();

   token = new Token::Separator(c);

   return _outerContext;
}

const Tokenizer::Context* NumericContext::readToken(Token& token, TextStreamReader& data) const
{
   auto c = data.peekChar();

   if (!c.isDigit())
      return _outerContext;

   auto value = 0LL;
   auto isHex = false;
   auto fracDigits = -1;

   while (!c.isNull())
   {
      if (c == _locale.decimalPoint())
      {
         if (fracDigits != -1)
            break;
         fracDigits = 0;
         // If the next character is not a digit don't eat the decimal point
         if (!data.peekChar(1).isDigit())
            break;
      }
      else if (c == u'x' || c == u'X')
      {
         if (value)
            break;

         isHex = true;
      }
      else if (c != _locale.groupSeparator())
      {
         if (isHex)
         {
            int hexValue;
            if (c.unicode() >= u'A' && c.unicode() <= u'F')
               hexValue = c.unicode() - u'A' + 10;
            else if (c.unicode() >= u'a' && c.unicode() <= u'f')
               hexValue = c.unicode() - u'a' + 10;
            else if (c.isDigit())
               hexValue = c.digitValue();
            else
               break;

            value <<= 4;
            value += hexValue;
         }
         else
         {
            if (!c.isDigit())
               break;

            value *= 10LL;
            value += c.digitValue();
         }

         if (fracDigits != -1)
            fracDigits++;
      }

      data.skipChar();
      c = data.peekChar();
   }

   if (fracDigits > 0)
   {
      token = new Token::Decimal(QDecimal(value, fracDigits));
   }
   else
   {
      token = new Token::Integer(value);
   }

   return _outerContext;
}

const Tokenizer::Context* HexadecimalDataContext::readToken(Token& token, TextStreamReader& data) const
{
   if (data.peekChar(0) != u'0' || data.peekChar(1) != u'x' && data.peekChar(1) != u'X' || data.peekChar(2) != u'"')
      return _outerContext;

   data.skipChar(3);

   QByteArray binaryData;
   auto isLowOrder = false;
   unsigned char byteValue = 0;

   auto c = data.peekChar();
   while (!c.isNull())
   {
      int hexValue;
      if (c.unicode() >= u'A' && c.unicode() <= u'F')
         hexValue = c.unicode() - u'A' + 10;
      else if (c.unicode() >= u'a' && c.unicode() <= u'f')
         hexValue = c.unicode() - u'a' + 10;
      else if (c.isDigit())
         hexValue = c.digitValue();
      else
         break;

      byteValue <<= 4;
      byteValue += hexValue;

      if (isLowOrder)
      {
         binaryData.append((char)byteValue);
         isLowOrder = false;
         byteValue = 0;
      }
      else
      {
         isLowOrder = true;
      }

      data.skipChar();
      c = data.peekChar();
   }

   if (c == u'"')
      data.skipChar();

   token = new Token::Binary(binaryData);

   return _outerContext;
}

const Tokenizer::Context* Base256DataContext::readToken(Token& token, TextStreamReader& data) const
{
   if (data.peekChar(0) != u'0' || data.peekChar(1) != u'b' && data.peekChar(1) != u'B' || data.peekChar(2) != u'"')
      return _outerContext;

   data.skipChar(3);

   QByteArray binaryData;

   auto c = data.peekChar();
   while (!c.isNull())
   {
      const auto byteValue = fromBase256(c.unicode());

      if (byteValue < 0)
         break;

      binaryData.append(byteValue);

      data.skipChar();
      c = data.peekChar();
   }

   if (c == u'"')
      data.skipChar();

   token = new Token::Binary(binaryData);

   return _outerContext;
}

const Tokenizer::Context* StringContext::readToken(Token& token, TextStreamReader& data) const
{
   auto c = data.peekChar();

   //if (c == u'@') // C#
   //   data.skipChar(); // The following string is not c-style!

   if (c != _delimiter)
      return _outerContext;

   data.skipChar();

   QString value;

   while (data.readChar(c)) // The delimiter is part of the token!
   {
      if (c == _delimiter)
      {
         // Peek for another string
         if (data.peekChar() != _delimiter)
            break;

         data.skipChar(); // Skip the delimiter
      }
      else
      {
         value += c;
      }
   }

   token = new Token::Text(value);

   return _outerContext;
}

const Tokenizer::Context* CommentContext::readToken(Token& token, TextStreamReader& data) const
{
   auto d = 0;

   for (; d < _commentDelimiters.count(); ++d)
   {
      auto startDelimiter = _commentDelimiters.at(d).first;
      auto i = 0;

      for (; i < startDelimiter.size(); ++i)
      {
         if (data.peekChar(i) != startDelimiter.at(i))
            break;
      }

      if (i == startDelimiter.size())
         break;
   }

   if (d == _commentDelimiters.count())
      return _outerContext;

   data.skipChar(_commentDelimiters.at(d).first.size());

   const auto bufferStart = data.bufferPos();
   auto endDelimiter = _commentDelimiters.at(d).second;
   auto skipEndDelimiter = false;

   if (endDelimiter.endsWith(u' '))
   {
      skipEndDelimiter = true;
      endDelimiter.chop(1);
   }

   while (!data.peekChar().isNull())
   {
      auto i = 0;

      for (; i < endDelimiter.size(); ++i)
      {
         if (data.peekChar(i) != endDelimiter.at(i))
            break;
      }

      if (i == endDelimiter.size())
         break;

      data.skipChar();
   }

   token = new Token::Comment(data.processedChars(bufferStart));

   if (skipEndDelimiter)
      data.skipChar(endDelimiter.size());

   return _outerContext;
}

ProbingContext::ProbingContext(const Context* outerContext) : Context(outerContext)
{
}

ProbingContext::~ProbingContext()
{
   qDeleteAll(_contexts);
}

const Tokenizer::Context* ProbingContext::readToken(Token& token, TextStreamReader& data) const
{
   const Context* context = nullptr;

   for (auto&& ctx : _contexts)
   {
      context = ctx->readToken(token, data);
      if (!context || token.isValid())
         break;
   }

   if (!token.isValid() && _outerContext != nullptr)
      return _outerContext->readToken(token, data);

   return context;
}

QString toBase256(const QByteArray& data)
{
   QString result;

   for (auto&& byteValue : data)
   {
      result.append(QChar(toBase256(byteValue)));
   }

   return result;
}

QByteArray fromBase256(const QString& data)
{
   QByteArray binaryData;

   for (auto&& c : data)
   {
      const auto byteValue = fromBase256(c.unicode());

      if (byteValue < 0)
         break;

      binaryData.append(byteValue);
   }

   return binaryData;
}
