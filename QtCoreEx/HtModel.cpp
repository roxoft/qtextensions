#include "stdafx.h"
#include "HtModel.h"

#include <QTextStream>

int HtModel::headerCount() const
{
   return rootNode()->attributeCount();
}

QString HtModel::header(int index) const
{
   return rootNode()->attribute(index).toString();
}

void HtModel::setHeader(int index, const QString& name)
{
   rootNode()->setAttribute(index, name);
}

void HtModel::save(QTextStream& os, const QLocaleEx& locale, int fieldCount) const
{
   rootNode()->save(os, locale, fieldCount);
}

void HtModel::load(QTextStream& is, const QLocaleEx& locale, bool header)
{
   rootNode()->removeAllChildNodes();
   if (header)
      rootNode()->removeAttributes(0, rootNode()->attributeCount());
   rootNode()->load(is, locale, header);
}
