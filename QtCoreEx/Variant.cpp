#include "stdafx.h"
#include "Variant.h"
#if QT_VERSION < QT_VERSION_CHECK(5,15,0)
#include <QRegExp>
#endif
#include <QRegularExpression>
#include "QRational.h" // Necessary for mapping old serialized versions
#include "QValue.h"
#include <QMutex>
#include <QThread>
#include <typeindex>
#ifdef Q_CC_GNU
   #include <cxxabi.h>
#endif

//
// class MetaTypeList
//

class MetaTypeList
{
public:
   MetaTypeInfo find(const std::type_info& typeInfo) const { MetaTypeInfo result; _mutex.lock(); result = _typeList.value(typeInfo, MetaTypeInfo()); _mutex.unlock(); return result; }
   MetaTypeInfo findPortable(const QByteArray& typeName) const;
   MetaTypeInfo findInternalName(const QByteArray& internalName) const;
   MetaTypeInfo get(const std::type_info& typeInfo) { MetaTypeInfo metaType = acquire(typeInfo); unlock(); return metaType; }

   MetaTypeInfo& acquire(const std::type_info& typeInfo) { _mutex.lock(); MetaTypeInfo& metaType = _typeList[typeInfo]; if (metaType.constData() == nullptr) metaType.data(); return metaType; }
   void unlock() { _mutex.unlock(); }

public:
   static MetaTypeList& getList() { static MetaTypeList typeList; return typeList; }

private:
   mutable QMutex _mutex;
   QMap<std::type_index, MetaTypeInfo> _typeList;
};

MetaTypeInfo MetaTypeList::findPortable(const QByteArray& typeName) const
{
   MetaTypeInfo result;

   _mutex.lock();
   for (auto it = _typeList.begin(); it != _typeList.end(); ++it)
   {
      if (it.value().constData()->portableName == typeName)
      {
         result = it.value();
         break;
      }
   }
   _mutex.unlock();

   return result;
}

MetaTypeInfo MetaTypeList::findInternalName(const QByteArray& internalName) const
{
   MetaTypeInfo result;

   _mutex.lock();
   for (auto it = _typeList.begin(); it != _typeList.end(); ++it)
   {
#if defined Q_CC_MSVC
      if (it.value().constData()->raw_name == internalName)
#else
      if (it.key().name() == internalName)
#endif
      {
         result = it.value();
         break;
      }
   }
   _mutex.unlock();

   return result;
}

//
// Type name handling
//

namespace std
{

#if defined Q_CC_MSVC
   inline const char* internal_type_name(const type_info& typeInfo) { return typeInfo.raw_name(); }
#elif defined Q_CC_GNU
   inline const char* internal_type_name(const type_info& typeInfo) { return typeInfo.name(); }
#else
   inline const char* internal_type_name(const type_info& typeInfo) { return typeInfo.name(); }
#endif

   static const char* stripFromTypeName(const char* typeName, const char* pattern)
   {
      const char* result = typeName;

      while (*result == *pattern && *pattern)
      {
         result++;
         pattern++;
      }

      if (*pattern)
         return typeName;
      return result;
   }

#if defined Q_CC_MSVC
   static QByteArray demangled_type_name(const type_info& typeInfo)
   {
      auto typeName = typeInfo.name();
      typeName = stripFromTypeName(typeName, "enum ");
      typeName = stripFromTypeName(typeName, "struct ");
      return stripFromTypeName(typeName, "class ");
   }
#elif defined Q_CC_GNU
   static QByteArray demangled_type_name(const type_info& typeInfo)
   {
      int         status;
      char        *realname = abi::__cxa_demangle(typeInfo.name(), nullptr, nullptr, &status);
      QByteArray  result(realname);

      free(realname);

      return result;
   }
#else
   static QByteArray demangled_type_name(const type_info& typeInfo)
   {
      return typeInfo.name();
   }
#endif
}

QByteArray portable_type_name(const std::type_info& typeInfo)
{
   if (typeInfo == typeid(int))
      return "int32";
   if (typeInfo == typeid(unsigned int))
      return "unsigned int32";
   if (typeInfo == typeid(long long))
      return "int64";
   if (typeInfo == typeid(unsigned long long))
      return "unsigned int64";
   if (typeInfo == typeid(long))
   {
      if (sizeof(long) == 4)
         return "int32";
      return "int64";
   }
   if (typeInfo == typeid(unsigned long))
   {
      if (sizeof(unsigned long) == 4)
         return "unsigned int32";
      return "unsigned int64";
   }
   if (typeInfo == typeid(float))
      return "real32";
   if (typeInfo == typeid(double))
      return "real64";
   if (typeInfo == typeid(short))
      return "int16";
   if (typeInfo == typeid(unsigned short))
      return "unsigned int16";
   if (typeInfo == typeid(char))
      return "int8";
   if (typeInfo == typeid(unsigned char))
      return "unsigned int8";
   return std::demangled_type_name(typeInfo);
}

QString plain_type_name(const std::type_info& typeInfo)
{
   return portable_type_name(typeInfo);
}

//
// Register as QVariant meta type
//

static bool variantRegisterMetaType()
{
   auto ok = true;

#if QT_VERSION_MAJOR < 6
   qRegisterMetaTypeStreamOperators<Variant>("Variant");
   if (!QMetaType::registerComparators<Variant>())
      ok = false;
#endif
   if (!QMetaType::registerConverter(&Variant::toString))
      ok = false;
   if (!QMetaType::registerConverter(&Variant::toDate))
      ok = false;
   if (!QMetaType::registerConverter(&Variant::toTime))
      ok = false;
   if (!QMetaType::registerConverter(&Variant::toDateTime))
      ok = false;

   return ok;
}

static auto variantMetaTypeRegistered = variantRegisterMetaType();

QDataStream& operator<<(QDataStream& out, const Variant& myObj)
{
   out << myObj.serialized();
   return out;
}

QDataStream& operator>>(QDataStream& in, Variant& myObj)
{
   QByteArray data;
   in >> data;
   myObj.deserialize(data);
   return in;
}

//
// Basic conversion routines
//

static bool convert(QString& lhs, bool rhs, const QLocaleEx& locale)
{
   lhs = locale.toString(rhs);
   return true;
}

static bool convert(bool& lhs, int rhs, const QLocaleEx& locale)
{
   Q_UNUSED(locale);

   lhs = (rhs != 0);
   return true;
}

static bool convert(unsigned int& lhs, int rhs, const QLocaleEx& locale)
{
   Q_UNUSED(locale);

   lhs = (unsigned int)rhs;
   return true;
}

static bool convert(long long int& lhs, int rhs, const QLocaleEx& locale)
{
   Q_UNUSED(locale);

   lhs = (long long int)rhs;
   return true;
}

static bool convert(unsigned long long int& lhs, int rhs, const QLocaleEx& locale)
{
   Q_UNUSED(locale);

   lhs = (unsigned long long int)rhs;
   return true;
}

static bool convert(double& lhs, int rhs, const QLocaleEx& locale)
{
   Q_UNUSED(locale);

   lhs = (double)rhs;
   return true;
}

static bool convert(QTime& lhs, int rhs, const QLocaleEx& locale)
{
   Q_UNUSED(locale);

   lhs = QTime::fromMSecsSinceStartOfDay(rhs * 1000);

   return true;
}

static bool convert(QDate& lhs, int rhs, const QLocaleEx& locale)
{
   Q_UNUSED(locale);

   lhs = QDate::fromJulianDay(rhs);

   return true;
}

static bool convert(QDateTime& lhs, int rhs, const QLocaleEx& locale)
{
   Q_UNUSED(locale);

   lhs = QDateTime::fromSecsSinceEpoch(rhs);

   return true;
}

static bool convert(QString& lhs, int rhs, const QLocaleEx& locale)
{
   lhs = locale.toString(rhs);
   return true;
}

static bool convert(bool& lhs, unsigned int rhs, const QLocaleEx& locale)
{
   Q_UNUSED(locale);

   lhs = (rhs != 0);
   return true;
}

static bool convert(int& lhs, unsigned int rhs, const QLocaleEx& locale)
{
   Q_UNUSED(locale);

   lhs = (int)rhs;
   return true;
}

static bool convert(long long int& lhs, unsigned int rhs, const QLocaleEx& locale)
{
   Q_UNUSED(locale);

   lhs = (long long int)rhs;
   return true;
}

static bool convert(unsigned long long int& lhs, unsigned int rhs, const QLocaleEx& locale)
{
   Q_UNUSED(locale);

   lhs = (unsigned long long int)rhs;
   return true;
}

static bool convert(double& lhs, unsigned int rhs, const QLocaleEx& locale)
{
   Q_UNUSED(locale);

   lhs = (double)rhs;
   return true;
}

static bool convert(QTime& lhs, unsigned int rhs, const QLocaleEx& locale)
{
   Q_UNUSED(locale);

   lhs = QTime::fromMSecsSinceStartOfDay(rhs * 1000);

   return true;
}

static bool convert(QDate& lhs, unsigned int rhs, const QLocaleEx& locale)
{
   Q_UNUSED(locale);

   lhs = QDate::fromJulianDay(rhs);

   return true;
}

static bool convert(QDateTime& lhs, unsigned int rhs, const QLocaleEx& locale)
{
   Q_UNUSED(locale);

   lhs = QDateTime::fromSecsSinceEpoch(rhs);

   return true;
}

static bool convert(QString& lhs, unsigned int rhs, const QLocaleEx& locale)
{
   lhs = locale.toString(rhs);
   return true;
}

static bool convert(bool& lhs, long long int rhs, const QLocaleEx& locale)
{
   Q_UNUSED(locale);

   lhs = (rhs != 0LL);
   return true;
}

static bool convert(int& lhs, long long int rhs, const QLocaleEx& locale)
{
   Q_UNUSED(locale);

   lhs = (int)rhs;
   return true;
}

static bool convert(unsigned int& lhs, long long int rhs, const QLocaleEx& locale)
{
   Q_UNUSED(locale);

   lhs = (unsigned int)(unsigned long long int)rhs;
   return true;
}

static bool convert(unsigned long long int& lhs, long long int rhs, const QLocaleEx& locale)
{
   Q_UNUSED(locale);

   lhs = (unsigned long long int)rhs;
   return true;
}

static bool convert(double& lhs, long long int rhs, const QLocaleEx& locale)
{
   Q_UNUSED(locale);

   lhs = (double)rhs;
   return true;
}

static bool convert(QTime& lhs, long long int rhs, const QLocaleEx& locale)
{
   Q_UNUSED(locale);

   lhs = QTime::fromMSecsSinceStartOfDay(rhs * 1000);

   return true;
}

static bool convert(QDate& lhs, long long int rhs, const QLocaleEx& locale)
{
   Q_UNUSED(locale);

   lhs = QDate::fromJulianDay(rhs);

   return true;
}

static bool convert(QDateTime& lhs, long long int rhs, const QLocaleEx& locale)
{
   Q_UNUSED(locale);

   lhs = QDateTime::fromMSecsSinceEpoch(rhs);

   return true;
}

static bool convert(QString& lhs, long long int rhs, const QLocaleEx& locale)
{
   lhs = locale.toString(rhs);
   return true;
}

static bool convert(bool& lhs, unsigned long long int rhs, const QLocaleEx& locale)
{
   Q_UNUSED(locale);

   lhs = (rhs != 0LL);
   return true;
}

static bool convert(int& lhs, unsigned long long int rhs, const QLocaleEx& locale)
{
   Q_UNUSED(locale);

   lhs = (int)(long long int)rhs;
   return true;
}

static bool convert(unsigned int& lhs, unsigned long long int rhs, const QLocaleEx& locale)
{
   Q_UNUSED(locale);

   lhs = (unsigned int)rhs;
   return true;
}

static bool convert(long long int& lhs, unsigned long long rhs, const QLocaleEx& locale)
{
   Q_UNUSED(locale);

   lhs = (long long int)rhs;
   return true;
}

static bool convert(double& lhs, unsigned long long int rhs, const QLocaleEx& locale)
{
   Q_UNUSED(locale);

   lhs = (double)rhs;
   return true;
}

static bool convert(QTime& lhs, unsigned long long int rhs, const QLocaleEx& locale)
{
   Q_UNUSED(locale);

   lhs = QTime::fromMSecsSinceStartOfDay(rhs * 1000);

   return true;
}

static bool convert(QDate& lhs, unsigned long long int rhs, const QLocaleEx& locale)
{
   Q_UNUSED(locale);

   lhs = QDate::fromJulianDay(rhs);

   return true;
}

static bool convert(QDateTime& lhs, unsigned long long int rhs, const QLocaleEx& locale)
{
   Q_UNUSED(locale);

   lhs = QDateTime::fromMSecsSinceEpoch((qint64)rhs);

   return true;
}

static bool convert(QString& lhs, unsigned long long int rhs, const QLocaleEx& locale)
{
   lhs = locale.toString(rhs);
   return true;
}

static bool convert(int& lhs, double rhs, const QLocaleEx& locale)
{
   Q_UNUSED(locale);

   lhs = qRound(rhs);
   return true;
}

static bool convert(unsigned int& lhs, double rhs, const QLocaleEx& locale)
{
   Q_UNUSED(locale);

   lhs = (unsigned int)qRound64(rhs);
   return true;
}

static bool convert(long long int& lhs, double rhs, const QLocaleEx& locale)
{
   Q_UNUSED(locale);

   lhs = qRound64(rhs);
   return true;
}

static bool convert(QTime& lhs, double rhs, const QLocaleEx& locale)
{
   Q_UNUSED(locale);

   int d, h, m, s;

   if (std::toTime(rhs, d, h, m, s) >= 0.5)
      s++;

   lhs = QTime(h, m, s);

   return true;
}

static bool convert(QDate& lhs, double rhs, const QLocaleEx& locale)
{
   Q_UNUSED(locale);

   lhs = QDate::fromJulianDay((qint64)rhs);

   return true;
}

static bool convert(QDateTime& lhs, double rhs, const QLocaleEx& locale)
{
   Q_UNUSED(locale);

   int d, h, m, s, ms;

   rhs = std::toTime(rhs, d, h, m, s);
   rhs *= 1000;
   ms = (int)rhs;
   rhs -= ms;
   if (rhs >= 0.5)
      ms++;

   lhs = QDateTime(QDate::fromJulianDay(d), QTime(h, m, s, ms));

   return true;
}

static bool convert(QString& lhs, double rhs, const QLocaleEx& locale)
{
   lhs = locale.toString(rhs);
   return true;
}

static bool convert(bool& lhs, const QString& rhs, const QLocaleEx& locale)
{
   bool ok;

   lhs = locale.toBool(rhs, &ok);
   return ok;
}

static bool convert(int& lhs, const QString& rhs, const QLocaleEx& locale)
{
   bool ok;

   lhs = locale.toInt(rhs, &ok);
   return ok;
}

static bool convert(unsigned int& lhs, const QString& rhs, const QLocaleEx& locale)
{
   bool ok;

   lhs = locale.toUInt(rhs, &ok);
   return ok;
}

static bool convert(long long int& lhs, const QString& rhs, const QLocaleEx& locale)
{
   bool ok;

   lhs = locale.toLongLong(rhs, &ok);
   return ok;
}

static bool convert(unsigned long long int& lhs, const QString& rhs, const QLocaleEx& locale)
{
   bool ok;

   lhs = locale.toULongLong(rhs, &ok);
   return ok;
}

static bool convert(double& lhs, const QString& rhs, const QLocaleEx& locale)
{
   bool ok;

   lhs = locale.toDouble(rhs, &ok);
   return ok;
}

static bool convert(QDate& lhs, const QString& rhs, const QLocaleEx& locale)
{
   if (locale.language() == QLocale::C)
      lhs = QDate::fromString(rhs, Qt::ISODate);
   else
      lhs = locale.toDate(rhs);
   return lhs.isValid();
}

static bool convert(QTime& lhs, const QString& rhs, const QLocaleEx& locale)
{
   if (locale.language() == QLocale::C)
      lhs = QTime::fromString(rhs, Qt::ISODate);
   else
      lhs = locale.toTime(rhs);
   return lhs.isValid();
}

static bool convert(QDateTime& lhs, const QString& rhs, const QLocaleEx& locale)
{
   if (locale.language() == QLocale::C)
      lhs = QDateTime::fromString(rhs, Qt::ISODate);
   else
      lhs = locale.toDateTime(rhs);
   return lhs.isValid();
}

static bool convert(QString& lhs, const QText& rhs, const QLocaleEx& locale)
{
   Q_UNUSED(locale);

   lhs = rhs;
   return true;
}

static bool convert(QText& lhs, const QString& rhs, const QLocaleEx& locale)
{
   Q_UNUSED(locale);

   lhs = rhs;
   return true;
}

static bool convert(bool& lhs, const QByteArray& rhs, const QLocaleEx& locale)
{
   bool ok;

   lhs = locale.toBool(QString::fromUtf8(rhs), &ok);
   return ok;
}

static bool convert(int& lhs, const QByteArray& rhs, const QLocaleEx& locale)
{
   Q_UNUSED(locale);

   bool ok;

   lhs = rhs.toInt(&ok);
   return ok;
}

static bool convert(unsigned int& lhs, const QByteArray& rhs, const QLocaleEx& locale)
{
   Q_UNUSED(locale);

   bool ok;

   lhs = rhs.toUInt(&ok);
   return ok;
}

static bool convert(double& lhs, const QByteArray& rhs, const QLocaleEx& locale)
{
   Q_UNUSED(locale);

   bool ok;

   lhs = rhs.toDouble(&ok);
   return ok;
}

static bool convert(QDate& lhs, const QByteArray& rhs, const QLocaleEx& locale)
{
#ifndef QT_NO_CAST_FROM_ASCII
   return convert(lhs, QString(rhs), locale);
#else
   return convert(lhs, QString::fromUtf8(rhs), locale);
#endif
}

static bool convert(QTime& lhs, const QByteArray& rhs, const QLocaleEx& locale)
{
#ifndef QT_NO_CAST_FROM_ASCII
   return convert(lhs, QString(rhs), locale);
#else
   return convert(lhs, QString::fromUtf8(rhs), locale);
#endif
}

static bool convert(QDateTime& lhs, const QByteArray& rhs, const QLocaleEx& locale)
{
#ifndef QT_NO_CAST_FROM_ASCII
   return convert(lhs, QString(rhs), locale);
#else
   return convert(lhs, QString::fromUtf8(rhs), locale);
#endif
}

static bool convert(QString& lhs, const QByteArray& rhs, const QLocaleEx& locale)
{
   Q_UNUSED(locale);

#ifndef QT_NO_CAST_FROM_ASCII
   lhs = rhs;
#else
   lhs = QString::fromUtf8(rhs);
#endif
   return true;
}

static bool convert(QDateTime& lhs, const QDate& rhs, const QLocaleEx& locale)
{
   Q_UNUSED(locale);

#if QT_VERSION < QT_VERSION_CHECK(5,14,0)
   lhs = QDateTime(rhs);
#else
   lhs = QDateTime(rhs.startOfDay());
#endif
   return true;
}

static bool convert(QString& lhs, const QDate& rhs, const QLocaleEx& locale)
{
   if (locale.language() == QLocale::C)
      lhs = rhs.toString(Qt::ISODate);
   else
      lhs = locale.toString(rhs);
   return true;
}

static bool convert(int& lhs, const QTime& rhs, const QLocaleEx& locale)
{
   Q_UNUSED(locale);

   lhs = QTime().msecsTo(rhs);
   return true;
}

static bool convert(unsigned int& lhs, const QTime& rhs, const QLocaleEx& locale)
{
   Q_UNUSED(locale);

   lhs = (unsigned int)QTime().msecsTo(rhs);
   return true;
}

static bool convert(double& lhs, const QTime& rhs, const QLocaleEx& locale)
{
   Q_UNUSED(locale);

   lhs = (double)QTime().msecsTo(rhs) / (24 * 60 * 60 * 1000);
   return true;
}

static bool convert(QString& lhs, const QTime& rhs, const QLocaleEx& locale)
{
   if (locale.language() == QLocale::C)
      lhs = rhs.toString(Qt::ISODate);
   else
      lhs = locale.toString(rhs);
   return true;
}

static bool convert(QDate& lhs, const QDateTime& rhs, const QLocaleEx& locale)
{
   Q_UNUSED(locale);

   lhs = rhs.date();
   return true;
}

static bool convert(QTime& lhs, const QDateTime& rhs, const QLocaleEx& locale)
{
   Q_UNUSED(locale);

   lhs = rhs.time();
   return true;
}

static bool convert(QString& lhs, const QDateTime& rhs, const QLocaleEx& locale)
{
   if (locale.language() == QLocale::C)
      lhs = rhs.toString(Qt::ISODate);
   else
      lhs = locale.toString(rhs);
   return true;
}

static bool convert(QString& lhs, const QVariant& rhs, const QLocaleEx& locale)
{
   lhs = locale.toString(rhs);
   return true;
}

static bool convert(QVariant& lhs, const QText& rhs, const QLocaleEx&)
{
   lhs.setValue((const QString&)rhs);
   return true;
}

static bool convert(int& lhs, Qt::ItemFlag rhs, const QLocaleEx&)
{
   lhs = (int)rhs;
   return true;
}

static bool convert(QVariant& lhs, Qt::ItemFlag rhs, const QLocaleEx&)
{
   lhs.setValue((int)rhs);
   return true;
}

static bool convert(Qt::ItemFlag& lhs, const QVariant& rhs, const QLocaleEx&)
{
   lhs = (Qt::ItemFlag)rhs.toInt();
#if QT_VERSION_MAJOR < 6
   return rhs.canConvert(QVariant::Int);
#else
   return rhs.canConvert(QMetaType::fromType<int>());
#endif
}

static bool convert(int& lhs, Qt::AlignmentFlag rhs, const QLocaleEx&)
{
   lhs = (int)rhs;
   return true;
}

static bool convert(QVariant& lhs, Qt::AlignmentFlag rhs, const QLocaleEx&)
{
   lhs.setValue((int)rhs);
   return true;
}

static bool convert(Qt::AlignmentFlag& lhs, const QVariant& rhs, const QLocaleEx&)
{
   lhs = (Qt::AlignmentFlag)rhs.toInt();
#if QT_VERSION_MAJOR < 6
   return rhs.canConvert(QVariant::Int);
#else
   return rhs.canConvert(QMetaType::fromType<int>());
#endif
}

//
// Global helper functions
//

MetaTypeInfo getAcceleratedMetaTypeInfo(QAtomicInt& protect, MetaTypeInfo& metaTypeInfo, const std::type_info& typeInfo)
{
   MetaTypeInfo result;

   // Set result to metaTypeInfo (metaTypeInfo is the cache for meta type information)

   while (!protect.testAndSetAcquire(0, 1))
      QThread::yieldCurrentThread();
   result = metaTypeInfo;
   protect.fetchAndStoreRelease(0);

   // Lookup the meta type info if information is old or not yet cached

   if (result.constData() == nullptr || result.constData()->old)
   {
      // Set result to the found meta type information

      result = MetaTypeList::getList().find(typeInfo);
      if (result.constData() == nullptr)
      {
         result.data()->portableName = portable_type_name(typeInfo); // Make result valid and set at least the portable name
#if defined Q_CC_MSVC
         result.data()->raw_name = typeInfo.raw_name();
#endif
      }

      // Store result in metaTypeInfo so it can be reused

      while (!protect.testAndSetAcquire(0, 1))
         QThread::yieldCurrentThread();
      metaTypeInfo = result;
      protect.fetchAndStoreRelease(0);
   }

   return result;
}

//
// VariantTypeInfo specializations
//

MetaTypeInfo VariantTypeInfo<QVariant>::getTypeInfo()
{
   static QAtomicInt    protect;
   static MetaTypeInfo  typeInfo;

   return getAcceleratedMetaTypeInfo(protect, typeInfo, typeid(QVariant));
}

//
// class Variant
//
const Variant Variant::null;

Variant::Variant(const char *value)
{
#if QT_VERSION_MAJOR < 6
   static_assert(sizeof(VariantTypeInfo<QString>) <= sizeof(Variant), "VariantTypeInfo<QString> is larger than Variant");
   new(this) VariantTypeInfo<QString>(QString(value));
#else
   static_assert(sizeof(VariantTypeInfo<QString>) > sizeof(Variant), "VariantTypeInfo<QString> is not larger than Variant");
   _object = new VariantTypeInfo<QString>(QString(value));
#endif
}

#ifdef _WIN32
Variant::Variant(const wchar_t *value)
{
#if QT_VERSION_MAJOR < 6
   static_assert(sizeof(VariantTypeInfo<QString>) <= sizeof(Variant), "VariantTypeInfo<QString> is larger than Variant");
   new(this) VariantTypeInfo<QString>(QString::fromUtf16((const ushort *)value));
#else
   static_assert(sizeof(VariantTypeInfo<QString>) > sizeof(Variant), "VariantTypeInfo<QString> is not larger than Variant");
   _object = new VariantTypeInfo<QString>(QString::fromUtf16((const char16_t *)value));
#endif
}
#endif

Variant::Variant(const ushort *value)
{
#if QT_VERSION_MAJOR < 6
   static_assert(sizeof(VariantTypeInfo<QString>) <= sizeof(Variant), "VariantTypeInfo<QString> is larger than Variant");
   new(this) VariantTypeInfo<QString>(QString::fromUtf16(value));
#else
   static_assert(sizeof(VariantTypeInfo<QString>) > sizeof(Variant), "VariantTypeInfo<QString> is not larger than Variant");
   _object = new VariantTypeInfo<QString>(QString::fromUtf16((const char16_t *)value));
#endif
}

#ifdef _WIN32
template <>
#endif
Variant::Variant(const QVariant& variant)
{
   if (variant.canConvert<Variant>())
   {
      assign(variant.value<Variant>());
   }
   else
   {
      switch (variant.userType())
      {
      case QMetaType::UnknownType:
      {
         break;
      }
      case QMetaType::Bool:
      {
         static_assert(sizeof(VariantTypeInfo<bool>) <= sizeof(Variant), "VariantTypeInfo<bool> is larger than Variant");
         new(this) VariantTypeInfo<bool>(variant.toBool());
         break;
      }
      case QMetaType::Char:
      {
         static_assert(sizeof(VariantTypeInfo<QChar>) <= sizeof(Variant), "VariantTypeInfo<QChar> is larger than Variant");
         new(this) VariantTypeInfo<QChar>(variant.toChar());
         break;
      }
      case QMetaType::Int:
      {
         static_assert(sizeof(VariantTypeInfo<int>) <= sizeof(Variant), "VariantTypeInfo<int> is larger than Variant");
         new(this) VariantTypeInfo<int>(variant.toInt());
         break;
      }
      case QMetaType::UInt:
      {
         static_assert(sizeof(VariantTypeInfo<unsigned int>) <= sizeof(Variant), "VariantTypeInfo<unsigned int> is larger than Variant");
         new(this) VariantTypeInfo<unsigned int>(variant.toUInt());
         break;
      }
      case QMetaType::LongLong:
      {
         if (sizeof(VariantTypeInfo<long long int>) <= sizeof(Variant))
            new(this) VariantTypeInfo<long long int>(variant.toLongLong());
         else
            _object = new VariantTypeInfo<long long int>(variant.toLongLong());
         break;
      }
      case QMetaType::ULongLong:
      {
         if (sizeof(VariantTypeInfo<unsigned long long int>) <= sizeof(Variant))
            new(this) VariantTypeInfo<unsigned long long int>(variant.toULongLong());
         else
            _object = new VariantTypeInfo<unsigned long long int>(variant.toULongLong());
         break;
      }
      case QMetaType::Float:
      case QMetaType::Double:
      {
         if (sizeof(VariantTypeInfo<double>) <= sizeof(Variant))
            new(this) VariantTypeInfo<double>(variant.toDouble());
         else
            _object = new VariantTypeInfo<double>(variant.toDouble());
         break;
      }
      case QMetaType::QDate:
      {
         if (sizeof(VariantTypeInfo<QDate>) <= sizeof(Variant))
            new(this) VariantTypeInfo<QDate>(variant.toDate());
         else
            _object = new VariantTypeInfo<QDate>(variant.toDate());
         break;
      }
      case QMetaType::QTime:
      {
         static_assert(sizeof(VariantTypeInfo<QTime>) <= sizeof(Variant), "VariantTypeInfo<QTime> is larger than Variant");
         new(this) VariantTypeInfo<QTime>(variant.toTime());
         break;
      }
      case QMetaType::QDateTime:
      {
         if (sizeof(VariantTypeInfo<QDateTime>) <= sizeof(Variant))
            new(this) VariantTypeInfo<QDateTime>(variant.toDateTime());
         else
            _object = new VariantTypeInfo<QDateTime>(variant.toDateTime());
         break;
      }
      case QMetaType::QString:
      {
#if QT_VERSION_MAJOR < 6
         static_assert(sizeof(VariantTypeInfo<QString>) <= sizeof(Variant), "VariantTypeInfo<QString> is larger than Variant");
         new(this) VariantTypeInfo<QString>(variant.toString());
#else
         static_assert(sizeof(VariantTypeInfo<QString>) > sizeof(Variant), "VariantTypeInfo<QString> is not larger than Variant");
         _object = new VariantTypeInfo<QString>(variant.toString());
#endif
         break;
      }
      case QMetaType::QByteArray:
      {
#if QT_VERSION_MAJOR < 6
         static_assert(sizeof(VariantTypeInfo<QByteArray>) <= sizeof(Variant), "VariantTypeInfo<QByteArray> is larger than Variant");
         new(this) VariantTypeInfo<QByteArray>(variant.toByteArray());
#else
         static_assert(sizeof(VariantTypeInfo<QString>) > sizeof(Variant), "VariantTypeInfo<QString> is not larger than Variant");
         _object = new VariantTypeInfo<QString>(variant.toByteArray());
#endif
         break;
      }
      default:
      {
         if (sizeof(VariantTypeInfo<QVariant>) <= sizeof(Variant))
            new(this) VariantTypeInfo<QVariant>(variant);
         else
            _object = new VariantTypeInfo<QVariant>(variant);
         break;
      }
      }
   }
}

void Variant::clear()
{
   if (_vMap != nullptr)
      reinterpret_cast<VariantMeta*>(this)->~VariantMeta();
   else if (_object != nullptr)
      _object->destroy();
   _vMap = nullptr;
   _object = nullptr;
}

bool Variant::canConvert(const std::type_info& from, const std::type_info& to)
{
   const auto result = MetaTypeList::getList().find(from);

   if (result.constData() == nullptr)
      return false;

   return result.constData()->conversionMap.contains(to);
}

bool Variant::operator==(const Variant& other) const
{
   const VariantMeta* left = metaObject();
   const VariantMeta* right = other.metaObject();

   if (left && right)
      return typeid(*left) == typeid(*right) && left->isEqualTo(*right);

   return left == nullptr && right == nullptr;
}

bool Variant::operator<(const Variant& other) const
{
   const VariantMeta* left = metaObject();
   const VariantMeta* right = other.metaObject();

   if (left && right)
   {
      const std::type_index leftType(typeid(*left));
      const std::type_index rightType(typeid(*right));

      if (leftType < rightType)
         return true;
      if (rightType < leftType)
         return false;
      return left->isLessThan(*right);
   }

   return left == nullptr && right != nullptr;
}

bool Variant::match(const Variant& other, Qt::MatchFlags flags) const
{
   const uint matchType = flags & 0x0F;

   if (matchType == Qt::MatchExactly)
      return operator==(other); // Variant based matching

   // QString based matching
   Qt::CaseSensitivity  cs = flags & Qt::MatchCaseSensitive ? Qt::CaseSensitive : Qt::CaseInsensitive;
   QString              t1 = to<QString>();
   QString              t2 = other.to<QString>();

#if QT_VERSION < QT_VERSION_CHECK(5,15,0)
   if (matchType == Qt::MatchRegExp)
#else
   if (matchType == Qt::MatchRegularExpression)
#endif
      return QRegularExpression(t2, flags & Qt::MatchCaseSensitive ? QRegularExpression::NoPatternOption : QRegularExpression::CaseInsensitiveOption).match(t1).hasMatch();
   if (matchType == Qt::MatchWildcard)
#if QT_VERSION < QT_VERSION_CHECK(5,15,0)
      return QRegExp(t2, cs, QRegExp::Wildcard).exactMatch(t1);
#else
      return QRegularExpression(QRegularExpression::wildcardToRegularExpression(t2), flags & Qt::MatchCaseSensitive ? QRegularExpression::NoPatternOption : QRegularExpression::CaseInsensitiveOption).match(t1).hasMatch();
#endif
   if (matchType == Qt::MatchStartsWith)
      return t1.startsWith(t2, cs);
   if (matchType == Qt::MatchEndsWith)
      return t1.endsWith(t2, cs);
   if (matchType == Qt::MatchFixedString)
      return t1.compare(t2, cs) == 0;
   return t1.contains(t2, cs);
}

QString Variant::toString(bool *ok, const QLocaleEx& locale) const { return to<QString>(ok, locale); }

QText Variant::toText(bool *ok, const QLocaleEx &locale) const { return to<QText>(ok, locale); }

bool Variant::toBool(bool *ok, const QLocaleEx &locale) const { return to<bool>(ok, locale); }

int Variant::toInt(bool *ok, const QLocaleEx &locale) const { return to<int>(ok, locale); }

uint Variant::toUInt(bool *ok, const QLocaleEx &locale) const { return to<unsigned int>(ok, locale); }

qint64 Variant::toInt64(bool *ok, const QLocaleEx &locale) const { return to<long long int>(ok, locale); }

quint64 Variant::toUInt64(bool *ok, const QLocaleEx &locale) const { return to<unsigned long long int>(ok, locale); }

double Variant::toDouble(bool *ok, const QLocaleEx &locale) const { return to<double>(ok, locale); }

QDecimal Variant::toDecimal(bool *ok, const QLocaleEx &locale) const { return to<QDecimal>(ok, locale); }

QByteArray Variant::toByteArray(bool *ok, const QLocaleEx &locale) const { return to<QByteArray>(ok, locale); }

QDate Variant::toDate(bool *ok, const QLocaleEx &locale) const { return to<QDate>(ok, locale); }

QDateEx Variant::toDateEx(bool *ok, const QLocaleEx &locale) const { return to<QDateEx>(ok, locale); }

QTime Variant::toTime(bool *ok, const QLocaleEx &locale) const { return to<QTime>(ok, locale); }

QTimeEx Variant::toTimeEx(bool *ok, const QLocaleEx &locale) const { return to<QTimeEx>(ok, locale); }

QDateTime Variant::toDateTime(bool *ok, const QLocaleEx &locale) const { return to<QDateTime>(ok, locale); }

QDateTimeEx Variant::toDateTimeEx(bool *ok, const QLocaleEx &locale) const { return to<QDateTimeEx>(ok, locale); }

QVariant Variant::toQVariant() const { return to<QVariant>(nullptr, defaultLocale()); }

QByteArray Variant::serialized() const
{
   SerStream os;

   os << os.version();
   os << *this;

   return os.internalBuffer();
}

bool Variant::deserialize(const QByteArray& serVariant)
{
   if (serVariant.isEmpty())
      return false;

   int         version = 0;
   SerStream   is(serVariant);

   if (serVariant.startsWith(".") || serVariant.startsWith("0."))
   {
      // This was either the version without SerStream version number or with the version number 0
      rpsWarning("Deprecated Variant serialization found");
      if (serVariant[0] == '0')
         is.readRaw(1LL);
   }
   else
      version = is.readInt32();

   if (is.hasError())
   {
      rpsError("Deserialization version not found in %s", serVariant.data());
      return false;
   }

   is.setVersion(version);
   is >> *this;

   return !is.hasError();
}

SerStream& operator<<(SerStream &stream, const Variant &value)
{
   QByteArray typeName;

   if (stream.version() == 1)
   {
      typeName = std::internal_type_name(value.type());
   }
   else
   {
      stream << '1'; // Current variant serialization version number
      if (value.metaObject())
         typeName = value.metaObject()->portableName();
      else
         typeName = "void";
   }

   stream << typeName;

   if (value.metaObject())
      value.metaObject()->serialize(&stream);

   return stream;
}

static const std::type_info& fromOldType(const QByteArray& typeName)
{
   if (typeName == ".?AVVariant@@")
      return typeid(void);
   if (typeName == ".?AVVBool@@")
      return typeid(bool);
   if (typeName == ".?AVVInt@@")
      return typeid(int);
   if (typeName == ".?AVVUInt@@")
      return typeid(unsigned int);
   if (typeName == ".?AVVLongLong@@")
      return typeid(long long int);
   if (typeName == ".?AVVULongLong@@")
      return typeid(unsigned long long int);
   if (typeName == ".?AVVDouble@@")
      return typeid(double);
   if (typeName == ".?AVVQDecimal@@")
      return typeid(QDecimal);
   if (typeName == ".?AVVQString@@")
      return typeid(QString);
   if (typeName == ".?AVVQText@@")
      return typeid(QText);
   if (typeName == ".?AVVQByteArray@@")
      return typeid(QByteArray);
   if (typeName == ".?AVVQDate@@")
      return typeid(QDate);
   if (typeName == ".?AVVQTime@@")
      return typeid(QTime);
   if (typeName == ".?AVVQDateTime@@")
      return typeid(QDateTime);
   if (typeName == ".?AVVQVariant@@")
      return typeid(QVariant);
   if (typeName == ".?AVQRational@@")
      return typeid(QRational);
   return typeid(MetaTypeInfo);
}

SerStream& operator>>(SerStream &stream, Variant &value)
{
   value.clear();

   QByteArray     type;
   MetaTypeInfo   typeInfo;

   if (stream.version() > 1)
   {
      char variantSerVersion = '\0';

      stream >> variantSerVersion;

      if (variantSerVersion != '0' && variantSerVersion != '1')
      {
         stream.setError(__FUNCTION__, QString("Unknown Variant serialization version %1").arg((int)variantSerVersion));
         return stream;
      }

      stream >> type;

      if (variantSerVersion == '0')
      {
         if (type == "__int64" || type == "long long")
            type = "int64";
         else if (type == "unsigned __int64" || type == "unsigned long long")
            type = "unsigned int64";
         else if (type == "double")
            type = "real64";
         else if (type.startsWith("enum "))
            type.remove(0, 5);
         else if (type.startsWith("struct "))
            type.remove(0, 7);
      }

      if (type == "int")
      {
         type = "int32";
      }
      else if (type == "unsigned int")
      {
         type = "unsigned int32";
      }

      if (type.isEmpty())
      {
         rpsError("Deserialization type not found");
         stream.setError(__FUNCTION__, "Deserialization type not found");
         return stream;
      }

      if (type == "void")
         return stream; // It is a NULL variant

      typeInfo = MetaTypeList::getList().findPortable(type);
   }
   else
   {
      if (stream.version() > 0)
      {
         stream >> type;

         if (type.isEmpty())
         {
            rpsError("Deserialization type is empty");
            stream.setError(__FUNCTION__, "Deserialization type is empty");
            return stream;
         }

         if (type == std::internal_type_name(typeid(void)))
            return stream; // It is a NULL variant

         typeInfo = MetaTypeList::getList().findInternalName(type);
      }
      else
      {
         for (;;)
         {
            int charValue = stream.readCharValue();

            if (charValue < 0 || charValue == ':')
               break;

            type.append((char)(unsigned char)charValue);
         }

         // Map old types
         const auto& typeStruct = fromOldType(type);

         if (typeStruct == typeid(void))
            return stream; // It is a NULL variant

         if (typeStruct == typeid(MetaTypeInfo))
         {
            rpsError("Deserialization type not found");
            stream.setError(__FUNCTION__, "Deserialization type not found");
            return stream;
         }

         typeInfo = MetaTypeList::getList().find(typeStruct);
      }
   }

   if (typeInfo.constData() && typeInfo.constData()->construct)
   {
      VariantMeta* object;

      if (sizeof(Variant) < (size_t)typeInfo.constData()->size)
      {
         object = typeInfo.constData()->construct(nullptr);
         value._object = object;
      }
      else
      {
         object = typeInfo.constData()->construct(&value);
      }

      if (!object->deserialize(&stream))
         stream.setError(__FUNCTION__, "Object deserialization failed");

      return stream;
   }

   if (typeInfo.constData())
   {
      rpsError("Deserialization: Type %s not registered", type.data());
      stream.setError(__FUNCTION__, QString("Deserialization: Type %s not registered").arg(QLatin1String(type.data())));
   }
   else
   {
      rpsError("Deserialization: Variant meta type not found: %s", type.data());
      stream.setError(__FUNCTION__, QString("Deserialization: Variant meta type not found: %s").arg(QLatin1String(type.data())));
   }

   return stream;
}

void Variant::_registerType(const std::type_info& typeId, VariantMeta* (*construct)(void*), void (*destroy)(VariantMeta*), int size, QByteArray portableName)
{
   if (portableName.isEmpty())
      portableName = portable_type_name(typeId);

   MetaTypeInfoData* typeInfo = MetaTypeList::getList().acquire(typeId).data();

   typeInfo->construct = construct;
   typeInfo->destroy = destroy;
   typeInfo->size = size;
   typeInfo->portableName = portableName;
#if defined Q_CC_MSVC
   typeInfo->raw_name = typeId.raw_name();
#endif

   MetaTypeList::getList().unlock();
}

void Variant::_registerConversion(const std::type_info& leftType, const std::type_info& rightType, GenericConversion genConv)
{
   MetaTypeInfoData* typeInfo = MetaTypeList::getList().acquire(rightType).data();

   if (leftType == typeid(QVariant))
      typeInfo->qVariantConversion = genConv;
   else
      typeInfo->conversionMap[leftType] = genConv;

   MetaTypeList::getList().unlock();
}

template <>
long Variant::to<long>(bool* ok, const QLocaleEx& locale) const
{
#ifdef Q_PROCESSOR_X86_64
   return (long)to<long long int>(ok, locale);
#else
   return (long)to<int>(ok, locale);
#endif
}

template <>
short Variant::to<short>(bool* ok, const QLocaleEx& locale) const { return (short)to<int>(ok, locale); }

template <>
char Variant::to<char>(bool* ok, const QLocaleEx& locale) const { return (char)to<int>(ok, locale); }

template <>
unsigned long Variant::to<unsigned long>(bool* ok, const QLocaleEx& locale) const
{
#ifdef Q_PROCESSOR_X86_64
   return (unsigned long)to<unsigned long long int>(ok, locale);
#else
   return (unsigned long)to<unsigned int>(ok, locale);
#endif
}

template <>
unsigned short Variant::to<unsigned short>(bool* ok, const QLocaleEx& locale) const { return (unsigned short)to<unsigned int>(ok, locale); }

template <>
unsigned char Variant::to<unsigned char>(bool* ok, const QLocaleEx& locale) const { return (unsigned char)to<unsigned int>(ok, locale); }

template <>
QString Variant::to<QString>(bool* ok, const QLocaleEx& locale) const
{
   if (ok)
      *ok = true;

   const auto mo = metaObject();

   if (const auto vType = dynamic_cast<const VariantTypeInfo<QString>*>(mo))
      return vType->value();

   QString value;

   if (mo && !mo->convertInto(&value, typeid(QString), locale))
   {
      if (ok)
         *ok = false;
      else
         rpsError("Unable to convert Variant from %s to %s.", mo->portableName().data(), portable_type_name(typeid(QString)).data());
      return QString("Variant(%1)?").arg(QString(mo->portableName()));
   }

   return value;
}

template <>
QText Variant::to<QText>(bool* ok, const QLocaleEx& locale) const
{
   if (ok)
      *ok = true;

   const auto mo = metaObject();

   if (const auto vType = dynamic_cast<const VariantTypeInfo<QText>*>(mo))
      return vType->value();

   QText value;

   if (mo && !mo->convertInto(&value, typeid(QText), locale))
   {
      if (ok)
         *ok = false;
      else
         rpsError("Unable to convert Variant from %s to %s.", mo->portableName().data(), portable_type_name(typeid(QText)).data());
      return QText("Variant(%1)?").arg(QString(mo->portableName()));
   }

   return value;
}

template<>
QVariant Variant::to<QVariant>(bool* ok, const QLocaleEx& locale) const
{
   if (metaObject() == nullptr)
      return QVariant();

   QVariant other;

   if (metaObject()->convertToQVariant(other))
   {
      if (ok)
         *ok = true;
      return other;
   }

   other = QVariant::fromValue(*this);

   if (ok)
      *ok = true;

   return other;
}

BEGIN_VARIANT_REGISTRATION
   // Register types
   Variant::registerType<bool>();
   Variant::registerType<int>();
   Variant::registerType<unsigned int>();
   Variant::registerType<long long int>("long long int");
   Variant::registerType<unsigned long long int>("unsigned long long int");
   Variant::registerType<double>();
   Variant::registerType<QString>();
   Variant::registerType<QText>();
   Variant::registerType<QByteArray>();
   Variant::registerType<QDate>();
   Variant::registerType<QTime>();
   Variant::registerType<QDateTime>();
   Variant::registerType<QVariant>();
   Variant::registerType<Qt::ItemFlag>();
   Variant::registerType<Qt::AlignmentFlag>();
   Variant::registerType<QSize>();
   Variant::registerType<Qt::GlobalColor>();

   // Register primitive conversions
   Variant::registerConversion<QString, bool>();
   Variant::registerConversion<bool, int>();
   Variant::registerConversion<unsigned int, int>();
   Variant::registerConversion<long long int, int>();
   Variant::registerConversion<unsigned long long int, int>();
   Variant::registerConversion<double, int>();
   Variant::registerConversion<QTime, int>();
   Variant::registerConversion<QDate, int>();
   Variant::registerConversion<QDateTime, int>();
   Variant::registerConversion<QString, int>();
   Variant::registerConversion<bool, unsigned int>();
   Variant::registerConversion<int, unsigned int>();
   Variant::registerConversion<long long int, unsigned int>();
   Variant::registerConversion<unsigned long long int, unsigned int>();
   Variant::registerConversion<double, unsigned int>();
   Variant::registerConversion<QTime, unsigned int>();
   Variant::registerConversion<QDate, unsigned int>();
   Variant::registerConversion<QDateTime, unsigned int>();
   Variant::registerConversion<QString, unsigned int>();
   Variant::registerConversion<bool, long long int>();
   Variant::registerConversion<int, long long int>();
   Variant::registerConversion<unsigned int, long long int>();
   Variant::registerConversion<unsigned long long int, long long int>();
   Variant::registerConversion<double, long long int>();
   Variant::registerConversion<QTime, long long int>();
   Variant::registerConversion<QDate, long long int>();
   Variant::registerConversion<QDateTime, long long int>();
   Variant::registerConversion<QString, long long int>();
   Variant::registerConversion<bool, unsigned long long int>();
   Variant::registerConversion<int, unsigned long long int>();
   Variant::registerConversion<unsigned int, unsigned long long int>();
   Variant::registerConversion<long long int, unsigned long long int>();
   Variant::registerConversion<double, unsigned long long int>();
   Variant::registerConversion<QTime, unsigned long long int>();
   Variant::registerConversion<QDate, unsigned long long int>();
   Variant::registerConversion<QDateTime, unsigned long long int>();
   Variant::registerConversion<QString, unsigned long long int>();
   Variant::registerConversion<int, double>();
   Variant::registerConversion<unsigned int, double>();
   Variant::registerConversion<long long int, double>();
   Variant::registerConversion<QTime, double>();
   Variant::registerConversion<QDate, double>();
   Variant::registerConversion<QDateTime, double>();
   Variant::registerConversion<QString, double>();
   Variant::registerConversion<bool, QString>();
   Variant::registerConversion<int, QString>();
   Variant::registerConversion<unsigned int, QString>();
   Variant::registerConversion<long long int, QString>();
   Variant::registerConversion<unsigned long long int, QString>();
   Variant::registerConversion<double, QString>();
   Variant::registerConversion<int, QTime>();
   Variant::registerConversion<unsigned int, QTime>();
   Variant::registerConversion<double, QTime>();
   Variant::registerConversion<QTime, QString>();
   Variant::registerConversion<QString, QTime>();
   Variant::registerConversion<QDate, QString>();
   Variant::registerConversion<QString, QDate>();
   Variant::registerConversion<QDateTime, QString>();
   Variant::registerConversion<QDateTime, QDate>();
   Variant::registerConversion<QDate, QDateTime>();
   Variant::registerConversion<QTime, QDateTime>();
   Variant::registerConversion<QString, QDateTime>();

   // Conversions to QVariant
   Variant::registerToQVariantConversion<bool>();
   Variant::registerToQVariantConversion<int>();
   Variant::registerToQVariantConversion<unsigned int>();
   Variant::registerToQVariantConversion<long long int>();
   Variant::registerToQVariantConversion<unsigned long long int>();
   Variant::registerToQVariantConversion<double>();
   Variant::registerToQVariantConversion<QByteArray>();
   Variant::registerToQVariantConversion<QString>();
   Variant::registerToQVariantConversion<QTime>();
   Variant::registerToQVariantConversion<QDate>();
   Variant::registerToQVariantConversion<QDateTime>();

   // Conversions from QVariant
   // !! There is no need to register conversion from QVariant for knwon types !!
   // !! A Variant never holds a QVariant if it can be converted into a known type in the Variant(QVariant) constructor !!
   Variant::registerConversion<QString, QVariant>();
   Variant::registerQVariantConversion<QSize>();

   // Flag Conversions
   Variant::registerConversion<int, Qt::ItemFlag>();
   Variant::registerConversion<QVariant, Qt::ItemFlag>();
   Variant::registerConversion<Qt::ItemFlag, QVariant>();
   Variant::registerConversion<int, Qt::AlignmentFlag>();
   Variant::registerConversion<QVariant, Qt::AlignmentFlag>();
   Variant::registerConversion<Qt::AlignmentFlag, QVariant>();

   // QText conversions
   // Except for conversion to QString QText uses the string conversion functions!
   Variant::registerConversion<bool, QText>();
   Variant::registerConversion<int, QText>();
   Variant::registerConversion<unsigned int, QText>();
   Variant::registerConversion<double, QText>();
   Variant::registerConversion<QDate, QText>();
   Variant::registerConversion<QTime, QText>();
   Variant::registerConversion<QDateTime, QText>();
   Variant::registerConversion<QString, QText>();
   Variant::registerConversion<QText, QString>();
   Variant::registerConversion<QVariant, QText>();

   // QByteArray conversions
   Variant::registerConversion<bool, QByteArray>();
   Variant::registerConversion<int, QByteArray>();
   Variant::registerConversion<unsigned int, QByteArray>();
   Variant::registerConversion<double, QByteArray>();
   Variant::registerConversion<QDate, QByteArray>();
   Variant::registerConversion<QTime, QByteArray>();
   Variant::registerConversion<QDateTime, QByteArray>();
   Variant::registerConversion<QString, QByteArray>();
END_VARIANT_REGISTRATION
