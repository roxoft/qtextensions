#ifndef DOUBLE_H
#define DOUBLE_H

#include <QtCore>
#include "architectures.h"

#ifdef __x86__
/* splitDouble splits a double into its normalized components.

number is the input parameter.
mantissa, exponent and sign are output parameters.

If the resulting mantissa is 0xFFFFFFFFFFFFFFFF the double is not valid. If the exponent is then zero the double is infinite (positive or negative). If the exponent is not zero the double is not a number (NaN).

In all other cases the double is calculated by:

mantissa * 2 ^ exponent
*/
void splitDouble(const double& number, quint64& mantissa, int& exponent, bool& sign);
#endif // __x86__

inline bool mantissaIsValid(const quint64& mantissa) { return mantissa != (quint64)(-1LL); }
inline bool isNaN(const quint64& mantissa, const int& exponent) { return mantissa == (quint64)(-1LL) && exponent; }
inline bool isInfinite(const quint64& mantissa, const int& exponent) { return mantissa == (quint64)(-1LL) && exponent == 0; }

/* Changes the exponent from fromBase representation to toBase representation adjusting the mantissa accordingly.

If mantissa is not a number (equal to 0xFFFFFFFFFFFFFFFF) the parameters are unchanged.

The precision parameter specifies the maximum precision of the mantissa plus 1 (1ULL << 52 for a double).
*/
void transformExponent(quint64& mantissa, int& exponent, int fromBase, int toBase, const quint64& precision);

#endif // DOUBLE_H
