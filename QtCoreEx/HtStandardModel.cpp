#include "stdafx.h"
#include "HtStandardModel.h"
#include "HtStandardNode.h"

HtStandardModel::~HtStandardModel()
{
   delete _root;
}

const HtNode *HtStandardModel::rootNode() const
{
   static HtStandardNode defaultNode;

   if (_root == NULL)
      return &defaultNode;
   return _root;
}

HtNode *HtStandardModel::rootNode()
{
   if (_root == NULL)
      _root = new HtStandardNode(this);
   return _root;
}

Variant HtStandardModel::defaultAttribute(int role) const
{
   if (role == Qt::ItemFlagsRole)
      return (int)defaultItemFlags();
   return HtItemModel::defaultAttribute(role);
}

void HtStandardModel::setRootNode(HtNode *root)
{
   if (_root != root)
   {
      onBeginResetModel();
      delete _root;
      if (root && (root->parent() || root->model()))
      {
         const auto node = new HtStandardNode;
         *node = root;
         root = node;
      }
      _root = root;
      if (_root)
         _root->setModel(this);
      onEndResetModel();
   }
}
