#include "stdafx.h"
#include "TestManager.h"
#include <TestFramework.h>
#include <QLibrary>
#include <QFileInfo>
#include <QDir>
#include <QMetaMethod>
#include "CsvParser.h"
#if defined(Q_OS_WIN)
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#endif

//
// UnitTestLibrary
//

typedef void (*CreateTestSuites)(QList<TestFramework*>*);
typedef void (*DeleteTestSuite)(TestFramework*);

class UnitTestLibrary
{
   Q_DISABLE_COPY(UnitTestLibrary)
public:
   UnitTestLibrary();
   ~UnitTestLibrary();

   QString load(const QString& fileName);

   bool isLoaded() const { return _createTestSuites && _deleteTestSuite; }

   QString name() const;

   QList<TestSuite> createTestSuites() const;
   void deleteTestSuites(const QList<TestSuite>& testSuites) const;

private:
#if defined(Q_OS_WIN)
   HMODULE           _testLib = NULL;
#else
   QLibrary _testLib;
#endif
   CreateTestSuites  _createTestSuites = nullptr;
   DeleteTestSuite   _deleteTestSuite = nullptr;
};

UnitTestLibrary::UnitTestLibrary()
{
}

UnitTestLibrary::~UnitTestLibrary()
{
#if defined(Q_OS_WIN)
   if (_testLib)
      FreeLibrary(_testLib);
#endif
}

QString UnitTestLibrary::load(const QString& fileName)
{
#if defined(Q_OS_WIN)
   _testLib = LoadLibraryEx((const wchar_t*)QDir::toNativeSeparators(fileName).utf16(), NULL, LOAD_WITH_ALTERED_SEARCH_PATH);

   if (_testLib == NULL)
      return qt_error_string(GetLastError());

   _createTestSuites = (CreateTestSuites)GetProcAddress(_testLib, "createTestSuites");
   if (_createTestSuites == NULL)
      return qt_error_string(GetLastError());

   _deleteTestSuite = (DeleteTestSuite)GetProcAddress(_testLib, "deleteTestSuite");
   if (_deleteTestSuite == NULL)
      return qt_error_string(GetLastError());
#else
   _testLib.setFileName(fileName);
   if (!_testLib.load())
      return _testLib.errorString();

   _createTestSuites = (CreateTestSuites)_testLib.resolve("createTestSuites");
   if (_createTestSuites == NULL)
      return _testLib.errorString();

   _deleteTestSuite = (DeleteTestSuite)_testLib.resolve("deleteTestSuite");
   if (_deleteTestSuite == NULL)
      return _testLib.errorString();
#endif

   return QString();
}

QString UnitTestLibrary::name() const
{
   QString result;

#if defined(Q_OS_WIN)
   wchar_t buffer[2048];

   buffer[GetModuleFileName(_testLib, buffer, 2047)] = '\0';

   result = QString::fromUtf16((const char16_t *)buffer);
#else
   return _testLib.fileName();
#endif

   return result;
}

QList<TestSuite> UnitTestLibrary::createTestSuites() const
{
   QList<TestSuite> testSuites;

   if (isLoaded())
   {
      QList<TestFramework*> suiteList;

      _createTestSuites(&suiteList);

      for (auto&& suite : suiteList)
      {
         testSuites.append(suite);
      }
   }

   return testSuites;
}

void UnitTestLibrary::deleteTestSuites(const QList<TestSuite>& testSuites) const
{
   if (isLoaded())
   {
      for (auto&& testSuite : testSuites)
      {
         _deleteTestSuite(testSuite._testFramework);
      }
   }
}

//
// TestSuite
//

TestSuite::TestSuite(TestFramework* testFramework) : _testFramework(testFramework), _metaObject(nullptr)
{
   if (_testFramework)
      _metaObject = _testFramework->metaObject();
}

TestSuite::TestSuite(const TestSuite& other) : _testFramework(other._testFramework), _metaObject(other._metaObject)
{
}

TestSuite::~TestSuite()
{
}

TestSuite& TestSuite::operator=(const TestSuite& other)
{
   _testFramework = other._testFramework;
   _metaObject = other._metaObject;
   return *this;
}

bool TestSuite::isValid() const
{
   return _testFramework && _metaObject;
}

QString TestSuite::name() const
{
   if (_testFramework == nullptr)
      return QString();

   QString testSuiteName = _testFramework->objectName();

   if (testSuiteName.isEmpty() && _metaObject)
      testSuiteName = QString::fromLatin1(_metaObject->className());

   return testSuiteName;
}

int TestSuite::methodCount() const
{
   if (!_metaObject)
      return 0;
   return _metaObject->methodCount() - _metaObject->methodOffset();
}

QString TestSuite::methodSignature(int index) const
{
   if (!_metaObject)
      return QString();
   return QString::fromLatin1(_metaObject->method(_metaObject->methodOffset() + index).methodSignature());
}

void TestSuite::setDataDirectory(const QString& path)
{
   if (_testFramework)
      _testFramework->setDataDir(path);
}

void TestSuite::setParentWindow(QWidget* parent)
{
   if (_testFramework)
      _testFramework->setParentWindow(parent);
}

void TestSuite::init(FunctionStatus status)
{
   if (_testFramework)
   {
      QObject::connect(_testFramework, &TestFramework::onError, [&status](const QString& msg){ status.addError(msg); });
      QObject::connect(_testFramework, &TestFramework::onWarning, [&status](const QString& msg){ status.addWarning(msg); });
      try
      {
         _testFramework->init();
      }
      catch (const TestException& ex)
      {
         status.addError(ex.message());
      }
      catch (...)
      {
         status.addError(tr("Unknow exception!"));
      }
      QObject::disconnect(_testFramework, SIGNAL(onError(const QString&)), 0, 0);
      QObject::disconnect(_testFramework, SIGNAL(onWarning(const QString&)), 0, 0);
   }
}

void TestSuite::deinit(FunctionStatus status)
{
   if (_testFramework)
   {
      QObject::connect(_testFramework, &TestFramework::onError, [&status](const QString& msg){ status.addError(msg); });
      QObject::connect(_testFramework, &TestFramework::onWarning, [&status](const QString& msg){ status.addWarning(msg); });
      try
      {
         _testFramework->deinit();
      }
      catch (const TestException& ex)
      {
         status.addError(ex.message());
      }
      catch (...)
      {
         status.addError(tr("Unknow exception!"));
      }
      QObject::disconnect(_testFramework, SIGNAL(onError(const QString&)), 0, 0);
      QObject::disconnect(_testFramework, SIGNAL(onWarning(const QString&)), 0, 0);
   }
}

void TestSuite::executeMethod(int index, FunctionStatus status)
{
   if (index >= 0 && index < methodCount())
   {
      QObject::connect(_testFramework, &TestFramework::onError, [&status](const QString& msg){ status.addError(msg); });
      QObject::connect(_testFramework, &TestFramework::onWarning, [&status](const QString& msg){ status.addWarning(msg); });

      try
      {
         _metaObject->method(_metaObject->methodOffset() + index).invoke(_testFramework);
      }
      catch (const TestException& ex)
      {
         status.addError(ex.message());
      }
      catch (...)
      {
         status.addError(tr("Unknow exception!"));
      }

      QObject::disconnect(_testFramework, SIGNAL(onError(const QString&)), 0, 0);
      QObject::disconnect(_testFramework, SIGNAL(onWarning(const QString&)), 0, 0);
   }
   else
      status.addError(tr("Method not found."));
}

//
// Test Manager
//

TestModule::TestModule()
{
}

TestModule::~TestModule()
{
   clear();
}

void TestModule::setParentWindow(QWidget* parent)
{
   _parent = parent;
   setParentWindow();
}

void TestModule::setDataDirectory(const QString& path)
{
   _dataDir = path;
   if (!_dataDir.isEmpty())
      _dataDir = QFileInfo(_dataDir).absoluteFilePath();
   setDataDirectory();
}

FunctionStatus TestModule::connect(QString libPath, const QString& dataPath)
{
   if (libPath.isEmpty())
      return FunctionStatus();

   if (!dataPath.isEmpty())
      setDataDirectory(dataPath);

   QFileInfo fileInfo(libPath);

   if (fileInfo.path() != ".")
      libPath = fileInfo.absoluteFilePath();

   FunctionStatus status(QString("Connecting to %1").arg(libPath));

   clear();

   _library = new UnitTestLibrary;

   auto errorMsg = _library->load(libPath);

   if (errorMsg.isEmpty())
   {
      _testSuites = _library->createTestSuites();
      setParentWindow();
      setDataDirectory();
   }
   else
   {
      status.addError(errorMsg);
      delete _library;
      _library = nullptr;
   }

   return status;
}

QString TestModule::libraryFile() const
{
   if (_library)
      return _library->name();
   return QString();
}

void TestModule::clear()
{
   if (_library)
   {
      _library->deleteTestSuites(_testSuites);
      _testSuites.clear();
      delete _library;
      _library = nullptr;
   }
}

void TestModule::setParentWindow()
{
   for (auto testSuite : _testSuites)
   {
      testSuite.setParentWindow(_parent);
   }
}

void TestModule::setDataDirectory()
{
   for (auto testSuite : _testSuites)
   {
      testSuite.setDataDirectory(_dataDir);
   }
}

QStringList TestManager::loadedModules()
{
   QStringList modules;

#if defined(Q_OS_WIN)
   auto hPsapi = LoadLibrary(L"Psapi");

   if (hPsapi == NULL)
      return QStringList();

   auto EnumProcessModules = (BOOL(*)(HANDLE, HMODULE*, DWORD, LPDWORD))GetProcAddress(hPsapi, "EnumProcessModules");

   if (EnumProcessModules == nullptr)
      return QStringList();

   int bufSize = 0;
   HMODULE* hMods = nullptr;
   HANDLE hProcess = GetCurrentProcess();
   DWORD cbNeeded = 1024 * sizeof(HMODULE);

   // Get a list of all the modules in this process.

   while (bufSize * sizeof(HMODULE) < cbNeeded)
   {
      bufSize = cbNeeded / sizeof(HMODULE);
      hMods = new HMODULE[bufSize];
      if (!EnumProcessModules(hProcess, hMods, cbNeeded, &cbNeeded))
         return QStringList();
   }

   for (unsigned int i = 0; i < (cbNeeded / sizeof(HMODULE)); i++)
   {
      TCHAR szModName[2048];

      // Get the full path to the module's file.

      if (GetModuleFileName(hMods[i], szModName, sizeof(szModName) / sizeof(*szModName)))
      {
         if (GetLastError() != ERROR_INSUFFICIENT_BUFFER)
            modules.append(QString::fromUtf16((const char16_t*)szModName));
      }
   }

   delete[] hMods;
#endif

   return modules;
}

TestManager::TestManager()
{
}

TestManager::~TestManager()
{
   qDeleteAll(_testModules);
}

void TestManager::setParentWindow(QWidget* parent)
{
   _parent = parent;
   for (auto&& testModule : _testModules)
   {
      testModule->setParentWindow(parent);
   }
}

TestModule* TestManager::addModule(FunctionStatus status, const QString& modulePath, const QString& dataPath)
{
   auto testModule = new TestModule;

   testModule->setParentWindow(_parent);

   status.transferMessagesFrom(testModule->connect(modulePath, dataPath));

   if (status.hasError())
   {
      delete testModule;
      testModule = nullptr;
   }
   else
      _testModules.append(testModule);

   return testModule;
}

QList<TestModule*> TestManager::addModulesFromFile(FunctionStatus status, const QString& fileName)
{
   QList<TestModule*> loadedModules;

   QFile file(fileName);

   if (file.open(QFile::ReadOnly | QFile::Text))
   {
      QTextStream is(&file);

#if QT_VERSION_MAJOR < 6
      is.setCodec("UTF-8");
#else
      is.setEncoding(QStringConverter::Utf8);
#endif

      CsvParser parser(&is, u';', u'"');

      while (true)
      {
         auto moduleInfo = parser.readRecord();

         if (moduleInfo.isEmpty())
            break;

         loadedModules.append(addModule(status, moduleInfo[0], moduleInfo.value(1)));
      }
   }

   return loadedModules;
}
