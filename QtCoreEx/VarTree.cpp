#include "stdafx.h"
#include "VarTree.h"
#include "ExpressionBase.h"
#include <QValue>

VarTree::Variable::Variable(const QString& typeName) : _type(typeName), _lookup(&_itemNames, &lookupKey)
{
}

VarTree::Variable::Variable(const Variant& value, const QString& typeName)
   : _basicType(BasicType::Value), _type(typeName.isEmpty() ? valueTypeName(value.type()) : typeName), _lookup(&_itemNames, &lookupKey), _value(value)
{
}

VarTree::Variable::Variable(ExpressionPtr f) : _basicType(BasicType::Function), _lookup(&_itemNames, &lookupKey), _f(f)
{
}

VarTree::Variable::Variable(const Variable& other)
   : _basicType(other._basicType), _type(other._type), _items(other._items), _itemNames(other._itemNames), _lookup(&_itemNames, &lookupKey), _value(other._value), _f(other._f)
{
}

VarTree::Variable::~Variable() // Cannot be inlined because of _f
{
}

VarTree::Variable& VarTree::Variable::operator=(const VarTree::Variable& other)
{
   _basicType = other.basicType();
   _type = other.typeName();
   _lookup.reset();
   _items = other._items;
   _itemNames = other.itemNames();
   _value = other.value();
   _f = other.function();

   return *this;
}

VarPtr VarTree::Variable::newCollection(const QString& typeName) const
{
   auto var = new Variable;

   var->_type = typeName;

   return var;
}

VarPtr VarTree::Variable::newFromValue(const Variant& value, const QString& typeName) const
{
   auto var = new Variable;

   var->_basicType = BasicType::Value;
   var->_type = typeName.isEmpty() ? valueTypeName(value.type()) : typeName;
   var->_value = value;

   return var;
}

VarPtr VarTree::Variable::newFromFunction(ExpressionPtr f) const
{
   if (!f)
      return VarPtr();

   auto var = new Variable;

   var->_basicType = BasicType::Function;
   var->_f = f;

   return var;
}

IVariable::BasicType VarTree::Variable::basicType() const
{
   return _basicType;
}

QString VarTree::Variable::typeName() const
{
   return _basicType == BasicType::Function ? QString("Function") : _type;
}

VarPtr VarTree::Variable::clone() const
{
   auto var = new Variable;

   var->_basicType = _basicType;
   var->_type = _type;
   for (auto&& item : _items)
      var->_items.append(item->clone());
   var->_itemNames = _itemNames;
   var->_value = _value;
   var->_f = _f;

   return var;
}

void VarTree::Variable::assignFrom(VarPtr other)
{
   if (!other)
      return;

   _basicType = other->basicType();
   _type = other->typeName();
   _lookup.reset();
   _items.clear();
   for (auto i = 0; i < other->itemCount(); ++i)
   {
      _items.append(other->getItem(i)->clone());
   }
   _itemNames = other->itemNames();
   _value = other->value();
   _f = other->function();
}

Variant VarTree::Variable::value() const
{
   return _value;
}

void VarTree::Variable::setValue(const Variant& value)
{
   _value = value;
}

QString VarTree::Variable::valueTypeName(const std::type_info& type)
{
   if (type == typeid(bool))
      return "Bool";
   if (type == typeid(QDate) || type == typeid(QTime) || type == typeid(QDateTime)
      || type == typeid(QDateEx) || type == typeid(QTimeEx) || type == typeid(QDateTimeEx))
      return "Date";
   if (type == typeid(QDecimal) || type == typeid(double) || type == typeid(QValue))
      return "Decimal";
   if (type == typeid(int) || type == typeid(unsigned int) || type == typeid(long long) || type == typeid(unsigned long long))
      return "Integer";
   if (type == typeid(QByteArray))
      return "Binary";
   return "String";
}

ExpressionPtr VarTree::Variable::function() const
{
   return _f;
}

void VarTree::Variable::setFunction(ExpressionPtr f)
{
   _f = f;
}

int VarTree::Variable::indexOfName(const QString& name) const
{
   return _lookup.indexOf(name);
}

QString VarTree::Variable::getName(int index) const
{
   return _itemNames.value(index);
}

VarPtr VarTree::Variable::getItem(int index)
{
   return _items.value(index);
}

VarPtr VarTree::Variable::getCollection(const QString& name)
{
   const auto index = _lookup.findOrAdd(name);

   if (index == _itemNames.count())
   {
      _itemNames.insert(index, name);
      _items.insert(index, VarPtr(new Variable));
   }

   return _items.at(index);
}

VarPtr VarTree::Variable::getCollection(int index)
{
   if (index > _items.count() || index < 0)
      return VarPtr();

   if (index == _items.count())
      _items.insert(index, VarPtr(new Variable));

   return _items.at(index);
}

void VarTree::Variable::setItem(const QString& name, const VarPtr& item)
{
   if (name.isEmpty())
   {
      setItem(_items.count(), item);
      return;
   }

   const auto index = _lookup.findOrAdd(name);

   if (index == _itemNames.count())
   {
      _itemNames.insert(index, name);
      _items.insert(index, item);
   }
   else
   {
      _items[index] = item;
   }
}

void VarTree::Variable::setItem(int index, const VarPtr& item)
{
   if (index > _items.count() || index < 0)
      return;

   if (index == _items.count())
      _items.insert(index, item);
   else
      _items[index] = item;
}

VarPtr VarTree::Variable::takeItem(const QString& name)
{
   const auto index = _lookup.take(name);

   if (index == -1)
      return VarPtr();

   _itemNames.removeAt(index);
   return _items.takeAt(index);
}

VarPtr VarTree::Variable::takeItem(int index)
{
   if (index >= _items.count() || index < 0)
      return VarPtr();

   if (index < _itemNames.count())
   {
      _lookup.reset();
      _itemNames.removeAt(index);
   }

   return _items.takeAt(index);
}

void VarTree::Variable::removeItem(const QString& name)
{
   const auto index = _lookup.take(name);

   if (index != -1)
   {
      _itemNames.removeAt(index);
      _items.removeAt(index);
   }
}

void VarTree::Variable::removeItem(int index)
{
   if (index < _items.count() && index >= 0)
   {
      if (index < _itemNames.count())
      {
         _lookup.reset();
         _itemNames.removeAt(index);
      }
      _items.removeAt(index);
   }
}

void VarTree::Variable::setItems(const QStringList& itemNames, const QList<VarPtr>& items)
{
   _lookup.reset();
   _itemNames = itemNames.mid(0, items.count());
   _items = items;
}

VarTree::VarTree() : _data(new Variable)
{
}

VarTree::VarTree(const VarPtr& dictionary): _data(dictionary)
{
   if (!_data)
      _data = new Variable;
}

Variant VarTree::value(const QStringList& pathList) const
{
   const auto var = variable(pathList);

   if (var)
      return var->value();

   return Variant::null;
}

QString VarTree::type(const QString& path) const
{
   auto var = variable(path);

   if (var)
      return var->typeName();

   return QString();
}

VarPtr VarTree::variable(const QString& path) const
{
   return variable(toPathList(path));
}

VarPtr VarTree::variable(const QStringList& pathList) const
{
   return getVariable(pathList, false, nullptr);
}

QList<int> VarTree::indexHierarchy(const QString& path) const
{
   return indexHierarchy(toPathList(path));
}

QList<int> VarTree::indexHierarchy(const QStringList& pathList) const
{
   QList<int> result;

   if (!getVariable(pathList, false, &result) && !result.isEmpty())
      result.last() = -1;

   return result;
}

QList<VarPtr> VarTree::hierarchy(const QString& path) const
{
   return hierarchy(toPathList(path));
}

QList<VarPtr> VarTree::hierarchy(const QStringList& pathList) const
{
   QList<VarPtr> result;
   auto indexes = indexHierarchy(pathList);
   auto var = _data;

   result.append(var);
   for (auto&& index : indexes)
   {
      var = var->getItem(index);
      result.append(var);
      if (!var)
         break;
   }

   return result;
}

int VarTree::rowCount() const
{
   int rowCount = 0;

   _data->iterateTree([&rowCount](const QStringList& , IVariable* varDef){ if (varDef->basicType() == IVariable::BasicType::Collection && rowCount < varDef->itemCount()) rowCount = varDef->itemCount(); });

   return rowCount;
}

VarPtr VarTree::newDictionaryAt(const QStringList& pathList, const QString& type)
{
   const auto item = _data->newCollection(type);

   set(pathList, item);

   return item;
}

void VarTree::set(const QString& path, const ExpressionPtr& expression)
{
   set(path, _data->newFromFunction(expression));
}

void VarTree::set(const QStringList& pathList, const VarPtr& variable)
{
   if (!variable || pathList.isEmpty())
      return;

   auto dict = getVariable(pathList.mid(0, pathList.count() - 1), true, nullptr);

   if (dict)
      dict->setItem(pathList.last(), variable);
}

void VarTree::set(const QStringList& pathList, const Variant& value, const QString& type)
{
   set(pathList, _data->newFromValue(value, type));
}

void VarTree::set(const QStringList& pathList, const Variant& value, const std::type_info& type)
{
   set(pathList, _data->newFromValue(value, Variable::valueTypeName(type)));
}

void VarTree::set(const QStringList& pathList, int index, const VarPtr& var)
{
   auto list = getVariable(pathList, true, nullptr);

   if (!list)
      return;

   if (index == -1)
      index = list->itemCount();

   list->setItem(index, var);
}

void VarTree::set(const QStringList& pathList, int index, const Variant& value, const QString& type)
{
   set(pathList, index, _data->newFromValue(value, type));
}

void VarTree::set(const QStringList& pathList, int index, const Variant& value, const std::type_info& type)
{
   set(pathList, index, _data->newFromValue(value, Variable::valueTypeName(type)));
}

void VarTree::setTree(const QString& path, const VarTree& tree)
{
   set(toPathList(path), tree._data);
}

void VarTree::setCellValue(int row, const QString& columnPath, const Variant& value)
{
   if (row < 0)
      return;

   auto var = variable(columnPath);

   if (var && row < var->itemCount())
      var->getItem(row)->setValue(value);
}

void VarTree::addColumn(const QStringList& pathList, const QString& type)
{
   auto var = getVariable(pathList, true, nullptr);

   if (var->itemCount() == 0)
   {
      const auto count = qMax(rowCount(), 1);

      for (auto i = 0; i < count; ++i)
      {
         var->setItem(i, _data->newFromValue(Variant::null, type));
      }
   }
}

void VarTree::addColumn(const QStringList& pathList, const std::type_info& type)
{
   auto var = getVariable(pathList, true, nullptr);

   if (var->itemCount() == 0)
   {
      const auto count = qMax(rowCount(), 1);

      for (auto i = 0; i < count; ++i)
      {
         var->setItem(i, _data->newFromValue(Variant::null, Variable::valueTypeName(type)));
      }
   }
}

void VarTree::setRowCount(int rowCount)
{
   // Go recursively through all columns
   if (rowCount <= 0)
      return;

   _data->iterateTree([rowCount](const QStringList&, IVariable* varDef)
   {
      if (varDef->itemCount() == 0)
         return;

      const auto firstItem = varDef->getItem(0);

      while (varDef->itemCount() > rowCount)
         varDef->removeItem(varDef->itemCount() - 1);
      while (varDef->itemCount() < rowCount)
         varDef->setItem(varDef->itemCount(), varDef->newFromValue(Variant::null, firstItem->typeName()));
   });
}

Variant VarTree::take(const QString& path, const QString& name)
{
   auto dict = getVariable(toPathList(path), false, nullptr);

   Variant o;

   if (dict)
   {
      auto var = dict->takeItem(name);

      if (var)
         o = var->value();
   }

   return o;
}

QStringList VarTree::toPathList(QString path)
{
   path = path.trimmed();

   Q_ASSERT(!path.startsWith(".") && !path.endsWith("."));

#if QT_VERSION < QT_VERSION_CHECK(5,14,0)
   return path.split(u'.', QString::SkipEmptyParts);
#else
   return path.split(u'.', Qt::SkipEmptyParts);
#endif
}

QList<int> VarTree::getIndexFromKey(QString& key)
{
   QList<int> indexes;

   auto keyEnd = key.indexOf(u'['); // Position des Indexes (-1 wenn kein Index vorhanden)
   auto indexStart = keyEnd;

   while (indexStart != -1)
   {
      const auto indexEnd = key.indexOf(u']', indexStart);

      if (indexEnd == -1)
      {
         keyEnd = -1;
         break;
      }

      indexStart++;

      bool ok;

      indexes.append(key.mid(indexStart, indexEnd - indexStart).toInt(&ok));

      if (!ok)
      {
         keyEnd = -1;
         break;
      }

      indexStart = indexEnd + 1;

      if (indexStart == key.length())
         break;

      if (key.at(indexStart) != u'[')
      {
         keyEnd = -1;
         break;
      }
   }

   if (keyEnd == -1)
      return QList<int>();

   key = key.mid(0, keyEnd).trimmed();

   return indexes;
}

VarTree VarTree::at(const QString& path, bool createDictionary)
{
   // path must lead to a dictionary
   return VarTree(getVariable(toPathList(path), createDictionary, nullptr));
}

VarPtr VarTree::getVariable(const QStringList& pathList, bool createDictionary, QList<int>* indexPath) const
{
   auto var = _data;

   // Iterate var through the tree according ot pathList

   for (auto&& pathElement : pathList)
   {
      auto key = pathElement.trimmed(); // Schlüsselwert

      // An empty key is not allowed

      if (key.isEmpty())
      {
         var.reset();
         break;
      };

      // Get index from the key (inside brackets)

      auto indexes = getIndexFromKey(key);

      // Check if the key is just a number (only if the key hadn't an index!)

      if (indexes.isEmpty())
      {
         auto ok = false;
         indexes.append(key.toInt(&ok));

         if (ok)
            key.clear();
         else
            indexes.clear();
      }

      // Get the variable at key

      if (!key.isEmpty())
      {
         if (createDictionary)
         {
            var = var->getCollection(key);
         }
         else
         {
            const auto index = var->indexOfName(key);

            if (indexPath)
               indexPath->append(index);

            if (index == -1)
               var.reset();
            else
               var = var->getItem(index);
         }

         if (!var)
            break;
      }

      // Get the variable at index

      for (auto&& index : indexes)
      {
         if (indexPath)
            indexPath->append(index);

         if (createDictionary)
            var = var->getCollection(index);
         else
            var = var->getItem(index);

         if (!var)
            break;
      }
   }

   return var;
}
