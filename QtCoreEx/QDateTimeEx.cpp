#include "stdafx.h"
#include "QDateTimeEx.h"
#include <QDateTime>
#include "Variant.h"

#ifdef Q_OS_WIN
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#else
#include <sys/time.h>
#include <time.h>
#endif

static bool isLeapYear(int year)
{
   if (year % 4)
      return false;
   if (year % 100)
      return true;
   return year % 400 == 0;
}

static int daysInMonth(int month, int year)
{
   switch (month)
   {
   case 2:
      return isLeapYear(year) ? 29 : 28;
   case 4:
   case 6:
   case 9:
   case 11:
      return 30;
   }

   return 31;
}

QDateTimeEx::QDateTimeEx() : _dateTime(0ULL)
{
}

QDateTimeEx::QDateTimeEx(const QDateTimeEx& other) : _dateTime(other._dateTime)
{
}

QDateTimeEx::QDateTimeEx(const QTime& time, DateTimePart precision) : _dateTime(0ULL)
{
   setTime(time, precision);
}

QDateTimeEx::QDateTimeEx(const QDate& date, DateTimePart precision) : _dateTime(0ULL)
{
   setDate(date, precision);
}

QDateTimeEx::QDateTimeEx(const QDateTime& dateTime, DateTimePart precision) : _dateTime(0ULL)
{
   setDateTime(dateTime, precision);
}

QDateTimeEx::QDateTimeEx(const QDate& date, const QTime& time, DateTimePart precision, QTimeZone timeZone) : _dateTime(0ULL)
{
   setDate(date);
   setTime(time, precision);
   if (date.isValid() && time.isValid() && timeZone.isValid())
   {
      if (timeZone == QTimeZone::utc())
      {
         _tzoffset = 1ULL;
         _tzsign = 0ULL;
         _dst = 0ULL;
      }
      else
      {
         setTimeZone(QDateTime(date, time, timeZone));
      }
   }
}

QDateTimeEx::QDateTimeEx(int year, int month, int day, int hour, int minute, int second, int msec, QTimeZone timeZone) : _dateTime(0ULL)
{
   setDate(year, month, day);
   setTime(hour, minute, second, msec);
   if (_hour && timeZone.isValid())
   {
      if (timeZone == QTimeZone::utc())
      {
         _tzoffset = 1ULL;
         _tzsign = 0ULL;
         _dst = 0ULL;
      }
      else
      {
         setTimeZone(QDateTime(toQDate(), toQTime(), timeZone));
      }
   }
}

QDateTimeEx::QDateTimeEx(int year, int month, int day, int hour, int minute, int second, int msec, int timeZoneOffset, bool dst) : _dateTime(0ULL)
{
   setDate(year, month, day);
   setTime(hour, minute, second, msec);
   setTimeZoneOffset(timeZoneOffset);
   _dst = dst ? 1ULL : 0ULL;
}

QDateTimeEx::~QDateTimeEx()
{
}

QDateTimeEx& QDateTimeEx::operator=(const QDateTimeEx& rhs)
{
   _dateTime = rhs._dateTime;
   return *this;
}

bool QDateTimeEx::operator==(const QDateTimeEx& rhs) const
{
   return _dateTime == rhs._dateTime;
}

#define EQUAL_PART(part) \
   if (left.part && right.part) \
   { \
      if (left.part != right.part) \
         return false; \
      equal = true; \
   }

bool QDateTimeEx::equalTo(const QDateTimeEx& rhs) const
{
   QDateTimeEx left;
   QDateTimeEx right;

   if ((_tzoffset != rhs._tzoffset || _tzsign != rhs._tzsign) && _tzoffset && rhs._tzoffset)
   {
      left = toUTC();
      right = rhs.toUTC();
   }
   else
   {
      left = *this;
      right = rhs;
   }

   auto equal = false;

   EQUAL_PART(_millennium)
   EQUAL_PART(_century)
   EQUAL_PART(_decade)
   EQUAL_PART(_year)
   EQUAL_PART(_month)
   EQUAL_PART(_day)
   EQUAL_PART(_hour)
   EQUAL_PART(_minute)
   EQUAL_PART(_second)
   EQUAL_PART(_msec)

   return equal;
}

#define DIFFERENT_PART(part) \
   if (left.part && right.part && left.part != right.part) \
      return true;

bool QDateTimeEx::differentTo(const QDateTimeEx& rhs) const
{
   QDateTimeEx left;
   QDateTimeEx right;

   if ((_tzoffset != rhs._tzoffset || _tzsign != rhs._tzsign) && _tzoffset && rhs._tzoffset)
   {
      left = toUTC();
      right = rhs.toUTC();
   }
   else
   {
      left = *this;
      right = rhs;
   }

   DIFFERENT_PART(_millennium)
   DIFFERENT_PART(_century)
   DIFFERENT_PART(_decade)
   DIFFERENT_PART(_year)
   DIFFERENT_PART(_month)
   DIFFERENT_PART(_day)
   DIFFERENT_PART(_hour)
   DIFFERENT_PART(_minute)
   DIFFERENT_PART(_second)
   DIFFERENT_PART(_msec)

   return false;
}

#define COMPARE_PART(part) if (left.part < right.part && left.part) \
      return true; \
   if (right.part < left.part && right.part) \
      return false;

bool QDateTimeEx::operator<(const QDateTimeEx& rhs) const
{
   QDateTimeEx left;
   QDateTimeEx right;

   if ((_tzoffset != rhs._tzoffset || _tzsign != rhs._tzsign) && _tzoffset && rhs._tzoffset)
   {
      left = toUTC();
      right = rhs.toUTC();
   }
   else
   {
      left = *this;
      right = rhs;
   }

   COMPARE_PART(_millennium)
   COMPARE_PART(_century)
   COMPARE_PART(_decade)
   COMPARE_PART(_year)
   COMPARE_PART(_month)
   COMPARE_PART(_day)
   COMPARE_PART(_hour)
   COMPARE_PART(_minute)
   COMPARE_PART(_second)
   COMPARE_PART(_msec)
   return false;
}

QDateTimeEx QDateTimeEx::addMonths(int months) const
{
   if (!_month || !months)
      return *this;

   int m = _month + months;
   int y = (m < 0 ? m - 11 : m) / 12;

   m -= y * 12;

   QDateTimeEx result(*this);

   if (y)
   {
      y += result.year();
      result.setYear(y);
   }
   else
   {
      y = result.year();
   }
   result._month = m;
   result._day = qMin(result._day, (unsigned long long)daysInMonth(m, y));

   return result;
}

QDateTimeEx QDateTimeEx::addDays(int days) const
{
   if (!_day || !days)
      return *this;

   int d = _day + days;
   int m = _month;
   int y = year();

   while (true)
   {
      if (d < 1)
      {
         if (m)
         {
            if (m == 1)
            {
               y--;
               m = 12;
            }
            else
            {
               m--;
            }
            d += daysInMonth(m, y);
         }
         else
         {
            d = 1;
         }

         if (d > 0)
            break;
      }
      else
      {
         int dim = daysInMonth(m, y);

         if (d <= dim)
            break;

         if (m)
         {
            d -= dim;
            if (m == 12)
            {
               m = 1;
               y++;
            }
            else
            {
               m++;
            }
         }
         else
         {
            d = dim;
         }
      }
   }

   QDateTimeEx result(*this);

   result._day = d;
   result._month = m;
   result.setYear(y);

   return result;
}

QDateTimeEx QDateTimeEx::addHours(int hours) const
{
   if (!_hour || !hours)
      return *this;

   int h = hour() + hours;
   int d = (h < 0 ? h - 23 : h) / 24;

   h -= d * 24;

   QDateTimeEx result = addDays(d);

   result.setHour(h);

   return result;
}

QDateTimeEx QDateTimeEx::addMinutes(int minutes) const
{
   if (!_minute || !minutes)
      return *this;

   int m = minute() + minutes;
   int h = (m < 0 ? m - 59 : m) / 60;

   m -= h * 60;

   QDateTimeEx result = addHours(h);

   result.setMinute(m);

   return result;
}

QDateTimeEx QDateTimeEx::addSeconds(int seconds) const
{
   if (!_second || !seconds)
      return *this;

   int s = second() + seconds;
   int m = (s < 0 ? s - 59 : s) / 60;

   s -= m * 60;

   QDateTimeEx result = addMinutes(m);

   result.setSecond(s);

   return result;
}

QDateTimeEx QDateTimeEx::currentDateTime(DateTimePart precision)
{
   QDateTimeEx dateTime;

#ifdef Q_OS_WIN
   TIME_ZONE_INFORMATION tz;
   memset(&tz, 0, sizeof(TIME_ZONE_INFORMATION));

   dateTime = currentDateTimeUtc(precision);

   if (GetTimeZoneInformation(&tz) == TIME_ZONE_ID_DAYLIGHT)
   {
      dateTime = dateTime.addMinutes(-tz.Bias - tz.DaylightBias);

      dateTime.setTimeZoneOffset(-tz.Bias - tz.DaylightBias);
      dateTime.setIsDst(true);
   }
   else
   {
      dateTime = dateTime.addMinutes(-tz.Bias - tz.StandardBias);

      dateTime.setTimeZoneOffset(-tz.Bias - tz.StandardBias);
      dateTime.setIsDst(false);
   }
#else
   struct timeval tv;
   gettimeofday(&tv, 0);
   struct tm* lt = localtime(&tv.tv_sec);
   dateTime.setDate(lt->tm_year + 1900, lt->tm_mon + 1, lt->tm_mday);
   dateTime.setTime(lt->tm_hour, (precision < DateTimePart::Hour ? lt->tm_min : -1), (precision < DateTimePart::Minute ? qMin(lt->tm_sec, 59) : -1), (precision < DateTimePart::Second ? tv.tv_usec / 1000 : -1));

   dateTime.setTimeZoneOffset(lt->tm_gmtoff / 60);
   dateTime.setIsDst(lt->tm_isdst == 1);
#endif

   return dateTime;
}

QDateTimeEx QDateTimeEx::currentDateTimeUtc(DateTimePart precision)
{
   QDateTimeEx dateTime;

#ifdef Q_OS_WIN
   SYSTEMTIME st;
   memset(&st, 0, sizeof(SYSTEMTIME));
   GetSystemTime(&st);
   dateTime.setDate(st.wYear, st.wMonth, st.wDay);
   dateTime.setTime(st.wHour, (precision < DateTimePart::Hour ? st.wMinute : -1), (precision < DateTimePart::Minute ? st.wSecond : -1), (precision < DateTimePart::Second ? st.wMilliseconds : -1));
#else
   struct timeval tv;
   gettimeofday(&tv, 0);
   struct tm* lt = gmtime(&tv.tv_sec);
   dateTime.setDate(lt->tm_year + 1900, lt->tm_mon + 1, lt->tm_mday);
   dateTime.setTime(lt->tm_hour, (precision < DateTimePart::Hour ? lt->tm_min : -1), (precision < DateTimePart::Minute ? qMin(lt->tm_sec, 59) : -1), (precision < DateTimePart::Second ? tv.tv_usec / 1000 : -1));
#endif

   dateTime._tzoffset = 1ULL;
   dateTime._tzsign = 0ULL;
   dateTime._dst = 0ULL;

   return dateTime;
}

QDateEx QDateTimeEx::currentDate(DateTimePart precision)
{
   QDateEx date;

#ifdef Q_OS_WIN
   SYSTEMTIME st;
   memset(&st, 0, sizeof(SYSTEMTIME));
   GetLocalTime(&st);
   date.setDate(st.wYear, (precision < DateTimePart::Year ? st.wMonth : 0), (precision < DateTimePart::Month ? st.wDay : 0));
#else
   struct timeval tv;
   gettimeofday(&tv, 0);
   struct tm* lt = localtime(&tv.tv_sec);
   date.setDate(lt->tm_year + 1900, (precision < DateTimePart::Year ? lt->tm_mon + 1 : 0), (precision < DateTimePart::Month ? lt->tm_mday : 0));
#endif

   return date;
}

QTimeEx QDateTimeEx::currentTime(DateTimePart precision)
{
   QTimeEx time;

#ifdef Q_OS_WIN
   SYSTEMTIME st;
   memset(&st, 0, sizeof(SYSTEMTIME));
   GetLocalTime(&st);
   time.setTime(st.wHour, (precision < DateTimePart::Hour ? st.wMinute : -1), (precision < DateTimePart::Minute ? st.wSecond : -1), (precision < DateTimePart::Second ? st.wMilliseconds : -1));
#else
   struct timeval tv;
   gettimeofday(&tv, 0);
   struct tm* lt = localtime(&tv.tv_sec);
   time.setTime(lt->tm_hour, (precision < DateTimePart::Hour ? lt->tm_min : -1), (precision < DateTimePart::Minute ? qMin(lt->tm_sec, 59) : -1), (precision < DateTimePart::Second ? tv.tv_usec / 1000 : -1));
#endif

   return time;
}

DateTimePart QDateTimeEx::minPrecision() const
{
   if (_msec)
      return DateTimePart::Millisecond;
   if (_second)
      return DateTimePart::Second;
   if (_minute)
      return DateTimePart::Minute;
   if (_hour)
      return DateTimePart::Hour;
   if (_day)
      return DateTimePart::Day;
   if (_month)
      return DateTimePart::Month;
   if (_year)
      return DateTimePart::Year;
   if (_decade)
      return DateTimePart::Decade;
   if (_century)
      return DateTimePart::Century;
   if (_millennium)
      return DateTimePart::Millennium;
   return DateTimePart::None;
}

DateTimePart QDateTimeEx::maxPrecision() const
{
   if (_millennium)
      return DateTimePart::Millennium;
   if (_century)
      return DateTimePart::Century;
   if (_decade)
      return DateTimePart::Decade;
   if (_year)
      return DateTimePart::Year;
   if (_month)
      return DateTimePart::Month;
   if (_day)
      return DateTimePart::Day;
   if (_hour)
      return DateTimePart::Hour;
   if (_minute)
      return DateTimePart::Minute;
   if (_second)
      return DateTimePart::Second;
   if (_msec)
      return DateTimePart::Millisecond;
   return DateTimePart::None;
}

int QDateTimeEx::year() const
{
   int year = 0;
   auto negative = false;

   if (_millennium)
   {
      if (_millennium < 4ULL)
      {
         negative = true;
         year = (4 - (int)_millennium) * 1000;
      }
      else
      {
         year = ((int)_millennium - 4) * 1000;
      }
   }
   if (_century)
      year += ((int)_century - 1) * 100;
   if (_decade)
      year += ((int)_decade - 1) * 10;
   if (_year)
      year += (int)_year - 1;

   return negative ? -year : year;
}

void QDateTimeEx::setYear(int year)
{
   _millennium = 0ULL;
   _century = 0ULL;
   _decade = 0ULL;
   _year = 0ULL;
   if (year)
   {
      if (year > -4000 && year < 12000)
      {
         _millennium = year / 1000 + 4;
         if (year < 0)
            year = -year;
         year %= 1000;
         _century = year / 100 + 1;
         year %= 100;
         _decade = year / 10 + 1;
         year %= 10;
         _year = year + 1;
      }
   }
}

QDateTimeEx QDateTimeEx::toUTC() const
{
   if (!_hour)
      return *this;

   int tzOffset;

   if (!hasTimeZone())
   {
      tzOffset = toQDateTime().offsetFromUtc() / 60;
   }
   else
   {
      tzOffset = timeZoneOffset();
   }

   QDateTimeEx result = addMinutes(-tzOffset);

   result._tzoffset = 1ULL;
   result._tzsign = 0ULL;
   result._dst = 0ULL;

   return result;
}

QDateTimeEx QDateTimeEx::toLocalTime(QTimeZone timeZone) const
{
   if (!_hour)
      return *this;

   if (!timeZone.isValid())
      timeZone = QTimeZone::systemTimeZone();

   if (!hasTimeZone() && timeZone == QTimeZone::systemTimeZone())
   {
      auto result = *this;

      result.setTimeZone(toQDateTime());

      return result;
   }

   auto result = toUTC();

   if (timeZone != QTimeZone::utc())
   {
      const auto dateTime = result.toQDateTime();
      const auto timeZoneOffset = timeZone.offsetFromUtc(dateTime) / 60;

      result = result.addMinutes(timeZoneOffset);

      result.setTimeZoneOffset(timeZoneOffset);
      result.setIsDst(timeZone.isDaylightTime(dateTime));
   }

   return result;
}

QDateEx QDateTimeEx::toDate() const
{
   return *this;
}

QTimeEx QDateTimeEx::toTime() const
{
   return *this;
}

QString QDateTimeEx::toString(const QLocaleEx& locale) const
{
   return locale.toString(*this);
}

QString QDateTimeEx::toString(const QString& format, const QLocaleEx& locale) const
{
   QString result;
   auto hasAmPm = false;
   QList<QPair<QChar, int>> expressionList;
   QChar prevChar;
   auto quotedString = false;

   for (auto c : format)
   {
      if (quotedString)
      {
         if (c == u'\'')
         {
            if (prevChar == u'\'')
               result += c;
            quotedString = false;
         }
         else
         {
            result += c;
         }
      }
      else if (strchr("dMyhHmszt", c.unicode()))
      {
         if (prevChar == c)
         {
            expressionList.last().second++;
         }
         else
         {
            expressionList.append(QPair<QChar, int>(c, 0));
            result += "%" + QString::number(expressionList.count());
         }
      }
      else if (c == u'a' || c == u'A')
      {
         hasAmPm = true;
         expressionList.append(QPair<QChar, int>(c, 0));
         result += "%" + QString::number(expressionList.count());
      }
      else if (c == u'\'')
      {
         if (prevChar == u'\'')
            result += c;
         quotedString = true;
      }
      else if (prevChar == u'a' && c == u'p' || prevChar == u'A' && c == u'P')
      {
         // Ignore
      }
      else
      {
         result += c;
         if (c == u'%')
            result += c;
      }

      prevChar = c;
   }

   for (auto&& expression : expressionList)
   {
      switch (expression.first.unicode())
      {
      case 'd':
         switch (expression.second)
         {
         case 0:
            result = result.arg(qMax(day(), 1));
            break;
         case 1:
            result = result.arg(qMax(day(), 1), 2, 10, QChar(u'0'));
            break;
         case 2:
            result = result.arg(locale.dayName(toQDate().dayOfWeek(), QLocale::ShortFormat));
            break;
         case 3:
            result = result.arg(locale.dayName(toQDate().dayOfWeek(), QLocale::LongFormat));
            break;
         default:
            result = result.arg("?");
         }
         break;
      case 'M':
         {
            auto m = _month ? (int)_month : 1;
            switch (expression.second)
            {
            case 0:
               result = result.arg(m);
               break;
            case 1:
               result = result.arg(m, 2, 10, QChar(u'0'));
               break;
            case 2:
               result = result.arg(locale.monthName(m, QLocale::ShortFormat));
               break;
            case 3:
               result = result.arg(locale.monthName(m, QLocale::LongFormat));
               break;
            default:
               result = result.arg("?");
            }
         }
         break;
      case 'y':
         {
            switch (expression.second)
            {
            case 1:
               result = result.arg(year() % 100, 2, 10, QChar(u'0'));
               break;
            case 3:
               result = result.arg(year(), 4, 10, QChar(u'0'));
               break;
            default:
               result = result.arg("?");
            }
         }
         break;
      case 'h':
         {
            auto h = qMax(hour(), 0);
            if (hasAmPm)
            {
               h %= 12;
               if (h == 0)
                  h = 12;
            }
            switch (expression.second)
            {
            case 0:
               result = result.arg(h);
               break;
            case 1:
               result = result.arg(h, 2, 10, QChar(u'0'));
               break;
            default:
               result = result.arg("?");
            }
         }
         break;
      case 'H':
         switch (expression.second)
         {
         case 0:
            result = result.arg(qMax(hour(), 0));
            break;
         case 1:
            result = result.arg(qMax(hour(), 0), 2, 10, QChar(u'0'));
            break;
         default:
            result = result.arg("?");
         }
         break;
      case 'm':
         switch (expression.second)
         {
         case 0:
            result = result.arg(qMax(minute(), 0));
            break;
         case 1:
            result = result.arg(qMax(minute(), 0), 2, 10, QChar(u'0'));
            break;
         default:
            result = result.arg("?");
         }
         break;
      case 's':
         switch (expression.second)
         {
         case 0:
            result = result.arg(qMax(second(), 0));
            break;
         case 1:
            result = result.arg(qMax(second(), 0), 2, 10, QChar(u'0'));
            break;
         default:
            result = result.arg("?");
         }
         break;
      case 'z':
         switch (expression.second)
         {
         case 0:
            result = result.arg(qMax(millisecond(), 0));
            break;
         case 2:
            result = result.arg(qMax(millisecond(), 0), 3, 10, QChar(u'0'));
            break;
         default:
            result = result.arg("?");
         }
         break;
      case 'a':
         result = result.arg(_hour > 12 ? locale.pmText().toLower() : locale.amText().toLower());
         break;
      case 'A':
         result = result.arg(_hour > 12 ? locale.pmText() : locale.amText());
         break;
      case 't':
         if (_tzoffset)
         {
            char buffer[9];
            auto i = 0;

            buffer[i++] = 'Z';
            buffer[i++] = _tzsign ? '-' : '+';
            buffer[i++] = '0' + ((int)_tzoffset - 1) / 600;
            buffer[i++] = '0' + ((int)_tzoffset - 1) % 600 / 60;
            buffer[i++] = ':';
            buffer[i++] = '0' + ((int)_tzoffset - 1) % 60 / 10;
            buffer[i++] = '0' + ((int)_tzoffset - 1) % 10;
            buffer[i++] = _dst ? 'S' : '\0';

            Q_ASSERT(i < 9);

            buffer[i++] = '\0';

            result = result.arg(buffer);
         }
         else
         {
            result = result.arg(QString());
         }
         break;
      }
   }

   return result.trimmed();
}

QString QDateTimeEx::toISOString() const
{
   char dateTime[32];
   auto i = 0;

   if (_millennium)
   {
      if (_millennium < 4ULL)
      {
         dateTime[i++] = '-';
         dateTime[i++] = '0' + 4 - (int)_millennium;
      }
      else
      {
         auto year = (int)_millennium - 4;

         if (year >= 10)
         {
            dateTime[i++] = '1';
            year -= 10;
         }
         dateTime[i++] = '0' + year;
      }
      dateTime[i++] = '0' + (_century ? (int)_century - 1 : 0);
      dateTime[i++] = '0' + (_decade ? (int)_decade - 1 : 0);
      dateTime[i++] = '0' + (_year ? (int)_year - 1 : 0);
      dateTime[i++] = '-';
      dateTime[i++] = '0' + (_month ? (int)_month / 10 : 0);
      dateTime[i++] = '0' + (_month ? (int)_month % 10 : 1);
      dateTime[i++] = '-';
      dateTime[i++] = '0' + (_day ? (int)_day / 10 : 0);
      dateTime[i++] = '0' + (_day ? (int)_day % 10 : 1);
   }

   if (_hour)
   {
      if (i)
         dateTime[i++] = 'T';

      dateTime[i++] = '0' + (_hour ? ((int)_hour - 1) / 10 : 0);
      dateTime[i++] = '0' + (_hour ? ((int)_hour - 1) % 10 : 0);
      dateTime[i++] = ':';
      dateTime[i++] = '0' + (_minute ? ((int)_minute - 1) / 10 : 0);
      dateTime[i++] = '0' + (_minute ? ((int)_minute - 1) % 10 : 0);
      if (_second)
      {
         dateTime[i++] = ':';
         dateTime[i++] = '0' + ((int)_second - 1) / 10;
         dateTime[i++] = '0' + ((int)_second - 1) % 10;

         if (_msec)
         {
            auto msec = (int)_msec - 1;

            dateTime[i++] = '.';
            dateTime[i++] = '0' + msec / 100;
            msec %= 100;
            dateTime[i++] = '0' + msec / 10;
            msec %= 10;
            dateTime[i++] = '0' + msec;
         }
      }
   }

   if (_tzoffset)
   {
      dateTime[i++] = _tzsign ? '-' : '+';
      dateTime[i++] = '0' + ((int)_tzoffset - 1) / 600;
      dateTime[i++] = '0' + ((int)_tzoffset - 1) % 600 / 60;
      dateTime[i++] = ':';
      dateTime[i++] = '0' + ((int)_tzoffset - 1) % 60 / 10;
      dateTime[i++] = '0' + ((int)_tzoffset - 1) % 10;
      if (_dst)
         dateTime[i++] = 'S';
   }

   Q_ASSERT(i < 32);

   dateTime[i++] = '\0';

   return dateTime;
}

QDateTimeEx QDateTimeEx::fromISOString(const QString &s)
{
   QDateTimeEx dateTime;

   auto number = 0;
   auto digits = 0;
   auto negative = false;
   auto part = DateTimePart::None;

   for (auto i = 0; i <= s.size(); ++i)
   {
      QChar c;

      if (i < s.size())
         c = s.at(i);

      if (c.isDigit())
      {
         digits++;
         number *= 10;
         number += c.digitValue();
      }
      else if (i == 0 && c == u'-')
      {
         negative = true;
      }
      else if (c != u'Z' || i + 1 == s.size()) // Ignore Z if its not the last character
      {
         if (digits == 0)
            break; // This is valid if the last character was a Z or an S

         switch (part)
         {
         case DateTimePart::None:
            if (c == u'-')
            {
               part = DateTimePart::Year;
               if (negative)
                  dateTime.setYear(-number);
               else
                  dateTime.setYear(number);
               negative = false;
            }
            else if (c == u':' && !negative)
            {
               part = DateTimePart::Hour;
               dateTime.setHour(number);
            }
            else
               return dateTime;
            break;
         case DateTimePart::Year:
            if (c != u'-')
               return dateTime;
            part = DateTimePart::Month;
            dateTime.setMonth(number);
            break;
         case DateTimePart::Month:
            if (!c.isNull() && c != u'T')
               return dateTime;
            part = DateTimePart::Day;
            dateTime.setDay(number);
            break;
         case DateTimePart::Day:
            if (c != u':')
               return dateTime;
            part = DateTimePart::Hour;
            dateTime.setHour(number);
            break;
         case DateTimePart::Hour:
            if (c == u'Z')
            {
               dateTime.setTimeZoneOffset(0);
            }
            else if (c == u'+')
            {
               part = DateTimePart::Millisecond;
            }
            else if (c == u'-')
            {
               part = DateTimePart::Millisecond;
               negative = true;
            }
            else if (c.isNull() || c == u':')
            {
               part = DateTimePart::Minute;
            }
            else
               return dateTime;
            dateTime.setMinute(number);
            break;
         case DateTimePart::Minute:
            if (c == u'Z')
            {
               dateTime.setTimeZoneOffset(0);
            }
            else if (c == u'+')
            {
               part = DateTimePart::Millisecond;
            }
            else if (c == u'-')
            {
               part = DateTimePart::Millisecond;
               negative = true;
            }
            else if (c.isNull() || c == u',' || c == u'.')
            {
               part = DateTimePart::Second;
            }
            else
               return dateTime;
            dateTime.setSecond(number);
            break;
         case DateTimePart::Second:
            if (c == u'Z')
            {
               dateTime.setTimeZoneOffset(0);
            }
            else if (c == u'+')
            {
               part = DateTimePart::Millisecond;
            }
            else if (c == u'-')
            {
               part = DateTimePart::Millisecond;
               negative = true;
            }
            else
               return dateTime;
            dateTime.setMillisecond(number);
            break;
         case DateTimePart::Millisecond:
            if (c == u'S')
               dateTime._dst = 1ULL;
            else if (!c.isNull() && c != u':')
               return dateTime;
            part = DateTimePart::Timezone;
            dateTime._tzsign = negative ? 1ULL : 0ULL;
            dateTime._tzoffset = (number * 60) + 1;
            break;
         case DateTimePart::Timezone:
            if (c == u'S')
               dateTime._dst = 1ULL;
            else if (!c.isNull())
               return dateTime;
            dateTime._tzoffset += number;
            break;
         }

         number = 0;
         digits = 0;
      }
   }

   return dateTime;
}

void QDateTimeEx::setDate(const QDate& date, DateTimePart precision)
{
   int year = 0, month = 0, day = 0;

   if (date.isValid())
      date.getDate(&year, &month, &day);

   if (precision > DateTimePart::Month)
      month = 0;

   if (precision > DateTimePart::Day)
      day = 0;

   setDate(year, month, day);
}

void QDateTimeEx::setDate(int year, int month, int day)
{
   Q_ASSERT(month >= 0 && day >= 0);
   setYear(year);
   _month = month;
   _day = day;
}

void QDateTimeEx::setTime(const QTime& time, DateTimePart precision)
{
   setTime(-1, -1, -1, -1);
   if (time.isValid() && precision <= DateTimePart::Hour)
   {
      setHour(time.hour());
      if (precision == DateTimePart::Hour)
         return;
      setMinute(time.minute());
      if (precision == DateTimePart::Minute)
         return;
      setSecond(time.second());
      if (precision == DateTimePart::Second)
         return;
      setMillisecond(time.msec());
   }
}

void QDateTimeEx::setTime(int hour, int minute, int second, int msec)
{
   Q_ASSERT(hour >= -1 && minute >= -1 && second >= -1 && msec >= -1);
   _hour = hour + 1;
   _minute = minute + 1;
   _second = second + 1;
   _msec = msec + 1;
}

void QDateTimeEx::setDateTime(const QDateTime& dateTime, DateTimePart precision)
{
   setDate(dateTime.date(), precision);
   setTime(dateTime.time(), precision);
   setTimeZone(dateTime);
}

QDate QDateTimeEx::toQDate() const
{
   if (_millennium)
      return QDate(year(), qMax(month(), 1), qMax(day(), 1));
   return QDate();
}

QTime QDateTimeEx::toQTime() const
{
   if (_hour)
      return QTime(hour(), qMax(minute(), 0), qMax(second(), 0), qMax(millisecond(), 0));
   return QTime();
}

QDateTime QDateTimeEx::toQDateTime() const
{
   if (!hasTimeZone())
      return QDateTime(toQDate(), toQTime());

   if (isUTC())
#if QT_VERSION < QT_VERSION_CHECK(6, 8, 0)
      return QDateTime(toQDate(), toQTime(), Qt::UTC);
#else
      return QDateTime(toQDate(), toQTime(), QTimeZone::UTC);
#endif

   QDateTime result(toQDate(), toQTime());
   const auto tzOffset = timeZoneOffset() * 60;

   if (result.offsetFromUtc() == tzOffset && result.isDaylightTime() == (_dst != 0ULL))
      return result;

   result = QDateTime();

   auto standardOffset = tzOffset;

   if (_dst)
      standardOffset -= 3600;

   for (auto&& tzId : QTimeZone::availableTimeZoneIds(standardOffset))
   {
      result = QDateTime(toQDate(), toQTime(), QTimeZone(tzId));

      if (result.isDaylightTime() == (_dst != 0ULL))
         break;

      result = QDateTime();
   }

   if (result.isValid() || _dst)
      return result;

   // If we have standard time the daylight saving time may still have been unknown
   // Try daylight saving time
   standardOffset -= 3600;

   for (auto&& tzId : QTimeZone::availableTimeZoneIds(standardOffset))
   {
      result = QDateTime(toQDate(), toQTime(), QTimeZone(tzId));

      if (result.isDaylightTime())
         break;

      result = QDateTime();
   }

   return result;
}

void QDateTimeEx::setTimeZone(const QDateTime& dateTime)
{
   if (dateTime.timeSpec() == Qt::UTC)
   {
      _tzoffset = 1ULL;
      _tzsign = 0ULL;
      _dst = 0ULL;
   }
   else
   {
      setTimeZoneOffset(dateTime.offsetFromUtc() / 60);
      setIsDst(dateTime.isDaylightTime());
   }
}

QDateEx::QDateEx(const QDateTimeEx& other) : QDateTimeEx(other._dateTime & ~0x7fffffffffULL)
{
}

QDateEx& QDateEx::operator=(const QDateTimeEx& rhs)
{
   _dateTime = rhs._dateTime & ~0x7fffffffffULL;
   return *this;
}

QDateEx& QDateEx::operator=(const QDate& rhs)
{
   setDate(rhs);
   return *this;
}

QTimeEx::QTimeEx(const QDateTimeEx& other) : QDateTimeEx(other._dateTime & 0x7ffffff000ULL)
{
}

QTimeEx& QTimeEx::operator=(const QDateTimeEx& rhs)
{
   _dateTime = rhs._dateTime & 0x7ffffff000ULL;
   return *this;
}

QTimeEx& QTimeEx::operator=(const QTime& rhs)
{
   setTime(rhs);
   return *this;
}

SerStream& operator<<(SerStream& stream, const QDateTimeEx& value)
{
   stream << (char)((int)value._millennium < 11 ? '/' + (int)value._millennium : 'A' - 11 + (int)value._millennium);
   stream << (char)('/' + (int)value._century);
   stream << (char)('/' + (int)value._decade);
   stream << (char)('/' + (int)value._year);
   stream << (char)('0' + (int)value._month / 10) << (char)('0' + (int)value._month % 10);
   stream << (char)('0' + (int)value._day / 10) << (char)('0' + (int)value._day % 10);
   stream << (char)('0' + (int)value._hour / 10) << (char)('0' + (int)value._hour % 10);
   stream << (char)('0' + (int)value._minute / 10) << (char)('0' + (int)value._minute % 10);
   stream << (char)('0' + (int)value._second / 10) << (char)('0' + (int)value._second % 10);
   if (value._msec)
   {
      auto msec = (int)value._msec - 1;
      stream << (char)('0' + msec / 100);
      msec %= 100;
      stream << (char)('0' + msec / 10);
      msec %= 10;
      stream << (char)('0' + msec);
   }
   else
      stream << '-' << '-' << '-';
   stream << (value._dst ? '1' : '0');
   stream << (value._tzsign ? '-' : '+');
   stream << (char)('0' + (int)value._tzoffset / 100);
   stream << (char)('0' + (int)value._tzoffset % 100 / 10);
   stream << (char)('0' + (int)value._tzoffset % 10);
   stream.endEntity();

   return stream;
}

SerStream& operator>>(SerStream& stream, QDateTimeEx& value)
{
   char c1, c2, c3;

   stream >> c1;
   value._millennium = c1 < 'A' ? c1 - '/' : c1 - 'A' + 11;
   stream >> c1;
   value._century = c1 - '/';
   stream >> c1;
   value._decade = c1 - '/';
   stream >> c1;
   value._year = c1 - '/';
   stream >> c1 >> c2;
   value._month = (c1 - '0') * 10 + (c2 - '0');
   stream >> c1 >> c2;
   value._day = (c1 - '0') * 10 + (c2 - '0');
   stream >> c1 >> c2;
   value._hour = (c1 - '0') * 10 + (c2 - '0');
   stream >> c1 >> c2;
   value._minute = (c1 - '0') * 10 + (c2 - '0');
   stream >> c1 >> c2;
   value._second = (c1 - '0') * 10 + (c2 - '0');
   stream >> c1 >> c2 >> c3;
   if (c1 != '-' && c2 != '-' && c3 != '-')
      value._msec = (unsigned long long)(c1 - '0') * 100ULL + (unsigned long long)(c2 - '0') * 10ULL + (unsigned long long)(c3 - '0') + 1ULL;
   else
      value._msec = 0ULL;
   stream >> c1;
   if (c1 == 'S' || c1 == 'W')
   {
      value._dst = (c1 == 'S' ? 1ULL : 0ULL);
      stream >> c1 >> c2;
      int tzhour = (c1 - '0') * 10 + (c2 - '0');
      stream >> c1 >> c2;
      int tzmin = (c1 - '0') * 10 + (c2 - '0');
      if (tzhour == 0)
      {
         value._tzsign = 0ULL;
         value._tzoffset = 0ULL;
      }
      else if (tzhour < 13)
      {
         value._tzsign = 1ULL;
         value._tzoffset = (13 - tzhour) * 60 + tzmin;
      }
      else
      {
         value._tzsign = 0ULL;
         value._tzoffset = (tzhour - 13) * 60 + tzmin;
      }
   }
   else
   {
      value._dst = (c1 == '1' ? 1ULL : 0ULL);
      stream >> c1;
      value._tzsign = (c1 == '-' ? 1ULL : 0ULL);
      stream >> c1 >> c2 >> c3;
      value._tzoffset = (c1 - '0') * 100 + (c2 - '0') * 10 + (c3 - '0');
   }
   stream.skipEntityEnd();

   return stream;
}

//
// Variant conversion functions for QDateTimeEx
//

static bool convert(QDateTimeEx& lhs, int rhs, const QLocaleEx& locale)
{
   Q_UNUSED(locale);

   lhs = QDateTime::fromSecsSinceEpoch(rhs);

   return true;
}

static bool convert(QDateTimeEx& lhs, unsigned int rhs, const QLocaleEx& locale)
{
   Q_UNUSED(locale);

   lhs = QDateTime::fromSecsSinceEpoch(rhs);

   return true;
}

static bool convert(QDateTimeEx& lhs, long long int rhs, const QLocaleEx& locale)
{
   Q_UNUSED(locale);

   lhs = QDateTime::fromMSecsSinceEpoch(rhs);

   return true;
}

static bool convert(QDateTimeEx& lhs, unsigned long long int rhs, const QLocaleEx& locale)
{
   Q_UNUSED(locale);

   lhs = QDateTime::fromMSecsSinceEpoch((qint64)rhs);

   return true;
}

static bool convert(QDateTimeEx& lhs, double rhs, const QLocaleEx& locale)
{
   Q_UNUSED(locale);

   int d, h, m, s, ms;

   rhs = std::toTime(rhs, d, h, m, s);
   rhs *= 1000;
   ms = (int)rhs;
   rhs -= ms;
   if (rhs >= 0.5)
      ms++;

   lhs.setDate(QDate::fromJulianDay(d));
   lhs.setTime(h, m, s, ms);

   return true;
}

static bool convert(QDateTimeEx& lhs, const QString& rhs, const QLocaleEx& locale)
{
   if (locale.language() == QLocale::C)
      lhs = QDateTimeEx::fromISOString(rhs);
   else
      lhs = locale.toDateTimeEx(rhs);
   return lhs.isValid();
}

static bool convert(QDateTimeEx& lhs, const QByteArray& rhs, const QLocaleEx& locale)
{
#ifndef QT_NO_CAST_FROM_ASCII
   return convert(lhs, QString(rhs), locale);
#else
   return convert(lhs, QString::fromUtf8(rhs), locale);
#endif
}

static bool convert(QDateTimeEx& lhs, const QTime& rhs, const QLocaleEx& locale)
{
   Q_UNUSED(locale);

   lhs.setTime(rhs);
   return true;
}

static bool convert(QDateTimeEx& lhs, const QTimeEx& rhs, const QLocaleEx& locale)
{
   Q_UNUSED(locale);

   lhs = rhs;
   return true;
}

static bool convert(QDateTimeEx& lhs, const QDate& rhs, const QLocaleEx& locale)
{
   Q_UNUSED(locale);

   lhs.setDate(rhs);
   return true;
}

static bool convert(QDateTimeEx& lhs, const QDateEx& rhs, const QLocaleEx& locale)
{
   Q_UNUSED(locale);

   lhs = rhs;
   return true;
}

static bool convert(QDateTimeEx& lhs, const QDateTime& rhs, const QLocaleEx& locale)
{
   Q_UNUSED(locale);

   lhs.setDateTime(rhs);
   return true;
}

static bool convert(QDate& lhs, const QDateTimeEx& rhs, const QLocaleEx& locale)
{
   Q_UNUSED(locale);

   lhs = rhs.toQDate();
   return true;
}

static bool convert(QTime& lhs, const QDateTimeEx& rhs, const QLocaleEx& locale)
{
   Q_UNUSED(locale);

   lhs = rhs.toQTime();
   return true;
}

static bool convert(QDateTime& lhs, const QDateTimeEx& rhs, const QLocaleEx& locale)
{
   Q_UNUSED(locale);

   lhs = rhs.toQDateTime();
   return true;
}

static bool convert(QString& lhs, const QDateTimeEx& rhs, const QLocaleEx& locale)
{
   if (locale.language() == QLocale::C)
      lhs = rhs.toISOString();
   else
      lhs = locale.toString(rhs);
   return true;
}

//
// Variant conversion functions for QDateEx
//

static bool convert(QDateEx& lhs, const QDateTimeEx& rhs, const QLocaleEx& locale)
{
   Q_UNUSED(locale);

   lhs = rhs;
   return true;
}

static bool convert(QDateEx& lhs, const QDate& rhs, const QLocaleEx& locale)
{
   Q_UNUSED(locale);

   lhs.setDate(rhs);
   return true;
}

static bool convert(QDateEx& lhs, const QString& rhs, const QLocaleEx& locale)
{
   if (locale.language() == QLocale::C)
      lhs = QDateTimeEx::fromISOString(rhs);
   else
      lhs = locale.toDate(rhs);
   return lhs.isValid();
}

static bool convert(QDate& lhs, const QDateEx& rhs, const QLocaleEx& locale)
{
   Q_UNUSED(locale);

   lhs = rhs.toQDate();
   return true;
}

static bool convert(QDateTime& lhs, const QDateEx& rhs, const QLocaleEx& locale)
{
   Q_UNUSED(locale);

   lhs = rhs.toQDateTime();
   return true;
}

static bool convert(QString& lhs, const QDateEx& rhs, const QLocaleEx& locale)
{
   if (locale.language() == QLocale::C)
      lhs = rhs.toISOString();
   else
      lhs = locale.toString(rhs);
   return true;
}

//
// Variant conversion functions for QDateEx
//

static bool convert(QTimeEx& lhs, const QDateTimeEx& rhs, const QLocaleEx& locale)
{
   Q_UNUSED(locale);

   lhs = rhs;
   return true;
}

static bool convert(QTimeEx& lhs, const QTime& rhs, const QLocaleEx& locale)
{
   Q_UNUSED(locale);

   lhs.setTime(rhs);
   return true;
}

static bool convert(QTimeEx& lhs, const QString& rhs, const QLocaleEx& locale)
{
   if (locale.language() == QLocale::C)
      lhs = QDateTimeEx::fromISOString(rhs);
   else
      lhs = locale.toTime(rhs);
   return lhs.isValid();
}

static bool convert(QTime& lhs, const QTimeEx& rhs, const QLocaleEx& locale)
{
   Q_UNUSED(locale);

   lhs = rhs.toQTime();
   return true;
}

static bool convert(QString& lhs, const QTimeEx& rhs, const QLocaleEx& locale)
{
   if (locale.language() == QLocale::C)
      lhs = rhs.toISOString();
   else
      lhs = locale.toString(rhs);
   return true;
}

BEGIN_VARIANT_REGISTRATION

Variant::registerType<QDateTimeEx>();
Variant::registerType<QDateEx>();
Variant::registerType<QTimeEx>();

Variant::registerConversion<QDateTimeEx, int>();
Variant::registerConversion<QDateTimeEx, unsigned int>();
Variant::registerConversion<QDateTimeEx, long long int>();
Variant::registerConversion<QDateTimeEx, unsigned long long int>();
Variant::registerConversion<QDateTimeEx, double>();
Variant::registerConversion<QDateTimeEx, QString>();
Variant::registerConversion<QDateTimeEx, QTime>();
Variant::registerConversion<QDateTimeEx, QTimeEx>();
Variant::registerConversion<QDateTimeEx, QDate>();
Variant::registerConversion<QDateTimeEx, QDateEx>();
Variant::registerConversion<QDateTimeEx, QDateTime>();
Variant::registerConversion<QDate, QDateTimeEx>();
Variant::registerConversion<QTime, QDateTimeEx>();
Variant::registerConversion<QDateTime, QDateTimeEx>();
Variant::registerConversion<QString, QDateTimeEx>();
Variant::registerConversion<QDateTimeEx, QText>();
Variant::registerConversion<QDateTimeEx, QByteArray>();

Variant::registerConversion<QDateEx, QDateTimeEx>();
Variant::registerConversion<QDateEx, QDate>();
Variant::registerConversion<QDateEx, QString>();
Variant::registerConversion<QDateEx, QText>();
Variant::registerConversion<QDate, QDateEx>();
Variant::registerConversion<QDateTime, QDateEx>();
Variant::registerConversion<QString, QDateEx>();

Variant::registerConversion<QTimeEx, QDateTimeEx>();
Variant::registerConversion<QTimeEx, QTime>();
Variant::registerConversion<QTimeEx, QString>();
Variant::registerConversion<QTimeEx, QText>();
Variant::registerConversion<QTime, QTimeEx>();
Variant::registerConversion<QString, QTimeEx>();

END_VARIANT_REGISTRATION
