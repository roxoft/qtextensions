#include "stdafx.h"
#include "numeric.h"
#include "TemplateConversions.h"

using namespace std;

QTCOREEX_EXPORT string std::toString(double number, int precision, char negativeSign, char decimalPoint, char thousandsSeparator)
{
   string result;
   doubleToString(number, precision, result, negativeSign, decimalPoint, thousandsSeparator);
   return result;
}

QTCOREEX_EXPORT wstring std::toWString(double number, int precision, wchar_t negativeSign, wchar_t decimalPoint, wchar_t thousandsSeparator)
{
   wstring result;
   doubleToString(number, precision, result, negativeSign, decimalPoint, thousandsSeparator);
   return result;
}

QTCOREEX_EXPORT string   std::toString(short number, int base, char negativeSign, char thousandsSeparator) { string result; integerToString(number, base, result, negativeSign, thousandsSeparator); return result; }
QTCOREEX_EXPORT wstring  std::toWString(short number, int base, wchar_t negativeSign, wchar_t thousandsSeparator) { wstring result; integerToString(number, base, result, negativeSign, thousandsSeparator); return result; }
QTCOREEX_EXPORT string   std::toString(unsigned short number, int base, char negativeSign, char thousandsSeparator) { string result; integerToString(number, base, result, negativeSign, thousandsSeparator); return result; }
QTCOREEX_EXPORT wstring  std::toWString(unsigned short number, int base, wchar_t negativeSign, wchar_t thousandsSeparator) { wstring result; integerToString(number, base, result, negativeSign, thousandsSeparator); return result; }
QTCOREEX_EXPORT string   std::toString(int number, int base, char negativeSign, char thousandsSeparator) { string result; integerToString(number, base, result, negativeSign, thousandsSeparator); return result; }
QTCOREEX_EXPORT wstring  std::toWString(int number, int base, wchar_t negativeSign, wchar_t thousandsSeparator) { wstring result; integerToString(number, base, result, negativeSign, thousandsSeparator); return result; }
QTCOREEX_EXPORT string   std::toString(unsigned int number, int base, char negativeSign, char thousandsSeparator) { string result; integerToString(number, base, result, negativeSign, thousandsSeparator); return result; }
QTCOREEX_EXPORT wstring  std::toWString(unsigned int number, int base, wchar_t negativeSign, wchar_t thousandsSeparator) { wstring result; integerToString(number, base, result, negativeSign, thousandsSeparator); return result; }
QTCOREEX_EXPORT string   std::toString(long number, int base, char negativeSign, char thousandsSeparator) { string result; integerToString(number, base, result, negativeSign, thousandsSeparator); return result; }
QTCOREEX_EXPORT wstring  std::toWString(long number, int base, wchar_t negativeSign, wchar_t thousandsSeparator) { wstring result; integerToString(number, base, result, negativeSign, thousandsSeparator); return result; }
QTCOREEX_EXPORT string   std::toString(unsigned long number, int base, char negativeSign, char thousandsSeparator) { string result; integerToString(number, base, result, negativeSign, thousandsSeparator); return result; }
QTCOREEX_EXPORT wstring  std::toWString(unsigned long number, int base, wchar_t negativeSign, wchar_t thousandsSeparator) { wstring result; integerToString(number, base, result, negativeSign, thousandsSeparator); return result; }
QTCOREEX_EXPORT string   std::toString(long long int number, int base, char negativeSign, char thousandsSeparator) { string result; integerToString(number, base, result, negativeSign, thousandsSeparator); return result; }
QTCOREEX_EXPORT wstring  std::toWString(long long int number, int base, wchar_t negativeSign, wchar_t thousandsSeparator) { wstring result; integerToString(number, base, result, negativeSign, thousandsSeparator); return result; }
QTCOREEX_EXPORT string   std::toString(unsigned long long int number, int base, char negativeSign, char thousandsSeparator) { string result; integerToString(number, base, result, negativeSign, thousandsSeparator); return result; }
QTCOREEX_EXPORT wstring  std::toWString(unsigned long long int number, int base, wchar_t negativeSign, wchar_t thousandsSeparator) { wstring result; integerToString(number, base, result, negativeSign, thousandsSeparator); return result; }

QTCOREEX_EXPORT double std::toTime(double time, int& days, int& hours, int& minutes, int& seconds)
{
   days = (int)time;
   time -= days;
   time *= 24;
   hours = (int)time;
   time -= hours;
   time *= 60;
   minutes = (int)time;
   time -= minutes;
   time *= 60;
   seconds = (int)time;
   time -= seconds;

   return time;
}
