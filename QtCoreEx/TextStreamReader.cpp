#include "stdafx.h"
#include "TextStreamReader.h"

QChar QTextStreamCharacterReader::readChar()
{
   QChar nextChar;

   if (_is && !_is->atEnd())
   {
      *_is >> nextChar;
      _pos++;
   }

   return nextChar;
}

int QTextStreamCharacterReader::pos()
{
   // Using _is->pos() is extremely slow!
   return _pos;
}

QChar QStringCharacterReader::readChar()
{
   if (_pos < _text.length())
      return _text.at(_pos++);
   return {};
}

int QStringCharacterReader::pos()
{
   return _pos;
}

TextStreamReader::TextStreamReader(const QString& text)
{
   _dataSource = new QStringCharacterReader(text);
   _deleteDataSource = true;
}

TextStreamReader::TextStreamReader(QTextStream* is)
{
   _dataSource = new QTextStreamCharacterReader(is);
   _deleteDataSource = true;
}

TextStreamReader::~TextStreamReader()
{
   if (_deleteDataSource)
      delete _dataSource;
}

void TextStreamReader::setText(const QString& text)
{
   setDataSource(new QStringCharacterReader(text));
   _deleteDataSource = true;
}

void TextStreamReader::setDataSource(CharacterStreamReader* dataSource)
{
   if (_deleteDataSource)
      delete _dataSource;
   _dataSource = dataSource;
   _deleteDataSource = false;
   _buffer.clear();
   _positions.clear();
   _bufferPos = 0;
   _charCount = 0;
   _lineCount = 0;
}

void TextStreamReader::setStream(QTextStream* is)
{
   setDataSource(new QTextStreamCharacterReader(is));
   _deleteDataSource = true;
}

bool TextStreamReader::atEnd()
{
   return peekChar().isNull();
}

QChar TextStreamReader::peekChar(int index)
{
   index += _bufferPos;
   while (index >= _buffer.length())
   {
      QChar nextChar;
      auto pos = 0;

      if (_dataSource)
      {
         pos = _dataSource->pos();
         nextChar = _dataSource->readChar();
      }

      if (nextChar.isNull())
         return nextChar;

      _buffer += nextChar;
      _positions += pos;
   }

   return _buffer[index];
}

bool TextStreamReader::readChar(QChar& c)
{
   c = peekChar();

   if (c.isNull())
      return false;

   _charCount++;
   if (c == u'\n')
      _lineCount++;

   _bufferPos++;
   return true;
}

bool TextStreamReader::skipChar(int count)
{
   bool bufferTooSmall = false;

   if (count < 0 || _bufferPos + count > _buffer.length())
   {
      count = _buffer.length() - _bufferPos;
      bufferTooSmall = true;
   }

   _charCount += count;
   while (count--)
   {
      if (_buffer[_bufferPos] == u'\n')
         _lineCount++;
      _bufferPos++;
   }

   return !bufferTooSmall;
}

QString TextStreamReader::processedChars(int bufferPos, int length) const
{
   if (bufferPos >= _bufferPos)
      return QString();

   if (length < 0 || length > _bufferPos - bufferPos)
      length = _bufferPos - bufferPos;

   return _buffer.mid(bufferPos, length);
}

void TextStreamReader::removeProcessedChars()
{
   _buffer.remove(0, _bufferPos);
   _positions.remove(0, _bufferPos);
   _bufferPos = 0;
}

int TextStreamReader::pos() const
{
   if (_bufferPos < _positions.length())
      return _positions.at(_bufferPos);
   if (_dataSource)
      return _dataSource->pos();
   return -1;
}
