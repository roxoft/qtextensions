#include "stdafx.h"
#include "FunctionStatusBase.h"
#include "FunctionStatus.h"
#include <QTextStream>
#include <QLocaleEx.h>

void FunctionStatusBase::add(const StatusMessage& statusMessage)
{
   if (!statusMessage.message().isEmpty())
      _statusMessages.append(statusMessage);
}

void FunctionStatusBase::reset()
{
   _statusMessages.clear();
}

StatusMessage::Severity FunctionStatusBase::severity() const
{
   auto maxSeverity = StatusMessage::S_None;

   std::for_each(_statusMessages.begin(), _statusMessages.end(), [&maxSeverity](const StatusMessage& sm){ if (maxSeverity < sm.severity()) maxSeverity = sm.severity(); });

   return maxSeverity;
}

bool FunctionStatusBase::hasMessages(StatusMessage::Severities severities) const
{
   return std::any_of(_statusMessages.begin(), _statusMessages.end(), [severities](const StatusMessage& sm) { return severities.testFlag(sm.severity()); });
}

int FunctionStatusBase::totalMessageCount(StatusMessage::Severities severities) const
{
   return std::count_if(_statusMessages.begin(), _statusMessages.end(), [severities](const StatusMessage& sm){return severities.testFlag(sm.severity());});
}

QString FunctionStatusBase::completeMessage(StatusMessage::Severities severities) const
{
   QString msg;
   QTextStream result(&msg);

   generateMessage(severities, result);

   return msg.trimmed();
}

QList<StatusMessage> FunctionStatusBase::messages() const
{
   return _statusMessages;
}

QStringList FunctionStatusBase::messages(StatusMessage::Severities severities) const
{
   QStringList result;

   for (auto&& status : _statusMessages)
   {
      if (severities.testFlag(status.severity()))
      {
         result.append(status.message());
      }
   }

   return result;
}

void FunctionStatusBase::transferMessagesFrom(FunctionStatusBase* other, const QStringList& context)
{
   if (other == nullptr || other == this)
      return;

   for (auto&& message : other->_statusMessages)
   {
      if (context.isEmpty())
      {
         add(message);
      }
      else
      {
         add(StatusMessage(message, context));
      }
   }

   other->reset();
}

void FunctionStatusBase::generateMessage(StatusMessage::Severities severities, QTextStream& writer) const
{
   auto messageWritten = false;
   QStringList contextPath;

   for (auto&& sm : _statusMessages)
   {
      if (severities.testFlag(sm.severity()) && !sm.message().isEmpty())
      {
         auto newContextPath = sm.context();

         if (messageWritten && contextPath.length() != newContextPath.length())
#if QT_VERSION < QT_VERSION_CHECK(5,14,0)
            writer << endl;
#else
            writer << Qt::endl;
#endif

         auto level = 0;

         while (level < contextPath.length() && level < newContextPath.length())
         {
            if (contextPath.at(level) != newContextPath.at(level))
               break;
            level++;
         }

         if (level < newContextPath.length())
         {
            writer << QString(level * 3, u' ');
            writer << newContextPath.mid(level).join(u'/');
#if QT_VERSION < QT_VERSION_CHECK(5,14,0)
            writer << endl;
#else
            writer << Qt::endl;
#endif
            level = newContextPath.length();
         }

         contextPath = newContextPath;

         writer << QString(level * 3, u' ');
         switch (sm.severity())
         {
         case StatusMessage::S_Debug:
            writer << tr("Debug");
            break;
         case StatusMessage::S_Verbose:
            writer << tr("Details");
            break;
         case StatusMessage::S_Information:
            writer << tr("Information");
            break;
         case StatusMessage::S_Warning:
            writer << tr("Warning");
            break;
         case StatusMessage::S_Error:
            writer << tr("Error");
            break;
         case StatusMessage::S_Critical:
            writer << tr("Critical");
            break;
         }
         writer << QString(":");

#if QT_VERSION < QT_VERSION_CHECK(5,14,0)
         auto messageParts = sm.message().split(u'\n', QString::SkipEmptyParts);
#else
         auto messageParts = sm.message().split(u'\n', Qt::SkipEmptyParts);
#endif
         auto spaces = QString(" ");

         for (auto&& messagePart : messageParts)
         {
            writer << spaces;
            writer << messagePart;
#if QT_VERSION < QT_VERSION_CHECK(5,14,0)
            writer << endl;
#else
            writer << Qt::endl;
#endif
            spaces = QString((level + 1) * 3, u' ');
         }

         messageWritten = true;
      }
   }
}
