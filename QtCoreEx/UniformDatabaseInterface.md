Uniform database interface
==========================

The QDbConnection and QDbCommand classes provide an abstraction layer for different databases.
