#include "stdafx.h"
#include "QDbCommand.h"
#include "QDbCommandEngine.h"
#include "QDbConnectionData.h"

class CommandErrorHandler : public QDbState::ErrorHandler
{
public:
   CommandErrorHandler() {}
   virtual ~CommandErrorHandler() {}

   virtual void onStateChanged(const QDbState &dbState);
};

void CommandErrorHandler::onStateChanged(const QDbState &dbState)
{
   if (dbState.type() == QDbState::Error)
      throw dbState;
}


QDbResultSet::QDbResultSet()
{
}

QDbResultSet::QDbResultSet(const QDbResultSet &other) : _engine(other._engine), _state(other._state)
{
}

QDbResultSet::~QDbResultSet()
{
}

QDbResultSet &QDbResultSet::operator=(const QDbResultSet &rhs)
{
   _engine = rhs._engine;
   _state = rhs._state;
   return *this;
}

bool QDbResultSet::isValid() const
{
   return _engine && _state.type() != QDbState::Error;
}

QDbSchema::Table QDbResultSet::schema() const
{
   if (_engine)
      return _engine->resultSchema();
   return QDbSchema::Table();
}

bool QDbResultSet::atEnd() const
{
   return !_engine || _engine->atEnd();
}

bool QDbResultSet::next()
{
   if (_engine && _state.type() != QDbState::Error)
   {
      _state = _engine->next();
      if (_state.type() != QDbState::Error)
         return !_engine->atEnd();
   }

   return false;
}

int QDbResultSet::affectedRows()
{
   int rowCount = -1;

   if (_engine)
      _state = _engine->rowCount(rowCount);

   return rowCount;
}

Variant QDbResultSet::valueAt(int index)
{
   Variant value;

   if (_engine)
      _state = _engine->columnValue(index, value);

   return value;
}

Variant QDbResultSet::valueAt(const QString &name)
{
   Variant value;

   if (_engine)
      _state = _engine->columnValue(name, value);

   return value;
}

Variant QDbResultSet::operator[](int index)
{
   return valueAt(index);
}

Variant QDbResultSet::operator[](const QString &name)
{
   return valueAt(name);
}


QDbCommand::QDbCommand()
{
}

QDbCommand::QDbCommand(QDbConnection connection) : _connection(connection)
{
   _state = connection.state();
}

QDbCommand::QDbCommand(const QString& statement)
{
   setStatement(statement);
}

QDbCommand::QDbCommand(QDbConnection connection, const QString& statement) : _connection(connection)
{
   _state = connection.state();
   setStatement(statement);
}

QDbCommand::QDbCommand(const QDbSchema::Function& function)
{
   setStatement(function);
}

QDbCommand::QDbCommand(QDbConnection connection, const QDbSchema::Function& function) : _connection(connection)
{
   _state = connection.state();
   setStatement(function);
}

QDbCommand::QDbCommand(const QDbCommand &other) : _connection(other._connection), _engine(other._engine), _state(other._state)
{
}

QDbCommand::~QDbCommand()
{
}

QDbCommand &QDbCommand::operator=(const QDbCommand &rhs)
{
   _connection = rhs._connection;
   _engine = rhs._engine;
   _state = rhs._state;
   return *this;
}

bool QDbCommand::exceptionsEnabled() const
{
   return dynamic_cast<CommandErrorHandler *>(_state.errorHandler().data());
}

void QDbCommand::enableExceptions()
{
   _state.setErrorHandler(QDbState::ErrhPtr(new CommandErrorHandler));
}

void QDbCommand::disableExceptions()
{
   if (exceptionsEnabled())
      _state.setErrorHandler(QDbState::ErrhPtr());
}

QDbConnection QDbCommand::connection() const
{
   if (!_connection.isValid())
      return QDbConnection::defaultConnection();
   return _connection;
}

void QDbCommand::setStatement(const QString& statement)
{
   setStatement(QDbConnection(), statement);
}

void QDbCommand::setStatement(const QDbConnection& connection, const QString& statement)
{
   _engine.reset();
   _state.resetState();

   if (connection.isValid())
      _connection = connection;
   if (!_connection.isValid())
      _connection = QDbConnection::defaultConnection();
   _state = _connection.state();

   if (_state.type() == QDbState::Error)
   {
      return;
   }

   if (statement.isEmpty())
      _state = QDbState(QDbState::Error, tr("SQL statement is empty"));
   else if (!_connection.isValid())
      _state = QDbState(QDbState::Error, tr("No database connection"));
   else
   {
      _engine = _connection._data->newEngine();
      if (!_engine)
         _state = QDbState(QDbState::Error, tr("No database engine"));
      else
         _state = _engine->setStatement(statement);
   }
}

void QDbCommand::setStatement(const QDbSchema::Function& function)
{
   setStatement(QDbConnection(), function);
}

void QDbCommand::setStatement(const QDbConnection& connection, const QDbSchema::Function& function)
{
   _engine.reset();
   _state.resetState();

   if (connection.isValid())
      _connection = connection;
   if (!_connection.isValid())
      _connection = QDbConnection::defaultConnection();
   _state = _connection.state();

   if (!function.isValid())
      _state = QDbState(QDbState::Error, tr("Procedure is invalid"));
   else if (!_connection.isValid())
      _state = QDbState(QDbState::Error, tr("No database connection"));
   else if (_state.type() != QDbState::Error)
   {
      _engine = _connection._data->newEngine();
      if (!_engine)
         _state = QDbState(QDbState::Error, tr("No database engine"));
      else
         _state = _engine->setStatement(function);
   }
}

void QDbCommand::setProcedure(const QString& procedure)
{
   setProcedure(QDbConnection(), procedure);
}

void QDbCommand::setProcedure(const QDbConnection& connection, const QString& procedure)
{
   _engine.reset();
   _state.resetState();

   if (connection.isValid())
      _connection = connection;
   if (!_connection.isValid())
      _connection = QDbConnection::defaultConnection();
   _state = _connection.state();

   if (procedure.isEmpty())
      _state = QDbState(QDbState::Error, tr("Procedure is invalid"));
   else if (!_connection.isValid())
      _state = QDbState(QDbState::Error, tr("No database connection"));
   else if (_state.type() != QDbState::Error)
   {
      _engine = _connection._data->newEngine();
      if (!_engine)
         _state = QDbState(QDbState::Error, tr("No database engine"));
      else
         _state = _engine->setProcedure(procedure);
   }
}

QString QDbCommand::statement() const
{
   if (_engine)
      return _engine->statement();
   return QString();
}

void QDbCommand::parsePlaceholders()
{
   if (_engine)
      _engine->preparePlaceholder();
}

void QDbCommand::setPlaceholderType(const QString& placeholder, QDbDataType type)
{
   if (type == DT_UNKNOWN)
      _state = QDbState(QDbState::Error, tr("Missing type for the placeholder '%1'").arg(placeholder), QDbState::Statement);
   else if (_engine)
   {
      _engine->preparePlaceholder();

      auto phIndex = _engine->placeholderIndex(placeholder.toUpper());

      if (phIndex == -1)
      {
         _state = QDbState(QDbState::Error, tr("Placeholder '%1' not found").arg(placeholder), _engine->statement());
      }
      else
      {
         _state = _engine->setPlaceholderType(phIndex, type);
         _state = _engine->setPlaceholderIsReceiving(phIndex, true);
      }
   }
}

void QDbCommand::setPlaceholderType(int pos, QDbDataType type)
{
   if (type == DT_UNKNOWN)
      _state = QDbState(QDbState::Error, tr("Missing type for the placeholder at position %1").arg(pos), QDbState::Statement);
   else if (_engine)
   {
      _engine->preparePlaceholder();

      _state = _engine->setPlaceholderType(pos - 1, type);
      _state = _engine->setPlaceholderIsReceiving(pos - 1, true);
   }
}

void QDbCommand::setPlaceholderValue(const QString& placeholder, const Variant& value, QDbDataType type, QDbTransferMode transferMode)
{
   if (_engine)
   {
      _engine->preparePlaceholder();

      auto phIndex = _engine->placeholderIndex(placeholder.toUpper());

      if (phIndex == -1)
      {
         _state = QDbState(QDbState::Error, tr("Placeholder '%1' not found").arg(placeholder), _engine->statement());
      }
      else
      {
         setPlaceholderValue(phIndex + 1, value, type, transferMode);
      }
   }
}

void QDbCommand::setPlaceholderValue(int pos, const Variant& value, QDbDataType type, QDbTransferMode transferMode)
{
   if (_engine)
   {
      _engine->preparePlaceholder();

      if (type != DT_UNKNOWN)
      {
         _state = _engine->setPlaceholderType(pos - 1, type);

         if (_state.type() == QDbState::Error)
         {
            return;
         }
      }

      switch (transferMode)
      {
      case TM_Default:
         break;
      case TM_Input:
         _state = _engine->setPlaceholderIsSending(pos - 1, true);
         _state = _engine->setPlaceholderIsReceiving(pos - 1, false);
         break;
      case TM_Output:
         _state = _engine->setPlaceholderIsSending(pos - 1, false);
         _state = _engine->setPlaceholderIsReceiving(pos - 1, true);
         break;
      case TM_InputOutput:
         _state = _engine->setPlaceholderIsSending(pos - 1, true);
         _state = _engine->setPlaceholderIsReceiving(pos - 1, true);
         break;
      }

      if (_state.type() == QDbState::Error)
      {
         return;
      }

      _state = _engine->setPlaceholderValue(pos - 1, value);
   }
}

QList<int> QDbCommand::placeholderPosition(const QString &name) const
{
   if (_engine)
      return _engine->placeholderPositions(name.toUpper());
   return QList<int>();
}

Variant QDbCommand::placeholderValue(const QString& name)
{
   Variant value;

   if (_engine)
   {
      auto phIndex = _engine->placeholderIndex(name.toUpper());

      if (phIndex == -1)
      {
         _state = QDbState(QDbState::Error, tr("Placeholder '%1' not found").arg(name), _engine->statement());
      }
      else
      {
         _state = _engine->placeholderValue(phIndex, value);
      }
   }

   return value;
}

Variant QDbCommand::placeholderValue(int pos)
{
   Variant value;

   if (_engine)
   {
      _state = _engine->placeholderValue(pos - 1, value);
   }

   return value;
}

QDbTransferMode QDbCommand::placeholderTransferMode(const QString& name)
{
   QDbTransferMode transferMode = TM_Default;

   if (_engine)
   {
      auto phIndex = _engine->placeholderIndex(name.toUpper());

      if (phIndex == -1)
      {
         _state = QDbState(QDbState::Error, tr("Placeholder '%1' not found").arg(name), _engine->statement());
      }
      else
      {
         transferMode = placeholderTransferMode(phIndex + 1);
      }
   }

   return transferMode;
}

QDbTransferMode QDbCommand::placeholderTransferMode(int pos)
{
   QDbTransferMode transferMode = TM_Default;

   if (_engine)
   {
      bool isSending;

      _state = _engine->placeholderIsSending(pos - 1, isSending);
      if (_state.type() == QDbState::Error)
         return transferMode;

      bool isReceiving;

      _state = _engine->placeholderIsReceiving(pos - 1, isReceiving);
      if (_state.type() == QDbState::Error)
         return transferMode;

      if (isSending && isReceiving)
         transferMode = TM_InputOutput;
      else if (isSending)
         transferMode = TM_Input;
      else if (isReceiving)
         transferMode = TM_Output;
   }

   return transferMode;
}

bool QDbCommand::prepare()
{
   if (_state.type() == QDbState::Error)
      return false;

   if (_engine)
      _state = _engine->prepare();
   else
      _state = QDbState(QDbState::Error, tr("No database engine"), QDbState::Connection);

   return _state.type() != QDbState::Error;
}

bool QDbCommand::exec(int rowsToFetch)
{
   if (_state.type() == QDbState::Error)
      return false;

   if (_engine)
      _state = _engine->exec(rowsToFetch > 0 ? rowsToFetch : 100);
   else
      _state = QDbState(QDbState::Error, tr("No database engine"), QDbState::Connection);

   return _state.type() != QDbState::Error;
}

bool QDbCommand::exec(QList<SqlParameter>& phList, int rowsToFetch)
{
   bool hasPostLob = false;

   for (auto&& ph : phList)
   {
      if (ph.isPostLob)
      {
         setPlaceholderType(ph.name, ph.getValue().type() == typeid(QString) || ph.getValue().type() == typeid(QText) ? DT_LARGE_STRING : DT_LARGE_BINARY);
         hasPostLob = true;
      }
      else if (ph.transferMode == TM_Output)
      {
         setPlaceholderType(ph.name, ph.type);
      }
      else
      {
         setPlaceholderValue(ph.name, ph.getValue(), ph.type, ph.transferMode);
      }
   }

   if (_state.type() == QDbState::Error)
      return false;

   QSqlTransaction transaction;

   if (hasPostLob)
      transaction.begin(connection());

   if (!exec(rowsToFetch))
      return false;

   if (hasPostLob)
   {
      for (auto&& ph : phList)
      {
         if (ph.isPostLob)
            setPlaceholderValue(ph.name, ph.getValue());
      }

      if (_state.type() == QDbState::Error)
         return false;

      transaction.commit();
   }

   // Fetch returning values
   for (auto&& ph : phList)
   {
      if (!ph.isPostLob && (ph.transferMode & TM_Output))
         ph.setValue(placeholderValue(ph.name));
   }

   return _state.type() != QDbState::Error;
}

bool QDbCommand::exec(const QString& statement, QList<SqlParameter>& phList, int rowsToFetch)
{
   setStatement(statement);

   if (_state.type() == QDbState::Error)
      return false;

   return exec(phList, rowsToFetch);
}

bool QDbCommand::exec(const QString& statement, int rowsToFetch)
{
   setStatement(statement);

   if (_state.type() == QDbState::Error)
      return false;

   return exec(rowsToFetch);
}

QString QDbCommand::preparedStatement() const
{
   if (_engine)
      return _engine->preparedStatement();
   return QString();
}

int QDbCommand::affectedRows()
{
   int rowCount = -1;

   if (_engine)
      _state = _engine->rowCount(rowCount);

   return rowCount;
}

QDbResultSet QDbCommand::result()
{
   QDbResultSet result;

   result._engine = _engine;
   result._state = _state;

   return result;
}

QString QDbCommand::description() const
{
   if (_engine)
      return _engine->description();
   return tr("No query");
}

bool QDbCommand::tableExists(const QString& tableName)
{
   return tableExists(QDbConnection(), tableName);
}

bool QDbCommand::tableExists(QDbConnection connection, const QString& tableName)
{
   if (!connection.isValid())
      connection = QDbConnection::defaultConnection();

   if (connection.isValid())
      return connection.tableExists(tableName);

   return false;
}
