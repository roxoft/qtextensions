#include "stdafx.h"
#include "FunctionStatus.h"
#include <QObject>

FunctionStatus::FunctionStatus(const QString& name)
   : _statusImpl(new FunctionStatusBase)
{
   if (!name.isEmpty())
      _context << name;
}

FunctionStatus::FunctionStatus(FunctionStatusBase* status, const QString& name)
   : _statusImpl(status)
{
   if (!name.isEmpty())
      _context << name;
}

FunctionStatus::FunctionStatus(const bp<FunctionStatusBase>& statusBase, const QStringList& context)
   : _context(context), _statusImpl(statusBase)
{
}

FunctionStatus::FunctionStatus(const FunctionStatus& other)
   : _context(other._context), _statusImpl(other._statusImpl)
{
}

FunctionStatus::~FunctionStatus()
{
}

FunctionStatus& FunctionStatus::operator=(const FunctionStatus& other)
{
   _context = other._context;
   _statusImpl = other._statusImpl;
   return *this;
}

bool FunctionStatus::operator==(const FunctionStatus& rhs) const
{
   return _context == rhs._context && _statusImpl == rhs._statusImpl;
}

void FunctionStatus::addMessage(QString message, StatusMessage::Severity severity)
{
   message = message.trimmed();
   if (message.isEmpty())
      message = "-?-";
   _statusImpl->add(StatusMessage(severity, message, _context));
}

FunctionStatus FunctionStatus::subStatus(const QString& name) const
{
   if (name.isEmpty())
      return FunctionStatus(_statusImpl, _context);
   return FunctionStatus(_statusImpl, QStringList(_context) << name);
}

void FunctionStatus::transferMessagesFrom(FunctionStatus status)
{
   _statusImpl->transferMessagesFrom(status._statusImpl, _context);
}
