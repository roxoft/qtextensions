#include "stdafx.h"
#include "checksum.h"

#define CRC32POLYREV 0xEDB88320 /* CRC-32 Polynom, umgekehrte Bitfolge */

QTCOREEX_EXPORT quint32 crc32(QIODevice* device)
{
   quint32 crc32_rev = 0xffffffff; /* Schieberegister, Startwert (111...) */
   char c;

   while (device->read(&c, 1) == 1)
   {
      int databits = 8;

      while (databits--)
      {
         if ((crc32_rev & 1) != (c & 1))
            crc32_rev = (crc32_rev >> 1) ^ CRC32POLYREV;
         else
            crc32_rev >>= 1;
         c >>= 1;
      }
   }

   return crc32_rev ^ 0xffffffff; /* inverses Ergebnis, MSB zuerst */
}

void Crc32::addChar(char c)
{
   int databits = 8;

   while (databits--)
   {
      if ((crc32_rev & 1) != (c & 1))
         crc32_rev = (crc32_rev >> 1) ^ CRC32POLYREV;
      else
         crc32_rev >>= 1;
      c >>= 1;
   }
}
