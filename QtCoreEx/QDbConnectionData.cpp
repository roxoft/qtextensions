#include "stdafx.h"
#include "QDbConnectionData.h"
#include "TemplateConversions.h"

QDbConnectionData* QDbConnectionData::_default = nullptr;

QDbConnectionData::QDbConnectionData()
{
   if (_default == nullptr)
      _default = this;
}

QDbConnectionData::~QDbConnectionData()
{
   if (_default == this)
      _default = nullptr;
}

bool QDbConnectionData::usePlaceholder(QDbDataType dbType, const Variant& value) const
{
   if (value.isValueNull())
      return false;

   Q_ASSERT(dbType != DT_UNKNOWN);

   switch (dbType)
   {
   case DT_UNKNOWN:
      return false;
   case DT_BOOL:
   case DT_INT:
   case DT_UINT:
   case DT_LONG:
   case DT_ULONG:
   case DT_DECIMAL:
      return false;
   case DT_DOUBLE:
      return true;
   case DT_STRING:
   case DT_LARGE_STRING:
   case DT_NSTRING:
   case DT_LARGE_NSTRING:
      return value.toString(QLocale::C).length() > _maxLiteralStringLength;
   case DT_DATE:
   case DT_TIME:
   case DT_DATETIME:
      return true;
   case DT_BINARY:
   case DT_LARGE_BINARY:
      return !treatEmptyAsNull() || !value.toByteArray().isEmpty();
   }

   return true;
}

QString QDbConnectionData::toBooleanLiteral(bool value) const
{
   return value ? "TRUE" : "FALSE";
}

QString QDbConnectionData::toNumericLiteral(long long value) const
{
   return QString::number(value);
}

QString QDbConnectionData::toNumericLiteral(double value) const
{
   QString result;

   doubleToString(value, -16, result);

   return result;
}

QString QDbConnectionData::toNumericLiteral(const QDecimal& value) const
{
   return value.toString(QLocale::C);
}

QString QDbConnectionData::toDateLiteral(const QDateEx& value) const
{
   return QString("DATE '%1'").arg(value.toString(SQL92_DATE_FORMAT));
}

QString QDbConnectionData::toTimeLiteral(const QTimeEx& value) const
{
   return QString("TIME '%1'").arg(value.toString(SQL92_TIME_FORMAT));
}

QString QDbConnectionData::toDateTimeLiteral(const QDateTimeEx& value) const
{
   return QString("TIMESTAMP '%1'").arg(value.toString(SQL92_DATETIME_FORMAT));
}

QString QDbConnectionData::toStringLiteral(QString value) const
{
   return QString("'%1'").arg(value.replace("'", "''"));
}

QString QDbConnectionData::toBinaryLiteral(const QByteArray& value) const
{
   return QString("X'%1'").arg(QString::fromLatin1(value.toHex()));
}
