#include "stdafx.h"
#include "HtmlTools.h"

struct HtmlCharacterEntity
{
   const char*  szEntity;
   QChar        character;
};

static const HtmlCharacterEntity latin1EntityList[] =
{
   {"nbsp", (ushort)160},
   {"iexcl", (ushort)161},
   {"cent", (ushort)162},
   {"pound", (ushort)163},
   {"curren", (ushort)164},
   {"yen", (ushort)165},
   {"brvbar", (ushort)166},
   {"sect", (ushort)167},
   {"uml", (ushort)168},
   {"copy", (ushort)169},
   {"ordf", (ushort)170},
   {"laquo", (ushort)171},
   {"not", (ushort)172},
   {"shy", (ushort)173},
   {"reg", (ushort)174},
   {"macr", (ushort)175},
   {"deg", (ushort)176},
   {"plusmn", (ushort)177},
   {"sup2", (ushort)178},
   {"sup3", (ushort)179},
   {"acute", (ushort)180},
   {"micro", (ushort)181},
   {"para", (ushort)182},
   {"middot", (ushort)183},
   {"cedil", (ushort)184},
   {"sup1", (ushort)185},
   {"ordm", (ushort)186},
   {"raquo", (ushort)187},
   {"frac14", (ushort)188},
   {"frac12", (ushort)189},
   {"frac34", (ushort)190},
   {"iquest", (ushort)191},
   {"Agrave", (ushort)192},
   {"Aacute", (ushort)193},
   {"Acirc", (ushort)194},
   {"Atilde", (ushort)195},
   {"Auml", (ushort)196},
   {"Aring", (ushort)197},
   {"AElig", (ushort)198},
   {"Ccedil", (ushort)199},
   {"Egrave", (ushort)200},
   {"Eacute", (ushort)201},
   {"Ecirc", (ushort)202},
   {"Euml", (ushort)203},
   {"Igrave", (ushort)204},
   {"Iacute", (ushort)205},
   {"Icirc", (ushort)206},
   {"Iuml", (ushort)207},
   {"ETH", (ushort)208},
   {"Ntilde", (ushort)209},
   {"Ograve", (ushort)210},
   {"Oacute", (ushort)211},
   {"Ocirc", (ushort)212},
   {"Otilde", (ushort)213},
   {"Ouml", (ushort)214},
   {"times", (ushort)215},
   {"Oslash", (ushort)216},
   {"Ugrave", (ushort)217},
   {"Uacute", (ushort)218},
   {"Ucirc", (ushort)219},
   {"Uuml", (ushort)220},
   {"Yacute", (ushort)221},
   {"THORN", (ushort)222},
   {"szlig", (ushort)223},
   {"agrave", (ushort)224},
   {"aacute", (ushort)225},
   {"acirc", (ushort)226},
   {"atilde", (ushort)227},
   {"auml", (ushort)228},
   {"aring", (ushort)229},
   {"aelig", (ushort)230},
   {"ccedil", (ushort)231},
   {"egrave", (ushort)232},
   {"eacute", (ushort)233},
   {"ecirc", (ushort)234},
   {"euml", (ushort)235},
   {"igrave", (ushort)236},
   {"iacute", (ushort)237},
   {"icirc", (ushort)238},
   {"iuml", (ushort)239},
   {"eth", (ushort)240},
   {"ntilde", (ushort)241},
   {"ograve", (ushort)242},
   {"oacute", (ushort)243},
   {"ocirc", (ushort)244},
   {"otilde", (ushort)245},
   {"ouml", (ushort)246},
   {"divide", (ushort)247},
   {"oslash", (ushort)248},
   {"ugrave", (ushort)249},
   {"uacute", (ushort)250},
   {"ucirc", (ushort)251},
   {"uuml", (ushort)252},
   {"yacute", (ushort)253},
   {"thorn", (ushort)254},
   {"yuml", (ushort)255},
};

static const HtmlCharacterEntity symbolEntityList[] =
{
   {"fnof", (ushort)402},
   {"Alpha", (ushort)913},
   {"Beta", (ushort)914},
   {"Gamma", (ushort)915},
   {"Delta", (ushort)916},
   {"Epsilon", (ushort)917},
   {"Zeta", (ushort)918},
   {"Eta", (ushort)919},
   {"Theta", (ushort)920},
   {"Iota", (ushort)921},
   {"Kappa", (ushort)922},
   {"Lambda", (ushort)923},
   {"Mu", (ushort)924},
   {"Nu", (ushort)925},
   {"Xi", (ushort)926},
   {"Omicron", (ushort)927},
   {"Pi", (ushort)928},
   {"Rho", (ushort)929},
   {"Sigma", (ushort)931},
   {"Tau", (ushort)932},
   {"Upsilon", (ushort)933},
   {"Phi", (ushort)934},
   {"Chi", (ushort)935},
   {"Psi", (ushort)936},
   {"Omega", (ushort)937},
   {"alpha", (ushort)945},
   {"beta", (ushort)946},
   {"gamma", (ushort)947},
   {"delta", (ushort)948},
   {"epsilon", (ushort)949},
   {"zeta", (ushort)950},
   {"eta", (ushort)951},
   {"theta", (ushort)952},
   {"iota", (ushort)953},
   {"kappa", (ushort)954},
   {"lambda", (ushort)955},
   {"mu", (ushort)956},
   {"nu", (ushort)957},
   {"xi", (ushort)958},
   {"omicron", (ushort)959},
   {"pi", (ushort)960},
   {"rho", (ushort)961},
   {"sigmaf", (ushort)962},
   {"sigma", (ushort)963},
   {"tau", (ushort)964},
   {"upsilon", (ushort)965},
   {"phi", (ushort)966},
   {"chi", (ushort)967},
   {"psi", (ushort)968},
   {"omega", (ushort)969},
   {"thetasym", (ushort)977},
   {"upsih", (ushort)978},
   {"piv", (ushort)982},
   {"bull", (ushort)8226},
   {"hellip", (ushort)8230},
   {"prime", (ushort)8242},
   {"Prime", (ushort)8243},
   {"oline", (ushort)8254},
   {"frasl", (ushort)8260},
   {"weierp", (ushort)8472},
   {"image", (ushort)8465},
   {"real", (ushort)8476},
   {"trade", (ushort)8482},
   {"alefsym", (ushort)8501},
   {"larr", (ushort)8592},
   {"uarr", (ushort)8593},
   {"rarr", (ushort)8594},
   {"darr", (ushort)8595},
   {"harr", (ushort)8596},
   {"crarr", (ushort)8629},
   {"lArr", (ushort)8656},
   {"uArr", (ushort)8657},
   {"rArr", (ushort)8658},
   {"dArr", (ushort)8659},
   {"hArr", (ushort)8660},
   {"forall", (ushort)8704},
   {"part", (ushort)8706},
   {"exist", (ushort)8707},
   {"empty", (ushort)8709},
   {"nabla", (ushort)8711},
   {"isin", (ushort)8712},
   {"notin", (ushort)8713},
   {"ni", (ushort)8715},
   {"prod", (ushort)8719},
   {"sum", (ushort)8721},
   {"minus", (ushort)8722},
   {"lowast", (ushort)8727},
   {"radic", (ushort)8730},
   {"prop", (ushort)8733},
   {"infin", (ushort)8734},
   {"ang", (ushort)8736},
   {"and", (ushort)8743},
   {"or", (ushort)8744},
   {"cap", (ushort)8745},
   {"cup", (ushort)8746},
   {"int", (ushort)8747},
   {"there4", (ushort)8756},
   {"sim", (ushort)8764},
   {"cong", (ushort)8773},
   {"asymp", (ushort)8776},
   {"ne", (ushort)8800},
   {"equiv", (ushort)8801},
   {"le", (ushort)8804},
   {"ge", (ushort)8805},
   {"sub", (ushort)8834},
   {"sup", (ushort)8835},
   {"nsub", (ushort)8836},
   {"sube", (ushort)8838},
   {"supe", (ushort)8839},
   {"oplus", (ushort)8853},
   {"otimes", (ushort)8855},
   {"perp", (ushort)8869},
   {"sdot", (ushort)8901},
   {"lceil", (ushort)8968},
   {"rceil", (ushort)8969},
   {"lfloor", (ushort)8970},
   {"rfloor", (ushort)8971},
   {"lang", (ushort)9001},
   {"rang", (ushort)9002},
   {"loz", (ushort)9674},
   {"spades", (ushort)9824},
   {"clubs", (ushort)9827},
   {"hearts", (ushort)9829},
   {"diams", (ushort)9830},
};

static const HtmlCharacterEntity specialEntityList[] =
{
   {"quot", (ushort)34},
   {"amp", (ushort)38},
   {"lt", (ushort)60},
   {"gt", (ushort)62},
   {"OElig", (ushort)338},
   {"oelig", (ushort)339},
   {"Scaron", (ushort)352},
   {"scaron", (ushort)353},
   {"Yuml", (ushort)376},
   {"circ", (ushort)710},
   {"tilde", (ushort)732},
   {"ensp", (ushort)8194},
   {"emsp", (ushort)8195},
   {"thinsp", (ushort)8201},
   {"zwnj", (ushort)8204},
   {"zwj", (ushort)8205},
   {"lrm", (ushort)8206},
   {"rlm", (ushort)8207},
   {"ndash", (ushort)8211},
   {"mdash", (ushort)8212},
   {"lsquo", (ushort)8216},
   {"rsquo", (ushort)8217},
   {"sbquo", (ushort)8218},
   {"ldquo", (ushort)8220},
   {"rdquo", (ushort)8221},
   {"bdquo", (ushort)8222},
   {"dagger", (ushort)8224},
   {"Dagger", (ushort)8225},
   {"permil", (ushort)8240},
   {"lsaquo", (ushort)8249},
   {"rsaquo", (ushort)8250},
   {"euro", (ushort)8364},
};

bool isTextHtml(QString text)
{
   text = text.toUpper();

   if (text.contains("<BIG>") && text.contains("</BIG>"))
      return true;
   if (text.contains("<B>") && text.contains("</B>"))
      return true;
   if (text.contains("<DIV>") && text.contains("</DIV>"))
      return true;
   if (text.contains("<U>") && text.contains("</U>"))
      return true;
   if (text.contains("<FONT") && text.contains("</FONT>"))
      return true;
   if (text.contains("<I>") && text.contains("</I>"))
      return true;
   if (text.contains("<HTML>") && text.contains("</HTML>"))
      return true;
   if (text.contains("<BODY") && text.contains("</BODY>"))
      return true;
   if (text.contains("<SPAN") && text.contains("</SPAN>"))
      return true;
   if (text.contains("<P") && text.contains("</P>"))
      return true;

   return false;
}

class HtmlEntitiesImpl : public HtmlEntities
{
public:
   HtmlEntitiesImpl();
   virtual ~HtmlEntitiesImpl() {}

   virtual const QMap<QChar, const char*>& charMap() const { return _charMap; }
   virtual const QMap<QByteArray, QChar>& entityMap() const { return _entityMap; }

   virtual QString encode(const QString& text) const;
   virtual QString decode(const QString& html) const;

private:
   QMap<QChar, const char*> _charMap;
   QMap<QByteArray, QChar> _entityMap;
};

#include <QStringList>

HtmlEntitiesImpl::HtmlEntitiesImpl()
{
   for (int i = sizeof(latin1EntityList) / sizeof(*latin1EntityList); i--; )
   {
      _charMap.insert(latin1EntityList[i].character, latin1EntityList[i].szEntity);
      _entityMap.insert(latin1EntityList[i].szEntity, latin1EntityList[i].character);
   }
   for (int i = sizeof(symbolEntityList) / sizeof(*symbolEntityList); i--; )
   {
      _charMap.insert(symbolEntityList[i].character, symbolEntityList[i].szEntity);
      _entityMap.insert(symbolEntityList[i].szEntity, symbolEntityList[i].character);
   }
   for (int i = sizeof(specialEntityList) / sizeof(*specialEntityList); i--; )
   {
      _charMap.insert(specialEntityList[i].character, specialEntityList[i].szEntity);
      _entityMap.insert(specialEntityList[i].szEntity, specialEntityList[i].character);
   }
}

QString HtmlEntitiesImpl::encode(const QString& text) const
{
   QString  result;
   bool     space = false;

   foreach (QChar qChar, text)
   {
      if (qChar.isSpace())
      {
         if (qChar == QLatin1Char('\n'))
         {
            result += QLatin1String("<br />");
            space = false;
         }
         else
            space = true;
      }
      else
      {
         if (space && !result.isEmpty())
            result += QLatin1Char(' ');
         space = false;

         auto it = _charMap.find(qChar);

         if (it == _charMap.end())
            result += qChar;
         else
         {
            result += QLatin1Char('&');
            result += QString::fromLatin1(it.value());
            result += QLatin1Char(';');
         }
      }
   }

   return result;
}

QString HtmlEntitiesImpl::decode(const QString& html) const
{
   QString  result;
   bool     isEntity = false;
   bool     isToken = false;
   QString  entity;

   foreach (QChar qChar, html)
   {
      if (isToken)
      {
         if (qChar == QLatin1Char('>'))
         {
            int end = entity.size();

            if (entity.endsWith(QLatin1Char('/')))
               end--;

            if (entity.left(end).compare(QLatin1String("br"), Qt::CaseInsensitive) == 0)
            {
               result += QLatin1Char('\n');
            }
            else
            {
               result += QLatin1Char('<');
               result += entity;
               result += qChar;
            }
            entity.clear();
            isToken = false;
         }
         else if (qChar == QLatin1Char('<'))
         {
            entity.clear();
         }
         else if (!qChar.isSpace())
         {
            entity += qChar;
         }
      }
      else if (isEntity)
      {
         if (qChar == QLatin1Char(';'))
         {
            auto it = _entityMap.find(entity.toLatin1());

            if (it == _entityMap.end())
            {
               result += QLatin1Char('&');
               result += entity;
               result += qChar;
            }
            else
            {
               result += it.value();
            }
            entity.clear();
            isEntity = false;
         }
         else if (qChar.isLetterOrNumber())
            entity += qChar;
         else
         {
            result += QLatin1Char('&');
            result += entity;
            result += qChar;
            entity.clear();
            isEntity = false;
         }
      }
      else if (qChar == QLatin1Char('&'))
      {
         isEntity = true;
      }
      else if (qChar == QLatin1Char('<'))
      {
         isToken = true;
      }
      else
      {
         result += qChar;
      }
   }

   return result;
}

QTCOREEX_EXPORT const HtmlEntities &htmlEntities()
{
   static const HtmlEntitiesImpl theHtmlEntities;

   return theHtmlEntities;
}

QChar HtmlStreamReader::peekChar(int index)
{
   index += _bufferPos;
   while (index >= _buffer.length())
   {
      auto nextChar = translateNextChar();

      if (nextChar.isNull())
         return nextChar;
   }

   return _buffer[index];
}

void HtmlStreamReader::init()
{
   // Create a hierarchical map of all entities in entityList.
   // This hierarchy of character lists is used to incrementally find a named entity as quickly as possible without using too much memory.

   for (int l = 0; l < 3; ++l)
   {
      const HtmlCharacterEntity* entityList = nullptr;
      int                        i = 0;

      switch (l)
      {
      case 0:
         entityList = latin1EntityList;
         i = sizeof(latin1EntityList) / sizeof(*latin1EntityList);
         break;
      case 1:
         entityList = symbolEntityList;
         i = sizeof(symbolEntityList) / sizeof(*symbolEntityList);
         break;
      case 2:
         entityList = specialEntityList;
         i = sizeof(specialEntityList) / sizeof(*specialEntityList);
         break;
      }

      while (i--)
      {
         const char* szEntity = entityList[i].szEntity;
         CEntity*    pEntity = &_rootEntity;

         while (*szEntity != '\0')
         {
            pEntity = &pEntity->entityMap[(ushort)*szEntity];
            szEntity++;
         }
         pEntity->character = entityList[i].character;
      }
   }
}

QChar HtmlStreamReader::readNextChar()
{
   QChar nextChar;
   auto pos = 0;

   if (_dataSource)
   {
      pos = _dataSource->pos();
      nextChar = _dataSource->readChar();
   }

   if (!nextChar.isNull())
   {
      _buffer += nextChar;
      _positions += pos;
   }

   return nextChar;
}

QChar HtmlStreamReader::translateNextChar()
{
   auto bufPos = _buffer.length();
   auto nextChar = readNextChar();

   while (nextChar == u'&')
   {
      int            startPos = _buffer.length() - 1;
      const CEntity* pEntity = NULL; // Pointer to the current HTML character entity node
      int            entityBase = 0;
      ushort         charVal = 0; // Has to be 0 otherwise number processing fails

      while (true)
      {
         nextChar = readNextChar();
         if (nextChar.isNull())
            break;

         if (nextChar == u';')
            break;

         if (entityBase)
         {
            if (nextChar == u'x')
            {
               if (charVal)
                  break;
               entityBase = 16;
            }
            else
            {
               if (!nextChar.isDigit())
                  break;

               charVal *= entityBase;
               charVal += (ushort)nextChar.digitValue();
            }
         }
         else if (pEntity == nullptr)
         {
            // We are either at the beginning of the recognition process (after '&') or it is a numeric code
            if (nextChar == u'#')
               entityBase = 10; // The base still may be 16
            else
               pEntity = &_rootEntity; // Begin of a named entity
         }

         // Named entity recognition process?
         if (pEntity != nullptr)
         {
            // Try to find the character in the current named entity character node
            auto it = pEntity->entityMap.find(nextChar);

            if (it == pEntity->entityMap.end())
               break; // Invalid character

            // Take the found entity node as current entity node
            pEntity = &it.value();
         }
      }

      if (pEntity)
         charVal = pEntity->character.unicode();

      if (charVal)
      {
         _buffer[startPos] = charVal;

         // Discard processed characters

         startPos++;

         auto skip = _buffer.length() - startPos;

         if (!nextChar.isNull() && nextChar != u';')
            skip--;

         _buffer.remove(startPos, skip);
         _positions.remove(startPos, skip);
      }
   }

   if (bufPos < _buffer.length())
      return _buffer[bufPos];

   return {};
}

void AbstractHtmlEmbeddedResourceManager::extractEmbeddedImages(QString& html)
{
   static const QString imgTag = "<img src=\"";
   static const QString embeddedImgDataTag = "data:image/";

   auto imgPos = html.indexOf(imgTag);
   auto imgCount = 0;

   while (imgPos != -1)
   {
      imgPos += imgTag.length();

      auto end = html.indexOf("\"", imgPos);
      if (end == -1)
         break;
      auto source = html.mid(imgPos, end - imgPos);
      if (source.startsWith(embeddedImgDataTag))
      {
         source.remove(0, embeddedImgDataTag.length());
         const auto typeEndPos = source.indexOf(u';');
         if (typeEndPos != -1)
         {
            auto imgType = source.left(typeEndPos);
            source.remove(0, typeEndPos);
            if (source.startsWith(";base64,"))
            {
               source.remove(0, 8);

               auto imageName = addResource(imgCount++, QByteArray::fromBase64(source.toLatin1()), imgType);

               if (!imageName.isEmpty())
                  html.replace(imgPos, end - imgPos, imageName);
            }
         }
      }

      imgPos = html.indexOf(imgTag, imgPos);
   }
}

void AbstractHtmlEmbeddedResourceManager::embedImages(QString& html)
{
   static const QString imgTag = "<img src=\"";

   auto imgPos = html.indexOf(imgTag);

   while (imgPos != -1)
   {
      imgPos += imgTag.length();

      auto end = html.indexOf("\"", imgPos);
      if (end == -1)
         break;

      const auto imageData = resource(html.mid(imgPos, end - imgPos));

      if (!imageData.first.isEmpty())
         html.replace(imgPos, end - imgPos, QString("data:image/%1;base64,%2").arg(imageData.second).arg(QString::fromLatin1(imageData.first.toBase64())));

      imgPos = html.indexOf(imgTag, imgPos);
   }
}

QStringList AbstractHtmlEmbeddedResourceManager::imageSources(const QString& html)
{
   static const QString imgTag = "<img src=\"";

   QStringList images;
   auto imgPos = html.indexOf(imgTag);

   while (imgPos != -1)
   {
      imgPos += imgTag.length();

      auto end = html.indexOf("\"", imgPos);
      if (end == -1)
         break;

      images.append(html.mid(imgPos, end - imgPos));

      imgPos = html.indexOf(imgTag, imgPos);
   }

   return images;
}

QString StandardHtmlEmbeddedResourceManager::addResource(int index, const QByteArray& data, const QString& imgType)
{
   if (data.isEmpty())
      return QString();

   auto imgName = QString("embedded_img%1.%2").arg(index).arg(imgType);

   _imageList.insert(imgName, data);

   return imgName;
}

QPair<QByteArray, QString> StandardHtmlEmbeddedResourceManager::resource(const QString& name) const
{
   return QPair<QByteArray, QString>(_imageList.value(name), name.section(u'.', -1));
}
