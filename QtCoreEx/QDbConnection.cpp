#include "stdafx.h"
#include "QDbConnection.h"
#include "QDbConnectionData.h"
#include "QRational.h"
#include "QText.h"
#include "QValue.h"
#include "TemplateConversions.h"

QTCOREEX_EXPORT QDbDataType dbTypeFor(const std::type_info& typeInfo)
{
   QDbDataType type = DT_UNKNOWN;

   if (typeInfo == typeid(bool))
      type = DT_BOOL;
   else if (typeInfo == typeid(int))
      type = DT_INT;
   else if (typeInfo == typeid(unsigned int))
      type = DT_UINT;
   else if (typeInfo == typeid(long long int))
      type = DT_LONG;
   else if (typeInfo == typeid(unsigned long long int))
      type = DT_ULONG;
   else if (typeInfo == typeid(double))
      type = DT_DOUBLE;
   else if (typeInfo == typeid(QDecimal))
      type = DT_DECIMAL;
   else if (typeInfo == typeid(QValue))
      type = DT_DECIMAL;
   else if (typeInfo == typeid(QString))
      type = DT_STRING;
   else if (typeInfo == typeid(QText))
      type = DT_STRING;
   else if (typeInfo == typeid(QDate))
      type = DT_DATE;
   else if (typeInfo == typeid(QDateEx))
      type = DT_DATE;
   else if (typeInfo == typeid(QTime))
      type = DT_TIME;
   else if (typeInfo == typeid(QTimeEx))
      type = DT_TIME;
   else if (typeInfo == typeid(QDateTime))
      type = DT_DATETIME;
   else if (typeInfo == typeid(QDateTimeEx))
      type = DT_DATETIME;
   else if (typeInfo == typeid(QRational))
      type = DT_DECIMAL;
   else if (typeInfo == typeid(QByteArray))
      type = DT_BINARY;
   else if (Variant::canConvert(typeInfo, typeid(QByteArray)))
      type = DT_BINARY;

   return type;
}

QTCOREEX_EXPORT QDbDataType dbTypeFromVariant(const Variant& value)
{
   if (value.isNull())
      return DT_UNKNOWN;

   auto dbType = dbTypeFor(value.type());

   if (dbType == DT_UNKNOWN)
   {
      if (value.hasConversionTo<QByteArray>())
         dbType = DT_BINARY;
      else
         dbType = DT_STRING;
   }

   return dbType;
}

QDbConnection::QDbConnection(QDbConnectionData *connection) : _data(connection)
{
}

QDbConnection::QDbConnection(const QDbConnection& other) : _data(other._data)
{
}

QDbConnection::~QDbConnection()
{
}

QDbConnection& QDbConnection::operator=(const QDbConnection& other)
{
   _data = other._data;
   return *this;
}

bool QDbConnection::operator==(const QDbConnection& other) const
{
   return _data == other._data;
}

QDbState::ErrhPtr QDbConnection::errorHandler() const
{
   if (_data)
      return _data->errorHandler();
   return QDbState::ErrhPtr();
}

void QDbConnection::setErrorHandler(QDbState::ErrhPtr errorHandler)
{
   if (_data)
      _data->setErrorHandler(errorHandler);
}

void QDbConnection::setErrorHandler(QDbState::ErrorHandler* errorHandler)
{
   setErrorHandler(QDbState::ErrhPtr(errorHandler));
}

bool QDbConnection::treatEmptyAsNull() const
{
   return _data && _data->treatEmptyAsNull();
}

void QDbConnection::setTreatEmptyAsNull(bool emptyIsNull)
{
   if (_data)
      _data->setTreatEmptyAsNull(emptyIsNull);
}

int QDbConnection::maxLiteralStringLength() const
{
   return _data ? _data->_maxLiteralStringLength : 0;
}

void QDbConnection::setMaxLiteralStringLength(int length)
{
   if (_data && length >= 0)
      _data->_maxLiteralStringLength = length;
}

QString QDbConnection::falseString() const
{
   return _data ? _data->_falseString : QString();
}

QString QDbConnection::trueString() const
{
   return _data ? _data->_trueString : QString();
}

void QDbConnection::setBooleanStrings(const QString& falseString, const QString& trueString)
{
   if (_data)
   {
      _data->_falseString = falseString;
      _data->_trueString = trueString;
   }
}

int QDbConnection::maxRowsPerDmlCall() const
{
   return _data ? _data->_maxRowsPerCall : 0;
}

void QDbConnection::setMaxRowsPerDmlCall(int maxRowsPerDmlCall)
{
   if (_data && maxRowsPerDmlCall > 0)
      _data->_maxRowsPerCall = maxRowsPerDmlCall;
}

bool QDbConnection::isValid() const
{
   return _data && _data->isValid();
}

QString QDbConnection::connectionString() const
{
   if (_data)
      return _data->connectionString();
   return QString();
}

QString QDbConnection::dataSourceName() const
{
   if (_data)
      return _data->dataSourceName();
   return QString();
}

QString QDbConnection::driverName() const
{
   if (_data)
      return _data->driverName();
   return QString();
}

SqlStyle QDbConnection::sqlStyle() const
{
   if (_data)
      return _data->sqlStyle();
   return SqlStyle::SQL92;
}

QDbConnection::PhType QDbConnection::phType() const
{
   if (_data)
      return _data->phType();
   return PHT_All;
}

QDbEnvironment * QDbConnection::environment()
{
   if (_data)
      return _data->environment();
   return nullptr;
}

QDbState QDbConnection::state() const
{
   if (_data)
      return _data->state();
   return QDbState();
}

bool QDbConnection::beginTransaction()
{
   if (_data)
      return _data->beginTransaction();
   return false;
}

bool QDbConnection::rollbackTransaction()
{
   if (_data)
      return _data->rollbackTransaction();
   return false;
}

bool QDbConnection::commitTransaction()
{
   if (_data)
      return _data->commitTransaction();
   return false;
}

bool QDbConnection::abortExecution()
{
   if (_data)
      return _data->abortExecution();
   return false;
}

bool QDbConnection::tableExists(const QString &tableName) const
{
   if (_data)
      return _data->tableExists(tableName);
   return false;
}

QString QDbConnection::escapeForLike(QString text)
{
   return text.replace("%", "[%]")
      .replace("_", "[_]")
      .replace("[", "[[]")
      ;
}

bool QDbConnection::usePlaceholder(QDbDataType dbType, const Variant& value) const
{
   if (_data)
      return _data->usePlaceholder(dbType, value);
   return true;
}

QChar QDbConnection::phIndicator() const
{
   if (_data)
      return _data->phIndicator();
   return QChar();
}

QString QDbConnection::toLiteral(QDbDataType dbType, const Variant &value) const
{
   static QLocaleEx cLocale(QLocale::C);

   if (value.isValueNull())
      return "NULL";

   QString result;

   if (dbType == DT_BOOL)
   {
      if (_data)
      {
         if (!_data->_falseString.isEmpty() && !_data->_trueString.isEmpty())
            result = _data->toStringLiteral(value.toBool(nullptr, cLocale) ? _data->_trueString : _data->_falseString);
         else
            result = _data->toBooleanLiteral(value.toBool(nullptr, cLocale));
      }
      else
      {
         result = value.toBool(nullptr, cLocale) ? "TRUE" : "FALSE";
      }
   }
   else if (dbType == DT_INT)
   {
      if (_data)
         result = _data->toNumericLiteral(value.toInt64(nullptr, cLocale));
      else
         result = QString::number(value.toInt(nullptr, cLocale));
   }
   else if (dbType == DT_UINT)
   {
      if (_data)
         result = _data->toNumericLiteral(value.toInt64(nullptr, cLocale));
      else
         result = QString::number(value.toUInt(nullptr, cLocale));
   }
   else if (dbType == DT_LONG)
   {
      if (_data)
         result = _data->toNumericLiteral(value.toInt64(nullptr, cLocale));
      else
         result = QString::number(value.toInt64(nullptr, cLocale));
   }
   else if (dbType == DT_ULONG)
   {
      if (_data)
         result = _data->toNumericLiteral(value.toInt64(nullptr, cLocale));
      else
         result = QString::number(value.toUInt64(nullptr, cLocale));
   }
   else if (dbType == DT_DOUBLE)
   {
      if (_data)
         result = _data->toNumericLiteral(value.toDouble(nullptr, cLocale));
      else
         doubleToString(value.toDouble(nullptr, cLocale), -16, result);
   }
   else if (dbType == DT_DECIMAL)
   {
      if (_data)
         result = _data->toNumericLiteral(value.toDecimal(nullptr, cLocale));
      else
         result = value.toString(cLocale);
   }
   else if (dbType == DT_STRING || dbType == DT_LARGE_STRING || dbType == DT_NSTRING || dbType == DT_LARGE_NSTRING)
   {
      if (treatEmptyAsNull() && value.toString(cLocale).isEmpty())
         result = "NULL";
      else if (_data)
         result = _data->toStringLiteral(value.toString(cLocale));
      else
         result = QString("'%1'").arg(value.toString(cLocale).replace("'", "''"));
   }
   else if (dbType == DT_DATE)
   {
      if (_data)
         result = _data->toDateLiteral(value.toDateEx(nullptr, cLocale));
      else
         result = QString("DATE '%1'").arg(value.toDateEx(nullptr, cLocale).toString(SQL92_DATE_FORMAT));
   }
   else if (dbType == DT_TIME)
   {
      if (_data)
         result = _data->toTimeLiteral(value.toTimeEx(nullptr, cLocale));
      else
         result = QString("TIME '%1'").arg(value.toTimeEx(nullptr, cLocale).toString(SQL92_TIME_FORMAT));
   }
   else if (dbType == DT_DATETIME)
   {
      if (_data)
         result = _data->toDateTimeLiteral(value.toDateTimeEx(nullptr, cLocale));
      else
         result = QString("TIMESTAMP '%1'").arg(value.toDateTime(nullptr, cLocale).toString(SQL92_DATETIME_FORMAT));
   }
   else if (dbType == DT_BINARY || dbType == DT_LARGE_BINARY)
   {
      if (treatEmptyAsNull() && value.toByteArray(nullptr, cLocale).isEmpty())
         result = "NULL";
      else if (_data)
         result = _data->toBinaryLiteral(value.toByteArray(nullptr, cLocale));
      else
         result = QString("X'%1'").arg(QString::fromLatin1(value.toByteArray(nullptr, cLocale).toHex()));
   }

   return result;
}

bool QDbConnection::isDebugMode() const
{
   if (_data)
      return _data->isDebugMode();
   return false;
}

void QDbConnection::setIsDebugMode(bool debugMode)
{
   if (_data)
      _data->setIsDebugMode(debugMode);
}

QDbSchema::Database QDbConnection::databaseSchema() const
{
   if (_data)
      return _data->databaseSchema();
   return QDbSchema::Database();
}

QDbSchema::Synonym QDbConnection::synonymSchema(const QString &name) const
{
   if (_data)
      return _data->synonymSchema(name);
   return QDbSchema::Synonym();
}

QDbSchema::Table QDbConnection::tableSchema(const QString &name) const
{
   if (_data)
      return _data->tableSchema(name);
   return QDbSchema::Table();
}

QDbSchema::Function QDbConnection::functionSchema(const QString &name) const
{
   if (_data)
      return _data->functionSchema(name);
   return QDbSchema::Function();
}

QDbSchema::Package QDbConnection::packageSchema(const QString &name) const
{
   if (_data)
      return _data->packageSchema(name);
   return QDbSchema::Package();
}

bool QDbConnection::schemaCachingEnabled() const
{
   if (_data)
      return _data->schemaCachingEnabled();
   return false;
}

void QDbConnection::enableSchemaCaching()
{
   if (_data)
      _data->enableSchemaCaching();
}

void QDbConnection::disableSchemaCaching()
{
   if (_data)
      _data->disableSchemaCaching();
}

QDbConnection QDbConnection::defaultConnection()
{
   return QDbConnectionData::_default;
}

void QDbConnection::removeDefaultConnection()
{
   QDbConnectionData::_default = nullptr;
}


QDbTransaction::QDbTransaction(bool begin) : _active(false)
{
   if (begin)
      this->begin();
}

QDbTransaction::QDbTransaction(QDbConnection connection, bool begin) : _connection(connection), _active(false)
{
   if (begin)
      this->begin();
}

QDbTransaction::~QDbTransaction()
{
   rollback();
}

void QDbTransaction::begin(QDbConnection connection)
{
   if (connection.isValid())
      _connection = connection;

   if (!_connection.isValid())
      _connection = QDbConnection::defaultConnection();

   if (!_active && _connection.isValid())
   {
      if (_connection.beginTransaction())
         _active = true;
   }
}

void QDbTransaction::commit()
{
   if (_active)
   {
      _active = false;
      _connection.commitTransaction();
   }
}

void QDbTransaction::rollback()
{
   if (_active)
   {
      _active = false;
      _connection.rollbackTransaction();
   }
}
