#include "stdafx.h"
#include "IVariable.h"
#include "ExpressionBase.h"
#include "Tokenizer.h"
#include "CPPExpression.h"

static char hexToDigit(int d)
{
   if (d < 10)
      return d + '0';
   return d - 10 + 'A';
}

QByteArray toPercentEncoding(const QString& text)
{
   QByteArray result;

   for (auto&& c : text)
   {
      auto u = c.unicode();

      if (u >= u'a' && u <= u'z' || u >= u'A' && u <= u'Z' || u >= u'0' && u <= u'9' || u == u'-' || u == u'.' || u == u'_')
         result.append(u);
      else if (u < 256u)
      {
         result.append('%');
         result.append(hexToDigit(u / 16));
         result.append(hexToDigit(u % 16));
      }
      else
      {
         result.append('%');
         result.append('%');
         result.append(hexToDigit(u >> 12));
         u &= 0xFFF;
         result.append(hexToDigit(u >> 8));
         u &= 0xFF;
         result.append(hexToDigit(u >> 4));
         u &= 0xF;
         result.append(hexToDigit(u));
      }
   }

   return result;
}

QString fromPercentEncoding(const QByteArray& data)
{
   QString result;
   int hexLength = 0;
   unsigned short u = 0;

   for (auto&& c : data)
   {
      if (c == '%')
      {
         hexLength += 2;
      }
      else if (hexLength)
      {
         u <<= 4;
         if (c >= '0' && c <= '9')
            u += c - '0';
         else if (c >= 'A' && c <= 'F')
            u += c - 'A' + 10;
         else if (c >= 'a' && c <= 'f')
            u += c - 'a' + 10;
         hexLength--;
         if (hexLength == 0)
         {
            result.append(u);
            u = 0;
         }
      }
      else
      {
         result.append(c);
      }
   }

   return result;
}

int IVariable::serializerVersion = 1;

bool IVariable::isItemInsertable(const VarPtr& item)
{
   if (item.data() == this)
      return false;

   for (auto i = item->itemCount(); i--; )
   {
      if (!isItemInsertable(item->getItem(i)))
         return false;
   }

   return true;
}

QByteArray IVariable::serializeToAscii(int indent) const
{
   QByteArray data;

   switch (basicType())
   {
   case BasicType::Collection:
      {
         const auto itemList = items();
         const auto nameList = itemNames();

         for (auto i = 0; i < itemList.count(); ++i)
         {
            data += '\n';
            data += QByteArray(indent, ' ');
            if (i < nameList.count())
               data += toPercentEncoding(nameList.at(i));
            data += ':';
            data += itemList.at(i)->serializeToAscii(indent + 2);
         }
      }
      break;
   case BasicType::Value:
      data = toPercentEncoding(typeName());
      data += '=';
      data += value().serialized();
      break;
   case BasicType::Function:
      data = toPercentEncoding(typeName());
      data += '!';
      if (function())
         data += toPercentEncoding(function()->toString());
      break;
   }

   return data;
}

int IVariable::deserializeFromAscii(const QByteArray& data, Tokenizer* tokenizer, int pos, int versionNumber, int indent)
{
   VarPtr lastItem;

   while (pos < data.length())
   {
      int spaces = 0;

      while (pos + spaces < data.length() && data.at(pos + spaces) == ' ')
         spaces++;

      if (pos + spaces == data.length())
         break;

      if (data.at(pos + spaces) == '\n')
      {
         // Empty line
         pos++;
         continue;
      }

      if (spaces < indent)
         break;

      if (spaces > indent)
      {
         pos = lastItem->deserializeFromAscii(data, tokenizer, pos, versionNumber, spaces);
         if (pos < 0)
            return -1; // ERROR
      }
      else
      {
         pos += spaces;

         auto start = pos;

         while (pos < data.length() && data.at(pos) != ':' && data.at(pos) != '\n')
            pos++;

         if (pos == data.length() || data.at(pos) != ':')
            return -1; // ERROR

         auto name = fromPercentEncoding(data.mid(start, pos - start));

         pos++; // Skip ':'
         start = pos;

         while (pos < data.length() && data.at(pos) != '=' && data.at(pos) != '!' && data.at(pos) != '\n')
            pos++;

         auto type = fromPercentEncoding(data.mid(start, pos - start));

         if (pos == data.length() || data.at(pos) == '\n')
         {
            lastItem = newCollection(type);
         }
         else if (data.at(pos) == '=')
         {
            pos++;
            start = pos;
            while (pos < data.length() && data.at(pos) != '\n')
               pos++;
            Variant value;
            value.deserialize(data.mid(start, pos - start));
            lastItem = newFromValue(value, type);
         }
         else if (data.at(pos) == '!')
         {
            Q_ASSERT(type == "Function");
            pos++;
            start = pos;
            while (pos < data.length() && data.at(pos) != '\n')
               pos++;
            if (tokenizer)
            {
               tokenizer->setText(fromPercentEncoding(data.mid(start, pos - start)));
               lastItem = newFromFunction(createCppExpressionTree(tokenizer->tokenList()));
            }
         }
         else
         {
            return -1; // ERROR
         }

         setItem(name, lastItem);

         if (pos < data.length())
            pos++;
      }
   }

   return pos;
}
