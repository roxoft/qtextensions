#include "TestCases.h"
#include "QODBCConnection.h"
#include <QDbCommand.h>
#include <QDir>
#include <QSettingsFile.h>

#ifdef Q_PROCESSOR_X86_32
static QDbConnection createSybaseConnection(int numericLimit = 0);
#endif

void TestCases::stmtParsingTests()
{
#ifdef Q_PROCESSOR_X86_32
   auto connection = createSybaseConnection();

   QDbCommand command(connection);

   command.setStatement("update cr_oci_test set test7 = :ph02, test6 = ?, test4 = ?, test5 = :ph04");
   command.parsePlaceholders();
   T_EQUALS(command.preparedStatement(), "update cr_oci_test set test7 = ?, test6 = ?, test4 = ?, test5 = ?");

   command.setStatement("  update cr_oci_test set test7 = :2, test6 = :ph01, test4 = :ph03, test5 = :");
   command.parsePlaceholders();
   T_EQUALS(command.preparedStatement(), "update cr_oci_test set test7 = ?, test6 = ?, test4 = ?, test5 = :");

   command.setStatement("select gpd_inhalt \"qpd:inhalt\", gpd_bemerkung from ihs_gp_dokumente where gpd_id = :gpd_id   ");
   command.parsePlaceholders();
   T_EQUALS(command.preparedStatement(), "select gpd_inhalt \"qpd:inhalt\", gpd_bemerkung from ihs_gp_dokumente where gpd_id = ?");
#else
   addWarning(tr("Test only for 32 bit"));
#endif
}

void TestCases::primaryTestSybase()
{
#ifdef Q_PROCESSOR_X86_32
   _primaryTest(createSybaseConnection());
#else
   addWarning(tr("Test only for 32 bit"));
#endif
}

void TestCases::primaryTestSqlServer()
{
   _primaryTest(createSqlServerConnection());
}

void TestCases::lobTestSybase()
{
#ifdef Q_PROCESSOR_X86_32
   _lobTest(createSybaseConnection());
#else
   addWarning(tr("Test only for 32 bit"));
#endif
}

void TestCases::lobTestSqlServer()
{
   _lobTest(createSqlServerConnection());
}

#ifdef Q_PROCESSOR_X86_32
static void getProcedureFromFile(const QString& fileName, QString& dropSql, QString& createSql)
{
   QFile procFile(fileName);

   T_ASSERT(procFile.open(QIODevice::ReadOnly | QIODevice::Text));

   auto sql = QString::fromLatin1(procFile.readAll());
   auto stmtEnd = sql.indexOf(u';');

   dropSql = sql.left(stmtEnd);
   createSql = sql.mid(stmtEnd + 1);
}
#endif

void TestCases::testStoredProcedure()
{
#ifdef Q_PROCESSOR_X86_32
   auto connection = createSybaseConnection();

   QDbCommand query;

#if 1
   T_ASSERT(!dataDir().isEmpty());

   QString dropSql;
   QString createSql;

   getProcedureFromFile(QDir(dataDir()).filePath(QString("SybaseErmittleKurs.sql")), dropSql, createSql);

   T_ASSERT(!createSql.isEmpty() && !dropSql.isEmpty());

   query.setStatement(createSql);
   if (!query.exec())
      addError(query.state().message());

   QDbSchema::Function funcInfo = connection.functionSchema(QString("ermittle_kurs"));

   QDbStoredProc releaseProc(QString("ermittle_kurs"));

   releaseProc.setPlaceholderValue(QString("@bankID"), 27);
   releaseProc.setPlaceholderValue(QString("@skontro"), 1);
   releaseProc.setPlaceholderValue(QString("@check"), false);

   if (!releaseProc.exec())
      addError(releaseProc.state().message(-1));

   QDecimal kurs = releaseProc.placeholderValue(QString("@kurs")).toDecimal();

   T_EQUALS_EX(kurs, QDecimal(12.12345678));

   query.setStatement(dropSql);
   if (!query.exec())
      addError(query.state().message());

   getProcedureFromFile(QDir(dataDir()).filePath(QString("SybaseVBArten.sql")), dropSql, createSql);

   T_ASSERT(!createSql.isEmpty() && !dropSql.isEmpty());

   query.setStatement(createSql);
   if (!query.exec())
      addError(query.state().message());

   funcInfo = connection.functionSchema(QString("ermittleVerbuchungsarten"));

   releaseProc.setStatement(QString("CALL ermittleVerbuchungsarten (27,456)"));

   if (!releaseProc.exec())
      addError(releaseProc.state().message(-1));

   auto result = releaseProc.result();

   T_IS_TRUE(result.next());

   T_EQUALS(result[0].toString(), eutf8("Test°Test2°Test3"));

   T_IS_TRUE(!result.next()); // Close the cursor otherwise the procedure can't be dropped

   query.setStatement(dropSql);
   if (!query.exec())
      addError(query.state().message());
#endif
#else
   addWarning(tr("Test only for 32 bit"));
#endif
}

void TestCases::numericTestSybase()
{
#ifdef Q_PROCESSOR_X86_32
   _numericLimitTest(createSybaseConnection(), false);
#else
   addWarning(tr("Sybase test only for 32 bit"));
#endif
}

void TestCases::numericTestSqlServer()
{
   _numericLimitTest(createSqlServerConnection(), false);
}

void TestCases::numericLimitTestSybase()
{
#ifdef Q_PROCESSOR_X86_32
   _numericLimitTest(createSybaseConnection(28), true);
#else
   addWarning(tr("Sybase test only for 32 bit"));
#endif
}

void TestCases::numericLimitTestSqlServer()
{
   _numericLimitTest(createSqlServerConnection(28), true);
}

void TestCases::_primaryTest(const QDbConnection& connection)
{
   if (!connection.isValid())
      return;

   QDbCommand command(connection);

   // CREATE_TEST_TABLE
   command.setStatement("create table cr_test ( id int not null, name varchar(100) not null, numeric_value numeric(10), char_bool_value char(1), bit_bool_value bit, date_value date, time_string varchar(8), time_value time, date_time datetime, primary key (id) )");
   if (!command.exec())
      addError(command.state().message());

   auto dbSchema = connection.databaseSchema();

   T_ASSERT(dbSchema.tables.count());

   //auto tableSchema = connection.tableSchema("cr_test");

   //T_EQUALS(tableSchema.name, "cr_test");
   //T_EQUALS(tableSchema.columns.count(), 9);

   // Fill test table

   command.setStatement("INSERT INTO CR_TEST (ID, NAME, NUMERIC_VALUE, CHAR_BOOL_VALUE, BIT_BOOL_VALUE, DATE_VALUE, TIME_STRING, TIME_VALUE, DATE_TIME) VALUES (:ph00, :ph01, :ph02, :ph03, :ph04, :ph05, :ph06, :ph07, :ph08)");

   command.setPlaceholderValue("ph00", 1);
   command.setPlaceholderValue("ph01", QString("Müller"));
   command.setPlaceholderValue("ph02", INT_MAX);
   command.setPlaceholderValue("ph03", true);
   command.setPlaceholderValue("ph04", true);
   command.setPlaceholderValue("ph05", QDate(2014, 5, 12));
   command.setPlaceholderValue("ph06", QTime(12, 30, 0));
   command.setPlaceholderValue("ph07", QTime(12, 30, 0));
   command.setPlaceholderValue("ph08", QDateTimeEx(2021, 2, 23, 12, 30, 00));
   T_ERROR_IF(!command.exec(), command.state().message());

   // Reset command
   command.setStatement("INSERT INTO CR_TEST (ID, NAME, NUMERIC_VALUE, CHAR_BOOL_VALUE, BIT_BOOL_VALUE, DATE_VALUE, TIME_STRING, TIME_VALUE, DATE_TIME) VALUES (:ph00, :ph01, :ph02, :ph03, :ph04, :ph05, :ph06, :ph07, :ph08)");

   command.setPlaceholderValue("ph00", 2);
   command.setPlaceholderValue("ph01", QString("Ralph"));
   command.setPlaceholderValue("ph02", 987654);
   command.setPlaceholderValue("ph03", false);
   command.setPlaceholderValue("ph04", false);
   command.setPlaceholderValue("ph05", QDateEx(2016, 5, 2));
   command.setPlaceholderValue("ph06", QString("12:50:00"));
   command.setPlaceholderValue("ph07", QString("12:50:00"));
   command.setPlaceholderValue("ph08", QDateTimeEx(2021, 2, 23, 12, 30, 1, 530));
   T_ERROR_IF(!command.exec(), command.state().message());

   // Repeat insert
   command.setPlaceholderValue("ph00", 3);
   command.setPlaceholderValue("ph01", QString("Hubert"));
   command.setPlaceholderValue("ph02", 654321);
   command.setPlaceholderValue("ph03", 0);
   command.setPlaceholderValue("ph04", 0);
   command.setPlaceholderValue("ph05", QDateEx(2018, 6, 15));
   command.setPlaceholderValue("ph06", QString("08:12:00"));
   command.setPlaceholderValue("ph07", QString("08:12:00"));
   command.setPlaceholderValue("ph08", QDateTimeEx(2020, 5, 25, 1, 5, 1, 990));
   T_ERROR_IF(!command.exec(), command.state().message());

   command.setPlaceholderValue("ph00", 4);
   command.setPlaceholderValue("ph01", QString("John"));
   command.setPlaceholderValue("ph02", -1);
   command.setPlaceholderValue("ph03", 1);
   command.setPlaceholderValue("ph04", 1);
   command.setPlaceholderValue("ph05", QDateTimeEx(2018, 6, 30));
   command.setPlaceholderValue("ph06", QTimeEx(18, 45, 0));
   command.setPlaceholderValue("ph07", QTimeEx(18, 45, 0));
   command.setPlaceholderValue("ph08", QDateTime(QDate(2020, 12, 12), QTime(12, 12, 12)));
   T_ERROR_IF(!command.exec(), command.state().message());

   // Insert with literals
   command.setStatement(QString("INSERT INTO CR_TEST (ID, NAME, NUMERIC_VALUE, CHAR_BOOL_VALUE, BIT_BOOL_VALUE, DATE_VALUE, TIME_STRING, TIME_VALUE, DATE_TIME) VALUES (%1, %2, %3, %4, %5, %6, %7, %8, %9)")
      .arg(connection.toLiteral(DT_INT, 6))
      .arg(connection.toLiteral(DT_STRING, "\\Path\\Name\nLine\\\nNew \\ä\\nö\\rü\\tß line\\"))
      .arg(connection.toLiteral(DT_LONG, 1234567890))
      .arg(connection.toLiteral(DT_STRING, "N"))
      .arg(connection.toLiteral(DT_BOOL, true))
      .arg(connection.toLiteral(DT_DATE, QDateEx(2020, 1, 20)))
      .arg(connection.toLiteral(DT_STRING, "12:55:44"))
      .arg(connection.toLiteral(DT_TIME, QTimeEx(15, 32, 0)))
      .arg(connection.toLiteral(DT_STRING, "2021-03-05 07:23:55.34"))
   );
   T_ERROR_IF(!command.exec(), command.state().message());

   command.setStatement(QString("INSERT INTO CR_TEST (ID, NAME, NUMERIC_VALUE, CHAR_BOOL_VALUE, BIT_BOOL_VALUE, DATE_VALUE, TIME_STRING, TIME_VALUE, DATE_TIME) VALUES (%1, %2, %3, %4, %5, %6, %7, %8, %9)")
      .arg(connection.toLiteral(DT_INT, 7))
      .arg(connection.toLiteral(DT_STRING, "\\Path\\Name\r\nLine\\\r\nNew \\ä\\nö\\rü\\tß line\\"))
      .arg(connection.toLiteral(DT_LONG, 1234567890))
      .arg(connection.toLiteral(DT_STRING, "N"))
      .arg(connection.toLiteral(DT_BOOL, true))
      .arg(connection.toLiteral(DT_DATE, QDateEx(2020, 1, 20)))
      .arg(connection.toLiteral(DT_STRING, "12:55:44"))
      .arg(connection.toLiteral(DT_TIME, QTimeEx(15, 32, 0)))
      .arg(connection.toLiteral(DT_DATETIME, QDateTimeEx(2020, 12, 24, 0, 0, 0)))
   );
   T_ERROR_IF(!command.exec(), command.state().message());

   // Update test table
   command.setStatement("UPDATE CR_TEST SET ID = :ph00, NAME = :ph01, NUMERIC_VALUE = :ph02, DATE_VALUE = :ph03, TIME_STRING = :ph04, TIME_VALUE = :ph05, DATE_TIME = :ph06 WHERE ID = :ph07");
   command.setPlaceholderValue("ph00", 5);
   command.setPlaceholderValue("ph01", QString("Horst"));
   command.setPlaceholderValue("ph02", 123456);
   command.setPlaceholderValue("ph03", QDate(2016, 6, 2));
   command.setPlaceholderValue("ph04", QString("12:00:15"));
   command.setPlaceholderValue("ph05", (const QDateTimeEx&)QTimeEx(12, 0, 15));
   command.setPlaceholderValue("ph06", QDateTimeEx(2021, 2, 22, 12, 22, 00));
   command.setPlaceholderValue("ph07", 1);
   T_ERROR_IF(!command.exec(), command.state().message());
   T_EQUALS(command.affectedRows(), 1);

   // Read test table
   command.setStatement("SELECT name, char_bool_value, bit_bool_value, date_value, time_string, time_value, DATE_TIME FROM cr_test ORDER BY id");
   T_ERROR_IF(!command.exec(), command.state().message());
   auto result = command.result();

   T_IS_TRUE(result.next());

   T_EQUALS("Ralph", result["name"].toString());
   T_EQUALS_EX(result["char_bool_value"].toBool(), false);
   T_EQUALS_EX(result["bit_bool_value"], false);
   T_EQUALS_EX(QDate(2016, 5, 2), result["date_value"].toDateEx(nullptr, QLocale::C));
   T_EQUALS(result["time_string"].toString(), "12:50:00");
   T_EQUALS_EX(result["time_value"].toTimeEx(nullptr, QLocale::C), QTimeEx(12, 50, 0));
   T_EQUALS(QDateTimeEx(2021, 2, 23, 12, 30, 1, 530).toISOString(), result["DATE_TIME"].toDateTimeEx().toISOString());

   T_ERROR_IF(result.state().type() == QDbState::Error, result.state().message());

   T_IS_TRUE(result.next());

   T_EQUALS("Hubert", result["name"].toString());
   T_EQUALS_EX(result["char_bool_value"].toBool(), false);
   T_EQUALS_EX(result["bit_bool_value"], false);
   T_EQUALS_EX(QDate(2018, 6, 15), result["date_value"].toDate(nullptr, QLocale::C));
   T_EQUALS(result["time_string"].toString(), "08:12:00");
   T_EQUALS_EX(result["time_value"].toTimeEx(nullptr, QLocale::C), QTimeEx(8, 12, 0));
   T_EQUALS_EX(QDateTimeEx(2020, 5, 25, 1, 5, 1, 990), result["DATE_TIME"].toDateTimeEx());

   T_ERROR_IF(result.state().type() == QDbState::Error, result.state().message());

   T_IS_TRUE(result.next());

   T_EQUALS("John", result["name"].toString());
   T_EQUALS_EX(result["char_bool_value"].toBool(), true);
   T_EQUALS_EX(result["bit_bool_value"], true);
   T_EQUALS_EX(QDate(2018, 6, 30), result["date_value"].toDate(nullptr, QLocale::C));
   T_EQUALS(result["time_string"].toString(), "18:45:00");
   T_EQUALS_EX(result["time_value"].toTimeEx(nullptr, QLocale::C), QTimeEx(18, 45, 0));
   T_EQUALS_EX(QDateTimeEx(2020, 12, 12, 12, 12, 12, 0), result["DATE_TIME"].toDateTimeEx());

   T_ERROR_IF(result.state().type() == QDbState::Error, result.state().message());

   T_IS_TRUE(result.next());

   T_EQUALS("Horst", result["name"].toString());
   T_EQUALS_EX(result["char_bool_value"].toBool(), true);
   T_EQUALS_EX(result["bit_bool_value"], true);
   T_EQUALS_EX(QDate(2016, 6, 2), result["date_value"].toDate(nullptr, QLocale::C));
   T_EQUALS(result["time_string"].toString(), "12:00:15");
   T_EQUALS_EX(result["time_value"].toTimeEx(nullptr, QLocale::C), QTimeEx(12, 0, 15));
   T_EQUALS_EX(QDateTimeEx(2021, 2, 22, 12, 22, 0, 0), result["DATE_TIME"].toDateTimeEx());

   T_ERROR_IF(result.state().type() == QDbState::Error, result.state().message());

   T_IS_TRUE(result.next());

   T_EQUALS(result["name"].toString().replace("\r", "\\r").replace("\n", "\\n"), "\\Path\\Name\\nLine\\\\nNew \\ä\\nö\\rü\\tß line\\");
   T_EQUALS(result["char_bool_value"].toString(), "N");
   T_EQUALS_EX(result["bit_bool_value"], true);
   T_EQUALS_EX(QDate(2020, 1, 20), result["date_value"].toDate(nullptr, QLocale::C));
   T_EQUALS(result["time_string"].toString(), "12:55:44");
   T_EQUALS_EX(result["time_value"].toTimeEx(nullptr, QLocale::C), QTimeEx(15, 32, 0));
   T_EQUALS_EX(QDateTimeEx(2021, 5, 3, 7, 23, 55, 340), result["DATE_TIME"].toDateTimeEx());

   T_IS_TRUE(result.next());

   T_EQUALS(result["name"].toString().replace("\r", "\\r").replace("\n", "\\n"), "\\Path\\Name\\r\\nLine\\\\r\\nNew \\ä\\nö\\rü\\tß line\\");
   T_EQUALS(result["char_bool_value"].toString(), "N");
   T_EQUALS_EX(result["bit_bool_value"], true);
   T_EQUALS_EX(QDate(2020, 1, 20), result["date_value"].toDate(nullptr, QLocale::C));
   T_EQUALS(result["time_string"].toString(), "12:55:44");
   T_EQUALS_EX(result["time_value"].toTimeEx(nullptr, QLocale::C), QTimeEx(15, 32, 0));
   T_EQUALS_EX(QDateTimeEx(2020, 12, 24, 0, 0, 0, 0), result["DATE_TIME"].toDateTimeEx());

   T_ERROR_IF(result.state().type() == QDbState::Error, result.state().message());

   // DESTROY_TEST_TABLE
   command.setStatement("drop table cr_test");
   T_ERROR_IF(!command.exec(), command.state().message());
}

void TestCases::_lobTest(const QDbConnection& connection)
{
   if (!connection.isValid())
      return;

   QDbCommand command(connection);

   // CREATE_TEST_TABLE
   command.setStatement("create table cr_test ( id integer not null, numeric_value numeric(10,2), large_string text, large_data image )");
   if (!command.exec())
      addError(command.state().message());

   // Fill test table

   command.setStatement("INSERT INTO CR_TEST (ID, numeric_value, large_string, large_data) VALUES (:ph01, :ph02, :ph03, :ph04)");

   command.setPlaceholderValue("ph01", 1);
   command.setPlaceholderValue("ph02", 7000.22);
   command.setPlaceholderValue("ph03", QString(10000, u'x'));
   command.setPlaceholderValue("ph04", QByteArray(10000, 'x'));
   T_ERROR_IF(!command.exec(), command.state().message());

   command.setPlaceholderValue("ph01", 2);
   command.setPlaceholderValue("ph02", 12345678.91);
   command.setPlaceholderValue("ph03", QString(20000, u'y'));
   command.setPlaceholderValue("ph04", QByteArray(20000, 'y'));
   T_ERROR_IF(!command.exec(), command.state().message());

   // Update test table

   command.setStatement("UPDATE CR_TEST SET ID = :ph01, numeric_value = :ph02, large_string = :ph03, large_data = :ph04 WHERE ID = :ph05");
   command.setPlaceholderValue("ph01", 3);
   command.setPlaceholderValue("ph02", 9876.54);
   command.setPlaceholderValue("ph03", QString(4000, u'u'));
   command.setPlaceholderValue("ph04", QByteArray(4000, 'u'));
   command.setPlaceholderValue("ph05", 1);
   T_ERROR_IF(!command.exec(), command.state().message());
   T_EQUALS(command.affectedRows(), 1);

   // Read test table

   command.setStatement("SELECT id, numeric_value, large_string, large_data FROM cr_test ORDER BY id");
   T_ERROR_IF(!command.exec(), command.state().message());
   auto result = command.result();

   T_IS_TRUE(result.next());

   T_EQUALS(result["id"].toInt(), 2);
   T_EQUALS(result["numeric_value"].toString(QLocale::C), "12345678.91");
   T_EQUALS(result["large_string"].toString().count(u'y'), 20000);
   T_EQUALS(result["large_data"].toByteArray().count('y'), 20000);

   T_IS_TRUE(result.next());

   T_EQUALS(result["id"].toInt(), 3);
   T_EQUALS(result["numeric_value"].toString(QLocale::C), "9876.54");
   T_EQUALS(result["large_string"].toString().count(u'u'), 4000);
   T_EQUALS(result["large_data"].toByteArray().count('u'), 4000);

   T_ERROR_IF(result.state().type() == QDbState::Error, result.state().message());

   T_IS_TRUE(!result.next());

   // DESTROY_TEST_TABLE
   command.setStatement("drop table cr_test");
   T_ERROR_IF(!command.exec(), command.state().message());
}

void TestCases::_numericLimitTest(const QDbConnection &connection, bool isLimited)
{
   if (!connection.isValid())
      return;

   QDbCommand command(connection);

   // Create test table

   command.setStatement("create table cr_test ( id int not null, numeric_value numeric(32, 16), primary key (id) )");
   if (!command.exec())
      addError(command.state().message());

   // Fill test table

   command.setStatement("INSERT INTO CR_TEST (ID, NUMERIC_VALUE) VALUES (:ph00, :ph01)");

   command.setPlaceholderValue("ph00", 1);
   command.setPlaceholderValue("ph01", QDecimal(42.5));
   T_ERROR_IF(!command.exec(), command.state().message());

   command.setPlaceholderValue("ph00", 2);
   command.setPlaceholderValue("ph01", QDecimal("123456789012.1234567890123456"));
   T_ERROR_IF(!command.exec(), command.state().message());

   if (!isLimited)
   {
      command.setPlaceholderValue("ph00", 3);
      command.setPlaceholderValue("ph01", QDecimal("1234567890123456.1234567890123456"));
      if (!command.exec())
      {
         addWarning(QString("Could not insert QDecimal(\"1234567890123456.1234567890123456\"):\n%1").arg(command.state().message()));
      }
   }

   // Read test table

   command.setStatement("SELECT numeric_value FROM cr_test ORDER BY id");
   T_ERROR_IF(!command.exec(), command.state().message());
   auto result = command.result();

   T_IS_TRUE(result.next());
   if (result["numeric_value"].toDecimal() == 0)
   {
      addWarning("Numeric precision of 32 not supported");
   }
   else
   {
      T_EQUALS_EX(result["numeric_value"].toDecimal(), 42.5);
      T_ERROR_IF(result.state().type() == QDbState::Error, result.state().message());

      T_IS_TRUE(result.next());
      T_EQUALS_EX(result["numeric_value"].toDecimal(), QDecimal("123456789012.1234567890123456"));
      T_ERROR_IF(result.state().type() == QDbState::Error, result.state().message());

      if (!isLimited)
      {
         if (result.next())
         {
            T_EQUALS_EX(result["numeric_value"].toDecimal(), QDecimal("1234567890123456.1234567890123456"));
            T_ERROR_IF(result.state().type() == QDbState::Error, result.state().message());
         }
         else
         {
            addWarning("Recordset with ID 3 not present");
         }
      }
   }

   // Destroy test table
   command.setStatement("drop table cr_test");
   T_ERROR_IF(!command.exec(), command.state().message());
}

#ifdef Q_PROCESSOR_X86_32
static QDbConnection createSybaseConnection(int numericLimit)
{
   QString connectionString;
   createSybaseConnectionString(connectionString, "dba", "sql", "SybaseTest");

   auto odbcConnection = new QODBCConnection(connectionString);

   //odbcConnection->setServerCharacterEncoding(QTextCodec::codecForMib(2252));

   // Taken from the Rogue Wave implementation it may be good to supply small values in the sql command.
   // ASA5: Some expressions (notably the + operator) cause an error if there is a placeholder on the right side!
   odbcConnection->_maxLiteralStringLength = 1024;

   if (odbcConnection->state().type() == QDbState::Error)
      throw TestException(odbcConnection->state().message());

   if (numericLimit > 0)
      odbcConnection->setNumericPrecisionLimit(numericLimit);

   return odbcConnection;
}
#endif

QDbConnection TestCases::createSqlServerConnection(int numericLimit)
{
   QFile credentialsFile(QDir(dataDir()).filePath("SqlServerCredentials.ini"));
   const QSettingsFile settings(&credentialsFile);

   const auto dbDriver = settings.value("Driver").toString();
   const auto dbServer = settings.value("Host").toString();
   const auto dbName = settings.value("Database").toString();
   const auto dbUser = settings.value("User").toString();
   const auto dbPassword = settings.value("Password").toString();

   if (dbServer.isEmpty())
   {
      addWarning(tr("Missing SqlServer database credentials: Test skipped"));
      return QDbConnection();
   }

   QString sqlServerConnectionString;
   createSqlServerConnectionString(sqlServerConnectionString, dbDriver, dbServer, dbUser, dbPassword, dbName);

   auto sqlServerConnection = new QODBCConnection(sqlServerConnectionString);

   if (sqlServerConnection->state().type() == QDbState::Error)
      throw TestException(sqlServerConnection->state().message());

   if (numericLimit > 0)
      sqlServerConnection->setNumericPrecisionLimit(numericLimit);

   return sqlServerConnection;
}

BEGIN_TEST_SUITES
   TEST_SUITE(TestCases);
END_TEST_SUITES
