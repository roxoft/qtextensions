<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE">
<context>
    <name>QODBCCommandEngine</name>
    <message>
        <location filename="QODBCCommandEngine.cpp" line="88"/>
        <source>Row error</source>
        <translation>Datensatzfehler</translation>
    </message>
    <message>
        <location filename="QODBCCommandEngine.cpp" line="110"/>
        <source>Invalid position of row counter</source>
        <translation>Ungültige Position für den Datensatzzähler</translation>
    </message>
    <message>
        <location filename="QODBCCommandEngine.cpp" line="112"/>
        <source>Index %1 not in result set</source>
        <translation>Der Index %1 ist nicht im Ergebnis</translation>
    </message>
    <message>
        <location filename="QODBCCommandEngine.cpp" line="132"/>
        <source>Column name %1 not found in result set</source>
        <translation>Der Spaltenname %1 ist nicht im Ergebnis</translation>
    </message>
    <message>
        <location filename="QODBCCommandEngine.cpp" line="913"/>
        <source>The data type of the placeholder at position %1 cannot be set after execution</source>
        <translation>Datentyp für den Platzhalter an Position %1 darf nach der Ausführung nicht gesetzt werden</translation>
    </message>
    <message>
        <location filename="QODBCCommandEngine.cpp" line="1232"/>
        <source>Placeholder at position %1 is not defined</source>
        <translation>Der Platzhalter an Position %1 ist nicht definiert</translation>
    </message>
</context>
<context>
    <name>QODBCConnection</name>
    <message>
        <location filename="QODBCConnection.cpp" line="768"/>
        <source>Unknown SQL data type</source>
        <translation>Unbekannter SQL Datentyp</translation>
    </message>
</context>
<context>
    <name>TestCases</name>
    <message>
        <location filename="TestCases.cpp" line="30"/>
        <location filename="TestCases.cpp" line="39"/>
        <location filename="TestCases.cpp" line="53"/>
        <location filename="TestCases.cpp" line="145"/>
        <source>Test only for 32 bit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="TestCases.cpp" line="154"/>
        <location filename="TestCases.cpp" line="168"/>
        <source>Sybase test only for 32 bit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="TestCases.cpp" line="549"/>
        <source>Missing SqlServer database credentials: Test skipped</source>
        <translation>SqlServer Zugangsdaten fehlen: Tests werden ausgelassen</translation>
    </message>
</context>
</TS>
