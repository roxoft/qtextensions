#ifndef TESTCASES_H
#define TESTCASES_H

#include <QLocaleEx.h> // Must be included before TestFramework to be able to resolve toQString(bool)
#include <TestFramework>
#include <QDbConnection.h>

// Prerequisite for Sybase tests:
//   ODBC-Entry in System-DSN named SybaseTest with the databe SybaseTest.db in TestData.
class TestCases : public TestFramework
{
   Q_OBJECT

public:
   TestCases(QObject *parent = 0) : TestFramework(parent) {}
   ~TestCases() override = default;

public slots:
   void stmtParsingTests();
   void primaryTestSybase();
   void primaryTestSqlServer();
   void lobTestSybase();
   void lobTestSqlServer();
   void testStoredProcedure();
   void numericTestSybase();
   void numericTestSqlServer();
   void numericLimitTestSybase();
   void numericLimitTestSqlServer();

private:
   void _primaryTest(const QDbConnection& connection);
   void _lobTest(const QDbConnection& connection);
   void _numericLimitTest(const QDbConnection& connection, bool isLimited);

   QDbConnection createSqlServerConnection(int numericLimit = 0);
};

#endif // TESTCASES_H
