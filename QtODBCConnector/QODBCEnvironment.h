#ifndef QODBCENVIRONMENT_H
#define QODBCENVIRONMENT_H

#include <QDbEnvironment>
#ifdef Q_OS_WIN
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#endif
#include <sql.h>
#include <sqlext.h>

QDbState checkerr(SQLRETURN status, SQLHANDLE handle, SQLSMALLINT type, bool throwOnError);

class QODBCEnvironment : public QDbEnvironment
{
public:
   QODBCEnvironment();
   virtual ~QODBCEnvironment();

   SQLHENV &env() { return _env; }

public:
   static QODBCEnvironment *theEnvironment;
   static int              environmentRefCount;

   static void attach();
   static void detach();

private:
   SQLHENV _env;
};

#endif // QODBCENVIRONMENT_H
