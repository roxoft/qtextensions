#ifndef QODBCCOMMANDENGINE_H
#define QODBCCOMMANDENGINE_H

#include <QDbCommandEngine>
#include "QODBCConnection.h"
#include "QODBCEnvironment.h"
#include <QVector>

// Implementation of QDbCommandEngine for ODBC
//
// Remark for ASA5: ODBC is somehow converting all strings to OEM (IBM 850) except for the bound strings. Therefore no strings should be supplied in the sql command.
class QODBCCommandEngine : public QDbCommandEngine
{
   Q_DISABLE_COPY(QODBCCommandEngine)
   Q_DECLARE_TR_FUNCTIONS(QODBCCommandEngine)

public:
   QODBCCommandEngine(QODBCConnection* connection, SQLHSTMT hstmt);
   virtual ~QODBCCommandEngine();

   QDbState rowCount(int &count) const override;

   bool atEnd() const override;

   QDbState next() override;

   QDbSchema::Table  resultSchema() const override;
   QDbState  columnValue(int index, Variant &value) const override;
   QDbState  columnValue(const QString &name, Variant &value) const override;

protected:
   class ODBCPhInfo : public PhInfo
   {
   public:
      ODBCPhInfo(const QDbSchema::Column& schema, QDbConnection::PhType phType, QODBCConnection* c);
      ~ODBCPhInfo() override { if (_buffer) free(_buffer); }

      // PhInfo interface

      bool isValid() const override { return _schema.vendorType; }

      Variant value() const override { return value(0); }
      QDbState setValue(const Variant& value) override { return setValue(value, 0); }

      void setType(QDbDataType type) override;

      // ODBC specific

      int   rowCount() const { return _rowCount; }
      void  setRowCount(int rowCount) { _rowCount = rowCount; }

      QDbState bindResult(SQLHSTMT hstmt, int index);
      QDbState bindParameter(SQLHSTMT hstmt, int index);
      QDbState setNumericDescriptor(SQLHSTMT hstmt, int pos, bool asParameter, bool withBuffer);
      QDbState getValuePiecewise(SQLHSTMT hstmt, int index);

      Variant  value(int row) const;
      QDbState setValue(const Variant &value, int row);

      void*       valueBuffer(int row = 0);
      const void* valueBuffer(int row = 0) const;
      bool        limitValueBufferSize();

      SQLLEN*        indicatorBuffer();
      const SQLLEN*  indicatorBuffer() const;

      SQLSMALLINT targetType();
      SQLSMALLINT targetType() const { return _cType; }

   private:
      void updateValueBufferSize();
      void createBuffer();

   private:
      QODBCConnection* _connection = nullptr;

      int      _cType = SQL_UNKNOWN_TYPE;
      SQLLEN   _valueBufferSize = 0; // Length of the buffer for 1 item!
      int      _rowCount = 1;
      quint8*  _buffer = nullptr;
      int      _bufferSize = 0;

      const SQLLEN _valueBufferSizeLimit = 0x10000; // Limit the size to 64k
      const SQLLEN _defaultChunkSize = 0x1000;
   };

   QDbConnectionData* connection() override { return _connection.data(); }
   PhInfo* createPhInfo(const QDbSchema::Column& schema, QDbConnection::PhType phType) override;
   QDbState setPlaceholderType(PhInfo* phInfo, QDbDataType type) override;
   QDbState setPlaceholderValue(PhInfo* phInfo, const Variant& value) override;
   QDbState placeholderValue(const PhInfo* phInfo, Variant& value) const override;
   QDbState prepare(const QList<PhInfo*>& phList) override;
   QDbState exec(const QList<PhInfo*>& phList, int rowsToFetch) override; // if rowsToFetch == 1 piecewise get is used to retrieve the values
   void clear() override;

private:
   QDbState prepareStatement(const QList<PhInfo*>& phList);

private:
   bp<QODBCConnection> _connection;

   SQLHSTMT                _hstmt = nullptr;
   bool                    _prepared = false;
   bool                    _executed = false;
   QVector<ODBCPhInfo*>    _resultList;
   unsigned int            _rowsFetched = 0;
   QVector<SQLUSMALLINT>   _rowStatusList;
   int                     _rowIndex = 0;
   bool                    _hasMore = false;
};

#endif // QODBCCOMMANDENGINE_H
