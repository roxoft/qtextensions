#ifndef QODBCCONNECTION_H
#define QODBCCONNECTION_H

#include "qtodbcconnector_global.h"
#include <QDbConnectionData>
#include <QDbSchema>
#include <QMap>

struct QTODBCCONNECTOR_EXPORT QODBCConnectionParameters
{
   QString dataSource;
   QString userName;
   QString password;

   QString toConnectionString() const;
};

struct QTODBCCONNECTOR_EXPORT QSqlServerConnectionParameters
{
   QString driver = "{SQL Server}";
   QString server;
   QString database;
   QString userName;
   QString password;

   QString toConnectionString() const;
};

// Implementation for QDbConnection for ODBC.
class QTODBCCONNECTOR_EXPORT QODBCConnection : public QDbConnectionData
{
   Q_DISABLE_COPY(QODBCConnection)
   Q_DECLARE_TR_FUNCTIONS(QODBCConnection)

public:
   QODBCConnection(const QString& connectionString);
   ~QODBCConnection() override;

   bool isValid() const override;

   QString  connectionString() const override;
   QString  dataSourceName() const override;
   QString  driverName() const override;
   QString  dbmsName() const override;
   QString  dbmsVersion() const override;
   SqlStyle sqlStyle() const override;
   QDbConnection::PhType phType() const override;

   QDbEnvironment *environment() override;

   bool beginTransaction() override;
   bool rollbackTransaction() override;
   bool commitTransaction() override;

   bool abortExecution() override;

   bool tableExists(const QString &tableName) override;

   QChar phIndicator() const override;

   QString toBooleanLiteral(bool value) const override;
   QString toDateLiteral(const QDateEx &value) const override;
   QString toTimeLiteral(const QTimeEx &value) const override;
   QString toDateTimeLiteral(const QDateTimeEx &value) const override;
   QString toStringLiteral(QString value) const override;
   QString toBinaryLiteral(const QByteArray &value) const override;

   QDbSchema::Database   databaseSchema() override;
   QDbSchema::Synonym    synonymSchema(const QString &name) override;
   QDbSchema::Table      tableSchema(const QString &name) override;
   QDbSchema::Function   functionSchema(const QString &name) override;
   QDbSchema::Package    packageSchema(const QString &name) override;

   QList<QDbSchema::Type> getTypeInfo(int sqlType = 0); // Default is SQL_ALL_TYPES

   // Limit the precision of numeric (decimal) values.
   // Some ODBC drivers cannot handle numeric precisions above a certain level, though the NUMERIC data type can handle 38 decimal digits.
   // Some drivers just return the value 0 if a numeric precision above the maximum level is requested.
   // In SQL Server, the default maximum precision of numeric and decimal data types is 38. In earlier versions of SQL Server, the default maximum is 28.
   // In Sybase 7 the maximum precision is 28.
   int numericPrecisionLimit() const;
   void setNumericPrecisionLimit(int limit); // limit <= 0 sets the limit to 38. The limit just reduces the precision when fetching numeric values from the database.

public:
   static QDbState installedDrivers(QMap<QString, QString> &drivers);
   static QDbState dataSources(QMap<QString, QString> &sources);
   static QDbDataType dataTypeFromSqlType(int sqlType);

protected:
   QDbCommandEngine *newEngine() override;

private:
   class Data;

private:
   QList<QDbSchema::Column> columns(const QString &name, QDbSchema::Column *returnColumn, QList<QDbSchema::Column> *resultSet);

private:
   int _numericLimit = 38;
   QString _connectString;
   Data* _data;
};

extern "C" {
   QTODBCCONNECTOR_EXPORT void createSybaseConnectionString(QString& connectString, const QString& userName, const QString& password, const QString& dataSourceName);
   QTODBCCONNECTOR_EXPORT void createSqlServerConnectionString(QString& connectString, const QString& driver, const QString& server, const QString& userName, const QString& password, const QString& dbName);
   QTODBCCONNECTOR_EXPORT QDbConnectionData* newODBCConnection(const QString& connectionString);
   QTODBCCONNECTOR_EXPORT QDbConnectionData* newODBCNumericLimitConnection(const QString& connectionString, int numericLimit);
}

#endif // QODBCCONNECTION_H
