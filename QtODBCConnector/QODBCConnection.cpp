#include "QODBCConnection.h"
#include "QODBCEnvironment.h"
#include "QODBCCommandEngine.h"
#include "msodbcsql.h"
#include <Variant.h>
#include <QRational.h>
#include <QDbConnection.h> // For SQL92_DATETIME_FORMAT

static const int maxNameLen = 255; // May only be needed by the remark, the other values can be 129

static void addToConnectionString(QString& connectionString, const QString& key, const QString& value)
{
   if (value.isEmpty())
      return;

   if (!connectionString.isEmpty())
      connectionString += u';';
   connectionString += key;
   connectionString += u'=';
   connectionString += value;
}

static int vendorTypeFromSqlType(int sqlType, int sqlSubType)
{
   if (sqlType != SQL_DATETIME)
   {
      return sqlType;
   }

   switch (sqlSubType)
   {
   case SQL_CODE_DATE: // SqlServer
   case SQL_DATE: // Sybase
      return SQL_TYPE_DATE;
   case SQL_CODE_TIME: // SqlServer
   case SQL_TIME: // Sybase
      return SQL_TYPE_TIME;
   case SQL_CODE_TIMESTAMP: // SqlServer
   case SQL_TIMESTAMP: // Sybase
      return SQL_TYPE_TIMESTAMP;
   }

   return 0;
}

class QODBCConnection::Data
{
public:
   Data() = default;
   ~Data()
   {
      if (connected)
         SQLDisconnect(dbc); /* disconnect from driver */
      /* free up allocated handles */
      SQLFreeHandle(SQL_HANDLE_DBC, dbc);
   }

public:
   SQLHDBC dbc = 0;
   bool connected = false;
   QString driverName;
   QString dbmsName;
   QString dbmsVersion;
   QMultiMap<int, QDbSchema::Type> typeMap;
   bool transaction = false;
};

QString QODBCConnectionParameters::toConnectionString() const
{
   QString connectString;

   // For connection parameters see (http://infocenter.sybase.com/help/index.jsp?topic=/com.sybase.infocenter.dc20116.1550/html/aseodbc/CHDCGBEH.htm)
   addToConnectionString(connectString, "DSN", dataSource);
   addToConnectionString(connectString, "UID", userName);
   addToConnectionString(connectString, "PWD", password);

   return connectString;
}

QString QSqlServerConnectionParameters::toConnectionString() const
{
   QString connectString;

   // For connection parameters see (https://www.connectionstrings.com/microsoft-sql-server-odbc-driver)
   addToConnectionString(connectString, "Driver", driver);
   addToConnectionString(connectString, "Server", server);
   addToConnectionString(connectString, "Database", database);
   addToConnectionString(connectString, "Uid", userName);
   addToConnectionString(connectString, "Pwd", password);

   return connectString;
}

QODBCConnection::QODBCConnection(const QString& connectionString) : _connectString(connectionString), _data(new Data)
{
   QODBCEnvironment::attach();

   _dbState = QODBCEnvironment::theEnvironment->dbState();

   /* Allocate a connection handle */
   if (_dbState.type() != QDbState::Error)
      _dbState = checkerr(SQLAllocHandle(SQL_HANDLE_DBC, QODBCEnvironment::theEnvironment->env(), &_data->dbc), QODBCEnvironment::theEnvironment->env(), SQL_HANDLE_ENV, false);

   // Connect to the DSN
   SQLWCHAR outstr[1024];
   SQLSMALLINT outstrlen;

   if (_dbState.type() != QDbState::Error)
      _dbState = checkerr(SQLDriverConnect(_data->dbc, NULL, (SQLWCHAR *)_connectString.utf16(), SQL_NTS, outstr, sizeof(outstr) / sizeof(*outstr), &outstrlen, SQL_DRIVER_NOPROMPT), _data->dbc, SQL_HANDLE_DBC, false);

   if (_dbState.type() != QDbState::Error)
   {
      _data->connected = true;
      _connectString = QString::fromUtf16((const char16_t *)outstr);
   }
}

QODBCConnection::~QODBCConnection()
{
   delete _data;

   QODBCEnvironment::detach();
}

bool QODBCConnection::isValid() const
{
   return _data->connected;
}

QString QODBCConnection::connectionString() const
{
   return _connectString;
}

QString QODBCConnection::dataSourceName() const
{
   wchar_t     dsn[SQL_MAX_DSN_LENGTH + 1];
   SQLSMALLINT lengthIndicator;

   _dbState = checkerr(SQLGetInfo(_data->dbc, SQL_DATA_SOURCE_NAME, (SQLPOINTER)dsn, sizeof(dsn) / sizeof(*dsn), &lengthIndicator), _data->dbc, SQL_HANDLE_DBC, false);
   if (_dbState.type() != QDbState::Error)
      return QString::fromUtf16((const char16_t *)dsn);
   return QString();
}

QString QODBCConnection::driverName() const
{
   if (_data->driverName.isEmpty())
   {
      wchar_t     dsn[SQL_MAX_DSN_LENGTH + 1];
      SQLSMALLINT lengthIndicator;

      _dbState = checkerr(SQLGetInfo(_data->dbc, SQL_DRIVER_NAME, (SQLPOINTER)dsn, sizeof(dsn) / sizeof(*dsn), &lengthIndicator), _data->dbc, SQL_HANDLE_DBC, false);
      if (_dbState.type() != QDbState::Error)
         _data->driverName = QString::fromUtf16((const char16_t *)dsn);
   }

   return _data->driverName;
}

QString QODBCConnection::dbmsName() const
{
   if (_data->dbmsName.isEmpty())
   {
      wchar_t     dsn[SQL_MAX_DSN_LENGTH + 1];
      SQLSMALLINT lengthIndicator;

      _dbState = checkerr(SQLGetInfo(_data->dbc, SQL_DBMS_NAME, (SQLPOINTER)dsn, sizeof(dsn) / sizeof(*dsn), &lengthIndicator), _data->dbc, SQL_HANDLE_DBC, false);
      if (_dbState.type() != QDbState::Error)
         _data->dbmsName = QString::fromUtf16((const char16_t *)dsn);
   }

   return _data->dbmsName;
}

QString QODBCConnection::dbmsVersion() const
{
   if (_data->dbmsVersion.isEmpty())
   {
      wchar_t     dsn[SQL_MAX_DSN_LENGTH + 1];
      SQLSMALLINT lengthIndicator;

      _dbState = checkerr(SQLGetInfo(_data->dbc, SQL_DBMS_VER, (SQLPOINTER)dsn, sizeof(dsn) / sizeof(*dsn), &lengthIndicator), _data->dbc, SQL_HANDLE_DBC, false);
      if (_dbState.type() != QDbState::Error)
         _data->dbmsVersion = QString::fromUtf16((const char16_t *)dsn);
   }

   return _data->dbmsVersion;
}

SqlStyle QODBCConnection::sqlStyle() const
{
   if (dbmsName().startsWith("Adaptive Server"))
      return SqlStyle::Sybase;
   if (dbmsName().startsWith("Microsoft SQL"))
      return SqlStyle::SqlServer;
   return SqlStyle::ODBC;
}

QDbConnection::PhType QODBCConnection::phType() const
{
   return QDbConnection::PHT_UnnamedOnly;
}

QDbEnvironment *QODBCConnection::environment()
{
   return QODBCEnvironment::theEnvironment;
}

bool QODBCConnection::beginTransaction()
{
   if (!_data->transaction)
   {
      _dbState = checkerr(SQLSetConnectAttr(_data->dbc, SQL_ATTR_AUTOCOMMIT, (SQLPOINTER) SQL_AUTOCOMMIT_OFF, (SQLINTEGER) 0), _data->dbc, SQL_HANDLE_DBC, false);
      _data->transaction = true;
      return true;
   }
   return false;
}

bool QODBCConnection::rollbackTransaction()
{
   if (_data->transaction)
   {
      _data->transaction = false;
      _dbState = checkerr(SQLEndTran(SQL_HANDLE_DBC, _data->dbc, SQL_ROLLBACK), _data->dbc, SQL_HANDLE_DBC, false); // May throw an exception
      if (_dbState.type() != QDbState::Error)
         _dbState = checkerr(SQLSetConnectAttr(_data->dbc, SQL_ATTR_AUTOCOMMIT, (SQLPOINTER) SQL_AUTOCOMMIT_ON, (SQLINTEGER) 0), _data->dbc, SQL_HANDLE_DBC, false);
   }
   return _dbState.type() != QDbState::Error;
}

bool QODBCConnection::commitTransaction()
{
   if (_data->transaction)
   {
      _data->transaction = false;
      _dbState = checkerr(SQLSetConnectAttr(_data->dbc, SQL_ATTR_AUTOCOMMIT, (SQLPOINTER) SQL_AUTOCOMMIT_ON, (SQLINTEGER) 0), _data->dbc, SQL_HANDLE_DBC, false);
   }
   return _dbState.type() != QDbState::Error;
}

bool QODBCConnection::abortExecution()
{
   return false;
}

bool QODBCConnection::tableExists(const QString &tableName)
{
   return tableSchema(tableName).isValid();
}

QChar QODBCConnection::phIndicator() const
{
   return u':';
}

QString QODBCConnection::toBooleanLiteral(bool value) const
{
   return value ? "1" : "0";
}

QString QODBCConnection::toDateLiteral(const QDateEx& value) const
{
   return QString("{d '%1'}").arg(value.toString(SQL92_DATE_FORMAT));
}

QString QODBCConnection::toTimeLiteral(const QTimeEx& value) const
{
   return QString("{t '%1'}").arg(value.toString(SQL92_TIME_FORMAT));
}

QString QODBCConnection::toDateTimeLiteral(const QDateTimeEx& value) const
{
   return QString("{ts '%1'}").arg(value.toString(SQL92_DATETIME_FORMAT));
}

QString QODBCConnection::toStringLiteral(QString value) const
{
   if (sqlStyle() == SqlStyle::Sybase)
   {
      value
         .replace("\\", "\\\\")
         .replace("\t", "\\t")
         //.replace("\r", "\\r")
         .replace("\n", "\\n");
   }
   else if (sqlStyle() == SqlStyle::SqlServer)
   {
      value.replace("\\\r\n", "\\\\\r\n\r\n").replace("\\\n", "\\\\\n\n");
   }

   return QDbConnectionData::toStringLiteral(value);
}

QString QODBCConnection::toBinaryLiteral(const QByteArray& value) const
{
   return QString("'0x%1'").arg(QString::fromLatin1(value.toHex()));
}

QDbSchema::Database QODBCConnection::databaseSchema()
{
   QDbSchema::Database dbInfo;

   SQLHSTMT stmt;
   SQLRETURN ret; /* ODBC API return status */
   SQLSMALLINT columns; /* number of columns in result-set */

   /* Allocate a statement handle */
   SQLAllocHandle(SQL_HANDLE_STMT, _data->dbc, &stmt);
   /* Retrieve a list of tables */
   SQLTablesA(stmt, NULL, 0, NULL, 0, NULL, 0, (SQLCHAR*)"TABLE", SQL_NTS);
   /* How many columns are there */
   SQLNumResultCols(stmt, &columns);
   /* Loop through the rows in the result-set */
   while (SQL_SUCCEEDED(ret = SQLFetch(stmt)))
   {
      QDbSchema::Table tableInfo;

      SQLUSMALLINT i;

      /* Loop through the columns */
      for (i = 1; i <= columns; i++)
      {
         SQLLEN   indicator;
         wchar_t  buf[512];
         /* retrieve column data as a string */
         ret = SQLGetData(stmt, i, SQL_C_WCHAR, buf, sizeof(buf) / sizeof(*buf), &indicator);
         if (SQL_SUCCEEDED(ret))
         {
            /* Handle null columns */
            if (indicator == SQL_NULL_DATA)
               *buf = u'\0';
            if (i == 2)
               tableInfo.schema = QString::fromUtf16((const char16_t *)buf);
            if (i == 3)
               tableInfo.name = QString::fromUtf16((const char16_t *)buf);
         }
      }

      dbInfo.tables.insert(tableInfo.name, tableInfo);
   }
   // free up allocated handles
   SQLFreeHandle(SQL_HANDLE_STMT, stmt);

   return dbInfo;
}

QDbSchema::Synonym QODBCConnection::synonymSchema(const QString &)
{
   QDbSchema::Synonym synonymInfo;

   return synonymInfo;
}

QDbSchema::Table QODBCConnection::tableSchema(const QString &name)
{
   QDbSchema::Table tableInfo;

   tableInfo.name = name;
   tableInfo.columns = columns(name, nullptr, nullptr);
   if (!tableInfo.columns.isEmpty())
      tableInfo.schema = tableInfo.columns.first().schema;

   return tableInfo;
}

QDbSchema::Function QODBCConnection::functionSchema(const QString &name)
{
   QDbSchema::Function funcInfo;

   SQLHSTMT stmt = NULL;

   try
   {
      SQLCHAR     szCatalog[maxNameLen];
      SQLCHAR     szSchema[maxNameLen];
      SQLCHAR     szProcedureName[maxNameLen];
      SQLCHAR     szRemark[maxNameLen];
      SQLSMALLINT procedureType;

      SQLLEN cbCatalog;
      SQLLEN cbSchema;
      SQLLEN cbProcedureName;
      SQLLEN cbRemark;
      SQLLEN cbProcedureType;

      /* Allocate a statement handle */
      checkerr(SQLAllocHandle(SQL_HANDLE_STMT, _data->dbc, &stmt), _data->dbc, SQL_HANDLE_DBC, true);

      // Retrieve a list of procedures
      checkerr(SQLProcedures(stmt, NULL, 0, NULL, 0, (SQLWCHAR*)name.utf16(), name.length()), stmt, SQL_HANDLE_STMT, true);

      checkerr(SQLBindCol(stmt, 1, SQL_C_CHAR, szCatalog, maxNameLen, &cbCatalog), stmt, SQL_HANDLE_STMT, true);
      checkerr(SQLBindCol(stmt, 2, SQL_C_CHAR, szSchema, maxNameLen, &cbSchema), stmt, SQL_HANDLE_STMT, true);
      checkerr(SQLBindCol(stmt, 3, SQL_C_CHAR, szProcedureName, maxNameLen,&cbProcedureName), stmt, SQL_HANDLE_STMT, true);
      checkerr(SQLBindCol(stmt, 7, SQL_C_CHAR, szRemark, maxNameLen,&cbRemark), stmt, SQL_HANDLE_STMT, true);
      checkerr(SQLBindCol(stmt, 8, SQL_C_SSHORT, &procedureType, 0,&cbProcedureType), stmt, SQL_HANDLE_STMT, true);

      /* Fetch the data */
      SQLRETURN ret;

      if (!SQL_SUCCEEDED(ret = SQLFetch(stmt)))
      {
         if (ret == SQL_NO_DATA)
            throw QDbState(); // Procedure not found
         throw checkerr(ret, stmt, SQL_HANDLE_STMT, false);
      }

      if (cbSchema != SQL_NULL_DATA)
         funcInfo.schema = QString::fromLocal8Bit((const char *)szSchema);
      if (cbProcedureName != SQL_NULL_DATA)
         funcInfo.name = QString::fromLocal8Bit((const char *)szProcedureName);

      // free up allocated handles
      SQLFreeHandle(SQL_HANDLE_STMT, stmt);
      stmt = NULL;

      funcInfo.arguments = columns(funcInfo.name, &funcInfo.retVal, &funcInfo.resultSet);
   }
   catch (const QDbState &dbState)
   {
      // free up allocated handles
      if (stmt)
         SQLFreeHandle(SQL_HANDLE_STMT, stmt);
      _dbState = dbState; // May throw an exception
   }

   return funcInfo;
}

QDbSchema::Package QODBCConnection::packageSchema(const QString &)
{
   QDbSchema::Package packageInfo;

   return packageInfo;
}

QList<QDbSchema::Type> QODBCConnection::getTypeInfo(int sqlType)
{
   if (_data->typeMap.isEmpty())
   {
      SQLHSTMT hstmt = SQL_NULL_HSTMT;  	// Statement handle
      SQLRETURN retcode;			// Return status

      SQLCHAR typeName[128];
      SQLSMALLINT dataType;
      SQLINTEGER columnSize;

      SQLLEN typeName_ind, dataType_ind, columnSize_ind;

      checkerr(SQLAllocHandle(SQL_HANDLE_STMT, _data->dbc, &hstmt), hstmt, SQL_HANDLE_STMT, true);

      checkerr(SQLGetTypeInfo(hstmt, SQL_ALL_TYPES), hstmt, SQL_HANDLE_STMT, true);

      checkerr(SQLBindCol(hstmt, 1, SQL_C_CHAR, (SQLPOINTER) typeName, (SQLLEN) sizeof(typeName), &typeName_ind), hstmt, SQL_HANDLE_STMT, true);
      checkerr(SQLBindCol(hstmt, 2, SQL_C_SHORT, (SQLPOINTER) &dataType, (SQLLEN) sizeof(dataType), &dataType_ind), hstmt, SQL_HANDLE_STMT, true);
      checkerr(SQLBindCol(hstmt, 3, SQL_C_ULONG, (SQLPOINTER) &columnSize, (SQLLEN) sizeof(columnSize), &columnSize_ind), hstmt, SQL_HANDLE_STMT, true);

      // Fetch each row
      while (SQL_SUCCEEDED(retcode = SQLFetch(hstmt)))
      {
         QDbSchema::Type typeInfo;

         typeInfo.name = QString::fromLatin1((char*)typeName);
         typeInfo.sqlType = dataType;
         typeInfo.size = columnSize;

         _data->typeMap.insert(dataType, typeInfo);
      }

      checkerr(retcode, hstmt, SQL_HANDLE_STMT, true);

      if (hstmt != SQL_NULL_HSTMT)
          SQLFreeHandle(SQL_HANDLE_STMT, hstmt);
   }

   if (sqlType == SQL_ALL_TYPES)
      return _data->typeMap.values();
   return _data->typeMap.values(sqlType);
}

int QODBCConnection::numericPrecisionLimit() const
{
   return _numericLimit;
}

void QODBCConnection::setNumericPrecisionLimit(int limit)
{
   _numericLimit = limit > 0 ? limit : 38;
}

QDbState QODBCConnection::installedDrivers(QMap<QString, QString> &drivers)
{
   QDbState dbState;

   QODBCEnvironment::attach();

   dbState = QODBCEnvironment::theEnvironment->dbState();
   if (dbState.type() != QDbState::Error)
   {
      SQLWCHAR driver[256];
      SQLWCHAR attributes[256];
      SQLSMALLINT driver_len;
      SQLSMALLINT attributes_len;
      SQLUSMALLINT direction;
      SQLRETURN ret;

      drivers.clear();
      direction = SQL_FETCH_FIRST;
      while (SQL_SUCCEEDED(ret = SQLDrivers(QODBCEnvironment::theEnvironment->env(), direction, driver, sizeof(driver) / sizeof(*driver), &driver_len, attributes, sizeof(attributes) / sizeof(*attributes), &attributes_len)))
      {
         auto sqlDriver = QString::fromUtf16((const char16_t *)driver);
         auto sqlAttributes = QString::fromUtf16((const char16_t *)attributes);

         if ((size_t)driver_len >= sizeof(driver) / sizeof(*driver))
            sqlDriver += "..."; // data truncation
         if ((size_t)attributes_len >= sizeof(attributes) / sizeof(*attributes))
            sqlAttributes += "..."; // data truncation

         drivers.insert(sqlDriver, sqlAttributes);

         direction = SQL_FETCH_NEXT;
      }

      dbState = checkerr(ret, QODBCEnvironment::theEnvironment->env(), SQL_HANDLE_ENV, false);
   }

   QODBCEnvironment::detach();

   return dbState;
}

QDbState QODBCConnection::dataSources(QMap<QString, QString> &sources)
{
   QDbState dbState;

   QODBCEnvironment::attach();

   dbState = QODBCEnvironment::theEnvironment->dbState();
   if (dbState.type() != QDbState::Error)
   {
      SQLWCHAR dsn[SQL_MAX_DSN_LENGTH + 1];
      SQLWCHAR desc[256];
      SQLSMALLINT dsn_len;
      SQLSMALLINT desc_len;
      SQLUSMALLINT direction;
      SQLRETURN ret;

      sources.clear();
      direction = SQL_FETCH_FIRST;
      while (SQL_SUCCEEDED(ret = SQLDataSources(QODBCEnvironment::theEnvironment->env(), direction, dsn, sizeof(dsn) / sizeof(*dsn), &dsn_len, desc, sizeof(desc) / sizeof(*desc), &desc_len)))
      {
         auto sqlDsn = QString::fromUtf16((const char16_t *)dsn);
         auto sqlDesc = QString::fromUtf16((const char16_t *)desc);

         if ((size_t)dsn_len >= sizeof(dsn) / sizeof(*dsn))
            sqlDsn += "..."; // data truncation
         if ((size_t)desc_len >= sizeof(desc) / sizeof(*desc))
            sqlDesc += "..."; // data truncation

         sources.insert(sqlDsn, sqlDesc);

         direction = SQL_FETCH_NEXT;
      }

      dbState = checkerr(ret, QODBCEnvironment::theEnvironment->env(), SQL_HANDLE_ENV, false);
   }

   QODBCEnvironment::detach();

   return dbState;
}

QDbDataType QODBCConnection::dataTypeFromSqlType(int sqlType)
{
   switch (sqlType)
   {
   case SQL_CHAR:
   case SQL_WCHAR:
   case SQL_VARCHAR:
   case SQL_WVARCHAR:
      return DT_STRING;
   case SQL_LONGVARCHAR:
   case SQL_WLONGVARCHAR:
      return DT_LARGE_STRING;
   case SQL_DECIMAL:
   case SQL_NUMERIC:
      return DT_DECIMAL;
   case SQL_BIT:
      return DT_BOOL;
   case SQL_TINYINT:
   case SQL_SMALLINT:
   case SQL_INTEGER:
      return DT_INT;
   case SQL_BIGINT:
      return DT_LONG;
   case SQL_REAL:
   case SQL_FLOAT:
   case SQL_DOUBLE:
      return DT_DOUBLE;
   case SQL_BINARY:
   case SQL_VARBINARY:
      return DT_BINARY;
   case SQL_LONGVARBINARY:
      return DT_LARGE_BINARY;
   case SQL_TYPE_DATE:
      return DT_DATE;
   case SQL_TYPE_TIME:
   case SQL_SS_TIME2:
      return DT_TIME;
   case SQL_TYPE_TIMESTAMP:
   case SQL_SS_TIMESTAMPOFFSET:
      return DT_DATETIME;
   }

   return DT_UNKNOWN;
}

QDbCommandEngine *QODBCConnection::newEngine()
{
   /* Allocate a statement handle */
   SQLHSTMT stmt = 0;

   _dbState = checkerr(SQLAllocHandle(SQL_HANDLE_STMT, _data->dbc, &stmt), _data->dbc, SQL_HANDLE_DBC, false);

   return new QODBCCommandEngine(this, stmt);
}

QList<QDbSchema::Column> QODBCConnection::columns(const QString &name, QDbSchema::Column *returnColumn, QList<QDbSchema::Column> *resultSet)
{
   QList<QDbSchema::Column> columnList;

   SQLHSTMT stmt = NULL;

   try
   {
      SQLCHAR szSchema[maxNameLen];
      SQLCHAR szCatalog[maxNameLen];
      SQLCHAR szColumnName[maxNameLen];
      SQLCHAR szTableName[maxNameLen];
      SQLCHAR szTypeName[maxNameLen];
      SQLCHAR szRemarks[maxNameLen];
      SQLCHAR szColumnDefault[maxNameLen];
      SQLCHAR szIsNullable[maxNameLen];

      SQLINTEGER ColumnSize;
      SQLINTEGER BufferLength;
      SQLINTEGER CharOctetLength;
      SQLINTEGER OrdinalPosition;

      SQLSMALLINT ColumnType;
      SQLSMALLINT DataType;
      SQLSMALLINT DecimalDigits;
      SQLSMALLINT NumPrecRadix;
      SQLSMALLINT Nullable;
      SQLSMALLINT SQLDataType;
      SQLSMALLINT DatetimeSubtypeCode;

      // Declare buffers for bytes available to return
      SQLLEN cbCatalog;
      SQLLEN cbSchema;
      SQLLEN cbTableName;
      SQLLEN cbColumnName;
      SQLLEN cbColumnType;
      SQLLEN cbDataType;
      SQLLEN cbTypeName;
      SQLLEN cbColumnSize;
      SQLLEN cbBufferLength;
      SQLLEN cbDecimalDigits;
      SQLLEN cbNumPrecRadix;
      SQLLEN cbNullable;
      SQLLEN cbRemarks;
      SQLLEN cbColumnDefault;
      SQLLEN cbSQLDataType;
      SQLLEN cbDatetimeSubtypeCode;
      SQLLEN cbCharOctetLength;
      SQLLEN cbOrdinalPosition;
      SQLLEN cbIsNullable;

      /* Allocate a statement handle */
      checkerr(SQLAllocHandle(SQL_HANDLE_STMT, _data->dbc, &stmt), _data->dbc, SQL_HANDLE_DBC, true);

      /* Retrieve a list of columns */
      if (returnColumn)
         checkerr(SQLProcedureColumns(stmt, NULL, 0, NULL, 0, (SQLWCHAR *)name.utf16(), SQL_NTS, NULL, 0), stmt, SQL_HANDLE_STMT, true);
      else
         checkerr(SQLColumns(stmt, NULL, 0, NULL, 0, (SQLWCHAR *)name.utf16(), SQL_NTS, NULL, 0), stmt, SQL_HANDLE_STMT, true);

      int col = 0;

      checkerr(SQLBindCol(stmt, ++col, SQL_C_CHAR, szCatalog, maxNameLen, &cbCatalog), stmt, SQL_HANDLE_STMT, true);
      checkerr(SQLBindCol(stmt, ++col, SQL_C_CHAR, szSchema, maxNameLen, &cbSchema), stmt, SQL_HANDLE_STMT, true);
      checkerr(SQLBindCol(stmt, ++col, SQL_C_CHAR, szTableName, maxNameLen,&cbTableName), stmt, SQL_HANDLE_STMT, true);
      checkerr(SQLBindCol(stmt, ++col, SQL_C_CHAR, szColumnName, maxNameLen, &cbColumnName), stmt, SQL_HANDLE_STMT, true);
      if (returnColumn)
         checkerr(SQLBindCol(stmt, ++col, SQL_C_SSHORT, &ColumnType, 0, &cbColumnType), stmt, SQL_HANDLE_STMT, true);
      checkerr(SQLBindCol(stmt, ++col, SQL_C_SSHORT, &DataType, 0, &cbDataType), stmt, SQL_HANDLE_STMT, true);
      checkerr(SQLBindCol(stmt, ++col, SQL_C_CHAR, szTypeName, maxNameLen, &cbTypeName), stmt, SQL_HANDLE_STMT, true);
      checkerr(SQLBindCol(stmt, ++col, SQL_C_SLONG, &ColumnSize, 0, &cbColumnSize), stmt, SQL_HANDLE_STMT, true);
      checkerr(SQLBindCol(stmt, ++col, SQL_C_SLONG, &BufferLength, 0, &cbBufferLength), stmt, SQL_HANDLE_STMT, true);
      checkerr(SQLBindCol(stmt, ++col, SQL_C_SSHORT, &DecimalDigits, 0, &cbDecimalDigits), stmt, SQL_HANDLE_STMT, true);
      checkerr(SQLBindCol(stmt, ++col, SQL_C_SSHORT, &NumPrecRadix, 0, &cbNumPrecRadix), stmt, SQL_HANDLE_STMT, true);
      checkerr(SQLBindCol(stmt, ++col, SQL_C_SSHORT, &Nullable, 0, &cbNullable), stmt, SQL_HANDLE_STMT, true);
      checkerr(SQLBindCol(stmt, ++col, SQL_C_CHAR, szRemarks, maxNameLen, &cbRemarks), stmt, SQL_HANDLE_STMT, true);
      checkerr(SQLBindCol(stmt, ++col, SQL_C_CHAR, szColumnDefault, maxNameLen, &cbColumnDefault), stmt, SQL_HANDLE_STMT, true);
      checkerr(SQLBindCol(stmt, ++col, SQL_C_SSHORT, &SQLDataType, 0, &cbSQLDataType), stmt, SQL_HANDLE_STMT, true);
      checkerr(SQLBindCol(stmt, ++col, SQL_C_SSHORT, &DatetimeSubtypeCode, 0, &cbDatetimeSubtypeCode), stmt, SQL_HANDLE_STMT, true);
      checkerr(SQLBindCol(stmt, ++col, SQL_C_SLONG, &CharOctetLength, 0, &cbCharOctetLength), stmt, SQL_HANDLE_STMT, true);
      checkerr(SQLBindCol(stmt, ++col, SQL_C_SLONG, &OrdinalPosition, 0, &cbOrdinalPosition), stmt, SQL_HANDLE_STMT, true);
      checkerr(SQLBindCol(stmt, ++col, SQL_C_CHAR, szIsNullable, maxNameLen, &cbIsNullable), stmt, SQL_HANDLE_STMT, true);

      /* Fetch the data */
      SQLRETURN ret;

      while (SQL_SUCCEEDED(ret = SQLFetch(stmt)))
      {
         QDbSchema::Column columnInfo;
         bool              isReturnColumn = false;
         bool              isResultColumn = false;

         if (cbSchema != SQL_NULL_DATA)
            columnInfo.schema = QString::fromLocal8Bit((const char *)szSchema);
         if (cbColumnName != SQL_NULL_DATA)
            columnInfo.name = QString::fromLocal8Bit((const char *)szColumnName);
         if (returnColumn && cbColumnType != SQL_NULL_DATA)
         {
            switch (ColumnType)
            {
            case SQL_PARAM_INPUT:
               columnInfo.writable = true;
               break;
            case SQL_PARAM_INPUT_OUTPUT:
               columnInfo.readable = true;
               columnInfo.writable = true;
               break;
            case SQL_PARAM_OUTPUT:
               columnInfo.readable = true;
               break;
            case SQL_RETURN_VALUE:
               columnInfo.readable = true;
               isReturnColumn = true;
               break;
            case SQL_RESULT_COL:
               columnInfo.readable = true;
               isResultColumn = true;
               break;
            }
         }
         else
            columnInfo.readable = true;
         if (cbSQLDataType > 0)
            columnInfo.vendorType = vendorTypeFromSqlType(SQLDataType, DatetimeSubtypeCode);
         else if (cbDataType > 0)
            columnInfo.vendorType = vendorTypeFromSqlType(DataType, DatetimeSubtypeCode);
         columnInfo.type = dataTypeFromSqlType(columnInfo.vendorType);
         if (cbColumnSize != SQL_NULL_DATA)
            columnInfo.size = ColumnSize == SQL_SS_LENGTH_UNLIMITED ? INT_MAX : ColumnSize;
         if (cbDecimalDigits != SQL_NULL_DATA)
            columnInfo.scale = DecimalDigits;
         Q_ASSERT(cbNumPrecRadix == SQL_NULL_DATA || NumPrecRadix == 10);
         if (cbNullable != SQL_NULL_DATA)
            columnInfo.nullable = (Nullable == SQL_NULLABLE);
         if (cbColumnDefault != SQL_NULL_DATA)
            columnInfo.hasDefault = true; // (const char *)szColumnDefault

         if (columnInfo.vendorType == 0)
            throw QDbState(QDbState::Error, tr("Unknown SQL data type"), QDbState::Connection);

         if (isResultColumn)
            resultSet->append(columnInfo);
         else if (isReturnColumn)
            *returnColumn = columnInfo;
         else
            columnList.append(columnInfo);
      }

      if (ret != SQL_NO_DATA)
         checkerr(ret, stmt, SQL_HANDLE_STMT, true);

      // free up allocated handles
      SQLFreeHandle(SQL_HANDLE_STMT, stmt);
   }
   catch (const QDbState &dbState)
   {
      // free up allocated handles
      if (stmt)
         SQLFreeHandle(SQL_HANDLE_STMT, stmt);
      _dbState = dbState; // May throw an exception
   }

   return columnList;
}

extern "C" {
   void createSybaseConnectionString(QString& connectString, const QString& userName, const QString& password, const QString& dataSourceName)
   {
      QODBCConnectionParameters parameters;

      parameters.dataSource = dataSourceName;
      parameters.userName = userName;
      parameters.password = password;

      connectString = parameters.toConnectionString();
   }

   void createSqlServerConnectionString(QString& connectString, const QString& driver, const QString& server, const QString& userName, const QString& password, const QString& dbName)
   {
      QSqlServerConnectionParameters parameters;

      if (!driver.isEmpty())
         parameters.driver = driver;
      parameters.server = server;
      parameters.database = dbName;
      parameters.userName = userName;
      parameters.password = password;

      connectString = parameters.toConnectionString();
   }

   QDbConnectionData* newODBCConnection(const QString& connectionString)
   {
      return new QODBCConnection(connectionString);
   }

   QDbConnectionData* newODBCNumericLimitConnection(const QString& connectionString, int numericLimit)
   {
      auto connection = new QODBCConnection(connectionString);

      if (connection)
         connection->setNumericPrecisionLimit(numericLimit);

      return connection;
   }
}
