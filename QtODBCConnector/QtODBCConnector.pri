HEADERS += $$PWD/include/QODBCConnection.h \
    $$PWD/include/qtodbcconnector_global.h \
    $$PWD/QODBCEnvironment.h \
    $$PWD/QODBCCommandEngine.h \
    $$PWD/msodbcsql.h \
    $$PWD/TestCases.h
SOURCES += $$PWD/QODBCConnection.cpp \
    $$PWD/QODBCEnvironment.cpp \
    $$PWD/QODBCCommandEngine.cpp \
    $$PWD/TestCases.cpp
