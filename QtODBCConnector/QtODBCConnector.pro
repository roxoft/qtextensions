TEMPLATE = lib
TARGET = QtODBCConnector
unix {
    VERSION = 1.1.0
}
QT += core
QT -= gui
DEFINES += QTODBCCONNECTOR_LIB UNICODE
TRANSLATIONS = QtODBCConnector_de.ts

INCLUDEPATH += include
DEPENDPATH += .

include(QtODBCConnector.pri)

contains(QT_ARCH, i386) {
    CONFIG(release, debug|release): INSTALL_LIB_PATH = $$quote($$system_path($$_PRO_FILE_PWD_/../x86/Release))
    else:CONFIG(debug, debug|release): INSTALL_LIB_PATH = $$quote($$system_path($$_PRO_FILE_PWD_/../x86/Debug))
} else {
    CONFIG(release, debug|release): INSTALL_LIB_PATH = $$quote($$system_path($$_PRO_FILE_PWD_/../x64/Release))
    else:CONFIG(debug, debug|release): INSTALL_LIB_PATH = $$quote($$system_path($$_PRO_FILE_PWD_/../x64/Debug))
}

LIBS += -L$${INSTALL_LIB_PATH}

LIBS += -lQtTestFramework

INCLUDEPATH += $$quote($$system_path($$_PRO_FILE_PWD_/../include/QtTestFramework))
DEPENDPATH += $$quote($$system_path($$_PRO_FILE_PWD_/../include/QtTestFramework))

LIBS += -lQtCoreEx

INCLUDEPATH += $$quote($$system_path($$_PRO_FILE_PWD_/../include/QtCoreEx))
DEPENDPATH += $$quote($$system_path($$_PRO_FILE_PWD_/../include/QtCoreEx))

INSTALL_HEADER_PATH = $$quote($$system_path($$_PRO_FILE_PWD_/../include/$${TARGET}))

win32 {
    CONFIG(release, debug|release): TARGET_PATH = $$quote($$system_path($$OUT_PWD/release/$${TARGET}))
    else:CONFIG(debug, debug|release): TARGET_PATH = $$quote($$system_path($$OUT_PWD/debug/$${TARGET}))

    LIBS += -lOdbc32

    QMAKE_POST_LINK += (if not exist $${INSTALL_LIB_PATH} $(MKDIR) $${INSTALL_LIB_PATH})
    QMAKE_POST_LINK += && $(COPY) $${TARGET_PATH}.lib $${INSTALL_LIB_PATH}
    QMAKE_POST_LINK += && $(COPY) $${TARGET_PATH}.dll $${INSTALL_LIB_PATH}
    CONFIG(debug, debug|release): QMAKE_POST_LINK += && $(COPY) $${TARGET_PATH}.pdb $${INSTALL_LIB_PATH}
    QMAKE_POST_LINK += && (if not exist $${INSTALL_HEADER_PATH} $(MKDIR) $${INSTALL_HEADER_PATH})
} else: unix {

    LIBS += -lodbc

    QMAKE_POST_LINK += $(MKDIR) $${INSTALL_LIB_PATH}
    QMAKE_POST_LINK += && $(COPY) -d $$OUT_PWD/lib$${TARGET}.* $${INSTALL_LIB_PATH}
    QMAKE_POST_LINK += && $(MKDIR) $${INSTALL_HEADER_PATH}
}
QMAKE_POST_LINK += && $(COPY) $$quote($$system_path($$_PRO_FILE_PWD_/include/*)) $${INSTALL_HEADER_PATH}

qtPrepareTool(LUPDATE, lupdate)
QMAKE_POST_LINK += && $$LUPDATE $$PWD/QtODBCConnector.pro

qtPrepareTool(LRELEASE, lrelease)

# Default rules for deployment.
unix {
    target.path = /usr/lib/x86_64-linux-gnu
    header_files.files = include/*
    header_files.path = /usr/include/QtExtensions/QtODBCConnector
    translations.path = /usr/share/QtExtensions/translations
    translations.extra = $$quote($$LRELEASE $$PWD/QtODBCConnector_de.ts -qm /usr/share/QtExtensions/translations/QtODBCConnector_de.qm)
}
!isEmpty(target.path): INSTALLS += target
!isEmpty(header_files.path): INSTALLS += header_files
!isEmpty(translations.path): INSTALLS += translations
