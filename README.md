QtExtensions
============

Introduction
------------

QtExtensions is a collection of libraries that extends some Qt libraries (QtCore, QtGui).

An overview is provided in QtExtensions.md.

The class documentation is in QtExtensionsDocumentation.zip (see index.html) which can be downloaded from sourceforge.net/projects/qtextensions/files.

The documentation was created with CoDoGen. It processes comments in common mark format (with some restrictions and extensions).


Building with MS Visual Studio
------------------------------

Use the Visual Studio 2022 solution QtExtensions.sln.

To build QtOracleConnector an Oracle client must be installed.
To build QtODBCConnector ODBC must be installed.
To build QtDB2Connector a DB2 client must be installed.

Remove the projects you don't need from QtExtensions.sln.


Building with Qt Creator
------------------------

Use the Qt Creator project file QtExtensions.pro.

QtOracleConnector and QtDB2Connector are not included in QtExtensions.pro.
QtService is not fully functional for Linux.

See AdditionalConfigurationSettings.txt for further details.
