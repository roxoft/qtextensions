#include "QtGuiExWidgets.h"

#include "QComboBoxExInterface.h"
#include "QDateEditExInterface.h"
#include "QDateTimeEditExInterface.h"
#include "QDialogExInterface.h"
#include "QHoverButtonInterface.h"
#include "QIncrementalSearchWidgetInterface.h"
#include "QNumberEditInterface.h"
#include "QPlainTextEditExInterface.h"
#include "QSelectorInterface.h"
#include "QSizeGripExInterface.h"
#include "QTableViewExInterface.h"
#include "QTitleBarCloseButtonInterface.h"
#include "QTreeViewExInterface.h"

QtGuiExWidgets::QtGuiExWidgets(QObject *parent)
   : QObject(parent)
{
   widgets.append(new QComboBoxExInterface(this));
   widgets.append(new QSizeGripExInterface(this));
   widgets.append(new QSelectorInterface(this));
   widgets.append(new QDateEditExInterface(this));
   widgets.append(new QDateTimeEditExInterface(this));
   widgets.append(new QNumberEditInterface(this));
   widgets.append(new QTreeViewExInterface(this));
   widgets.append(new QTableViewExInterface(this));
   widgets.append(new QPlainTextEditExInterface(this));
   widgets.append(new QTitleBarCloseButtonInterface(this));
   widgets.append(new QIncrementalSearchWidgetInterface(this));
   widgets.append(new QDialogExInterface(this));
   widgets.append(new QHoverButtonInterface(this));
}

QtGuiExWidgets::~QtGuiExWidgets()
= default;

QList<QDesignerCustomWidgetInterface*> QtGuiExWidgets::customWidgets() const
{
   return widgets;
}

#if QT_VERSION < 0x050000
Q_EXPORT_PLUGIN2(qtguiexdesignerplugin, QtGuiExWidgets)
#endif // QT_VERSION < 0x050000
