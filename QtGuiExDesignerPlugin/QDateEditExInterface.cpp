#include <QDateEditEx>
#include "QDateEditExInterface.h"

#include <QtPlugin>

QDateEditExInterface::QDateEditExInterface(QObject *parent)
   : QObject(parent)
{
   initialized = false;
}

void QDateEditExInterface::initialize(QDesignerFormEditorInterface * /*core*/)
{
   if (initialized)
      return;

   initialized = true;
}

bool QDateEditExInterface::isInitialized() const
{
   return initialized;
}

QWidget *QDateEditExInterface::createWidget(QWidget *parent)
{
   return new QDateEditEx(parent);
}

QString QDateEditExInterface::name() const
{
   return QLatin1String("QDateEditEx");
}

QString QDateEditExInterface::group() const
{
   return QLatin1String("Qt GUI extensions");
}

QIcon QDateEditExInterface::icon() const
{
   return QIcon();
}

QString QDateEditExInterface::toolTip() const
{
   return QString();
}

QString QDateEditExInterface::whatsThis() const
{
   return QString();
}

bool QDateEditExInterface::isContainer() const
{
   return false;
}

QString QDateEditExInterface::domXml() const
{
   return QLatin1String("<widget class=\"QDateEditEx\" name=\"dateEditEx\">\n"
                        " <property name=\"geometry\">\n"
                        "  <rect>\n"
                        "   <x>0</x>\n"
                        "   <y>0</y>\n"
                        "   <width>100</width>\n"
                        "   <height>18</height>\n"
                        "  </rect>\n"
                        " </property>\n"
                        "</widget>\n");
}

QString QDateEditExInterface::includeFile() const
{
   return QLatin1String("QDateEditEx");
}
