TEMPLATE = lib
TARGET = QtGuiExDesignerPlugin
unix {
    VERSION = 1.1.0
}
QT += widgets uiplugin
CONFIG += plugin debug_and_release

include(QtGuiExDesignerPlugin.pri)

contains(QT_ARCH, i386) {
    CONFIG(release, debug|release): INSTALL_LIB_PATH = $$quote($$system_path($$_PRO_FILE_PWD_/../x86/Release))
    else:CONFIG(debug, debug|release): INSTALL_LIB_PATH = $$quote($$system_path($$_PRO_FILE_PWD_/../x86/Debug))
} else {
    CONFIG(release, debug|release): INSTALL_LIB_PATH = $$quote($$system_path($$_PRO_FILE_PWD_/../x64/Release))
    else:CONFIG(debug, debug|release): INSTALL_LIB_PATH = $$quote($$system_path($$_PRO_FILE_PWD_/../x64/Debug))
}

LIBS += -L$${INSTALL_LIB_PATH}

LIBS += -lQtCoreEx

INCLUDEPATH += $$quote($$system_path($$_PRO_FILE_PWD_/../include/QtCoreEx))
DEPENDPATH += $$quote($$system_path($$_PRO_FILE_PWD_/../include/QtCoreEx))

LIBS += -lQtGuiEx

INCLUDEPATH += $$quote($$system_path($$_PRO_FILE_PWD_/../include/QtGuiEx))
DEPENDPATH += $$quote($$system_path($$_PRO_FILE_PWD_/../include/QtGuiEx))

INSTALL_BIN_PATH = $$quote($$system_path($$[QT_INSTALL_PLUGINS]/designer))
CONFIG(release, debug|release) {
    win32 {
        TARGET_PATH = $$quote($$system_path($$OUT_PWD/release/$${TARGET}))
        QMAKE_POST_LINK += $(COPY) $${TARGET_PATH}.dll $$INSTALL_BIN_PATH
        QMAKE_POST_LINK += && $(COPY) $$quote($$system_path($${INSTALL_LIB_PATH}/QtCoreEx.dll)) $$quote($$system_path($(QTDIR)/bin))
        QMAKE_POST_LINK += && $(COPY) $$quote($$system_path($${INSTALL_LIB_PATH}/QtGuiEx.dll)) $$quote($$system_path($(QTDIR)/bin))
    } else: unix {
        QMAKE_POST_LINK += $(COPY) -d $$OUT_PWD/lib$${TARGET}.* $$INSTALL_BIN_PATH
        QMAKE_POST_LINK += && $(COPY) -d $${INSTALL_LIB_PATH}/libQtCoreEx.* $$quote($$system_path($(QTDIR)/lib))
        QMAKE_POST_LINK += && $(COPY) -d $${INSTALL_LIB_PATH}/libQtGuiEx.* $$quote($$system_path($(QTDIR)/lib))
    }
}
