#ifndef QTGUIEXWIDGETS_H
#define QTGUIEXWIDGETS_H

#include "QtUiPlugin/QDesignerCustomWidgetInterface"
#include <QtUiPlugin>
#include <qplugin.h>

class QtGuiExWidgets : public QObject, public QDesignerCustomWidgetCollectionInterface
{
   Q_OBJECT
   Q_INTERFACES(QDesignerCustomWidgetCollectionInterface)
#if QT_VERSION >= 0x050000
   Q_PLUGIN_METADATA(IID "org.qt-project.Qt.QDesignerCustomWidgetCollectionInterface")
#endif // QT_VERSION >= 0x050000

public:
    explicit QtGuiExWidgets(QObject *parent = 0);
    virtual ~QtGuiExWidgets();

    virtual QList<QDesignerCustomWidgetInterface*> customWidgets() const override;

private:
    QList<QDesignerCustomWidgetInterface*> widgets;
};

#endif // QTGUIEXWIDGETS_H
