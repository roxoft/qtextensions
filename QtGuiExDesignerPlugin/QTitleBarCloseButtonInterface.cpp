#include <QTitleBarCloseButton>
#include "QTitleBarCloseButtonInterface.h"

#include <QtPlugin>

QTitleBarCloseButtonInterface::QTitleBarCloseButtonInterface(QObject* parent)
   : QObject(parent)
{
   initialized = false;
}

void QTitleBarCloseButtonInterface::initialize(QDesignerFormEditorInterface * /*core*/)
{
   if (initialized)
      return;

   initialized = true;
}

bool QTitleBarCloseButtonInterface::isInitialized() const
{
   return initialized;
}

QWidget *QTitleBarCloseButtonInterface::createWidget(QWidget *parent)
{
   return new QTitleBarCloseButton(parent);
}

QString QTitleBarCloseButtonInterface::name() const
{
   return QLatin1String("QTitleBarCloseButton");
}

QString QTitleBarCloseButtonInterface::group() const
{
   return QLatin1String("Qt GUI extensions");
}

QIcon QTitleBarCloseButtonInterface::icon() const
{
   return QIcon();
}

QString QTitleBarCloseButtonInterface::toolTip() const
{
   return QString();
}

QString QTitleBarCloseButtonInterface::whatsThis() const
{
   return QString();
}

bool QTitleBarCloseButtonInterface::isContainer() const
{
   return false;
}

QString QTitleBarCloseButtonInterface::domXml() const
{
   return QLatin1String("<widget class=\"QTitleBarCloseButton\" name=\"titleBarCloseButton\">\n"
                        " <property name=\"geometry\">\n"
                        "  <rect>\n"
                        "   <x>0</x>\n"
                        "   <y>0</y>\n"
                        "   <width>8</width>\n"
                        "   <height>8</height>\n"
                        "  </rect>\n"
                        " </property>\n"
                        "</widget>\n");
}

QString QTitleBarCloseButtonInterface::includeFile() const
{
   return QLatin1String("QTitleBarCloseButton");
}
