#include <QNumberEdit>
#include "QNumberEditInterface.h"

#include <QtPlugin>

QNumberEditInterface::QNumberEditInterface(QObject *parent)
   : QObject(parent)
{
   initialized = false;
}

void QNumberEditInterface::initialize(QDesignerFormEditorInterface * /*core*/)
{
   if (initialized)
      return;

   initialized = true;
}

bool QNumberEditInterface::isInitialized() const
{
   return initialized;
}

QWidget *QNumberEditInterface::createWidget(QWidget *parent)
{
   return new QNumberEdit(parent);
}

QString QNumberEditInterface::name() const
{
   return QLatin1String("QNumberEdit");
}

QString QNumberEditInterface::group() const
{
   return QLatin1String("Qt GUI extensions");
}

QIcon QNumberEditInterface::icon() const
{
   return QIcon();
}

QString QNumberEditInterface::toolTip() const
{
   return QString();
}

QString QNumberEditInterface::whatsThis() const
{
   return QString();
}

bool QNumberEditInterface::isContainer() const
{
   return false;
}

QString QNumberEditInterface::domXml() const
{
   return QLatin1String("<widget class=\"QNumberEdit\" name=\"numberEdit\">\n"
                        " <property name=\"geometry\">\n"
                        "  <rect>\n"
                        "   <x>0</x>\n"
                        "   <y>0</y>\n"
                        "   <width>100</width>\n"
                        "   <height>18</height>\n"
                        "  </rect>\n"
                        " </property>\n"
                        "</widget>\n");
}

QString QNumberEditInterface::includeFile() const
{
   return QLatin1String("QNumberEdit");
}
