#include <QHoverButton>
#include "QHoverButtonInterface.h"
#include <QtPlugin>

QHoverButtonInterface::QHoverButtonInterface(QObject* parent) : QObject(parent)
{
   initialized = false;
}

void QHoverButtonInterface::initialize(QDesignerFormEditorInterface* /*core*/)
{
   if (initialized)
      return;

   initialized = true;
}

bool QHoverButtonInterface::isInitialized() const
{
   return initialized;
}

QString QHoverButtonInterface::name() const
{
   return "QHoverButton"; // Name of the custom widget
}

QString QHoverButtonInterface::group() const
{
   return "Qt GUI extensions"; // Group where it will appear in Designer
}

QIcon QHoverButtonInterface::icon() const
{
   return QIcon();
}

QString QHoverButtonInterface::toolTip() const
{
   return QString();
}

QString QHoverButtonInterface::whatsThis() const
{
   return QString();
}

QString QHoverButtonInterface::includeFile() const
{
   return "QHoverButton"; // Include file for the custom widget
}

QWidget* QHoverButtonInterface::createWidget(QWidget* parent)
{
   return new QHoverButton(parent); // Create an instance of the custom widget
}

bool QHoverButtonInterface::isContainer() const
{
   return false; // This widget cannot contain other widgets
}

QString QHoverButtonInterface::domXml() const
{
   return QLatin1String("<widget class=\"QHoverButton\" name=\"hoverButton\">\n"
      " <property name=\"geometry\">\n"
      "  <rect>\n"
      "   <x>0</x>\n"
      "   <y>0</y>\n"
      "   <width>100</width>\n"
      "   <height>30</height>\n"
      "  </rect>\n"
      " </property>\n"
      "</widget>\n");
}
