#include <QComboBoxEx>
#include "QComboBoxExInterface.h"

#include <QtPlugin>

QComboBoxExInterface::QComboBoxExInterface(QObject *parent)
   : QObject(parent)
{
   initialized = false;
}

void QComboBoxExInterface::initialize(QDesignerFormEditorInterface * /*core*/)
{
   if (initialized)
      return;

   initialized = true;
}

bool QComboBoxExInterface::isInitialized() const
{
   return initialized;
}

QWidget *QComboBoxExInterface::createWidget(QWidget *parent)
{
   QComboBoxEx *comboBox = new QComboBoxEx(parent);

   comboBox->setDesignerMode();

   return comboBox;
}

QString QComboBoxExInterface::name() const
{
   return QLatin1String("QComboBoxEx");
}

QString QComboBoxExInterface::group() const
{
   return QLatin1String("Qt GUI extensions");
}

QIcon QComboBoxExInterface::icon() const
{
   return QIcon();
}

QString QComboBoxExInterface::toolTip() const
{
   return QString();
}

QString QComboBoxExInterface::whatsThis() const
{
   return QString();
}

bool QComboBoxExInterface::isContainer() const
{
   return true;
}

QString QComboBoxExInterface::domXml() const
{
   return QLatin1String("<widget class=\"QComboBoxEx\" name=\"comboBoxEx\">\n"
                        " <property name=\"geometry\">\n"
                        "  <rect>\n"
                        "   <x>0</x>\n"
                        "   <y>0</y>\n"
                        "   <width>100</width>\n"
                        "   <height>20</height>\n"
                        "  </rect>\n"
                        " </property>\n"
                        "</widget>\n");
}

QString QComboBoxExInterface::includeFile() const
{
   return QLatin1String("QComboBoxEx");
}
