#ifndef QDATETIMEEDITEXINTERFACE_H
#define QDATETIMEEDITEXINTERFACE_H

#include "QtUiPlugin/QDesignerCustomWidgetInterface"

class QDateTimeEditExInterface : public QObject, public QDesignerCustomWidgetInterface
{
   Q_OBJECT
   Q_INTERFACES(QDesignerCustomWidgetInterface)

public:
   QDateTimeEditExInterface(QObject *parent = 0);

   bool isContainer() const;
   bool isInitialized() const;
   QIcon icon() const;
   QString domXml() const;
   QString group() const;
   QString includeFile() const;
   QString name() const;
   QString toolTip() const;
   QString whatsThis() const;
   QWidget *createWidget(QWidget *parent);
   void initialize(QDesignerFormEditorInterface *core);

private:
   bool initialized;
};

#endif // QDATETIMEEDITEXINTERFACE_H
