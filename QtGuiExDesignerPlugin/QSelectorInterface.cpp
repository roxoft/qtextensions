#include <QSelector>
#include "QSelectorInterface.h"

#include <QtPlugin>

QSelectorInterface::QSelectorInterface(QObject *parent)
   : QObject(parent)
{
   initialized = false;
}

void QSelectorInterface::initialize(QDesignerFormEditorInterface * /*core*/)
{
   if (initialized)
      return;

   initialized = true;
}

bool QSelectorInterface::isInitialized() const
{
   return initialized;
}

QWidget *QSelectorInterface::createWidget(QWidget *parent)
{
   return new QSelector(parent);
}

QString QSelectorInterface::name() const
{
   return QLatin1String("QSelector");
}

QString QSelectorInterface::group() const
{
   return QLatin1String("Qt GUI extensions");
}

QIcon QSelectorInterface::icon() const
{
   return QIcon();
}

QString QSelectorInterface::toolTip() const
{
   return QString();
}

QString QSelectorInterface::whatsThis() const
{
   return QString();
}

bool QSelectorInterface::isContainer() const
{
   return false;
}

QString QSelectorInterface::domXml() const
{
   return QLatin1String("<widget class=\"QSelector\" name=\"selector\">\n"
                        " <property name=\"geometry\">\n"
                        "  <rect>\n"
                        "   <x>0</x>\n"
                        "   <y>0</y>\n"
                        "   <width>100</width>\n"
                        "   <height>20</height>\n"
                        "  </rect>\n"
                        " </property>\n"
                        "</widget>\n");
}

QString QSelectorInterface::includeFile() const
{
   return QLatin1String("QSelector");
}
