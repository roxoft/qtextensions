#include <QDateTimeEditEx>
#include "QDateTimeEditExInterface.h"

#include <QtPlugin>

QDateTimeEditExInterface::QDateTimeEditExInterface(QObject *parent)
   : QObject(parent)
{
   initialized = false;
}

void QDateTimeEditExInterface::initialize(QDesignerFormEditorInterface * /*core*/)
{
   if (initialized)
      return;

   initialized = true;
}

bool QDateTimeEditExInterface::isInitialized() const
{
   return initialized;
}

QWidget *QDateTimeEditExInterface::createWidget(QWidget *parent)
{
   return new QDateTimeEditEx(parent);
}

QString QDateTimeEditExInterface::name() const
{
   return QLatin1String("QDateTimeEditEx");
}

QString QDateTimeEditExInterface::group() const
{
   return QLatin1String("Qt GUI extensions");
}

QIcon QDateTimeEditExInterface::icon() const
{
   return QIcon();
}

QString QDateTimeEditExInterface::toolTip() const
{
   return QString();
}

QString QDateTimeEditExInterface::whatsThis() const
{
   return QString();
}

bool QDateTimeEditExInterface::isContainer() const
{
   return false;
}

QString QDateTimeEditExInterface::domXml() const
{
   return QLatin1String("<widget class=\"QDateTimeEditEx\" name=\"dateTimeEditEx\">\n"
                        " <property name=\"geometry\">\n"
                        "  <rect>\n"
                        "   <x>0</x>\n"
                        "   <y>0</y>\n"
                        "   <width>100</width>\n"
                        "   <height>18</height>\n"
                        "  </rect>\n"
                        " </property>\n"
                        "</widget>\n");
}

QString QDateTimeEditExInterface::includeFile() const
{
   return QLatin1String("QDateTimeEditEx");
}
