#include <QTableViewEx>
#include "QTableViewExInterface.h"

#include <QtPlugin>

QTableViewExInterface::QTableViewExInterface(QObject* parent)
   : QObject(parent)
{
   initialized = false;
}

void QTableViewExInterface::initialize(QDesignerFormEditorInterface * /*core*/)
{
   if (initialized)
      return;

   initialized = true;
}

bool QTableViewExInterface::isInitialized() const
{
   return initialized;
}

QWidget *QTableViewExInterface::createWidget(QWidget *parent)
{
   return new QTableViewEx(parent);
}

QString QTableViewExInterface::name() const
{
   return QLatin1String("QTableViewEx");
}

QString QTableViewExInterface::group() const
{
   return QLatin1String("Qt GUI extensions");
}

QIcon QTableViewExInterface::icon() const
{
   return QIcon();
}

QString QTableViewExInterface::toolTip() const
{
   return QString();
}

QString QTableViewExInterface::whatsThis() const
{
   return QString();
}

bool QTableViewExInterface::isContainer() const
{
   return false;
}

QString QTableViewExInterface::domXml() const
{
   return QLatin1String("<widget class=\"QTableViewEx\" name=\"tableViewEx\">\n"
                        " <property name=\"geometry\">\n"
                        "  <rect>\n"
                        "   <x>0</x>\n"
                        "   <y>0</y>\n"
                        "   <width>100</width>\n"
                        "   <height>18</height>\n"
                        "  </rect>\n"
                        " </property>\n"
                        "</widget>\n");
}

QString QTableViewExInterface::includeFile() const
{
   return QLatin1String("QTableViewEx");
}
