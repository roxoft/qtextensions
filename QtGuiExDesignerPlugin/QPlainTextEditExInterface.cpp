#include <QPlainTextEditEx>
#include "QPlainTextEditExInterface.h"

#include <QtPlugin>

QPlainTextEditExInterface::QPlainTextEditExInterface(QObject* parent)
   : QObject(parent)
{
   initialized = false;
}

void QPlainTextEditExInterface::initialize(QDesignerFormEditorInterface * /*core*/)
{
   if (initialized)
      return;

   initialized = true;
}

bool QPlainTextEditExInterface::isInitialized() const
{
   return initialized;
}

QWidget* QPlainTextEditExInterface::createWidget(QWidget *parent)
{
   return new QPlainTextEditEx(parent);
}

QString QPlainTextEditExInterface::name() const
{
   return QLatin1String("QPlainTextEditEx");
}

QString QPlainTextEditExInterface::group() const
{
   return QLatin1String("Qt GUI extensions");
}

QIcon QPlainTextEditExInterface::icon() const
{
   return QIcon();
}

QString QPlainTextEditExInterface::toolTip() const
{
   return QString();
}

QString QPlainTextEditExInterface::whatsThis() const
{
   return QString();
}

bool QPlainTextEditExInterface::isContainer() const
{
   return false;
}

QString QPlainTextEditExInterface::domXml() const
{
   return QLatin1String("<widget class=\"QPlainTextEditEx\" name=\"plainTextEditEx\">\n"
                        " <property name=\"geometry\">\n"
                        "  <rect>\n"
                        "   <x>0</x>\n"
                        "   <y>0</y>\n"
                        "   <width>100</width>\n"
                        "   <height>20</height>\n"
                        "  </rect>\n"
                        " </property>\n"
                        "</widget>\n");
}

QString QPlainTextEditExInterface::includeFile() const
{
   return QLatin1String("QPlainTextEditEx");
}
