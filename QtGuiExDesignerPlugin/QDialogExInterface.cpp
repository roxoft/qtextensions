#include <QDialogEx>
#include "QDialogExInterface.h"

#include <QtPlugin>

QDialogExInterface::QDialogExInterface(QObject *parent)
   : QObject(parent)
{
   initialized = false;
}

void QDialogExInterface::initialize(QDesignerFormEditorInterface * /*core*/)
{
   if (initialized)
      return;

   initialized = true;
}

bool QDialogExInterface::isInitialized() const
{
   return initialized;
}

QWidget *QDialogExInterface::createWidget(QWidget *parent)
{
   return new QDialogEx(parent);
}

QString QDialogExInterface::name() const
{
   return QLatin1String("QDialogEx");
}

QString QDialogExInterface::group() const
{
   return QLatin1String("Qt GUI extensions");
}

QIcon QDialogExInterface::icon() const
{
   return QIcon();
}

QString QDialogExInterface::toolTip() const
{
   return QString();
}

QString QDialogExInterface::whatsThis() const
{
   return QString();
}

bool QDialogExInterface::isContainer() const
{
   return true;
}

QString QDialogExInterface::domXml() const
{
   return QLatin1String("<widget class=\"QDialogEx\" name=\"dialogEx\">\n"
                        " <property name=\"geometry\">\n"
                        "  <rect>\n"
                        "   <x>0</x>\n"
                        "   <y>0</y>\n"
                        "   <width>640</width>\n"
                        "   <height>480</height>\n"
                        "  </rect>\n"
                        " </property>\n"
                        "</widget>\n");
}

QString QDialogExInterface::includeFile() const
{
   return QLatin1String("QDialogEx");
}
