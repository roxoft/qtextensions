#include <QTreeViewEx>
#include "QTreeViewExInterface.h"

#include <QtPlugin>

QTreeViewExInterface::QTreeViewExInterface(QObject *parent)
   : QObject(parent)
{
   initialized = false;
}

void QTreeViewExInterface::initialize(QDesignerFormEditorInterface * /*core*/)
{
   if (initialized)
      return;

   initialized = true;
}

bool QTreeViewExInterface::isInitialized() const
{
   return initialized;
}

QWidget *QTreeViewExInterface::createWidget(QWidget *parent)
{
   return new QTreeViewEx(parent);
}

QString QTreeViewExInterface::name() const
{
   return QLatin1String("QTreeViewEx");
}

QString QTreeViewExInterface::group() const
{
   return QLatin1String("Qt GUI extensions");
}

QIcon QTreeViewExInterface::icon() const
{
   return QIcon();
}

QString QTreeViewExInterface::toolTip() const
{
   return QString();
}

QString QTreeViewExInterface::whatsThis() const
{
   return QString();
}

bool QTreeViewExInterface::isContainer() const
{
   return false;
}

QString QTreeViewExInterface::domXml() const
{
   return QLatin1String("<widget class=\"QTreeViewEx\" name=\"treeViewEx\">\n"
                        " <property name=\"geometry\">\n"
                        "  <rect>\n"
                        "   <x>0</x>\n"
                        "   <y>0</y>\n"
                        "   <width>100</width>\n"
                        "   <height>18</height>\n"
                        "  </rect>\n"
                        " </property>\n"
                        "</widget>\n");
}

QString QTreeViewExInterface::includeFile() const
{
   return QLatin1String("QTreeViewEx");
}
