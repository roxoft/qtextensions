#include <QSizeGripEx>
#include "QSizeGripExInterface.h"

#include <QtPlugin>

QSizeGripExInterface::QSizeGripExInterface(QObject *parent)
   : QObject(parent)
{
   initialized = false;
}

void QSizeGripExInterface::initialize(QDesignerFormEditorInterface * /*core*/)
{
   if (initialized)
      return;

   initialized = true;
}

bool QSizeGripExInterface::isInitialized() const
{
   return initialized;
}

QWidget *QSizeGripExInterface::createWidget(QWidget *parent)
{
   return new QSizeGripEx(parent);
}

QString QSizeGripExInterface::name() const
{
   return QLatin1String("QSizeGripEx");
}

QString QSizeGripExInterface::group() const
{
   return QLatin1String("Qt GUI extensions");
}

QIcon QSizeGripExInterface::icon() const
{
   return QIcon();
}

QString QSizeGripExInterface::toolTip() const
{
   return QString();
}

QString QSizeGripExInterface::whatsThis() const
{
   return QString();
}

bool QSizeGripExInterface::isContainer() const
{
   return false;
}

QString QSizeGripExInterface::domXml() const
{
   return QLatin1String("<widget class=\"QSizeGripEx\" name=\"sizeGripEx\">\n"
                        " <property name=\"geometry\">\n"
                        "  <rect>\n"
                        "   <x>0</x>\n"
                        "   <y>0</y>\n"
                        "   <width>13</width>\n"
                        "   <height>13</height>\n"
                        "  </rect>\n"
                        " </property>\n"
                        "</widget>\n");
}

QString QSizeGripExInterface::includeFile() const
{
   return QLatin1String("QSizeGripEx");
}
