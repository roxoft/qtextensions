#include <QIncrementalSearchWidget>
#include "QIncrementalSearchWidgetInterface.h"

#include <QtPlugin>

QIncrementalSearchWidgetInterface::QIncrementalSearchWidgetInterface(QObject* parent)
   : QObject(parent)
{
   initialized = false;
}

void QIncrementalSearchWidgetInterface::initialize(QDesignerFormEditorInterface * /*core*/)
{
   if (initialized)
      return;

   initialized = true;
}

bool QIncrementalSearchWidgetInterface::isInitialized() const
{
   return initialized;
}

QWidget *QIncrementalSearchWidgetInterface::createWidget(QWidget *parent)
{
   return new QIncrementalSearchWidget(parent);
}

QString QIncrementalSearchWidgetInterface::name() const
{
   return QLatin1String("QIncrementalSearchWidget");
}

QString QIncrementalSearchWidgetInterface::group() const
{
   return QLatin1String("Qt GUI extensions");
}

QIcon QIncrementalSearchWidgetInterface::icon() const
{
   return QIcon();
}

QString QIncrementalSearchWidgetInterface::toolTip() const
{
   return QString();
}

QString QIncrementalSearchWidgetInterface::whatsThis() const
{
   return QString();
}

bool QIncrementalSearchWidgetInterface::isContainer() const
{
   return false;
}

QString QIncrementalSearchWidgetInterface::domXml() const
{
   return QLatin1String("<widget class=\"QIncrementalSearchWidget\" name=\"incrementalSearchWidget\">\n"
                        " <property name=\"geometry\">\n"
                        "  <rect>\n"
                        "   <x>0</x>\n"
                        "   <y>0</y>\n"
                        "   <width>250</width>\n"
                        "   <height>25</height>\n"
                        "  </rect>\n"
                        " </property>\n"
                        "</widget>\n");
}

QString QIncrementalSearchWidgetInterface::includeFile() const
{
   return QLatin1String("QIncrementalSearchWidget");
}
