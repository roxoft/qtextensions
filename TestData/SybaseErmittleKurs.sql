DROP PROCEDURE ermittle_kurs;
create procedure                                                                                
ermittle_kurs(in @bankID decimal(8),in @skontro decimal(7), in @check char(1) default 'F', out @kurs decimal(28,16))
on exception resume
begin
    declare @kurs decimal(28,16);
    if @check = 'T' then
       set @kurs = 100.12345678;
    elseif @bankID = 27 and @skontro = 1 then
       set @kurs = 12.12345678;
    else
       set @kurs = 112.12345678;
    end if;		
end;

